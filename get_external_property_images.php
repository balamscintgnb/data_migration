<?php
extract($_POST);

if($subdomain_cnb!=''){
	$json = file_get_contents('data/'.$subdomain_cnb.'/properties.json');
	$json_data = json_decode($json);
	
	$properties = $json_data;
	$count = 0;
	foreach($properties as $row){
		$notice_images	= $row->notice_images;
		$count +=sizeof($notice_images);
	}
	
	foreach($properties as $row){
		$notice_images	= $row->notice_images;
		if(sizeof($notice_images)>0){
?>
<span id="img_row_<?=$row->notice_id?>" style="display:none;">
	<input type="checkbox" checked="checked" name="notice_gallery_images[]" notice-key="<?=$row->notice_id?>" value="<?=implode(',',$notice_images)?>" />
</span>
<?php 
		}
	} 
?>		  				
<span id="hid_img_count">0</span> / <?php echo $count;?> <span id="last_row">images downloading</span>
<script>

$(document).ready(function(){
	setTimeout(function(){ start_copy_images(); }, 3000);
});	

function start_copy_images(){
	var chkArray = $('input[name="notice_gallery_images[]"]:checked');
	if(chkArray.length>0){
		var obj = chkArray[0];
		var images = obj.value;
		var notice_id = $(obj).attr('notice-key');

		$.post('copy_property_images.php',{subdomain_cnb:'<?=$subdomain_cnb?>',notice_id:notice_id,images:images}, function(data){
			var info = jQuery.parseJSON(data);
			if(info.success) {
				$('#img_row_'+notice_id).html('');
				var total_count = parseInt($('#hid_img_count').html())+parseInt(info.count);
				
				$('#hid_img_count').html(total_count);
				start_copy_images();
			}
		});
	}else{
		$('#last_row').html('images downloaded');
	}
}
</script>
<?php } ?>