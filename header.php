<?php  require_once 'header_init.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <title>Import</title>
	    <link rel="icon" type="image/png" href="assets/images/gnb_properties_favicon.png">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css" />
	    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/style.css" />
	    <link rel="stylesheet" type="text/css" media="screen" href="assets/fonts/fonts.css" />
	    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />

		<?php
		// if(isset($_SESSION['user'])) {
		$board_information_data = $db_connect->get_boardinformation();
		$board_information = json_decode($board_information_data);
		// }
		?>
