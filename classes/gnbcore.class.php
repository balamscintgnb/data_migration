<?php
class GNBCore{

    private $connection;
    public $table;
    private $isDevelopment;

    public function __construct($sql_details, $isDevelopment=false){
        try {

            $this->isDevelopment=$isDevelopment;
            $this->connection = new PDO("mysql:host={$sql_details['host']};dbname={$sql_details['database']};charset=utf8", $sql_details['username'], $sql_details['password']
                , array(PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

			if(isset($isDevelopment) && $isDevelopment){
				$this->connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            }

            return $this->connection;

			//if (!empty($connection_options['collation'])) {
			  //$this->connection->exec('SET NAMES utf8 COLLATE ' . $connection_options['collation']);
			/*}
			else {*/
			  //$this->connection->exec('SET NAMES utf8');
			//}
			exit;

        } catch (PDOException $exception) {
			if($this->isDevelopment){
                Utility::debug($exception);
			}else{
            	Utility::debug('Unknown Error1: Sub Domain not found');
			}
        }
    }

    //todo remove unwanted prepare
    public function create_database($database_name){
        try {
            $database_create_query = "CREATE DATABASE `$database_name`";
            $database_create_query_prepare = $this->connection->prepare($database_create_query);
            if ($database_create_query_prepare->execute()) {
                echo json_encode(array('success' => true, 'message' => 'Database created successfully'));
            } else {
                $error = $database_create_query_prepare->errorInfo();

                echo json_encode(array('success' => false, 'message' => 'Unable to create Database ' . $error[2]));
            }

        } catch (PDOException $exception) {
            echo json_encode(array('success' => false, 'message' => 'Error : ' . $exception->getMessage()));
        }
    }

    public function create_table($database_name){
        try {
            $table_creation_query = $this->connection->prepare("CREATE TABLE $database_name.board_information
		        (`id` INT(11) NOT NULL AUTO_INCREMENT,
		        `subdomain` VARCHAR(255) NOT NULL,
		        `agent_name` VARCHAR(255) NOT NULL,
		        `agent_logo` TEXT NOT NULL,
		        `import_completion_estimated` DATETIME NOT NULL, PRIMARY KEY(`id`));");

            if ($table_creation_query->execute()) {
                echo json_encode(array('success' => true, 'message' => 'table created successfully'));
            } else {
                $error = $table_creation_query->errorInfo();

                echo json_encode(array('success' => false, 'message' => 'unable to create table' . $error[2]));
            }

        } catch (PDOException $exception) {
            echo json_encode(array('success' => false, 'message' => 'Error : ' . $exception->getMessage()));

        }

    }

    public function insert_table($database_name, $agent_name, $agent_logo, $completion_date){
        try {
            $null = 'NULL';
            $insert_table_query = $this->connection->prepare("INSERT INTO  $database_name.board_information
		        VALUES (:id,:subdomain,:agent_name,:agent_logo,:completion_date);");
		            $insert_table_query->bindParam(':id', $null, PDO::PARAM_NULL);
		            $insert_table_query->bindParam(':subdomain', $database_name);
		            $insert_table_query->bindParam(':agent_name', $agent_name);
		            $insert_table_query->bindParam(':agent_logo', $agent_logo);
		            $insert_table_query->bindParam(':completion_date', $completion_date);

            if ($insert_table_query->execute()) {
                echo json_encode(array('success' => true, 'message' => 'Data Successfully Inserted'));
            } else {
                $error = $insert_table_query->errorInfo();
                echo json_encode(array('success' => false, 'message' => 'Data not Inserted' . $error[2]));
            }

        } catch (PDOException $exception) {
            echo json_encode(array('success' => false, 'message' => 'Error' . $exception->getMessage()));
        }
    }

    public function insert_table_common($columns, $values, $table_name){
        try {
            $null = 'NULL';

			$column_string = implode(',', $columns);
			
			$column_values = array();
			foreach($columns as $column){
				$column_values[] = ':'.$column;
			}
			$column_values_string = implode(',', $column_values);
			
            $insert_table_query = $this->connection->prepare("INSERT INTO  $table_name ($column_string) VALUES ($column_values_string);");

			$column_count=count($columns);
			$value_count=count($values);

			if($column_count!=$value_count || $value_count<=0){
				return json_encode(array('success' => false, 'message' => 'Fields and Values mismatch'));
			}

			for($i=0; $i<$column_count; $i++){
				$insert_table_query->bindParam(':'.$columns[$i], $values[$i]);
			}

            if ($insert_table_query->execute()) {
                return json_encode(array('success' => true, 'message' => 'Data Successfully Inserted', 'id'=>$this->connection->lastInsertId()));
            } else {
                $error = $insert_table_query->errorInfo();
                return json_encode(array('success' => false, 'message' => 'Data not Inserted' . $error[2]));
            }

        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'message' => 'Error ' . $exception->getMessage()));
        }
    }

    public function get_boardinformation(){

        try {
            $board_information_query = "SELECT * FROM board_information LIMIT 1";
            $board_information_query_prepare = $this->connection->prepare($board_information_query);

            if ($board_information_query_prepare->execute()) {
                $data = $board_information_query_prepare->fetchObject();
                return json_encode(array('success' => true, 'data' => $data, 'message' => 'Data Successfully returned'));

            } else {
                $error = $board_information_query_prepare->errorInfo();
                return json_encode(array('success' => false, 'data' => null, 'message' => 'Error on data returning ' . $error[2]));

            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));
        }

    }

    public function get_datas($table, $offest=0, $limit=1){
        try {

			$data=null;

			if($table!=''){
                // $tablelist_query = "SELECT * FROM  $table  WHERE `RECORD_UPLOADED`=0  limit :offset , :limit";

                $tablelist_query = "SELECT * FROM $table  limit :offset , :limit";

                
                $table_query_prepare = $this->connection->prepare($tablelist_query);
				$table_query_prepare->bindParam(':limit', $limit, PDO::PARAM_INT);
                $table_query_prepare->bindParam(':offset', $offest, PDO::PARAM_INT);

	            if ($table_query_prepare->execute()) {
                    $data = $table_query_prepare->fetchAll();
                    Utility::filter_obj($data);					
                    //$result = array('success' => true, 'data' => $data, 'message' => 'Data Successfully returned');
	            } else {
					echo "\nPDO::errorInfo():\n";
					print_r($this->connection->errorInfo());

	            }
			}
        } catch (PDOException $exception) {
			echo '<pre><code>';
			var_dump($exception);
			echo '</code></pre>';
        }

		return $data;
    }

    public function get_tablecolumns(){
        try {
            $column_query = "SHOW COLUMNS FROM $this->table";
            $column_query_prepare = $this->connection->prepare($column_query);
            if ($column_query_prepare->execute()) {
                $data = $column_query_prepare->fetchAll();
                return json_encode(array('success' => true, 'data' => $data, 'message' => 'Data Successfully returned'));

            } else {
                return json_encode(array('success' => false, 'data' => null, 'message' => 'Error : fetching on data'));

            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));

        }
    }

    public function truncate_table(){
        try {
            $table_truncate_query = "TRUNCATE TABLE `$this->table`";
            $table_truncate_query_prepare = $this->connection->prepare($table_truncate_query);
            if ($table_truncate_query_prepare->execute()) {
                echo json_encode(array('success' => true, 'message' => 'Table truncated succcessfully'));
            } else {
                $error = $table_truncate_query_prepare->errorInfo();
                echo json_encode(array('success' => false, 'message' => 'Error : Table not truncated ' . $error[2]));

            }
        } catch (PDOException $pdoexception) {
            echo json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));
        }

    }

    public function delete_table(){
        try {
            $table_delete_query = "DROP TABLE `$this->table`";
            $table_delete_query_prepare = $this->connection->prepare($table_delete_query);
            if ($table_delete_query_prepare->execute()) {
                echo json_encode(array('success' => true, 'message' => 'Table deleted succcessfully'));
            } else {
                $error = $table_delete_query_prepare->errorInfo();
                echo json_encode(array('success' => false, 'message' => 'Error : Table not deleted ' . $error[2]));

            }
        } catch (PDOException $pdoexception) {
            echo json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));
        }
    }

    public function queryExecute($sql){

        $result=false;

        try {
            $result = $this->connection->query($sql);
        } catch (PDOException $exception) {
            //todo debug
            var_dump($exception->getMessage());

        }
        return $result;
    }

    public function queryFetch($sql){

        $data=false;

        try {
            $query_result = $this->connection->query($sql);
            if ($query_result) {
                $data = $query_result->fetchAll();
                Utility::filter_obj($data);
                return json_encode(array('success' => true, 'data' => $data, 'message' => 'Query executed succcessfully'));
            }else{
                return json_encode(array('success' => false, 'data' => null, 'message' => 'Query not executed'));
            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));
        }
    }

    public function get_tablelists($table, $record_limit){
        try {
            $query_table_list = "select * from $table where `RECORD_UPLOADED`=0 limit 0, $record_limit";
            $query_table_list_prepare = $this->connection->prepare($query_table_list);

            if ($query_table_list_prepare->execute()) {
                $data = $query_table_list_prepare->fetchAll();
                Utility::filter_obj($data);	
                return json_encode(array('success' => true, 'data' => $data, 'message' => 'Query executed succcessfully'));

            } else {
                return json_encode(array('success' => false, 'data' => null, 'message' => 'Query not executed'));
            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));

        }
    }

	//todo use prepared statement well
    public function record_update_table($table, $update_ids, $status){
        $update_ids_imploded = implode(',', $update_ids);
        try {
            $query_table_update = "update $table set `RECORD_UPLOADED`=$status where `ID` in ($update_ids_imploded)";
            $query_table_update_prepare = $this->connection->prepare($query_table_update);
            if ($query_table_update_prepare->execute()) {

                return json_encode(array('success' => true, 'message' => 'Records updated successfully '));

            } else {
                return json_encode(array('success' => false, 'message' => 'Query not executed'));

            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'message' => 'Error' . $exception->getMessage()));
        }
    }
    public function record_update_server_info($table, $row_id, $server_id){
        try {
            $query_table_update = "update $table set `RECORD_UPLOADED` = '1',`SERVER_ID` = '$server_id' where `ID` = '$row_id'";
            $query_table_update_prepare = $this->connection->prepare($query_table_update);
            if ($query_table_update_prepare->execute()) {
            	
                return json_encode(array('success' => true, 'message' => 'Record updated successfully '.$query_table_update));
                
            } else {
            	
                return json_encode(array('success' => false, 'message' => 'Query not executed'));
                
            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'message' => 'Error' . $exception->getMessage()));
        }
    }
    public function get_tableinformationlist($db){
        try {
            $tablelist_query = "SELECT TABLE_NAME,TABLE_ROWS,UPDATE_TIME FROM information_schema.tables WHERE table_schema=?  AND table_name NOT LIKE 'board_information'";

            $table_query_prepare = $this->connection->prepare($tablelist_query);
            $table_query_prepare->bindParam(1, $db);
            if ($table_query_prepare->execute()) {
                $data = $table_query_prepare->fetchAll();
                return json_encode(array('success' => true, 'data' => $data, 'message' => 'Data Successfully returned'));

            } else {
                return json_encode(array('success' => false, 'data' => null, 'message' => 'Error : fetching on data'));

            }
        } catch (PDOException $exception) {
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));

        }
    }

    public function update_boardinformation($table, $status){
        try{ 
         $update_query = "UPDATE $table SET `server`=$status";
         $update_query_prepare = $this->connection->prepare($update_query);
         if ($update_query_prepare->execute()) {
            return json_encode(array('success' => true, 'message' => 'Data Successfully returned'));

         }

        }catch(PDOException $exception){
            return json_encode(array('success' => false, 'data' => null, 'message' => 'Error' . $exception->getMessage()));

        }
    }

}
