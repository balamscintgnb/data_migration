<?php 

class Utility{

    public function getHost($url){
        $host = explode('.', $url);
        return $host[1];
    }
	
    public static function get_subdomain($url){
        list($subdomain,$host) = explode('.', $url);
         return 'di_cnb_'.$subdomain;
    }

    public static function getCurlData($url, $data, $header = array(), $debug = false){
    	
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        if (isset($header) && is_array($header) && sizeof($header) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60); // connection timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 300); //request timeout in scecondsCURLOPT_CONNECTTIMEOUT

        if ($debug) {
            $handler = fopen('php://temp', 'w+');
            curl_setopt($ch, CURLOPT_VERBOSE, $debug); //true 0
            curl_setopt($ch, CURLOPT_STDERR, $handler);
        }

        $result = curl_exec($ch);
        
        if ($debug) {
           if(isset($header) && is_array($header) && sizeof($header)>0){
               echo "<pre><code>";
               echo "<b>URL, Request Headers:</b> <br/><br/>";
               var_dump($url, $header);
               echo "<br/><br/>";
               echo "</pre></code>";
           }

           if(isset($data) && ((is_array($data) && sizeof($data)>0) || (!is_array($data) && trim($data)!=''))){
               echo "<pre><code>";
               echo "<b>Request Body:</b> <br/><br/>";
               var_dump($data);
               echo "<br/><br/>";
               echo "</pre></code>";
           }

            if ($handler != '') {
                fclose($handler);
            }
        }

        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            $error = $error_message;

            $error .= "\n<br/>" . 'cURL error: ' . curl_error($ch) . '. Curl error code: ' . $errno;
            $result = json_encode(array("success" => false, "message" => $error));

        }

        if($debug){
            echo "<pre><code>";
            echo "<b>Response:</b> <br/><br/>";
            var_dump($result);
            echo "<br/><br/>";
            echo "</pre></code>";
        }

        curl_close($ch);

        return $result;
    }
	
	function validate_email_id($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		    return true;
		} else {
			return false;
		}
    }
    
    public static function debug($exception){

        $message = json_encode(array('message'=> $exception));

        echo '
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Import Debug</title>
                <link rel="icon" type="image/png" href="assets/images/gnb_properties_favicon.png">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css" />
                <link rel="stylesheet" type="text/css" media="screen" href="assets/css/style.css" />
                <link rel="stylesheet" type="text/css" media="screen" href="assets/fonts/fonts.css" />
            </head>
            <body>
                <div>';

        echo '<pre><code>';
        var_dump($exception);
        echo '</code></pre>';
        
        echo '
                    </div>
                </body>
            </html>';
        exit;
    }

    public static function filter_obj(&$value){
        if(is_array($value) || is_obj($value)){
            array_walk_recursive($value, 'self::filter');
        }else{
            $value=self::filter($value);
        }
    } 

    public static function clearValues(&$value){
        $value = str_ireplace(array('n/a'), '', $value);
    }

    public static function filter(&$item){
        $item = utf8_encode(trim($item));
    }

    public static function html_escape($html){
        $xml = new DOMDocument();
        $html = stripcslashes($html);
        $html = str_replace("\r", "", $html);
        $html = str_replace("\n", "", $html);
        $xml->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        $html = $xml->saveHTML();
        $html = preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $html);
        return $html;
    }
    
    public function uploadCDNServer($data, $subdomain_name, $isDevelopment){
    	$uploaded_data = array();
    	$absolute_path = dirname(__FILE__);
    	//$absolute_path = str_replace('/cnb/classes', '/cnb', $absolute_path);
    	foreach($data as $d){
            $local_path = "data/$subdomain_name/".$d->PROPERTY_IMAGE; //'C:\wamp64\www\import\data_mappings\ashbury_software/'.
            $extension  = pathinfo($d->PROPERTY_IMAGE,PATHINFO_EXTENSION);
            $d->PROPERTY_IMAGE = md5($local_path).'.'.$extension;

    		if($d->PROPERTY_IMAGE_TYPE=='display_photo'){
    			$server_path = 'cnb/images/'.$subdomain_name.'/notices/'.$d->PROPERTY_SERVER_ID.'/'.$d->PROPERTY_IMAGE;
    		}else{
    			$server_path = 'cnb/images/'.$subdomain_name.'/notices/'.$d->PROPERTY_SERVER_ID.'/gallery/'.$d->PROPERTY_IMAGE;
            }

    		if(file_exists($local_path)){
                $url = $this->upload_to_cdn_server($local_path, $server_path);
    		}else{
    			$local_path = str_replace('.jpg','.JPG',$local_path);

    			$url = $this->upload_to_cdn_server($local_path, $server_path);
    		}
			
			//@unlink($local_path);
			$d->SERVER_URL = $url;
    		$uploaded_data[] = $d;
    	}
    	return $uploaded_data;
    }
    
    public function upload_to_cdn_server($local_path, $remote_path){
		require_once('includes/amazon/S3.php');
		$S3 = new S3_Class(AWS_KEY, AWS_SECRET);
		$S3->putObject($local_path, $remote_path);
		return AWS_CDN_BASE_URL_USER_UPLOADS.$remote_path;
	}
	
	public function remove_from_cdn_server($remote_path){
		require_once('includes/amazon/S3.php');
		$S3 = new S3_Class(AWS_KEY, AWS_SECRET);
		$result = $S3->deleteObject($remote_path);
		return $result;
    }
    
    public static function validate_email($email){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		else{
			return false;
		}
	}
    
    public static function unformatted_price($value){ 

        if($value){
        $value = preg_replace('/[A-z]/','',$value);
        return str_replace(array(',','£'), '', $value);
        }
        else 
        {
             return 0.00;
        }

    }
    
    public static function unformatted_price_frequency($value){ 

        if($value){
        $value = preg_replace('/[0-9 ^.]/','',$value);
        return str_replace(array(',','£'), '', $value);
        }
        else 
        {
             return '';
        }

    }

    public static function convert_tosqldate($date_in, $format){

       $date_format = DateTime::createFromFormat($format, trim($date_in));
  
        if($date_format) { 
        return $date_format->format('Y-m-d');
        }
        else 
        { 
            return NULL;
        }
    }
    
    public static function convert_tosqldatetime($date_in, $format){

        $date_format = DateTime::createFromFormat($format, trim($date_in));
   
         if($date_format) { 
         return $date_format->format('Y-m-d H:i:s');
         }
         else 
         { 
             return NULL;
         }
     }
    public static function format_content($in_string){ 

        $string =  iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $in_string);
        $string = addslashes(preg_replace('/^\p{Z}+|\p{Z}+$/u', '', $string));
        return $string;
    }
}