<?php

extract($_POST);

if ($_POST['filename']!='') {
	
	$my_url = parse_url($_SERVER['HTTP_HOST']);
	$my_path = explode('.',$my_url['path']);
	
	$upload_path='uploads/'.$_POST['filename'];

	mkdir('data/'.$my_path[0].'/'.$_POST['folder_path_to'].'/',0777);
	
	$zip = new ZipArchive;
	$res = $zip->open($upload_path);
	if ($res === TRUE) {
	  $zip->extractTo('data/'.$my_path[0].'/'.$_POST['folder_path_to'].'/');
	  $zip->close();
	  @unlink($upload_path);
	  echo json_encode(array("success" => true,"response" =>'Extracted Successfully!'));
	} else {
	  echo json_encode(array("success" => true,"response" =>'Error on Extraction !'));
	}
	
}
?>