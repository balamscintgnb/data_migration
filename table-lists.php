<?php require 'header.php';?>
<link rel="stylesheet" href="assets/css/dashboard.css" />
</head>
  <body>
  <?php include_once 'header_menu.php';?>

<div class="container-fluid">
      <div class="row">

        <?php include_once 'side_menu.php';?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <?php
$get_table_lists = $db_connect->get_tableinformationlist($sql_details['database']);
$table_lists = json_decode($get_table_lists);

?>
<h4 class=""> Tables <a href="import-table.php"  class="btn btn-sm btn-info float-right" >import</a> </h4>

<table  id="example" class="table table-striped table-bordered" style="width:100%;">
<thead>
<th> Table Name </th> <th> Rows </th> <th> Updated Time </th> <th> Last Sync Time </th> <th> Action </th>
</thead>
<tbody>
<?php foreach ($table_lists->data as $table_list) {?>
<tr> <td><?php echo ucwords(str_replace('_', ' ', $table_list->TABLE_NAME)); ?> </td>
<td><?php echo $table_list->TABLE_ROWS ?></td>
 <td> <?php echo ($table_list->UPDATE_TIME == null) ? '-' : date('d.m.Y h:i:A', strtotime($table_list->UPDATE_TIME)); ?></td>
 <td> - </td>

  <td>
 <a href="table-view.php?table=<?php echo $table_list->TABLE_NAME; ?>" class="btn btn-sm btn-success" > <span data-feather="eye"></span> View</a>
  <button id="push_<?php echo $table_list->TABLE_NAME; ?>" class="btn btn-sm btn-info push-tbl" data-tablename="<?php echo $table_list->TABLE_NAME; ?>"> <span data-feather="refresh-cw"></span> Sync </button>
  <button class="btn btn-sm btn-warning truncate-tbl" data-tablename="<?php echo $table_list->TABLE_NAME; ?>">  <span data-feather="trash"></span> Truncate </button>
  <button class="btn btn-sm btn-danger delete-tbl" data-tablename="<?php echo $table_list->TABLE_NAME; ?>">  <span data-feather="x"></span> Delete </button>
  <?php
  if($table_list->TABLE_NAME=="properties"){
  ?>
  &nbsp;&nbsp;&nbsp;<a class="gen_status" onclick="generate_server_id(0,'<?php echo $table_list->TABLE_NAME; ?>');" >Generate property images</a>&nbsp;&nbsp;&nbsp;
    <!--href="temp/get_properties_server_id.php"-->
  <?php
	}
	?>
	&nbsp;&nbsp;&nbsp;<span id="status_<?php echo $table_list->TABLE_NAME; ?>" class="image_status"></span>
</td>
</tr>

<?php }?>
</tbody>
</table>
</main>


</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace();
      $(document).ready(function(){

          var oTable = $('#example').DataTable({
            "pageLength":25,
            "scrollY": $(window).height()-260,
            "scrollX": true,
          });

           $('#example').on('click','.truncate-tbl',function() {
            var del_confirm = confirm('Are you sure want to truncate the table ?');
            if(del_confirm) {
              $.post({ url:'truncate.php', data:{ table:$(this).data('tablename') },

              success:function(data){
                 window.location.reload();
              }})
            }

          });


           $('#example').on('click','.delete-tbl',function() {
            var del_confirm = confirm('Are you sure want to delete the table ?');
            if(del_confirm) {
              $.post({ url:'delete_table.php', data:{ table:$(this).data('tablename') },

              success:function(){
                window.location.reload();

              }})
            }

          });

		 $('#example').on('click', '.push-tbl', function() {
      var del_confirm = confirm('Are you sure want to Push the table to Server?');
            if(del_confirm) {
                push_records($(this).data('tablename'));
            }
          });

          function push_records(table_name) { 
        	  $('#status_'+table_name).html('Processing...');
        	  $('#push_'+table_name).hide();
        	  $.post({ url:'push_table.php', data:{ table:table_name },
              success:function(response_text){
                //response_text = jQuery.parseJSON(response_text);

                if(response_text.success){
                  console.log(response_text.message +' inserted : '+ response_text.inserted+ ' Failed :'+ response_text.failed);
                  
                  if(response_text.message_code == 100){ 
                     alert('Completed');
                     $('#status_'+table_name).html('Completed.');
                     $('#push_'+table_name).show();
                  }else {//if(response_text.message_code != 100)
                  	//return false;
                     setTimeout(function() {
                    	push_records(table_name);
                     }, 3000);
                  }
                  
                }else{
                  console.log(response_text.message +' '+ response_text.failed);
                  setTimeout(function() {
                    	push_records(table_name);
                     }, 10000);
                }
              }})
            }
            
            
            
      })
      
      
      function generate_server_id(p,table_name) {
		        $('#status_'+table_name).html('Generating...');
		        $('.gen_status').hide();
		    	$.get('get_properties_server_id.php',{p:p},function(data){
					var info = jQuery.parseJSON(data);
					if(info.success) {
						var info = jQuery.parseJSON(data);
						if(info.success) {
							p++;
							if(p<info.total){
								console.log(p);
								generate_server_id(p,table_name);
							}else{
								$('#status_'+table_name).html('Generated.');
								$('.gen_status').show();
								location.reload();
							}
						}else{
							$('#status_'+table_name).html('No properties.');
							$('.gen_status').show();
						}
					}
				
		    	});
		    }
            
      
    </script>
    
      <style>
    	.gen_status{
    		color:#007bff !important;
    		cursor:pointer;
    	}
    </style>


  </body>
</html>
