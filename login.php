<?php include_once 'header.php';?>
<link rel="stylesheet" type="text/css" media="screen" href="assets/css/signin.css" />
</head>
<body class="text-center">
    <form class="form-signin" method="post">
      <img class="mb-4 boardimg" src="<?php echo isset($board_information->agent_logo) ? $board_information->agent_logo:'' ; ?>" alt="logo"  height=""> <!-- vassets/images/gnb_property_logo.png -->
      <h1 class="h3 mb-3 font-weight-normal">Please sign in <?php echo isset($board_information->agent_logo) ? $board_information->agent_name:'' ; ?> </h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" value="<?php echo isset($_COOKIE['member_login']) ?  $_COOKIE['member_login'] : ''; ?>" required="" autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" value="<?php echo isset($_COOKIE['member_password']) ? $_COOKIE['member_password'] : ''; ?>" required/>
      <div class="checkbox mb-3">
        <label>
        <input type="checkbox" value="remember-me"  name="remember" id="remember" checked="<?php echo isset($_COOKIE['member_login'])? 'checked':''?>"> Remember me
        </label>
      </div>
      <input class="btn btn-lg btn-primary btn-block" id="btn-login" type="button" value="Sign in"/>
      <p class="mt-5 mb-3">&copy; <?php echo (date('Y')-1).'-'.date('Y') ?></p>
    </form>
<?php require 'footer.php'; ?>