<?php
require_once 'header_init.php';

extract($_POST);

$utility = new Utility();

$query = "SELECT * from property_files where RECORD_UPLOADED=0 and FILE_UPLOADED = 1 order by id desc limit 0, 100";

$get_table_result = $db_connect->queryFetch($query);

$table_result = json_decode($get_table_result);

$data = array();
$sent_ids = array();

//
$board_information_data = $db_connect->get_boardinformation();
$board_information = json_decode($board_information_data);
$information = $board_information->data;

$db_domain =   explode('_', $db_name);

//todo move this
$dev_name = '';
$subdomain_name = $db_domain[2];

$host = $_SERVER["SERVER_NAME"];

if(isset($board) && $board != '' && $information->server==1){
   $subdomain_name = $board;
}

if($information->server==0){
    $dev_name = '.dev';
}

// $dev_name = '.dev';
 
if ($table_result->data) {
    $i = 0;
    foreach ($table_result->data as $table_lists) {
        $data[$i] = $table_lists;
        $sent_ids[] = $table_lists->id;
        $i++;
    }
}

// $sql = "update property_files set RECORD_UPLOADED=0,FILE_UPLOADED=1 where RECORD_UPLOADED = 2";
// // $sql = "update property_files set RECORD_UPLOADED=0,FILE_UPLOADED=1 where PROPERTY_API_KEY = 'SPH140029'";
// // $sql = "update property_files set RECORD_UPLOADED=0,FILE_UPLOADED=1 where PROPERTY_API_KEY IN ('SPH140029','STO110007')";
// $db_connect->queryExecute($sql);
// 	echo $url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/property_files_mapping_api.php';
	
	exit;
	

if (count($data) > 0) {
	
	$url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/property_files_mapping_api.php';
	
	// exit;
	
	//echo '<pre><code>';var_dump($data, 1);echo '</code></pre>';
    $request = json_encode(array('Token' => 1234, 'Data' => $data), JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES); // token generate on both end
    //echo '<br/><pre><code>';var_dump($request, 3);echo '</code></pre><br/>';

    $header = array('Content-Type: application/json', 'Request-API-Token: GNB-DATA-1234-ASED');

    $response = $utility->getCurlData($url, $request, $header, false);

    $response_json = json_decode($response, true);

    $response_this = array('success' => false, 'message' => 'JSON Error', 'response' => $response, 'message_code'=>101);

    if ($response_json != null) {
        $response_this = $response_json;

        $failed_ids_array = array();

        if (isset($response_json['failed_ids'])) {
            $failed_ids = $response_json['failed_ids'];

            foreach ($failed_ids as $key => $value) {
                $failed_ids_array[] = $key;
            }

            $array_diff = array_diff($sent_ids, $failed_ids_array);

            if (count($array_diff) > 0) {
                $update_ids1 = implode(",", $array_diff);

                $db_connect->record_update_table('property_files', (array) $update_ids1, 1);
            }
            if(count($failed_ids_array) > 0) {
                $update_ids2 = implode(",", $failed_ids_array);

                $db_connect->record_update_table('property_files', (array) $update_ids2, 2);
            }

        }

    }
    ?>
<script type="text/javascript">
function reload(){
	window.location='?1=1';
}
setTimeout(function() {
	reload();
}, 3000);
</script>
<?php
} else {
    $response_this = array('success' => true, 'message' => 'No Records to Push' , 'inserted'=>0, 'failed'=>0, 'message_code'=>100);
}

echo '<br/>';
//header('Content-Type: application/json');
echo json_encode($response_this);
?>