<?php require_once 'header.php'; ?>
</head>
<body>

<link rel="stylesheet" href="assets/css/dashboard.css" />
  <body>
  <?php include_once 'header_menu.php';?>

<div class="container-fluid">
      <div class="row">
        <?php include_once 'side_menu.php';?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        

<div class="">


<h4 class="float-left"> Overall Completion </h4>
<div class="text-right"> 
<span class="p-2 badge badge-dark font-weight-normal">Data Gathering</span>
<span class="p-2 badge badge-warning font-weight-normal">Data Verification</span>
<span class="p-2 badge badge-info font-weight-normal">Data Formatting</span>
<span class="p-2 badge badge-primary font-weight-normal">Data Migration</span>
<span class="p-2 badge badge-secondary font-weight-normal">Migration Verification</span>
<span class="p-2 badge badge-success font-weight-normal">Deployment</span> </div>

<div class="progress mt-3">
<div class="progress-bar bg-dark" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">10%</div>
  <div class="progress-bar bg-warning" role="progressbar" style="width: 5%" aria-valuenow="5" aria-valuemin="5" aria-valuemax="100">5%</div>
  <div class="progress-bar bg-info" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">10%</div>
  <div class="progress-bar bg-yellow" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
  <div class="progress-bar bg-secondary" role="progressbar" style="width: 0%" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">5%</div>
  <div class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">5%</div>

</div>
<div>
<p class="text-left float-left"> Start Date : 10.07.2018 </p>
<p class="text-right float-right"> Estimated Date : <?php echo date('d.m.Y',strtotime($board_information->data->import_completion_estimated)) ?> </p>
</div>

</div>
<?php 
$get_table_lists = $db_connect->get_tableinformationlist($sql_details['database']);
$table_lists = json_decode($get_table_lists);
?>
<div class="clearfix"></div>
<div class="row ml-0 mb-3">

<div class="card text-white p-0 bg-secondary col-2 mr-2">
  <div class="card-body">
  <h5 class="card-title">Total Tables</h5>
    <h2 class="card-title text-right display-4"> <?php echo count($table_lists->data); ?> </h2>
  </div>
</div>

<div class="card text-white bg-warning p-0 col-2">
  <div class="card-body">
    <h5 class="card-title">Pending Tables</h5>
      <h2 class="card-title text-right display-4"> <?php echo count($table_lists->data); ?> </h2>
    </div>
  </div>

</div>

<div class="clearfix"></div>

<h4 class="">Tables</h4>

<div class="list_table_holder mt-2">
<ul class="list-group custom-table-list-group row">

<?php  foreach($table_lists->data as $table_list) {   ?> 

<li class="col-3 list-group-item list-group-item-action flex-column align-items-start">
  <div class="d-flex w-100 justify-content-between align-items-center">
      <h5 class="mb-1"><a href="table-view.php?table=<?php echo $table_list->TABLE_NAME; ?>"><?php  echo ucwords(str_replace('_', ' ', $table_list->TABLE_NAME));  ?></a>  </h5> 
      <span class="badge badge-primary badge-pill"><?php echo $table_list->TABLE_ROWS ?></span>
    </div>
    <small> Modified at: <?php echo ($table_list->UPDATE_TIME != null)?date('d.m.Y h:i:A', strtotime($table_list->UPDATE_TIME)):'-'; ?></small>
</li>

<?php } ?>
</ul>
</div>
</main>


</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
<?php require_once 'footer.php';?>