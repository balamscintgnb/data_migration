<?php
////error_reporting(0); 
////ini_set('display_errors',1);
require_once 'classes/gnbcore.class.php';
require_once 'classes/utility.class.php';

extract($_POST);


if($subdomain_cnb==''){
	echo json_encode(array("success"=>false,"response"=>"Invalid domain"));
	exit();
}
$server = $_SERVER["SERVER_NAME"];
$db_name=Utility::get_subdomain($server);

require_once 'includes/config.php';

$db_connect = new GNBCore($sql_details, true);
$utility = new Utility();

if($property_url!='' && $property_category>0){
	
	$url = "http://lalithkaushik.com/estate_cnb_properties/get_properties.php";
	
	if(!isset($property_count) || trim($property_count)=='' || (((int)$property_count)<=0) ){
	    $property_count=5;
	}
	
	
	mkdir('data/'.$subdomain_cnb.'/',0777);
	
	mkdir('data/'.$subdomain_cnb.'/images/',0777);

	
	if(file_exists('data/'.$subdomain_cnb.'/properties.json')){
		@unlink('data/'.$subdomain_cnb.'/properties.json');
	}
	
	
	$postParams = "domain=".$subdomain_cnb."&property_url=".urlencode($property_url)."&count=$property_count&property_category=$property_category";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);  
	$property_html = curl_exec($ch);
	$sample_properties_list = json_decode($property_html);
	
	if($sample_properties_list->success){
		$sample_properties_list = $sample_properties_list->response;
	
		file_put_contents('data/'.$subdomain_cnb.'/properties.json',json_encode($sample_properties_list));
		

	
		//echo json_encode(array("success"=>true,"response"=>'<span style="color:#228B22">Added</span>',"data"=>$sample_properties_list));
	}else{
		echo json_encode(array("success"=>false,"response"=>$sample_properties_list->response));
		exit;
	}
	/*$json = '{
	"success": true,
	"response": "<span style=\"color:#228B22\">Added<\/span>",
	"data": [{
		"notice_title": "3 bed terraced house for sale",
		"notice_content": "<div class=\"dp-description__text\">\n Set in the family-friendly and highly popular Methleys Home Zone - requiring considerable renovation and improvement, a three bedroom back to back mid-terrace property on four storeys. UPVC double glazing. EPC Rating- F<br>Accommodation includes living room, kitchen, first floor bedroom, bathroom, two second floor bedrooms. The property is requiring considerable internal and external improvements including attention needed to the front walls of the upper floors of the property.<br><br><strong>Ground Floor<\/strong><br><br>Steps up to uPVC double glazed door to:<br><br><strong>Lounge (4.2m x 3.9m (13\'9\" x 12\'9\"))<\/strong><br><br>Door to steps leading to first floor<br><br><strong>Kitchen (3.3m x 1.6 (10\'9\" x 5\'2\"))<\/strong><br><br>Door to basement<br><br><strong>First Floor<\/strong><br><br><strong>Landing<\/strong><br><br>Stairs leading to second floor<br><br><strong>Bedroom 1 (4.2m x 3.4m (13\'9\" x 11\'1\" ))<\/strong><br><br><strong>Bathroom (2.4m 2.1m (7\'10\" 6\'10\"))<\/strong><br><br><strong>Second Floor<\/strong><br><br><strong>Bedroom 2 (4.2m x 3.4m (13\'9\" x 11\'1\"))<\/strong><br><br><strong>Bedroom 3 (2.4m x 2.1m (7\'10\" x 6\'10\" ))<\/strong><br><br>Historically used as a kitchen<br><br><strong>Basement<\/strong><br><br>Multi-roomed versatile basement with access to front courtyard<br><br><strong>Outside<\/strong><br><br>Low wall, courtyard<br><br><strong>Methleys Home Zone<\/strong><br><br>The property is located within a street and road scheme constructed in 2001, to provide improvements to pedestrian safety and traffic calming measures.<br><br><strong>Agents Note<\/strong><br><br>The property is in a condition of disrepair and requires attention to its structure before it is suitable for residence.<br><br><strong>Tenure<\/strong><br><br>Freehold<br><br><strong>How To Get There<\/strong><br><br>From Harrogate Road, turn onto Methleys Drive then left onto Methley Lane.<br><br><strong>Viewings<\/strong><br><br>Please ring us to make an appointment. We are open from 9am to 5.30pm Monday to Friday, 9am to 4pm on Saturdays and 11am to 2pm on Sundays.<br><br><strong>General<\/strong><br><br>Every care has been taken with the preparation of these sales particulars, but complete accuracy cannot be guaranteed. In all cases, buyers should verify matters for themselves. Where property alterations have been undertaken buyers should check that relevant permissions have been obtained. If there is any point which is of particular importance then let us know and we will verify it for you. These particulars do not constitute a contract or part of a contract.<br><br><strong>Fixtures &amp; Fittings<\/strong><br><br>The fixtures, fittings and appliances have not been tested and therefore no guarantee can be given that they are in working order.<br><br><strong>Internal Photographs<\/strong><br><br>Photographs are reproduced for general information and it cannot be inferred that any item shown is included in the sale.<br><br><strong>Measurements<\/strong><br><br>All measurements quoted are approximate.<br><br><strong>Alan Cooke Estate Agents Ltd<\/strong><br><br>Incorporated in England 6539351\n                                    <\/div>",
		"notice_price": "145000",
		"display_price": "\u00a3145,000",
		"notice_location": "Methley Lane, Chapel Allerton, Leeds LS7",
		"notice_frequency": "",
		"notice_type": 43,
		"notice_images": ["cnb_domains\/deatha\/eaeb6a\/0.jpg", "cnb_domains\/deatha\/eaeb6a\/1.jpg", "cnb_domains\/deatha\/eaeb6a\/2.jpg", "cnb_domains\/deatha\/eaeb6a\/3.jpg"],
		"notice_images_360": "",
		"notice_youtube_links": "",
		"bedroom": "3",
		"bathroom": "1",
		"reception": "1",
		"notice_link": "http:\/\/www.zoopla.co.uk\/for-sale\/details\/49547976",
		"notice_id": "eaeb6a",
		"notice_admin_fee": "",
		"notice_available_date": "",
		"notice_post_code": "",
		"let_agreed": false,
		"notice_source": "zoopla"
	}, {
		"notice_title": "2 bed flat for sale",
		"notice_content": "<div class=\"dp-description__text\">\n                                        *** own A brand new 2 bed, 2 bath apartment with parking from \u00c2\u00a3181,500***<br>Northgate House is a stunning brand new development comprising of 54 luxury 1 and 2 bedroom apartments. With high quality interiors and automatic lift to all floors plus secure underground car parking.<br><br>Located in the heart of trendy Meanwood with direct travel routes into Leeds city centre, Leeds Universities and is situated right next to bustling restaurants, bars, shops and Waitrose &amp; Aldi supermarkets.<br><br>Northgate House will set the standard for delivering quality residential lifestyles to satisfy the very real housing demand from young professionals, international students and families looking for the very best for living, working and socialising in Leeds.<br><br>A complete range of furniture options is available so the apartment can be furnished and ready to move in on completion.<br><br>54 brand new luxury apartments, housed in a stylish building, with enviable access to local amenities, supermarkets, hip bars and restaurants. Each apartment comes fitted with a complete range of fitted kitchen and bathroom appliances and has a secured, undercroft parking bay with additional access to cycle storage, thoughtfully catering for the modern urban resident. 1 bedroom apartments from \u00c2\u00a3151,000 and 2 bedrooms from \u00c2\u00a3181,500<br><br><strong>Specifications<\/strong><br><br>All apartments benefit from double glazed windows with opening doors to balconies where applicable, video entry system, CCTV to communal internal and external areas, electronic controlled access to exterior doors. Ceiling mounted smoke detectors and heat detectors<br><br>High quality internal doors to habitable rooms with modern door furniture and timber painted front door.<br><br>Soft white painted plasterboard ceilings and walls, wood floors, or similar, to kitchen, living room and hallway, tiling to bathrooms and en suite.<br><br><strong>Typical Interior<\/strong><br><br><strong>Kitchen<\/strong><br><br>Individually designed contemporary kitchens in a range of gloss or matte finishes with wall units with integrated lighting below. Integrated appliances include single oven and microwave, dishwasher, fridge\/freezer, washer\/dryer<br><br><strong>Typical Interior<\/strong><br><br><strong>Bathroom<\/strong><br><br>Fittings will include white ceramic basin, chrome basin mixer taps, white ceramic WC with soft closing seat and dual flush, white acrylic bath and wall mounted hand held shower in main bathrooms, shower in master en suite, glass shower screens, good quality tiling to walls in bathroom and en suite, chrome heated towel rail, mirrored vanity cabinet with shaver socket and integrated feature lighting<br><br><strong>Typical Interior<\/strong><br><br><strong>Typical Interior<\/strong><br><br><strong>Typical Interior<\/strong><br><br><strong>Typical Interior<\/strong><br><br><strong>Furniture Packages<\/strong><br><br>Furniture packages are available so the apartments will be fully furnished on completion to a contemporary design. There is a choice of 3 different designs starting from \u00c2\u00a32350 + VAT for a 1 bedroom and \u00c2\u00a33500 + VAT for a 2 bedroom.<br><br><strong>Tenure<\/strong><br><br>Leasehold - 250 year lease<br>Contact our office for details about ground rent and service charges<br><br><strong>Location<\/strong><br><br>Set in the heart of ever changing Meanwood the properties are a short stroll from numerous local eateries and bars and with nearby Headingley just a few minutes drive away the opportunity for socialising is abundant.<br><br>The Meanwood Valley Local Nature Reserve Trail offers you the opportunity to experience and enjoy wildlife in often seemingly-remote surroundings. An excellent way to explore the valley is to follow the Meanwood Valley trail which runs from Woodhouse Moor, through the Meanwood Valley and on to Breary Marsh at Golden Acre.<br><br>The Universities of Leeds are close-by so the area is perfect for international students and post graduates looking to live in the finest accommodation.<br><br>Leeds city centre is easily accessible as Northgate House is located on one of the main arterial routes into the centre with connections to the major motorway links via the Leeds Inner Ring Road. Leeds city centre offers a wealth of stylish shopping and decadent dining, to contemporary arts and vibrant nightlight.<br><br><strong>General<\/strong><br><br>Every care has been taken with the preparation of these sales particulars, but complete accuracy cannot be guaranteed. In all cases, buyers should verify matters for themselves. If there is any point which is of particular importance then let us know and we will verify it for you. These particulars do not constitute a contract or part of a contract.<br><br><strong>Typical Interior Photographs<\/strong><br><br>Photographs produced on these particulars are artists impressions only and are for general guidance and may differ from the final presentation.<br><br><strong>Alan Cooke Estate Agents Ltd<\/strong><br><br>Incorporated in England 6539351\n                                    <\/div>",
		"notice_price": "181500",
		"display_price": "\u00a3181,500\n\n\n\n\n\n\n\n                From",
		"notice_location": "Northgate House, Stonegate Road, Meanwood LS6",
		"notice_frequency": "",
		"notice_type": 43,
		"notice_images": ["cnb_domains\/deatha\/0a968e\/0.jpg", "cnb_domains\/deatha\/0a968e\/1.jpg", "cnb_domains\/deatha\/0a968e\/2.jpg", "cnb_domains\/deatha\/0a968e\/3.jpg", "cnb_domains\/deatha\/0a968e\/4.jpg", "cnb_domains\/deatha\/0a968e\/5.jpg", "cnb_domains\/deatha\/0a968e\/6.jpg"],
		"notice_images_360": "",
		"notice_youtube_links": "",
		"bedroom": "2",
		"bathroom": "2",
		"reception": "1",
		"notice_link": "http:\/\/www.zoopla.co.uk\/for-sale\/details\/49533152",
		"notice_id": "0a968e",
		"notice_admin_fee": "",
		"notice_available_date": "",
		"notice_post_code": "",
		"let_agreed": false,
		"notice_source": "zoopla"
	}, {
		"notice_title": "2 bed detached bungalow for sale",
		"notice_content": "<div class=\"dp-description__text\">\n Luxury single floor living with exciting potential to add further accommodation (subject to necessary planning approvals), in an enviable location. A spacious two bedroom detached dormer bungalow set in good size gardens situated in one of North Leeds most desired residential locations just off Wigton Lane.<br><br>The property has been extended providing superb living accommodation. Excellent local amenities and shopping facilities are available at near-by Slaid Hill. Furthermore you are within walking distance of the countryside and high-calibre golf courses. Modern gas combi central heating system. EPC Rating- D<br><br>Accommodation includes entrance porch, lounge, dining room, fitted breakfast kitchen, rear porch with access door leading to integral garage, lovely garden room, master bedroom with deluxe en-suite shower room and built in wardrobes, second double bedroom with built in wardrobes. Spacious loft room that is currently used as a bedroom with en-suite shower room, plus additional spacious loft storage offering much further potential for development.<br><br><strong>Ground Floor<\/strong><br><br>UPVC double glazed door with double glazed side panels to:<br><br><strong>Entrance Porch<\/strong><br><br>Central heating radiator, spacious cloak cupboard, glazed door and side panel into:<br><br><strong>Fine Lounge (6.4m x 3.7m (20\'11\" x 12\'1\"))<\/strong><br><br>Feature stone fireplace with gas \'living-flame\' fire, two central heating radiators, uPVC double glazed window to front, coving, inset ornamental alcove with cupboard<br><br><strong>Fine Lounge<\/strong><br><br>Glazed bi-folding doors into:<br><br><strong>Open Dining Room (3.5m x 3.7m + 2.9m x 1.9m (11\'5\" x 12\'1\" + 9\'6\" x)<\/strong><br><br>Central heating radiator, uPVC double glazed window overlooking garden, inset ceiling lighting, coving, uPVC double glazed doors leading into garden room, glazed double doors into inner hallway, glazed door into kitchen<br><br><strong>Open Dining Room<\/strong><br><br><strong>Open Dining Room<\/strong><br><br><strong>Fitted Breakfast Kitchen (5.1m x 3.5m (16\'8\" x 11\'5\" ))<\/strong><br><br>With superb and generous range of timber effect fitted units and glass fronted cabinets with corresponding worktops, breakfast bar, stainless steel 1.5 bowl sink with mixer tap and drainer, integrated dishwasher, integrated freezer, ceramic tiled walls, inset ceiling lighting, ceramic tiled floor, built in oven, built in microwave, four ring gas hob, store cupboard housing newly installed gas-fired \u00e2\u0080\u0098combi\u00e2\u0080\u0099 central and water heating boiler, double glazed window overlooking garden room, glazed door to:<br><br><strong>Rear Porch<\/strong><br><br>Ceramic tiled floor, glazed door leading out to covered walkway by side of property, door into:<br><br><strong>Integral Garage<\/strong><br><br>Plumbed for washing machine, secure roller door<br><br><strong>Garden Room (6m x 3.6m (19\'8\" x 11\'9\"))<\/strong><br><br>UPVC double glazed windows to three sides and double doors leading out to garden, ceramic tiled floor, central heating radiator<br><br><strong>Inner Hallway<\/strong><br><br>Door to staircase leading up to first floor, central heating radiator, coving<br><br><strong>Master Bedroom Suite<\/strong><br><br>Comprising of:<br><br><strong>Bedroom 1 (4.3m x 3.3m (14\'1\" x 10\'9\" ))<\/strong><br><br>Two built in wardrobes with sliding mirrored doors, coving, central heating radiator, uPVC double glazed window to front<br><br><strong>Ensuite Shower Room<\/strong><br><br>Deluxe shower room - fully ceramic tiled walls and floor, walk-in shower cubicle, low WC, vanity washbasin set within cabinets, uPVC double glazed window, wall mirror<br><br><strong>Bedroom 2 (4.3m x 3.1m (14\'1\" x 10\'2\" ))<\/strong><br><br>Built in wardrobe with sliding mirrored doors, central heating radiator, uPVC double glazed window overlooking garden, coving<br><br><strong>House Bathroom<\/strong><br><br>White suite of curved panelled bath with wall shower, low WC, vanity washbasin with cabinets, wall mirror, ceramic tiled walls and floor, heated towel rail, inset ceiling lighting<br><br><strong>First Floor<\/strong><br><br><strong>Loft Room (5.6m x 4.5m + 3.0m x 2.6m (18\'4\" x 14\'9\" + 9\'10\")<\/strong><br><br>This spacious room offers huge potential. Currently used as a bedroom, uPVC double glazed window, electric convection heating.<br><br><strong>Ensuite Shower Room<\/strong><br><br>Walk-in shower cubicle, low WC, pedestal washbasin<br><br><strong>Spacious Loft Storage<\/strong><br><br>With great potential - a vastly spacious area currently used as storage.<br><br><strong>Outside<\/strong><br><br>Double width drive to integral garage, lawned and stocked garden to front, gate to covered walkway leading to rear. Lawned and stocked west facing mature rear garden with patio and garden shed.<br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Tenure<\/strong><br><br>Freehold<br><br><strong>How To Get There<\/strong><br><br>Approached from Harrogate Road, turn right at Alwoodley Gates into Wigton Lane and after about a mile turn right into Plantation Gardens and No 10 is situated on the right.<br><br><strong>Viewings<\/strong><br><br>Please ring us to make an appointment. We are open from 9am to 5.30pm Monday to Friday, 9am to 4pm on Saturdays and 11am to 2pm on Sundays.<br><br><strong>General<\/strong><br><br>Every care has been taken with the preparation of these sales particulars, but complete accuracy cannot be guaranteed. In all cases, buyers should verify matters for themselves. Where property alterations have been undertaken buyers should check that relevant permissions have been obtained. If there is any point which is of particular importance then let us know and we will verify it for you. These particulars do not constitute a contract or part of a contract.<br><br><strong>Fixtures &amp; Fittings<\/strong><br><br>The fixtures, fittings and appliances have not been tested and therefore no guarantee can be given that they are in working order.<br><br><strong>Internal Photographs<\/strong><br><br>Photographs are reproduced for general information and it cannot be inferred that any item shown is included in the sale.<br><br><strong>Measurements<\/strong><br><br>All measurements quoted are approximate.<br><br><strong>Floorplan<\/strong><br><br>The floorplan is provided for general guidance and is not to scale.<br><br><strong>Alan Cooke Estate Agents Ltd<\/strong><br><br>Incorporated in England 6539351\n                                    <\/div>",
		"notice_price": "465000",
		"display_price": "\u00a3465,000",
		"notice_location": "Plantation Gardens, Off Wigton Lane, Alwoodley LS17",
		"notice_frequency": "",
		"notice_type": 43,
		"notice_images": ["cnb_domains\/deatha\/eae061\/0.jpg", "cnb_domains\/deatha\/eae061\/1.jpg", "cnb_domains\/deatha\/eae061\/2.jpg", "cnb_domains\/deatha\/eae061\/3.jpg", "cnb_domains\/deatha\/eae061\/4.jpg", "cnb_domains\/deatha\/eae061\/5.jpg", "cnb_domains\/deatha\/eae061\/6.jpg", "cnb_domains\/deatha\/eae061\/7.jpg", "cnb_domains\/deatha\/eae061\/8.jpg", "cnb_domains\/deatha\/eae061\/9.jpg", "cnb_domains\/deatha\/eae061\/10.jpg", "cnb_domains\/deatha\/eae061\/11.jpg", "cnb_domains\/deatha\/eae061\/12.jpg", "cnb_domains\/deatha\/eae061\/13.jpg", "cnb_domains\/deatha\/eae061\/14.jpg", "cnb_domains\/deatha\/eae061\/15.jpg", "cnb_domains\/deatha\/eae061\/16.jpg", "cnb_domains\/deatha\/eae061\/17.jpg", "cnb_domains\/deatha\/eae061\/18.jpg"],
		"notice_images_360": "",
		"notice_youtube_links": "",
		"bedroom": "2",
		"bathroom": "3",
		"reception": "3",
		"notice_link": "http:\/\/www.zoopla.co.uk\/for-sale\/details\/49520000",
		"notice_id": "eae061",
		"notice_admin_fee": "",
		"notice_available_date": "",
		"notice_post_code": "",
		"let_agreed": false,
		"notice_source": "zoopla"
	}, {
		"notice_title": "2 bed flat for sale",
		"notice_content": "<div class=\"dp-description__text\">\n Great opportunity for first time buyers: A delightful two bedroom first floor apartment in a small block. Most conveniently located and offering quite, spacious accommodation. Private entrance, gas central heating system, uPVC double glazing, 22\' living room, recently installed fitted kitchen, built in wardrobes, white shower room, loft storage. Maintained grounds and garage. EPC Rating- D<br><br><strong>Private Entrance<\/strong><br><br>UPVC double glazed door to staircase to the:<br><br><strong>First Floor Landing<\/strong><br><br>UPVC double glazed window, coving<br><br><strong>Spacious Living Room (6.78 x 3.91 max (22\'2\" x 12\'9\" max))<\/strong><br><br>UPVC double glazed window, two central heating radiators, coving, feature marble fire surround and electric \'living flame\' fire<br><br><strong>Spacious Living Room<\/strong><br><br><strong>Fitted Kitchen (4.45 x 1.93 (14\'7\" x 6\'3\"))<\/strong><br><br>Recently installed range of units and work tops, stainless steel sink with mixer tap and drainer, completely wall tiled, plumbed for washing machine, cupboard housing gas fired Worcester combi central and water heating boiler, UPVC double glazed window, built in oven<br><br><strong>Central Hall<\/strong><br><br>Central heating radiator, pull down ladder to part boarded loft with light and power<br><br><strong>Bedroom 1 (3.63 x 3.23 (11\'10\" x 10\'7\"))<\/strong><br><br>Built in wardrobes with mirror doors, central heating radiator, UPVC double glazed window<br><br><strong>Bedroom 2 (3.05 x 2.36 (10\'0\" x 7\'8\"))<\/strong><br><br>Central heating radiator, UPVC double glazed window<br><br><strong>Shower Room<\/strong><br><br>White suite, walk in shower cubicle, pedestal washbasin, low WC, central heating radiator, airing cupboard, completely wall tiled<br><br><strong>Outside<\/strong><br><br>Maintained grounds, garage &amp; shared parking<br><br><strong>Tenure<\/strong><br><br>Leasehold from November 2015 (approx 173 years remaining) Service charge currently \u00c2\u00a3660 per annum which includes \u00c2\u00a360 ground rent<br><br><strong>How To Get There<\/strong><br><br>From Harrogate Road turn into Stonegate Road, cross over King Lane and then turn left into Garth Drive<br><br><strong>Viewings<\/strong><br><br>Please ring us to make an appointment. We are open from 9am to 5.30pm Monday to Friday, 9am to 4pm on Saturdays and 11am to 2pm on Sundays.<br><br><strong>General<\/strong><br><br>Every care has been taken with the preparation of these sales particulars, but complete accuracy cannot be guaranteed. In all cases, buyers should verify matters for themselves. Where property alterations have been undertaken buyers should check that relevant permissions have been obtained. If there is any point which is of particular importance then let us know and we will verify it for you. These particulars do not constitute a contract or part of a contract.<br><br><strong>Fixtures &amp; Fittings<\/strong><br><br>The fixtures, fittings and appliances have not been tested and therefore no guarantee can be given that they are in working order.<br><br><strong>Internal Photographs<\/strong><br><br>Photographs are reproduced for general information and it cannot be inferred that any item shown is included in the sale.<br><br><strong>Measurements<\/strong><br><br>All measurements quoted are approximate.<br><br><strong>Floorplan<\/strong><br><br>The floorplan is provided for general guidance and is not to scale.<br><br><strong>Alan Cooke Estate Agents Ltd<\/strong><br><br>Incorporated in England 6539351\n                                    <\/div>",
		"notice_price": "145000",
		"display_price": "\u00a3145,000",
		"notice_location": "Garth Drive, Moortown, Leeds LS17",
		"notice_frequency": "",
		"notice_type": 43,
		"notice_images": ["cnb_domains\/deatha\/fcc656\/0.jpg", "cnb_domains\/deatha\/fcc656\/1.jpg", "cnb_domains\/deatha\/fcc656\/2.jpg", "cnb_domains\/deatha\/fcc656\/3.jpg", "cnb_domains\/deatha\/fcc656\/4.jpg", "cnb_domains\/deatha\/fcc656\/5.jpg", "cnb_domains\/deatha\/fcc656\/6.jpg"],
		"notice_images_360": "",
		"notice_youtube_links": "",
		"bedroom": "2",
		"bathroom": "1",
		"reception": "1",
		"notice_link": "http:\/\/www.zoopla.co.uk\/for-sale\/details\/49515749",
		"notice_id": "fcc656",
		"notice_admin_fee": "",
		"notice_available_date": "",
		"notice_post_code": "",
		"let_agreed": false,
		"notice_source": "zoopla"
	}, {
		"notice_title": "2 bed detached bungalow for sale",
		"notice_content": "<div class=\"dp-description__text\">\n Proudly located on one of North Leeds most prestigious addresses, with views over Sand Moor Golf Course - a rare opportunity to purchase this fine location single floor accommodation with proud elevated aspect and south facing outlook. A two bedroom detached bungalow - fully uPVC double glazed and gas central heating system. EPC Rating- E.<br><br>Accommodation includes reception porch, entrance hall, spacious lounge, dining room, breakfast kitchen, rear porch, two double bedrooms with built in wardrobes, white bathroom suite. Lawned and stocked gardens with rear courtyard for parking and double garage with remote controlled access.<br><br>The location of this bungalow is truly exceptional and is ideally suited for mature families wishing for prime real estate bungalows. Pricing is available on application.<br><br><strong>Ground Floor<\/strong><br><br>UPVC double glazed door with side panels leading into:<br><br><strong>Entrance Porch<\/strong><br><br>Ceramic tiled floor, uPVC double glazed secure door leading to:<br><br><strong>Hallway<\/strong><br><br>Cloak cupboard, double width linen\/airing cupboard with hot water immersion tank, pull down ladder to part boarded loft<br><br><strong>Guest Wc<\/strong><br><br>Pink low WC, washbasin<br><br><strong>Lounge (5.5m x 3.9m (18\'0\" x 12\'9\" ))<\/strong><br><br>UPVC double glazed south facing window to front, double central heating radiator, dado rail, feature fireplace with gas \'living-flame\' fire, glazed double doors leading to:<br><br><strong>Dining Room (3.9m x 3.4 m (12\'9\" x 11\'1\" m ))<\/strong><br><br>UPVC double glazed window to rear, double central heating radiator, coving, inset ceiling lighting<br><br><strong>Breakfast Kitchen (4.8m x 4.2m max (15\'8\" x 13\'9\" max))<\/strong><br><br>Range of fitted units with corresponding worktops, plumbed for washing machine and dishwasher, stainless steel sink with mixer tap and drainer, waste disposal unit, built in double oven, four ring gas hob, gas central heating boiler, integrated microwave, breakfast bar, pantry store, part ceramic tiled walls, double central heating radiator, uPVC double glazed window &amp; timber double glazed door leading to rear porch<br><br><strong>Breakfast Kitchen<\/strong><br><br><strong>Rear Porch<\/strong><br><br>UPVC double glazed window to three sides, uPVC double glazed door leading out to rear courtyard<br><br><strong>Bedroom 1 (4.5m x 3.3m (14\'9\" x 10\'9\"))<\/strong><br><br>Built in furniture including wardrobes, dresser, drawers and bedside tables, uPVC double glazed window to front, central heating radiator, coving<br><br><strong>Bedroom 2 (3.8m x 3.2m (12\'5\" x 10\'5\"))<\/strong><br><br>Built in furniture including wardrobes, dresser and drawers, central heating radiator, uPVC double glazed window<br><br><strong>Bathroom (2.4m x 2.2m (7\'10\" x 7\'2\"))<\/strong><br><br>White suite of panel bath, walk-in shower cubicle with power shower, low WC, pedestal washbasin, ceramic tiled walls, ceramic tiled floor, central heating radiator, uPVC double glazed window, inset ceiling lighting<br><br><strong>Outside<\/strong><br><br>Paved driveway leading to rear, pleasant lawned and stocked south facing front garden. Paved courtyard and small stocked garden to rear with views overlooking Sand Moor Golf Course. Detached double width garage with remote controlled door<br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Outside<\/strong><br><br><strong>Rear View<\/strong><br><br><strong>Agents Note<\/strong><br><br>Current ownership of the property is held by an estate which is currently awaiting grant of probate. Whilst a sale on the property may be provisionally agreed, exchange of contracts can only be attained once probate has been granted.<br><br><strong>Tenure<\/strong><br><br>Freehold<br><br><strong>How To Get There<\/strong><br><br>Alwoodley Lane is accessed directly from Harrogate Road or from King Lane<br><br><strong>Viewings<\/strong><br><br>Please ring us to make an appointment. We are open from 9am to 5.30pm Monday to Friday, 9am to 4pm on Saturdays and 11am to 2pm on Sundays.<br><br><strong>General<\/strong><br><br>Every care has been taken with the preparation of these sales particulars, but complete accuracy cannot be guaranteed. In all cases, buyers should verify matters for themselves. Where property alterations have been undertaken buyers should check that relevant permissions have been obtained. If there is any point which is of particular importance then let us know and we will verify it for you. These particulars do not constitute a contract or part of a contract.<br><br><strong>Fixtures &amp; Fittings<\/strong><br><br>The fixtures, fittings and appliances have not been tested and therefore no guarantee can be given that they are in working order.<br><br><strong>Internal Photographs<\/strong><br><br>Photographs are reproduced for general information and it cannot be inferred that any item shown is included in the sale.<br><br><strong>Measurements<\/strong><br><br>All measurements quoted are approximate.<br><br><strong>Floorplan<\/strong><br><br>The floorplan is provided for general guidance and is not to scale.<br><br><strong>Alan Cooke Estate Agents Ltd<\/strong><br><br>Incorporated in England 6539351\n                                    <\/div>",
		"notice_price": "",
		"display_price": "POA",
		"notice_location": "Alwoodley Lane, Alwoodley, Leeds LS17",
		"notice_frequency": "",
		"notice_type": 43,
		"notice_images": ["cnb_domains\/deatha\/140afc\/0.jpg", "cnb_domains\/deatha\/140afc\/1.jpg", "cnb_domains\/deatha\/140afc\/2.jpg", "cnb_domains\/deatha\/140afc\/3.jpg", "cnb_domains\/deatha\/140afc\/4.jpg", "cnb_domains\/deatha\/140afc\/5.jpg", "cnb_domains\/deatha\/140afc\/6.jpg", "cnb_domains\/deatha\/140afc\/7.jpg", "cnb_domains\/deatha\/140afc\/8.jpg", "cnb_domains\/deatha\/140afc\/9.jpg", "cnb_domains\/deatha\/140afc\/10.jpg", "cnb_domains\/deatha\/140afc\/11.jpg", "cnb_domains\/deatha\/140afc\/12.jpg", "cnb_domains\/deatha\/140afc\/13.jpg", "cnb_domains\/deatha\/140afc\/14.jpg", "cnb_domains\/deatha\/140afc\/15.jpg"],
		"notice_images_360": "",
		"notice_youtube_links": "",
		"bedroom": "2",
		"bathroom": "2",
		"reception": "2",
		"notice_link": "http:\/\/www.zoopla.co.uk\/for-sale\/details\/49472791",
		"notice_id": "140afc",
		"notice_admin_fee": "",
		"notice_available_date": "",
		"notice_post_code": "",
		"let_agreed": false,
		"notice_source": "zoopla"
	}]
	}';*/

	
	
	
	$properties = $sample_properties_list;
	$count = 0;
	foreach($properties as $row){
		$count++;
		$PROPERTY_ID				= $row->notice_id;
		$PROPERTY_REF_ID			= $row->notice_id;
		$PROPERTY_TITLE				= $row->notice_title;
		$PROPERTY_DESCRIPTION		= addslashes(Utility::html_escape($row->notice_content));
		$category					= $row->notice_type;
		
		$PROPERTY_PRICE				= $row->notice_price;
		$PROPERTY_PRICE_FREQUENCY	= $row->notice_frequency;	
		$PROPERTY_QUALIFIER			= '';
		$PROPERTY_AVAILABLE_DATE	= $row->notice_available_date;	
		$notice_link = $row->notice_link;	
		
		if($PROPERTY_AVAILABLE_DATE==''){
			$PROPERTY_AVAILABLE_DATE = date('Y-m-d H:i:s');
		}
		  
		$PROPERTY_AVAILABILITY		= 'AVAILABLE';
		$PROPERTY_ADMIN_FEES		= $row->notice_admin_fee;	
		$PROPERTY_BEDROOMS			= $row->bedroom;	
		$PROPERTY_BATHROOMS			= $row->bathroom;
		$PROPERTY_RECEPTION			= $row->reception;
		$PROPERTY_ASSETS			= '';
		$PROPERTY_STATUS			= 'ACTIVE';
		$notice_images				= $row->notice_images;
		$notice_images_array		= array();
		if(sizeof($notice_images)>0){
			foreach($notice_images as $img_row){
				$image_info = pathinfo($img_row);
				$notice_images_array[] = array("type"=>"photo","path"=>'images/'.$PROPERTY_REF_ID.'/'.$image_info['basename']);
			}
		}
		
		if(sizeof($notice_images_array)>0){
			$PROPERTY_ASSETS		= json_encode($notice_images_array);
		}
		
		$PROPERTY_FORMATTED_ADDRESS = addslashes(addslashes($row->notice_location));
		$PROPERTY_ADDRESS_LINE_1	= $PROPERTY_ADDRESS_LINE_2	=  $PROPERTY_ADDRESS_CITY	=	'';
		$PROPERTY_ADDRESS_COUNTY	= $PROPERTY_ADDRESS_POSTCODE	= '';
		$street						= $locality = $area = $town = $county = $postcode = '';
		
		$notice_location_array		= explode(',',$row->notice_location);
		$street						= trim($notice_location_array[0]);
		$locality					= trim($notice_location_array[1]);
		$area						= trim($notice_location_array[2]);
		$town						= trim($notice_location_array[3]);
		$county						= trim($notice_location_array[4]);
		$postcode					= trim($notice_location_array[5]);
		
		if($street!=''){
			$PROPERTY_ADDRESS_LINE_1=addslashes($street);
		}
	
		if($locality!=''){
			$PROPERTY_ADDRESS_CITY.=$locality;
		}
	
		if($area!=''){
			if($PROPERTY_ADDRESS_CITY!=""){
				$PROPERTY_ADDRESS_CITY.=', ';
			}
			$PROPERTY_ADDRESS_CITY.=$area;
		}
	
		if($town!=''){
			$PROPERTY_ADDRESS_COUNTY.=$town;
		}
	
		if($county!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$county;
		}
	
		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}

		
		$PROPERTY_CATEGORY			= '';
		if($category==43){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
		}
		else if($category==44){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
		}
		else if($category==45){
			$PROPERTY_CATEGORY		= 'COMMERCIAL SALES';
		}
		else if($category==46){
			$PROPERTY_CATEGORY		= 'COMMERCIAL LETTINGS';
		}
		
		$notice_source='';
		if(stristr($notice_link,'zoopla')){
			$notice_source='zoopla';
		}else if(stristr($notice_link,'rightmove')){
			$notice_source='rightmove';
		}if(stristr($notice_link,'onthemarket')){
			$notice_source='onthemarket';
		}
		
		
		$sql_create ="CREATE TABLE IF NOT EXISTS `properties` (
		  `id` int(11) NOT NULL,
		  `BRANCH_ID` varchar(45) DEFAULT '0',
		  `PROPERTY_ID` varchar(255) DEFAULT NULL,
		  `PROPERTY_REF_ID` varchar(255) DEFAULT NULL,
		  `PROPERTY_VENDOR_ID` varchar(255) DEFAULT NULL,
		  `PROPERTY_STAFF_ID` varchar(255) DEFAULT NULL,
		  `PROPERTY_TITLE` varchar(255) DEFAULT NULL,
		  `PROPERTY_SHORT_DESCRIPTION` text NOT NULL,
		  `PROPERTY_DESCRIPTION` longtext CHARACTER SET utf8 NOT NULL,
		  `PROPERTY_CATEGORY` varchar(255) NOT NULL,
		  `PROPERTY_PRICE` varchar(255) NOT NULL,
		  `PROPERTY_PRICE_FREQUENCY` varchar(255) NOT NULL,
		  `PROPERTY_QUALIFIER` varchar(255) NOT NULL,
		  `PROPERTY_AVAILABLE_DATE` datetime DEFAULT NULL,
		  `PROPERTY_ADDRESS_LINE_1` varchar(255) NOT NULL,
		  `PROPERTY_ADDRESS_LINE_2` varchar(255) NOT NULL,
		  `PROPERTY_ADDRESS_CITY` varchar(255) NOT NULL,
		  `PROPERTY_ADDRESS_COUNTY` varchar(255) NOT NULL,
		  `PROPERTY_ADDRESS_POSTCODE` varchar(255) NOT NULL,
		  `PROPERTY_FORMATTED_ADDRESS` varchar(255) NOT NULL,
		  `PROPERTY_STATUS` varchar(255) NOT NULL,
		  `PROPERTY_SOURCE` varchar(255) NOT NULL,
		  `PROPERTY_AVAILABILITY` varchar(255) NOT NULL,
		  `PROPERTY_ADMIN_FEES` varchar(255) NOT NULL,
		  `PROPERTY_TYPE` varchar(255) NOT NULL,
		  `PROPERTY_BEDROOMS` varchar(255) NOT NULL,
		  `PROPERTY_BATHROOMS` varchar(255) NOT NULL,
		  `PROPERTY_RECEPTION` varchar(255) NOT NULL,
		  `PROPERTY_TENURE` varchar(255) NOT NULL,
		  `Lease_term_years` varchar(255) DEFAULT NULL,
		  `PROPERTY_CLASSIFICATION` varchar(255) NOT NULL,
		  `PROPERTY_CURRENT_OCCUPANT` varchar(255) NOT NULL,
		  `KITCHEN-DINER` varchar(255) NOT NULL,
		  `OFF-ROAD_PARKING` varchar(255) NOT NULL,
		  `ON-ROAD_PARKING` varchar(255) NOT NULL,
		  `GARDEN` varchar(255) NOT NULL,
		  `WHEELCHAIR_ACCESS` varchar(255) NOT NULL,
		  `ELEVATOR_IN_BUILDING` varchar(255) NOT NULL,
		  `POOL` varchar(255) NOT NULL,
		  `GYM` varchar(255) NOT NULL,
		  `KITCHEN` varchar(255) NOT NULL,
		  `DINING_ROOM` varchar(255) NOT NULL,
		  `FURNISHED` varchar(255) NOT NULL,
		  `INTERNET` varchar(255) NOT NULL,
		  `WIRELESS_INTERNET` varchar(255) NOT NULL,
		  `TV` varchar(255) NOT NULL,
		  `WASHER` varchar(255) NOT NULL,
		  `DRYER` varchar(255) NOT NULL,
		  `DISHWASHER` varchar(255) NOT NULL,
		  `PETS_ALLOWED` varchar(255) NOT NULL,
		  `FAMILY_OR_CHILD_FRIENDLY` varchar(255) NOT NULL,
		  `DSS_ALLOWED` varchar(255) NOT NULL,
		  `SMOKING_ALLOWED` varchar(255) NOT NULL,
		  `24_7_SECURITY` varchar(255) NOT NULL,
		  `HOT_TUB` varchar(255) NOT NULL,
		  `CLEANER` varchar(255) NOT NULL,
		  `EN-SUITE` varchar(255) NOT NULL,
		  `SECURE_CAR_PARKING` varchar(255) NOT NULL,
		  `OPEN_PLAN_LOUNGE` varchar(255) NOT NULL,
		  `VIDEO_DOOR_ENTRY` varchar(255) NOT NULL,
		  `CONCIERGE_SERVICES` varchar(255) NOT NULL,
		  `PROPERTY_CUSTOM_FEATURES` longblob,
		  `PROPERTY_ROOMS` longblob NOT NULL,
		  `PROPERTY_ASSETS` longblob NOT NULL,
		  `PROPERTY_IMAGE_1` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_2` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_3` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_4` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_5` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_6` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_7` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_8` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_9` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_10` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_11` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_12` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_13` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_14` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_15` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_FLOOR_1` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_FLOOR_2` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_FLOOR_3` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_FLOOR_4` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_FLOOR_5` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_EPC_1` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_EPC_2` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_EPC_3` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_EPC_4` varchar(255) DEFAULT NULL,
		  `PROPERTY_IMAGE_EPC_5` varchar(255) DEFAULT NULL,
		  `CERTIFICATE_EXPIRE_DATE` longblob,
		  `PROPERTY_EPC_VALUES` text NOT NULL,
		  `PROPERTY_CREATED_ON` text NOT NULL,
		  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
		  `INSTRUCTED_DATE` date DEFAULT NULL,
		  `PROPERTY_LETTING_SERVICE` varchar(45) DEFAULT NULL,
		  `PROPERTY_LETTING_FEE` varchar(15) DEFAULT NULL,
		  `PROPERTY_LETTING_FEE_TYPE` varchar(15) DEFAULT NULL,
		  `PROPERTY_LETTING_FEE_FREQUENCY` varchar(15) DEFAULT NULL,
		  `PROPERTY_MANAGEMENT_FEE` varchar(15) DEFAULT NULL,
		  `PROPERTY_MANAGEMENT_FEE_TYPE` varchar(15) DEFAULT NULL,
		  `PROPERTY_MANAGEMENT_FEE_FREQUENCY` varchar(15) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		
		ALTER TABLE `properties` ADD PRIMARY KEY (`id`);
		
		ALTER TABLE `properties` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
		
		CREATE TABLE IF NOT EXISTS `property_images` (
		  `id` int(11) NOT NULL,
		  `PROPERTY_SERVER_ID` varchar(255) NOT NULL,
		  `PROPERTY_LOCAL_ID` varchar(500) NOT NULL,
		  `PROPERTY_IMAGE_TYPE` varchar(255) NOT NULL,
		  `PROPERTY_IMAGE` text NOT NULL,
		  `record_uploaded` tinyint(1) NOT NULL DEFAULT '0'
		) ENGINE=MyISAM DEFAULT CHARSET=latin1;
		
		ALTER TABLE `property_images` ADD PRIMARY KEY (`id`);
		
		ALTER TABLE `property_images` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
		
		";
		
		$db_connect->queryExecute($sql_create) or die($sql_create);

		$columns = array('PROPERTY_ID','PROPERTY_REF_ID','PROPERTY_TITLE','PROPERTY_DESCRIPTION','PROPERTY_CATEGORY','PROPERTY_PRICE','PROPERTY_PRICE_FREQUENCY','PROPERTY_QUALIFIER','PROPERTY_AVAILABLE_DATE','PROPERTY_FORMATTED_ADDRESS','PROPERTY_AVAILABILITY','PROPERTY_ADMIN_FEES','PROPERTY_BEDROOMS','PROPERTY_BATHROOMS','PROPERTY_RECEPTION','PROPERTY_ASSETS','PROPERTY_SHORT_DESCRIPTION','PROPERTY_ADDRESS_LINE_1','PROPERTY_ADDRESS_LINE_2','PROPERTY_ADDRESS_CITY','PROPERTY_ADDRESS_COUNTY','PROPERTY_ADDRESS_POSTCODE','PROPERTY_STATUS','PROPERTY_TYPE','PROPERTY_TENURE','PROPERTY_CLASSIFICATION','PROPERTY_CURRENT_OCCUPANT','KITCHEN-DINER');
	//	$values = array($PROPERTY_REF_ID,$PROPERTY_REF_ID,$PROPERTY_TITLE,$PROPERTY_DESCRIPTION,$PROPERTY_CATEGORY,$PROPERTY_PRICE,$PROPERTY_PRICE_FREQUENCY,$PROPERTY_QUALIFIER,$PROPERTY_AVAILABLE_DATE,$PROPERTY_FORMATTED_ADDRESS,$PROPERTY_AVAILABILITY,$PROPERTY_ADMIN_FEES,$PROPERTY_BEDROOMS,$PROPERTY_BATHROOMS,$PROPERTY_RECEPTION,$PROPERTY_ASSETS,'',$PROPERTY_ADDRESS_LINE_1,$PROPERTY_ADDRESS_LINE_2,$PROPERTY_ADDRESS_CITY,$PROPERTY_ADDRESS_COUNTY,$PROPERTY_ADDRESS_POSTCODE,'',$PROPERTY_CATEGORY,'','','','');
		$values = array('',23,'sd','sdf',$PROPERTY_CATEGORY,'54.0','pcm','2','3','3','4','4',5,'5','f',$PROPERTY_ASSETS,'',$PROPERTY_ADDRESS_LINE_1,$PROPERTY_ADDRESS_LINE_2,$PROPERTY_ADDRESS_CITY,$PROPERTY_ADDRESS_COUNTY,$PROPERTY_ADDRESS_POSTCODE,'','$PROPERTY_CATEGORY','12','12','12','12');
	//	$added_info =	$db_connect->insert_table_common($columns, $values, 'properties');
		$sql = "INSERT INTO properties (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`,`PROPERTY_SOURCE`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHING_STATUS', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON','$notice_source')";

		$added_info =	$db_connect->queryExecute($sql) or die($sql);
	
	
	}
	echo json_encode(array("success"=>true,"response"=>'<span style="color:#228B22">Added</span>',"count"=>$count));
}else{
	echo json_encode(array("success"=>false,"response"=>'<span style="color:#FF0000">Missing fields</span>'));
}
?>