<?php
ini_set('display_errors', 1);
require_once 'header.php';
?>
<link rel="stylesheet" href="assets/css/dashboard.css" />
</head>
  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">GNB Import</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </nav>

<div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link " href="index.php">
                  <span data-feather="home"></span>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link " href="table-lists.php">
                  <span data-feather="list"></span>
                  Table List
                </a>
              </li>

            </ul>
          </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

<h4 class=""> Import </h4>

<form method="post" action="import-table.php"  enctype="multipart/form-data">

<div class="form-group">
    <label for="exampleFormControlFile1">Example file input</label>
    <input type="file" class="form-control-file" id="ifile" name="upload" accept=".sql">
  </div>

  <input type="submit" class="btn btn-primary" name="save" value="Submit" />
</form>

<div class="log_holder">
    <?php
if (isset($_POST['save'])) {

    $location_file = $_FILES['upload']['tmp_name'];
    $name_file = $_FILES['upload']['name'];
    $type_file = $_FILES['upload']['type'];
    $size_file = $_FILES['upload']['size'];
    move_uploaded_file($location_file, 'uploads/' . $name_file);
    $table_result = $db_connect->queryExecute('uploads/' . $name_file);

    $filename = 'uploads/' . $name_file;
    // Temporary variable, used to store current query
    $templine = '';
// Read in entire file
    $fp = fopen($filename, 'r');
// Loop through each line
    while (($line = fgets($fp)) !== false) {
        // Skip it if it's a comment
        if (substr($line, 0, 2) == '--' || $line == '') {
            continue;
        }

        // Add this line to the current segment
        $templine .= $line;
        // If it has a semicolon at the end, it's the end of the query
        if (substr(trim($line), -1, 1) == ';') {
            // Perform the query
            if (!$db_connect->queryExecute($templine)) {
                print('Error performing query \'<i> : <br />');
            } else {
                print('Success performing query \'<i> : <br />');

            }
            // Reset temp variable to empty
            $templine = '';
        }
    }

}

?>
</div>
</main>


</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace();
    $('.log_holder').height($(window).height()-300)
    </script>


  </body>
</html>
