<?php require 'header.php';?>
<link rel="stylesheet" href="assets/css/dashboard.css" />
</head>
<body>
<?php 
include_once 'header_menu.php';

$my_url = parse_url($_SERVER['HTTP_HOST']);
$my_path = explode('.',$my_url['path']);
$subdomain_cnb = $my_path[0]; 
?>

<div class="container-fluid">
      <div class="row">
	    <?php include_once 'side_menu.php';?>
		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
			<h4 class=""> Extract properties </h4>
			<table  id="example" class="table table-striped table-bordered" style="width:50%;">
				<tr>
					<td>Property url *</td>
					<td><input type="text" placeholder="Enter url" id="property_external_url" class="form-control mandatory_field" /></td>
				</tr>
				<tr>
					<td>Property category *</td>
					<td>
						<select id="property_category" class="form-control mandatory_field">
					    	<option value="">---Category---</option>
					    	<option value="43">Residential Sales</option>
					    	<option value="44">Residential Lettings</option>
					    	<option value="45">Commercial sales</option>
					    	<option value="46">Commercial Lettings</option>
					    </select>
					</td>
				</tr>
				<tr>
					<td>No of properties</td>
					<td><input type="text" placeholder="Enter number" id="property_count" class="form-control"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<a href="javascript:;" id="ext_submit_btn" class="btn btn-sm btn-info float-right" onclick="get_external_properties();" >PROCEED</a>
						<span id="ext_submit_status"></span><br><span id="images_container"></span>
					</td>
				</tr>
			</table>
		</main>
	</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
var subdomain_cnb = '<?=$subdomain_cnb?>';
feather.replace();
  
function get_external_properties(){
	var flag = 0;
	var property_external_url = $('#property_external_url').val();
	var property_category = $('#property_category').val();
	var property_count = $('#property_count').val();
	
	$('.mandatory_field').each(function() {
    	if(!$(this).val()){
    		$(this).css('border-color','#FF0000');
    		flag = 1;
    	}else{
    		$(this).css('border-color','');
    	}
    });
	
	if(flag==0){
		$('#ext_submit_btn').hide();
		$('#ext_submit_status').html('Processing...');
		$.post({ url:'fetch_external_properties.php', data:{ subdomain_cnb:subdomain_cnb,property_count:property_count,property_category:property_category,property_url:property_external_url },
			success:function(response_text){
		    response_text = jQuery.parseJSON(response_text);
		    if(response_text.success){
				$('#ext_submit_status').html(response_text.count+' properties fetched.copying property images...');
				get_property_images();
		    }else{
		    	$('#ext_submit_btn').show();
		    	$('#ext_submit_status').html(response_text.response);
		    }
		}});
	}else{
		$('#ext_submit_btn').show();
	}
}

function get_property_images(){
    $.post({ url:'get_external_property_images.php', data:{ subdomain_cnb:subdomain_cnb},
        success:function(response_text){
        $('#images_container').html(response_text);
    }});
}

     function push_records(table_name) { 
          
              $.post({ url:'push_table.php', data:{ table:'properties' },
              success:function(response_text){
                //response_text = jQuery.parseJSON(response_text);

                if(response_text.success){
                  console.log(response_text.message +' inserted : '+ response_text.inserted+ ' Failed :'+ response_text.failed);
                  
                  if(response_text.message_code == 100){ 
                     alert('Completed');
                  }else if(response_text.message_code != 100){
                     setTimeout(function() {
                    	push_records(table_name);
                     }, 3000)
                  }
                  
                }else{
                  console.log(response_text.message +' '+ response_text.failed);
                }
              }})
            }
</script>


</body>
</html>