<?php
ini_set('PHP_MAX_EXECUTION_TIME', 10000);
extract($_GET);
include 'header.php';
$db_connect->table = $table;
$get_columns = $db_connect->get_tablecolumns();
$columns = json_decode($get_columns);
?>
</head>
<body>
<div class="m-4">
<h4 class="dispaly-4 mt-3 mb-3"><?php  echo ucwords(str_replace('_', ' ', $table)); ?> <a href="javascript:history.go(-1);"  class="text-right float-right btn btn-primary "> Back</a>
 </h3>

<table id="example" class="table table-striped table-bordered" >
<thead>
	<tr>
        <?php
$data_col = '';
foreach ($columns->data as $column) {
    $data_col .= '{"data": "' . $column->Field . '" },';?>
		<th style="width:200px;"><?php echo ucfirst(str_replace('_', ' ', $column->Field)); ?></th>
		<?php
}?>
	</tr>
</thead>

<tfoot>
	<tr>
		<?php
foreach ($columns->data as $column) {
    ?>
            <th style="width:200px;"><?php echo $column->Field ?></th>
                <?php
}?>


	</tr>
</tfoot>


</table>
    </div>
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {

    $('#example').DataTable( {
        "pageLength":25,
        "processing": true,
        "serverSide": true,
        "scrollY": $(window).height()-290,
        "scrollX": true,
        "ajax": {
            "url": "post.php?table=<?php echo $table; ?>",
            "type": "POST",
        },
        "columns": [
           <?php echo $data_col ?>
        ],
        "order": [[1, 'asc']]

     });
    });

</script>
</body>
</html>
