<?php
require_once 'header_init.php';
require_once 'plugins/ssp.class.php';

$db_connect->table=$_GET['table'];
$get_columns = $db_connect->get_tablecolumns();
$columns = json_decode($get_columns);

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case object
// parameter names
$blob_fields=[];
foreach($columns->data as $column){
$data_col[]=array('db'=>$column->Field, 'dt'=>$column->Field);
$blob_fields[$column->Field]=$column->Type;

}
$primaryKey = $data_col[0]['db'];

 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

$ssp = new SSP($sql_details);
$data_ssp = $ssp->simple( $_POST, $sql_details, $_GET['table'], $primaryKey, $data_col, $blob_fields);
echo json_encode(
    $data_ssp
);
?>

