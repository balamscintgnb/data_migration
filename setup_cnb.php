<?php 
// Database Creation 
$setup_cnb=true;

require_once 'header_init.php';

// DB 
$db_result = json_decode($db_connect->create_database($db_name)); 

// Table Creation 
$table_result = json_decode($db_connect->create_table($db_name)); 

//table insert
$row_result = json_decode($db_connect->insert_table($db_name, $agent_sub_domain, $agent_name, $agent_logo, $completion_date, 0));

$success = true;

if(!$db_result->success || !$table_result->success || !$row_result->success){
	$success = false;
}

echo json_encode(array("success"=>$success, "response"=> $db_result->message."\n".$table_result->message."\n".$row_result->message));
?>