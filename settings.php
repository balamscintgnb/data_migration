<?php require 'header.php';?>
<link rel="stylesheet" href="assets/css/dashboard.css" />
</head>
  <body>
  <?php include_once 'header_menu.php';?>
    <div class="container-fluid">
        <div class="row">
        <?php include_once 'side_menu.php'; 
        if(!empty($_POST)){ 

            $db_connect->update_boardinformation( 'board_information',$_POST['server']);
            
            $board_information_data = $db_connect->get_boardinformation();
            $board_information = json_decode($board_information_data);

        }else { 
            $board_information_data = $db_connect->get_boardinformation();
            $board_information = json_decode($board_information_data);
        }
        $information = $board_information->data;
        $board = $information->subdomain; // Board Name get from subdomain  
        
        ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <h4 class=""> Settings </h4> 
                <form method="post" action=""> 
                <label class="s">
                 <input type="radio" name="server" value="1" <?php echo ($information->server==1)?'checked':''; ?> onchange="this.form.submit();" > Live 
                 <input type="radio" name="server" value="0" <?php echo ($information->server==0)?'checked':''; ?> onchange="this.form.submit();" > Development
<br />
<?php 
$dev='';
if($information->server == 0)
$dev='.dev';
echo "<br />";
echo " Data will be pushed to http://". $board. $dev. ".globalnoticeboard.com";

?>
                </form>
</label>
            </main>
        </div>
    </div>
  </body>

  <!-- Bootstrap core JavaScript
    <label>
    <?php if($information->server==1){ ?> 
      <input id="checkValue" name="server" type="checkbox" value="0"   onchange="this.form.submit();" />    
    <?php } else { ?> 
            <input id="checkValue" name="server" type="checkbox" value="1"  <?php echo ($information->server==0)?'checked':''; ?>  onchange="this.form.submit();" />           
    <?php } ?>

      <span class="slider"></span>
    </label>

    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>-->
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

</html>