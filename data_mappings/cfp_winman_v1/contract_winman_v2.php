<?php

require_once '../includes/config.php';

require_once '../../header_init.php';


$sql_details_src = array( 

    'username' => 'root',
    'password' => '',
    'database'   => 'jennings_cfp_let2',
    'host' => 'localhost:3307'

);


$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = 'select * from `host_contract`';

$data = json_decode($db_connect_src->queryFetch($query),true);

/*echo '<pre>';
print_r($data);
echo '</pre>';

exit;*/

if(count($data)>0){

	foreach($data['data'] as $row){

		$COMPANY_NAME='';

		$CLIENT_TYPE			= 'SUPPLIER';
		$CLIENT_SUB_TYPE		= '';

		$CLIENTID				= 'B2_'.$row['CCODE'];

		$COMPANY_NAME				= $row['CCO_NAME'];

		$CLIENT_TITLE			= '';
		$CLIENT_NAME			= $row['CNAME'];

		if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
			if($CLIENT_NAME!=''){
				$CLIENT_NAME.=' ';
			}
			$CLIENT_NAME.='('.$COMPANY_NAME.')';
		}

		$CLIENT_NAME			= $CLIENT_NAME;

		$CLIENT_PRIMARY_PHONE='';

		$CLIENT_PRIMARY_EMAIL=$row['CTELHOME'];

		$email1					= trim($row['CE-MAIL']);


		$CLIENT_NOTES			= '';
		$CLIENT_PRIMARY_PHONE	= $row['CTELMOBL'];

		if($db_utility->validate_email_id($email1)){
			$CLIENT_PRIMARY_EMAIL	= $email1;
		}
		else{
			$CLIENT_NOTES			= $email1.' ';
		}

		$CLIENT_EMAIL_1			= '';
		$CLIENT_EMAIL_2			= '';
		$CLIENT_EMAIL_3			= '';
		$CLIENT_EMAIL_4			= '';
		$CLIENT_EMAIL_5			= '';

		$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';


		$phone_1 				= '';
		$phone_2 				= $row['CTELWK1'];
		$phone_3 				= $row['CTELWK2'];

		$fax_1= $row['CTELFAX'];


		$CLIENT_ADDRESS_LINE_1 	= '';
		$CLIENT_ADDRESS_LINE_2	= '';
		$CLIENT_ADDRESS_CITY	= '';
		$CLIENT_ADDRESS_TOWN	= '';
		$CLIENT_ADDRESS_POSTCODE= '';

		$address_line1		= trim($row['CADD1']);
		$street				= trim($row['CADD2']);
		$city			= trim($row['CADD3']);
		$town				= trim($row['CADD4']);
		$postcode			= trim($row['CPSTCD']);

		if($address_line1!=''){
			$CLIENT_ADDRESS_LINE_1.=$address_line1;
		}

		if($street!=''){
			$CLIENT_ADDRESS_LINE_2.=$street;
		}

		if($city!=''){
			$CLIENT_ADDRESS_CITY.=$city;
		}

		if($town!=''){
			$CLIENT_ADDRESS_TOWN.=$town;
		}

		if($postcode!=''){
			$CLIENT_ADDRESS_POSTCODE.=$postcode;
		}

		$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
		$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
		$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
		$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
		$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

		$CLIENT_PHONE_1			= $phone_1;
		$CLIENT_PHONE_2			= $phone_2;
		$CLIENT_PHONE_3			= $phone_3;
		$CLIENT_PHONE_4			= $phone_4;
		$CLIENT_PHONE_5			= $phone_5;

		$CLIENT_MOBILE_1		= $mobile_1;
		$CLIENT_MOBILE_2		= $mobile_2;
		$CLIENT_MOBILE_3		= $mobile_3;
		$CLIENT_MOBILE_4		= $mobile_4;
		$CLIENT_MOBILE_5		= $mobile_5;

		$CLIENT_FAX_1			= $fax_1;
		$CLIENT_FAX_2			= $fax_2;
		$CLIENT_FAX_3			= $fax_3;
		$CLIENT_FAX_4			= $fax_4;
		$CLIENT_FAX_5			= $fax_5;

		$CLIENT_STATUS ='';
		$CLIENT_STAFF_ID ='';
		$CLIENT_NOTES.= $row['CREMARK'];

		if($row['lastmodified']!=''){
			$CLIENT_CREATED_ON 		= "'".date('Y-m-d',strtotime($row['lastmodified']))."'";
		}
		else 
		$CLIENT_CREATED_ON  = "NULL";
	
		$truncate_sql = "DELETE FROM cnb_jennings_winman.`clients` where `CLIENTID`='$CLIENTID'";
		$db_connect->queryExecute($truncate_sql);


	$sql = "INSERT INTO cnb_jennings_winman.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', $CLIENT_CREATED_ON , '0')";
	
	 $db_connect->queryExecute($sql);
	}
}
?>
