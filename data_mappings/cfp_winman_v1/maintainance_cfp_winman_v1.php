<?php
// require_once '../includes/config.php';

require_once '../../header_init.php';

$sql_details_src = array(
    'username' => $user_name,
    'password' => $password,
    'database'   => 'ds_cnb_stones_cfp_1',
    'host' => 'localhost'
);

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = 'select * from `host_worksheets`';

$data = json_decode($db_connect_src->queryFetch($query),true);

/*echo '<pre>';
print_r($data);
echo '</pre>';

exit;*/

if(count($data)>0){

  $i=0;

  foreach($data['data'] as $row){

    $JOB_NOTES='';
    
    $MAINTENANCE_CATEGORY='24';

    $MAINTENANCE_CODE			= $row['ID'];
    $PROPERTY_ID				= $row['PCODE'];
    $TENANCY_CODE			= $row['TYCODE'];
    $TENANT_CODE			= $row['TCODE'];
    $SUPPLIER_CODE			= $row['CCODE'];
    $ESTIMATE_COST			= $row['OurEstimate'];
    $MAINTENANCE_RATING			= $row['Rating'];

    $publicJobDetails			= $row['PublicJobDetails'];//notes

    $MAINTENANCE_DATE_REPORTED='';
    if($row['DateReported']!=''){
      $MAINTENANCE_DATE_REPORTED			= date('Y-m-d',strtotime($row['DateReported']));
    }

    $MAINTENANCE_DATE_COMPLETED='';
    if($row['DateCompleted']!=''){
      $MAINTENANCE_DATE_COMPLETED			= '"'.date('Y-m-d',strtotime($row['DateCompleted'])).'"';
    }

    $MAINTENANCE_NOTES			= $row['Notes'];

    $MAINTENANCE_ITEM_DATE='';
    if($row['ItemDate']!=''){
      $MAINTENANCE_ITEM_DATE			= date('Y-m-d',strtotime($row['ItemDate']));
    }

    $MAINTENANCE_ITEM_DESCRIPTION			= $row['ItemDesc'];
    
    if($MAINTENANCE_ITEM_DESCRIPTION	==''){
      $MAINTENANCE_ITEM_DESCRIPTION.='MAINTENANCE';
    }
    
    $MAINTENANCE_ITEM_AMOUNT			= $row['ItemAmount'];
    $MAINTENANCE_ITEM_VAT			= $row['ItemAmVAT'];

    $MAINTENANCE_ITEM_PREF_DATE='';
    if($row['PrefSDate']!=''){
      $MAINTENANCE_ITEM_PREF_DATE			= date('Y-m-d',strtotime($row['PrefSDate']));
    }

    $MAINTENANCE_CAT_TYPE			= $row['ExpType'];

    //Archived, DateArchived

    /*
    ItemPer-NA
    ExpPosted-??
    ScheduleItemID-NA
    ExpenseID-NA
    Schedule-NA
    JobId-NA
    JobRef-NA
    JobSummary-NA
    JobDetails-NA
    JobStatus-NA
    TimelineSent-NA
     */

     /*
      <option value="1">Air Conditioning</option>
      <option value="2">Alarms And Smoke Detectors</option>
      <option value="3">Audiovisual</option>
      <option value="4">Bathroom And Toilet</option>
      <option value="5">Communal/Shared Facilities</option>
      <option value="6">Doors, Garages And Locks</option>
      <option value="7">Electricity</option>
      <option value="8">Exterior And Garden</option> --- 
      <option value="9">Fire</option>
      <option value="10">Furniture</option>
      <option value="11">Gas</option>
      <option value="12">Heating</option>
      <option value="13">Hot Water</option>
      <option value="14">Internal Floors, Walls And Ceiling</option>
      <option value="15">Inventory</option>
      <option value="16">Kitchen</option>
      <option value="17">Laundry</option>
      <option value="18">Lighting</option>
      <option value="19">Pests/Vermin</option>
      <option value="20">Roof</option>
      <option value="21">Stairs</option>
      <option value="22">Water And Leaks</option>
      <option value="23">Windows</option>
      <option value="24">Other</option>
     */
    
    if($MAINTENANCE_CAT_TYPE=="10" || $MAINTENANCE_CAT_TYPE=="0"){
      //leak cleaning
      $MAINTENANCE_CATEGORY='22';
    }else if($MAINTENANCE_CAT_TYPE=="14"){
      $MAINTENANCE_CATEGORY='23';
    }else if($MAINTENANCE_CAT_TYPE=="1"){//
      //repair
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="3"){//
      //painting
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="4"){//
      //electricity repair
      $MAINTENANCE_CATEGORY='7';
    }else if($MAINTENANCE_CAT_TYPE=="5"){
      //cleaning
      $MAINTENANCE_CATEGORY='24';////
    }else if($MAINTENANCE_CAT_TYPE=="6"){
      //cleaning
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="7"){
      //rent protection
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="8"){
      //key, leak
      $MAINTENANCE_CATEGORY='6';
    }else if($MAINTENANCE_CAT_TYPE=="9"){
      //top up, buy
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="10"){
      //cleaning and repair
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="11"){
      //heat, leak
      $MAINTENANCE_CATEGORY='24';
    }else if($MAINTENANCE_CAT_TYPE=="13"){
      //top up gas, electric
      $MAINTENANCE_CATEGORY='11';
    }else if($MAINTENANCE_CAT_TYPE=="15"){
      //new, buy
      $MAINTENANCE_CATEGORY='24';
    }else{
      $MAINTENANCE_CATEGORY='24';
    }

    $JOB_NOTES=$MAINTENANCE_NOTES;

    if($MAINTENANCE_ITEM_VAT!=''){
        $JOB_NOTES.="VAT:".$MAINTENANCE_ITEM_VAT.'<br/>';
    }
    
    if($MAINTENANCE_RATING!=''){
        $JOB_NOTES.="Rating:".$MAINTENANCE_RATING.'<br/>';
    }

    $PAID_BY='0';
    if($TENANT_CODE!='' && isset($TENANT_CODE)){
        $PAID_BY=2;
    }

    if($MAINTENANCE_ITEM_PREF_DATE!=''){
      $JOB_NOTES.="Preferred Date:".$MAINTENANCE_ITEM_PREF_DATE.'<br/>';
    }

    //MAINTENANCE_ITEM_DATE
    if($MAINTENANCE_ITEM_DATE!=''){
      $JOB_NOTES.="Item Date:".$MAINTENANCE_ITEM_DATE.'<br/>';
    }

    if($publicJobDetails!=''){
      $JOB_NOTES.=$publicJobDetails.'<br/>';
    }

    //$JOB_NOTES=$JOB_NOTES;
    $JOB_REQUEST_DATE='NULL';
    $JOB_UPDATED_ON='NULL';
    $JOB_CONFIRMED_DATE='NULL';
    $JOB_INVOICE_AMT_AGENT='NULL';
    $JOB_PAYABLE_STATUS='0';
    $JOB_STATUS='0';
    $JOB_TRANSACTION_ID='0';
    if($MAINTENANCE_DATE_COMPLETED==''){
    	$MAINTENANCE_DATE_COMPLETED='NULL';
    }
     

  $sql = "INSERT INTO `maintenance_jobs` (`JOB_REF_NO`, `REPORT_PROBLEM_ID`, `SUPPLIER_ID`, `PROPERTY_ID`, `JOB_CATEGORY`, `JOB_DESCRIPTION`, `JOB_ON`, `JOB_PRIORITY`,
  `JOB_ESTIMATED_COST`, `JOB_WILL_BE_PAID_BY`, `JOB_CONTRACTOR_INVOICE_FILE`, `JOB_AGENT_INVOICE_FILE`, `JOB_REQUEST_DATE`, `JOB_CONFIRMED_DATE`, `JOB_CONTRACTOR_QUOTE_FILE`,
  `JOB_AGENT_QUOTE_FILE`, `JOB_INVOICE_AMT_CONTRACTOR`, `JOB_INVOICE_AMT_AGENT`, `JOB_PAYABLE_USER_ID`, `JOB_PAYABLE_STATUS`, `JOB_PAYABLE_AMOUNT`, `JOB_PAYABLE_PAID_AMOUNT`,
    `JOB_PAYABLE_INVOICE_FILE`, `JOB_PAYABLE_TRANSACTION_IDS`, `JOB_STATUS`,`JOB_NOTES`,`JOB_CREATED_ON`, `JOB_COMPLETED_ON`, `JOB_TRANSACTION_ID`, `JOB_UPDATED_ON`)
    VALUES ('$MAINTENANCE_CODE', '1', '$SUPPLIER_CODE', '$PROPERTY_ID', '$MAINTENANCE_CATEGORY', '$MAINTENANCE_ITEM_DESCRIPTION', '$MAINTENANCE_DATE_REPORTED', '1', '$ESTIMATE_COST', '$PAID_BY', '', '',$JOB_REQUEST_DATE, $JOB_CONFIRMED_DATE, '', '', '$MAINTENANCE_ITEM_AMOUNT', $JOB_INVOICE_AMT_AGENT, '', '$JOB_PAYABLE_STATUS', '', '', '', '','$JOB_STATUS','$JOB_NOTES', '$MAINTENANCE_DATE_REPORTED', $MAINTENANCE_DATE_COMPLETED, '$JOB_TRANSACTION_ID', $JOB_UPDATED_ON)";

//	 echo $sql.'<br/>';
    $db_connect->queryExecute($sql) or die($sql);

  //$i++;

    /*if($i>10){
      break;
    }*/
  }

}
?>