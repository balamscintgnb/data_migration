<?php
// error_reporting(E_ALL);
// ini_set('display_errors',1);

// require_once '../includes/config.php';

$data_mapping = true;

require_once '../../header_init.php';

$sql_details_src = array(
    'username' => $user_name,
    'password' => $password,
    'database'   => 'ds_cnb_move',
    'host' => 'localhost'
);

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

//$data = $db_connect->get_datas_join('host_ten_inf','lighthouse_vebra_rent_src');
 $query = 'select `TYCODE`,`TYPCODE`,`TYPLOCK`,`TYNAME`,`TYSTATUS`,`TYSTART`,`TYCurSTART`,`TYRECTYPE`,`TYRENEW`,`TYPeriodic`,`TYNTQ`,`TYVAC`,`TYINSP`,
`TYINSPTXT`,`TYTERMMTH`,`TYGASIN`,`TYGASOUT`,`TYELECIN`,`TYELECOUT`,`TYWATRIN`,`TYWATROUT`,`TYSEWIN`,`TYSEWOUT`,`TYRENT`,`TYPER`,`TYPERWORDS`,`TYRENTDAT`,`TYRENTDAY`,
`TYRENTWK`,`TYRENTMTH`,`TYRENTAPP`,`TYDEPHOLD`,`TYFDEP3`,`DepositProtectionScheme`,`DepositProtectionSchemeID`,`DepositProtectionSchemeNotes`,`DepositProtectionSchemeNotesGeneral`,
`DepositProtectionSchemeRegistrationNumber`,`TYDEPFULL`,`PLETPCT`,`PMANPCT`,`PSTATUS`,`TYUSERNT1`,`TYUSERNT2`,`TYUSERNT3`,`TYUSERNT4`,`PCONELECT`,`PCONGAS`,`PCONWATER` from host_ten_inf as p1 left join host_prop_inf as p2 on p1.TYPCODE=p2.PCODE';

$data = json_decode($db_connect_src->queryFetch($query),true);

if(count($data)>0){

    foreach($data['data'] as $row){

        $TENANCY_NOTES='';

        $TENANCY_END_DATE =  $TENANCY_START_DATE	=   $TENANCY_CURRENT_START_DATE	="NULL";
        $TENANCY_CODE			= $row['TYCODE'];
        $PROPERTY_REF_ID			= $row['TYPCODE'];
        $PROPERTY_LOCK			= $row['TYPLOCK'];
        //$TENANCY_ID		= $row['TCODE'];
        $TENANCY_STATUS			= $row['TYSTATUS'];
        if($row['TYSTART']!='')
        $TENANCY_START_DATE			= "'".$row['TYSTART']."'";
        if($row['TYCurSTART']!='')
        $TENANCY_CURRENT_START_DATE			= "'".$row['TYCurSTART']."'";
        $TENANCY_RECEIPT_TYPE			= $row['TYRECTYPE'];
        $TENANCY_RENEW_TYPE			= $row['TYRENEW'];
        $TENANCY_PERIODIC_TYPE			= $row['TYPeriodic'];
        if($row['TYVAC']!='')
        $TENANCY_END_DATE			= "'".$row['TYVAC']."'";
        $TENANCY_VACATING_TYPE			= $row['TYVAC'];
        $TENANCY_INSPECTION_DATE			= $row['TYINSP'];
        $TENANCY_INSPECTION_DATE_PERIOD			= $row['TYINSPTXT'];
        $TENANCY_CONTRACT_TERM_MONTH			= $row['TYTERMMTH'];
        //$TENANCY_CONTRACT_TERM_YEAR			= $row['TYTERMYR'];
        $TENANCY_GAS_IN			= $row['TYGASIN'];
        $TENANCY_GAS_OUT			= $row['TYGASOUT'];
        $TENANCY_ELE_IN			= $row['TYELECIN'];
        $TENANCY_ELE_OUT			= $row['TYELECOUT'];
        $TENANCY_WATR_IN			= $row['TYWATRIN'];
        $TENANCY_WATR_OUT			= $row['TYWATROUT'];
        $TENANCY_SEW_IN			= $row['TYSEWIN'];
        $TENANCY_SEW_OUT			= $row['TYSEWOUT'];
        $TENANCY_ELE_SUPPLIER		= $row['PCONELECT'];
        $TENANCY_GAS_SUPPLIER		= $row['PCONGAS'];
        $TENANCY_WATER_SUPPLIER		= $row['PCONWATER'];
        $TENANCY_RENT			= $row['TYRENT'];
        $TENANCY_PERIOD			= $row['TYPER'];
        $TENANCY_PERIOD_WORDS			= $row['TYPERWORDS'];

        $TENANCY_RENT_PAYMENT_FREQUENCY='';

        if($TENANCY_PERIOD_WORDS!=''){

            if($TENANCY_PERIOD_WORDS=='Four Week' || $TENANCY_PERIOD_WORDS=='Four Weekly'){
                $TENANCY_PERIOD_WORDS='p4w';
                $TENANCY_RENT_PAYMENT_FREQUENCY='four_weekly';
            }else if($TENANCY_PERIOD_WORDS=='Half Year'){
                $TENANCY_PERIOD_WORDS='p6m';
                $TENANCY_RENT_PAYMENT_FREQUENCY='semi_annually';
            }else if(stristr($TENANCY_PERIOD_WORDS, 'month') || stristr($TENANCY_PERIOD_WORDS, 'Monthly')){
                $TENANCY_PERIOD_WORDS='pcm';
                $TENANCY_RENT_PAYMENT_FREQUENCY='monthly';
            }else if($TENANCY_PERIOD_WORDS=='Week'){
                $TENANCY_PERIOD_WORDS='pw';
                $TENANCY_RENT_PAYMENT_FREQUENCY='weekly';
            }else if($TENANCY_PERIOD_WORDS=='Year'){
                $TENANCY_PERIOD_WORDS='pa';
                $TENANCY_RENT_PAYMENT_FREQUENCY='annually';
            }else if(stristr($TENANCY_PERIOD_WORDS, 'Periodic')){
                $TENANCY_PERIOD_WORDS='';
                $TENANCY_RENT_PAYMENT_FREQUENCY='';
            }

        }

        $TENANCY_DUE_DATE		= $row['TYRENTDAT'];
        $TENANCY_RENT_DUE_NOTES			= $row['TYRENTDAY'];
        $TENANCY_WEEK			= $row['TYRENTWK'];
        $TENANCY_MONTH			= $row['TYRENTMTH'];
        $TENANCY_RENT_APPORTIONED			= $row['TYRENTAPP'];
        $TENANCY_DEPOSIT_HOLD			= $row['TYDEPHOLD'];
        $TENANCY_PAYABLE_TO_LANDLORD			= $row['TYFDEP3'];
        $TENANCY_PROTECTION_SCHEME			= $row['DepositProtectionScheme'];
        $TENANCY_PROTECTION_SCHEME_ID			= $row['DepositProtectionSchemeID'];
        $TENANCY_PROTECTION_SCHEME_NOTES			= $row['DepositProtectionSchemeNotes'];
        $TENANCY_PROTECTION_SCHEME_NOTES_GENERAL			= $row['DepositProtectionSchemeNotesGeneral'];

        $DUE_DATE='';
        if($TENANCY_DUE_DATE!=''){
            $DUE_DATE 		= date('d',strtotime($TENANCY_DUE_DATE));
        }


        $DEPOSIT_HELD='';

        if($TENANCY_PAYABLE_TO_LANDLORD==1){
            $DEPOSIT_HELD=2;
        }else if ($TENANCY_PROTECTION_SCHEME==1){
            $DEPOSIT_HELD=1;
        }

        $TENANCY_PROTECTION_ID='';

        if($DEPOSIT_HELD==1){
            if($TENANCY_PROTECTION_SCHEME_ID==1){
                $TENANCY_PROTECTION_ID=1;
            }else{
                $TENANCY_PROTECTION_ID='';
            }
        }else{
            $TENANCY_PROTECTION_ID='';
        }

        $TENANCY_FULL_DEPOSIT			= $row['TYDEPFULL'];

        $TENANCY_LETTING_FEE_PERCENTAGE			= $row['PLETPCT'];

        $TENANCY_LETTING_FEE_PERIOD			= 'pcm';

        $TENANCY_MANAGEMENT_FEE_PERCENTAGE			= $row['PMANPCT'];

        $TENANCY_MANAGEMENT_SERVICE			= $row['PSTATUS'];

        $MANAGED_LEVEL='';

        if($TENANCY_MANAGEMENT_SERVICE!=''){
            if(stristr($TENANCY_MANAGEMENT_SERVICE, 'LET ONLY')){
                $MANAGED_LEVEL=2;
            }else if(stristr($TENANCY_MANAGEMENT_SERVICE, 'MANAGED')){
                $MANAGED_LEVEL=1;
            }else{
                $MANAGED_LEVEL='';
            }
        }

        $TENANCY_NOTES.=' '.$TENANCY_PROTECTION_SCHEME_NOTES;

        $TENANCY_NOTES.=' '.$TENANCY_PROTECTION_SCHEME_NOTES_GENERAL;

        $TENANCY_NOTES.= ' '.$row['TYUSERNT1'];

        $TENANCY_NOTES.= ' '.$row['TYUSERNT2'];

        $TENANCY_NOTES.= ' '.$row['TYUSERNT3'];

        $TENANCY_NOTES.= ' '.$row['TYUSERNT4'];

        $TENANCY_NOTES.= ' '.$TENANCY_RENT_DUE_NOTES;

        if($row['DepositProtectionSchemeRegistrationNumber']!=''){
            $TENANCY_NOTES.= '\n Deposit Scheme ID:'.$row['DepositProtectionSchemeRegistrationNumber'];
        }

        $query2 = "select `TCODE` from `host_tenants` where `TTYCODE`='$TENANCY_CODE'";

        $data2 = json_decode($db_connect_src->queryFetch($query2),true);

        $SHARED_TENANT_IDS='';
        $shared_tenant_array=array();
        $sh=0;
        $lead_tenant='';

        if(count($data2)>0){
            foreach($data2['data'] as $row2){
                $sh++;
                if($sh==1){
                    $lead_tenant=$row2['TCODE'];
                }else{
                    $shared_tenant_array[]=$row2['TCODE'];
                }
            }
        }

        $SHARED_TENANT_IDS=json_encode($shared_tenant_array);

        /*echo '<pre>';
        print_r($SHARED_TENANT_IDS);
        echo '</pre>';*/

        //exit;
         $sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
            `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
            `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`, `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
            `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
            `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
            `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`)
            VALUES ('','$TENANCY_CODE', '$PROPERTY_REF_ID', '', '$lead_tenant','$SHARED_TENANT_IDS',$TENANCY_START_DATE, $TENANCY_END_DATE, '$TENANCY_RENT', '$TENANCY_PERIOD_WORDS',
            '$TENANCY_RENT_PAYMENT_FREQUENCY', '$DUE_DATE', '', '', '', '', '', '', '$TENANCY_FULL_DEPOSIT', NULL, '$DEPOSIT_HELD', '$TENANCY_PROTECTION_ID', '$MANAGED_LEVEL',
            '$TENANCY_LETTING_FEE_PERCENTAGE','','$TENANCY_LETTING_FEE_PERIOD', '$TENANCY_MANAGEMENT_FEE_PERCENTAGE', '', '', '', '', '', '', '', '0.00','','$TENANCY_NOTES')";

		//echo $sql.'<br/>';
		//var_dump($row['PLETPCT']);
        $db_connect->queryExecute($sql) or die($sql);
    }

}

echo 'Done';
?>