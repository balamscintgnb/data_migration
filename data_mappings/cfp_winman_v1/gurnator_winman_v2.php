<?php

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_utility = new Utility();


$sql_details_src = array( 

    'username' => 'root',
    'password' => '',
    'database'   => 'jennings_cfp_let2',
    'host' => 'localhost:3307'

);


$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = "select * from host_garant";

$data = json_decode($db_connect_src->queryFetch($query),true);

/*echo '<pre>';
print_r($data);
echo '</pre>';

exit;*/

if(count($data)>0){

	foreach($data['data'] as $row){

		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'GUARANTOR';

		$ACCOUNT_NAME='';

		$ACCOUNT_NUMBER='';

		$ACCOUNT_SORTCODE='';

		$CLIENTID				= $row['GCODE'];
		$CLIENT_TENANCY_ID				= $row['GTCODE'];

		$CLIENT_TITLE			= '';
		$CLIENT_NAME			= $row['GNAME'];

		$CLIENT_NAME2			= $row['GCHNAME'];
		$CLIENT_STAFF_ID 		= '';
		if(trim($CLIENT_NAME2)!='' && $CLIENT_NAME2!=$CLIENT_NAME){
			if($CLIENT_NAME!=''){
				$CLIENT_NAME.=' & ';
			}
			$CLIENT_NAME.=$CLIENT_NAME2;
		}

		$CLIENT_NAME			= $CLIENT_NAME;

	
		$CLIENT_STATUS = '';
		

		//$email1='';

		$CLIENT_PRIMARY_PHONE='';

		$CLIENT_PRIMARY_EMAIL='';

		$email1					= trim($row['GE-MAIL']);

		$CLIENT_NOTES			= '';

		$CLIENT_PRIMARY_PHONE	= $row['GTELMOBL'];

		if($db_utility->validate_email_id($email1)){
			$CLIENT_PRIMARY_EMAIL	= $email1;
		}
		else{
			$CLIENT_NOTES			= $email1.' ';
		}

		$CLIENT_EMAIL_1			= '';
		$CLIENT_EMAIL_2			= '';
		$CLIENT_EMAIL_3			= '';
		$CLIENT_EMAIL_4			= '';
		$CLIENT_EMAIL_5			= '';

		$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';


		$phone_1 				= $row['GTEL'];
		$phone_2 				= $row['GTELWK'];
		$phone_3 				= $row['GEMTEL'];
		$phone_4 				= $row['GBKTEL'];

		$fax_1= $row['GTELFAX'];


		$CLIENT_ADDRESS_LINE_1 	= '';
		$CLIENT_ADDRESS_LINE_2	= '';
		$CLIENT_ADDRESS_CITY	= '';
		$CLIENT_ADDRESS_TOWN	= '';
		$CLIENT_ADDRESS_POSTCODE= '';

		$address_line1		= trim($row['GADD1']);
		$street				= trim($row['GADD2']);
		$city			= trim($row['GADD3']);
		$town				= trim($row['GADD4']);
		$postcode			= trim($row['GPSTCD']);

		if($address_line1!=''){
			$CLIENT_ADDRESS_LINE_1.=$address_line1;
		}

		if($street!=''){
			$CLIENT_ADDRESS_LINE_2.=$street;
		}

		if($city!=''){
			$CLIENT_ADDRESS_CITY.=$city;
		}

		if($town!=''){
			$CLIENT_ADDRESS_TOWN.=$town;
		}

		if($postcode!=''){
			$CLIENT_ADDRESS_POSTCODE.=$postcode;
		}

		$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
		$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
		$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
		$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
		$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

		$CLIENT_ADDRESS1_LINE_1 	= '';
		$CLIENT_ADDRESS1_LINE_2	= '';
		$CLIENT_ADDRESS1_CITY	= '';
		$CLIENT_ADDRESS1_TOWN	= '';
		$CLIENT_ADDRESS1_POSTCODE= '';

		$address_line11		= trim($row['GCHADD1']);
		$street1				= trim($row['GCHADD2']);
		$city1			= trim($row['GCHADD3']);
		$town1				= trim($row['GCHADD4']);
		$postcode1			= trim($row['GCHPSTCD']);

		if($address_line11!=''){
			$CLIENT_ADDRESS1_LINE_1.=$address_line11;
		}

		if($street1!=''){
			$CLIENT_ADDRESS1_LINE_2.=$street1;
		}

		if($city1!=''){
			$CLIENT_ADDRESS1_CITY.=$city1;
		}

		if($town1!=''){
			$CLIENT_ADDRESS1_TOWN.=$town1;
		}

		if($postcode1!=''){
			$CLIENT_ADDRESS1_POSTCODE.=$postcode1;
		}

		$CLIENT_ADDRESS1_LINE_1			= $CLIENT_ADDRESS1_LINE_1;
		$CLIENT_ADDRESS1_LINE_2			= $CLIENT_ADDRESS1_LINE_2;
		$CLIENT_ADDRESS1_CITY			= $CLIENT_ADDRESS1_CITY;
		$CLIENT_ADDRESS1_TOWN			= $CLIENT_ADDRESS1_TOWN;
		$CLIENT_ADDRESS1_POSTCODE		= $CLIENT_ADDRESS1_POSTCODE;

		$CLIENT_ADDRESS2_LINE_1 	= '';
		$CLIENT_ADDRESS2_LINE_2	= '';
		$CLIENT_ADDRESS2_CITY	= '';
		$CLIENT_ADDRESS2_TOWN	= '';
		$CLIENT_ADDRESS2_POSTCODE= '';

		$address_line12		= trim($row['GEMADD1']);
		$street2				= trim($row['GEMADD2']);
		$city2			= trim($row['GEMADD3']);
		$town2				= trim($row['GEMADD4']);
		$postcode2			= trim($row['GEMPSTCD']);

		if($address_line12!=''){
			$CLIENT_ADDRESS2_LINE_1.=$address_line12;
		}

		if($street2!=''){
			$CLIENT_ADDRESS2_LINE_2.=$street2;
		}

		if($city2!=''){
			$CLIENT_ADDRESS2_CITY.=$city2;
		}

		if($town2!=''){
			$CLIENT_ADDRESS2_TOWN.=$town2;
		}

		if($postcode2!=''){
			$CLIENT_ADDRESS2_POSTCODE.=$postcode2;
		}

		$CLIENT_ADDRESS2_LINE_1			= $CLIENT_ADDRESS2_LINE_1;
		$CLIENT_ADDRESS2_LINE_2			= $CLIENT_ADDRESS2_LINE_2;
		$CLIENT_ADDRESS2_CITY			= $CLIENT_ADDRESS2_CITY;
		$CLIENT_ADDRESS2_TOWN			= $CLIENT_ADDRESS2_TOWN;
		$CLIENT_ADDRESS2_POSTCODE		= $CLIENT_ADDRESS2_POSTCODE;

		$CLIENT_PHONE_1			= $phone_1;
		$CLIENT_PHONE_2			= $phone_2;
		$CLIENT_PHONE_3			= $phone_3;
		$CLIENT_PHONE_4			= $phone_4;
		$CLIENT_PHONE_5			= $phone_5;

		$CLIENT_MOBILE_1		= $mobile_1;
		$CLIENT_MOBILE_2		= $mobile_2;
		$CLIENT_MOBILE_3		= $mobile_3;
		$CLIENT_MOBILE_4		= $mobile_4;
		$CLIENT_MOBILE_5		= $mobile_5;

		$CLIENT_FAX_1			= $fax_1;
		$CLIENT_FAX_2			= $fax_2;
		$CLIENT_FAX_3			= $fax_3;
		$CLIENT_FAX_4			= $fax_4;
		$CLIENT_FAX_5			= $fax_5;

		$ACCOUNT_NAME= $row['GBKACTNAME'];

		$ACCOUNT_NUMBER= $row['GBKACT'];

		$ACCOUNT_SORTCODE= $row['GBKSORT'];

		$CLIENT_NOTES.= $row['GNOTES'];

		
		$CLIENT_CREATED_ON 		="null";
		

		$truncate_sql = "DELETE FROM cnb_jennings_winman.`clients` where `CLIENTID`='$CLIENTID'";
		$db_connect->queryExecute($truncate_sql);


	$sql = "INSERT INTO cnb_jennings_winman.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', $CLIENT_CREATED_ON , '0')";
	
	 $db_connect->queryExecute($sql);
	}
}
?>
