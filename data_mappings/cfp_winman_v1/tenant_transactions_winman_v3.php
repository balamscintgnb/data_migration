<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once '../../header_init.php';
require_once '../includes/config.php';


$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

 $query = "select * from invoice as c where (c.CompCode LIKE '%RNT%' OR c.CompCode LIKE '%ADM%' OR c.CompCode LIKE '%ADP%' OR c.CompCode LIKE '%DEP%' OR c.CompCode LIKE '%TEN%') ";

$query2 = 'show columns from tenant_transactions';
$data_1 = $db_connect_src->queryFetch($query);
$data2 = $db_connect_src->queryFetch($query2);
$data=json_decode($data_1);
$table_column = '';
foreach($data2 as $single_key) {
	$table_column .=$single_key['Field'].',';

}
$generated_column = trim($table_column,',');
$ct=1;
if(count($data)>0){

	foreach($data->data as $row){
		$row =(array) $row;

		$transactionid=$row['ID'];
		$LETTING_CUSTOM_REF_NO=	 $row['TYCode'];
		$PROPERTY_ID			=0;
		$PROPERTY_ROOM_ID			= 0;
		$TENANT_ID			=0;
		$SHARED_TENANT_IDS=json_encode(array());
		$DUE_DATE="NULL";
		$RENT_START = "NULL";
		$RENT_END = "NULL";
		$PAYMENT_DATE="NULL";
		if(strstr($row['TransDesc'], 'to') || strstr($row['TransDesc'], '-') || strstr($row['TransDesc'], 'rent')) {
			
			$formatted_desc = str_replace('.','/', $row['TransDesc']);
			$pattern ="/\d{1,2}\/\d{2}\/\d{2,4}/";
			if(preg_match_all($pattern,$formatted_desc, $matches )){

				if(isset($matches)){
					if(isset($matches[0][0]))
						$RENT_START = "'".convert_valid_format($matches[0][0])."'";
					if(isset($matches[0][1]))
						 $RENT_END = "'".convert_valid_format($matches[0][1])."'";
				}else { 
							$RENT_START = "NULL";
		$RENT_END = "NULL";
				}				
			}		
		}
		
		if($row['Date'] !=''){
			$PAYMENT_DATE=$DUE_DATE = "'".convert_valid_format($row['Date'])."'";
		}


		$DUE_AMOUNT=$row['Debit'];
		$PAID_AMOUNT=$row['Credit'];
		$TRANSACTION_STATUS='PAID';
		$TRANSACTION_NO='';
		if($row['TransRef'] != '')
		$TRANSACTION_NO=$row['TransRef'];

		$PAYMENT_METHOD='CASH';
		$type ='';
		if(strstr($row['CompCode'],'ADM'))
			$type = 'ADMIN';
		else if(strstr($row['CompCode'],'DEP') || strstr($row['CompCode'],'ADP'))
			$type = 'DEPOSIT';
		else if(strstr($row['CompCode'],'RNT'))
			$type = 'RENT';
		else if(strstr($row['CompCode'],'TEN'))
			$type = 'INVENTORY';

		$TRANSACTION_TYPE=$type; 

		$TRANSACTION_DESCRIPTION=trim($row['TransDesc']);
		$TRANSACTION_NOTES = "\n".'Invoice Date :'.trim($row['Date']);
		$sql_query = "insert into cnb_movehousing.`tenant_transactions3` VALUES (NULL,'$transactionid','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$PROPERTY_ROOM_ID',
		 '$TENANT_ID','$SHARED_TENANT_IDS',$RENT_START,$RENT_END, $DUE_DATE,$PAYMENT_DATE,
		'$DUE_AMOUNT','$PAID_AMOUNT','$TRANSACTION_STATUS','$TRANSACTION_TYPE','$TRANSACTION_NO','$PAYMENT_METHOD','$TRANSACTION_DESCRIPTION','$TRANSACTION_NOTES',0);";

	//	echo $sql_query;
	$db_connect_src->queryExecute($sql_query) or die($sql_query);

	}
}

function convert_valid_format($indate){ 
$indate_change1 = str_replace('-','.',$indate);
$indate_change2 = str_replace('/','.',$indate_change1);
if(strstr($indate_change2,':')){
	$indate_change =$indate_change2;
}else{
	$indate_change = $indate_change2.' 00:00:00';
}
$date = DateTime::createFromFormat('d.m.Y H:i:s', $indate_change);

if(checkdate($date->format("m"), $date->format("d"), $date->format("Y")))
return $date->format("Y-m-d H:i:s");
else 
return  "NULL";
}

	