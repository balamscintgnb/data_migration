<?php 

//require_once '../includes/config.php';

require_once '../../header_init.php';

$sql_details_src = array(
    'username' => $user_name,
    'password' => $password,
    'database'   => 'ds_cnb_stones_cfp_2',
    'host' => 'localhost'
);

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

//$query = 'select * from `host_worksheets`';

// $end = 10000;
$end = 600;
//$p=0;
$p = $_GET['p']??1;
$offset = ($p-1)*$end;


echo $query = "select ID,TYCODE,ItemDesc,ItemDate,InvoiceDate,ItemAmount,Received,InvRef,CompCode  from invoice where CompCode LIKE '%RNT%' OR CompCode LIKE '%ADM%' OR CompCode LIKE '%ADP%' OR CompCode LIKE '%DEP%' OR CompCode LIKE '%TEN%' limit $offset, $end";
// 

//echo $query = "select count(ID)  from invoice where (CompCode LIKE '%RNT%' OR CompCode LIKE '%ADM%' OR CompCode LIKE '%ADP%' OR CompCode LIKE '%DEP%' OR CompCode LIKE '%TEN%')";
// $count=73227;
// echo '<br/>';
$count=1612;

$page = ceil($count/$end);

$data = json_decode($db_connect_src->queryFetch($query),true);

// var_dump($data);

// exit;

//$query = "select * from invoice as c where (c.CompCode LIKE '%RNT%' OR c.CompCode LIKE '%ADM%' OR c.CompCode LIKE '%ADP%' OR c.CompCode LIKE '%DEP%' OR c.CompCode LIKE '%TEN%') ";

//$query = "select * from invoice where CompCode LIKE '%RNT%' OR CompCode LIKE '%ADM%' OR CompCode LIKE '%ADP%' OR CompCode LIKE '%DEP%' OR CompCode LIKE '%TEN%' limit $offset, $end ";

//$query = "select * from invoice where `ItemAmount` > 0 AND `Received` > 0 AND `InvoiceDate` is not null AND ( CompCode LIKE '%RNT%' OR CompCode LIKE '%ADM%' OR CompCode LIKE '%ADP%' OR CompCode LIKE '%DEP%' OR CompCode LIKE '%TEN%' ) limit $offset, $end ";

/*
$query2 = 'show columns from tenant_transactions';
$data_1 = $db_connect_src->queryFetch($query);
$data=json_decode($data_1);
$table_column = '';
*/

$i=0;

if(count($data['data'])>0){

	foreach($data['data'] as $row){

		//var_dump($row); exit;

		$row= (array)$row;
		$transactionid=$row['ID'];
		
		$LETTING_CUSTOM_REF_NO=	 $row['TYCODE'];
		$PROPERTY_ID			=0;
		$PROPERTY_ROOM_ID			= 0;
		$TENANT_ID			=0;
		$SHARED_TENANT_IDS=json_encode(array());
		$DUE_DATE="NULL";
		$RENT_START = "NULL";
		$RENT_END = "NULL";
		$PAYMENT_DATE="NULL";

		if(strstr($row['ItemDesc'], 'to') || strstr($row['ItemDesc'], '-') || strstr($row['ItemDesc'], 'rent')) {
			
			$formatted_desc = str_replace('.','/', $row['ItemDesc']);
			$pattern ="/\d{1,2}\/\d{2}\/\d{2,4}/";
			if(preg_match_all($pattern,$formatted_desc, $matches )){
				if(isset($matches)){
					if(isset($matches[0][0]))
						$RENT_START = "'".convert_valid_format($matches[0][0])."'";
					if(isset($matches[0][1]))
						 $RENT_END = "'".convert_valid_format($matches[0][1])."'";
				}else { 
					$RENT_START = "NULL";
					$RENT_END = "NULL";
				}				
			}		
		}

		/*if($row['Date'] !=''){
			$PAYMENT_DATE=$DUE_DATE = "'".convert_valid_format($row['Date'])."'";
		}*/
		
		if(trim($row['ItemDate']) !=''){
			$DUE_DATE = "'".$row['ItemDate']."'";
		}else if(trim($row['InvoiceDate']) !=''){
			$DUE_DATE = "'".$row['InvoiceDate']."'";
			$PAYMENT_DATE=$DUE_DATE;	
		}

		$DUE_AMOUNT=$row['ItemAmount'];
		$PAID_AMOUNT=$row['Received'];
		$TRANSACTION_STATUS='PAID';
		$TRANSACTION_NO='';

		if($row['InvRef'] != ''){
			$TRANSACTION_NO=$row['InvRef'];
		}

		/*
		$DUE_AMOUNT=$row['Debit'];
		$PAID_AMOUNT=$row['Credit'];
		$TRANSACTION_STATUS='PAID';
		$TRANSACTION_NO='';
		if($row['TransRef'] != '')
		$TRANSACTION_NO=$row['TransRef'];
		*/

		$PAYMENT_METHOD='CASH';

		if(strstr($row['CompCode'],'ADM'))
			$type = 'ADMIN';
		else if(strstr($row['CompCode'],'DEP') || strstr($row['CompCode'],'ADP'))
			$type = 'DEPOSIT';
		else if(strstr($row['CompCode'],'RNT'))
			$type = 'RENT';
		else if(strstr($row['CompCode'],'TEN'))
			$type = 'INVENTORY';
		else
			$type ='OTHERS';

		$TRANSACTION_TYPE=$type; 

		$TRANSACTION_DESCRIPTION=trim($row['ItemDesc']);

		$TRANSACTION_NOTES='';
		if(trim($row['InvoiceDate'])!=''){
			$TRANSACTION_NOTES = 'Invoice Date: '.trim($row['InvoiceDate']).'<br/>';
		}
		
		$DUE_DATE = trim($DUE_DATE);
		//$DUE_DATE = str_replace(array(,'','1971'),NULL, $DUE_DATE);
		
		//var_dump($DUE_DATE);
		
		//echo $DUE_DATE.'<br/>';
		
		if($DUE_DATE=="'0000-00-00 00:00:00'" || $DUE_DATE=='' || $DUE_DATE=="'1971-01-01 00:00:00'" ){
			$DUE_DATE='NULL';
		}
		
		//var_dump($DUE_DATE);
		// $DUE_DATE = str_replace('',NULL, $DUE_DATE); 
		//$TRANSACTION_NOTES = "\n".'Invoice Date :'.;

	    $sql_query = "insert into `tenant_transactions` VALUES (NULL,'$transactionid','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$PROPERTY_ROOM_ID',
		'$TENANT_ID','$SHARED_TENANT_IDS',$RENT_START,$RENT_END,$DUE_DATE,$PAYMENT_DATE,'$DUE_AMOUNT','$PAID_AMOUNT','$TRANSACTION_STATUS','$TRANSACTION_TYPE',
		'$TRANSACTION_NO','$PAYMENT_METHOD','$TRANSACTION_DESCRIPTION','$TRANSACTION_NOTES',0);";
//echo $sql_query.'<br/>';
	$db_connect->queryExecute($sql_query) or die($sql_query);

		echo 'Inserted '.++$i.'<br/>';
	 
	}
	$p++;
?>
<script type="text/javascript">
<?php 
if($p<=$page){
?>
function reload(){
	window.location='?p=<?=$p;?>&debug=1';
}
setTimeout(function() {
	reload();
}, 3000);
<?php
}
?>
</script>

<?php
}

function convert_valid_format($indate){ 

	$indate_change1 = str_replace('-','.',$indate);
	$indate_change2 = str_replace('/','.',$indate_change1);

	if(strstr($indate_change2,':')){
		$indate_change =$indate_change2;
	}else{
		$indate_change = $indate_change2.' 00:00:00';
	}
	$date = DateTime::createFromFormat('d.m.Y H:i:s', $indate_change);
	
	if(checkdate($date->format("m"), $date->format("d"), $date->format("Y")))
		return $date->format("Y-m-d H:i:s");
	else 
		return  null;
	}
?>	