<?php 
extract($_GET);
$data_mapping=true;
$isDevelopment=true;

if($error==1){
	error_reporting(E_ALL);
	ini_set('display_errors',1);
}

$port=3306;
$dstnDBName='cnb_move_housing_cfp';

require_once '../../header_init.php';

require_once '../includes/config.php';

$sql_details_src = array( 

    'username' => 'root',
    'password' => '',
    'database'   => 'cnb_move_housing_cfp',
    'host' => 'localhost:'.$port

);

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$p = $_GET['p']??0;
$offset = $p*5000;
$end = 5000;
$query = "select * from invoice where CompCode LIKE '%RNT%' OR CompCode LIKE '%ADM%' OR CompCode LIKE '%ADP%' OR CompCode LIKE '%DEP%' OR CompCode LIKE '%TEN%' limit $offset, $end ";
$query2 = 'show columns from tenant_transactions';
$data_1 = $db_connect_src->queryFetch($query);
$data=json_decode($data_1);
$table_column = '';

$ct=1;

if(count($data->data)>0){
	foreach($data->data as $row){
		$row= (array)$row;
		$transactionid=$row['ID'];
		$LETTING_CUSTOM_REF_NO=	 'B2_'.$row['TYCODE'];
		$PROPERTY_ID			=0;
		$PROPERTY_ROOM_ID			= 0;
		$TENANT_ID			=0;
		$SHARED_TENANT_IDS=json_encode(array());
		$DUE_DATE="NULL";
		$RENT_START = "NULL";
		$RENT_END = "NULL";
		$PAYMENT_DATE="NULL";
		if(strstr($row['ItemDesc'], 'to') || strstr($row['ItemDesc'], '-') || strstr($row['ItemDesc'], 'rent')) {
			
			$formatted_desc = str_replace('.','/', $row['ItemDesc']);
			$pattern ="/\d{1,2}\/\d{2}\/\d{2,4}/";
			if(preg_match_all($pattern,$formatted_desc, $matches )){
				if(isset($matches)){
					if(isset($matches[0][0]))
						$RENT_START = "'".convert_valid_format($matches[0][0])."'";
					if(isset($matches[0][1]))
						 $RENT_END = "'".convert_valid_format($matches[0][1])."'";
				}else { 
							$RENT_START = "NULL";
		$RENT_END = "NULL";
				}				
			}		
		}
		
		if($row['ItemDate'] !=''){
		$DUE_DATE = "'".$row['ItemDate']."'";
		}
		else if($row['InvoiceDate'] !=''){
		$DUE_DATE = "'".$row['InvoiceDate']."'";
		$PAYMENT_DATE=$DUE_DATE;			

		}

		$DUE_AMOUNT=$row['ItemAmount'];
		$PAID_AMOUNT=$row['Received'];
		$TRANSACTION_STATUS='PAID';
		$TRANSACTION_NO='';
		if($row['InvRef'] != '')
		$TRANSACTION_NO=$row['InvRef'];

		$PAYMENT_METHOD='CASH';

		if(strstr($row['CompCode'],'ADM'))
			$type = 'ADMIN';
		else if(strstr($row['CompCode'],'DEP') || strstr($row['CompCode'],'ADP'))
			$type = 'DEPOSIT';
		else if(strstr($row['CompCode'],'RNT'))
			$type = 'RENT';
		else if(strstr($row['CompCode'],'TEN'))
			$type = 'INVENTORY';
		else
			$type ='';

		$TRANSACTION_TYPE=$type; 

		$TRANSACTION_DESCRIPTION=trim($row['ItemDesc']);
		$TRANSACTION_NOTES = "\n".'Invoice Date :'.trim($row['InvoiceDate']);
	    $sql_query = "insert into cnb_movehousing.`tenant_transactions2` VALUES (NULL,'$transactionid','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$PROPERTY_ROOM_ID',
		 '$TENANT_ID','$SHARED_TENANT_IDS',$RENT_START,$RENT_END, $DUE_DATE,$PAYMENT_DATE,
		'$DUE_AMOUNT','$PAID_AMOUNT','$TRANSACTION_STATUS','$TRANSACTION_TYPE','$TRANSACTION_NO','$PAYMENT_METHOD','$TRANSACTION_DESCRIPTION','$TRANSACTION_NOTES',0);";
	 $db_connect_src->queryExecute($sql_query) or die($sql_query);

	}
}

function convert_valid_format($indate){ 
$indate_change1 = str_replace('-','.',$indate);
$indate_change2 = str_replace('/','.',$indate_change1);
if(strstr($indate_change2,':')){
	$indate_change =$indate_change2.'.000000';
}else{
	$indate_change = $indate_change2.' 00:00:00';
}

$date = DateTime::createFromFormat('d.m.Y h:i:s', $indate_change);

if(checkdate($date->format("m"), $date->format("d"), $date->format("Y")))
return $date->format("Y-m-d H:i:s");

}
?>
<script>
window.location.reload= window.location+'?p=<?= $p+1;?>';

</script>	