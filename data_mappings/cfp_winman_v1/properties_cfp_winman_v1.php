<?php
/*error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';*/

$data_mapping = true;

require_once '../../header_init.php';

$sql_details_src = array(
    'username' => $user_name,
    'password' => $password,
    'database'   => 'ds_cnb_move',
    'host' => 'localhost'
);

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = 'select * from host_prop_inf as p1 left join host_prop_inf2 as p2 on p1.PCODE=p2.PCODE';

$data = json_decode($db_connect_src->queryFetch($query),true);

if(count($data)>0){

	foreach($data['data'] as $row){

		/*echo '<pre>';
		print_r($row);
		echo '</pre>';

		exit;*/

		$PROPERTY_ID				= $row['PCODE'];

		$PROPERTY_REF_ID			= '';

		$PROPERTY_VENDOR_ID			= $row['PLCODE'];

		$PROPERTY_STAFF_ID			= 0;

		$PROPERTY_TITLE				= '';
		$PROPERTY_SHORT_DESCRIPTION	= $row['SHORT ADVERT'];
		$PROPERTY_DESCRIPTION		= '';

		$PROPERTY_CLASSIFICATION	= '';

		$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';

		$PROPERTY_PRICE				= (int)$row['PRICE'];
		$PROPERTY_QUALIFIER			= '';
		$PROPERTY_PRICE_FREQUENCY	= '';
		$FURNISHING='';

		$rent_frequency				= '';
		if($PROPERTY_PRICE>0){
			$rent_frequency 		= $row['cPriceStatus'];
		}

		/*$rent_frequency 		= $item_array[$row['PANTRT']];
		$rent_frequency 		= $item_array[$row['PLETPCT']];
		$rent_frequency 		= $item_array[$row['PLETFEE']];*/

		$PROPERTY_TITLE 		= $row['PTITLE'];

		$short_desc 		= $row['PSHORT'];
		$medium_desc 		= $row['PMED'];
		$long_desc 		= $row['PLONG'];

		if($short_desc!=''){
			$PROPERTY_DESCRIPTION.=$short_desc;
		}

		if($medium_desc!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.=$medium_desc;
		}

		if($long_desc!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.=$medium_desc;
		}

		if($PROPERTY_TITLE!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.=$PROPERTY_TITLE;
		}

		if($row['PGASINV']!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.='<b>Gas Inventory: </b>'.$row['PGASINV'];
		}

		if($row['PGASCHK']!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.='<b>GAS Check: </b>'.$row['PGASCHK'];
		}

		if($row['PADVERT']!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.=$row['PADVERT'];
		}

		if($row['PCONCOUNC']!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='<br/><br/>';
			}
			$PROPERTY_DESCRIPTION.='<b>Council: </b>'.$row['PCONCOUNC'];
		}

		if(stristr($rent_frequency, 'pcm')){
			$PROPERTY_PRICE_FREQUENCY	= 'PCM';
		}else if(stristr($rent_frequency, 'pw')){
			$PROPERTY_PRICE_FREQUENCY	= 'PW';
		}
		$PROPERTY_AVAILABLE_DATE	= "NULL";
		if($row['PAVAIL'] != null)
		$PROPERTY_AVAILABLE_DATE	= "'".date('Y-m-d', strtotime($row['PAVAIL']))."'";

		$PROPERTY_ADDRESS_LINE_1	= '';
		$PROPERTY_ADDRESS_LINE_2	= '';
		$PROPERTY_ADDRESS_CITY		= '';
		$PROPERTY_ADDRESS_COUNTY	= '';
		$PROPERTY_ADDRESS_POSTCODE	= '';

		$address_line1			= $row['PADD1'];
		$address_line2		= $row['PADD2'];
		$address_line3				= $row['PADD3'];
		$address_line4			= $row['PADD4'];
		$address_line5				= $row['PADD5'];
		$postcode			= $row['PPSTCD'];

		$is_property_vacant			= (int)$row['PVACANT'];

		$PROPERTY_CURRENT_OCCUPANT='';
		if($is_property_vacant){
			$PROPERTY_CURRENT_OCCUPANT='FALSE';
		}

		$PROPERTY_FORMATTED_ADDRESS='';

		if($address_line1!=''){
			$PROPERTY_ADDRESS_LINE_1.=$address_line1;
		}

		if($address_line2!=''){
			$PROPERTY_ADDRESS_LINE_2.=$address_line2;
			$PROPERTY_FORMATTED_ADDRESS=$address_line2;
		}

		if($address_line3!=''){
			$PROPERTY_ADDRESS_CITY.=$address_line3;
			if($PROPERTY_FORMATTED_ADDRESS!=''){
				$PROPERTY_FORMATTED_ADDRESS.=', ';
			}
			$PROPERTY_FORMATTED_ADDRESS=$address_line3;
		}

		if($address_line4!=''){
			$PROPERTY_ADDRESS_COUNTY.=$address_line4;
			if($PROPERTY_FORMATTED_ADDRESS!=''){
				$PROPERTY_FORMATTED_ADDRESS.=', ';
			}
			$PROPERTY_FORMATTED_ADDRESS=$address_line4;
		}else if($address_line5!=''){
			if($PROPERTY_FORMATTED_ADDRESS!=''){
				$PROPERTY_FORMATTED_ADDRESS.=', ';
			}
			$PROPERTY_FORMATTED_ADDRESS=$address_line5;
		}

		if($address_line5!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$address_line5;
		}

		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}

		$PROPERTY_STATUS			= 'ARCHIVED';
		$PROPERTY_AVAILABILITY		= '';

		$PROPERTY_ADMIN_FEES		= $row['PMANPCT'];

		if($row['Match2'] != 'Unknown')
		$PROPERTY_TYPE				= $row['Match2'];
		else 
		$PROPERTY_TYPE				= '';
		
		$PROPERTY_BEDROOMS			= $row['Numeric1'];
		$PROPERTY_BATHROOMS			= $row['Numeric2'];
		$PROPERTY_RECEPTION			= $row['Numeric3'];
		$PROPERTY_TENURE			= '';

		$KITCHEN_DINER				= 'FALSE';
		$OFF_ROAD_PARKING			= 'FALSE';
		$ON_ROAD_PARKING			= 'FALSE';
		$GARDEN						= 'FALSE';
		$WHEELCHAIR_ACCESS			= 'FALSE';
		$ELEVATOR_IN_BUILDING		= 'FALSE';
		$POOL						= 'FALSE';
		$GYM						= 'FALSE';
		$KITCHEN					= 'FALSE';
		$DINING_ROOM				= 'FALSE';
		$FURNISHED					= 'FALSE';
		$INTERNET					= 'FALSE';
		$WIRELESS_INTERNET			= 'FALSE';
		$TV							= 'FALSE';
		$WASHER						= 'FALSE';
		$DRYER						= 'FALSE';
		$DISHWASHER					= 'FALSE';
		$PETS_ALLOWED				= 'FALSE';
		$FAMILY_OR_CHILD_FRIENDLY	= 'FALSE';
		$DSS_ALLOWED				= 'FALSE';
		$SMOKING_ALLOWED			= 'FALSE';
		$SECURITY					= 'FALSE';
		$HOT_TUB					= 'FALSE';
		$CLEANER					= 'FALSE';
		$EN_SUITE					= 'FALSE';
		$SECURE_CAR_PARKING			= 'FALSE';
		$OPEN_PLAN_LOUNGE			= 'FALSE';
		$VIDEO_DOOR_ENTRY			= 'FALSE';
		$CONCIERGE_SERVICES			= 'FALSE';

		$features_array				= array();

		for($i=1;$i<=10;$i++){
			$this_value = $row['Fac'.$i];
			Utility::clearValues($this_value);
			if($this_value!=''){
				$features_array[]=$this_value;
			}
		}

		if($row['PSTATUS']!=''){

			if(stristr($row['PSTATUS'], 'available') || stristr($row['PSTATUS'], 'let only') || stristr($row['PSTATUS'], 'agent')){
				$PROPERTY_AVAILABILITY	= 'AVAILABLE';
			}

			if(stristr($row['PSTATUS'], 'managed')){
				$PROPERTY_AVAILABILITY	= 'LET';
			}
		}

    $FURNISHING=$row['Match3'];

		$FURNISHING_STATUS='';

		if(stristr($FURNISHING,'WHITE GOODS') || stristr($FURNISHING,'YES')){
			 $FURNISHING_STATUS					= 'Furnished';
		}else if(stristr($FURNISHING,'NO') || stristr($FURNISHING,'Unknown')){
				$FURNISHING_STATUS					= 'Unfurnished';
		}else if(stristr($FURNISHING,'PART')){
				$FURNISHING_STATUS					= 'Part Furnished';
		}else{
				$FURNISHING_STATUS='';
		}

		if(stristr($row['Match4'],'ECH') || stristr($row['Match4'],'ECON7') || stristr($row['Match4'],'GCH')){
			$FAMILY_OR_CHILD_FRIENDLY					= 'TRUE';
		}


		if(stristr($row['Match5'],'YES')){
			$SMOKING_ALLOWED					= 'TRUE';
		}

		if(stristr($row['Match6'],'YES') || stristr($row['Match6'],'NEG')){
			$PETS_ALLOWED					= 'TRUE';
		}

		if(stristr($row['Match7'],'LARGE') || stristr($row['Match7'],'MEDIUM') || stristr($row['Match7'],'PATIO') || stristr($row['Match7'],'SMALL')){
			$GARDEN					= 'TRUE';
		}

		if(stristr($row['Match8'],'YES')){
			$DSS_ALLOWED					= 'TRUE';
		}

		if(stristr($row['Match9'],'DRIVEWAY') || stristr($row['Match9'],'GARAGE')){
			$SECURE_CAR_PARKING					= 'TRUE';
		}

		if(stristr($row['Match9'],'ALLOCATED') || stristr($row['Match9'],'STREET')){
			$ON_ROAD_PARKING					= 'TRUE';
		}

		///

		if(array_search('pool', $features_array)){
			$POOL					= 'TRUE';
		}

		if(array_search('concierge', $features_array)){
			$CONCIERGE_SERVICES		= 'TRUE';
		}

		if(array_search('garden', $features_array)){
			$GARDEN					= 'TRUE';
		}

		if(array_search('lift', $features_array)){
			$ELEVATOR_IN_BUILDING	= 'TRUE';
		}

		if(array_search('security', $features_array)){
			$SECURITY				= 'TRUE';
		}

		if(array_search('gym', $features_array)){
			$GYM				= 'GYM';
		}

		if(array_search('internet', $features_array)){
			$INTERNET = 'TRUE';
		}

		if(array_search('parking', $features_array)){
			$OFF_ROAD_PARKING		= 'TRUE';
		}

		if(array_search('EN-SUITE', $features_array)){
			$EN_SUITE		= 'TRUE';
		}

		$PROPERTY_SOURCE ='CFP_WINMAN';
		$PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$row['EnergyRatingCurrent'], "energy_potential"=> $row['EnergyRatingPotential'], "co2_current"=>$row['CO2RatingCurrent'], "co2_potential"=> $row['CO2RatingPotential']));

		$PROPERTY_ASSETS			= json_encode(array());
		$PROPERTY_CUSTOM_FEATURES	= json_encode($features_array);
		$PROPERTY_ROOMS				= json_encode(array());

		$PROPERTY_CREATED_ON		= date('Y-m-d H:i:s', strtotime($row['PADDED']));

		$sql = "INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_SOURCE`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_SOURCE','$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE, '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHING_STATUS', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON')";
		//echo $sql.'<br/>';
		$db_connect->queryExecute($sql) or die($sql);
	}
}

echo 'Done';
?>