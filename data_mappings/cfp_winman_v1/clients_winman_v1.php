<?php
$data_mapping = true;

require_once '../../header_init.php';

/*
//joins for multiple tables check
//tenancies
SELECT * FROM cnb_stones2_cfp1.`host_ten_inf` as c1 inner join cnb_stones2_cfp2.`host_ten_inf` as c2 on c1.TYCODE=c2.TYCODE
//properties
SELECT * FROM cnb_stones2_cfp1.`host_prop_inf` as c1 inner join cnb_stones2_cfp2.`host_prop_inf` as c2 on c1.PCODE=c2.PCODE
//landlord
SELECT * FROM cnb_stones2_cfp1.`host_land_inf` as c1 inner join cnb_stones2_cfp2.`host_land_inf` as c2 on c1.LCODE=c2.LCODE
//guarantors
SELECT * FROM cnb_stones2_cfp1.`host_garant` as c1 inner join cnb_stones2_cfp2.`host_garant` as c2 on c1.GCODE=c2.GCODE
//supplier
SELECT * FROM cnb_stones2_cfp1.`host_contract` as c1 inner join cnb_stones2_cfp2.`host_contract` as c2 on c1.CCODE=c2.CCODE
//tenants
SELECT * FROM cnb_stones2_cfp1.`host_tenants` as c1 inner join cnb_stones2_cfp2.`host_tenants` as c2 on c1.TCODE=c2.TCODE
*/

/*$sql_details_src = array(
    'username' => 'root',
    'password' => 'GmeNz8Y?>h-m-',
    'database'   => 'ds_cnb_stones_cfp_1',
    'host' => 'localhost'
);*/

/*$sql_details_src = array( 
    'username' => 'root',
    'password' => '',
    'database'   => 'move_housing_bk',
    'host' => 'localhost:3307'
);*/

/*
host_tenants - tenants/applicants - search criteria by category
host_contract
host_garant
host_land_inf
host_worksheets
host_prop_inf - PNegotiator 
host_prop_inf2
host_ten_inf
invoice
*/
$sql_details_src = array(
    'username' => $user_name,
    'password' => $password,
    'database'   => 'ds_cnb_move',
    'host' => 'localhost'
);

$db_utility = new Utility;
$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = 'select * from `host_tenants`';
$data = json_decode($db_connect_src->queryFetch($query),true);

/*echo '<pre>';
print_r($data);
echo '</pre>';

exit;*/

if(count($data)>0){
$sno=1;
	foreach($data['data'] as $row){
		$CLIENT_STAFF_ID='';
		$CLIENT_TYPE			= 'TENANT';
		$CLIENT_SUB_TYPE		= '';

		$CLIENTID				= $row['TCODE'];
		$CLIENT_TENANCY_ID				= $row['TTYCODE'];

		$CLIENT_TITLE			= '';
		$CLIENT_NAME			= $row['TNAME'];
		$CLIENT_NAME			= $CLIENT_NAME;

		$CLIENT_STATUS = 'ACTIVE';
		if(stristr($row['TSTATUS'], 'archive') || stristr($row['TSTATUS'], 'expire')){
			$CLIENT_STATUS = 'INACTIVE';
		}

		$CLIENT_PRIMARY_PHONE='';

		$CLIENT_PRIMARY_EMAIL='';

		$email1					= trim($row['TE-MAIL']);


		$CLIENT_NOTES			= '';
		$CLIENT_PRIMARY_PHONE	= $row['TTELMOBL'];

		if($db_utility->validate_email_id($email1)){
			$CLIENT_PRIMARY_EMAIL	= $email1;
		}
		else{
			$CLIENT_NOTES			= $email1.' ';
		}

		// $CLIENT_EMAIL_1			= $row['email2'];
		$CLIENT_EMAIL_1			= '';
		$CLIENT_EMAIL_2			= '';
		$CLIENT_EMAIL_3			= '';
		$CLIENT_EMAIL_4			= '';
		$CLIENT_EMAIL_5			= '';

		$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';


		$phone_1 				= $row['TTELHOME'];
		$phone_2 				= $row['TTELWK1'];
		$phone_3 				= $row['TTELWK2'];

		$fax_1= $row['TTELFAX'];


		$CLIENT_ADDRESS_LINE_1 	= '';
		$CLIENT_ADDRESS_LINE_2	= '';
		$CLIENT_ADDRESS_CITY	= '';
		$CLIENT_ADDRESS_TOWN	= '';
		$CLIENT_ADDRESS_POSTCODE= '';

		$address_line1		= trim($row['TADD1']);
		$street				= trim($row['TADD2']);
		$city			= trim($row['TADD3']);
		$town				= trim($row['TADD4']);
		$postcode			= trim($row['TPSTCD']);

		if($address_line1!=''){
			$CLIENT_ADDRESS_LINE_1.=$address_line1;
		}

		if($street!=''){
			$CLIENT_ADDRESS_LINE_2.=$street;
		}

		if($city!=''){
			$CLIENT_ADDRESS_CITY.=$city;
		}

		if($town!=''){
			$CLIENT_ADDRESS_TOWN.=$town;
		}

		if($postcode!=''){
			$CLIENT_ADDRESS_POSTCODE.=$postcode;
		}

		$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
		$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
		$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
		$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
		$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

		$CLIENT_PHONE_1			= $phone_1;
		$CLIENT_PHONE_2			= $phone_2;
		$CLIENT_PHONE_3			= $phone_3;
		$CLIENT_PHONE_4			= $phone_4;
		$CLIENT_PHONE_5			= $phone_5;

		$CLIENT_MOBILE_1		= $mobile_1;
		$CLIENT_MOBILE_2		= $mobile_2;
		$CLIENT_MOBILE_3		= $mobile_3;
		$CLIENT_MOBILE_4		= $mobile_4;
		$CLIENT_MOBILE_5		= $mobile_5;

		$CLIENT_FAX_1			= $fax_1;
		$CLIENT_FAX_2			= $fax_2;
		$CLIENT_FAX_3			= $fax_3;
		$CLIENT_FAX_4			= $fax_4;
		$CLIENT_FAX_5			= $fax_5;
		
		if($row['TNOTES']!=''){
			$CLIENT_NOTES.= $row['TNOTES'].'<br/>';
		}
		if($row['TUSERNT1']!=''){
			$CLIENT_NOTES.= $row['TUSERNT1'].'<br/>';
		}
		if($row['TUSERNT2']!=''){
			$CLIENT_NOTES.= $row['TUSERNT2'].'<br/>';
		}
		if($row['TUSERNT3']!=''){
			$CLIENT_NOTES.= $row['TUSERNT3'].'<br/>';
		}
		if($row['TUSERNT4']!=''){
			$CLIENT_NOTES.= $row['TUSERNT4'].'<br/>';
		}
		$CLIENT_CREATED_ON='NULL';
		if($row['TADDED']!=''){
			$CLIENT_CREATED_ON 		= date('Y-m-d',strtotime($row['TADDED']));
		}
		
		if($CLIENT_NAME!=''){
			$sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON', '0')";
		
			$db_connect->queryExecute($sql) or die($sql);
		}else{
			echo "failed <br/>";
			$sno++;
		}
	}
	echo $sno;
}
?>
