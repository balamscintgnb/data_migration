<?php 
Class DB_test { 

      private $_host = 'localhost:3307';
      private $_username = 'root';
      private $_password = '';
      
      protected $connection;
      
      public function __construct($db)
      {
          if (!isset($this->connection)) {
              
              $this->connection= new mysqli($this->_host, $this->_username, $this->_password, $db);
              if (!$this->connection) {
                  echo 'Cannot connect to database server';
                  exit;
              }            
          }    
          
          return $this->connection;
      }

      public function execute($query){ 
          
        $result = $this->connection->query($query);
        
        if ($result == false) {
           
            return false;
        } else {
            return $result;
        }        

      }
      public function escape($value){ 

        return $this->connection->real_escape_string($value);

      }

      function validate_email($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		    return true;
		} else {
			return false;
		}
    }
  
  }
?>