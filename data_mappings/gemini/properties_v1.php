<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);
include 'datamapping_config.php';
$_database1 = 'cnb_sonia';
$_database2 = 'sonia_sql_bk';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);


if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*100;
}
else{
	$p=0;
	$page = 0;
}

$query = $db2->execute("SELECT * FROM `property` WHERE 1 LIMIT $page, 100");
if($query->num_rows>0){
while($row = $query->fetch_array()){



  $PROPERTY_ID				= $row['Reference'];
  $PROPERTY_REF_ID			= $row['Reference'];
  $PROPERTY_VENDOR_ID			= $row['Landlord Ref'];
  $PROPERTY_STAFF_ID			= '';
  $PROPERTY_TITLE				= '';
  $PROPERTY_SHORT_DESCRIPTION	= '';
  $PROPERTY_DESCRIPTION		= $db2->escape($row['Description']);

  $PROPERTY_CATEGORY			= '';
  $recordtype					=  '433';
  $category					= '41';

  $PROPERTY_CLASSIFICATION	= '';

  if($recordtype==433 && $category==41){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
  }
  if($recordtype==301){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
	  $PROPERTY_CLASSIFICATION= 'New Build';
  }
  else if($recordtype==23 && $category==41){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
  }
  else if($recordtype==23 && $category==43){
	  $PROPERTY_CATEGORY		= 'COMMERCIAL SALES';
  }
  else if($recordtype==24 && $category==41){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
  }
  else if($recordtype==24 && $category==43){
	  $PROPERTY_CATEGORY		= 'COMMERCIAL LETTINGS';
  }
  else if($recordtype==26 && $category==41){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
  }
  else if($recordtype==26 && $category==43){
	  $PROPERTY_CATEGORY		= 'COMMERCIAL AUCTION';
  }
  else if($recordtype==23 && $category==0){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
  }
  else if($recordtype==24 && $category==0){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
  }
  else if($recordtype==26 && $category==0){
	  $PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
  }


  $PROPERTY_PRICE				= $row['Asking Rent PCM'];
  $PROPERTY_QUALIFIER			= '';
if($row['Asking Rent PCM'] > 1)
  $PROPERTY_PRICE_FREQUENCY	= 'PCM';
else 
$PROPERTY_PRICE_FREQUENCY	= '';

  $PROPERTY_AVAILABLE_DATE	= "NULL";


  $PROPERTY_ADDRESS_LINE_1		=   $PROPERTY_ADDRESS_LINE_2		=  $PROPERTY_ADDRESS_CITY			='';
  $PROPERTY_ADDRESS_COUNTY		=  $PROPERTY_ADDRESS_POSTCODE		= '';
  $street				=	$locality	= 	$area =	$town= 	$county	= $postcode	= '';


	if($row['Address6'] !='')
	{
	$street				= trim($row['Address1']);
	$locality			= trim($row['Address2']);
	$area				= trim($row['Address3']);
	$town				= trim($row['Address4']);
	$county				= trim($row['Address5']);
	$postcode			= trim($row['Address6']);
	}
	else if($row['Address5'] !='')	{
	$street				= trim($row['Address1']);
	$locality			= trim($row['Address2']);
	$area				= trim($row['Address3']);
	$town				= trim($row['Address4']);
	$postcode			= trim($row['Address5']);
	}
	else if($row['Address4'] !='')	{
	$street				= trim($row['Address1']);
	$locality			= trim($row['Address2']);
	$area				= trim($row['Address3']);
	$postcode			= trim($row['Address4']);
	}
	else if($row['Address3'] !='')	{
	$street				= trim($row['Address1']);
	$locality			= trim($row['Address2']);
	$postcode			= trim($row['Address3']);
	}
	else if($row['Address2'] !='')	{
	$street				= trim($row['Address1']);
	$postcode			= trim($row['Address2']);
	}
	else if($row['Address1'] !='')	{
	$street				= trim($row['Address1']);
	}


	if($street!=''){
		$PROPERTY_ADDRESS_LINE_1=$street;
	}

	if($locality!=''){
		$PROPERTY_ADDRESS_CITY.=$locality;
	}

	if($area!=''){
		if($PROPERTY_ADDRESS_CITY!=""){
			$PROPERTY_ADDRESS_CITY.=', ';
		}
		$PROPERTY_ADDRESS_CITY.=$area;
	}

	if($town!=''){
		$PROPERTY_ADDRESS_COUNTY.=$town;
	}

	if($county!=''){
		if($PROPERTY_ADDRESS_COUNTY!=""){
			$PROPERTY_ADDRESS_COUNTY.=', ';
		}
		$PROPERTY_ADDRESS_COUNTY.=$county;
	}

	if($postcode!=''){
		$PROPERTY_ADDRESS_POSTCODE.=$postcode;
	}
	$PROPERTY_ADDRESS_LINE_1		= $db2->escape($PROPERTY_ADDRESS_LINE_1);
		$PROPERTY_ADDRESS_LINE_2		= $db2->escape($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY			= $db2->escape($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY		= $db2->escape($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE		= $db2->escape($PROPERTY_ADDRESS_POSTCODE);

		$PROPERTY_FORMATTED_ADDRESS	= '';
		//if($row['sys_status']==117){
			$PROPERTY_STATUS			= 'ARCHIVED';
			
			
 	if($row['LetOnly'] == 1) 
		  $PROPERTY_AVAILABILITY		= 'Let';
	else 
		$PROPERTY_AVAILABILITY		= 'AVAILABLE';
			
		
		$PROPERTY_ADMIN_FEES		= '';


		// ;
	
		$PROPERTY_TYPE				='';
		$PROPERTY_BEDROOMS			= $row['BedroomNumber'];
		$PROPERTY_BATHROOMS			= $row['Bathrooms'];
		$PROPERTY_RECEPTION			= $row['Receptions'];
		$PROPERTY_TENURE			= '';
		$PROPERTY_CURRENT_OCCUPANT	= '';

		$KITCHEN_DINER				= 'FALSE';
		$OFF_ROAD_PARKING			= 'FALSE';
		$ON_ROAD_PARKING			= 'FALSE';
		$GARDEN						= 'FALSE';
		$WHEELCHAIR_ACCESS			= 'FALSE';
		$ELEVATOR_IN_BUILDING		= 'FALSE';
		$POOL						= 'FALSE';
		$GYM						= 'FALSE';
		$KITCHEN					= 'FALSE';
		$DINING_ROOM				= 'FALSE';
		$FURNISHED					= 'FALSE';
		$INTERNET					= 'FALSE';
		$WIRELESS_INTERNET			= 'FALSE';
		$TV							= 'FALSE';
		$WASHER						= 'FALSE';
		$DRYER						= 'FALSE';
		$DISHWASHER					= 'FALSE';
		$PETS_ALLOWED				= 'FALSE';
		$FAMILY_OR_CHILD_FRIENDLY	= 'FALSE';
		$DSS_ALLOWED				= 'FALSE';
		$SMOKING_ALLOWED			= 'FALSE';
		$SECURITY					= 'FALSE';
		$HOT_TUB					= 'FALSE';
		$CLEANER					= 'FALSE';
		$EN_SUITE					= 'FALSE';
		$SECURE_CAR_PARKING			= 'FALSE';
		$OPEN_PLAN_LOUNGE			= 'FALSE';
		$VIDEO_DOOR_ENTRY			= 'FALSE';
		$CONCIERGE_SERVICES			= 'FALSE';

		$feature_1					= '';
		$feature_2					= '';
		$feature_3					= '';
		$feature_4					= '';
		$feature_5					= '';
		$feature_6					= '';
		$feature_7					= '';
		$feature_8					= '';
		$feature_9					= '';
		$feature_10					= '';

		$features_array				= array();
		if($feature_1!=""){
			$features_array[] = $feature_1;
		}

		if($feature_2!=""){
			$features_array[] = $feature_2;
		}

		if($feature_3!=""){
			$features_array[] = $feature_3;
		}

		if($feature_4!=""){
			$features_array[] = $feature_4;
		}

		if($feature_5!=""){
			$features_array[] = $feature_5;
		}

		if($feature_6!=""){
			$features_array[] = $feature_6;
		}

		if($feature_7!=""){
			$features_array[] = $feature_7;
		}

		if($feature_8!=""){
			$features_array[] = $feature_8;
		}

		if($feature_9!=""){
			$features_array[] = $feature_9;
		}

		if($feature_10!=""){
			$features_array[] = $feature_10;
		}


		

		if($row['internet']==1){
			$INTERNET = 'TRUE';
		}

		$outside_features			= $item_array[$row['outside']];
		if(strstr($outside_features,'garden')){
			$GARDEN					= 'TRUE';
		}

		if(strstr($outside_features,'parking')){
			$OFF_ROAD_PARKING		= 'TRUE';
		}

		$PROPERTY_ASSETS			= array();
		$PROPERTY_ASSETS			= $db2->escape(json_encode($PROPERTY_ASSETS));


		$custom_features			= $row['bullets'];
		$custom_features_array		= array();
		if($custom_features!=""){
			$custom_features_array	= explode("\n", $custom_features);;
		}

		$PROPERTY_CUSTOM_FEATURES	= $custom_features_array;
		$PROPERTY_ROOMS				= isset($rooms[$PROPERTY_ID])? $rooms[$PROPERTY_ID]:array();

		$PROPERTY_CUSTOM_FEATURES	= $db2->escape(json_encode($PROPERTY_CUSTOM_FEATURES));
		$PROPERTY_ROOMS				= $db2->escape(json_encode($PROPERTY_ROOMS));

		$PROPERTY_CREATED_ON		= $row['PropertyAdded'];

		$CERTIFICATE_EXPIRE_DATE = '';
		  ////////////////////////EXPIRE DATE ///////////////////
		  $Gas_certificate_expiry_date=$row['GasSafetyNextDue'];
		  $EPC_expiry_date=$row['EPCExpiryDate'];
		  $Legionella_risk_assessment_date=$row['LegionellaNextDue'];
		  $Smoke_CO_alarm_expiry_date=$row['SmokeAlarmsDue'];
  
		  $CERTIFICATE_DATE = array('1'=>$Gas_certificate_expiry_date,
			  '4'=>$EPC_expiry_date, 
			  '6'=>$Legionella_risk_assessment_date, 
			  '7'=>$Smoke_CO_alarm_expiry_date);
  
			  $CERTIFICATE_EXPIRE_DATE = json_encode($CERTIFICATE_DATE);


		$sql = "INSERT INTO `properties`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_CREATED_ON`, `PROPERTY_EPC_VALUES`,`CERTIFICATE_EXPIRE_DATE`, `RECORD_UPLOADED`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE, '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_CREATED_ON', '', '$CERTIFICATE_EXPIRE_DATE', '0')";
		$db1->execute($sql) or die($sql);



}
echo $p;
?>
<script>
window.location="properties_v1.php?p=<?= $p+1;?>";
</script>
<?php
} 
else { 
	echo "END ";
}
?>
