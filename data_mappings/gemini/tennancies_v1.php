<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);
include 'datamapping_config.php';
$_database1 = 'cnb_sonia';
$_database2 = 'sonia_sql_bk';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);


if(isset($_GET['p']) && $_GET['p']!=NULL){
	$p = $_GET['p'];
	$page = $p*100;
}
else{
	$p=0;
	$page = 0;
}

$query = $db2->execute("SELECT * FROM `tenancy` inner join `tenant` on `tenancy`.ID = `tenant`.TenancyID WHERE 1 LIMIT $page, 100");
if($query->num_rows>0){
while($row = $query->fetch_array()){

        $TENANCY_NOTES=NULL;

        $TENANCY_END_DATE =  $TENANCY_START_DATE	=   $TENANCY_CURRENT_START_DATE	="NULL";
        $TENANCY_CODE			= 'T_'.$row['ID'];
        $PROPERTY_REF_ID			= $row['PropertyID'];
        $PROPERTY_LOCK			= NULL;
        //$TENANCY_ID		= $row['TCODE'];
        $TENANCY_STATUS			= $row['Status'];
        if($row['StartDate']!=NULL)
        $TENANCY_START_DATE			= "'".$row['StartDate']."'";

        $TENANCY_RECEIPT_TYPE			= '';
        $TENANCY_RENEW_TYPE			= '';
        $TENANCY_PERIODIC_TYPE			= $row['Periodic'];
        if($row['EndDate']!=NULL)
        $TENANCY_END_DATE			= "'".$row['EndDate']."'";
        else 
        $TENANCY_END_DATE			= "'0000-00-00'";

        $TENANCY_VACATING_TYPE			= $row['EndDate'];
        $TENANCY_INSPECTION_DATE			= '';
        $TENANCY_INSPECTION_DATE_PERIOD			=  '';
        $TENANCY_CONTRACT_TERM_MONTH			= '';
        //$TENANCY_CONTRACT_TERM_YEAR			= $row['TYTERMYR'];
        $TENANCY_GAS_IN			= NULL;
        $TENANCY_GAS_OUT		= NULL;
        $TENANCY_ELE_IN			= NULL;
        $TENANCY_ELE_OUT	= NULL;
        $TENANCY_WATR_IN		= NULL;
        $TENANCY_WATR_OUT		= NULL;
        $TENANCY_SEW_IN		= NULL;
        $TENANCY_SEW_OUT		= NULL;
        $TENANCY_ELE_SUPPLIER	= NULL;
        $TENANCY_GAS_SUPPLIER	= NULL;
        $TENANCY_WATER_SUPPLIER	= NULL;
        $TENANCY_RENT			= $row['Rent'];
        $TENANCY_PERIOD			= $row['Rent Period'];
        $TENANCY_PERIOD_WORDS			= $row['Rent Period'];

        $TENANCY_RENT_PAYMENT_FREQUENCY=NULL;

        if($TENANCY_PERIOD_WORDS!=NULL){

            if($TENANCY_PERIOD_WORDS=='Four Week' || $TENANCY_PERIOD_WORDS=='Four Weekly'){
                $TENANCY_PERIOD_WORDS='p4w';
                $TENANCY_RENT_PAYMENT_FREQUENCY='four_weekly';
            }else if($TENANCY_PERIOD_WORDS=='Half Year' || $TENANCY_PERIOD_WORDS=='Half Yearly'){
                $TENANCY_PERIOD_WORDS='p6m';
                $TENANCY_RENT_PAYMENT_FREQUENCY='semi_annually';
            }else if(stristr($TENANCY_PERIOD_WORDS, 'month') || stristr($TENANCY_PERIOD_WORDS, 'Monthly')){
                $TENANCY_PERIOD_WORDS='pcm';
                $TENANCY_RENT_PAYMENT_FREQUENCY='monthly';
            }else if($TENANCY_PERIOD_WORDS=='Weekly'){
                $TENANCY_PERIOD_WORDS='pw';
                $TENANCY_RENT_PAYMENT_FREQUENCY='weekly';
            }else if($TENANCY_PERIOD_WORDS=='Year' || $TENANCY_PERIOD_WORDS=='Yearly'){
                $TENANCY_PERIOD_WORDS='pa';
                $TENANCY_RENT_PAYMENT_FREQUENCY='annually';
            }else if(stristr($TENANCY_PERIOD_WORDS, 'Periodic')){
                $TENANCY_PERIOD_WORDS=NULL;
                $TENANCY_RENT_PAYMENT_FREQUENCY=NULL;
            }

        }

        $TENANCY_DUE_DATE		= $row['Date Rent Due'];
        $TENANCY_RENT_DUE_NOTES		= NULL;
        $TENANCY_WEEK			= NULL;
        $TENANCY_MONTH		= NULL;
        $TENANCY_RENT_APPORTIONED		= NULL;
        $TENANCY_DEPOSIT_HOLD		= NULL;
        $TENANCY_PAYABLE_TO_LANDLORD		= NULL;
        $TENANCY_PROTECTION_SCHEME		= NULL;
        $TENANCY_PROTECTION_SCHEME_ID			= NULL;
        $TENANCY_PROTECTION_SCHEME_NOTES		= NULL;
        $TENANCY_PROTECTION_SCHEME_NOTES_GENERAL		= NULL;

        $DUE_DATE=NULL;
        if($TENANCY_DUE_DATE!=NULL){
            $DUE_DATE 		= date('d',strtotime($row['Date Rent Due']));
        }


        $get_DEPOSIT_HELD=$row['TDSHeldBy'];

        if($get_DEPOSIT_HELD=='Agent'){
            $DEPOSIT_HELD=3;
        }else  if($get_DEPOSIT_HELD=='Landlord'){
            $DEPOSIT_HELD=2;
        }

        $get_TENANCY_PROTECTION_SCHEME_ID=$row['TDSName'];

        if($get_TENANCY_PROTECTION_SCHEME_ID=='Deposit Protection Service'){
            $TENANCY_PROTECTION_ID=1;
        }else if($get_TENANCY_PROTECTION_SCHEME_ID=='Mydeposits.co.uk'){
            $TENANCY_PROTECTION_ID=2;
        }else if($get_TENANCY_PROTECTION_SCHEME_ID=='Tenancy Deposit Solutions Ltd'){
            $TENANCY_PROTECTION_ID=4;
        }else if($get_TENANCY_PROTECTION_SCHEME_ID=='Tenancy Deposit Solutions Ltd (NLA)'){
            $TENANCY_PROTECTION_ID=4;
        }
        
      
        $TENANCY_FULL_DEPOSIT			=$row['Deposit'];

        $TENANCY_LETTING_FEE_PERCENTAGE			= '0.00';

        $TENANCY_LETTING_FEE_PERIOD			= $TENANCY_PERIOD_WORDS;

        $TENANCY_MANAGEMENT_FEE_PERCENTAGE			= '0.0';

        $TENANCY_MANAGEMENT_SERVICE			= '0.0';

        $MANAGED_LEVEL=NULL;

        if($TENANCY_MANAGEMENT_SERVICE!=NULL){
            if(stristr($TENANCY_MANAGEMENT_SERVICE, 'LET ONLY')){
                $MANAGED_LEVEL=2;
            }else if(stristr($TENANCY_MANAGEMENT_SERVICE, 'MANAGED')){
                $MANAGED_LEVEL=1;
            }else{
                $MANAGED_LEVEL=NULL;
            }
        }

        $TENANCY_NOTES.=' '.$TENANCY_PROTECTION_SCHEME_NOTES;

        $TENANCY_NOTES.=' '.$TENANCY_PROTECTION_SCHEME_NOTES_GENERAL;

        $TENANCY_NOTES.=' '.$TENANCY_RENT_DUE_NOTES;

        if($row['DepositProtectionSchemeRegistrationNumber']!=NULL){
            $TENANCY_NOTES.= '\n Deposit Scheme ID:'.$row['DepositProtectionSchemeRegistrationNumber'];
        }

        // $query2 = "select `TCODE` from `host_tenants` where `TTYCODE`='$TENANCY_CODE'";

        // $data2 = json_decode($db_connect_src->queryFetch($query2),true);

       $SHARED_TENANT_IDS=NULL;
        // $shared_tenant_array=array();
        // $sh=0;
        $lead_tenant='Ten_'.$row['Reference'];

        // $SHARED_TENANT_IDS=json_encode($shared_tenant_array);

        /*echo '<pre>';
        print_r($SHARED_TENANT_IDS);
        echo '</pre>';*/

        //exit;
        $sql = "INSERT INTO cnb_sonia.`lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
        `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
        `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`, `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
        `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
        `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
        `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`)
        VALUES (NULL,'$TENANCY_CODE', '$PROPERTY_REF_ID', NULL, '$lead_tenant','$SHARED_TENANT_IDS',$TENANCY_START_DATE, $TENANCY_END_DATE, '$TENANCY_RENT', '$TENANCY_PERIOD_WORDS',
        '$TENANCY_RENT_PAYMENT_FREQUENCY', '$DUE_DATE', NULL, '0.00', NULL, NULL, NULL, '0.00', '$TENANCY_FULL_DEPOSIT', NULL, '$DEPOSIT_HELD', '$TENANCY_PROTECTION_ID', '$MANAGED_LEVEL',
        '$TENANCY_LETTING_FEE_PERCENTAGE','0.00','$TENANCY_LETTING_FEE_PERIOD', '$TENANCY_MANAGEMENT_FEE_PERCENTAGE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00',NULL,'$TENANCY_NOTES')";


$db1->execute($sql) or die($sql);  

}
echo $p;
?>
<script>
window.location="tennancies_v1.php?p=<?= $p+1;?>";
</script>
<?php
} 
else { 
	echo "END ";
}
?>
