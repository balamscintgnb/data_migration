<?php
include("db.php");
include("Classes/PHPExcel/IOFactory.php");

$file='files/Rent_properties.csv';

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file);
} catch (Exception $e) {
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);
//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();
//It returns the highest number of columns
$highest_column = $sheet->getHighestColumn();

$query = "insert into `properties` ("; 

//Loop through first row
for($row =1; $row <= 1; $row++) {
	$single_row = $sheet->rangeToArray('A' . $row . ':' . $highest_column . $row, NULL, TRUE, FALSE);
	foreach($single_row[0] as $key=>$value) {		
			$query .= "`".mysqli_real_escape_string($con, $value)."`,";
	}
	$query = substr($query, 0, -1);
}
$query .= ") VALUES ";

for($row = 2; $row <= $total_rows; $row++) {
	//Read a single row of data and store it as a array.
	//This line of code selects range of the cells like A1:D1
	$multi_row = $sheet->rangeToArray('A' . $row . ':' . $highest_column . $row, NULL, TRUE, FALSE);
	
	//Creating a dynamic query based on the rows from the excel file
	$query .= "(";
	//Print each cell of the current row
	foreach($multi_row[0] as $key=>$value) {

		
		$query .= "'".mysqli_real_escape_string($con, $value)."',";
	}
	$query = substr($query, 0, -1);
	$query .= "),";
}

$query = substr($query, 0, -1);



// At last we will execute the dynamically created query an save it into the database
mysqli_query($con, $query);
if(mysqli_affected_rows($con) > 0) {
	echo '<span class="msg">Database table updated!</span>';
} else {
	echo '<span class="msg">Can\'t update database table! try again.</span>';
}
// Finally we will remove the file from the uploads folder (optional)
//unlink($file);
?>
