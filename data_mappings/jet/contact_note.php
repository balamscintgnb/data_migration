<?php
// NOTE: Kindly run this following query after run this file for a new client
// UPDATE `properties` a SET a.PROPERTY_NOTES = (SELECT b.CONTACT_NOTES FROM `contact_notes` b WHERE b.PROPERTY_id = a.PROPERTY_ID)
error_reporting(E_ALL);
ini_set('display_errors', 1); 

require_once '../../header_init.php';

	$contact_notes_sql = "SELECT DISTINCT PROPERTY_ID FROM `notes` WHERE ASSOC_CODE IS NULL AND CONTACT_TYPE = 'PH' ORDER BY CONTACT_NOTES_DATE DESC ";
	$contact_notes_exists = json_decode($db_connect->queryFetch($contact_notes_sql),true);

	$ins_count = 1;
	if($contact_notes_exists['success'] == 1)
	{
		foreach ($contact_notes_exists['data'] as $key => $value)
		{
			if(trim($value['PROPERTY_ID']) != '')
			{
				$sql_sess = "SET SESSION group_concat_max_len = 1000000";
				$db_connect->queryExecute($sql_sess) or die ($sql_sess);

				$notes_array = array();
				$contact_notes_sql2 = "SELECT GROUP_CONCAT(CONTACT_NOTES ORDER BY CONTACT_NOTES_DATE DESC SEPARATOR '\n\n') as CONTACT_NOTES FROM `notes` WHERE ASSOC_CODE IS NULL AND CONTACT_TYPE = 'PH' AND PROPERTY_ID = '" . trim($value['PROPERTY_ID']) . "' GROUP BY ASSOC_CODE ORDER BY CONTACT_NOTES_DATE DESC ";
				$contact_notes_exists2 = json_decode($db_connect->queryFetch($contact_notes_sql2),true);
				if($contact_notes_exists2['success'] == 1)
				{
					
					$notes_array[] = $contact_notes_exists2['data'][0]['CONTACT_NOTES'];
					$CONTACT_NOTES = addslashes(json_encode($notes_array));
					
					$PROPERTY_ID = $value['PROPERTY_ID'];
					$CONTACT_TYPE = 'PH';
					$ins_sql ="INSERT INTO `contact_notes` (`PROPERTY_ID`, `CONTACT_TYPE`, `CONTACT_NOTES`) VALUES ('$PROPERTY_ID', '$CONTACT_TYPE', '$CONTACT_NOTES')";
					$db_connect->queryExecute($ins_sql) or die ($ins_sql);
					$ins_count++;
					unset($notes_array);
				}
			}
		}
		echo $ins_count . " CONTACT NOTES INSERTED SUCCESSFULLY"; //	2 has been deducted from row var since row 	
	}


// table structure

// CREATE TABLE `contact_notes` (
//  `id` bigint(20) NOT NULL AUTO_INCREMENT,
//  `PROPERTY_ID` varchar(255) DEFAULT NULL,
//  `ASSOC_CODE` varchar(255) DEFAULT NULL,
//  `ASSOC_TYPE` varchar(255) DEFAULT NULL,
//  `NEG_CODE` varchar(255) DEFAULT NULL,
//  `NEG_NAME` varchar(255) DEFAULT NULL,
//  `CONTACT_TYPE` varchar(255) DEFAULT NULL,
//  `CONTACT_NOTES` longtext,
//  `CONTACT_NOTES_DATE` datetime DEFAULT NULL,
//  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
//  PRIMARY KEY (`id`)
// ) ENGINE=MyISAM AUTO_INCREMENT=95600 DEFAULT CHARSET=latin1
?>