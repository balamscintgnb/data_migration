<?php
// rk created this file on 2019-01-17 12-47 for fetching property notes and update in properties table
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/jet_stn_prp.xls';
$file_name='../source_data/jet/jet_stn_prp_a.csv';

$thisProceed=true;

try {
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$notes_updated_count = 0;
$new_props = array();

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
			$PROPERTY_NOTES = '';
			$PROPERTY_NOTES = trim($objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue());
			if($PROPERTY_NOTES!='')
			{
				$PROPERTY_NOTES =  addslashes(format_array(preg_split ('/$\R?^/m', trim($PROPERTY_NOTES))));
			}

			$chk_prop_exist_sql = "SELECT * FROM `properties` WHERE PROPERTY_ID = '$PROPERTY_ID' AND PROPERTY_NOTES IS NULL";
			$property_exists = json_decode($db_connect->queryFetch($chk_prop_exist_sql),true);
			
			if(@$property_exists['data'][0]['PROPERTY_ID'])
			{
				$update_notes_sql = "UPDATE `properties` SET PROPERTY_NOTES = '" . $PROPERTY_NOTES . "' WHERE PROPERTY_ID = '" . $PROPERTY_ID . "'";
				$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
				$notes_updated_count++;
			}
			else
			{
				$new_props[] = $PROPERTY_ID;
			}
			// exit();
		}	// $row if ends here
	}	// for loop ends here
	echo "IN " . ($row - 2) . " ROUNDS " . $notes_updated_count . " PROPERTY NOTES UPDATED SUCCESSFULLY"; //	2 has been deducted from row var since row initialized as 1 and for does it nature ++
	echo "<pre>";
	print_r($new_props);
	echo "</pre>";
}	// if $thisProceed ends here

function format_array(array $PROPERTY_NOTES_SPLITTED)
{
	foreach( $PROPERTY_NOTES_SPLITTED as $key => $value )
	{
		if( trim($value) == "" )
		{
			unset($PROPERTY_NOTES_SPLITTED[$key]);
		}
	}
	return json_encode(array_values($PROPERTY_NOTES_SPLITTED));
	// return json_encode(array_values($PROPERTY_NOTES_SPLITTED),JSON_UNESCAPED_UNICODE);
}


// 			code to fetch from db in AMC side
// 			$chk_prop_exist_sql = "SELECT * FROM `properties` WHERE PROPERTY_ID = 'LHA090010'";
// 			$chk_prop_exist_sql = "SELECT * FROM `properties` WHERE PROPERTY_ID = 'SPH170068'";
// 			$chk_prop_exist_sql = "SELECT * FROM `properties` WHERE PROPERTY_ID = 'RHL170002'";
// 			$property_exists = json_decode($db_connect->queryFetch($chk_prop_exist_sql),true);
// 			echo $property_exists['data'][0]['PROPERTY_ID'];
// 			echo "<br/>";
// 			echo $property_exists['data'][0]['PROPERTY_NOTES'];
// 			echo "<br/>";
// 			echo "<br/> json decoded: <pre>";
// 			print_r(stripslashes(json_decode($property_exists['data'][0]['PROPERTY_NOTES']))); // perfect one
// 			echo "</pre>";
// 			echo "<br/><pre>";
// 			echo "<br/> exploded : ";
// 			print_r(explode(',', stripslashes($property_exists['data'][0]['PROPERTY_NOTES'])));
// 			echo "</pre>";

// exit;
?>