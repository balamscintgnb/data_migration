<?php
// rk created this file on 2019-01-17 12-47 for fetching letting notes and update in lettings table
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jet/jet_stn_ten.csv';

$thisProceed=true;

try {
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$notes_updated_count = 0;
$new_props = array();

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$LETTING_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
			$LETTING_NOTES = '';
        	$extra = trim($objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue());
        	if($extra!='')
        	{
				$LETTING_NOTES = addslashes(get_res(preg_split('/$\R?^/m', trim($extra))));
				$chk_let_exist_sql = "SELECT * FROM `lettings` WHERE LETTING_ID = '" . $LETTING_ID . "' AND LETTING_NOTES = ''";
				$let_exists = json_decode($db_connect->queryFetch($chk_let_exist_sql),true);
				
				if(@$let_exists['data'][0]['LETTING_ID'])
				{
					$update_notes_sql = "UPDATE `lettings` SET LETTING_NOTES = '" . $LETTING_NOTES . "' WHERE LETTING_ID = '" . $LETTING_ID . "'";
					$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
					$notes_updated_count++;
				}
				else
				{
					$new_props[] = $LETTING_ID;
				}
        	}
		}	// $row if ends here
	}	// for loop ends here
	echo "IN " . ($row - 2) . " ROUNDS " . $notes_updated_count . " LETTING NOTES UPDATED SUCCESSFULLY"; //	2 has been deducted from row var since row initialized as 1 and for does it nature ++
	echo "<pre>";
	print_r($new_props);
	echo "</pre>";
}	// if $thisProceed ends here

function get_res(array $LETTING_NOTES_SPLITTED)
{
	$store_me = array();
	if(isset($LETTING_NOTES_SPLITTED))
	{
		foreach( $LETTING_NOTES_SPLITTED as $key => $value )
		{
			if( trim($value) == "" )
			{
				unset($LETTING_NOTES_SPLITTED[$key]);
			}
			else
			{
				$explode_exploded = explode('=', $value);
				if(isset($explode_exploded[1]) && trim($explode_exploded[1])!='')
				{
					if(is_string(trim($explode_exploded[1])) && !is_numeric(trim($explode_exploded[1])))
					{
						if(strpos(trim($explode_exploded[1]), '{') === FALSE && strrpos(trim($explode_exploded[1]), '}') === FALSE)
						{
							if(substr(trim($explode_exploded[1]), 0, 1) == "'" && substr(trim($explode_exploded[1]),-1) == "'" )
							{
								$store_me[] = substr(trim($explode_exploded[1]), 1,-1);
								// $store_me[] = substr(substr(trim($explode_exploded[1]), 1,-1), -1);
							}
							else
							{
								$store_me[] = trim($explode_exploded[1]);
							}
						}
						else
						{
							$store_me[] = substr(trim($explode_exploded[1]), 1,-1);
						}
					}
				}
			}
		}
	}

	if(empty($store_me))
	{
		return '';
	}
	else
	{
		return json_encode($store_me);
	}
}
?>