<?php
// rk created this file on 2019-01-21 17-07 for fetching client notes and update in clients table - this is only for client files
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/jet_stn_cnt.csv';
$file_name='../source_data/jet/jet_stn_cnt_a.csv';

$thisProceed=true;

try
{
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$notes_updated_count = 0;

$updated_clients_id = array();
$updated_clients = array();
$new_clients = array();
$empty_notes_clients = array();

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
			$CLIENT_NOTES = '';
        	$extra = trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue());
        	if($extra!='' && $CLIENTID!='')
        	{
				if(in_array($CLIENTID, $updated_clients_id))
				{
					$newly_created_extra = $extra;
					foreach ($updated_clients as $updated_client)
					{
						foreach ($updated_client as $key => $value)
						{
							if($key == $CLIENTID)
							{
								$newly_created_extra .= "\n" . $value;
							}
						}
					}
					
					$CLIENT_NOTES = addslashes(get_res(preg_split('/$\R?^/m', stripslashes(trim($newly_created_extra)))));

					$update_notes_sql = "UPDATE `clients` SET CLIENT_NOTES = '" . $CLIENT_NOTES . "', RECORD_UPLOADED = 0 WHERE CLIENT_NOTES = '' AND CLIENTID = '" . $CLIENTID . "'";
					$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
					$notes_updated_count++;
				}
				else
				{
					$chk_client_exist_sql = "SELECT * FROM `clients` WHERE CLIENT_NOTES = '' AND CLIENTID = '" . $CLIENTID . "'";
					$client_exists = json_decode($db_connect->queryFetch($chk_client_exist_sql),true);
					
					if(@$client_exists['data'][0]['CLIENTID'])
					{
						$CLIENT_NOTES = addslashes(get_res(preg_split('/$\R?^/m', stripslashes(trim($extra)))));
						// $id = $client_exists['data'][0]['id'];
						// $update_notes_sql = "UPDATE `clients` SET CLIENT_NOTES = '" . $CLIENT_NOTES . "' WHERE id = '" . $id . "'";
						$update_notes_sql = "UPDATE `clients` SET CLIENT_NOTES = '" . $CLIENT_NOTES . "', RECORD_UPLOADED = 0 WHERE CLIENT_NOTES = '' AND CLIENTID = '" . $CLIENTID . "'";
						$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
						$notes_updated_count++;
						$updated_clients_id[] = $CLIENTID;
					}
					else
					{
						$new_clients[] = $CLIENTID;
					}
				}
				$updated_clients[$row][$CLIENTID] = $extra;
        	}
        	else
        	{
				$empty_notes_clients[] = $CLIENTID;
        	}
		}	// $row if ends here
	}	// for loop ends here
	echo "IN " . ($row - 2) . " ROUNDS " . $notes_updated_count . " CLIENT NOTES UPDATED SUCCESSFULLY"; //	2 has been deducted from row var since row initialized as 1 and for does it nature ++
	echo "<br/><br/>CLIENT(S) WHOSE NOTES ARE UPDATED : <br/><pre>";
	print_r($updated_clients_id);
	echo "</pre>";
	echo "CLIENT(S) WHO ARE NOT IN DB : <br/><pre>";
	print_r($new_clients);
	echo "</pre>";
	echo "<br/><br/>CLIENT(S) WHOSE NOTES ARE EMPTY IN EXCEL ITSELF : <br/><pre>";
	print_r($empty_notes_clients);
	echo "</pre>";
}	// if $thisProceed ends here

function get_res(array $CLIENT_NOTES_SPLITTED)
{
	$store_me = array();

	if(isset($CLIENT_NOTES_SPLITTED))
	{
		foreach( $CLIENT_NOTES_SPLITTED as $key => $value )
		{
			if( trim($value) == "" )
			{
				unset($CLIENT_NOTES_SPLITTED[$key]);
			}
			else
			{
				$explode_exploded = explode('=', $value);
				// if(isset($explode_exploded[1]) && trim($explode_exploded[1])!='')
				if(isset($explode_exploded[1]))
				{
					if(is_string(trim($explode_exploded[1])) && !is_numeric(trim($explode_exploded[1])))
					{
						if(strpos(trim($explode_exploded[1]), '{') === FALSE && strrpos(trim($explode_exploded[1]), '}') === FALSE)
						{
							if(substr(trim($explode_exploded[1]), 0, 1) == "'" && substr(trim($explode_exploded[1]),-1) == "'" )
							{
								$store_me[] = substr(trim($explode_exploded[1]), 1,-1);
								// $store_me[] = substr(substr(trim($explode_exploded[1]), 1,-1), -1);
							}
							else
							{
								$store_me[] = trim($explode_exploded[1]);
							}
						}
						else
						{
							$store_me[] = substr(trim($explode_exploded[1]), 1,-1);
						}
					}
				}
			}
		}
	}

	if(empty($store_me))
	{
		return '';
	}
	else
	{
		return json_encode($store_me);
	}
}
?>