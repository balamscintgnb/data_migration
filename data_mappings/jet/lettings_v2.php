<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/stn_exit_upd/jet_stn_tenext.csv';
// $file_name='../source_data/jet/stpr/jet_stn_tenext.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}



//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
        $total_row = 0;

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


        //logic for data mapping.
        $LETTING_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
        $LETTING_CUSTOM_REF_NO=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
        $PROPERTY_ID='';
        $PROPERTY_ROOM_ID='';
        $TENANT_ID='';
        $Get_TENANT_ID = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());

        $SHARED_TENANT_IDS='';
        $Get_START_DATE = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        // $LETTING_START_DATE = date("Y-m-d",strtotime($Get_START_DATE));

        // $Get_END_DATE = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
        // $LETTING_END_DATE = date("Y-m-d",strtotime($Get_END_DATE));
        // if($LETTING_END_DATE == '1970-01-01'){
        //     $LETTING_END_DATE = '00-00-0000';
        // }


        if(stristr($Get_START_DATE, "\N") || $Get_START_DATE == '' )
        {
            $LETTING_START_DATE = '0000-00-00';
        }
        else
        {
            $LETTING_START_DATE = date("Y-m-d",strtotime($Get_START_DATE));
        }

        $Get_END_DATE = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());

        if(stristr($Get_END_DATE, "\N") || $Get_END_DATE == '' )
        {
            $LETTING_END_DATE = '0000-00-00';
        }
        else
        {
            $LETTING_END_DATE = date("Y-m-d",strtotime($Get_END_DATE));
        }

        //  echo "<pre>"; print_r($LETTING_ID); echo "</pre>";

        $LETTING_RENT=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
        $LETTING_RENT_FREQUENCY='';
        if($LETTING_RENT != "0"){
            $LETTING_RENT_FREQUENCY = 'pw';
        }
        // echo "<pre>"; print_r($LETTING_RENT_FREQUENCY); echo "</pre>";
        $RENT_FREQUENCY='';
        $LETTING_RENT_PAYMENT_FREQUENCY='';
        $LETTING_RENT_PAYMENT_DAY='';
        $LETTING_RENT_ADVANCE='';

        $LETTING_RENT_ADVANCE_TYPE='';
        $LETTING_NOTICE_PERIOD='';
        $LETTING_NOTICE_PERIOD_TYPE='';
        $LETTING_BREAK_CLAUSE='';
        $LETTING_BREAK_CLAUSE_TYPE='';
        $LETTING_DEPOSIT='NULL';

        $LETTING_DEPOSIT_DUE_DATE='';
        $LETTING_DEPOSIT_HELD_BY='';
        $LETTING_DEPOSIT_PROTECTION_SCHEME='';
        $LETTING_SERVICE='';
        $LETTING_FEES = 'NULL';
            
        $LETTING_FEE_FREQUENCY ='';
            
        $LETTING_MANAGEMENT_FEES='';
        $LETTING_MANAGEMENT_FEE_TYPE ='';
        $LETTING_MANAGEMENT_FEE_FREQUENCY ='';

        $LETTING_ADMIN_FEES = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));

        $LETTING_ADMIN_FEE_TYPE='';
        $LETTING_INVENTORY_FEES='';      
        $LETTING_INVENTORY_FEE_TYPE='';
        $LETTING_RENT_GUARANTEE='';

        $LETTING_LANDLORD_PAYMENT='';
        $LETTING_VAT_PERCENTAGE='';
        $LETTING_NOTES = '';
        $LETTING_DATETIME='';
        $TENANT_NAME='';

        
        /////////////////////////////ADD MORE FIELDS/////////////////////////
        $Get_commission = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
        $Get_Fixed_commission = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
        $LETTING_FEE_TYPE = '';

        if($Get_commission == '0'){
            $Get_commission = '';
        }
        if($Get_Fixed_commission == '0'){
            $Get_Fixed_commission = '';
        }


        if($Get_commission != ''){
            $LETTING_FEE_TYPE='2';
            $LETTING_FEES = "'".$Get_commission."'";
        }
        else if($Get_Fixed_commission != ''){
            $LETTING_FEES = "'".$Get_Fixed_commission."'";
            $LETTING_FEE_TYPE='1';
        }
        
        
            
        /////////////////////////////SUB QUERY//////////////////////////
           
        $Query1="SELECT tblcode FROM letting_tab WHERE `code` LIKE '$LETTING_ID'";

        $Insert_Query1 = json_decode($db_connect->queryFetch($Query1),true);

        $PROPERTY_ID=(isset($Insert_Query1['data'][0]['tblcode']))?trim($Insert_Query1['data'][0]['tblcode']):'';



        $Query2="SELECT maintencode FROM tenant_tab WHERE `code` LIKE '$Get_TENANT_ID'";

        $Insert_Query2 = json_decode($db_connect->queryFetch($Query2),true);

        $TENANT_ID=(isset($Insert_Query2['data'][0]['maintencode']))?trim($Insert_Query2['data'][0]['maintencode']):'';

        // echo "<pre>"; print_r($PROPERTY_ID); echo "</pre>";
    
        ///////////////////////////END SUB QUERY////////////////////////

        if($PROPERTY_ID != ''){

            $total_row++;
        $sql ="INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`, `SHARED_TENANT_IDS`,
                `LETTING_START_DATE`, `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`,
                `LETTING_RENT_ADVANCE`, `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,
                `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`,
                `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`, `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`,
                `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`, `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`,
                `LETTING_RENT_GUARANTEE`, `LETTING_VAT_PERCENTAGE`, `LETTING_NOTES`) VALUES
                ('$LETTING_ID', '$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID', '$SHARED_TENANT_IDS', '$LETTING_START_DATE',
                '$LETTING_END_DATE', $LETTING_RENT, '$LETTING_RENT_FREQUENCY', '$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
                '$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE', '$LETTING_BREAK_CLAUSE_TYPE',
                $LETTING_DEPOSIT, '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE', $LETTING_FEES,
                '$LETTING_FEE_TYPE', '$LETTING_FEE_FREQUENCY', '$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY',
                '$LETTING_ADMIN_FEES', '$LETTING_ADMIN_FEE_TYPE', '$LETTING_INVENTORY_FEES', '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE',
                '$LETTING_VAT_PERCENTAGE', '$LETTING_NOTES')";

            // echo $row."<br/>";
             $db_connect->queryExecute($sql) or die($sql);
    
    // insert into table    
        }

    }
}
        echo$total_row." LETTINGS INSERTED SUCCESSFULLY";
}
?>