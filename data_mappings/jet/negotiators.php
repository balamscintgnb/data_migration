<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/stn_exit_upd/jet_stn_negs.csv';
$file_name='../source_data/jet/stpr/jet_stn_negs.csv';

$thisProceed=true;

try {
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$negs_count = 0;
$int_array = array();
$int_sub_array = array();
$int_sub_array2 = array();
if($thisProceed)
{
	unset($int_array);
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			unset($int_sub_array);
			unset($int_sub_array2);

			$CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
			$CLIENT_NAME = trim( $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
			$split1_misc = trim( $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());

			$int_sub_array['name'] = $CLIENT_NAME;
			
			// mobile, DOD and email data integrating here
			$FILTER1 = explode(';',trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));
			if(isset($FILTER1) && count($FILTER1)>0)
			{
				foreach ($FILTER1 as $value)
				{
					if(trim($value)!='' && strpos($value, ':'))
					{
						$value_exploded = explode(':', $value);
						if(isset($value_exploded) && count($value_exploded)>0)
						{
							$int_sub_array[trim($value_exploded[0])] = trim($value_exploded[1]);
						}
					}
				}
			}

			// Role,Salesforce id kind of misc data integrating here
			if(isset($split1_misc) && count($split1_misc)>0)
			{
				foreach (preg_split('/$\R?^/m', trim($split1_misc)) as $value2)
				{
					if(trim($value2)!='' && strpos($value2, '='))
					{
						$value2_exploded = explode('=', $value2);
						if(isset($value2_exploded) && count($value2_exploded)>0)
						{
							if(strpos(trim($value2_exploded[1]), "'"))
							{
								$value2_exploded[1] = str_replace("'", "", $value2_exploded[1]);
							}
							$int_sub_array2[trim($value2_exploded[0])] = trim(stripslashes($value2_exploded[1]));
						}
					}
				}
			}
			$int_array[$CLIENTID] = array_merge($int_sub_array,$int_sub_array2);

		}   // if($row>1) ENDS HERE   
	}   // for($row =1; $row <= $total_rows; $row++) ENDS HERE
	$fname = 'negotiators.json';
	$mode = 'w';
	$write_me_into_json = fopen($fname, $mode);
	if(fwrite($write_me_into_json, json_encode($int_array)) > 0)
	{
		echo $fname . " has been generated successfully <br/><br/> The content written on it is the following (Paste this content on <a href='http://json.parser.online.fr/' target='_blank'>JSON PARSER</a> and verify it): <br/><br/>";
		echo "<a href='http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']) .'/'.$fname."' target='_blank'>" . $fname ."</a>";
		// echo json_encode($int_array);
	}
	fclose($write_me_into_json);
}   // if($thisProceed) ENDS HERE

// echo "<br/>";
// echo "<br/>";
// echo "<br/>";
// $read = file_get_contents($fname);
// // echo "<pre>";
// // print_r(json_decode($read));
// // echo "</pre>";
// $obj_new = json_decode($read);
// echo $obj_new->AKZ->name;
?>