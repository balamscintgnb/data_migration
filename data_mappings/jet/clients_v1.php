<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

// $file_name='../source_data/jet/stn_exit_upd/jet_stn_cnt.csv';
// $file_name='../source_data/jet/stn_exit_upd/jet_stn_cnt_a.csv';
// $file_name='../source_data/jet/stpr/jet_stn_cnt.csv';
// $file_name='../source_data/jet/stpr/jet_stn_cnt_a.csv';

$thisProceed=true;


try {
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
    {
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }
    else
    { 
        $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
}
catch (Exception $e)
{
    $thisProceed=false;
    die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$fname = 'negotiators.json';
$read = file_get_contents($fname);
$obj_negs = json_decode($read);
// echo $obj_negs->AKZ->name;

$landlord_count = 0;
$applicant_count = 0;
$tenant_count = 0;
$buyer_count = 0;
$address_book_count = 0;
$solicitor_count = 0;

if($thisProceed)
{
    for($row =1; $row <= $total_rows; $row++)
    {
        if($row>1)
        {
            $CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $CLIENT_TITLE = '';
            
            $CLIENT_NAME='';
                $TITLE = trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
                $NAME1  = trim( $objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
                $NAME2  = trim( $objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());

                $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$TITLE." ".$NAME1." ".$NAME2)));
                
            
            $FILTER1 = explode(';',trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue()));
            $FILTER2 = count($FILTER1);
           // echo "<pre>"; print_r($CLIENT_NAME); echo "<pre/>";
            
            $FIND1 = $FIND2 = $FIND3 = $FIND4 = $FIND5 = $FIND6 = '';

            if(isset($FILTER1))
            {
                if($FILTER2 == 2)    {
                    $FIND1 = trim($FILTER1[0]);
                    $FIND2 = trim($FILTER1[1]);
                    }
                else if($FILTER2 == 3)    {
                    $FIND1 = trim($FILTER1[0]);
                    $FIND2 = trim($FILTER1[1]);
                    $FIND3 = trim($FILTER1[2]);
                    }
                else if($FILTER2 == 4)    {
                    $FIND1 = trim($FILTER1[0]);
                    $FIND2 = trim($FILTER1[1]);
                    $FIND3 = trim($FILTER1[2]);
                    $FIND4 = trim($FILTER1[3]);
                    }
                else if($FILTER2 == 5)    {
                    $FIND1 = trim($FILTER1[0]);
                    $FIND2 = trim($FILTER1[1]);
                    $FIND3 = trim($FILTER1[2]);
                    $FIND4 = trim($FILTER1[3]);
                    $FIND5 = trim($FILTER1[4]);
                    }
                else if($FILTER2 == 6)    {
                    $FIND1 = trim($FILTER1[0]);
                    $FIND2 = trim($FILTER1[1]);
                    $FIND3 = trim($FILTER1[2]);
                    $FIND4 = trim($FILTER1[3]);
                    $FIND5 = trim($FILTER1[4]);
                    $FIND6 = trim($FILTER1[5]);
                    }
                else{
                    $FIND1 = trim($FILTER1[0]);
                }
            }

           // echo $FIND1;
           $match1 = $match2 = $match3 = $match4 = $match5 = $match6 = $MAIL1 = $MAIL2 = $MAIL3 = $MAIL4 = $MAIL5 = $MAIL6 = $MOB1 = $MOB2 = $MOB3 = $MOB4 = $MOB5 = $MOB6 = $CLIENT_EMAIL_1=$CLIENT_EMAIL_2= $CLIENT_EMAIL_3= $CLIENT_EMAIL_4= $CLIENT_EMAIL_5= $CLIENT_PRIMARY_EMAIL='';

           
           if($FIND1!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND1, $match1);
                $MAIL1 = trim(@$match1[0]);
           }
           if($FIND2!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND2, $match2);
                $MAIL2 = trim(@$match2[0]);
           }
           if($FIND3!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND3, $match3);
                $MAIL3 = trim(@$match3[0]);
           }
           if($FIND4!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND4, $match4);
                $MAIL4 = trim(@$match4[0]);
           }
           if($FIND5!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND5, $match5);
                $MAIL5 = trim(@$match5[0]);
           }
           if($FIND6!=''){
                preg_match('/\b[^\s]+@[^\s]+/', $FIND6, $match6); 
                $MAIL6 = trim(@$match6[0]);
           }



            if($MAIL1 != ''){
                $CLIENT_PRIMARY_EMAIL = $MAIL1;
            }
            if($MAIL2 != ''){
                if($CLIENT_PRIMARY_EMAIL !=''){
                    $CLIENT_EMAIL_1 = $CLIENT_PRIMARY_EMAIL;
                }
             $CLIENT_PRIMARY_EMAIL = $MAIL2;
            }
            if($MAIL3 != ''){
                if($CLIENT_PRIMARY_EMAIL !=''){
                    $CLIENT_EMAIL_2 = $CLIENT_PRIMARY_EMAIL;
                }
                $CLIENT_PRIMARY_EMAIL = $MAIL3;
            }
            if($MAIL4 != ''){
                if($CLIENT_PRIMARY_EMAIL !=''){
                    $CLIENT_EMAIL_3 = $CLIENT_PRIMARY_EMAIL;
                }
                $CLIENT_PRIMARY_EMAIL = $MAIL4;
            }
            if($MAIL5 != ''){
                if($CLIENT_PRIMARY_EMAIL !=''){
                    $CLIENT_EMAIL_4 = $CLIENT_PRIMARY_EMAIL;
                }
                $CLIENT_PRIMARY_EMAIL = $MAIL5;
            }
            if($MAIL6 != ''){
                if($CLIENT_PRIMARY_EMAIL !=''){
                    $CLIENT_EMAIL_5 = $CLIENT_PRIMARY_EMAIL;
                }
                $CLIENT_PRIMARY_EMAIL = $MAIL6;
            }


           // echo "<pre>"; print_r($CLIENT_EMAIL_3); echo "<pre/>";


            if(strlen(preg_replace('/[^0-9]/','',$FIND1))>9){
               $MOB1 = preg_replace('/[^0-9]/','',$FIND1);
            }
            if(strlen(preg_replace('/[^0-9]/','',$FIND2))>9){
                $MOB2 = preg_replace('/[^0-9]/','',$FIND2);
            }
            if(strlen(preg_replace('/[^0-9]/','',$FIND3))>9){
                $MOB3 = preg_replace('/[^0-9]/','',$FIND3);
            }
            if(strlen(preg_replace('/[^0-9]/','',$FIND4))>9){
                $MOB4 = preg_replace('/[^0-9]/','',$FIND4);
            }
            if(strlen(preg_replace('/[^0-9]/','',$FIND5))>9){
                $MOB5 = preg_replace('/[^0-9]/','',$FIND5);
            }
            if(strlen(preg_replace('/[^0-9]/','',$FIND6))>9){
                $MOB6 = preg_replace('/[^0-9]/','',$FIND6);
            }

            $CLIENT_PRIMARY_PHONE = '';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
        
            if($MOB1){
                $CLIENT_PRIMARY_PHONE = $MOB1;
            }if($MOB2){
                $CLIENT_PHONE_1 = $MOB2;
            }if($MOB3){
                $CLIENT_PHONE_2 = $MOB3;
            }if($MOB4){
                $CLIENT_PHONE_3 = $MOB4;
            }if($MOB5){
                $CLIENT_PHONE_4 = $MOB5;
            }if($MOB6){
                $CLIENT_PHONE_5 = $MOB6;
            }

            // echo "<pre>"; print_r($CLIENT_PRIMARY_PHONE); echo "<pre/>";
             
            $CLIENT_COMPANY_NAME='';
            $CLIENT_COMPANY_NAME= trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());

            $CLIENT_TYPE='';
            $CLIENT_SUB_TYPE='';
            
            $CLIENT_APPLICANT_TYPE='';

            
            $STATUS = trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            if($STATUS){
                $CLIENT_STATUS = 'ACTIVE';
            }
            else{
                $CLIENT_STATUS = '';
            }
            
            $STAFF_ID = trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
            
            $CLIENT_STAFF_ID=($STAFF_ID)?((isset($obj_negs->$STAFF_ID->e))?$obj_negs->$STAFF_ID->e:''):'';
            
           
            $CLIENT_ADDRESS_LINE_1='';
            $LINE = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
            $LINE2 = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
            $LINE3 = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_1 = trim($LINE);
            if($LINE2){
                if($CLIENT_ADDRESS_LINE_1){
                    $CLIENT_ADDRESS_LINE_1.= ', ';
                }
                $CLIENT_ADDRESS_LINE_1.= $LINE2; 
            }
            if($LINE3){
                if($CLIENT_ADDRESS_LINE_1){
                    $CLIENT_ADDRESS_LINE_1.= ', ';
                }
                $CLIENT_ADDRESS_LINE_1.= $LINE3;
            }
            $CLIENT_ADDRESS_LINE_1 = addslashes($CLIENT_ADDRESS_LINE_1);
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
            $CLIENT_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
            $CLIENT_ADDRESS_TOWN = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
            $CLIENT_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));

            if($CLIENT_ADDRESS_TOWN == ''){
                $CLIENT_ADDRESS_TOWN = trim($CLIENT_ADDRESS_CITY);
                // $CLIENT_ADDRESS_CITY = trim($CLIENT_ADDRESS_LINE_2);
                $CLIENT_ADDRESS_CITY = '';
            }
            

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';

            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';

            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            

            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES='';
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';
            $RECORD_UPLOADED = '8';
            $SEARCH_CRITERIA = "NULL";

            
            ////////////////// CHECKING LANDLORD EXISTENCE //////////////
            $Query1 = "SELECT * FROM `landlord_tab` WHERE `cntcode`='$CLIENTID'";
            $FIND_TYPE1 = json_decode($db_connect->queryFetch($Query1),true);
            if(isset($FIND_TYPE1['data'][0]['cntcode']))
            {
                $TYPE_LORD = $FIND_TYPE1['data'][0]['cntcode'];
                if($TYPE_LORD == $CLIENTID)
                {
                    $CLIENT_TYPE = 'landlord';
                    $RECORD_UPLOADED = '0';

                    // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                    if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                    {
                        $CLIENT_TYPE='SOLICITOR';
                        if(trim($CLIENT_NAME)=='')
                        {
                            $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                            $CLIENT_COMPANY_NAME = '';
                        }
                    }
                    else
                    {
                        if(trim($CLIENT_NAME)=='')
                        {
                            $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                        }
                        else
                        {
                            if(trim($CLIENT_COMPANY_NAME)!='')
                            {
                                $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                            }
                        }
                        $CLIENT_COMPANY_NAME = '';
                    }

                    $chk_qry="SELECT * FROM `clients` WHERE `CLIENTID` = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE'";
                    $chk_qry_res = json_decode($db_connect->queryFetch($chk_qry),true);

                    if(count($chk_qry_res['data'])<=0)
                    {
                        $sql1 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                        `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                        `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                        `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                        `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                        `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                        `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                        `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                        '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                        '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                        '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                        '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                        '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                        '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                        '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";
                        $db_connect->queryExecute($sql1) or die($row."<br>".$sql1);
                        if($CLIENT_TYPE == 'SOLICITOR')
                        {
                            $solicitor_count++;
                        }
                        else
                        {
                            $landlord_count++;
                        }
                    }
                    $CLIENT_TYPE = '';
                }
            }

            ////////////////// CHECKING APPLICANT EXISTENCE //////////////
            if($CLIENT_TYPE == '')
            {
                $SEARCH_CRITERIA_APPLICANT = array();
                unset($SEARCH_CRITERIA_APPLICANT);
                $TYPE_APP = $TYPE_APP2 = '';
                $Query2 = "SELECT * FROM `applicant_tab` WHERE `cntcode`='$CLIENTID'";
                $FIND_TYPE2 = json_decode($db_connect->queryFetch($Query2),true);
                if(isset($FIND_TYPE2['data']) && count($FIND_TYPE2['data'])>0)
                {
                    $TYPE_APP = (isset($FIND_TYPE2['data'][0]['cntcode']))?$FIND_TYPE2['data'][0]['cntcode']:'';
                    if($TYPE_APP == $CLIENTID)
                    {
                        $PROPERTY_TYPE = $NOTE = $SEARCH_CRITERIA=$category=$MIN_PRIC2 =$MAX_PRIC2 =$MIN_RENT2 =$MAX_RENT2 =$MIN_BATH =$MAX_BATH =$MIN_BED =$MAX_BED =$MIN_RECP =$MAX_RECP =$FURNIS =$NOTE = $MAX_PRIC = $MIN_PRIC = '';

                        $MIN_BATH = (isset($FIND_TYPE2['data'][0]['num4']))?$FIND_TYPE2['data'][0]['num4']:'';
                        $MAX_BATH = (isset($FIND_TYPE2['data'][0]['num4to']))?$FIND_TYPE2['data'][0]['num4to']:'';

                        $MIN_BED = (isset($FIND_TYPE2['data'][0]['totalnum']))?$FIND_TYPE2['data'][0]['totalnum']:'';
                        $MAX_BED = (isset($FIND_TYPE2['data'][0]['totalnumto']))?$FIND_TYPE2['data'][0]['totalnumto']:'';
                        
                        $MIN_RECP = (isset($FIND_TYPE2['data'][0]['num3']))?$FIND_TYPE2['data'][0]['num3']:'';
                        $MAX_RECP = (isset($FIND_TYPE2['data'][0]['num3to']))?$FIND_TYPE2['data'][0]['num3to']:'';

                        $FURNIS = (isset($FIND_TYPE2['data'][0]['furnished']))?$FIND_TYPE2['data'][0]['furnished']:'';
                        $NOTE = (isset($FIND_TYPE2['data'][0]['notes']))?$FIND_TYPE2['data'][0]['notes']:'';
                        
                        $NOTE3 = preg_replace( '/[^[:print:]]/', '',$NOTE);    
                        $NOTE3 = addslashes($NOTE3);

                        $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$PROPERTY_TYPE,
                                'property_applicant_search_attribute_2_from'=>$MIN_BED,
                                'property_applicant_search_attribute_2_to'=>$MAX_BED,
                                'property_applicant_search_attribute_3_from'=>$MIN_BATH,
                                'property_applicant_search_attribute_3_to'=>$MAX_BATH,
                                'property_applicant_search_attribute_4_from'=>$MIN_RECP,
                                'property_applicant_search_attribute_4_to'=>$MAX_RECP,
                                'property_applicant_search_attribute_6'=>'',
                                'property_applicant_search_attribute_8'=>'',
                                'property_applicant_search_attribute_15'=>'',
                                'property_applicant_search_attribute_22'=>'',
                                'property_applicant_search_attribute_24'=>'',
                                'property_applicant_search_attribute_30'=>$FURNIS
                                );

                        $chk_my_cat = (isset($FIND_TYPE2['data'][0]['type']))?$FIND_TYPE2['data'][0]['type']:'';
                        if($chk_my_cat=='L')
                        {
                            $CLIENT_APPLICANT_TYPE = 'LET';
                            $category='44';
                            $Frequency = 'pw';
                            
                            $MIN_RENT = (isset($FIND_TYPE2['data'][0]['minrent']))?$FIND_TYPE2['data'][0]['minrent']:0;
                            $MAX_RENT = (isset($FIND_TYPE2['data'][0]['maxrent']))?$FIND_TYPE2['data'][0]['maxrent']:0;

                                $SEARCH_CRITERIA_APPLICANT=array('price'=>$MAX_RENT,'pricefrom'=>$MIN_RENT,'filter_array'=>$applicant_search_criteria,'frequency'=>$Frequency,'category'=>$category,'location'=>'');
                        }

                        if($chk_my_cat=='S')
                        {
                            $CLIENT_APPLICANT_TYPE = 'SALE';
                            $category='43';
                            $Frequency = '';
                            
                            $MIN_PRIC = (isset($FIND_TYPE2['data'][0]['minprice']))?$FIND_TYPE2['data'][0]['minprice']:0;
                            $MAX_PRIC = (isset($FIND_TYPE2['data'][0]['maxprice']))?$FIND_TYPE2['data'][0]['maxprice']:0;

                            $SEARCH_CRITERIA_APPLICANT=array('price'=>$MAX_PRIC,'pricefrom'=>$MIN_PRIC,'filter_array'=>$applicant_search_criteria,'frequency'=>$Frequency,'category'=>$category,'location'=>'');
                        }
                            
                        $SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);
                        $SEARCH_CRITERIA = "'".$SEARCH_CRITERIA."'";

                        $CLIENT_TYPE = 'applicant';
                        $RECORD_UPLOADED = '0';

                        // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                        if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                        {
                            $CLIENT_TYPE='SOLICITOR';
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                                $CLIENT_COMPANY_NAME = '';
                            }
                        }
                        else
                        {
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                            }
                            else
                            {
                                if(trim($CLIENT_COMPANY_NAME)!='')
                                {
                                    $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                                }
                            }
                            $CLIENT_COMPANY_NAME = '';
                        }

                        $chk_qry="SELECT * FROM `clients` WHERE `CLIENTID` = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE'";
                        $chk_qry_res = json_decode($db_connect->queryFetch($chk_qry),true);

                        if(count($chk_qry_res['data'])<=0)
                        {
                            $sql2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_APPLICANT_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                            `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                            `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                            `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                            `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                            `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                            `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                            `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_APPLICANT_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                            '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                            '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                            '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                            '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                            '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                            '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$NOTE3', '$CLIENT_FAX_1',
                            '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";
                            $db_connect->queryExecute($sql2) or die($row."<br>".$sql2);
                            if($CLIENT_TYPE == 'SOLICITOR')
                            {
                                $solicitor_count++;
                            }
                            else
                            {
                                $applicant_count++;
                            }
                        }
                        $CLIENT_TYPE = '';
                    }   // if($TYPE_APP == $CLIENTID || $TYPE_APP2 == $CLIENTID) ENDS HERE
                }
                else
                {
                    $Query4 = "SELECT * FROM `applicant2_tab` WHERE `cntcode`='$CLIENTID'";
                    $FIND_TYPE4 = json_decode($db_connect->queryFetch($Query4),true);
                    if(isset($FIND_TYPE4['data']) && count($FIND_TYPE4['data']) > 0)
                    {
                        $TYPE_APP2 = (isset($FIND_TYPE4['data'][0]['cntcode']))?$FIND_TYPE4['data'][0]['cntcode']:'';
                        if($TYPE_APP2 == $CLIENTID)
                        {
                            $PROPERTY_TYPE = $NOTE = $SEARCH_CRITERIA=$category=$MIN_PRIC2 =$MAX_PRIC2 =$MIN_RENT2 =$MAX_RENT2 =$MIN_BATH =$MAX_BATH =$MIN_BED =$MAX_BED =$MIN_RECP =$MAX_RECP =$FURNIS =$NOTE = $MAX_PRIC = $MIN_PRIC = '';

                            $MIN_BATH = (isset($FIND_TYPE4['data'][0]['num4']))?$FIND_TYPE4['data'][0]['num4']:'';
                            $MAX_BATH = (isset($FIND_TYPE4['data'][0]['num4to']))?$FIND_TYPE4['data'][0]['num4to']:'';

                            $MIN_BED = (isset($FIND_TYPE4['data'][0]['totalnum']))?$FIND_TYPE4['data'][0]['totalnum']:'';
                            $MAX_BED = (isset($FIND_TYPE4['data'][0]['totalnumto']))?$FIND_TYPE4['data'][0]['totalnumto']:'';
                            
                            $MIN_RECP = (isset($FIND_TYPE4['data'][0]['num3']))?$FIND_TYPE4['data'][0]['num3']:'';
                            $MAX_RECP = (isset($FIND_TYPE4['data'][0]['num3to']))?$FIND_TYPE4['data'][0]['num3to']:'';

                            $FURNIS = (isset($FIND_TYPE4['data'][0]['furnished']))?$FIND_TYPE4['data'][0]['furnished']:'';
                            $NOTE = (isset($FIND_TYPE4['data'][0]['notes']))?$FIND_TYPE4['data'][0]['notes']:'';
                            
                            $NOTE3 = preg_replace( '/[^[:print:]]/', '',$NOTE);    
                            $NOTE3 = addslashes($NOTE3);

                            $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$PROPERTY_TYPE,
                                    'property_applicant_search_attribute_2_from'=>$MIN_BED,
                                    'property_applicant_search_attribute_2_to'=>$MAX_BED,
                                    'property_applicant_search_attribute_3_from'=>$MIN_BATH,
                                    'property_applicant_search_attribute_3_to'=>$MAX_BATH,
                                    'property_applicant_search_attribute_4_from'=>$MIN_RECP,
                                    'property_applicant_search_attribute_4_to'=>$MAX_RECP,
                                    'property_applicant_search_attribute_6'=>'',
                                    'property_applicant_search_attribute_8'=>'',
                                    'property_applicant_search_attribute_15'=>'',
                                    'property_applicant_search_attribute_22'=>'',
                                    'property_applicant_search_attribute_24'=>'',
                                    'property_applicant_search_attribute_30'=>$FURNIS
                                    );

                            $chk_my_cat = (isset($FIND_TYPE4['data'][0]['type']))?$FIND_TYPE4['data'][0]['type']:'';
                            if($chk_my_cat=='L')
                            {
                                $CLIENT_APPLICANT_TYPE = 'LET';
                                $category='44';
                                $Frequency = 'pw';
                                
                                $MIN_RENT = (isset($FIND_TYPE4['data'][0]['minrent']))?$FIND_TYPE4['data'][0]['minrent']:0;
                                $MAX_RENT = (isset($FIND_TYPE4['data'][0]['maxrent']))?$FIND_TYPE4['data'][0]['maxrent']:0;

                                    $SEARCH_CRITERIA_APPLICANT=array('price'=>$MAX_RENT,'pricefrom'=>$MIN_RENT,'filter_array'=>$applicant_search_criteria,'frequency'=>$Frequency,'category'=>$category,'location'=>'');
                            }

                            if($chk_my_cat=='S')
                            {
                                $CLIENT_APPLICANT_TYPE = 'SALE';
                                $category='43';
                                $Frequency = '';
                                
                                $MIN_PRIC = (isset($FIND_TYPE4['data'][0]['minprice']))?$FIND_TYPE4['data'][0]['minprice']:0;
                                $MAX_PRIC = (isset($FIND_TYPE4['data'][0]['maxprice']))?$FIND_TYPE4['data'][0]['maxprice']:0;

                                $SEARCH_CRITERIA_APPLICANT=array('price'=>$MAX_PRIC,'pricefrom'=>$MIN_PRIC,'filter_array'=>$applicant_search_criteria,'frequency'=>$Frequency,'category'=>$category,'location'=>'');
                            }
                                
                            $SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);
                            $SEARCH_CRITERIA = "'".$SEARCH_CRITERIA."'"; 
                            $CLIENT_TYPE = 'applicant';
                            $RECORD_UPLOADED = '0';

                            // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                            if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                            {
                                $CLIENT_TYPE='SOLICITOR';
                                if(trim($CLIENT_NAME)=='')
                                {
                                    $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                                    $CLIENT_COMPANY_NAME = '';
                                }
                            }
                            else
                            {
                                if(trim($CLIENT_NAME)=='')
                                {
                                    $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                                }
                                else
                                {
                                    if(trim($CLIENT_COMPANY_NAME)!='')
                                    {
                                        $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                                    }
                                }
                                $CLIENT_COMPANY_NAME = '';
                            }

                            $chk_qry="SELECT * FROM `clients` WHERE `CLIENTID` = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE'";
                            $chk_qry_res = json_decode($db_connect->queryFetch($chk_qry),true);

                            if(count($chk_qry_res['data'])<=0)
                            {
                                $sql2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_APPLICANT_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_APPLICANT_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                                '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                                '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                                '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                                '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                                '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                                '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$NOTE3', '$CLIENT_FAX_1',
                                '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";
                                $db_connect->queryExecute($sql2) or die($row."<br>".$sql2);
                                if($CLIENT_TYPE == 'SOLICITOR')
                                {
                                    $solicitor_count++;
                                }
                                else
                                {
                                    $applicant_count++;
                                }
                            }
                            $CLIENT_TYPE = '';
                        }   // if($TYPE_APP == $CLIENTID || $TYPE_APP2 == $CLIENTID) ENDS HERE
                    }
                }               

            }

            ////////////////// CHECKING TENANT EXISTENCE //////////////
            if($CLIENT_TYPE == '')
            {
                $TYPE_TEN = '';
                $Query3 = "SELECT * FROM `tenant_tab` WHERE `maintencode`='$CLIENTID'";
                $FIND_TYPE3 = json_decode($db_connect->queryFetch($Query3),true);
                if(isset($FIND_TYPE3['data'][0]['maintencode']))
                {
                    $TYPE_TEN = $FIND_TYPE3['data'][0]['maintencode'];                        
                    if($TYPE_TEN == $CLIENTID)
                    {
                        $CLIENT_TYPE = 'Tenant';
                        $RECORD_UPLOADED = '0';

                        // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                        if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                        {
                            $CLIENT_TYPE='SOLICITOR';
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                                $CLIENT_COMPANY_NAME = '';
                            }
                        }
                        else
                        {
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                            }
                            else
                            {
                                if(trim($CLIENT_COMPANY_NAME)!='')
                                {
                                    $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                                }
                            }
                            $CLIENT_COMPANY_NAME = '';
                        }

                        $chk_qry="SELECT * FROM `clients` WHERE `CLIENTID` = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE'";
                        $chk_qry_res = json_decode($db_connect->queryFetch($chk_qry),true);

                        if(count($chk_qry_res['data'])<=0)
                        {
                            $sql3 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                            `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                            `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                            `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                            `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                            `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                            `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                            `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                            '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                            '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                            '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                            '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                            '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                            '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                            '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";
                                $db_connect->queryExecute($sql3) or die($row."<br>".$sql3);
                            if($CLIENT_TYPE == 'SOLICITOR')
                            {
                                $solicitor_count++;
                            }
                            else
                            {
                                $tenant_count++;
                            }
                        }
                            $CLIENT_TYPE = '';
                    }
                }
            }

            ////////////////// CHECKING BUYER EXISTENCE //////////////
            if($CLIENT_TYPE == '')
            {
                $TYPE_BUY = '';
                $Query5 = "SELECT * FROM `buyer_tab` WHERE `cntcode`='$CLIENTID'";
                $FIND_TYPE5 = json_decode($db_connect->queryFetch($Query5),true);
                if(isset($FIND_TYPE5['data'][0]['cntcode']))
                {
                    $TYPE_BUY = $FIND_TYPE5['data'][0]['cntcode'];
                    if($TYPE_BUY == $CLIENTID)
                    {
                        $CLIENT_TYPE = 'BUYER';
                        $RECORD_UPLOADED = '0';

                        // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                        if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                        {
                            $CLIENT_TYPE='SOLICITOR';
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                                $CLIENT_COMPANY_NAME = '';
                            }
                        }
                        else
                        {
                            if(trim($CLIENT_NAME)=='')
                            {
                                $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                            }
                            else
                            {
                                if(trim($CLIENT_COMPANY_NAME)!='')
                                {
                                    $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                                }
                            }
                            $CLIENT_COMPANY_NAME = '';
                        }

                        $chk_qry="SELECT * FROM `clients` WHERE `CLIENTID` = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE'";
                        $chk_qry_res = json_decode($db_connect->queryFetch($chk_qry),true);

                        if(count($chk_qry_res['data'])<=0)
                        {
                            $sql5 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                            `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                            `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                            `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                            `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                            `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                            `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                            `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                            '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                            '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                            '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                            '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                            '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                            '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                            '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";
                            $db_connect->queryExecute($sql5) or die($row."<br>".$sql5);
                            if($CLIENT_TYPE == 'SOLICITOR')
                            {
                                $solicitor_count++;
                            }
                            else
                            {
                                $buyer_count++;
                            }
                        }
                        $CLIENT_TYPE = '';
                    }
                }
            }



            ////////////////// CHECKING ADDRESSBOOK EXISTENCE IF CLIENT TYPE IS EMPTY AFTER ALL ABOVE SCENARIOS //////////////
            $filter_id2 = '';
            $Query6 = "SELECT * FROM `clients` WHERE `CLIENTID`='$CLIENTID'";
            $filter_id = json_decode($db_connect->queryFetch($Query6),true);
            if(isset($filter_id['data']))
            {
                // $filter_id2 = trim($filter_id['data'][0]['CLIENTID']);
                // if($filter_id2 != $CLIENTID)
                if(sizeof($filter_id['data'])<=0)
                {
                    $CLIENT_TYPE = 'ADDRESSBOOK';

                    // if( stristr($CLIENT_NAME, 'Llp') ||stristr($CLIENT_COMPANY_NAME, 'solicitor') || stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_NAME, 'LAW') || stristr($CLIENT_PRIMARY_EMAIL, 'law') )
                    if( stristr($CLIENT_NAME, 'solicitor') || stristr($CLIENT_NAME, 'Llp') || stristr($CLIENT_COMPANY_NAME, 'solicitor')  || stristr($CLIENT_COMPANY_NAME, 'lawyers') || stristr($CLIENT_COMPANY_NAME, 'law') || stristr($CLIENT_PRIMARY_EMAIL, 'solicitor') || stristr($CLIENT_PRIMARY_EMAIL, 'lawyers') || stristr($CLIENT_PRIMARY_EMAIL, 'law.') )
                    {
                        $CLIENT_TYPE='SOLICITOR';
                        if(trim($CLIENT_NAME)=='')
                        {
                            $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                            $CLIENT_COMPANY_NAME = '';
                        }
                    }
                    else
                    {
                        if(trim($CLIENT_NAME)=='')
                        {
                            $CLIENT_NAME = $CLIENT_COMPANY_NAME;
                        }
                        else
                        {
                            if(trim($CLIENT_COMPANY_NAME)!='')
                            {
                                $CLIENT_NAME .=  " ( " . $CLIENT_COMPANY_NAME . " ) ";
                            }
                        }
                        $CLIENT_COMPANY_NAME = '';
                    }

                    $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_COMPANY_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                        `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                        `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                        `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                        `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                        `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                        `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                        `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_COMPANY_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                        '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                        '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                        '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                        '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                        '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                        '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                        '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', $SEARCH_CRITERIA, '$RECORD_UPLOADED')";

                    $db_connect->queryExecute($sql) or die($row."<br>".$sql);
                    if($CLIENT_TYPE == 'SOLICITOR')
                    {
                        $solicitor_count++;
                    }
                    else
                    {
                        $address_book_count++;
                    }
                    $FILTER1 = [];
                }
                // else
                // {
                //     echo "filter id 2:" . $filter_id2;
                //     echo "<br/><br/>";
                //     echo "CLIENTID:" . $CLIENTID;
                //     echo "<br/><br/>";
                //     // echo "<br/><br/><pre>";
                //     // print_r($filter_id);
                //     // echo "</pre><br/><br/>";
                //     // echo "<br/><br/>";
                // }
            }
        }   // if($row>1) ENDS HERE   
    }   // for($row =1; $row <= $total_rows; $row++) ENDS HERE
    // echo "CLIENTS INSERTED SUCCESSFULLY";

echo $solicitor_count  . " SOLICITOR(S) INSERTED SUCCESSFULLY <br/>";
echo $landlord_count  . " LANDLORD(S) INSERTED SUCCESSFULLY <br/>";
echo $applicant_count  . " APPLICANT(S) INSERTED SUCCESSFULLY <br/>";
echo $tenant_count  . " TENANT(S) INSERTED SUCCESSFULLY <br/>";
echo $buyer_count  . " BUYER(S) INSERTED SUCCESSFULLY <br/>";
echo $address_book_count  . " ADDRESSBOOK(S) INSERTED SUCCESSFULLY";


}   // if($thisProceed) ENDS HERE
?>