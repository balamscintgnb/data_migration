<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/Stones_Property/jet_stn_extra.csv';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
        
            $PROPERTY_ROOMS='';
            $PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        //logic for data mapping.
        
            $room_sizes = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
            $room_sizes_array = explode("\n", $room_sizes);

            $propery_rooms = array();    
            $room_sizes = array();
            if(!empty($room_sizes_array)){
                foreach($room_sizes_array as $room_size){
                    if(stristr($room_size,'zRoomName')){
                            $room_size_type = explode('=',strip_tags($room_size));
                            $room_size_type_id = str_replace("zRoomName","",$room_size_type[0]);
                            $room_size_type_value = str_replace("\\","",$room_size_type[1]); 
                            

        
                            $room_sizes[$room_size_type_id]['property_room_title'] = trim($room_size_type_value,"'");
                    }
                    

                    if(stristr($room_size,'zRoomSize')){
                        $room_size_type = explode('=',$room_size);
                        $room_size_type_id = str_replace("zRoomSize","",$room_size_type[0]);
                        $room_size_type_value = str_replace("\\","",$room_size_type[1]); 
                        $room_sizes[$room_size_type_id]['property_room_size'] = trim($room_size_type_value,"'");
                    }

                    if(stristr($room_size,'zRoomDesc')){
                        $room_size_type = explode('=',$room_size);
                        $room_size_type_id = str_replace("zRoomDesc","",$room_size_type[0]);
                        $room_size_type_value = str_replace("\\","",$room_size_type[1]); 
                        $room_sizes[$room_size_type_id]['property_room_description'] = trim($room_size_type_value,"'");
                    }
                   

                }
                $propery_rooms[$PROPERTY_ID] = $room_sizes;
                
            }
            

            foreach( $propery_rooms as $PROPERTY_ID => $room_values ){ 
                        $sno = 0;
                      foreach($room_sizes as $room_size_type_id => $room_values2){
                        $sno++;
                        // echo '<pre>';
                        $room_id = $PROPERTY_ID.$sno;
                        $property_room_title = addslashes($room_values2['property_room_title']);
                        $title = preg_replace( '/[^[:print:]]/', '',$property_room_title);
                        $property_room_size = addslashes($room_values2['property_room_size']);
                        $size = preg_replace( '/[^[:print:]]/', '',$property_room_size);
                        $property_room_description = addslashes($room_values2['property_room_description']);
                        $description = preg_replace( '/[^[:print:]]/', '',$property_room_description);
                        // echo '<pre/>';
                        $Query = "INSERT INTO `property_rooms` (`property_id`, `property_room_title`, `property_room_size`, `property_room_description`, `room_id`) 
                                  VALUES ('$PROPERTY_ID', '$title', '$size', '$description', '$room_id') ";
                            $db_connect->queryExecute($Query) or die($Query);
                            // echo "<pre>"; print_r($room_id); echo "<pre/>";
                            }
            
        }
    }    
       echo"ROOMS ADDED SUCCESSFULLY";
}
?>