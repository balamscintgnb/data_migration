<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/stn_exit_upd/jet_stn_prp.xls';
// $file_name='../source_data/jet/stn_exit_upd/jet_stn_prp_a.xls';
// $file_name='../source_data/jet/stpr/jet_stn_prp.xls';
// $file_name='../source_data/jet/stpr/jet_stn_prp_a.xls';

$thisProceed=true;

try {
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}
//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();
// $total_rows = 10;

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$fname = 'negotiators.json';
$read = file_get_contents($fname);
$obj_negs = json_decode($read);
// echo $obj_negs->AKZ->name;

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$Get_catg = '';
			$splitted_cat = '';
			$Get_catg = trim($objPHPExcel->getActiveSheet()->getCell('BS'.$row)->getValue());
			$splitted_cat = str_split($Get_catg);

			for($rk=0; $rk < count($splitted_cat); $rk++)
			{
				$PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
				$PROPERTY_REF_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
				
				if($Get_catg == "LS" || $Get_catg == "SL")
				{
					if($splitted_cat[$rk] == 'S')
					{
						$PROPERTY_ID .= '_SALES';
						$PROPERTY_REF_ID .= '_SALES';
					}
				}

				$PROPERTY_LANDLORD_ID = trim($objPHPExcel->getActiveSheet()->getCell('BQ'.$row)->getValue());
					
				$PROPERTY_STAFF_ID = '';
				$STAFF_ID = trim($objPHPExcel->getActiveSheet()->getCell('BR'.$row)->getValue());
				$PROPERTY_STAFF_ID = ($STAFF_ID)?((isset($obj_negs->$STAFF_ID->e))?$obj_negs->$STAFF_ID->e:''):'';

				$PROPERTY_PRICE = '';
				$PROPERTY_VENDOR_PRICE = '';
				$PROPERTY_PRICE_FREQUENCY='';
				$PROPERTY_QUALIFIER='';
				$PROPERTY_CATEGORY='';
				$PROPERTY_TITLE='';
				// $PROPERTY_SHORT_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue()));
				$PROPERTY_SHORT_DESCRIPTION = '';
				$PROPERTY_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue()));
				$PROPERTY_AVAILABILITY = '';

				$R_price = $objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
				$Sale_price = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
				$Let_AVAILABILITY = trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
				$Sale_AVAILABILITY = trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
				// $Get_catg = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BS'.$row)->getValue()));
				$get_qualifier = '';
				$get_qualifier = trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
				
				// if($Get_catg == "S")
				if($splitted_cat[$rk] == "S")
				{
					$PROPERTY_CATEGORY='RESIDENTIAL SALES';
					$PROPERTY_PRICE = $Sale_price;


					if($get_qualifier == 'GP'){
						$PROPERTY_QUALIFIER='Guide price';
					}
					else if($get_qualifier == 'AP'){ 
						$PROPERTY_QUALIFIER='Fixed price';
					}
					else if($get_qualifier == 'FP'){ 
						$PROPERTY_QUALIFIER='Fixed price';
					}
					else if($get_qualifier == 'OO'){ 
						$PROPERTY_QUALIFIER='Offers over';
					}
					else if($get_qualifier == 'OE'){ 
						$PROPERTY_QUALIFIER='Offers in the region of';
					}
					else if($get_qualifier == 'OR'){ 
						$PROPERTY_QUALIFIER='Offers in the region of';
					}
					else if($get_qualifier == 'PA'){ 
						$PROPERTY_QUALIFIER='Price on application';
					}


					// CO - Completed 
					// EX - Exchanged/Missives Concluded 
					// FA - For Sale -Available 
					// FU - For Sale - Unavailable 
					// PA - Introduction/Pre Appraisal 
					// PV - Paid Valuation 
					// RE - Reserved 
					// SA - Under offer -Available 
					// SU - Under offer - Unavailable 
					// SE - Sold Externally 
					// VA - Coming to market/Market Appraisal/Valuation 
					// WD - Withdrawn 


					if($Sale_AVAILABILITY == 'SE'){
						$PROPERTY_AVAILABILITY = 'external';
					}
					else if($Sale_AVAILABILITY == 'WD' || $Sale_AVAILABILITY == 'CO' || $Sale_AVAILABILITY == 'FU' || $Sale_AVAILABILITY == 'SU'){
						$PROPERTY_AVAILABILITY = 'Withdrawn';
					}
					else if($Sale_AVAILABILITY == 'EX'){
						$PROPERTY_AVAILABILITY = 'exchanged';
					}
					else if($Sale_AVAILABILITY == 'FA' || $Sale_AVAILABILITY == 'SA'){
						$PROPERTY_AVAILABILITY = 'Available';
					}
					else if($Sale_AVAILABILITY == 'PA' || $Sale_AVAILABILITY == 'VA' || $Sale_AVAILABILITY == 'PV'){
						$PROPERTY_AVAILABILITY = 'Appraisal';
					}
					else if($Sale_AVAILABILITY == 'RE'){
						$PROPERTY_AVAILABILITY = 'Reserved';
					}
					else if($Sale_AVAILABILITY == 'EX'){
						$PROPERTY_AVAILABILITY = 'exchanged';
					}
					

					$PROPERTY_PRICE_TO = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
					$PROPERTY_PRICE_FROM = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()));
					$PROPERTY_VENDOR_PRICE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));

				}        
				else
				{
					$PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
					$PROPERTY_PRICE = $R_price;
					$PROPERTY_PRICE_FREQUENCY='pw'; 

					// LA -To Let -Available 
					// LU -To Let - Unavailable 
					// LO - Let by other agent 
					// LP - Let privately 
					// MA - Coming to market / Market Appraisal / Valuation 
					// PR - Provisional 
					// SO - Sold 
					// AA -Arranging Tenancy -Available 
					// TA -Arranging Tenancy - Unavailable 
					// AC -Tenancy Current -Available 
					// TC -Tenancy Current - Unavailable 
					// TF -Tenancy Finished 
					// TX -Tenancy Cancelled 
					// UA - Under Offer -Available 
					// UU - Under Offer - Unavailable 
					// WD - Withdrawn 

					// rk commented & added the following on 2019-02-07 15-55 for setting letting status based on the JET Software Doc.
					// if($Let_AVAILABILITY == 'AA' || $Let_AVAILABILITY == 'LA' || $Let_AVAILABILITY == 'AC' || $Let_AVAILABILITY == 'UA' || $Let_AVAILABILITY == 'PR'){
					// 	$PROPERTY_AVAILABILITY = 'Available';
					// }
					// else if($Let_AVAILABILITY == 'LO'){
					// 	$PROPERTY_AVAILABILITY = 'external';
					// }
					// else if($Let_AVAILABILITY == 'LP' || $Let_AVAILABILITY == 'SO'){
					// 	$PROPERTY_AVAILABILITY = 'Let';
					// }
					// else if($Let_AVAILABILITY == 'MA'){
					// 	$PROPERTY_AVAILABILITY = 'Appraisal';
					// }
					// else if($Let_AVAILABILITY == ''){
					// 	$PROPERTY_AVAILABILITY = 'Let Agreed';
					// }
					// else if($Let_AVAILABILITY == 'WD' || $Let_AVAILABILITY == 'LU' || $Let_AVAILABILITY == 'TA' || $Let_AVAILABILITY == 'UU' || $Let_AVAILABILITY == 'TF' || $Let_AVAILABILITY == 'TC'){
					// 	$PROPERTY_AVAILABILITY = 'Withdrawn';
					// }

					if($Let_AVAILABILITY == 'LA' || $Let_AVAILABILITY == 'PR'){
						$PROPERTY_AVAILABILITY = 'Available';
					}
					else if($Let_AVAILABILITY == 'LO'){
						$PROPERTY_AVAILABILITY = 'external';
					}
					else if($Let_AVAILABILITY == 'LP' || $Let_AVAILABILITY == 'SO' || $Let_AVAILABILITY == 'AA' || $Let_AVAILABILITY == 'AC' || $Let_AVAILABILITY == 'TC'){
						$PROPERTY_AVAILABILITY = 'Let';
					}
					else if($Let_AVAILABILITY == 'MA'){
						$PROPERTY_AVAILABILITY = 'Appraisal';
					}
					// else if($Let_AVAILABILITY == 'AA'){
					// 	$PROPERTY_AVAILABILITY = 'Let Agreed';
					// }
					else if($Let_AVAILABILITY == 'UA' || $Let_AVAILABILITY == 'UU'){
						$PROPERTY_AVAILABILITY = 'Under Offer';
					}
					else if($Let_AVAILABILITY == 'WD' || $Let_AVAILABILITY == 'LU' || $Let_AVAILABILITY == 'TA' || $Let_AVAILABILITY == 'TF' || $Let_AVAILABILITY == 'TX'){
						$PROPERTY_AVAILABILITY = 'Withdrawn';
					}


					$PROPERTY_PRICE_TO = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue()));
					$PROPERTY_PRICE_FROM = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue()));


				}
				
				// echo"<pre>"; print_r($row."=".$Get_catg."="); echo"<pre/>";
				$PROPERTY_AVAILABLE_DATE="NULL";
				$Get_date_1 = trim($objPHPExcel->getActiveSheet()->getCell('BM'.$row)->getformattedValue());
				if(stristr($Get_date_1, "\N") || $Get_date_1 == '' )
				{
					$PROPERTY_AVAILABLE_DATE = '0000-00-00';
				}
				else
				{
					$PROPERTY_AVAILABLE_DATE = date('Y-m-d H:i:s',strtotime($Get_date_1));
				}
				
				// rk added the following archive date is to check whether we gotta move the property to archive or else let it be as active
				$PROPERTY_STATUS='Active';
				$PROPERTY_ARCHIVE_DATE="";
				$get_arch_date = trim($objPHPExcel->getActiveSheet()->getCell('BP'.$row)->getformattedValue());
				if(!stristr($get_arch_date, "\N") || $get_arch_date != '' )
				{
					$CUR_DATE_STT = strtotime(date('Y-m-d'));
					$PROPERTY_ARCHIVE_DATE_STT = strtotime($get_arch_date);
					if($PROPERTY_ARCHIVE_DATE_STT <= $CUR_DATE_STT)
					{
						$PROPERTY_STATUS='';
					}
				}

				$PROPERTY_ADDRESS_LINE_1='';

				$LINE = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
				$LINE2 = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
				$LINE3 = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
				$PROPERTY_ADDRESS_LINE_1 = trim($LINE);
				
				if($LINE2)
				{
					if($PROPERTY_ADDRESS_LINE_1)
					{
						$PROPERTY_ADDRESS_LINE_1.= ', ';
					}
					$PROPERTY_ADDRESS_LINE_1.= $LINE2; 
				}
				
				if($LINE3)
				{
					if($PROPERTY_ADDRESS_LINE_1)
					{
						$PROPERTY_ADDRESS_LINE_1.= ', ';
					}
					$PROPERTY_ADDRESS_LINE_1.= $LINE3;
				}

				$PROPERTY_ADDRESS_LINE_1 = addslashes(trim($PROPERTY_ADDRESS_LINE_1));
				$PROPERTY_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
				$PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
				$PROPERTY_ADDRESS_COUNTY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
				$PROPERTY_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));

		   
				$PROPERTY_FORMATTED_ADDRESS='';
				$STATUS='';
				

				$PROPERTY_ADMIN_FEES='';
				$PROPERTY_TYPE = '';
				

				$PROPERTY_BEDROOMS = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue()));
				$PROPERTY_BATHROOMS = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue()));
				$PROPERTY_RECEPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue()));
				$PROPERTY_TENURE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue()));


				if($PROPERTY_TENURE == 'F'){
					$PROPERTY_TENURE = 'Freehold';
				}
				else if($PROPERTY_TENURE == 'L'){
					$PROPERTY_TENURE = 'Leasehold';
				}
				else if($PROPERTY_TENURE == 'S'){
					$PROPERTY_TENURE = 'Share of Freehold';
				}
				else{
					$PROPERTY_TENURE = '';
				}


				$Lease_term_years='';
				
				
				$CLASSIFICATION=trim($objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue());
				$PROPERTY_CLASSIFICATION='';
				if($CLASSIFICATION == '1')
				{
					$PROPERTY_CLASSIFICATION='Long Let';
				}
				else if($CLASSIFICATION == '2')
				{
					$PROPERTY_CLASSIFICATION='Short Let';
				}



				$PROPERTY_CURRENT_OCCUPANT='';
				$KITCHEN_DINER='';
				$OFF_ROAD_PARKING='';
				$ON_ROAD_PARKING='';
				$GARDEN='';
				$WHEELCHAIR_ACCESS='';
				$ELEVATOR_IN_BUILDING='';
				$POOL='';
				$GYM='';
				$DINING_ROOM='';
				$FURNISHED = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue()));

				if($FURNISHED !=''){
					if($FURNISHED !=0){
						$FURNISHED = '1';
					} else{
						$FURNISHED = '0';
					}
				}
				$KITCHEN='';
				$Toilets='';
				$INTERNET='';
				$WIRELESS_INTERNET='';
				$TV='';

				$WASHER='';
				$DRYER='';
				$DISHWASHER='';
				$PETS_ALLOWED='';
				$FAMILY_OR_CHILD_FRIENDLY='';
				$DSS_ALLOWED='';
				$SMOKING_ALLOWED='';
				$SECURITY='';
				$HOT_TUB='';

				$CLEANER='';
				$EN_SUITE='';
				$SECURE_CAR_PARKING='';
				$OPEN_PLAN_LOUNGE='';
				$VIDEO_DOOR_ENTRY='';
				$CONCIERGE_SERVICES='';
				$PROPERTY_CUSTOM_FEATURES='';
				$PROPERTY_ROOMS='';
				$PROPERTY_ASSETS='';

				$PROPERTY_IMAGE_1='';
				$PROPERTY_IMAGE_2='';
				$PROPERTY_IMAGE_3='';
				$PROPERTY_IMAGE_4='';
				$PROPERTY_IMAGE_5='';
				$PROPERTY_IMAGE_6='';
				$PROPERTY_IMAGE_7='';
				$PROPERTY_IMAGE_8='';
				$PROPERTY_IMAGE_9='';

				$PROPERTY_IMAGE_10='';
				$PROPERTY_IMAGE_11='';
				$PROPERTY_IMAGE_12='';
				$PROPERTY_IMAGE_13='';
				$PROPERTY_IMAGE_14='';
				$PROPERTY_IMAGE_15='';
				$PROPERTY_IMAGE_FLOOR_1='';
				$PROPERTY_IMAGE_FLOOR_2='';
				$PROPERTY_IMAGE_FLOOR_3='';
				$PROPERTY_IMAGE_FLOOR_4='';
				$PROPERTY_IMAGE_FLOOR_5='';
				$PROPERTY_IMAGE_EPC_1='';
				$PROPERTY_IMAGE_EPC_2='';
				$PROPERTY_IMAGE_EPC_3='';
				$PROPERTY_IMAGE_EPC_4='';
				$PROPERTY_IMAGE_EPC_5='';
				$PROPERTY_EPC_VALUES='';
				// $CERTIFICATE_EXPIRE_DATE='NULL';
				$PROPERTY_AGE = '';
				$PROPERTY_CONDITION = '';
				$PROPERTY_PROPOSED_PRICE = '';

			   
					
				/////////////////////////////ADD MORE FIELDS/////////////////////////

				if($PROPERTY_CATEGORY == 'RESIDENTIAL SALES')
				{
					$Query1="SELECT * FROM `clients` WHERE `CLIENTID` = '$PROPERTY_LANDLORD_ID' AND CLIENT_TYPE = 'vendor'";
					$chk_Insert_vendord = json_decode($db_connect->queryFetch($Query1),true);
					if(isset($chk_Insert_vendord['data']) && count($chk_Insert_vendord['data'])<=0)
					{
						$Query2="SELECT * FROM `clients` WHERE `CLIENTID` = '$PROPERTY_LANDLORD_ID' AND CLIENT_TYPE = 'ADDRESSBOOK'";
						$Insert_vendord2 = json_decode($db_connect->queryFetch($Query2),true);
						if(isset($Insert_vendord2['data']) && count($Insert_vendord2['data'])>0)
						{
							$update_qry = "UPDATE clients SET CLIENT_TYPE = 'vendor' WHERE CLIENTID = '" . $PROPERTY_LANDLORD_ID . "' AND CLIENT_TYPE = 'ADDRESSBOOK'";
							$db_connect->queryExecute($update_qry) or die($update_qry);
						}
						else
						{
							$Query1="SELECT * FROM `clients` WHERE `CLIENTID` = '$PROPERTY_LANDLORD_ID'";
							$Insert_vendord = json_decode($db_connect->queryFetch($Query1),true);
							if(isset($Insert_vendord['data']) && count($Insert_vendord['data'])>0)
							{
								$CLIENT_NAME = addslashes($Insert_vendord['data'][0]['CLIENT_NAME']);
								$CLIENT_TYPE = 'vendor';
								$CLIENT_PRIMARY_EMAIL = addslashes($Insert_vendord['data'][0]['CLIENT_PRIMARY_EMAIL']);
								$CLIENT_EMAIL_1 = addslashes($Insert_vendord['data'][0]['CLIENT_EMAIL_1']);
								$CLIENT_EMAIL_2 = addslashes($Insert_vendord['data'][0]['CLIENT_EMAIL_2']);
								$CLIENT_EMAIL_3 = addslashes($Insert_vendord['data'][0]['CLIENT_EMAIL_3']);
								$CLIENT_EMAIL_4 = addslashes($Insert_vendord['data'][0]['CLIENT_EMAIL_4']);
								$CLIENT_EMAIL_5 = addslashes($Insert_vendord['data'][0]['CLIENT_EMAIL_5']);
								$CLIENT_PRIMARY_PHONE = addslashes($Insert_vendord['data'][0]['CLIENT_PRIMARY_PHONE']);
								$CLIENT_ADDRESS_LINE_1 = addslashes($Insert_vendord['data'][0]['CLIENT_ADDRESS_LINE_1']);
								$CLIENT_ADDRESS_LINE_2 = addslashes($Insert_vendord['data'][0]['CLIENT_ADDRESS_LINE_2']);
								$CLIENT_ADDRESS_CITY = addslashes($Insert_vendord['data'][0]['CLIENT_ADDRESS_CITY']);
								$CLIENT_ADDRESS_TOWN = addslashes($Insert_vendord['data'][0]['CLIENT_ADDRESS_TOWN']);
								$CLIENT_ADDRESS_POSTCODE = addslashes($Insert_vendord['data'][0]['CLIENT_ADDRESS_POSTCODE']);
								$CLIENT_ACCOUNT_NAME = addslashes($Insert_vendord['data'][0]['CLIENT_ACCOUNT_NAME']);
								$CLIENT_ACCOUNT_NO = addslashes($Insert_vendord['data'][0]['CLIENT_ACCOUNT_NO']);

								$CLIENT_ACCOUNT_SORTCODE = addslashes($Insert_vendord['data'][0]['CLIENT_ACCOUNT_SORTCODE']);
								$CLIENT_PHONE_1 = addslashes($Insert_vendord['data'][0]['CLIENT_PHONE_1']);
								$CLIENT_PHONE_2 = addslashes($Insert_vendord['data'][0]['CLIENT_PHONE_2']);
								$CLIENT_PHONE_3 = addslashes($Insert_vendord['data'][0]['CLIENT_PHONE_3']);
								$CLIENT_PHONE_4 = addslashes($Insert_vendord['data'][0]['CLIENT_PHONE_4']);
								$CLIENT_PHONE_5 = addslashes($Insert_vendord['data'][0]['CLIENT_PHONE_5']);
								$CLIENT_NOTES = addslashes($Insert_vendord['data'][0]['CLIENT_NOTES']);
								// echo "<pre>"; print_r($CLIENT_PRIMARY_EMAIL); echo "<pre/>";
								
								$Query2 = "INSERT INTO `clients` (`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`,
									`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`,
									`CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`,
									`CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
									`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`,
									`CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`) VALUES ('$PROPERTY_LANDLORD_ID', '', '$CLIENT_NAME', '$CLIENT_TYPE', '', '', '', '$CLIENT_PRIMARY_EMAIL', '',
									'$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '', '', '', '', '', '', '', '', '', '', '$CLIENT_ACCOUNT_NAME',
									'$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3',
									'$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '', '', '', '', NULL, '', '', '', '', '', '', NULL)";
								$db_connect->queryExecute($Query2) or die($Query2);
							}
						}
					}
				}


				//////////////////////////// ADD QUERY //////////////////////////

				if($PROPERTY_AVAILABILITY == 'Appraisal')
				{
					$PROPERTY_VALUATION_STATUS = 0;

					$PROPERTY_VALUER_NOTES = '';
					$PROPERTY_VALUER_NOTES = trim($objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue());
					if($PROPERTY_VALUER_NOTES!='')
					{
						$PROPERTY_VALUER_NOTES =  addslashes(format_array(preg_split ('/$\R?^/m', trim($PROPERTY_VALUER_NOTES))));
					}

					$sql_val = "SELECT * FROM `valuations` WHERE `PROPERTY_ID` LIKE '%$PROPERTY_ID%' AND `PROPERTY_CATEGORY` = '$PROPERTY_CATEGORY'";
					$sql_val_res = json_decode($db_connect->queryFetch($sql_val),true);

					if(count($sql_val_res['data'])<=0)
					{
						$Query4 = "INSERT INTO `valuations` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`,
							`PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`,
							`PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`,
							`PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`, `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`,
							`PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`, `PROPERTY_APPOINTMENT_FOLLOWUPDATE`) 
							VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_LANDLORD_ID', '$PROPERTY_CATEGORY', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY',
							'$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_VALUATION_STATUS', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION',
							'$PROPERTY_TENURE', '$PROPERTY_AGE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CONDITION', '$PROPERTY_PRICE_FROM', '$PROPERTY_PRICE_TO', '$PROPERTY_VENDOR_PRICE',
							 '$PROPERTY_PROPOSED_PRICE', '$PROPERTY_VALUER_NOTES', '', '', '', '', '', NULL, NULL, NULL) ";
						$db_connect->queryExecute($Query4) or die($row."<br>".$Query4);
					}
				}
				else
				{
					// $Query3 = "SELECT * FROM `properties` WHERE `PROPERTY_ID` = '$PROPERTY_ID'";
					// $Query3 = "SELECT * FROM `properties` WHERE `PROPERTY_ID` LIKE '%$PROPERTY_ID%'";
					$Query3 = "SELECT * FROM `properties` WHERE `PROPERTY_ID` LIKE '%$PROPERTY_ID%' AND `PROPERTY_CATEGORY` = '$PROPERTY_CATEGORY'";
					$FILTER_PRO = json_decode($db_connect->queryFetch($Query3),true);
					// $FILTER_PROPERTY = (isset($FILTER_PRO['data'][0]))?$FILTER_PRO['data'][0]['PROPERTY_ID']:'';
					// if($PROPERTY_ID != $FILTER_PROPERTY)
					if(count($FILTER_PRO['data'])<=0)
					{

						$PROPERTY_CREATED_ON = 'NULL';
						$PROPERTY_VENDOR_SOLICITOR_ID = '';
						$PROPERTY_BUYER_SOLICITOR_ID = '';
						$PROPERTY_BUYER_ID = '';
						$INSTRUCTED_DATE = '';

// CC - Collect first rent payment - 3
// CO - Letting only 		- 2
// IT - Introducing tenant 	- 2
// MT - Managed tenancy 		- 1
// RC - Rent collection 		- 3

						$PROPERTY_LETTING_SERVICE = '';
						$PROPERTY_LETTING_SERVICE = trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
						if($PROPERTY_LETTING_SERVICE!='')
						{
							if(trim($PROPERTY_LETTING_SERVICE) == 'MT')
							{
								$PROPERTY_LETTING_SERVICE = '1';
							}
							else if(trim($PROPERTY_LETTING_SERVICE) == 'CO' || trim($PROPERTY_LETTING_SERVICE) == 'IT')
							{
								$PROPERTY_LETTING_SERVICE = '2';
							}
							else if(trim($PROPERTY_LETTING_SERVICE) == 'RC' || trim($PROPERTY_LETTING_SERVICE) == 'CC')
							{
								$PROPERTY_LETTING_SERVICE = '3';
							}
						}
						

						$PROPERTY_LETTING_FEE = trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue());
						$PROPERTY_LETTING_FEE_TYPE = '';

						if($PROPERTY_LETTING_FEE != '')
						{
							$PROPERTY_LETTING_FEE_TYPE='2';
						}

						$PROPERTY_LETTING_FEE_FREQUENCY = '';
						$PROPERTY_MANAGEMENT_FEE = '';
						$PROPERTY_MANAGEMENT_FEE_TYPE = '';
						$PROPERTY_MANAGEMENT_FEE_FREQUENCY = ''; 

						$PROPERTY_NOTES = '';
						$PROPERTY_NOTES = trim($objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue());
						if($PROPERTY_NOTES!='')
						{
							$PROPERTY_NOTES =  addslashes(format_array(preg_split ('/$\R?^/m', trim($PROPERTY_NOTES))));
						}

						$CERTIFICATE_EXPIRE_DATE='NULL';
						// if want to uncomment this, certificates.php scripts must have been executed to import the data from "jet_stn_cert.csv" to certificates tbl
						// $CERTIFICATE_EXPIRE_DATE_QRY = "SELECT a.CERTIFICATE_END_DATE FROM certificates a WHERE a.CERTIFICATE_END_DATE = ( SELECT MAX(CERTIFICATE_END_DATE) FROM certificates b WHERE b.CERTIFICATE_PRPCODE = a.CERTIFICATE_PRPCODE AND b.CERTIFICATE_PRPCODE = '" . $PROPERTY_ID . "' ORDER BY b.CERTIFICATE_END_DATE DESC LIMIT 1 )";
						//     $CERTIFICATE_EXPIRE_DATE_QRY_RES = json_decode($db_connect->queryFetch($CERTIFICATE_EXPIRE_DATE_QRY),true);
						//     if(isset($CERTIFICATE_EXPIRE_DATE_QRY_RES['data']) && count($CERTIFICATE_EXPIRE_DATE_QRY_RES['data'])>0)
						//     {
						//         $CERTIFICATE_EXPIRE_DATE = "'" . json_encode(array(1=>trim($CERTIFICATE_EXPIRE_DATE_QRY_RES['data'][0]['CERTIFICATE_END_DATE']))) . "'";
						//     }

						$sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
											`PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
											`PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
											`PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
											`PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
											`KITCHEN_DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
											`DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
											`DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
											`CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
											`PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
											`PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
											`PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
											`CERTIFICATE_EXPIRE_DATE`, `PROPERTY_EPC_VALUES`, `PROPERTY_ROOMS`,`PROPERTY_ASSETS`,`PROPERTY_CREATED_ON`,  `PROPERTY_VENDOR_SOLICITOR_ID`,  `PROPERTY_BUYER_SOLICITOR_ID`,  `PROPERTY_BUYER_ID`,  `PROPERTY_NOTES`,  `INSTRUCTED_DATE`,  `PROPERTY_LETTING_SERVICE`,  `PROPERTY_LETTING_FEE`,  `PROPERTY_LETTING_FEE_TYPE`,  `PROPERTY_LETTING_FEE_FREQUENCY`,  `PROPERTY_MANAGEMENT_FEE`,  `PROPERTY_MANAGEMENT_FEE_TYPE`,  `PROPERTY_MANAGEMENT_FEE_FREQUENCY`) 
											VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_LANDLORD_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
											'$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE' ,
											'$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
											'$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
											'$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
											'$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
											'$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
											'$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
											'$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
											'$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
											'$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
											'$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
											'$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
											'$PROPERTY_IMAGE_EPC_5', $CERTIFICATE_EXPIRE_DATE, '$PROPERTY_EPC_VALUES', '$PROPERTY_ROOMS','$PROPERTY_ASSETS',$PROPERTY_CREATED_ON,'$PROPERTY_VENDOR_SOLICITOR_ID','$PROPERTY_BUYER_SOLICITOR_ID','$PROPERTY_BUYER_ID','$PROPERTY_NOTES','$INSTRUCTED_DATE','$PROPERTY_LETTING_SERVICE','$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY','$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY')";

						$db_connect->queryExecute($sql) or die($row."<br>".$sql);
					}	// if(sizeof($FILTER_PRO['data'])<=0) ends here
				}	// if($PROPERTY_AVAILABILITY == 'Appraisal') else ends here
			}	// for $rk ends here
		}	// if($row>1) ends here
	}	// for($row =1; $row <= $total_rows; $row++) ends here
	echo "PROPERTIES INSERTED SUCCESSFULLY";
}	// if($thisProceed) ends here

function format_array(array $PROPERTY_NOTES_SPLITTED)
{
	foreach( $PROPERTY_NOTES_SPLITTED as $key => $value )
	{
		if( trim($value) == "" )
		{
			unset($PROPERTY_NOTES_SPLITTED[$key]);
		}
	}
	return json_encode(array_values($PROPERTY_NOTES_SPLITTED));
	// return json_encode(array_values($PROPERTY_NOTES_SPLITTED),JSON_UNESCAPED_UNICODE);
}

?>