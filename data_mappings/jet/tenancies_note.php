<?php
// rk created this file on 2019-01-17 12-47 for fetching tenancy notes and update in tenancies table
error_reporting(E_ALL);
ini_set('display_errors', 1); 

require_once '../../header_init.php';
	$ins_count = 1;
	$sql = "SELECT DISTINCT PROPERTY_ID FROM `notes` WHERE ASSOC_TYPE = 'ten' AND PROPERTY_ID <>''";
	$ten_exists = json_decode($db_connect->queryFetch($sql),true);
	if($ten_exists['success'] == 1)
	{
		foreach ($ten_exists['data'] as $key => $value)
		{
			if(trim($value['PROPERTY_ID']) != '')
			{
				$chk_assoc_exist_sql = "SELECT DISTINCT ASSOC_CODE FROM `notes` WHERE PROPERTY_ID = '" . $value['PROPERTY_ID'] . "' AND ASSOC_TYPE = 'ten' ORDER BY ASSOC_CODE, CONTACT_NOTES_DATE DESC";
				$assoc_exists = json_decode($db_connect->queryFetch($chk_assoc_exist_sql),true);
				if($assoc_exists['success'] == 1)
				{
					foreach ($assoc_exists['data'] as $assoc_key => $assoc_value)
					{
						if(trim($assoc_value['ASSOC_CODE']) != '')
						{
							$notes_array = array();
							/*
							$chk_notes_exist_sql = "SELECT * FROM `notes` WHERE PROPERTY_ID = '" . $value['PROPERTY_ID'] . "' AND ASSOC_CODE = '" . trim($assoc_value['ASSOC_CODE']) . "' AND ASSOC_TYPE = 'ten' ORDER BY CONTACT_NOTES_DATE DESC";
							$let_exists = json_decode($db_connect->queryFetch($chk_notes_exist_sql),true);
							if($let_exists['success'] == 1)
							{
								$CONTACT_NOTES = '';
								foreach ($let_exists['data'] as $let_key => $let_value)
								{
									if(trim($let_value['CONTACT_NOTES']) != '')
									{
										$notes_array[$let_value['id']][] = trim($let_value['CONTACT_NOTES']);
										
										// if(strlen($CONTACT_NOTES)>0)
										// {
										// 	// $CONTACT_NOTES .= '\n\n' . trim($let_value['CONTACT_NOTES']);
										// }
										// else
										// {
										// 	$CONTACT_NOTES = trim($let_value['CONTACT_NOTES']);
										// }
										
									}
								}
									
									$LETTING_NOTES = addslashes(json_encode($notes_array));
									$PROPERTY_ID = $value['PROPERTY_ID'];
									$LETTING_ID = $assoc_value['ASSOC_CODE'];
									$ins_sql ="INSERT INTO `letting_notes` (`PROPERTY_ID`, `LETTING_ID`, `LETTING_NOTES`) VALUES ('$PROPERTY_ID', '$LETTING_ID', '$LETTING_NOTES')";
									// $ins_sql ="INSERT INTO `tenancies_notes` (`PROPERTY_ID`, `ASSOC_CODE`, `CONTACT_NOTES`) VALUES ('$PROPERTY_ID', '$ASSOC_CODE', '$CONTACT_NOTES')";
									$db_connect->queryExecute($ins_sql) or die ($ins_sql);
									$ins_count++;
									unset($notes_array);
							}
							*/
									// $sql = "SELECT PROPERTY_ID FROM `notes` WHERE ASSOC_CODE = '" . trim($assoc_value['ASSOC_CODE']) . "' AND ASSOC_TYPE = 'ten' ORDER BY CONTACT_NOTES_DATE DESC ";
									// $prpcode_exists = json_decode($db_connect->queryFetch($sql),true);
									// if(strlen(trim(@$prpcode_exists['data'][0]['PROPERTY_ID']))>0 && trim(@$prpcode_exists['data'][0]['PROPERTY_ID']))
									// {

									// }
							
								$sql_sess = "SET SESSION group_concat_max_len = 100000000";
								$db_connect->queryExecute($sql_sess) or die ($sql_sess);
								$chk_notes_exist_sql = "SELECT GROUP_CONCAT(CONTACT_NOTES ORDER BY CONTACT_NOTES_DATE DESC SEPARATOR '\n\n') as CONTACT_NOTES FROM `notes` WHERE ASSOC_CODE = '" . trim($assoc_value['ASSOC_CODE']) . "' AND ASSOC_TYPE = 'ten' GROUP BY ASSOC_CODE ORDER BY CONTACT_NOTES_DATE DESC";
								// $chk_notes_exist_sql = "SELECT GROUP_CONCAT(CONTACT_NOTES ORDER BY CONTACT_NOTES_DATE DESC SEPARATOR '\n\n') as CONTACT_NOTES FROM `notes` WHERE PROPERTY_ID = '" . $value['PROPERTY_ID'] . "' AND ASSOC_CODE = '" . trim($assoc_value['ASSOC_CODE']) . "' AND ASSOC_TYPE = 'ten' GROUP BY PROPERTY_ID, ASSOC_CODE ORDER BY CONTACT_NOTES_DATE DESC";
								$let_exists = json_decode($db_connect->queryFetch($chk_notes_exist_sql),true);
								if($let_exists['success'] == 1)
								{
									$notes_array[] = $let_exists['data'][0]['CONTACT_NOTES'];
									$LETTING_NOTES = addslashes(json_encode($notes_array));
									
									$PROPERTY_ID = $value['PROPERTY_ID'];
									$LETTING_ID = $assoc_value['ASSOC_CODE'];
									$ins_sql ="INSERT INTO `letting_notes` (`PROPERTY_ID`, `LETTING_ID`, `LETTING_NOTES`) VALUES ('$PROPERTY_ID', '$LETTING_ID', '$LETTING_NOTES')";
									// $ins_sql ="INSERT INTO `tenancies_notes` (`PROPERTY_ID`, `ASSOC_CODE`, `CONTACT_NOTES`) VALUES ('$PROPERTY_ID', '$ASSOC_CODE', '$CONTACT_NOTES')";
									$db_connect->queryExecute($ins_sql) or die ($ins_sql);
									$ins_count++;
									unset($notes_array);
								}
						}
					}
				}
				
			}
		}
		echo $ins_count . " TENANCY NOTES INSERTED SUCCESSFULLY"; //	2 has been deducted from row var since row 	
	}

// table structure

// CREATE TABLE `tenancies_notes` (
//  `id` bigint(20) NOT NULL AUTO_INCREMENT,
//  `PROPERTY_ID` varchar(255) DEFAULT NULL,
//  `ASSOC_CODE` varchar(255) DEFAULT NULL,
//  `ASSOC_TYPE` varchar(255) DEFAULT NULL,
//  `NEG_CODE` varchar(255) DEFAULT NULL,
//  `NEG_NAME` varchar(255) DEFAULT NULL,
//  `CONTACT_TYPE` varchar(255) DEFAULT NULL,
//  `CONTACT_NOTES` longtext,
//  `CONTACT_NOTES_DATE` datetime DEFAULT NULL,
//  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
//  PRIMARY KEY (`id`)
// ) ENGINE=MyISAM DEFAULT CHARSET=latin1
?>