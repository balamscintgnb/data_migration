<?php
// rk created this file on 2019-01-21 12-47 for fetching applicant client notes and update in clients table - this is only for applicants files
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jet/Ignored Files/jet_stn_jnl.csv';
$file_name_2='../source_data/jet/Ignored Files/jet_stn_negs.csv';

$thisProceed=true;

try
{
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
		if(pathinfo($file_name_2, PATHINFO_EXTENSION)=='csv')
		{
			$objReader_2 = PHPExcel_IOFactory::createReader('CSV');
			$objPHPExcel_2 = $objReader_2->load($file_name_2);

			$sheet_2 = $objPHPExcel_2->getSheet(0);
			$total_rows_2 = $sheet_2->getHighestRow();
		}
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$notes_updated_count = 0;

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
			// if($PROPERTY_ID!='')
			// {
				$NEG_CODE = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
				$NEG_NAME = ''; 
				for($row_2 =1; $row_2 <= $total_rows_2; $row_2++)
				{
					if($row_2>1)
					{
						$NEG_CODE_CMP = trim($objPHPExcel_2->getActiveSheet()->getCell('C'.$row_2)->getValue());
						if(trim($NEG_CODE) == trim($NEG_CODE_CMP))
						{
							$NEG_NAME = trim($objPHPExcel_2->getActiveSheet()->getCell('D'.$row_2)->getValue()); 
						}
					}
				}

				if($NEG_NAME == '')
				{
					$NEG_NAME = $NEG_CODE; 
				}

				// echo "<br/><br/>";
				$CONTACT_TYPE = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
				$format_date = '';
				// echo "<br/><br/>";
				$CONTACT_NOTES_DATE = trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());

				// if($PROPERTY_ID == 'SPH170156' && $NEG_CODE == 'RUT' && $CONTACT_TYPE == 'PH')
				// {
				// 	echo "con date :" . $CONTACT_NOTES_DATE;
				// 	echo "<br/>";
				// 	echo "con notes :" . trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
				// 	echo "<br/><br/><br/>";
				// }

				if(trim($CONTACT_NOTES_DATE) != '' )
				{
					$format_date = date("d/m/Y", strtotime(trim($CONTACT_NOTES_DATE)));
					// if(DateTime::createFromFormat('Y-m-d H:i:s', $CONTACT_NOTES_DATE) !== FALSE)
					// {
					// }
					// else
					// {
					// 	$CONTACT_NOTES_DATE = '0000-00-00';
					// }
				}
				else
				{
					$CONTACT_NOTES_DATE = '0000-00-00';
				}
				
				$notes =  trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());

				if($format_date != '')
				{
					$CONTACT_NOTES = $format_date . " " . $NEG_NAME . " - " . $notes;
				}
				else
				{
					$CONTACT_NOTES = $NEG_NAME . " - " . $notes;
				}
				// echo $CONTACT_NOTES;

	
				$sql ="INSERT INTO `notes` (`PROPERTY_ID`, `NEG_CODE`, `NEG_NAME`, `CONTACT_TYPE`, `CONTACT_NOTES`, `CONTACT_NOTES_DATE`) VALUES ('$PROPERTY_ID', '$NEG_CODE', '$NEG_NAME', '$CONTACT_TYPE', '$CONTACT_NOTES', '$CONTACT_NOTES_DATE')";
				$db_connect->queryExecute($sql) or die ($sql);
				$sql1 ="INSERT INTO `notes_jnl_ony` (`PROPERTY_ID`, `NEG_CODE`, `NEG_NAME`, `CONTACT_TYPE`, `CONTACT_NOTES`, `CONTACT_NOTES_DATE`) VALUES ('$PROPERTY_ID', '$NEG_CODE', '$NEG_NAME', '$CONTACT_TYPE', '$CONTACT_NOTES', '$CONTACT_NOTES_DATE')";
				$db_connect->queryExecute($sql1) or die ($sql1);
				
				// ($db_connect->queryExecute($sql1)) ? ($x++) : die ($sql1);
				
				// if($db_connect->queryExecute($sql1))
				// 	$x++;
				// else
				// 	die($sql);

				
				// if($db_connect->queryExecute($sql1))
				// {
				// 	$x++;
				// }
				// else
				// {
				// 	die($sql);
				// }
				// exit();
				$notes_updated_count++;
			// }	// if($PROPERTY_ID!='') ends here
		}	// $row if ends here
	}	// for loop ends here
	echo "IN " . ($row - 2) . " ROUNDS " . $notes_updated_count . " NOTES UPDATED SUCCESSFULLY"; //	2 has been deducted from row var since row 
}

// table structure

//  	CREATE TABLE `notes` (
//  `id` bigint(20) NOT NULL AUTO_INCREMENT,
//  `PROPERTY_ID` varchar(255) DEFAULT NULL,
//  `ASSOC_CODE` varchar(255) DEFAULT NULL,
//  `ASSOC_TYPE` varchar(255) DEFAULT NULL,
//  `NEG_CODE` varchar(255) DEFAULT NULL,
//  `NEG_NAME` varchar(255) DEFAULT NULL,
//  `CONTACT_TYPE` varchar(255) DEFAULT NULL,
//  `CONTACT_NOTES` longtext,
//  `CONTACT_NOTES_DATE` datetime DEFAULT NULL,
//  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
//  PRIMARY KEY (`id`)
// ) ENGINE=MyISAM AUTO_INCREMENT=95600 DEFAULT CHARSET=latin1

//  	CREATE TABLE `notes_jnl_ony` (
//  `id` bigint(20) NOT NULL AUTO_INCREMENT,
//  `PROPERTY_ID` varchar(255) DEFAULT NULL,
//  `ASSOC_CODE` varchar(255) DEFAULT NULL,
//  `ASSOC_TYPE` varchar(255) DEFAULT NULL,
//  `NEG_CODE` varchar(255) DEFAULT NULL,
//  `NEG_NAME` varchar(255) DEFAULT NULL,
//  `CONTACT_TYPE` varchar(255) DEFAULT NULL,
//  `CONTACT_NOTES` longtext,
//  `CONTACT_NOTES_DATE` datetime DEFAULT NULL,
//  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
//  PRIMARY KEY (`id`)
// ) ENGINE=MyISAM AUTO_INCREMENT=57627 DEFAULT CHARSET=latin1
?>