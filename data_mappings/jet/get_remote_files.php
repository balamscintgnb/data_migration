<?php
// Report all PHP errors (see changelog)
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/

//$data_mapping = true;

//require_once '../../includes/config.php';
require_once '../../header_init.php';

//copy ('https://app.jetsoftware.co.uk/stn/exitstaging/pictur	es/LHA/08/LHA080029_01.jpg', 'test.jpg'); exit;
//extract($_GET);

require_once '../plugins/PHPExcel/IOFactory.php';

$software='jet';
$cnb='stones';

$totalFiles=0;
$totalFilesCopied=0;
$thisProceed=true;

if($p==1){
	$file_name='../source_data/jet/stpr/jet_stn_pictures.xls';
	$dstn_path='../../data/stpr/properties_images/';
}else if($p==2){
	$file_name='../source_data/jet/stpr/jet_stn_prplet.xls';
	$dstn_path='../../data/stpr/tenancies/';
}else{
	$thisProceed=false;
}

try {
    //Load the excel(.xls/.xlsx) file
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
    die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

//echo shell_exec('curl --digest -u exit:907c46d4cdd190df99f9ba4088be02d3 https://app.jetsoftware.co.uk/stn/exitstaging/pictures/LHA/08/LHA080035_05.png');

//
$db_utility = new Utility();

$board_information_data = $db_connect->get_boardinformation();
$board_information = json_decode($board_information_data);
$information = $board_information->data;
$db_domain =   explode('_', $db_name);

//todo move this
$dev_name = '';
$subdomain_name = $db_domain[2];

$host = $_SERVER["SERVER_NAME"];

if(isset($board) && $board != '' && $information->server==1){
   $subdomain_name = $board;
}

if($information->server==0){
    $dev_name = '.dev';
}

// $dev_name = '.dev';

// echo $url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/get_all_property_server_ids.php';
echo $url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/get_all_tenancies_server_ids.php';
exit;
echo '<br/>';

$request = json_encode(array('Token' => 1234, 'Data' => array())); // token generate on both end

$header = array('Content-Type: application/json', 'Request-API-Token: GNB-DATA-1234-ASED');

$response = $db_utility->getCurlData($url, $request, $header, false);

$response = json_decode($response, true);
// var_dump($response);
// $success_ids = $response['property_ids'];
$success_ids = $response['tenancy_ids'];
// $success_ids = $response;
// echo $response['total'];
// var_dump($success_ids);

// print_r($success_ids);
// foreach($success_ids as $key => $value){
	
// 	 echo $key;
// 	 echo "<br/>";
// }

// exit;
	

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){

        if($row>1){
           
            //curl --digest -u exit:907c46d4cdd190df99f9ba4088be02d3 https://app.jetsoftware.co.uk/stn/exitstaging/pictures/LHA/08/LHA080035_05.png
            
            //$file ='https://app.jetsoftware.co.uk/stn/exitstaging/pictures/LHA/08/LHA080035_05.png';

            //
            $local_file_name = '';
            $caption = '';
            $file_type = '';
            $order = '';
            
            $receipient_id = 0;
            
            if($p==1){
            	$PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            	$local_file_name = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            	$file_type = 'GALLERY_IMAGE';
            	$order = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
            	
            	$image_type = strtolower(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
            	
            	if($image_type=='ep'){
            		$file_type = 'EPC';
            	}
            	
            	if($image_type=='fp'){
            		$file_type = 'FLOOR_PLAN';
            	}
            }
            
            if($p==2){
            	$PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            	$caption = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
            	$receipient_id = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            }
            
            $url=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
            //echo '<br/>';
            
            $totalFiles++;

            $path_info = pathinfo($url);
           
            $dstn_path1 = $dstn_path.$PROPERTY_ID;

            $file_full_path = $dstn_path1.'/'.$path_info['basename'];
            
            $file_ext = $path_info['extension'];
            
			if(isset($fetch_files) && $fetch_files==1){
			
			    if(!file_exists($file_full_path) || filesize($file_full_path)<=0 ){
			
			        if(!is_dir($dstn_path1)){
			            mkdir($dstn_path1, 0777, true);
			        }
			
			        $request = trim(shell_exec('curl --digest -u exit:907c46d4cdd190df99f9ba4088be02d3 '.$url));
			
			        file_put_contents($file_full_path, $request);
			        echo 'Copied '.$file_full_path.' ~ '.$path_info['basename'].'<br/>';
			        $totalFilesCopied++;
			        sleep(1);
			    }else{
			        echo "Already Copied<br/>";
			    }
			}
			 
			 
			if(isset($populate_table) && $populate_table==1){   
				
				if($p==1){
					
					$notice_id = $success_ids[$PROPERTY_ID];
					
					if(trim($notice_id)==''){
						$notice_id = $success_ids["$PROPERTY_ID"];
					}
					
					$order = 0;
					
					$file_type = 'PROPERTY';
					
					if(trim($notice_id)!=''){
						$server_file_name = md5($path_info['filename'].date('dmyhis').rand().rand().rand()).'.'.$path_info['extension'];
					
						$sql ="INSERT INTO `property_files` (`PROPERTY_ID`, `PROPERTY_ROOM_ID`, `PROPERTY_TENANCY_ID`, `PROPERTY_API_KEY`, `PROPERTY_ROOM_API_KEY`, `PROPERTY_TENANCY_KEY`, `PROPERTY_FILE_PATH`, `PROPERTY_FILE_CAPTION`, `PROPERTY_FILE_EXTENSION`, `PROPERTY_FILE_TYPE`, `PROPERTY_FILE_ORDER`, `PROPERTY_FILE_DATE_TIME`, `PROPERTY_SERVER_FILE_NAME`) VALUES ('$notice_id', '0', '0', '$PROPERTY_ID', NULL, NULL, '$local_file_name', '$caption', '$file_ext', '$file_type', '$order', NULL, '$server_file_name');";
						
						$db_connect->queryExecute($sql);// or die($row."<br>".$sql);
						echo $sql;
						echo '<br/>';
						$totalFilesCopied++;
					}
				}
				
				if($p==2){
					$notice_id = $success_ids["$PROPERTY_ID"];
					
					if(trim($notice_id)==''){
						$notice_id = $success_ids[$PROPERTY_ID];
					}
					// echo $notice_id;
					// exit;
					$local_file_name = $path_info['basename'];
					
					$file_type = 'DOCS';
					$order=0;
					
					if(trim($notice_id)!=''){
						$server_file_name = md5($path_info['filename'].date('dmyhis').rand().rand().rand()).'.'.$path_info['extension'];
					
						// $sql ="INSERT INTO `property_files` (`PROPERTY_ID`, `PROPERTY_ROOM_ID`, `PROPERTY_TENANCY_ID`, `PROPERTY_API_KEY`, `PROPERTY_ROOM_API_KEY`, `PROPERTY_TENANCY_KEY`, `PROPERTY_FILE_PATH`, `PROPERTY_FILE_CAPTION`, `PROPERTY_FILE_EXTENSION`, `PROPERTY_FILE_TYPE`, `PROPERTY_FILE_ORDER`, `PROPERTY_FILE_DATE_TIME`, `PROPERTY_SERVER_FILE_NAME`, `PROPERTY_RECEIVER_API_KEY`) VALUES ('$notice_id', '0', '0', '$PROPERTY_ID', NULL, NULL, '$local_file_name', '$caption', '$file_ext', '$file_type', '$order', NULL, '$server_file_name', '$receipient_id')";
						$sql ="INSERT INTO `property_files` (`PROPERTY_ID`, `PROPERTY_ROOM_ID`, `PROPERTY_TENANCY_ID`, `PROPERTY_API_KEY`, `PROPERTY_ROOM_API_KEY`, `PROPERTY_TENANCY_KEY`, `PROPERTY_FILE_PATH`, `PROPERTY_FILE_CAPTION`, `PROPERTY_FILE_EXTENSION`, `PROPERTY_FILE_TYPE`, `PROPERTY_FILE_ORDER`, `PROPERTY_FILE_DATE_TIME`, `PROPERTY_SERVER_FILE_NAME`, `PROPERTY_RECEIVER_API_KEY`) VALUES ('0', '0', '$notice_id', '', NULL,'$PROPERTY_ID', '$local_file_name', '$caption', '$file_ext', '$file_type', '$order', NULL, '$server_file_name', '$receipient_id')";
						
						$db_connect->queryExecute($sql) or die($row."<br>".$sql);
						$totalFilesCopied++;
					}
				}
			
			}
		    
            echo '<br/>';
            //break;
        }
    }
}



echo 'Total Files Copied: '.$totalFilesCopied.'<br/>';
echo 'Total Files: '.$totalFiles.'<br/>';
?>