<?php
// rk created this file on 2019-01-16 12-47 for fetching certificate data and store in dummy table certificates
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jet/stn_exit_upd/jet_stn_cert.csv';

$thisProceed=true;

try {
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$CERTIFICATE_PRPCODE = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
			$CERTIFICATE_TYPE = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
			if($CERTIFICATE_TYPE=='GS')
			{
				$CERTIFICATE_TYPE = 1;
			}

			$CERTIFICATE_START_DATE = trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
			if(stristr($CERTIFICATE_START_DATE, "\N") || $CERTIFICATE_START_DATE == '' )
			{
				$CERTIFICATE_START_DATE = '0000-00-00';
			}
			else
			{
				$CERTIFICATE_START_DATE = Utility::convert_tosqldate($CERTIFICATE_START_DATE, 'Y-m-d H:i:s');
			}
			// else
			// {
			// 	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$CERTIFICATE_START_DATE))
			// 	{
			// 		// $CERTIFICATE_START_DATE="'".$CERTIFICATE_START_DATE."'";
			// 	}
			// 	else
			// 	{
			// 		$CERTIFICATE_START_DATE = Utility::convert_tosqldate($CERTIFICATE_START_DATE, 'd/m/Y');
			// 		$CERTIFICATE_START_DATE="'".$CERTIFICATE_START_DATE."'";
			// 	}
			// }

			$CERTIFICATE_END_DATE = trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
			if(stristr($CERTIFICATE_END_DATE, "\N") || $CERTIFICATE_END_DATE == '' )
			{
				$CERTIFICATE_END_DATE = '0000-00-00';
			}
			else
			{
				$CERTIFICATE_END_DATE = Utility::convert_tosqldate($CERTIFICATE_END_DATE, 'Y-m-d H:i:s');
			}
			// else
			// {
			// 	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$CERTIFICATE_END_DATE))
			// 	{
			// 		// $CERTIFICATE_END_DATE="'".$CERTIFICATE_END_DATE."'";
			// 	}
			// 	else
			// 	{
			// 		$CERTIFICATE_END_DATE = Utility::convert_tosqldate($CERTIFICATE_END_DATE, 'd-m-Y');
			// 		$CERTIFICATE_END_DATE="'".$CERTIFICATE_END_DATE."'";
			// 	}
			// }


			$notes=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
			$extra=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
			$cmpcode=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
					
			$conc_notes = '';
			if($notes != '')
			{
				$conc_notes = $notes;
			}
			
			if($extra != '')
			{
				if($conc_notes != '')
					$conc_notes .= '\n' . $extra;
				else
					$conc_notes = $extra;
			}
			
			if($cmpcode != '')
			{
				if($conc_notes != '')
					$conc_notes .= '\n' . $cmpcode;
				else
					$conc_notes = $cmpcode;
			}

			$CERTIFICATE_NOTES = $conc_notes;

			$sql ="INSERT INTO `certificates` (`CERTIFICATE_PRPCODE`, `CERTIFICATE_TYPE`, `CERTIFICATE_START_DATE`, `CERTIFICATE_END_DATE`, `CERTIFICATE_NOTES`) VALUES ('$CERTIFICATE_PRPCODE', '$CERTIFICATE_TYPE', '$CERTIFICATE_START_DATE', '$CERTIFICATE_END_DATE', '$CERTIFICATE_NOTES')";
			$db_connect->queryExecute($sql) or die ($sql);
			// exit();
		}	// $row if ends here
	}	// for loop ends here
	echo ($row - 2) . " CERTIFICATES INSERTED SUCCESSFULLY"; //	2 has been deducted from row var since row initialized as 1 and for does it nature ++
}	// if $thisProceed ends here

// table to be created

// CREATE TABLE `certificates` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `CERTIFICATE_PRPCODE` varchar(255) DEFAULT NULL,
//  `CERTIFICATE_TYPE` varchar(255) DEFAULT NULL,
//  `CERTIFICATE_START_DATE` date DEFAULT NULL,
//  `CERTIFICATE_END_DATE` date DEFAULT NULL,
//  `CERTIFICATE_NOTES` longtext,
//  `RECORD_UPLOADED` tinyint(1) NOT NULL DEFAULT '0',
//  PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=1306 DEFAULT CHARSET=latin1


// NOTE: DON'T  MIND: fetching query

// SELECT a.* FROM tbl_certificate a INNER JOIN ( SELECT cert_id, CERTIFICATE_PRPCODE, CERTIFICATE_START_DATE, MAX(CERTIFICATE_END_DATE) mxdate FROM tbl_certificate GROUP BY CERTIFICATE_PRPCODE ) b ON a.CERTIFICATE_PRPCODE = b.CERTIFICATE_PRPCODE AND a.CERTIFICATE_END_DATE = b.mxdate HAVING CERTIFICATE_PRPCODE = 'SPH110297' 

// SELECT  a.*
// FROM    tbl_certificate a
//         INNER JOIN 
//         (
//             SELECT  cert_id, CERTIFICATE_PRPCODE, CERTIFICATE_START_DATE, MAX(CERTIFICATE_END_DATE) mxdate, CERTIFICATE_NOTES
//             FROM    tbl_certificate
//             GROUP   BY CERTIFICATE_PRPCODE
//         ) b ON a.CERTIFICATE_PRPCODE = b.CERTIFICATE_PRPCODE AND a.CERTIFICATE_END_DATE = b.mxdate

// SELECT a.* FROM tbl_certificate a JOIN (SELECT  DISTINCT CERTIFICATE_PRPCODE, MAX(CERTIFICATE_END_DATE) mxdate FROM tbl_certificate GROUP BY CERTIFICATE_PRPCODE) b ON a.CERTIFICATE_PRPCODE = b.CERTIFICATE_PRPCODE AND a.CERTIFICATE_END_DATE = b.mxdate

// SELECT  cert_id, CERTIFICATE_PRPCODE, MAX(CERTIFICATE_END_DATE) mxdate FROM tbl_certificate GROUP BY CERTIFICATE_PRPCODE

// SELECT a.* FROM `tbl_certificate` a, (SELECT cert_id, CERTIFICATE_PRPCODE, MAX(CERTIFICATE_END_DATE) mxdate FROM tbl_certificate GROUP BY CERTIFICATE_PRPCODE ORDER BY cert_id ASC) b WHERE a.cert_id = b.cert_id

// SELECT a.* FROM `certificates` a, (SELECT id, CERTIFICATE_PRPCODE, MAX(CERTIFICATE_END_DATE) mxdate FROM certificates GROUP BY CERTIFICATE_PRPCODE ORDER BY id ASC) b WHERE a.id = b.id 

?>