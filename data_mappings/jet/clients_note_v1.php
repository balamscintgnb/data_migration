<?php
// rk created this file on 2019-01-21 12-47 for fetching applicant client notes and update in clients table - this is only for applicants files
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
// $file_name='../source_data/jet/jet_stn_app.csv';
$file_name='../source_data/jet/jet_stn_app_a.csv';

$thisProceed=true;

try
{
	if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv')
	{
		$objReader = PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader->load($file_name);
	}
	else
	{ 
		$objPHPExcel = PHPExcel_IOFactory::load($file_name);
	}
}
catch (Exception $e)
{
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$total_rows = $sheet->getHighestRow();

$notes_updated_count = 0;
$updated_clients_id = array();
$updated_clients = array();
$new_clients = array();
$empty_notes_clients = array();
$notes_array = array();
if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			$CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue());
			$CLIENT_NOTES = '';
        	$notes = trim($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue());
        	if($notes!='' && $CLIENTID!='')
        	{
				if(in_array($CLIENTID, $updated_clients_id))
				{
					$newly_created_note = $notes;
					foreach ($updated_clients as $updated_client)
					{
						foreach ($updated_client as $key => $value)
						{
							if($key == $CLIENTID)
							{
								$newly_created_note .= "\n\n" . $value;
							}
						}
					}
					
					$notes_array[] = $newly_created_note;
					$CLIENT_NOTES = addslashes(json_encode($notes_array));

					$update_notes_sql = "UPDATE `clients` SET CLIENT_NOTES = '" . $CLIENT_NOTES . "', RECORD_UPLOADED = 0 WHERE CLIENT_TYPE = 'applicant' AND CLIENTID = '" . $CLIENTID . "'";
					$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
					$notes_updated_count++;
				}
				else
				{
					$notes_array[] = $notes;
					$CLIENT_NOTES = addslashes(json_encode($notes_array));
					// $CLIENT_NOTES = addslashes(get_res(preg_split('/$\R?^/m', trim($notes))));
					$chk_client_exist_sql = "SELECT * FROM `clients` WHERE CLIENT_NOTES = '' AND CLIENT_TYPE = 'applicant' AND CLIENTID = '" . $CLIENTID . "'";
					$let_exists = json_decode($db_connect->queryFetch($chk_client_exist_sql),true);
					
					if(@$let_exists['data'][0]['CLIENTID'])
					{
						$update_notes_sql = "UPDATE `clients` SET CLIENT_NOTES = '" . $CLIENT_NOTES . "', RECORD_UPLOADED = 0 WHERE CLIENT_NOTES = '' AND CLIENT_TYPE = 'applicant' AND CLIENTID = '" . $CLIENTID . "'";
						$db_connect->queryExecute($update_notes_sql) or die ($update_notes_sql);
						$notes_updated_count++;
						$updated_clients_id[] = $CLIENTID;

						// $updated_clients[$row][$CLIENTID] = $notes;

						// $updated_clients[$row]['id'] = $CLIENTID;
						// $updated_clients[$row]['notes'] = $notes;
					}
					else
					{
						$new_clients[] = $CLIENTID;
					}
				}
				$updated_clients[$row][$CLIENTID] = $notes;
				unset($notes_array);
        	}
        	else
        	{
				$empty_notes_clients[] = $CLIENTID;
        	}
		}	// $row if ends here
	}	// for loop ends here
	echo "IN " . ($row - 2) . " ROUNDS " . $notes_updated_count . " CLIENT NOTES UPDATED SUCCESSFULLY"; //	2 has been deducted from row var since row initialized as 1 and for does it nature ++
	echo "<br/><br/>CLIENT(S) WHOSE NOTES ARE UPDATED : <br/><pre>";
	print_r($updated_clients_id);
	echo "</pre>";
	// echo "<br/><br/>CLIENT(S) WHO ARE NOT IN DB : <br/><pre>";
	// print_r($new_clients);
	// echo "</pre>";
	// echo "<br/><br/>CLIENT(S) WHOSE NOTES ARE EMPTY IN EXCEL ITSELF : <br/><pre>";
	// print_r($empty_notes_clients);
	// echo "</pre>";
	// echo "<br/><br/><br/><pre>";
}	// if $thisProceed ends here
?>