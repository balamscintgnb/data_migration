Steps to follow while preparing the data from jet Software:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Create the following tables:
---------------------------

	- applicant_tab
	- applicant2_tab
	- landlord_tab
	- tenant_tab
	- buyer_tab
	- letting_tab
	
	- board_information
	- clients
	- properties
	- lettings
	- valuation
	- letting_offers
	- sale_offers
	- certificates
	- property_rooms
	- notes
	- notes_jnl_only
	- notes_jnlmisc
	- letting_notes
	- viewings
	- sms

Directly import the "jet_stn_app.csv" file as "applicant_tab" named table in phpmyadmin.

Directly import the "jet_stn_app_a.csv" file as "applicant2_tab" named table in phpmyadmin.

Directly import the "jet_stn_lld.csv" file as "landlord_tab" named table in phpmyadmin.

Directly import the "jet_stn_ten.csv" file as "tenant_tab" named table in phpmyadmin.

Directly import the "jet_stn_offers.csv" file as "buyer_tab" named table in phpmyadmin.

Directly import the "jet_stn_prplet.csv" file as "letting_tab" named table in phpmyadmin.


Board Information:
~~~~~~~~~~~~~~~~~

-	As we do for all clients

> Negotiators:
  ~~~~~~~~~~~

	- negotiators.php should be run for (jet_stn_negs.csv) before run the clients_v1.php, properties.php and diary.php to create the negotiators.json whose data have been used in clients_v1.php, properties.php and diary.php


> Clients:
  ~~~~~~~

	- clients_v1.php should be run for two files (cnt and cnt_a) to import data in clients table

> Certificates:
  ~~~~~~~~~~~~

-	certificates.php should be run for ‘jet_stn_cert.csv’ to import data in certificates table


> Properties:
  ~~~~~~~~~~
	
	- properties.php Data should be run for two files (prp and prp_a) to import data in properties table.
	
	- while running this script, 'vendor' role users might be inserted/updated based on the client type, “ADDRESSBOOK” as well valuations table data might be filled based on the “PROPERTY_AVAILABILITY”.


> Certificates and Properties Mapping:
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-	certificates_properties_mapping.php should be run for mapping the CERTIFICATE_END_DATE field value from the certificate table to the CERTIFICATE_EXPIRE_DATE field in the property table.


> Room Size:
  ~~~~~~~~~

-	room_size.php should be run for ‘jet_stn_extra.csv’ to import data in property_rooms table


> Lettings:
  ~~~~~~~~

-	lettings_v1.php should be run for ‘jet_stn_ten.csv’ to import data into lettings table
-	lettings_v2.php should be run for ‘jet_stn_tenext.csv’ to import data into lettings table


> Offers:
  ~~~~~~

-	offers.php should be run for ‘jet_stn_offers.csv’ to import data into letting_offers and sale_offers table based on the PROPERTY_OFFER_TYPE


> Notes:
  ~~~~~

-	notes_v1.php should be run for ‘jet_stn_jnl.csv’ with the ‘jet_stn_negs.csv’ for importing data into notes table

-	notes_v2.php should be run for ‘jet_stn_jnlmisc.csv’ with the ‘jet_stn_negs.csv’ for importing data into notes table

-	After run the notes_v1.php and notes_v2.php, we should run tenancies_note.php and contact_note.php for importing letting notes and contact notes into all_notes table (letting_notes table - deprecated). This will be then segregated based on the ‘NOTES_TYPE’.
(NOTES_TYPE: ‘10’=> letting note ‘90’=> contact note)

-  	Run property_notes.php for importing property general notes into all_notes table
	(NOTES_TYPE: ‘60’=> RESIDENTIAL LETTINGS ‘70’=> RESIDENTIAL SALES)

(Hint: when we run the notes_v1.php & notes_v2.php files, the data will be imported into table ‘notes_jnl_only’ & ‘notes_jnlmisc’. This is only for the purpose to check the count of each file rows and table records and additionally, if we make any mistake while run the notes_v2.php then we need to truncate all data from notes table and have to re-run the notes_v1.php also. To ignore this kind of time consumption, we can take truncate the notes table data and copy the notes_jnl_only table into notes then we can directly run the notes_v2.php. This would be more helpful at the time of large data.) 

> Diary:
  ~~~~~

-	Diary.php should be run for ‘jet_stn_diary.xls’ for importing appointments, viewings etc into viewings table.

> Sms:
  ~~~~~

-	sms.php should be run for ‘jet_stn_messages.xls’ for importing sms communications into sms table.


URLs order to be run step by step:
*********************************


http://localhost:8080/import/data_mappings/jet/negotiators.php

http://localhost:8080/import/data_mappings/jet/clients_v1.php (for two files)****

http://localhost:8080/import/data_mappings/jet/certificates.php

http://localhost:8080/import/data_mappings/jet/properties.php (for two files)****

http://localhost:8080/import/data_mappings/jet/certificates_properties_mapping.php

http://localhost:8080/import/data_mappings/jet/room_size.php

http://localhost:8080/import/data_mappings/jet/lettings_v1.php

http://localhost:8080/import/data_mappings/jet/lettings_v2.php

http://localhost:8080/import/data_mappings/jet/notes_v1.php

http://localhost:8080/import/data_mappings/jet/notes_v2.php

http://localhost:8080/import/data_mappings/jet/contact_note.php

http://localhost:8080/import/data_mappings/jet/tenancies_note.php

http://localhost:8080/import/data_mappings/jet/offers.php

http://localhost:8080/import/data_mappings/jet/diary.php

http://localhost:8080/import/data_mappings/jet/sms.php

																																			Prepared By,

																																			Data Import Team,
																																			     GNB Tech
