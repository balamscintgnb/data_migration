<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/chanin_jupix/ches2.xls';

$db_utility = new Utility();

//$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
   
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
           
            
            $user_details[]=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $enquiry_time[]= $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getformattedValue();
            $get_property_address[] = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $search_info[]=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

           // }
        //logic for data mapping.

        if($row % 3 == 0){ 
          //  $user_details = $enquiry_time = $get_property_address = array();
            $ENQUIRY_ID='100'.$row;
            $ENQUIRY_USER_ID='';
            $ENQUIRY_DATETIME= Utility :: convert_tosqldatetime($enquiry_time[0].' '.$enquiry_time[1],'d/m/Y H:i A');
            $ENQUIRY_SEARCH_INFO='';
            $address  = [] ;
            if($get_property_address[0]){
            $address = explode(',',$get_property_address[0]);
            }
            else { 
                $address = explode(',',$get_property_address[1]);

            }


            $count_part = count($address);
            echo '<br />';

                if($count_part == 6){
                    $address_1 = trim($address[0]).', '.trim($address[1]).', '.trim($address[2]);
                    $address_2= trim($address[3]);
                    $PROPERTY_ADDRESS_POSTCODE= trim($address[5]);
                } else if($count_part == 5){
                    $address_1 = trim($address[0]).', '.trim($address[1]).', '.trim($address[2]);
                    $address_2= trim($address[3]);
                    $PROPERTY_ADDRESS_POSTCODE= trim($address[4]);
                } else if($count_part == 4){
                    $address_1 = trim($address[0]).', '.trim($address[1]).', '.trim($address[2]);
                    $address_2= trim($address[3]);
                    $PROPERTY_ADDRESS_POSTCODE= trim($address[3]);
                }
                $ENQUIRY_PROPERTY_ID=0;

                 $address_available_query = "SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$address_1' AND `PROPERTY_ADDRESS_POSTCODE` LIKE '$PROPERTY_ADDRESS_POSTCODE' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";

                $address_available = json_decode($db_connect->queryFetch($address_available_query),true);

                if($address_available['data'][0]['PROPERTY_ID']!=''){
                    $ENQUIRY_PROPERTY_ID=$address_available['data'][0]['PROPERTY_ID'];
                }
                else{
                   $address_available_query1 = "SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$address_2' AND `PROPERTY_ADDRESS_POSTCODE` LIKE '$PROPERTY_ADDRESS_POSTCODE' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";
                    $address_available1 = json_decode($db_connect->queryFetch($address_available_query1),true);
                    $ENQUIRY_PROPERTY_ID=$address_available1['data'][0]['PROPERTY_ID'];

                    if($ENQUIRY_PROPERTY_ID=='')
                        $address_available_query = "SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$address_1' AND `PROPERTY_ADDRESS_POSTCODE` LIKE '$PROPERTY_ADDRESS_POSTCODE' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";

                }
            
                $ENQUIRY_NOTES = trim($get_property_address[0]);
                    
                $query_1="SELECT CLIENTID  FROM `clients123` WHERE `CLIENT_NAME` LIKE '$user_details[0]' AND `CLIENT_TYPE` LIKE 'applicant'";
                $client_exist = json_decode($db_connect->queryFetch($query_1),true);
                $ENQUIRY_APPLICANT_ID=0;
        if(empty($client_exist['data'])){
            $ENQUIRY_APPLICANT_ID='TEN_20'.$row;
            $query="INSERT INTO `clients_dev` (`id`, `CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES 
            (NULL, '$ENQUIRY_APPLICANT_ID', '', '$user_details[0]', 'applicant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, '0') ";
            $db_connect->queryExecute($query);
            }else {
                $ENQUIRY_APPLICANT_ID=$client_exist['data'][0]['CLIENTID'];
            }
   
            if( $search_info[0] == 'Booking a viewing'){
                $ENQUIRY_SEARCH_INFO=7;
            } else if( $search_info[0] == 'Property details'){
                $ENQUIRY_SEARCH_INFO=6;
            } else if( $search_info[1] == 'Booking a viewing'){
                $ENQUIRY_SEARCH_INFO=7;
            } else if( $search_info[1] == 'Property details'){
                $ENQUIRY_SEARCH_INFO=6;
            } else{
                $ENQUIRY_SEARCH_INFO=$search_info[0];
                $ENQUIRY_SEARCH_INFO=$search_info[1];
            }
                
//    echo  $sql="INSERT INTO `enquiries` (`ENQUIRY_ID`, `ENQUIRY_APPLICANT_ID`, `ENQUIRY_USER_ID`, `ENQUIRY_PROPERTY_ID`, `ENQUIRY_NOTES`, `ENQUIRY_DATETIME`, `ENQUIRY_SEARCH_INFO`) 
//                 VALUES ('$ENQUIRY_ID', '$ENQUIRY_APPLICANT_ID', '$ENQUIRY_USER_ID', '$ENQUIRY_PROPERTY_ID', '$ENQUIRY_NOTES', '$ENQUIRY_DATETIME', '$ENQUIRY_SEARCH_INFO')";
//               // $db_connect->queryExecute($sql) or die($sql);
//         //    echo $row++;
//         echo '<br />';
            $user_details =  $enquiry_time=  $search_info =  $get_property_address = [];
        }           
     }
  }
}
?>