<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/lettings/Maintenance_All.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
	$number = $MAINTENANCE_ID = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        	//logic for data mapping.

        	$MAINTENANCE_ID++;
        	$MAINTEN_ID=str_pad($MAINTENANCE_ID, 5, "0", STR_PAD_LEFT);
        	$JOB_REF_NO = "MAINTENANCE".$MAINTEN_ID;
			$REPORT_PROBLEM_ID = '';
			$JOB_CATEGORY = '';
			$JOB_DESCRIPTION = '';
			$JOB_ON = '';
			$JOB_PRIORITY = '';
			$JOB_ESTIMATED_COST = '';
			$JOB_WILL_BE_PAID_BY = '';
			$JOB_CONTRACTOR_INVOICE_FILE = '';
			$JOB_AGENT_INVOICE_FILE = '';
			$JOB_REQUEST_DATE = '';
			$JOB_CONFIRMED_DATE = '';
			$JOB_CONTRACTOR_QUOTE_FILE = '';
			$JOB_AGENT_QUOTE_FILE = '';
			$JOB_INVOICE_AMT_CONTRACTOR = '';
			$JOB_INVOICE_AMT_AGENT = '';
			$JOB_PAYABLE_USER_ID = '';
			$JOB_PAYABLE_STATUS = '';
			$JOB_PAYABLE_AMOUNT = '';
			$JOB_PAYABLE_PAID_AMOUNT = '';
			$JOB_PAYABLE_INVOICE_FILE = '';
			$JOB_PAYABLE_TRANSACTION_IDS = '';
			$JOB_STATUS = '';
			$JOB_NOTES = '';
			$JOB_CREATED_ON = '';
			$JOB_COMPLETED_ON = '';
			$JOB_TRANSACTION_ID = '';
			$JOB_UPDATED_ON = '';

	        $SUPPLIER_ID = '';
	        $CLIENT_TYPE = 'SUPPLIER';
	        $CLIENT_NAME = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

			$Query1="SELECT CLIENTID FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE '$CLIENT_TYPE'";
			$Find_sup = json_decode($db_connect->queryFetch($Query1),true);  
			
			if(isset($Find_sup['data'][0]['CLIENTID']))
			{
			$SUPPLIER_ID=$Find_sup['data'][0]['CLIENTID'];
			}
			else
			{ 
			$number++;
			$pro_id=str_pad($number, 4, "0", STR_PAD_LEFT);
			$SUPPLIER_ID = 'SUP60'.$pro_id;
			$Query2="INSERT INTO `clients` (`CLIENTID`, `CLIENT_NAME`, `CLIENT_TYPE`) VALUES ('$SUPPLIER_ID','$CLIENT_NAME', '$CLIENT_TYPE')";
			// $db_connect->queryExecute($Query2) or die($Query2);
			}

			$PROPERTY_ADDRESS_LINE_1 = $PROPERTY_ADDRESS_LINE_2 = $PROPERTY_ADDRESS_CITY = $PROPERTY_ADDRESS_POSTCODE = '';
	        $PROPERTY_ID = 0;

    		$address = explode(',',trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
            $count_part = count($address);

			if($count_part == 3){
				$PROPERTY_ADDRESS_LINE_1 = trim($address[0]);
				$PROPERTY_ADDRESS_LINE_2 = trim($address[1]);
				$PROPERTY_ADDRESS_POSTCODE = trim($address[2]);
				// echo "<pre>"; print_r($address); echo"</pre>";
			}
			else if($count_part == 4)
			{
				$PROPERTY_ADDRESS_LINE_1 = trim($address[0]);
				$ADDRESS_LINE_2 = trim($address[1]);
					if($ADDRESS_LINE_2){
						if($PROPERTY_ADDRESS_LINE_1){
							$PROPERTY_ADDRESS_LINE_1.= ", ";
						}
						$PROPERTY_ADDRESS_LINE_1.=$ADDRESS_LINE_2;
					}
				$PROPERTY_ADDRESS_LINE_2 = trim($address[2]);
				$PROPERTY_ADDRESS_POSTCODE = trim($address[3]);
			}

			$Query3 = "SELECT * FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$PROPERTY_ADDRESS_LINE_1' AND (`PROPERTY_ADDRESS_LINE_2` LIKE '$PROPERTY_ADDRESS_LINE_2' OR `PROPERTY_ADDRESS_POSTCODE` LIKE '$PROPERTY_ADDRESS_POSTCODE')";
			$VIEWING_PROPERTY_ID = json_decode($db_connect->queryFetch($Query3),true);

			if(isset($VIEWING_PROPERTY_ID['data'][0]['PROPERTY_ID'])){
				$PROPERTY_ID=$VIEWING_PROPERTY_ID['data'][0]['PROPERTY_ID'];
				$MAINTENANCE_CATEGORY=$VIEWING_PROPERTY_ID['data'][0]['PROPERTY_CATEGORY'];
			}
			else
			{
				$PROPERTY_ADDRESS_LINE_1 = str_replace("  "," ",$PROPERTY_ADDRESS_LINE_1);
				$PROPERTY_ADDRESS_LINE_2 = str_replace("  "," ",$PROPERTY_ADDRESS_LINE_2);
				$PROPERTY_ADDRESS_POSTCODE = str_replace("  "," ",$PROPERTY_ADDRESS_POSTCODE);
				$Query4 = "SELECT * FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$PROPERTY_ADDRESS_LINE_1' AND (`PROPERTY_ADDRESS_LINE_2` LIKE '$PROPERTY_ADDRESS_LINE_2' OR `PROPERTY_ADDRESS_POSTCODE` LIKE '$PROPERTY_ADDRESS_POSTCODE')";
				$address_available1 = json_decode($db_connect->queryFetch($Query4),true);
				$PROPERTY_ID = isset($address_available1['data'][0]['PROPERTY_ID']) ? $address_available1['data'][0]['PROPERTY_ID'] :'0';
			}

        $JOB_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
        // if(stristr($JOB_DESCRIPTION,'Air Conditioning')){
        // 	$JOB_CATEGORY = 1;
        // }
        $JOB_CATEGORY = (stristr($JOB_DESCRIPTION,'Toilet') || stristr($JOB_DESCRIPTION,'Bathroom')) ? 1 :'';

        $MAINTENANCE_DATE_REPORTED = Utility::convert_tosqldate(trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getformattedValue()),'d/m/Y');
        if($MAINTENANCE_DATE_REPORTED){
        	$JOB_DESCRIPTION.= "\nMAINTENANCE DATE REPORTED IS : ".$MAINTENANCE_DATE_REPORTED;
        }
        echo "<pre>"; print_r($row."  == ".$JOB_CATEGORY); echo "</pre>";

                ///////////////////////// FIND MORE THEN FIELDS ///////////////////////////


		$sql = "INSERT INTO `maintenance_jobs` (`JOB_REF_NO`, `REPORT_PROBLEM_ID`, `SUPPLIER_ID`, `PROPERTY_ID`, `JOB_CATEGORY`, `JOB_DESCRIPTION`, `JOB_ON`, `JOB_PRIORITY`, `JOB_ESTIMATED_COST`, `JOB_WILL_BE_PAID_BY`, `JOB_CONTRACTOR_INVOICE_FILE`, `JOB_AGENT_INVOICE_FILE`, `JOB_REQUEST_DATE`, `JOB_CONFIRMED_DATE`, `JOB_CONTRACTOR_QUOTE_FILE`, `JOB_AGENT_QUOTE_FILE`, `JOB_INVOICE_AMT_CONTRACTOR`, `JOB_INVOICE_AMT_AGENT`, `JOB_PAYABLE_USER_ID`, `JOB_PAYABLE_STATUS`, `JOB_PAYABLE_AMOUNT`, `JOB_PAYABLE_PAID_AMOUNT`, `JOB_PAYABLE_INVOICE_FILE`, `JOB_PAYABLE_TRANSACTION_IDS`, `JOB_STATUS`, `JOB_NOTES`, `JOB_CREATED_ON`, `JOB_COMPLETED_ON`, `JOB_TRANSACTION_ID`, `JOB_UPDATED_ON`, `RECORD_UPLOADED`) VALUES 
		(NULL, '', '', '', '', '', '', NULL, '', NULL, '', NULL, NULL, current_timestamp(), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
		// $db_connect->queryExecute($sql) or die ($sql);
		}
	}
		$z = $row-2; echo $z." MAINTENANCE READ SUCCESSFULLY";
}
?>