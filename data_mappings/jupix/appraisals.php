<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/lettings/appraisals.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$updat = $valuat = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
        //logic for data mapping.
        
        $Enter_PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
            $PROPERTY_ID = ''.$pro_id;
		
		
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
        $PROPERTY_VENDOR_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        $PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
        $PROPERTY_ADDRESS_LINE_1='';
            $add_sub_type_1=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
            $add_sub_type_2=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $add_sub_type_3=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();

	        if($add_sub_type_1!=''){
	            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
	        } 
	
	        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
				if($PROPERTY_ADDRESS_LINE_1!=""){
					$PROPERTY_ADDRESS_LINE_1.=', ';
				}
				$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
	        }
	        
	        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
				if($PROPERTY_ADDRESS_LINE_1!=""){
					$PROPERTY_ADDRESS_LINE_1.=' ';
				}
				$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
	        }
	        
        $PROPERTY_ADDRESS_LINE_1 = addslashes(trim($PROPERTY_ADDRESS_LINE_1));
        $PROPERTY_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue()));
        $PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue()));
        $PROPERTY_ADDRESS_COUNTY='';
        $PROPERTY_ADDRESS_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
        
        // Common customer
        /* $PROPERTY_STATUS=''; */
        $PROPERTY_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());
        $PROPERTY_TYPE='';
        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION='';
        $PROPERTY_TENURE='';
        $PROPERTY_AGE='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CONDITION='';
        $PROPERTY_PRICE_FROM=$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();
        $PROPERTY_PRICE_TO=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $PROPERTY_VENDOR_PRICE='';
        $PROPERTY_PROPOSED_PRICE='';
        $PROPERTY_VALUER_NOTES='';

        $Floor_Area=$objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();
        $Guide_Rent=$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();
        $Rent_Frequency=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();
        if($Floor_Area == '0 sq m' || $Floor_Area == '0 sq ft'){
            $Floor_Area = '';
        }
        else if($Floor_Area !='')
        {
        	$PROPERTY_VALUER_NOTES="Floor Area is : ".$Floor_Area." | ";
        }
        if($Guide_Rent!=''){
            $PROPERTY_VALUER_NOTES.='Guide Rent is: '.$Guide_Rent;
        }


        $PROPERTY_VALUER_NOTES_1='';
        $PROPERTY_VALUER_NOTES_2='';
        $PROPERTY_VALUER_NOTES_3='';
        $PROPERTY_VALUER_NOTES_4='';
        $PROPERTY_VALUER_NOTES_5='';
        $STARTTIME=trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        $PROPERTY_APPOINTMENT = date('H:i:s',strtotime($STARTTIME));
        
        $PROPERTY_APPOINTMENT_ENDTIME='NULL';
        $PROPERTY_APPOINTMENT_FOLLOWUPDATE='NULL';

        $given_date=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

        if($given_date!=''){
        $test_date = explode(' ',$given_date);
        $test_date1 = explode('-',$test_date[0]);
        $PROPERTY_APPOINTMENT_STARTTIME="'".$test_date1[0].'-'.$test_date1[1].'-'.$test_date1[2].' '.$PROPERTY_APPOINTMENT."'";
        }
        if($given_date==''){
            $PROPERTY_APPOINTMENT_STARTTIME="NULL";
        }

		if(stristr($PROPERTY_STATUS, 'Carried Out'))
		{
            $PROPERTY_STATUS='1';
        }
        elseif(stristr($PROPERTY_STATUS, 'Cancelled') || stristr($PROPERTY_STATUS, 'Lost Dead') || stristr($PROPERTY_STATUS, 'Pending'))
        {
            $PROPERTY_STATUS='0';
        }
        elseif(stristr($PROPERTY_STATUS,'Won'))
        {
        	$PROPERTY_STATUS='2';
		}
		else
        {
            $PROPERTY_STATUS='0';
        }


        $Query1 = "SELECT * FROM `valuations` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
		$property_exists = json_decode($db_connect->queryFetch($Query1),true);
		
		if(@$property_exists['data'][0]['PROPERTY_ID']){
		    $update_status = "update `valuations` set PROPERTY_VALUER_NOTES = '$PROPERTY_VALUER_NOTES',PROPERTY_PRICE_TO = '$PROPERTY_PRICE_TO',PROPERTY_PRICE_FROM = '$PROPERTY_PRICE_FROM',PROPERTY_APPOINTMENT_STARTTIME = '$PROPERTY_APPOINTMENT_STARTTIME',RECORD_UPLOADED = '0',PROPERTY_STATUS = '$PROPERTY_STATUS' WHERE PROPERTY_ID = '$PROPERTY_ID' AND PROPERTY_CATEGORY = '$PROPERTY_CATEGORY' ";
        	$db_connect->queryExecute($update_status);
        	$updat++;
		}else{

		$sql="INSERT INTO `valuations`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`,
		    `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`,
		    `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`,
		    `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`,
		    `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`,
		    `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`, `PROPERTY_APPOINTMENT_FOLLOWUPDATE`) VALUES
		    ('$PROPERTY_ID','$PROPERTY_REF_ID','$PROPERTY_VENDOR_ID','$PROPERTY_CATEGORY','$PROPERTY_ADDRESS_LINE_1','$PROPERTY_ADDRESS_LINE_2',
		    '$PROPERTY_ADDRESS_CITY','$PROPERTY_ADDRESS_COUNTY','$PROPERTY_ADDRESS_POSTCODE','$PROPERTY_STATUS','$PROPERTY_TYPE',
		    '$PROPERTY_BEDROOMS','$PROPERTY_BATHROOMS','$PROPERTY_RECEPTION','$PROPERTY_TENURE','$PROPERTY_AGE','$PROPERTY_CLASSIFICATION',
		    '$PROPERTY_CONDITION','$PROPERTY_PRICE_FROM','$PROPERTY_PRICE_TO','$PROPERTY_VENDOR_PRICE','$PROPERTY_PROPOSED_PRICE',
		    '$PROPERTY_VALUER_NOTES','$PROPERTY_VALUER_NOTES_1','$PROPERTY_VALUER_NOTES_2','$PROPERTY_VALUER_NOTES_3','$PROPERTY_VALUER_NOTES_4',
		    '$PROPERTY_VALUER_NOTES_5',$PROPERTY_APPOINTMENT_STARTTIME,$PROPERTY_APPOINTMENT_ENDTIME,$PROPERTY_APPOINTMENT_FOLLOWUPDATE)";
		($db_connect->queryExecute($sql)) ? $valuat++ : die($sql);
		
        }
        }
    }
		echo$valuat." VALUATION INSERTED SUCCESSFULLY <br><br>";
		echo "NUMBER OF ".$updat." VALUATION UPDATED SUCCESSFULLY";
}
?>