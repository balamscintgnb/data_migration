<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/lettings/landlords.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$Tot_row = 0;
		$update_no = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
            //logic for data mapping.

			// $update_Query = "UPDATE `clients` SET RECORD_UPLOADED = 5";
			// $db_connect->queryExecute($update_Query); echo "Success";
			// exit();
			
            $CLIENTID = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $CLIENT_TITLE='';
            $CLIENT_NAME = addslashes(str_replace(" ()","",$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
            //$CLIENT_NAME=preg_replace('/[^A-Za-z0-9\-]/', ' ', $NAME);
            $COMPANY_NAME='';
            $CLIENT_TYPE='landlord';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            //$CLIENT_PRIMARY_EMAIL=str_replace("'"," ", $EMAIL);
            $CLIENT_PRIMARY_PHONE='';
            $CLIENT_ADDRESS_LINE_1='';
            $CLIENT_ADDRESS_LINE_2='';
            $CLIENT_ADDRESS_CITY='';
            $CLIENT_ADDRESS_TOWN='';
            $CLIENT_ADDRESS_POSTCODE='';
            $get_address = explode('-',$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());

            $count_val = count($get_address);
            if($count_val==2)    {
	            $CLIENT_ADDRESS_LINE_1=$get_address[0];
	            $CLIENT_ADDRESS_LINE_2='';
	            $CLIENT_ADDRESS_CITY='';
	            $CLIENT_ADDRESS_TOWN='';
	            $CLIENT_ADDRESS_POSTCODE=$get_address[1];
            } else if($count_val==3)    {
	            $CLIENT_ADDRESS_LINE_1=$get_address[0];
	            $CLIENT_ADDRESS_LINE_2='';
	            $CLIENT_ADDRESS_CITY=$get_address[1];
	            $CLIENT_ADDRESS_TOWN='';
	            $CLIENT_ADDRESS_POSTCODE=$get_address[2];
            } else if($count_val==4)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
                $CLIENT_ADDRESS_LINE_2=$get_address[1];
                $CLIENT_ADDRESS_CITY=$get_address[2];
                $CLIENT_ADDRESS_TOWN='';
                $CLIENT_ADDRESS_POSTCODE=$get_address[3];
            } else if($count_val==5)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
                $CLIENT_ADDRESS_LINE_2=$get_address[1];
                $CLIENT_ADDRESS_CITY=$get_address[2];
                $CLIENT_ADDRESS_TOWN=$get_address[3];
                $CLIENT_ADDRESS_POSTCODE=str_replace('.','',$get_address[4]);
            }
            else { 
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
    
            }
            $CLIENT_ADDRESS_LINE_1 = addslashes($CLIENT_ADDRESS_LINE_1);
            $CLIENT_ADDRESS_LINE_2 = addslashes($CLIENT_ADDRESS_LINE_2);
            $CLIENT_ADDRESS_CITY = addslashes($CLIENT_ADDRESS_CITY);
            $CLIENT_ADDRESS_TOWN = addslashes($CLIENT_ADDRESS_TOWN);
            $CLIENT_ADDRESS_POSTCODE= addslashes($CLIENT_ADDRESS_POSTCODE);

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
            
            if(stristr("$MOBILE_1", "/", true)){
                $CLIENT_MOBIL = explode('/',$MOBILE_1);
                $CLIENT_MOBILE_1 =Utility::format_content(addslashes(preg_replace('/[^A-Za-z0-9\-]/', '',  $CLIENT_MOBIL[0])));
                $CLIENT_MOBILE_2 =Utility::format_content(addslashes(preg_replace('/[^A-Za-z0-9\-]/', '',  $CLIENT_MOBIL[1])));
            }
            else{
                $CLIENT_MOBILE_1=Utility::format_content(addslashes(preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1)));
            }
            
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $NOTES=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $CLIENT_NOTES=addslashes(str_replace("'"," ", $NOTES));
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';
          
            if($CLIENT_PRIMARY_EMAIL=='.'){
                $CLIENT_PRIMARY_EMAIL="";
            }
            $Query1 = "SELECT * FROM `clients` WHERE CLIENTID LIKE '$CLIENTID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
			$Clients_exists = json_decode($db_connect->queryFetch($Query1),true);
			
			if(@$Clients_exists['data'][0]['CLIENTID']){
			    // $update_Query = "UPDATE `clients` SET `CLIENT_MOBILE_1` = '$CLIENT_MOBILE_1' , `CLIENT_MOBILE_2` = '$CLIENT_MOBILE_2' , `RECORD_UPLOADED` = 0 WHERE `CLIENTID` = '$CLIENTID' AND `CLIENT_TYPE` = '$CLIENT_TYPE'";
			    // ($db_connect->queryExecute($update_Query)) ? $update_no++ : die($update_Query);
			}else{
	    		$Tot_row++;
			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
			    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
			    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
			    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
			    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
			    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
			    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
			    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME',
			    '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
			    '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
			    '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
			    '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
			    '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
			    '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
			    '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
			    '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '')";
			$db_connect->queryExecute($sql) or die($sql);
	        }
	         
        }
    }
    		$z = $row-2;
    		echo "TOTAL ROWS : ".$z. "<br><br>CLIENTS INSERTED SUCCESSFULLY COUNT IS : ".$Tot_row;
    		// echo $update_no."CLIENTS UPDATED";
}
?>