<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/lettings/tenancy.csv';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

			//logic for data mapping.
        
        	$Update_Query = "UPDATE `lettings` SET `RECORD_UPLOADED` = 0";
        	$db_connect->queryExecute($Update_Query); echo "Success";
        	exit();
        
	        $LETTING_ID = "LET_10001".$row;
	        $LETTING_CUSTOM_REF_NO = $LETTING_ID;
	        $PROPERTY_ID='';
	        $PROPERTY_ROOM_ID='';
	        $TENANT_ID='';
	
	        $SHARED_TENANT_IDS='';
	        $LETTING_START_DATE='';
	
	        $given_date1=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue(), 'l dS M y');
	
	            if($given_date1!='')
	            {
	            $LETTING_START_DATE="'".$given_date1."'";
	            }
	            if($given_date1==''){
	                $LETTING_START_DATE="NULL";
	            }

	        $LETTING_END_DATE='';
	
	        $given_date2=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue(),'l dS M y');
	            
	            if($given_date2=='' || $given_date2=='Periodic')
	            {
	                $LETTING_END_DATE="NULL";
	            }
	            else
	            {
	            $LETTING_END_DATE = "'".$given_date2."'";
	            }
	            
	            // echo "<pre>"; print_r($given_date2); echo"<pre>";
	        $LETTING_RENT= addslashes(trim(str_replace(",","",$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue())));
	        $LETTING_RENT_FREQUENCY='';
	        $RENT_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
	        $LETTING_RENT_PAYMENT_FREQUENCY='';
	        $LETTING_RENT_PAYMENT_DAY='';
	        $LETTING_RENT_ADVANCE='';


	        $LETTING_RENT_ADVANCE_TYPE='';
	        $LETTING_NOTICE_PERIOD='';
	        $LETTING_NOTICE_PERIOD_TYPE='';
	        $LETTING_BREAK_CLAUSE='';
	        $LETTING_BREAK_CLAUSE_TYPE='';

	        $LETTING_DEPOSIT_DUE_DATE='';
	        $LETTING_DEPOSIT_HELD_BY='';
	        $LETTING_DEPOSIT_PROTECTION_SCHEME='';
	        $LETTING_SERVICE='';
	        $LETTING_FEES='';

	            if($LETTING_RENT=='0'){
	                $LETTING_RENT_FREQUENCY='';
	                $LETTING_FEE_FREQUENCY='';
	                $LETTING_MANAGEMENT_FEE_FREQUENCY='';
	                $LETTING_FEE_TYPE='';
	                $LETTING_MANAGEMENT_FEE_TYPE='';
	            }
	            else
	            {
	                $LETTING_RENT_FREQUENCY=$RENT_FREQUENCY;
	                $LETTING_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
	                $LETTING_MANAGEMENT_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
	                $LETTING_MANAGEMENT_FEE_TYPE='1';
	                $LETTING_FEE_TYPE='1';
	            }

	        $LETTING_MANAGEMENT_FEES='';
	        $LETTING_ADMIN_FEES='';
	        $LETTING_ADMIN_FEE_TYPE='';
	        $LETTING_INVENTORY_FEES='';      
	        $LETTING_INVENTORY_FEE_TYPE='';
	        $LETTING_RENT_GUARANTEE='';
	
	        $LETTING_LANDLORD_PAYMENT='';
	        $LETTING_VAT_PERCENTAGE='';
	        $LETTING_NOTES='';
	        $LETTING_DATETIME='';
	        $TENANT_NAME='';


        /////////////////////////////ADD MORE FIELDS/////////////////////////

	        $PROPERTY_ADDRESS_LINE_1='';
	
	            $add_sub_type_1=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
	            $add_sub_type_2=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
	            $add_sub_type_3=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
	
			        if($add_sub_type_1!='')
			        {
			            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
			        } 
			
			        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2)
			        {
						if($PROPERTY_ADDRESS_LINE_1!=""){
							$PROPERTY_ADDRESS_LINE_1.=', ';
						}
						$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
			        }
			        
			        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3)
			        {
						if($PROPERTY_ADDRESS_LINE_1!=""){
							$PROPERTY_ADDRESS_LINE_1.=' ';
						}
						$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
			        } 

	        $get_let_service = $objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
	            if($get_let_service == 'Let Only')
	                $LETTING_SERVICE=2;
	            if($get_let_service == 'Fully Managed')
	                $LETTING_SERVICE=1;
	            if($get_let_service == 'Rent Collection Only')
	                $LETTING_SERVICE=3;
	
	            if($LETTING_RENT_FREQUENCY == 'pcm')
	                $LETTING_RENT_PAYMENT_FREQUENCY='monthly';
	            if($LETTING_RENT_FREQUENCY == 'pw')
	                $LETTING_RENT_PAYMENT_FREQUENCY='weekly';
	            if($LETTING_RENT_FREQUENCY == 'p4w')
	                $LETTING_RENT_PAYMENT_FREQUENCY='four_weekly';
	        
            $CLIENT_NAME = '';
            $Shard_tenant = '';
            $TENANT_NAME = '';
            $Shard_tenant1 = '';
            $Shard_tenant2 = '';
            $Shard_tenant3 = '';
            $Shard_tenant4 = '';
            $Query_2 = '';
            $resul = '';
            $TENANT_IDS1 = '';
            $TENANT_IDS2 = '';
            $TENANT_IDS3 = '';
            $TENANT_IDS4 = '';
        
				$NAME = addslashes(trim(str_replace("  ()","",$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue())));
				$CLIENT=str_replace("'", ' ', $NAME);
				$CLIENT_NA=str_replace("`", '', $CLIENT);
				$filter_name=explode(',', $CLIENT_NA);
			
			$TENANT_NAME = trim($filter_name[0]);
			$Shard_tenant1 = trim(@$filter_name[1]);
			$Shard_tenant2 = trim(@$filter_name[2]);
			$Shard_tenant3 = trim(@$filter_name[3]);
			$Shard_tenant4 = trim(@$filter_name[4]);

            $PHONE=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=preg_replace('/[^A-Za-z0-9\-]/', ' ', $PHONE);


                /////////////////////////////SUB QUERY//////////////////////////

                $query_3="SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$PROPERTY_ADDRESS_LINE_1'";
                $FIND_PRO_ID = json_decode($db_connect->queryFetch($query_3),true);

                $PROPERTY_ID=$FIND_PRO_ID['data'][0]['PROPERTY_ID'];


                $query_1="SELECT CLIENTID  FROM `clients` WHERE `CLIENT_NAME` LIKE '$TENANT_NAME' AND `CLIENT_PRIMARY_PHONE` LIKE '$CLIENT_PRIMARY_PHONE' AND `CLIENT_TYPE` LIKE 'Tenant'";
                $client_exist = json_decode($db_connect->queryFetch($query_1),true);
        
                $TENANT_ID=@$client_exist['data'][0]['CLIENTID'];

				if($Shard_tenant1 != '')
        		{
	                $Query_4="SELECT CLIENTID  FROM `clients` WHERE `CLIENT_NAME` LIKE '$Shard_tenant1' AND `CLIENT_TYPE` LIKE 'Tenant'";
	                $find_s_ten1 = json_decode($db_connect->queryFetch($Query_4),true);
	                $TENANT_IDS1 =$find_s_ten1['data'][0]['CLIENTID'];
        		}

				if($Shard_tenant2 != '')
        		{
                $Query_5="SELECT CLIENTID  FROM `clients` WHERE `CLIENT_NAME` LIKE '$Shard_tenant2' AND `CLIENT_TYPE` LIKE 'Tenant'";
                $find_s_ten2 = json_decode($db_connect->queryFetch($Query_5),true);
                $TENANT_IDS2 =$find_s_ten2['data'][0]['CLIENTID'];
        		}

				if($Shard_tenant3 != '')
        		{
                $Query_6="SELECT CLIENTID  FROM `clients` WHERE `CLIENT_NAME` LIKE '$Shard_tenant3' AND `CLIENT_TYPE` LIKE 'Tenant'";
                $find_s_ten3 = json_decode($db_connect->queryFetch($Query_6),true);
                $TENANT_IDS3 =$find_s_ten3['data'][0]['CLIENTID'];
        		}
        
        		if($Shard_tenant4 != '')
        		{
                $Query_7="SELECT CLIENTID  FROM `clients` WHERE `CLIENT_NAME` LIKE '$Shard_tenant4' AND `CLIENT_TYPE` LIKE 'Tenant'";
                $find_s_ten4 = json_decode($db_connect->queryFetch($Query_7),true);
                $TENANT_IDS4 =$find_s_ten4['data'][0]['CLIENTID'];
        		}

                unset($convert_SHARED_TENANT_IDS);
                $convert_SHARED_TENANT_IDS = array();
        
                    if($TENANT_IDS1 != ''){
                        $convert_SHARED_TENANT_IDS[] = $TENANT_IDS1;
                    }
                    if($TENANT_IDS2 != ''){
                        $convert_SHARED_TENANT_IDS[] = $TENANT_IDS2;
                    }
                    if($TENANT_IDS3 != ''){
                        $convert_SHARED_TENANT_IDS[] = $TENANT_IDS3;
                    }
                    if($TENANT_IDS4 != ''){
                        $convert_SHARED_TENANT_IDS[] = $TENANT_IDS4;
                    }
              
                    $SHARED_TENANT_IDS = json_encode($convert_SHARED_TENANT_IDS);
        
                  //echo "<pre>"; print_r($SHARED_TENANT_IDS); echo"<pre>";

        ///////////////////////////END SUB QUERY////////////////////////

				$sql ="INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`, `SHARED_TENANT_IDS`,
				    `LETTING_START_DATE`, `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`,
				    `LETTING_RENT_ADVANCE`, `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,
				    `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`,
				    `LETTING_SERVICE`, `LETTING_FEE_TYPE`, `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`,
				    `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`, `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`,
				    `LETTING_RENT_GUARANTEE`, `LETTING_VAT_PERCENTAGE`, `LETTING_NOTES`) VALUES
				    ('$LETTING_ID', '$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID', '$SHARED_TENANT_IDS', $LETTING_START_DATE,
				     $LETTING_END_DATE, '$LETTING_RENT', '$LETTING_RENT_FREQUENCY', '$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
				    '$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE', '$LETTING_BREAK_CLAUSE_TYPE',
				    '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE',
				    '$LETTING_FEE_TYPE', '$LETTING_FEE_FREQUENCY', '$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY',
				    '$LETTING_ADMIN_FEES', '$LETTING_ADMIN_FEE_TYPE', '$LETTING_INVENTORY_FEES', '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE',
				    '$LETTING_VAT_PERCENTAGE', '$LETTING_NOTES')";
				$db_connect->queryExecute($sql) or die($sql);
    }
}
			$z = $row-2;
	    	echo $z." LETTINGS ADDED SUCCESSFULLY";
}
?>