<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/sales/offers.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns


if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            //logic for data mapping.
            $SALE_OFFER_ID = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $Enter_PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
            $SALE_PROPERTY_ID = ''.$pro_id;
            $PROPERTY_APPLICANT_ID = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
            $delet = array("£", ",");
            $PRICE = trim(str_replace($delet,"",$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getformattedValue()));
            $PROPERTY_SALE_OFFER_PRICE=preg_replace('/[^A-Za-z0-9\-]/', '', $PRICE);
            $PROPERTY_SALE_SOLICITOR_ID='';
            $PROPERTY_SALE_OFFER_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
            	if(stristr($PROPERTY_SALE_OFFER_STATUS,'Accepted'))
            	{
            		$PROPERTY_SALE_OFFER_STATUS = '1';
            	}
            	else if(stristr($PROPERTY_SALE_OFFER_STATUS,'Declined') || stristr($PROPERTY_SALE_OFFER_STATUS,'Withdrawn'))
            	{
            		$PROPERTY_SALE_OFFER_STATUS = '2';
            	}
            	else if(stristr($PROPERTY_SALE_OFFER_STATUS,'Pending'))
            	{
            		$PROPERTY_SALE_OFFER_STATUS = '0';
            	}
            	else
            	{
            		$PROPERTY_SALE_OFFER_STATUS = '0';
            	}

            $given_date=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            $PROPERTY_SALE_OFFER_DATETIME=Utility::convert_tosqldate($given_date, 'l dS M y');
            	if($PROPERTY_SALE_OFFER_DATETIME)
            	{
					$PROPERTY_SALE_OFFER_DATETIME="'".$PROPERTY_SALE_OFFER_DATETIME."'";
            	}
            	else
            	{
            		$PROPERTY_SALE_OFFER_DATETIME = "NULL";
            	}

			// echo "<pre>";
			// echo $PROPERTY_SALE_OFFER_DATETIME;
			// echo"</pre>";

			$sql = "INSERT INTO `sale_offers` (`SALE_OFFER_ID`, `SALE_PROPERTY_ID`, `PROPERTY_APPLICANT_ID`, `PROPERTY_SALE_OFFER_PRICE`, `PROPERTY_SALE_SOLICITOR_ID`, `PROPERTY_SALE_OFFER_STATUS`, `PROPERTY_SALE_OFFER_NOTES`, `PROPERTY_SALE_OFFER_DATETIME`) VALUES 
			('$SALE_OFFER_ID', '$SALE_PROPERTY_ID', '$PROPERTY_APPLICANT_ID', '$PROPERTY_SALE_OFFER_PRICE', '$PROPERTY_SALE_SOLICITOR_ID', '$PROPERTY_SALE_OFFER_STATUS', '', $PROPERTY_SALE_OFFER_DATETIME)";
			$db_connect->queryExecute($sql) or die($sql);
    }
}
			$z = $row-2;
			echo $z." SALE OFFERS INSERTED SUCCESSFULLY";
}
?>  