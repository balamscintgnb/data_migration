<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/sales/Contacts.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$leveit = $insert_coun = $update_coun = $suply_no = $soli_no = 0;
    for($row=0; $row <=$total_rows; $row++){

            $name[]=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getformattedValue();
            // if ($name_1.length>0){
            //     $name_1=preg_replace("  ",'',$name_1);
            // }
            $address[]=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            // if ($address_1.length>0){
            //     $address_1=preg_replace("  ",'',$address_1);
            // }
            $mobile_no[]=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
            // if ($mob_1.length>0){
            //     $mob_1=preg_replace("  ",'',$mob_1);
            // }
            $find_type[]=trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());

        //logic for data mapping.

        if($row % 2 == 0){ 

			// echo "<pre>";
            // echo"------name<br/><br>";
            // print_r($name);
            // echo"-----add<br/><br>";
            // print_r($address);
            // echo"------mob<br/><br>";
				
				
				

            $CLIENT_TITLE='';
            $CLIENT_NAME='';
            $COMPANY_NAME='';
            $CLIENT_NAME= addslashes(trim(preg_replace("/(^\s+)|(\s+$)/us", "", @$name[0])));
            $COMPANY_NAME= trim(preg_replace("/(^\s+)|(\s+$)/us", "", @$name[1]));
            $CLIENT_TYPE='SUPPLIER';
            $CLIENT_SUB_TYPE=trim($find_type[0]);
            if($CLIENT_SUB_TYPE =='Solicitors'){
            	$CLIENT_TYPE = "SOLICITOR";
            	$CLIENT_SUB_TYPE = '';
            }else if($CLIENT_SUB_TYPE =='None'){
            	$CLIENT_SUB_TYPE = '';
            }
            if($CLIENT_TYPE =='SOLICITOR'){
            	$soli_no++;
            	$soli_id=str_pad($soli_no, 4, "0", STR_PAD_LEFT);
				$CLIENTID = 'SOL70'.$soli_id;
            }else if($CLIENT_TYPE =='SUPPLIER'){
            	$suply_no++;
            	$suply_id=str_pad($suply_no, 4, "0", STR_PAD_LEFT);
				$CLIENTID = 'SUP60'.$suply_id;
            }
            
            
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL= trim(preg_replace("/(^\s+)|(\s+$)/us", "", @$mobile_no[1]));      
            
            $number = explode("/",trim(preg_replace("/(^\s+)|(\s+$)/us", "", preg_replace('/[^0-9\/]/','',@$mobile_no[0]))));
            $CLIENT_PRIMARY_PHONE = trim(@$number[0]);
            $CLIENT_PHONE_1 = trim(@$number[1]);
            
            $CLIENT_ADDRESS_LINE_1 = $CLIENT_ADDRESS_CITY = '';
            $CLIENT_ADDRESS_LINE_1= trim(preg_replace("/(^\s+)|(\s+$)/us", "", @$address[0]));
            $CLIENT_ADDRESS_LINE_2='';
            $CLIENT_ADDRESS_CITY= trim(preg_replace("/(^\s+)|(\s+$)/us", "", @$address[1]));
            $CLIENT_ADDRESS_TOWN='';
            $CLIENT_ADDRESS_POSTCODE='';

            $CLIENT_ADDRESS1_LINE_1 = $CLIENT_ADDRESS1_LINE_2 = $CLIENT_ADDRESS1_CITY = $CLIENT_ADDRESS1_TOWN = $CLIENT_ADDRESS1_POSTCODE = '';
            $CLIENT_ADDRESS2_LINE_1 = $CLIENT_ADDRESS2_LINE_2 = $CLIENT_ADDRESS2_CITY = $CLIENT_ADDRESS2_TOWN = $CLIENT_ADDRESS2_POSTCODE = '';
            $CLIENT_ACCOUNT_NAME = $CLIENT_ACCOUNT_NO = $CLIENT_ACCOUNT_SORTCODE = $CLIENT_NOTES = '';
            $CLIENT_EMAIL_1 = $CLIENT_EMAIL_2 = $CLIENT_EMAIL_3 = $CLIENT_EMAIL_4 = $CLIENT_EMAIL_5 = '';
            $CLIENT_PHONE_2 = $CLIENT_PHONE_3 = $CLIENT_PHONE_4 = $CLIENT_PHONE_5 = $Main_id = $mob = $mob2 = $Email = '';
            $CLIENT_MOBILE_1 = $CLIENT_MOBILE_2 = $CLIENT_MOBILE_3 = $CLIENT_MOBILE_4 = $CLIENT_MOBILE_5 = '';
            $CLIENT_FAX_1 = $CLIENT_FAX_2 = $CLIENT_FAX_3 = $CLIENT_FAX_4 = $CLIENT_FAX_5 = $CLIENT_CREATED_ON = '';

            ///////////////// ADD SUB QUERY //////////////////////////
            if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
                if($CLIENT_NAME!=''){
                    $CLIENT_NAME.=" (".$COMPANY_NAME.")";
                }else{
                	$CLIENT_NAME.=$COMPANY_NAME;
                }
            }
            
            $get_client_query="SELECT * FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE '$CLIENT_TYPE'";
            $landlord_exist = json_decode($db_connect->queryFetch($get_client_query),true);
            if(isset($landlord_exist['data'][0]['CLIENTID']))
            {
            	$Main_id = trim(@$landlord_exist['data'][0]['CLIENTID']);
            	$mob = trim(@$landlord_exist['data'][0]['CLIENT_PRIMARY_PHONE']);
            	$mob2 = trim(@$landlord_exist['data'][0]['CLIENT_PHONE_1']);
            	$Email = trim(@$landlord_exist['data'][0]['CLIENT_PRIMARY_EMAIL']);
            	
            	if($mob!='' && $mob!=$CLIENT_PRIMARY_PHONE){
            		$Query1 = "UPDATE `clients` SET `CLIENT_PHONE_3` = '$mob' WHERE `CLIENTID` = '$Main_id'";
            		($db_connect->queryExecute($Query1)) ? $update_coun++ : die($Query1);
            	}
            	else{$leveit++;}
            	
            	if($mob2!='' && $mob2!=$CLIENT_PHONE_1){
            		$Query2 = "UPDATE `clients` SET `CLIENT_PHONE_2` = '$mob2' WHERE `CLIENTID` = '$Main_id'";
            		($db_connect->queryExecute($Query2)) ? $update_coun++ : die($Query2);
            	}
            	else{$leveit++;}
            	
            	if($Email!='' && $Email!=$CLIENT_PRIMARY_EMAIL){
            		$Query3 = "UPDATE `clients` SET `CLIENT_EMAIL_1` = '$Email' WHERE `CLIENTID` = '$Main_id'";
            		($db_connect->queryExecute($Query3)) ? $update_coun++ : die($Query3);
            	}else{$leveit++;}
            	
            	// echo "<pre>"; print_r($Query3); echo "<pre/>";
            	// echo "<pre>"; print_r("old  = ".$Email); echo "<pre/>";
            	// echo "<pre>"; print_r("new  = ".$CLIENT_PRIMARY_EMAIL); echo "<pre/>";
            }
            else
            {
				$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
					`CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
					`CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
					`CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
					`CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
					`CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
					`CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
					`CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME',
					'$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
					'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
					'$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
					'$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
					'$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
					'$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
					'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
					'$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','')";
				($db_connect->queryExecute($sql)) ? $insert_coun++ : die($sql);
            }
			$name = $address = $mobile_no = $find_type = [];
        }           
     }
    		$row = $row-1;
    		$z = $row/2;
    		echo "TOTAL ROW IS ".$z."<br>";
    		
    		if($insert_coun>0){
    			echo "<br><br>".$insert_coun." CLIENTS INSERTED SUCCESSFULY";
    		}
    		if($update_coun>0){
    			echo "<br><br>".$update_coun." CLIENTS UPDATED SUCCESSFULY";
    		}
}
?>