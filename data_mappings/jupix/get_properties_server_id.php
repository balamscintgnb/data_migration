<?php

include_once '../../classes/gnbcore.class.php';
include_once '../../classes/utility.class.php';
include_once '../../includes/config.php';

$db_connect = new GNBCore($sql_details, $isDevelopment);

if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*100;
}
else{
	$p=0;
	$page = 0;
}
$utility = new Utility();
$properties = $db_connect->get_datas(`cnb_chanin`.'properties', $page, 100);
if(sizeof($properties)>0){
$request_array = array();
$ids = array();
$assets = array();

foreach($properties as $property){
	$request_array[] = $property['PROPERTY_ID'];
	$ids[$property['PROPERTY_ID']] = $property['id'];
	$assets[$property['id']] = json_decode($property['PROPERTY_ASSETS']);
}
$subdomain_name = 'lgkl.dev';


 $url = 'https://' . $subdomain_name . '.globalnoticeboard.com/plugins/data_migration/get_property_server_id.php'; 



$request = json_encode(array('Token' => 1234, 'Data' => $request_array)); // token generate on both end

$header = array('Content-Type: application/json', 'Request-API-Token: GNB-DATA-1234-ASED');

$response = $utility->getCurlData($url, $request, $header, true);
$response = json_decode($response);


$success_ids = $response->success_ids;
foreach($success_ids as $key => $value){
	$id = $ids[$key];
//	$status = $db_connect->record_update_server_info($table, $id, $value);
//	$status = json_decode($status);
	//if($status->success){
		$property_assets = $assets[$id];
		$i=0;
	
	
		foreach($property_assets as $property_asset){
		
			if($property_asset->type=='photo' && $i==0){
				
				$columns = array('PROPERTY_SERVER_ID', 'PROPERTY_LOCAL_ID', 'PROPERTY_IMAGE_TYPE', 'PROPERTY_IMAGE');
				$values = array($value, $id, 'display_photo', $property_asset->path);
				$db_connect->insert_table_common($columns, $values, `cnb_chanin`.'property_images');
			}



			$columns = array('PROPERTY_SERVER_ID', 'PROPERTY_LOCAL_ID', 'PROPERTY_IMAGE_TYPE', 'PROPERTY_IMAGE');
			$values = array($value, $id, $property_asset->type, $property_asset->path);
			$db_connect->insert_table_common($columns, $values, `cnb_chanin`.'property_images');
			$i++;
		}
	//}
}
echo $p,$page;
?>
<script>
window.location="get_properties_server_id.php?p=<?= $p+1;?>";
</script>
<?php
}
