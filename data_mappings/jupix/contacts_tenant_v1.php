<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/lettings/tenancy.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$c1 = 0; $c2 = 0; $c3 = 0; $c4 = 0; $c5 = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            ////////// EMPTY ///////
            $CLIENTID1 = '';
            $CLIENTID2 = '';
            $CLIENTID3 = '';
            $CLIENTID4 = '';
            $CLIENTID5 = '';
            $CLIENT_NAME1 = '';
            $CLIENT_NAME2 = '';
            $CLIENT_NAME3 = '';
            $CLIENT_NAME4 = '';
            $CLIENT_NAME5 = '';

            //logic for data mapping.

            $CLIENTID1='40000'.$row;
            $CLIENT_TITLE='';
            $CLIENT_NAME = '';
                $NAME=str_replace("  ()","",$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                $CLIENT=str_replace("'", ' ', $NAME);
                $CLIENT_NA=str_replace("`", '', $CLIENT);
                $filter_name=explode(',', $CLIENT_NA);

                    $CLIENT_NAME1 = addslashes(trim($filter_name[0]));
                    $CLIENT_NAME2 = addslashes(trim($filter_name[1]));
                    $CLIENT_NAME3 = addslashes(trim($filter_name[2]));
                    $CLIENT_NAME4 = addslashes(trim($filter_name[3]));
                    $CLIENT_NAME5 = addslashes(trim($filter_name[4]));


                //echo "<pre>"; print_r($CLIENT_NAME2); echo"<pre>";
            $COMPANY_NAME='';
            $CLIENT_TYPE='Tenant';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();      
            $PHONE=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=preg_replace('/[^A-Za-z0-9\-]/', ' ', $PHONE);

            $CLIENT_ADDRESS_LINE_1='';
            $CLIENT_ADDRESS_LINE_2='';
            $CLIENT_ADDRESS_CITY='';
            $CLIENT_ADDRESS_TOWN='';
            $CLIENT_ADDRESS_POSTCODE='';

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';

            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';

            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';

            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';

            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';

            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';

            $CLIENT_NOTES='';

            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';

            //////////////////////////// ADD SUB QUERY /////////////////
            if($CLIENT_NAME2 != ''){
            	$c2++;
                $CLIENTID2='40002'.$row;
	            $Query2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
	                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
	                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
	                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
	                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
	                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
	                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
	                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID2', '', '$CLIENT_NAME2','Tenant', '', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '','')";
	            $db_connect->queryExecute($Query2);
            }
            if($CLIENT_NAME3 != ''){
            	$c3++;
                $CLIENTID3='40003'.$row;
                $Query3 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID3', '', '$CLIENT_NAME3','Tenant', '', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '','')";
            $db_connect->queryExecute($Query3);
            }
            if($CLIENT_NAME4 != ''){
            	$c4++;
                $CLIENTID4='40004'.$row;
                $Query4 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID4', '', '$CLIENT_NAME4','Tenant', '', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '','')";
            	$db_connect->queryExecute($Query4);
            }
            if($CLIENT_NAME5 != ''){
            	$c5++;
                $CLIENTID5='40005'.$row;
                $Query5 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID5', '', '$CLIENT_NAME5','Tenant', '', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '','', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '', '', '','', '', '', '', '','')";
            	$db_connect->queryExecute($Query5);
            }
            
            $Query1 = "SELECT * FROM clients WHERE CLIENT_NAME = '$CLIENT_NAME1' AND CLIENT_TYPE = '$CLIENT_TYPE' AND CLIENT_PRIMARY_EMAIL = '$CLIENT_PRIMARY_EMAIL' AND CLIENT_PRIMARY_PHONE = '$CLIENT_PRIMARY_PHONE'";
            
   //         $Query1 = "SELECT * FROM clients WHERE CLIENT_NAME = '$CLIENT_NAME1' AND CLIENT_TYPE = '$CLIENT_TYPE'";
			// $qry_str = '';
			
			// if(trim($CLIENT_PRIMARY_EMAIL)!='')
			// {
			// 	$qry_str = " AND CLIENT_PRIMARY_EMAIL = '$CLIENT_PRIMARY_EMAIL'";	
			// }
			
			
			// if(trim($CLIENT_PRIMARY_PHONE)!='')
			// {
			// 	if(trim($qry_str)!='')
			// 		$qry_str .= " AND CLIENT_PRIMARY_PHONE = '$CLIENT_PRIMARY_PHONE'";	
			// 	else
			// 		$qry_str = " AND CLIENT_PRIMARY_PHONE = '$CLIENT_PRIMARY_PHONE'";	
			// }
			
			// if(trim($qry_str)!=''){
			// 	$Query1 .= $qry_str;
			// }

			$Query1 = "SELECT * FROM clients WHERE CLIENT_NAME = '$CLIENT_NAME1' AND CLIENT_TYPE = '$CLIENT_TYPE' AND (CLIENT_PRIMARY_EMAIL = '$CLIENT_PRIMARY_EMAIL' OR CLIENT_PRIMARY_PHONE = '$CLIENT_PRIMARY_PHONE')";
             
            $Clients_exists = json_decode($db_connect->queryFetch($Query1),true);
			
				if(isset($Clients_exists['data'][0]['CLIENTID'])){
				
				}else{
          
				$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
				    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
				    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
				    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
				    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
				    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
				    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
				    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID1', '$CLIENT_TITLE', '$CLIENT_NAME1',
				    '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				    '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
				    '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
				    '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
				    '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
				    '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				    '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
				    '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','')";
				($db_connect->queryExecute($sql)) ? $c1++ : die($sql);
				}
        }
    }
    	$z = $c2+$c3+$c4+$c5;
    	$y = $row-2;
    	echo "TOTAL ROWS : ".$y. " & LEAD TENANT INSERTED SUCCESSFULLY COUNT IS : ".$c1."<br>";
	    echo $z." SHARED TENANT INSERTED SUCCESSFULLY";
}
?>