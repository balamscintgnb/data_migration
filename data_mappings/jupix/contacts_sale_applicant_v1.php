<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/sales/applicants.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$Tot_row = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

			// $update_Query = "UPDATE `clients` SET RECORD_UPLOADED = 5";
			// $db_connect->queryExecute($update_Query); echo "Success";
			// exit();

            $search_criteria=array();    
            //logic for data mapping.

            $CLIENT_TITLE = $COMPANY_NAME = $CLIENT_STAFF_ID = $CLIENT_SUB_TYPE='';
            $CLIENT_PRIMARY_PHONE = $CLIENT_ADDRESS = $CLIENT_ADDRESS_LINE_1 = $CLIENT_ADDRESS_LINE_2 = $CLIENT_ADDRESS_CITY = $CLIENT_ADDRESS_TOWN = $CLIENT_ADDRESS_POSTCODE='';
            $CLIENT_ADDRESS1_LINE_1 = $CLIENT_ADDRESS1_LINE_2 = $CLIENT_ADDRESS1_CITY = $CLIENT_ADDRESS1_TOWN = $CLIENT_ADDRESS1_POSTCODE = $CLIENT_ADDRESS2_LINE_1 = $CLIENT_ADDRESS2_LINE_2 = $CLIENT_ADDRESS2_CITY = $CLIENT_ADDRESS2_TOWN = $CLIENT_ADDRESS2_POSTCODE = $CLIENT_ACCOUNT_NAME = $CLIENT_ACCOUNT_NO = $CLIENT_ACCOUNT_SORTCODE = $CLIENT_EMAIL_1 = $CLIENT_EMAIL_2 = $CLIENT_EMAIL_3 = $CLIENT_EMAIL_4 = $CLIENT_EMAIL_5 = $CLIENT_PHONE_1 = $CLIENT_PHONE_2 = $CLIENT_PHONE_3 = $CLIENT_PHONE_4 = $CLIENT_PHONE_5='';
			$CLIENT_MOBILE_1 = $CLIENT_MOBILE_2 = $CLIENT_MOBILE_3 = $CLIENT_MOBILE_4 = $CLIENT_MOBILE_5 = $CLIENT_NOTES = $CLIENT_FAX_1 = $CLIENT_FAX_2 = $CLIENT_FAX_3 = $CLIENT_FAX_4 = $CLIENT_FAX_5 = $CLIENT_CREATED_ON='';

            ///////////////FILTER SPECIAL CHERACTOR///////////////////////
            $applicant_name = str_replace(" ()","",trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
            $CLIENT_NAM1 = str_replace(",", " &", $applicant_name);
            $CLIENT_NAM = preg_replace('/[^A-Za-z0-9\-]/', ' ', $CLIENT_NAM1);
            $CLIENT_NAME = addslashes(trim($CLIENT_NAM));

            $CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $CLIENT_TYPE='applicant';
            $CLIENT_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
                if($CLIENT_STATUS == 'Archived'){
                    $CLIENT_STATUS = 'Inactive';
                }

            $CLIENT_PRIMARY_EMAIL = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
            $get_address = explode('-', preg_replace('/[\x00-\x1F\x80-\xFF]/', '',trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getformattedValue())));
            $count_val = count($get_address);
            if($count_val==2)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
                $CLIENT_ADDRESS_LINE_2='';
                $CLIENT_ADDRESS_CITY='';
                $CLIENT_ADDRESS_TOWN='';
                $CLIENT_ADDRESS_POSTCODE=$get_address[1];
            } else if($count_val==3)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
                $CLIENT_ADDRESS_LINE_2='';
                $CLIENT_ADDRESS_CITY=$get_address[1];
                $CLIENT_ADDRESS_TOWN='';
                $CLIENT_ADDRESS_POSTCODE=$get_address[2];
            } else if($count_val==4)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
                $CLIENT_ADDRESS_LINE_2=$get_address[1];
                $CLIENT_ADDRESS_CITY=$get_address[2];
                $CLIENT_ADDRESS_TOWN='';
                $CLIENT_ADDRESS_POSTCODE=$get_address[3];
            } else if($count_val==5)    {
                    $CLIENT_ADDRESS_LINE_1=$get_address[0];
                    $CLIENT_ADDRESS_LINE_2=$get_address[1];
                    $CLIENT_ADDRESS_CITY=$get_address[2];
                    $CLIENT_ADDRESS_TOWN=$get_address[3];
                    $CLIENT_ADDRESS_POSTCODE=str_replace('.','',$get_address[4]);
            } else if($count_val==6)    {
                $CLIENT_ADDRESS_LINE_1=$get_address[0].$get_address[1];
                $CLIENT_ADDRESS_LINE_2=$get_address[2];
                $CLIENT_ADDRESS_CITY=$get_address[3];
                $CLIENT_ADDRESS_TOWN=$get_address[4];
                $CLIENT_ADDRESS_POSTCODE=str_replace('.','',$get_address[5]);
            }
            else { 
                $CLIENT_ADDRESS_LINE_1=$get_address[0];
            }
            $CLIENT_ADDRESS_LINE_1 = addslashes(trim($CLIENT_ADDRESS_LINE_1));
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim($CLIENT_ADDRESS_LINE_2));
            $CLIENT_ADDRESS_CITY = addslashes(trim($CLIENT_ADDRESS_CITY));
            $CLIENT_ADDRESS_TOWN = addslashes(trim($CLIENT_ADDRESS_TOWN));
            $CLIENT_ADDRESS_POSTCODE= addslashes(trim($CLIENT_ADDRESS_POSTCODE));

            $MOBILE_1 = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
            $CLIENT_NUMBER = preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1);

            //ADD SEARCH CRITE AREA ///
            $Maximum_Price = trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $Minimum_Bedrooms = trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            $Minimum_Bathrooms = trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
            $Minimum_Ensuites = trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
            $Minimum_Toilets = trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
            $Minimum_Reception_Rooms = trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            $Floor_Area_Actual_Units_Sq_Ft = trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
            $Minimum_Acres = trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
            $Property_Style = trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
            $Property_Style = explode('-',$Property_Style);
            $Property_Style= implode(' | ',$Property_Style);
            $category='43';

            $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$Property_Style,
            'property_applicant_search_attribute_2_from'=>$Minimum_Bedrooms, 
            'property_applicant_search_attribute_2_to'=>'',
            'property_applicant_search_attribute_3_from'=>$Minimum_Bathrooms, 
            'property_applicant_search_attribute_3_to'=>'',
            'property_applicant_search_attribute_4_from'=>$Minimum_Reception_Rooms, 
            'property_applicant_search_attribute_4_to'=>'');

            $search_criteria=array('price'=>$Maximum_Price,'pricefrom'=>'',
            'filter_array'=>$applicant_search_criteria,
            'frequency'=>'','category'=>$category,'location'=>'');

            $SEARCH_CRITERIA = json_encode($search_criteria);

            if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
                if($CLIENT_NAME!=''){
                    $CLIENT_NAME.=' ';
                }
                $CLIENT_NAME.='('.$COMPANY_NAME.')';
            }

             /////////////MOBILE NUMBER TYPE////////////////
             $tel_type = '';
             $tel_type = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
 
             if($tel_type=="Mobile"){
                 $CLIENT_MOBILE_1 = $CLIENT_NUMBER;
             }
 
             else if($tel_type=="Home"){
                 $CLIENT_PRIMARY_PHONE = $CLIENT_NUMBER;   
             }  
             else if($tel_type=="Work"){
                 $CLIENT_PHONE_1 = $CLIENT_NUMBER;   
             }
 
             //////////////// END /////////////////
             if($CLIENT_NAME)
             {
 
			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, 
			    `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`,
			    `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, 
			    `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, 
			    `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, 
			    `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, 
			    `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, 
			    `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,
			    `SEARCH_CRITERIA`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', 
			    '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2',
			    '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', 
			    '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
			    '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO',
			    '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5',
			    '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1',
			    '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', 
			    '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '$SEARCH_CRITERIA','')";
			($db_connect->queryExecute($sql)) ? $Tot_row++ : die($sql);
             }
        }
    }
    		$z = $row-2;
	    	echo "TOTAL ROWS : ".$z. " & APPLICANTS INSERTED SUCCESSFULLY COUNT IS : ".$Tot_row;
}
?>