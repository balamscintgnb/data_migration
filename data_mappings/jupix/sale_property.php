<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/sales/sales.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$Total_row = 0;
		$update_count = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
                    //$id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()*/;
        
        

        //var_dump($id);

        //logic for data mapping.
        

        $Enter_PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $pro_id = str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
            $PROPERTY_ID = ''.$pro_id;
        $PROPERTY_REF_ID = $objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
        $PROPERTY_VENDOR_ID = $objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

        $PROPERTY_BUYER_ID = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_BUYER_SOLICITOR_ID = $objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();
        $PROPERTY_VENDOR_SOLICITOR_ID = $objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue();

        $PROPERTY_STAFF_ID = '';
        $PROPERTY_TITLE = '';
        $PROPERTY_SHORT_DESCRIPTION = '';
        $KITCHEN = '';
        $Toilets = '';
        $PROPERTY_DESCRIPTION = '';
        $PROPERTY_CATEGORY = 'RESIDENTIAL SALES';
        $PROPERTY_PRICE = $objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY='';
        $PROPERTY_QUALIFIER=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_AVAILABLE_DATE='NULL';

        $PROPERTY_ADDRESS_LINE_1 = '';

            $add_sub_type_1 = $objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
            $add_sub_type_2 = $objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            $add_sub_type_3 = $objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();

        if($add_sub_type_1!=''){
            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
        } 

        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
        }
        
        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=' ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
        }      
            
        $PROPERTY_ADDRESS_LINE_2 = $objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY = $objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY = $objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
        $PROPERTY_ADDRESS_POSTCODE = $objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
        $PROPERTY_FORMATTED_ADDRESS = '';
        $STATUS = '';
        $PROPERTY_AVAILABILITY='SOLD';

        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE='';
        $PROPERTY_TYPE='';

        $PROPERTY_BEDROOMS='';
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION='';
        $PROPERTY_TENURE='';
        $Lease_term_years='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
       
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON='NULL';
        $PROPERTY_STATUS='';
        $Sale_Status = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
        
        ///////////////////////////// ADD MORE FIELDS /////////////////////////
        
        $Query1 = "SELECT `PROPERTY_ID` FROM `properties` WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";
        $Property_exist = json_decode($db_connect->queryFetch($Query1),TRUE);
        $property_ID2 = $Property_exist['data'][0]['PROPERTY_ID'];
        
        if($property_ID2 = $PROPERTY_ID){
            if(stristr($Sale_Status,'Completed')){
                $Query2 = "UPDATE `properties` SET `PROPERTY_BUYER_SOLICITOR_ID` = '$PROPERTY_BUYER_SOLICITOR_ID', `PROPERTY_VENDOR_SOLICITOR_ID` = '$PROPERTY_VENDOR_SOLICITOR_ID', `PROPERTY_BUYER_ID` = '$PROPERTY_BUYER_ID' WHERE `PROPERTY_ID` = '$PROPERTY_ID' `PROPERTY_CATEGORY` = '$PROPERTY_CATEGORY'";
                ($db_connect->queryExecute($Query2)) ? $update_count++ : die($Query2);
                // echo "<pre>";print_r($Query2);echo "<pre/>";
            }
        }
        else{
        	$Total_row++;
        $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
                `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
                `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
                `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
                `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
                `KITCHEN_DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
                `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
                `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
                `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
                `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
                `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
                `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
                `CERTIFICATE_EXPIRE_DATE`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,`PROPERTY_BUYER_ID`,
                `PROPERTY_VENDOR_SOLICITOR_ID`,`PROPERTY_BUYER_SOLICITOR_ID`,`PROPERTY_NOTES`,`PROPERTY_LETTING_FEE_FREQUENCY`,`PROPERTY_LETTING_FEE_TYPE`,
            	`PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`) 
                VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE ,
                '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                '$PROPERTY_IMAGE_EPC_5', '$CERTIFICATE_EXPIRE_DATE', '$PROPERTY_EPC_VALUES', $PROPERTY_CREATED_ON,'$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','$PROPERTY_ASSETS',
                '$PROPERTY_BUYER_ID', '$PROPERTY_VENDOR_SOLICITOR_ID', '$PROPERTY_BUYER_SOLICITOR_ID','','','','','','')";
		    //insert into table    
		    $db_connect->queryExecute($sql) or die($sql);

    		}
        }
	}
			echo$Total_row." PROPERTIES INSERTED SUCCESSFULLY<br>";
			echo$update_count." PROPERTIES UPDATED";
			
}
?>