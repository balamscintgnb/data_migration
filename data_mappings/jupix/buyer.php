<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/sales/sales.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$solicit_row = 0;$Buyer_row = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
            //logic for data mapping.

			$update_Query = "update `clients` set `RECORD_UPLOADED` = 5";
			$db_connect->queryExecute($update_Query); echo "Success";
			exit();
			
            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $CLIENT_TITLE='';
            $NAME=$objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue();
            $CLIENT=str_replace("'", ' ', $NAME);
            $CLIENT_NA=str_replace("`", '', $CLIENT);
            $CLIENT_NAME=str_replace(","," &",$CLIENT_NA);
            $CLIENT_NAME = addslashes(trim($CLIENT_NAME));
            $COMPANY_NAME='';
            $CLIENT_TYPE='BUYER';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue();      
            $CLIENT_PRIMARY_PHONE='';
            $MOBILE_1 = addslashes($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue());           
            $CLIENT_NUMBER = preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1);
            $CLIENT_ADDRESS_LINE_1='';
            $CLIENT_ADDRESS_LINE_2='';
            $CLIENT_ADDRESS_CITY='';
            $CLIENT_ADDRESS_TOWN='';
            $CLIENT_ADDRESS_POSTCODE='';

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';

            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';

            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';

            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';

            $CLIENT_PHONE_1 = addslashes($objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue());
            $CLIENT_PHONE_2 = addslashes($objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue());
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';

            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';

            $CLIENT_NOTES='';

            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';
            $Sale_Status = trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
            
            //////////////// BUYER_SOLICITOR_DETAILS ////////////
            /////////// START //////////////
            $BUYER_SOLICITOR_ID = trim($objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue());
            $BUYER_SOLICITOR_NAME = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue()));
            $BUYER_SOLICITOR_landline = trim($objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue());
            $BUYER_SOLICITOR_MOB = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue()));
            $BUYER_SOLICITOR_FAX = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue()));
            $BUYER_SOLICITOR_EMAIL = trim($objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue());

			/////////// END ///////////
			
			//////////////// VENDOR_SOLICITOR_DETAILS ////////////
            /////////// START //////////////
            $VENDOR_SOLICITOR_ID = trim($objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue());
            $VENDOR_SOLICITOR_NAME = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue()));
            $VENDOR_SOLICITOR_landline = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue()));
            $VENDOR_SOLICITOR_MOB = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue()));
            $VENDOR_SOLICITOR_FAX = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue()));
            $VENDOR_SOLICITOR_EMAIL = trim($objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue());

			/////////// END ///////////
             ///////////// MOBILE NUMBER TYPE ////////////////
			$tel_type='';
			$tel_type=$objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue();
			if($tel_type=="Mobile"){
			$CLIENT_MOBILE_1 = $CLIENT_NUMBER;
			}
			else if($tel_type=="Home"){
			$CLIENT_PRIMARY_PHONE = $CLIENT_NUMBER;   
			}  
			else if($tel_type=="Work"){
			$CLIENT_PHONE_1 = $CLIENT_NUMBER;   
			}
			//////////////// END /////////////////
			
			$Query1 = "SELECT * FROM `clients` WHERE CLIENTID = '$CLIENTID' AND CLIENT_TYPE LIKE 'applicant' ";
			$Clients_Find = json_decode($db_connect->queryFetch($Query1),true);
			
			if(@$Clients_Find['data'][0]['CLIENTID']){
			$CLIENT_NAME = addslashes($Clients_Find['data'][0]['CLIENT_NAME']);
			$CLIENT_PRIMARY_EMAIL = addslashes($Clients_Find['data'][0]['CLIENT_PRIMARY_EMAIL']);
			$CLIENT_PRIMARY_PHONE = addslashes($Clients_Find['data'][0]['CLIENT_PRIMARY_PHONE']);
			$CLIENT_ADDRESS_LINE_1 = addslashes($Clients_Find['data'][0]['CLIENT_ADDRESS_LINE_1']);
			$CLIENT_ADDRESS_LINE_2 = addslashes($Clients_Find['data'][0]['CLIENT_ADDRESS_LINE_2']);
			$CLIENT_ADDRESS1_CITY = addslashes($Clients_Find['data'][0]['CLIENT_ADDRESS1_CITY']);
			$CLIENT_ADDRESS1_TOWN = addslashes($Clients_Find['data'][0]['CLIENT_ADDRESS1_TOWN']);
			$CLIENT_ADDRESS_POSTCODE = addslashes($Clients_Find['data'][0]['CLIENT_ADDRESS_POSTCODE']);
			$CLIENT_MOBILE_1 = addslashes($Clients_Find['data'][0]['CLIENT_MOBILE_1']);
			$CLIENT_MOBILE_2 = addslashes($Clients_Find['data'][0]['CLIENT_MOBILE_2']);
			$CLIENT_MOBILE_3 = addslashes($Clients_Find['data'][0]['CLIENT_MOBILE_3']);
			$CLIENT_PHONE_1 = addslashes($Clients_Find['data'][0]['CLIENT_PHONE_1']);
			$CLIENT_PHONE_2 = addslashes($Clients_Find['data'][0]['CLIENT_PHONE_2']);
			$CLIENT_FAX_1 = addslashes($Clients_Find['data'][0]['CLIENT_FAX_1']);
			}
			
			
			if($Sale_Status == 'Completed'){
				$Query2 = "SELECT * FROM `clients` WHERE CLIENTID = '$CLIENTID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
				$Clients_Exist = json_decode($db_connect->queryFetch($Query2),true);
				if(@$Clients_Exist['data'][0]['CLIENTID']){

				}
				else{
        			$Buyer_row++;
			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
			    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
			    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
			    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
			    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
			    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
			    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
			    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME',
			    '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
			    '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
			    '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
			    '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
			    '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
			    '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
			    '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
			    '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','')";
			$db_connect->queryExecute($sql) or die($sql);
             }
			}
			
			$CLIENT_TYPE = 'SOLICITOR';
			
			if($BUYER_SOLICITOR_ID != ''){
				$Query3 = "SELECT * FROM `clients` WHERE CLIENTID = '$BUYER_SOLICITOR_ID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
				$Clients_Solici1 = json_decode($db_connect->queryFetch($Query3),true);
				if(@$Clients_Solici1['data'][0]['CLIENTID']){
					#continue;
				}
				else{
        			$solicit_row++;
			$sql1 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
			    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
			    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
			    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
			    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
			    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
			    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
			    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$BUYER_SOLICITOR_ID', '', '$BUYER_SOLICITOR_NAME',
			    '$CLIENT_TYPE', '', '', '', '$VENDOR_SOLICITOR_EMAIL', '$BUYER_SOLICITOR_landline', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
			    '$BUYER_SOLICITOR_MOB', '', '', '', '', '', '$VENDOR_SOLICITOR_FAX', '', '', '', '', '$CLIENT_CREATED_ON','')";
			$db_connect->queryExecute($sql1) or die($sql1);
             }
			}
			
			if($VENDOR_SOLICITOR_ID != ''){
				$Query4 = "SELECT * FROM `clients` WHERE CLIENTID = '$VENDOR_SOLICITOR_ID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
				$Clients_Solici2 = json_decode($db_connect->queryFetch($Query4),true);
				if(@$Clients_Solici2['data'][0]['CLIENTID']){
					#continue;
				}
				else{
        			$solicit_row++;
			$sql2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
			    `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
			    `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
			    `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
			    `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
			    `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
			    `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
			    `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$VENDOR_SOLICITOR_ID', '', '$VENDOR_SOLICITOR_NAME',
			    '$CLIENT_TYPE', '', '', '', '$VENDOR_SOLICITOR_EMAIL', '$VENDOR_SOLICITOR_landline', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
			    '$VENDOR_SOLICITOR_MOB', '', '', '', '', '', '$VENDOR_SOLICITOR_FAX', '', '', '', '', '$CLIENT_CREATED_ON','')";
			$db_connect->queryExecute($sql2) or die($sql2);
             }
			}
			
        }
    }
    		echo $Buyer_row . " BUYER INSERTED SUCCESSFULLY<br>";
    		echo $solicit_row . " SOLICITOR INSERTED SUCCESSFULLY";
}
?>