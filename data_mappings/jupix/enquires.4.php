<?php

require_once 'includes/db.php';
//require_once 'includes/cnb/notices.php';
require_once 'includes/cnb.php';
//require_once 'includes/cnb/cnb_info.php';

require_once '../../plugin_includes/RightMove/ParseBLM_class.php';

require_once '../../plugin_includes/common_functions.php';

require_once '../../../Excel/Classes/PHPExcel.php';
		
require_once '../../../Excel/Classes/PHPExcel/IOFactory.php';

//
$plugin_path = '../plugins/address_book/';
include_once 'includes/cnb/cnb_clients.php';
include_once('includes/cnb/cnb_lettings.php');
include_once 'includes/cnb/categories.php';
//

extract($_POST);

$post_verify=1;
$test_sync=0;

if(isset($post_verify)&&$post_verify==1){
	

	$log_format='d-m-Y H:i:s';
	
	//todo login verification
    
	$response='';
	$log="\n";
	$added_excel=0;
	$failed_excel=0;
	$this_proceed=false;
	
	//
	$cnb_clients = new CNB_Clients($db);
	$cnb_lettings 	= new CNB_Lettings($db);
	$category_class = new Category_extend_class($db);
	
	//$objRichText = new PHPExcel_RichText();
	//$htmlHelper = new PHPExcel_Helper_HTML();
	
	$client_type=2;
	$client_type_label='';
	
	if($client_type==2){
		$client_type_label='Applicants';
	}
	
	if($test_sync!=1){
	    $log.=date($log_format).': '.$subdomain_cnb.' - Export intialized.'."\n";
	}else{
	    $log.=date($log_format).': '.$subdomain_cnb.' - Test Export intialized.'."\n";
	}
	
	
	if(!$_GET['id']){
		$page=0;
		$excel_row=1;
	}
	else{
		$page=$_GET['id'];
	}



	$estate_cnb_categories = $category_class->get_cnb_categories();
	$categories_name = array();
	foreach($estate_cnb_categories as $category_info){
		$categories_name[$category_info->category_id] = $category_info->category_name;
	}
	$price_freq_types = array('1'=>'pa','2'=>'pcm','3'=>'pw');
	
	
	
	//file apth 
	
	
	
		$today_date=date('d_m_Y');
	$logs = 'logs/';
	$today_folder = 'data/'.$subdomain_cnb.'/';
	
	$zip_file_name=date('d_m_Y_H_i');
	$zip_file=$subdomain_cnb.'-'.$zip_file_name;
	
	$excel_file=$subdomain_cnb.'-Clients-'.$today_date.'.xls';
	$log_file_path=$logs.$subdomain_cnb.'-'.$today_date.'.txt';
	
	if(!is_dir($today_folder)){
		mkdir($today_folder, 0777, true);
	}
	
	if(!is_dir($logs)){
		mkdir($logs, 0777);
	}
	
	//backups delete
	if($test_sync!=1){
		$format='d-m-Y';
	    $threshold_date=date($format, strtotime(" +3 days"));
	    
	    // ParseBLM::delete_files_by_date($logs, $threshold_date, array('txt'), $format);
	    // $log.=date($log_format).': '.$subdomain_cnb.' - Log files < '.$threshold_date.' deleted.'."\n";
	    
	    // ParseBLM::delete_files_by_date($today_folder, $threshold_date, array('xls', 'zip', 'jpeg', 'jpg', 'png'), $format);
	    // $log.=date($log_format).': '.$subdomain_cnb.' - Data files < '.$threshold_date.' deleted.'."\n";
	}
        
        
        
        
    $excel_file_path=$today_folder.$excel_file;
   
 
			
    $log.=date('d-m-Y H:i:s').': '.$subdomain_cnb.' - Export started.'."\n";
    
   	$objPHPExcel = PHPExcel_IOFactory::load($excel_file_path);

    
	$objPHPExcel->getProperties()->setCreator("GNB")
			 ->setLastModifiedBy("GNB")
			 ->setTitle(ucwords($subdomain_cnb).' - '.$client_type_label)
			 ->setSubject('Export')
			 ->setDescription("Export for All $client_type_label")
			 ->setKeywords("$client_type_label")
			 ->setCategory("Xml");
			 
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("$client_type_label");
	$objPHPExcel->getActiveSheet()->setCellValue('A1', "Title");
	$objPHPExcel->getActiveSheet()->setCellValue('B1', "Name");
	$objPHPExcel->getActiveSheet()->setCellValue('C1', "Email");
	$objPHPExcel->getActiveSheet()->setCellValue('D1', "Company");
	$objPHPExcel->getActiveSheet()->setCellValue('E1', "Sub Type");
	//$objPHPExcel->getActiveSheet()->setCellValue('F1', "Phone");
	//$objPHPExcel->getActiveSheet()->setCellValue('G1', "Address");
	$objPHPExcel->getActiveSheet()->setCellValue('F1', "Notes");
	$objPHPExcel->getActiveSheet()->setCellValue('G1', "Contact Information");
	
	$objPHPExcel->getActiveSheet()->setCellValue('H1', "Opt In sent");
	$objPHPExcel->getActiveSheet()->setCellValue('I1', "Opt Expired On");
	$objPHPExcel->getActiveSheet()->setCellValue('J1', "Opt Submitted On");
	
	$objPHPExcel->getActiveSheet()->setCellValue('K1', "Guarantors");
	$objPHPExcel->getActiveSheet()->setCellValue('L1', "Search Criteria");
	
	
	
	//file path 
	
	
	if($page==0 || $page<135){ 

	$cnb_clients->set('contact_type', $client_type);
	$address = $cnb_clients->get_clients($page);
	
	//var_dump($address);
	



// removed  



	//$user_id_array = array();
    $address_id_array = array();
    foreach ($address as $address_row) {
        //$user_id_array[] = $address_row->address_user_id;
        $address_id_array[] = $address_row->address_id;
    }

    $address_notes_count_array = $cnb_clients->get_client_notes_count($address_id_array);
	//
	
	$log.="--------------------------------------------------------\n";
        

// file path  removed 



    //if($cnb_detail->cnb_status==0){
    	

	
	
	
	
	
	
    if(count($address)>0){
    	
    
    	
    	$excel_row = $objPHPExcel->getActiveSheet()->getHighestRow()+1;

    	
    	$i=0;
    	foreach ($address as $address_row) {
           
            $address_id = $address_row->address_id;
            $address_notes_count = 0;
            if (isset($address_notes_count_array->{$address_id})) {
                $address_notes_count = $address_notes_count_array->{$address_id};
            }
            $address_user_id = $address_row->address_user_id;
			//$address_optin_datetime = $address_row->address_optin_datetime;
			
            //$title = ($address_row->address_user_name != "") ? ucwords($address_row->address_user_name) : 'Unknown';
    	
    		//foreach($notices_and_users as $notice_each){
    		
    		/*echo '<pre><code>';
    		var_dump($address_row);
    		echo '</code></pre>';
    		
    		exit;*/
    
    	
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$excel_row, $address_row->address_user_title);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$excel_row, $address_row->address_user_name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$excel_row, $address_row->address_user_email);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$excel_row, $address_row->address_company_name);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$excel_row, $address_row->address_book_sub_type);
			//$objPHPExcel->getActiveSheet()->setCellValue('F'.$excel_row, $address_row->address_user_phone);
			//$objPHPExcel->getActiveSheet()->setCellValue('G'.$excel_row, $address_row->address_user_address);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$excel_row, $address_notes_count);
			
			//
			$cnb_lettings->set('contact_primary_id', $address_id);
			$cnb_lettings->get_property_applicant();
			
			/*$property_applicant_id = $cnb_lettings->get('property_applicant_id');
			$property_applicant_name = $cnb_lettings->get('property_applicant_name');
			$property_applicant_email = $cnb_lettings->get('property_applicant_email');*/
			$property_applicant_contacts = $cnb_lettings->get('property_applicant_contacts');
			$property_applicant_search_criteria = $cnb_lettings->get('property_applicant_search_criteria');
			
			//
			$contact_type_value='';
			$search_info='';
			foreach($property_applicant_contacts as $property_applicant_contact){
				
				if($property_applicant_contact->address_type_id==30){
					$contact_type_value.='Landline: '.$property_applicant_contact->address_type_value.",\n";
				}
				
				if($property_applicant_contact->address_type_id==10){
					$contact_type_value.='Mobile: '.$property_applicant_contact->address_type_value.",\n";
				}
				
				if($property_applicant_contact->address_type_id==100){
					$contact_type_value.='Postal Address: '.$property_applicant_contact->address_type_value.",\n";
				}
				
				if($property_applicant_contact->address_type_id==20){
					$contact_type_value.='Email: '.$property_applicant_contact->address_type_value.",\n";
				}
				
				if($property_applicant_contact->address_type_id==40){
					$contact_type_value.='Fax: '.$property_applicant_contact->address_type_value.",\n";
				}
				
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$excel_row, $contact_type_value);
			
			$opt_in_time=' - ';
			if($cnb_lettings->get('contact_user_send_optin')!='0000-00-00 00:00:00'){
				$opt_in_time = date('d.m.Y H:i', strtotime($cnb_lettings->get('contact_user_send_optin')));
			}
			
			$opt_expired_time=' - ';
			if($cnb_lettings->get('contact_user_optin_expire_on')!='0000-00-00 00:00:00'){
				$opt_expired_time = date('d.m.Y H:i', strtotime($cnb_lettings->get('contact_user_optin_expire_on')));
			}
			
			$opt_submitted_time=' - ';
			if($cnb_lettings->get('contact_user_optin_submitted_on')!='0000-00-00 00:00:00'){
				$opt_submitted_time = date('d.m.Y H:i', strtotime($cnb_lettings->get('contact_user_optin_submitted_on')));
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$excel_row, $opt_in_time);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$excel_row, $opt_expired_time);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$excel_row, $opt_submitted_time);
			
			//
			$cnb_lettings->set('contact_primary_id',$address_id);
			$address_book_notes = $cnb_lettings->get_address_book_notes();
			
			//var_dump($address_book_notes); exit;
			
			$notes='';
			
			foreach($address_book_notes as $address_book_note){
				/*$objRichText->createText($address_book_note->address_notes."\n");

				$objBold = $objRichText->createTextRun(date('d.m.Y H:i',strtotime($address_book_note->address_notes_datetime)));
				$objBold->getFont()->setBold(true);*/
				
				//$notes.=$address_book_note->address_notes."\n".date('d.m.Y H:i',strtotime($address_book_note->address_notes_datetime)).",\n\n";
				
				$notes .= "$address_book_note->address_notes\n".date('d.m.Y H:i',strtotime($address_book_note->address_notes_datetime))."\n\n";
				
			}
			
			//$rich_text = $htmlHelper->toRichTextObject($notes);
			//
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$excel_row, $notes);
			
			//
			$grantors = $cnb_lettings->get_address_user_grantors();
			
			$guarantors_info='';
			
			foreach($grantors as $grantor){
			
				$guarantor_id = $grantor->property_letting_grantor_id;
				$cnb_lettings->set('contact_primary_id',$guarantor_id);
				$cnb_lettings->get_letting_client();
				
				$guarantors_info .= "Name: ".$cnb_lettings->get('contact_user_name')."\n";
				$guarantors_info .= "Email: ".$cnb_lettings->get('contact_user_email')."\n";
				$guarantors_info .= "Relation: ".ucfirst($grantor->property_letting_grantor_relation)."\n";
				$guarantor_contacts = $cnb_lettings->get('contact_other_fields');
				
				$guarantor_contact_info='';
				foreach($guarantor_contacts as $guarantor_contact){
				
					if($guarantor_contact->address_type_id==30){
						$guarantor_contact_info .= 'Landline: '.$guarantor_contact->address_type_value."\n";
					}
					
					if($guarantor_contact->address_type_id==10){
						$guarantor_contact_info .= 'Mobile: '.$guarantor_contact->address_type_value."\n";
					}
					
					if($guarantor_contact->address_type_id==100){
						$guarantor_contact_info .= 'Postal Address: '.$guarantor_contact->address_type_value."\n";
					}
				}
				$guarantors_info .= $guarantor_contact_info."\n";
			}
			//


			$objPHPExcel->getActiveSheet()->setCellValue('K'.$excel_row, $guarantors_info);
			//
			/*$currency_symbol = get_currency_symbol($wallet->get('wallet_currency'));
			$address_book_status = $cnb_lettings->get('contact_status');*/
			
			//
			
			//address_user_address
			
			//
	$search_criteria_field_array = $property_applicant_search_criteria;
			$search_criteria='';
			
			$k=0;
			$location_map_fields = array();
			foreach($search_criteria_field_array as $search_criteria_field){
			
				$price_from = $search_criteria_field->property_applicant_search_price_from;
				$price_to = $search_criteria_field->property_applicant_search_price_to;
				$price_freq = $search_criteria_field->property_applicant_search_price_frequency;
				
				$location = $search_criteria_field->property_applicant_search_location;
				$category_id = $search_criteria_field->property_applicant_search_category;
				$search_id = $search_criteria_field->property_applicant_id;
				
				$cnb_lettings->set('property_applicant_search_category',$category_id);
				$filters = $cnb_lettings->get_property_applicant_search_criteria_form();
				
				$property_category='';
				if($category_id==44)
				{ 
					$property_category = 'Residential lettings';
				}
				else if($category_id==43)
				{ 
					$property_category = 'Residential Sales';
				}
				else if($category_id==45)
				{ 
					$property_category = 'Commercial sales';
				}
				else if($category_id==46)
				{ 
					$property_category = 'Commercial lettings';
				}
				
				$search_info .=  'Min Price:'.$price_from."\n";
				$search_info .=  'Max Price: '.$price_to."\n";
				$search_info .=  'Max Frequency: '.$price_freq."\n";
				$search_info .=  'Location: '.$location."\n";
				$search_info .=  'Property Category: '.$property_category."\n";

				$filter_attribute = array();
				foreach($filters as $key => $value){
					
					$attribute_id = $key;
					$filter = $value;
					$field_id = $filter['id'];
				
					if($filter['type']=='numeric'){
						if(strstr($attribute_id,'_from')){
							$filter_min = $search_criteria_field->{$attribute_id};
							$search_info .=  $value['label'].':'.$filter_min."\n";

						}
						if(strstr($attribute_id,'_to')){
							$filter_max = $search_criteria_field->{$attribute_id};
						   $search_info .=  $value['label'].':'.$filter_max."\n";

						}
						if($filter_min>0){
							$filter_attribute['numeric'][$field_id]['min'] = $filter_min;
						}
						if($filter_max>0){
							$filter_attribute['numeric'][$field_id]['max'] = $filter_max;
						}

					}else if($filter['type']=='categorical'){
						$filter_categorical_value = $search_criteria_field->{$attribute_id};
						if($filter_categorical_value!=''){
							$filter_attribute['categorical'][$field_id] = $filter_categorical_value;
						 $search_info .=  $value['label'].':'.$filter_categorical_value."\n";

						}
					}else if($filter['type']=='boolean'){
						$filter_boolean = $search_criteria_field->{$attribute_id};
						if($filter_boolean==1){
							$filter_attribute['boolean'][$field_id] = $filter_boolean;
							 $search_info .=  $value['label'].': Yes'."\n";

						}
					}
					
					

				}
				
				

			}
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$excel_row, $search_info); 
			
			$added_excel++;
			
			$excel_row++;
		 $i++;
				if($i%30==0){
					$ct=$page=$_GET['id']+1;
					echo '<script> window.location.href="export_clients.php?id='.$ct.'" </script>';
				}
				if($_GET['id'] == 133)
				{
						$this_proceed=true;
				}
		
    	}
    }
	
	//$objWorkSheet = $objPHPExcel->createSheet("$transaction_type-$instruction-$archived");
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	//$objWriter->save($excel_file_path);
	$objWriter->save($excel_file_path);

    /*}else{
        $response.='Error:- CNB Status, '.$cnb_detail->cnb_status.'<br/>';
        $log.=date($log_format).': '.$subdomain_cnb.' - CNB Status:- '.$cnb_detail->cnb_status.".\n";
    }*/
    
    
    //todo delete all copied, generated files
	if(ParseBLM::is_dir_empty($today_folder)){
	    if(@rmdir($today_folder)){
	        $log.=date($log_format).': '.$subdomain_cnb.' - Folder removed - '.$today_folder.".\n";
	    }
	}

	$log.=date($log_format).': '.$subdomain_cnb.' - '.$client_type_label.' Inserted - '.$added_excel.', Failed -'.$failed_excel.".\n";
	
	$log.="--------------------------------------------------------\n\n\n";
	
	writeLogFile($log_file_path, $log);
	}

	if($this_proceed){
		
		/*header('Content-Description: File Transfer');
	    //header('Content-Type: application/octet-stream');
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0, max-age=0');
	    header('Pragma: public');
		    
		    
		header('Content-type: application/vnd.ms-excel');
		// It will be called file.xls
		header('Content-Disposition: attachment; filename="'.$excel_file_path.'"');
		$objWriter->save('php://output');*/
		
		if (file_exists($excel_file_path)) {
		
			header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename='.$excel_file);
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($excel_file_path));
		     
		    ob_clean();
		    flush();
		    readfile($excel_file_path);
		    exit;
	    } 
	}
}
?>