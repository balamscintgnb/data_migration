<?php
// Report all PHP errors (see changelog)
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once 'notices.php';

$limit = 20;

$software='me3_images';
$cnb='me3';
$clientID=847;

$totalProperties=0;
$totalImages=0;
$totalImagesCopied=0;
$totalPropertiesWithImages=0;

foreach($notices as $notice){

	$api =  (int)str_replace('10293100', '', $notice['notice_api_item_id']);
	$api_source =  (int)str_replace('10293100', '', $notice['notice_api_item_id']);

	if(!is_dir("$software/$api_source/")){
		mkdir("$software/$api_source/", true);
	}

	for($i=1; $i<=$limit;$i++){

		echo $url = "http://media2.jupix.co.uk/v3/clients/$clientID/properties/$api/IMG_".$api."_".$i."_large.jpg";
		echo '<br/>';
		//$url = "http://cdn1.jupix.co.uk/v3/clients/847/properties/2386/IMG_2386_7_large.jpg";

		$request = curl_get_file_info($url, 'Content-Length');

		if($request>0){
			$request1 = getUrlContent($url);

			if($i==1){
				$totalPropertiesWithImages++;
			}

			if($request1){
				$fileMetaData = pathinfo($url);
				$totalImages++;

				if(file_put_contents("$software/$cnb/".$fileMetaData['basename'], $request1)){
					$totalImagesCopied++;
				}
			}
		}
		sleep(1);
	}

	$totalProperties++;
	//break;
}


echo 'Total Properties: '.$totalProperties.'<br/>';
echo 'Total Images: '.$totalImagesCopied.'<br/>';
echo 'Total Properties with Images: '.$totalImagesCopied.'<br/>';



function getUrlContent($url, $data=array()){
    
    $postData = "";
    if(is_array($data)){
        foreach( $data as $key => $val ) {
           $postData .=$key."=".$val."&";
        }
        $postData = rtrim($postData, "&");
    }
    
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:57.0) Gecko/20100101 Firefox/57.0');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_POST, 1);
	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 0);
	
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	 
	$html = curl_exec($ch);

	//handle curl error 
	$chError = curl_errno($ch);

    if($chError) {
       $html=false;
    }
	
	curl_close($ch);
	
	return $html;
}

/**
 * Returns the header/meta info of remote file.
 *
 * @param $url - The location of the remote file to download. Cannot
 * be null or empty.
 *
 * @param $headerName - The header info of the remote file to. Cannot
 * be null or empty.
 * 
 * @return The header info of the file referenced by $url with $headerName or -1.
 */
 
function curl_get_file_info( $url, $headerName ) {
    
    $curl = curl_init( $url );
    
    // Issue a HEAD request and follow any redirects.
    curl_setopt( $curl, CURLOPT_NOBODY, true );
    curl_setopt( $curl, CURLOPT_HEADER, true );
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    
    $headerData = curl_exec( $curl );
    
	curl_close( $curl );
	
    $header = "";
    
    if( $headerData ) {
        
		$status = "";
        
        if( preg_match_all( "/HTTP\/1\.[01] (\d\d\d)/", $headerData, $matches ) ) {
			$status = (int)$matches[1][count($matches[1])-1];
		}
        
        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
            $header = get_header_from_curl_response($headerData, $headerName);
        }
    }
    
    return $header;
}

function get_header_from_curl_response($headerContent, $headerName){
    
    $header='';

    // Split the string on every "double" new line.
	$arrRequests = explode("\r\n\r\n", $headerContent);

    // Loop of response headers. The "count() -1" is to 
    //avoid an empty row for the extra line break before the body of the response.
    $break=false;
    for ($index = count($arrRequests)-2; $index >= 0; $index--) {
        foreach (explode("\r\n", $arrRequests[$index]) as $i => $line){
            $key = explode(': ', $line);
            if($key[0]==$headerName){
                $header = $key[1];
                $break=true;
                break;
            }
        }
        if($break){
        	break;
        }
    }
   
    return $header;
}
?>