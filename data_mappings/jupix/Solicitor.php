<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/Cottage/Sales/sales.xls';

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.


            ///////////////// Owner Solicitor Maping //////////////////////

            $CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue());
            $CLIENT_NAME = trim($objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue());
            $COMPANY_NAME = '';
            $CLIENT_TYPE='SOLICITOR';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();      
            $CLIENT_PRIMARY_PHONE = '';
            $MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();           
            $CLIENT_NUMBER = preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1);
            $CLIENT_ADDRESS_LINE_1 = '';
            $CLIENT_ADDRESS_LINE_2 = '';
            $CLIENT_ADDRESS_CITY = '';
            $CLIENT_ADDRESS_TOWN = '';
            $CLIENT_ADDRESS_POSTCODE = '';

            $CLIENT_ADDRESS1_LINE_1 = '';
            $CLIENT_ADDRESS1_LINE_2 = '';
            $CLIENT_ADDRESS1_CITY = '';
            $CLIENT_ADDRESS1_TOWN = '';
            $CLIENT_ADDRESS1_POSTCODE = '';

            $CLIENT_ADDRESS2_LINE_1 = '';
            $CLIENT_ADDRESS2_LINE_2 = '';
            $CLIENT_ADDRESS2_CITY = '';
            $CLIENT_ADDRESS2_TOWN = '';
            $CLIENT_ADDRESS2_POSTCODE = '';

            $CLIENT_ACCOUNT_NAME = '';
            $CLIENT_ACCOUNT_NO = '';
            $CLIENT_ACCOUNT_SORTCODE = '';

            $CLIENT_EMAIL_1 = '';
            $CLIENT_EMAIL_2 = '';
            $CLIENT_EMAIL_3 = '';
            $CLIENT_EMAIL_4 = '';
            $CLIENT_EMAIL_5 = '';

            $CLIENT_PHONE_1 = '';
            $CLIENT_PHONE_2 = '';
            $CLIENT_PHONE_3 = '';
            $CLIENT_PHONE_4 = '';
            $CLIENT_PHONE_5 = '';

            $CLIENT_MOBILE_1 = '';
            $CLIENT_MOBILE_2 = '';
            $CLIENT_MOBILE_3 = '';
            $CLIENT_MOBILE_4 = '';
            $CLIENT_MOBILE_5 = '';

            $CLIENT_NOTES = '';

            $CLIENT_FAX_1 = '';
            $CLIENT_FAX_2 = '';
            $CLIENT_FAX_3 = '';
            $CLIENT_FAX_4 = '';
            $CLIENT_FAX_5 = '';
            $CLIENT_CREATED_ON = '';

            ///////////////// ADD SUB QUERY //////////////////////////

            //////////////// END /////////////////

             /////////////MOBILE NUMBER TYPE////////////////
             $tel_type = '';
             $tel_type = $objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();
             $tel_type2 = '';
             $CLIENT_NUMBER2 = '';
             $tel_type2 = $objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();
             $CLIENT_NUMBER2 = $objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
             $tel_type3 = '';
             $CLIENT_NUMBER3 = '';
             $tel_type3 = $objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();
             $CLIENT_NUMBER3 = $objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue();
 
 
             if($tel_type=="Mobile"){
                 $CLIENT_MOBILE_1 = $CLIENT_NUMBER;
             }
             else if($tel_type=="Home"){
                 $CLIENT_PRIMARY_PHONE = $CLIENT_NUMBER;   
             }  
             else if($tel_type=="Work"){
                 $CLIENT_PHONE_1 = $CLIENT_NUMBER;   
             }
             else if($tel_type=="Office"){
                $CLIENT_PHONE_2 = $CLIENT_NUMBER;   
            }
 
            if($tel_type2 == "Home"){
                $CLIENT_PHONE_3 = $CLIENT_NUMBER2;
            }
            else if($tel_type2 == "Work"){
                $CLIENT_PHONE_4 = $CLIENT_NUMBER2;   
            }
            else if($tel_type2 == "Fax"){
                $CLIENT_FAX_1 = $CLIENT_NUMBER2;   
            }

            if($tel_type3 == "Home"){
                $CLIENT_PHONE_5 = $CLIENT_NUMBER2;
            }
            else if($tel_type3 == "Work"){
                $CLIENT_MOBILE_2 = $CLIENT_NUMBER2;   
            }
            
             //////////////// END /////////////////


             ///////////////////////// Applicant Solicitor Maping /////////////////////////////


            $CLIENT_PHONE2_1 = '';
            $CLIENT_PHONE2_2 = '';
            $CLIENT_PHONE2_3 = '';
            $CLIENT_PHONE2_4 = '';
            $CLIENT_PHONE2_5 = '';

            $CLIENT_MOBILE2_1 = '';
            $CLIENT_MOBILE2_2 = '';
            $CLIENT_MOBILE2_3 = '';
            $CLIENT_MOBILE2_4 = '';
            $CLIENT_MOBILE2_5 = '';

            $CLIENT_FAX2_1 = '';
            $CLIENT_FAX2_2 = '';
            $CLIENT_FAX2_3 = '';
            $CLIENT_FAX2_4 = '';
            $CLIENT_FAX2_5 = '';

            $CLIENTID2 = '';
            $CLIENT_NAME2 = '';
            $CLIENT_PRIMARY_EMAIL2 = '';
            $MOBILE1 = '';
            $MOB_TYPE1 = '';
            $MOB_TYPE2 = '';
            $MOBILE2 = '';
            $MOB_TYPE3 = '';
            $MOBILE3 = '';





            $CLIENTID2 = trim($objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue());
            $CLIENT_NAME2 = trim($objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue());
            $CLIENT_PRIMARY_EMAIL2=$objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue();      
             
            
            $MOB_TYPE1 = $objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue();
            $MOBILE1=$objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue();          
            $MOB_TYPE2 = $objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue();
            $MOBILE2 = $objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue();
            
            $MOB_TYPE3 = $objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue();
            $MOBILE3 = $objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue();



            if($MOB_TYPE1=="Work"){
                $CLIENT_MOBILE2_1 = $MOBILE1;
            }
            else if($MOB_TYPE1=="Home"){
                $CLIENT_PHONE2_2 = $MOBILE1;   
            }  
            else if($MOB_TYPE1=="Office"){
                $CLIENT_PHONE2_1 = $MOBILE1;   
            }
            

           if($MOB_TYPE2 == "Home"){
               $CLIENT_PHONE2_3 = $MOBILE2;
           }
           else if($MOB_TYPE2 == "Work"){
               $CLIENT_PHONE2_4 = $MOBILE2;   
           }
           else if($MOB_TYPE2 == "Fax"){
               $CLIENT_FAX2_1 = $MOBILE2;   
           }

           if($MOB_TYPE3 == "Home"){
               $CLIENT_PHONE2_5 = $MOBILE3;
           }
           else if($MOB_TYPE3 == "Work"){
               $CLIENT_MOBILE2_2 = $MOBILE3;   
           }
           else if($MOB_TYPE3 == "Other"){
            $CLIENT_MOBILE2_3 = $MOBILE3;   
           }
           else if($MOB_TYPE3 == "Fax"){
            $CLIENT_FAX2_2 = $MOBILE3;   
           }
            


           ////////////////////////// END /////////////////////
            

        
          
        $Query1 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES ('$CLIENTID', '', '$CLIENT_NAME',
                '$CLIENT_TYPE', '', '', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '', '', '', '', '','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','', '',
                 '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON')";
            // echo $row."<br/><br/>";
        $db_connect->queryExecute($Query1) or die($Query1);


        $Query2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES ('$CLIENTID2', '', '$CLIENT_NAME2',
                '$CLIENT_TYPE', '', '', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL2', '$CLIENT_PRIMARY_PHONE', '', '', '', '', '','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','', '',
                 '$CLIENT_PHONE2_1', '$CLIENT_PHONE2_2', '$CLIENT_PHONE2_3', '$CLIENT_PHONE2_4', '$CLIENT_PHONE2_5',
                '$CLIENT_MOBILE2_1', '$CLIENT_MOBILE2_2', '$CLIENT_MOBILE2_3', '$CLIENT_MOBILE2_4', '$CLIENT_MOBILE2_5', '$CLIENT_NOTES', '$CLIENT_FAX2_1',
                '$CLIENT_FAX2_2', '$CLIENT_FAX2_3', '$CLIENT_FAX2_4', '$CLIENT_FAX2_5', '$CLIENT_CREATED_ON')";
            // echo $row."<br/><br/>";
        $db_connect->queryExecute($Query2) or die($Query2);


        }
    }
}
?>