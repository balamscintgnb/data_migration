<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/gareth/lettings/properties.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$Total_row = 0;$valuat = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        //logic for data mapping.
        
	$update_Query = "UPDATE `valuations` SET `RECORD_UPLOADED` = 0 WHERE `PROPERTY_ID` = '0126'";
	$db_connect->queryExecute($update_Query); echo "SUCCESS";
	exit();
        $Enter_PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
            $PROPERTY_ID = ''.$pro_id;
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
        $PROPERTY_LANDLORD_ID=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
        $PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();
        $PROPERTY_TITLE='';
        $PROPERTY_SHORT_DESCRIPTION='';
        $KITCHEN=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();
        $Toilets=$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
        $PROPERTY_DESCRIPTION='';

            if($KITCHEN){
                $PROPERTY_DESCRIPTION.="KITCHEN : ".$KITCHEN;
            }
            if($Toilets){
                if($KITCHEN){
                    $PROPERTY_DESCRIPTION.="<br>";
                }
                $PROPERTY_DESCRIPTION.="Toilets : ".$Toilets;
            }

        $PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue();
        $PROPERTY_QUALIFIER='';
        $given_date=trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue());

            if($given_date!=''){
            $test_date = explode(' ',$given_date);
            $test_date1 = explode('/',$test_date[0]);
            $PROPERTY_AVAILABLE_DATE="'".$test_date1[2].'-'.$test_date1[1].'-'.$test_date1[0]."'";
            }
            if($given_date==''){
                $PROPERTY_AVAILABLE_DATE="NULL";
            }


            $PROPERTY_ADDRESS_LINE_1='';

            $add_sub_type_1=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $add_sub_type_2=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $add_sub_type_3=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();

        if($add_sub_type_1!=''){
            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
        } 

        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
        }
        
        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=' ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
        }
        $PROPERTY_ADDRESS_LINE_1 = addslashes($PROPERTY_ADDRESS_LINE_1);
        $PROPERTY_ADDRESS_CITY='';
        $PROPERTY_ADDRESS_LINE_2 = addslashes($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
        $PROPERTY_ADDRESS_CITY = addslashes($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
        $PROPERTY_ADDRESS_COUNTY = addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
        $PROPERTY_ADDRESS_POSTCODE = addslashes($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
        $PROPERTY_FORMATTED_ADDRESS='';
        $STATUS='';
        $PROPERTY_AVAILABILITY='';

        $get_status = explode('-',$objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getformattedValue());
            $count_val = count($get_status);
            if($count_val==2)    {
                $STATUS=trim($get_status[1]);
                $PROPERTY_AVAILABILITY=trim($get_status[0]);
            } else if($count_val==3){
                $STATUS=trim($get_status[1].$get_status[2]);
                $PROPERTY_AVAILABILITY=trim($get_status[0]);
            }else{
                $PROPERTY_AVAILABILITY = trim($get_status[0]);
            }

        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE='';
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
        if($PROPERTY_TYPE ==''){
        	$PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
        }
        

        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
        $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
        $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_TENURE='';
        $Lease_term_years='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $DINING_ROOM='';
        $FURNISHED=$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON='';
        $CERTIFICATE_EXPIRE_DATE='NULL';
        $PROPERTY_LETTING_SERVICE = '';

        $given_date_1=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue(),'d M y');
            if($given_date_1!=''){
   
            $PROPERTY_CREATED_ON="'".$given_date_1."'";
            }
            if($given_date_1==''){
                $PROPERTY_CREATED_ON="NULL";
            }
        
        if($PROPERTY_ADDRESS_CITY==''){
            $PROPERTY_ADDRESS_CITY.=$PROPERTY_ADDRESS_LINE_2;
        }  if($PROPERTY_ADDRESS_CITY==''){
            $PROPERTY_ADDRESS_CITY.=$PROPERTY_ADDRESS_COUNTY;
        } 
        /////////////////////////////ADD MORE FIELDS/////////////////////////
        $area_squre_feet=array();
        $read_squre_feet=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
        if($read_squre_feet=='0'){
            $read_squre_feet='';
        } else{
            $area_squre_feet=array();
        }
        
        if($read_squre_feet>0){
                $area_squre_feet[]="Floor Area Squre Feet : ".$read_squre_feet;
        }
        
        $PROPERTY_CUSTOM_FEATURES="'".json_encode($area_squre_feet)."'";

        $PROPERTY_STATUS='';
        
        if($STATUS=='On Market'){
            $PROPERTY_STATUS='ACTIVE';
        }else{
            $PROPERTY_STATUS = '';
        }


        if(stristr($PROPERTY_TYPE,'Flat')){
                $PROPERTY_TYPE = 'Flat';
        }else if(stristr($PROPERTY_TYPE,'Maisonette')){
            $PROPERTY_TYPE='Maisonette';
        }else if($PROPERTY_TYPE=='Flat Semi Detached'){
            $PROPERTY_TYPE='Semi Detached';
        }else if($PROPERTY_TYPE=='Bungalow Detached'){
            $PROPERTY_TYPE='detached bungalow';
        }else if($PROPERTY_TYPE=='House'){
            $PROPERTY_TYPE='house';
        }else if($PROPERTY_TYPE =='Detached House' || $PROPERTY_TYPE=='Link Detached'){
            $PROPERTY_TYPE='detached house';
        }else if($PROPERTY_TYPE=='End Terraced House'){
            $PROPERTY_TYPE='End Terrace'; 
        }else if($PROPERTY_TYPE=='House Maisonette'){
            $PROPERTY_TYPE='Maisonette';
        }else if($PROPERTY_TYPE=='House Semi Detached' || $PROPERTY_TYPE=='Semi-Detached House'){
            $PROPERTY_TYPE='semi-detached house';
        }else if($PROPERTY_TYPE=='House Terraced' || $PROPERTY_TYPE=='Mid Terraced House'){
            $PROPERTY_TYPE='terraced';
        }else if($PROPERTY_TYPE=='Studio Flat'|| $PROPERTY_TYPE=='Studio'){
            $PROPERTY_TYPE='Studio';
        }else if($PROPERTY_TYPE=='Offices'){
            $PROPERTY_TYPE='office';
        }


        if($PROPERTY_PRICE=='0'){
            $PROPERTY_PRICE_FREQUENCY='';
        }
        
        $FORMATTED_ADDRESS_FINAL = '';
		$POSTCODE = $PROPERTY_ADDRESS_POSTCODE;
		
		$POSTCODE2 = explode(" ",$POSTCODE);
		
		$FORMATTED_ADDRESS1 = preg_replace("/[^A-Za-z?![:space:]]/","",$PROPERTY_ADDRESS_LINE_1);
		$FORMATTED_ADDRESS2 = preg_replace("/[^A-Za-z?![:space:]]/","",$PROPERTY_ADDRESS_LINE_2);
		$FORMATTED_ADDRESS3 = preg_replace("/[^A-Za-z?![:space:]]/","",$PROPERTY_ADDRESS_CITY);
		
		if(trim($FORMATTED_ADDRESS2)!='')
		{
		  $FORMATTED_ADDRESS_FINAL = $FORMATTED_ADDRESS2;
		}
		else if(trim($FORMATTED_ADDRESS3)!='')
		{
		  $FORMATTED_ADDRESS_FINAL = $FORMATTED_ADDRESS3;
		}
		else if(trim($FORMATTED_ADDRESS1)!='' && strlen($FORMATTED_ADDRESS1)>0)
		{
		  $FORMATTED_ADDRESS_FINAL = $FORMATTED_ADDRESS1;
		}
		
		if(is_array($POSTCODE2) && isset($POSTCODE2[0]))
		{
		  if(trim($FORMATTED_ADDRESS_FINAL)!='')
		    $PROPERTY_FORMATTED_ADDRESS = $FORMATTED_ADDRESS_FINAL.", ".$POSTCODE2[0];
		  else
		    $PROPERTY_FORMATTED_ADDRESS = $POSTCODE2[0];
		}
		else
		{
		  $PROPERTY_FORMATTED_ADDRESS = $FORMATTED_ADDRESS_FINAL;
		}
        
        //////////////////////////// ADD QUERY //////////////////////////



        if($PROPERTY_AVAILABILITY == 'Appraisal'){

            $PROPERTY_APPOINTMENT_STARTTIME="NULL";
            $PROPERTY_APPOINTMENT_ENDTIME="NULL";
            $PROPERTY_AGE='';
            $PROPERTY_CONDITION='';
            $PROPERTY_PRICE_FROM='0';
            $PROPERTY_VENDOR_PRICE='';
            $PROPERTY_PROPOSED_PRICE='';
            $PROPERTY_VALUER_NOTES='';
            $PROPERTY_VALUER_NOTES_1='';
            $PROPERTY_VALUER_NOTES_2='';
            $PROPERTY_VALUER_NOTES_3='';
            $PROPERTY_VALUER_NOTES_4='';
            $PROPERTY_VALUER_NOTES_5='';


            $Query2 = "SELECT * FROM `valuations` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
			$Valuation_exists = json_decode($db_connect->queryFetch($Query2),true);
			
			if(@$Valuation_exists['data'][0]['PROPERTY_ID']){
			    
			}else{
				if(stristr($STATUS, 'Carried Out'))
				{
		            $PROPERTY_STATUS='1';
		        }
		        elseif(stristr($STATUS, 'Cancelled') || stristr($STATUS, 'Lost Dead') || stristr($STATUS, 'Pending'))
		        {
		            $PROPERTY_STATUS='0';
		        }
		        elseif(stristr($STATUS,'Won'))
		        {
		        	$PROPERTY_STATUS='2';
        		}
        		else
		        {
		            $PROPERTY_STATUS='0';
		        }

			$appraisal_sql="INSERT INTO `valuations`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`,
		        `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`,
		        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`,
		        `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`,
		        `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`,
		        `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`) VALUES
		        ('$PROPERTY_ID','$PROPERTY_REF_ID','$PROPERTY_LANDLORD_ID','$PROPERTY_CATEGORY','$PROPERTY_ADDRESS_LINE_1','$PROPERTY_ADDRESS_LINE_2',
		        '$PROPERTY_ADDRESS_CITY','$PROPERTY_ADDRESS_COUNTY','$PROPERTY_ADDRESS_POSTCODE','$PROPERTY_STATUS','$PROPERTY_TYPE',
		        '$PROPERTY_BEDROOMS','$PROPERTY_BATHROOMS','$PROPERTY_RECEPTION','$PROPERTY_TENURE','$PROPERTY_AGE','$PROPERTY_CLASSIFICATION',
		        '$PROPERTY_CONDITION','$PROPERTY_PRICE_FROM','$PROPERTY_PRICE','$PROPERTY_VENDOR_PRICE','$PROPERTY_PROPOSED_PRICE',
		        '$PROPERTY_VALUER_NOTES','$PROPERTY_VALUER_NOTES_1','$PROPERTY_VALUER_NOTES_2','$PROPERTY_VALUER_NOTES_3','$PROPERTY_VALUER_NOTES_4',
		        '$PROPERTY_VALUER_NOTES_5',$PROPERTY_APPOINTMENT_STARTTIME,$PROPERTY_APPOINTMENT_ENDTIME)";
			($db_connect->queryExecute($appraisal_sql)) ? $valuat++ : die($appraisal_sql);
			}
	        }
	        else{
        	$Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
			$property_exists = json_decode($db_connect->queryFetch($Query1),true);
			
			if(@$property_exists['data'][0]['PROPERTY_ID']){
			    
			}else{
	        	$Total_row++;
			$sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
				`PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
				`PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
				`PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
				`PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
				`KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
				`DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
				`DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
				`CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
				`PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
				`PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
				`PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
				`PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
				`PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`,`PROPERTY_NOTES`,`PROPERTY_LETTING_FEE_FREQUENCY`,`PROPERTY_LETTING_FEE_TYPE`,
            	`PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`) 
				VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_LANDLORD_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
				'$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
				'$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
				'$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
				'$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
				'$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
				'$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
				'$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
				'$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
				'$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
				'$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
				'$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
				'$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
				'$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', $PROPERTY_CREATED_ON, '0','','','','$PROPERTY_LETTING_SERVICE',NULL,'','','','','','')";
			$db_connect->queryExecute($sql) or die($sql);
        	}
		}
	}
}
				echo$Total_row." PROPERTIES INSERTED SUCCESSFULLY <br><br>";
				echo$valuat." VALUATION INSERTED SUCCESSFULLY";
}
?>