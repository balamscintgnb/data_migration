<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/jupix/charles/sales/Viewings_All.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns


if($thisProceed){
		$cont_count = $insert_coun = $number = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
			
			// $Update_Query = "DELETE FROM `clients` WHERE `RECORD_UPLOADED` = 1";
			// $db_connect->queryExecute($Update_Query);echo "Success";
			// exit();
			
            //logic for data mapping.
            $Find_Query = "SELECT count(*) as num_rows FROM `viewings`";
            $Find_locat = json_decode($db_connect->queryFetch($Find_Query),true);
            $VIEWING = (isset($Find_locat['data'][0]['num_rows']))?$Find_locat['data'][0]['num_rows']+1: 0;

        	$view_id=str_pad($VIEWING, 5, "0", STR_PAD_LEFT);
            $VIEWING_ID = 'VIEW2'.$view_id;
            
            $VIEWING_APPLICANT_ID = 0;
            $VIEWING_USER_ID = '';
            $VIEWING_PROPERTY_ID = '';

			$get_property_address = $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $address = explode(',',$get_property_address);
            $count_part = count($address);
            if($count_part == 3)
				$address_1 = addslashes(trim($address[0]));
            else if($count_part == 4)
            {
				$address_1 = addslashes(trim($address[0]).', '.trim($address[1]));
				$address_2 = addslashes(trim($address[0]).' '.trim($address[1]));
            }

			$address_available_query = "SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_ADDRESS_LINE_1` LIKE '$address_1' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";
            $address_available = json_decode($db_connect->queryFetch($address_available_query),true);

			if(isset($address_available['data'][0]['PROPERTY_ID']))
				$VIEWING_PROPERTY_ID=$address_available['data'][0]['PROPERTY_ID'];
            else
            {
				$address_3 = trim(str_replace('  ',' ',$address[0]));
				$address_available_query_test = "SELECT PROPERTY_ID FROM properties WHERE PROPERTY_ADDRESS_LINE_1 LIKE '$address_3' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";
				$address_available_test = json_decode($db_connect->queryFetch($address_available_query_test),true);
             
				if(isset($address_available_test['data'][0]['PROPERTY_ID']))
				{
					$VIEWING_PROPERTY_ID=$address_available_test['data'][0]['PROPERTY_ID'];
				}
				else
				{
				$VIEWING_PROPERTY_ID = 0;
				$address_available_query_test = "SELECT PROPERTY_ID FROM properties WHERE PROPERTY_ADDRESS_LINE_1 LIKE '$address_3' AND `PROPERTY_CATEGORY` LIKE 'RESIDENTIAL SALES'";
				$address_available_test = json_decode($db_connect->queryFetch($address_available_query_test),true);
				}
            }

            $VIEWING_DATETIME='';
            $given_date = Utility::convert_tosqldatetime($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue(),'D dS M y h:ia');

	            if($given_date!='')
	            {
	            $VIEWING_DATETIME="'".$given_date."'";
	            }
	            if($given_date=='')
	            {
	                $VIEWING_DATETIME="NULL";
	            }

            $VIEWING_NOTES = '';
            $VIEWING_STATUS = '';
            $VIEWING_TYPE = "1";
            $applicant_name = str_replace(" ()","",$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $CLIENT_NAM1 = str_replace(",", " &", $applicant_name);
            $CLIENT_NAM = preg_replace('/[^A-Za-z0-9\-]/', ' ', $CLIENT_NAM1);
            $CLIENT_NAME = addslashes(trim($CLIENT_NAM));
            $CLIENT_TYPE = 'applicant';

            $get_client_query="SELECT CLIENTID FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE '$CLIENT_TYPE'";
            $landlord_exist = json_decode($db_connect->queryFetch($get_client_query),true);

	            if(isset($landlord_exist['data'][0]['CLIENTID'])){
	            $VIEWING_APPLICANT_ID=$landlord_exist['data'][0]['CLIENTID'];
	            }
            	else
            	{
                $query_2="SELECT CLIENTID FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE '$CLIENT_TYPE'";
                $landlord_exist2 = json_decode($db_connect->queryFetch($query_2),true);  

	                if(isset($landlord_exist2['data'][0]['CLIENTID']))
	                {
	                    $VIEWING_APPLICANT_ID=$landlord_exist2['data'][0]['CLIENTID'];
	                }
	                else
	                {
						// $VIEWING_APPLICANT_ID = '2002'.$row;
						$number++;
	                	$pro_id=str_pad($number, 4, "0", STR_PAD_LEFT);
						$VIEWING_APPLICANT_ID = 'APP22'.$pro_id;
						echo "<pre>" ,$VIEWING_APPLICANT_ID, "</pre>";
						$query3="INSERT INTO `clients` (`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`CLIENT_COMPANY_NAME`) VALUES ('$VIEWING_APPLICANT_ID', '', '$CLIENT_NAME', '$CLIENT_TYPE', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '','')";
						($db_connect->queryExecute($query3)) ? $cont_count++ : die($query3);
						// echo"<pre>"; print_r($query3); echo "</pre>";
	                }
            	}

            $status_get=strtolower($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());

            if(stristr($status_get,'cancel'))
            {
                $VIEWING_STATUS='2';
            }
            else if(stristr($status_get,'pending'))
            {
                $VIEWING_STATUS='0';
            }
            else if(stristr($status_get,'follow'))
            {
                $VIEWING_STATUS='1';
            }
            else
            {
                $VIEWING_STATUS='0';
            }

			$sql ="INSERT INTO `viewings` (`VIEWING_ID`, `VIEWING_APPLICANT_ID`, `VIEWING_USER_ID`, `VIEWING_PROPERTY_ID`, `VIEWING_DATETIME`, `VIEWING_NOTES`, `VIEWING_FOLLOWUP`, `VIEWING_TYPE`, `VIEWING_STATUS`) VALUES 
			('$VIEWING_ID', '$VIEWING_APPLICANT_ID', '$VIEWING_USER_ID', '$VIEWING_PROPERTY_ID', $VIEWING_DATETIME, '$VIEWING_NOTES', '', '$VIEWING_TYPE', '$VIEWING_STATUS')";
			($db_connect->queryExecute($sql)) ? $insert_coun++ : die ($sql);
        }
    }
    		$z = $row-2;
            echo "TOTAL ROW IS ".$z."<br><br>";
            if($insert_coun>0){
                echo "SUCCESSFULY INSERTED VIEWINGS COUNT IS : ".$insert_coun;
            }
            if($cont_count>0){
                echo "SUCCESSFULY INSERTED APPLICANT COUNT IS : ".$cont_count;
            }
}
?>