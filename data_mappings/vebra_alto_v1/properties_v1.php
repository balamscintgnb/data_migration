<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 1000);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/arbon/Property.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            $PROPERTY_ADMIN_FEES='';
            $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $PROPERTY_ID = str_replace('_', '', $PROPERTY_ID);
            $PROPERTY_ID ='13879_'.$PROPERTY_ID;        
            $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

            $PROPERTY_STAFF_ID='';
            $PROPERTY_TITLE='';

            $PROPERTY_SHORT_DESCRIPTION='';
            $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue();

            $property_category_code=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue(); 
            
            
            if($PROPERTY_REF_ID !='' ) {
                $sale_qry = "SELECT Price FROM sale_property WHERE `PropertyId` = '$PROPERTY_REF_ID'";
                $sale_qry_exists = json_decode($db_connect->queryFetch($sale_qry),true);
               // echo '<pre>',print_r($sale_qry_exists['data'][0]['Price']),'</pre>';
               $PROPERTY_PRICE=''; 
                if(@$sale_qry_exists['data'][0]['Price']>0){
                    $PROPERTY_PRICE=$sale_qry_exists['data'][0]['Price'];
                }               
            }
            

            if($PROPERTY_PRICE>0){
                $PROPERTY_CATEGORY = "RESIDENTIAL SALES";  
                $PROPERTY_AVAILABILITY = 'For sale';     
                $PROPERTY_TENURE='Freehold';       
            }else{
                $PROPERTY_CATEGORY = "RESIDENTIAL LETTINGS";
                $PROPERTY_AVAILABILITY = 'Let Agreed';
                $PROPERTY_TENURE='Short Let or Long let';
            } 
           // echo $PROPERTY_PRICE.'<br/>';
            $PROPERTY_PRICE_FREQUENCY='';           
            $PROPERTY_QUALIFIER='';
            $PROPERTY_AVAILABLE_DATE='NULL';
            $PROPERTY_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();            

            $address = explode(',',$PROPERTY_ADDRESS);
            
            $PROPERTY_ADDRESS_LINE_1 = @addslashes($address[0]);
            $PROPERTY_ADDRESS_LINE_2 = @addslashes($address[1]);
            
            $PROPERTY_ADDRESS_CITY = @addslashes($address[2]);
            $PROPERTY_ADDRESS_COUNTY = @addslashes($address[3]);

            if($PROPERTY_ADDRESS_CITY==''){
                $PROPERTY_ADDRESS_CITY = @addslashes($address[1]); 
            }
            if($PROPERTY_ADDRESS_COUNTY==''){
                $PROPERTY_ADDRESS_COUNTY = @addslashes($address[2]); 
            }  
            $PROPERTY_ADDRESS_POSTCODE='';
            $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
            $PROPERTY_FORMATTED_ADDRESS= addslashes($PROPERTY_FORMATTED_ADDRESS); 
            $PROPERTY_STATUS='';

            $active_status = trim($objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue());

            

            if($active_status=='True'){
                 $PROPERTY_STATUS='';              
            }else{
                $PROPERTY_STATUS='ACTIVE';
            }
          //  echo $PROPERTY_STATUS.'<br/>';
            $PROPERTY_ADMIN_FEES='';
            $PROPERTY_TYPE='';

            $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
            $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            $Lease_term_years='';
            $PROPERTY_CLASSIFICATION='';
            $PROPERTY_CURRENT_OCCUPANT='';
            $KITCHEN_DINER='';
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING='';
            $GARDEN='';
            $WHEELCHAIR_ACCESS='';
            $ELEVATOR_IN_BUILDING='';
            $POOL='';
            $GYM='';
            $KITCHEN='';
            $DINING_ROOM='';
            $FURNISHED='';
            $INTERNET='';
            $WIRELESS_INTERNET='';
            $TV='';
    
            $WASHER='';
            $DRYER='';
            $DISHWASHER='';
            $PETS_ALLOWED='';
            $FAMILY_OR_CHILD_FRIENDLY='';
            $DSS_ALLOWED='';
            $SMOKING_ALLOWED='';
            $SECURITY='';
            $HOT_TUB='';
    
            $CLEANER='';
            $EN_SUITE='';
            $SECURE_CAR_PARKING='';
            $OPEN_PLAN_LOUNGE='';
            $VIDEO_DOOR_ENTRY='';
            $CONCIERGE_SERVICES='';
            $PROPERTY_CUSTOM_FEATURES='';
            $PROPERTY_ROOMS='';
            $PROPERTY_ASSETS='';
    
            $PROPERTY_IMAGE_1='';
            $PROPERTY_IMAGE_2='';
            $PROPERTY_IMAGE_3='';
            $PROPERTY_IMAGE_4='';
            $PROPERTY_IMAGE_5='';
            $PROPERTY_IMAGE_6='';
            $PROPERTY_IMAGE_7='';
            $PROPERTY_IMAGE_8='';
            $PROPERTY_IMAGE_9='';
    
            $PROPERTY_IMAGE_10='';
            $PROPERTY_IMAGE_11='';
            $PROPERTY_IMAGE_12='';
            $PROPERTY_IMAGE_13='';
            $PROPERTY_IMAGE_14='';
            $PROPERTY_IMAGE_15='';
            $PROPERTY_IMAGE_FLOOR_1='';
            $PROPERTY_IMAGE_FLOOR_2='';
            $PROPERTY_IMAGE_FLOOR_3='';
            $PROPERTY_IMAGE_FLOOR_4='';
            $PROPERTY_IMAGE_FLOOR_5='';
            $PROPERTY_IMAGE_EPC_1='';
            $PROPERTY_IMAGE_EPC_2='';
            $PROPERTY_IMAGE_EPC_3='';
            $PROPERTY_IMAGE_EPC_4='';
            $PROPERTY_IMAGE_EPC_5='';
            $PROPERTY_EPC_VALUES='';
            $PROPERTY_CREATED_ON=date('Y-m-d H:i:s'); 
             
            $PROPERTY_ACCETABLE_PRICE ='';

            $floor_area = $objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            if($floor_area!=''){
                $PROPERTY_DESCRIPTION.='\n Floor: '.$floor_area;
            }
            $year_built = $objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            if($year_built!=''){
                $PROPERTY_DESCRIPTION.='\n Year Built: '.$year_built;
            }
            
            $PROPERTY_DESCRIPTION=addslashes(trim(htmlentities($PROPERTY_DESCRIPTION)));

          //  $PROPERTY_VENDOR_ID=  $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $PROPERTY_VENDOR_ID='';
            $PROPERTY_SOLICITOR_ID=0;
            $INSTRUCTED_DATE='NULL';

            $currentEnergyEfficiency = $objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue();
            $potentialEnergyEfficiency = $objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
            $currentEnvironmentImpact = $objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();
            $potentialEnvironmentImpact = $objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue();
            if($currentEnergyEfficiency!="" && $currentEnergyEfficiency!='0'){
            $PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$currentEnergyEfficiency, "energy_potential"=> $potentialEnergyEfficiency,
                  "co2_current"=>$currentEnvironmentImpact, "co2_potential"=> $potentialEnvironmentImpact));             
            }
            $PROPERTY_LETTING_SERVICE = '';           
            $PROPERTY_LETTING_FEE_TYPE=$PROPERTY_PRICE_FREQUENCY;
            $PROPERTY_LETTING_FEE_FREQUENCY ='';
            $PROPERTY_MANAGEMENT_FEE_TYPE='';
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY='';
            $PROPERTY_MANAGEMENT_FEE='';
            $CERTIFICATE_EXPIRE_DATE='';
            $PROPERTY_LETTING_FEE='';


            $extra_features = array();
            $extra_feature_1 = $objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();
            if($extra_feature_1 !="" && $extra_feature_1 !="False" ){
                $extra_feature_1_value ="Vacant : ".$extra_feature_1;
                array_push($extra_features,$extra_feature_1_value);
            }

            $extra_feature_2 = $objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
          
            if($extra_feature_2 !="" && $extra_feature_2 !="False" ){
                $extra_feature_2_value ="Building : ".$extra_feature_2;
                array_push($extra_features,$extra_feature_2_value);
            }
            

            if(!empty($extra_features)){
                $PROPERTY_CUSTOM_FEATURES=addslashes(json_encode($extra_features));
            }

            $PROPERTY_ROOMS='';

         


            $sql1 ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_VENDOR_SOLICITOR_ID`,`PROPERTY_TITLE`,
            `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
            `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
            `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
            `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
            `KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
            `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
            `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
            `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
            `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
            `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
            `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
            `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
            `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
            `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
            `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
             VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID','$PROPERTY_SOLICITOR_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
            '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
            '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
            '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
            '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
            '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
            '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
            '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
            '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
            '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
            '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
            '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
            '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
            '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
            '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
            '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
            '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)"; 

          // echo $sql1 .'<br/><br/>';
          $db_connect->queryExecute($sql1) or die($sql1);

         /*   if($row==100){
            break;
        }    */



        }
    }

}












?>