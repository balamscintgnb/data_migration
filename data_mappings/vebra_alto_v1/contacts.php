<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

// $query_CONT = "SELECT *FROM `tenants`";
//
// $data = json_decode($db_connect->queryFetch($query_CONT),true);
//
// $contacts_array = array();
// foreach($data['data'] as $row_c){
// $contacts_array[] = $row_c['ContactId'];
// }
//
// $contacts_array=array_unique($contacts_array);

$file_name1='../source_data/arbon/Applicant.xls';

$thisProceed=true;

try {
		//Load the excel(.xls/.xlsx) file
		$objPHPExcel = PHPExcel_IOFactory::load($file_name1);
} catch (Exception $e) {
		$thisProceed=false;
		die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows2 = $sheet->getHighestRow();

//print_r($sheet);exit;

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$contacts_array = array();

if($thisProceed){

		for($row2 =1; $row2 <= $total_rows2; $row2++){

				if($row2>1){
						$contacts_array[] = $objPHPExcel->getActiveSheet()->getCell('B'.$row2)->getValue();
				}
		}
}

$contacts_array=array_unique($contacts_array);

// echo '<pre><code>';
// print_r($contacts_array);
// echo '</code></pre>';
//
// exit;

$file_name='../source_data/arbon/Contact.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){


    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID=addslashes($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $CLIENT_TITLE='';
            $CLIENT_NAME='';
            $COMPANY_NAME='';

						$name1=trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
            $name2=trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());

            if($name2!='')
            $CLIENT_NAME=$name1.' & '.$name2;
            else
            $CLIENT_NAME=$name1;

						$CLIENT_TYPE			= 'APPLICANT';
						$CLIENT_SUB_TYPE		= '';

						$COMPANY_NAME=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();

						if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
							if($CLIENT_NAME!=''){
								$CLIENT_NAME.=' ';
							}
							$CLIENT_NAME.='('.$COMPANY_NAME.')';
						}

						$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';

						$phone_1 				= '';
						$phone_2 				= '';
						$phone_3 				= '';

						$CLIENT_ADDRESS_LINE_1 	= '';
						$CLIENT_ADDRESS_LINE_2	= '';
						$CLIENT_ADDRESS_CITY	= '';
						$CLIENT_ADDRESS_TOWN	= '';
						$CLIENT_ADDRESS_POSTCODE= '';

						$housename=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue()));
						$housenumber='';
						$street='';
						$locality='';
						$town='';
						$county='';
						$postcode='';

						if($housename!=''){
							$CLIENT_ADDRESS_LINE_1.=$housename;
						}

						if($housenumber!=''){
							if($CLIENT_ADDRESS_LINE_1!=""){
								$CLIENT_ADDRESS_LINE_1.=', ';
							}
							$CLIENT_ADDRESS_LINE_1.=$housenumber;
						}

						if($street!=''){
							$CLIENT_ADDRESS_LINE_2.=$street;
						}

						if($locality!=''){
							$CLIENT_ADDRESS_CITY.=$locality;
						}

						if($town!=''){
							$CLIENT_ADDRESS_TOWN.=$town;
						}

						if($county!=''){
							if($CLIENT_ADDRESS_TOWN!=""){
								$CLIENT_ADDRESS_TOWN.=', ';
							}
							$CLIENT_ADDRESS_TOWN.=$county;
						}

						if($postcode!=''){
							$CLIENT_ADDRESS_POSTCODE.=$postcode;
						}

						$STATUS=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();

						$CLIENT_STATUS='';

						if(strtolower($STATUS)=='true'){
							$CLIENT_STATUS='INACTIVE';
						}else{
							$CLIENT_STATUS='ACTIVE';
						}

						$query_client="SELECT *FROM `email_addresses` WHERE `LinkedId` LIKE '$CLIENTID'";

						$data = json_decode($db_connect->queryFetch($query_client),true);

						$CLIENT_PRIMARY_EMAIL=$data['data'][0]['Address'];

            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_PHONE='';
						$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
						$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
						$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
						$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
						$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=$phone_2;
            $CLIENT_PHONE_2=$phone_3;
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';

						$NOTES2=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();

            if($NOTES2!=''){
                $CLIENT_NOTES.='\n '.$NOTES2;
            }

						$NOTES3=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();

            if($NOTES3!=''){
                $CLIENT_NOTES.='\n '.$NOTES3;
            }

						$CLIENT_NAME=addslashes($CLIENT_NAME);

					  $CLIENT_NOTES=preg_replace( '/[^[:print:]]/', '',addslashes($CLIENT_NOTES));

						$CLIENT_NOTES=Utility::format_content($CLIENT_NOTES);


            /*echo '<pre><code>';
        print_r();
        echo '</code></pre>';*/

			if(in_array($CLIENTID,$contacts_array)){

			echo $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
				`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
				`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
				`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`) VALUES
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";

			  echo"<br/><br/>";
			//echo $row;
        $db_connect->queryExecute($sql) or die ($sql);

			}

        }
    }
}
?>
