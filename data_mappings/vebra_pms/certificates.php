<?php
include 'includes/db.php';
include 'source_db.php';


if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}

$query = $db->query("SELECT * FROM $dbname`certificates` WHERE 1 LIMIT $page, 250");
if($query->num_rows>0){
	$rows = $query->rows;

	
	foreach($rows as $row){
		$PROPERTY_ID				= $row['prop_id'];
		$PROPERTY_REF_ID			= '';
		$PROPERTY_VENDOR_ID			= '';
		$PROPERTY_STAFF_ID			= $staffs_array[$row['prop_manager']];
		$PROPERTY_TITLE				= '';
		$PROPERTY_SHORT_DESCRIPTION	= '';
		$PROPERTY_DESCRIPTION		= $db->escape($row['prop_brief_details']);
		
		$PROPERTY_CATEGORY			= '';
		$recordtype					= $row['recordtype'];
		$category					= $row['category'];
		
		$PROPERTY_CLASSIFICATION	= '';
		
		if($recordtype==433 && $category==41){
			$PROPERTY_CATEGORY		= 'COMMERCIAL LETTING';
		}
		if($recordtype==301){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
			$PROPERTY_CLASSIFICATION= 'New Build';
		}
		else if($recordtype==23 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
		}
		else if($recordtype==23 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL SALES';
		}
		else if($recordtype==24 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
		}
		else if($recordtype==24 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL LETTINGS';
		}
		else if($recordtype==26 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
		}
		else if($recordtype==26 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL AUCTION';
		}
		else if($recordtype==23 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
		}
		else if($recordtype==24 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
		}
		else if($recordtype==26 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
		}
		
		
		$PROPERTY_PRICE				= $row['prop_rent_price'];
		$PROPERTY_QUALIFIER			= '';
		
		$PROPERTY_PRICE_FREQUENCY	= '';
		
		if($row['prop_stmt_freq']=='0'){
			$PROPERTY_PRICE_FREQUENCY	= 'PCM';
		}
		else if($row['prop_stmt_freq']=='3'){
			$PROPERTY_PRICE_FREQUENCY	= 'PCM';
		}
		else if($row['prop_stmt_freq']=='5'){
			$PROPERTY_PRICE_FREQUENCY	= 'PQ';
		}
		
		
		$PROPERTY_AVAILABLE_DATE	= $row['prop_avail_date'];
		
		$PROPERTY_ADDRESS_LINE_1	= '';
		$PROPERTY_ADDRESS_LINE_2	= '';
		$PROPERTY_ADDRESS_CITY		= '';
		$PROPERTY_ADDRESS_COUNTY	= '';
		$PROPERTY_ADDRESS_POSTCODE	= '';
		$PROPERTY_FLOOR_AREA		= $row['prop_area2'];
		
		if($row['prop_suspended']==1){
			$PROPERTY_STATUS			= 'ARCHIVED';
		}
		else{
			$PROPERTY_STATUS			= 'ACTIVE';
		}
		
		$housename			= trim($row['prop_name']);
		$housenumber		= '';
		$street				= trim($row['prop_address1']);
		$locality			= '';
		$area				= trim($row['prop_address2']);
		$town				= '';
		$county				= trim($row['prop_address4']);
		$postcode			= trim($row['prop_postcode']);
		
		if($housename!=''){
			$PROPERTY_ADDRESS_LINE_1.=$housename;
		}
		
		if($housenumber!=''){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$housenumber;
		}
		
		if($street!=''){
			$PROPERTY_ADDRESS_LINE_2.=$street;
		}
		
		if($locality!=''){
			$PROPERTY_ADDRESS_CITY.=$locality;
		}
		
		if($area!=''){
			if($PROPERTY_ADDRESS_CITY!=""){
				$PROPERTY_ADDRESS_CITY.=', ';
			}
			$PROPERTY_ADDRESS_CITY.=$area;
		}
		
		if($town!=''){
			$PROPERTY_ADDRESS_COUNTY.=$town;
		}
		
		if($county!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$county;
		}
		
		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}
		
		$PROPERTY_ADDRESS_LINE_1		= mysql_real_escape_string($PROPERTY_ADDRESS_LINE_1);
		$PROPERTY_ADDRESS_LINE_2		= mysql_real_escape_string($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY			= mysql_real_escape_string($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY		= mysql_real_escape_string($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE		= mysql_real_escape_string($PROPERTY_ADDRESS_POSTCODE);
		
		$PROPERTY_FORMATTED_ADDRESS	= '';
		
		$PROPERTY_AVAILABILITY		= $status_array[$row['prop_status']];
		$PROPERTY_ADMIN_FEES		= $row[''];
		$PROPERTY_TYPE				= $item_array[$row['prop_ptype']];
		$PROPERTY_BEDROOMS			= $row['prop_numbeds'];
		$PROPERTY_BATHROOMS			= $row['prop_numBaths'];
		$PROPERTY_RECEPTION			= $row['prop_numReceps'];
		$PROPERTY_TENURE			= '';
		$PROPERTY_CURRENT_OCCUPANT	= '';
		
		$KITCHEN_DINER				= 'FALSE';
		$OFF_ROAD_PARKING			= 'FALSE';
		$ON_ROAD_PARKING			= 'FALSE';
		$GARDEN						= 'FALSE';
		$WHEELCHAIR_ACCESS			= 'FALSE';
		$ELEVATOR_IN_BUILDING		= 'FALSE';
		$POOL						= 'FALSE';
		$GYM						= 'FALSE';
		$KITCHEN					= 'FALSE';
		$DINING_ROOM				= 'FALSE';
		$FURNISHED					= 'FALSE';
		$INTERNET					= 'FALSE';
		$WIRELESS_INTERNET			= 'FALSE';
		$TV							= 'FALSE';
		$WASHER						= 'FALSE';
		$DRYER						= 'FALSE';
		$DISHWASHER					= 'FALSE';
		$PETS_ALLOWED				= 'FALSE';
		$FAMILY_OR_CHILD_FRIENDLY	= 'FALSE';
		$DSS_ALLOWED				= 'FALSE';
		$SMOKING_ALLOWED			= 'FALSE';
		$SECURITY					= 'FALSE';
		$HOT_TUB					= 'FALSE';
		$CLEANER					= 'FALSE';
		$EN_SUITE					= 'FALSE';
		$SECURE_CAR_PARKING			= 'FALSE';
		$OPEN_PLAN_LOUNGE			= 'FALSE';
		$VIDEO_DOOR_ENTRY			= 'FALSE';
		$CONCIERGE_SERVICES			= 'FALSE';
		
		$LETTING_SERVICE_TYPE		= $managementtype_array[$row['prop_management_type']];
		$LETTING_STATUS				= $status_array[$row['prop_letStatus']];
		
		$PROPERTY_ROOMS				= isset($rooms[$PROPERTY_ID])? $rooms[$PROPERTY_ID]:array();
		$PROPERTY_ROOMS				= $db->escape(json_encode($PROPERTY_ROOMS));
		
		$PROPERTY_CREATED_ON		= $row['prop_regn_date'];
		
		$sql = "INSERT INTO `properties`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_CREATED_ON', '0')";
		$db->query($sql);
	}
	echo $p;
?>
<script>
window.location="properties.php?p=<?= $p+1;?>";
</script>
<?php
}
?>