<?php
include 'includes/db.php';
include 'source_db.php';


if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}

function get_transaction_freq($LETTING_ID){
	global $db, $dbname;
	$trans_obj = $db->query("SELECT * FROM $dbname`transactions` WHERE `tran_ten_id` = '$LETTING_ID' AND `tran_description` = 'Rent' ;");
	if($trans_obj->num_rows>0){
		$transinfo = $trans_obj->rows;
		$days = ''; $l_end_date = ''; $l_start_date = ''; $rent = '';
		foreach($transinfo as $traninfo){
			if($traninfo['tran_for_start']!='' && $traninfo['tran_for_end']!='' && $days==''){
				$date1=date_create($traninfo['tran_for_start']);
				$date2=date_create($traninfo['tran_for_end']);
				$diff=date_diff($date1, $date2);
				$days = $diff->days;
				
			}
			if($traninfo['tran_for_end']!=''){
				$l_end_date = $traninfo['tran_for_end'];
			}
			
			if($l_start_date==''){
				$l_start_date = $traninfo['tran_for_start'];
				$rent = $traninfo['tran_amount2'];
			}
		}
		if($days!=''){
			return array('days'=>$days, 'amt'=>$rent, 'letting_end_date'=>$l_end_date, 'letting_start_date'=>$l_start_date);
		}
		else if($l_start_date!=''){
			return array('days'=>30, 'amt'=>$rent, 'letting_end_date'=>$l_end_date, 'letting_start_date'=>$l_start_date);
		}
		else{
			return false;
		}
	}
	else{
		return array('days'=>'NORENT', 'amt'=>0);
	}
}


$query = $db->query("SELECT * FROM $dbname`tenants` WHERE 1 LIMIT $page, 250");
if($query->num_rows>0){
	$rows = $query->rows;

	
	foreach($rows as $row){
		$LETTING_ID							= $row['ten_id'];
		$arr 	= get_transaction_freq($LETTING_ID);
		if($arr){
			$freq = $arr['days'];
			$amount = $arr['amt'];
			$lease_end_date = $arr['letting_end_date'];
			$lease_start_date = $arr['letting_start_date'];
			//echo 'days : '.$freq; echo '<br>';
			//var_dump($arr);
			$FREQUENCY = '';
			if($freq==30 || $freq==29 || $freq==28 || $freq==27 || $freq==26){
				$FREQUENCY = 'PCM';
				$freq = 'PCM';
			}
			else if($freq==364 || $freq==365){
				$FREQUENCY = 'PA';
			}
			else if($freq==180 || $freq==181 || $freq==182 || $freq==183){
				$FREQUENCY = 'P6M';
			}
			else if($freq==89 || $freq==90 || $freq==91){
				$FREQUENCY = 'PQ';
			}
			else if($freq=='NORENT'){
				$FREQUENCY = 'NORENT';
			}
			else{
				$FREQUENCY = $freq;
			}
			
			$fields = "";
			if(ceil($row['ten_rentAmount'])==0 && $amount>0){
				$fields = ", `LETTING_RENT` = '$amount'";
			}
			
			if(strtotime($row['ten_lease_start'])>strtotime($row['ten_lease_end']) && $lease_end_date!=''){
				$fields.= ", `LETTING_END_DATE` = '$lease_end_date'";
			}
			else if(($row['ten_lease_end']=='0000-00-00 00:00:00' || $row['ten_lease_end']=='') && $lease_end_date!=''){
				$fields.= ", `LETTING_END_DATE` = '$lease_end_date'";
			}
			
			if(($row['ten_lease_start']=='0000-00-00 00:00:00' || $row['ten_lease_start']=='') && $lease_start_date!=''){
				$fields.= ", `LETTING_START_DATE` = '$lease_start_date'";
			}
			
			$month_rent = ($row['ten_weeklyRentAmount']*52)/12;
			$month_rent = ceil($month_rent);
			$month_rent1 = $month_rent-1;
			$diff_month_rent = $month_rent - $row['ten_rentAmount'];
			if($row['ten_rentAmount']==$month_rent){
				$freq = 'PCM';
			}
			else if($row['ten_rentAmount']==$month_rent1){
				$freq = 'PCM';
			}
			else if($diff_month_rent<=1){
				$freq = 'PCM';
			}
			else if($row['ten_rentAmount']<$row['ten_weeklyRentAmount']){
				$freq = 'PCM';
			}
			else if($FREQUENCY==''){
				$freq = 'PCM';
			}
			
			if($row['ten_lease_start']!='0000-00-00 00:00:00' && $row['ten_lease_start']!='' && $FREQUENCY == 'NORENT'){
				$FREQUENCY = 'PCM';
			}
			
			
			if(is_numeric($FREQUENCY)){
				$FREQUENCY = 'PCM';
			}
			
			if($FREQUENCY == 'NORENT'){
				$FREQUENCY = '';
			}
			
			$sql = "UPDATE `lettings` SET `LETTING_RENT_FREQUENCY` = '$freq', `LETTING_RENT_PAYMENT_FREQUENCY` = '$FREQUENCY' $fields WHERE `LETTING_ID` = '$LETTING_ID'"; echo '<br>';
			$db->query($sql);
		}
		
	}
?>
<script>
window.location="lettings_updation.php?p=<?= $p+1;?>";
</script>
<?php
}
?>