<?php
include 'includes/db.php';
include 'source_db.php';


if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}

//tenleasetypes

$status_query = mysql_query("SELECT * FROM $dbname`tenleasetypes` WHERE 1;");
$status_array = array();
while($status = mysql_fetch_array($status_query)){
	$status_array[$status['lt_id']] = $status['lt_name'];
}

//ten_depositStatus

$depositstatus_query = mysql_query("SELECT * FROM $dbname`depositstatus` WHERE 1;");
$depositstatus_array = array();
while($depositstatus = mysql_fetch_array($depositstatus_query)){
	$depositstatus_array[$depositstatus['ds_id']] = $depositstatus['ds_type'];
}

$client_last_id_obj =  $db->query("SELECT MAX(CLIENTID) AS LAST FROM `clients` ");
$client_last_id = $client_last_id_obj->row['LAST'];

$query = $db->query("SELECT * FROM $dbname`tenants` WHERE 1 LIMIT $page, 250");
if($query->num_rows>0){
	$rows = $query->rows;

	
	foreach($rows as $row){
		$LETTING_ID							= $row['ten_id'];
		$LETTING_CUSTOM_REF_NO 				= '';
		$PROPERTY_ID						= $row['ten_prop_id'];
		$PROPERTY_ROOM_ID					= '';
		$TENANT_ID							= '';
		$SHARED_TENANT_IDS					= '';
		
		$LETTING_START_DATE					= $row['ten_lease_start'];
		$LETTING_END_DATE					= $row['ten_lease_end'];
		$LETTING_RENT						= $row['ten_rentAmount'];
		$LETTING_RENT_FREQUENCY				= '';
		$LETTING_RENT_PAYMENT_FREQUENCY		= '';
		$LETTING_RENT_PAYMENT_DAY			= '';
		$LETTING_RENT_ADVANCE				= '';
		$LETTING_RENT_ADVANCE_TYPE			= '';
		$LETTING_NOTICE_PERIOD				= $row['ten_review_freq_days_notice'];
		$LETTING_NOTICE_PERIOD_TYPE			= 'DAY';
		$LETTING_BREAK_CLAUSE				= '';
		$LETTING_BREAK_CLAUSE_TYPE			= '';
		$LETTING_DEPOSIT					= $row['ten_depositAmount'];
		$LETTING_DEPOSIT_DUE_DATE			= '';
		
		if($row['ten_depositStatus']==2){
			$help_by = 'LANDLORD';
		}
		else if($row['ten_depositStatus']==3){
			$help_by = 'AGENT';
		}
		else if($row['ten_depositStatus']==1){
			$help_by = 'PROTECTION SCHEME';
		}
		else if($row['ten_depositStatus']==0){
			$help_by = 'AGENT';
		}
		
		
		$LETTING_DEPOSIT_HELD_BY			= $help_by;
		$LETTING_DEPOSIT_PROTECTION_SCHEME	= '';
		$LETTING_SERVICE					= '';
		$LETTING_FEES						= '';
		$LETTING_FEE_TYPE					= '';
		$LETTING_FEE_FREQUENCY				= '';
		$LETTING_MANAGEMENT_FEES			= '';
		$LETTING_MANAGEMENT_FEE_TYPE		= '';
		$LETTING_MANAGEMENT_FEE_FREQUENCY	= '';
		$LETTING_ADMIN_FEES					= '';
		$LETTING_ADMIN_FEE_TYPE				= '';
		$LETTING_INVENTORY_FEES				= '';
		$LETTING_INVENTORY_FEE_TYPE			= '';
		$LETTING_RENT_GUARANTEE				= '';
		$LETTING_LANDLORD_PAYMENT			= '';
		$LETTING_VAT_PERCENTAGE				= '';
		$LETTING_NOTES						= json_encode(array($row['ten_hotnotes1'], $row['ten_hotnotes2']));
		$LETTING_TYPE						= $status_array[$row['ten_lease_type']];
		$LETTING_NOTES						= mysql_real_escape_string($LETTING_NOTES);
		$LETTING_DATETIME					= $row['ten_regn_date'];
		
		$client_last_id++;
		
		$CLIENTID							= $client_last_id;
		$CLIENT_TITLE			= $row['cont_title'];
		
		$CLIENT_TITLE			= $row['cont_title'];
		$CLIENT_NAME			= $row['ten_first_name'].' '.$row['ten_name'];
		$CLIENT_NAME			= mysql_real_escape_string($CLIENT_NAME);
	
		$CLIENT_STAFF_ID		= '';
	
		$CLIENT_TYPE			= 'TENANT';
		$CLIENT_SUB_TYPE		= '';
	
		
	
	
		$CLIENT_STATUS = 'ACTIVE';
		
		$CLIENT_PRIMARY_EMAIL	= '';
		$email1					= trim($row['ten_email']);
	
	
		$CLIENT_NOTES			= '';
		$CLIENT_PRIMARY_PHONE	= '';
	
		if(validate_email($email1)){
			$CLIENT_PRIMARY_EMAIL	= mysql_real_escape_string($email1);
		}
		else{
			$CLIENT_EMAIL_1			= $email1;
		}
		
	
		
		$CLIENT_EMAIL_2			= '';
		$CLIENT_EMAIL_3			= '';
		$CLIENT_EMAIL_4			= '';
		$CLIENT_EMAIL_5			= '';
	
		$CLIENT_ADDRESS_LINE_1 	= '';
		$CLIENT_ADDRESS_LINE_2	= '';
		$CLIENT_ADDRESS_CITY	= '';
		$CLIENT_ADDRESS_TOWN	= '';
		$CLIENT_ADDRESS_POSTCODE= '';
	
		$housename			= trim($row['cont_address1']);
		$housenumber		= '';
		$street				= trim($row['cont_address2']);
		$locality			= '';
		$area				= trim($row['cont_address3']);
		$town				= '';
		$county				= trim($row['cont_address4']);
		$postcode			= trim($row['cont_postcode']);
	
		if($housename!=''){
			$CLIENT_ADDRESS_LINE_1.=$housename;
		}
	
		if($housenumber!=''){
			if($CLIENT_ADDRESS_LINE_1!=""){
				$CLIENT_ADDRESS_LINE_1.=', ';
			}
			$CLIENT_ADDRESS_LINE_1.=$housenumber;
		}
	
		if($street!=''){
			$CLIENT_ADDRESS_LINE_2.=$street;
		}
	
		if($locality!=''){
			$CLIENT_ADDRESS_CITY.=$locality;
		}
	
		if($area!=''){
			if($CLIENT_ADDRESS_CITY!=""){
				$CLIENT_ADDRESS_CITY.=', ';
			}
			$CLIENT_ADDRESS_CITY.=$area;
		}
	
		if($town!=''){
			$CLIENT_ADDRESS_TOWN.=$town;
		}
	
		if($county!=''){
			if($CLIENT_ADDRESS_TOWN!=""){
				$CLIENT_ADDRESS_TOWN.=', ';
			}
			$CLIENT_ADDRESS_TOWN.=$county;
		}
	
		if($postcode!=''){
			$CLIENT_ADDRESS_POSTCODE.=$postcode;
		}
	
		$CLIENT_ADDRESS_LINE_1			= mysql_real_escape_string($CLIENT_ADDRESS_LINE_1);
		$CLIENT_ADDRESS_LINE_2			= mysql_real_escape_string($CLIENT_ADDRESS_LINE_2);
		$CLIENT_ADDRESS_CITY			= mysql_real_escape_string($CLIENT_ADDRESS_CITY);
		$CLIENT_ADDRESS_TOWN			= mysql_real_escape_string($CLIENT_ADDRESS_TOWN);
		$CLIENT_ADDRESS_POSTCODE		= mysql_real_escape_string($CLIENT_ADDRESS_POSTCODE);
		
		$phone_1= $row['ten_phone2']; $phone_2= $row['ten_phone3']; $phone_3= ''; $phone_4= ''; $phone_5= ''; 
		$mobile_1= $row['ten_mobile']; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; 
		$fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';
	
		
	
	
		$CLIENT_PHONE_1			= mysql_real_escape_string($phone_1);
		$CLIENT_PHONE_2			= mysql_real_escape_string($phone_2);
		$CLIENT_PHONE_3			= mysql_real_escape_string($phone_3);
		$CLIENT_PHONE_4			= mysql_real_escape_string($phone_4);
		$CLIENT_PHONE_5			= mysql_real_escape_string($phone_5);
	
		$CLIENT_MOBILE_1		= mysql_real_escape_string($mobile_1);
		$CLIENT_MOBILE_2		= $mobile_2;
		$CLIENT_MOBILE_3		= $mobile_3;
		$CLIENT_MOBILE_4		= $mobile_4;
		$CLIENT_MOBILE_5		= $mobile_5;
	
		$CLIENT_FAX_1			= mysql_real_escape_string($fax_1);
		$CLIENT_FAX_2			= $fax_2;
		$CLIENT_FAX_3			= $fax_3;
		$CLIENT_FAX_4			= $fax_4;
		$CLIENT_FAX_5			= $fax_5;
		
		if($CLIENT_PHONE_1!=""){
			$CLIENT_PRIMARY_PHONE = $CLIENT_PHONE_1;
		}
		else if($CLIENT_MOBILE_1!=""){
			$CLIENT_PRIMARY_PHONE = $CLIENT_MOBILE_1;
		}
	
		$CLIENT_NOTES.= mysql_real_escape_string($row['notes']);
		
		
		$sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON', '0')";
		$db->query($sql);
		
		
		$TENANT_ID							= $CLIENTID;
		
		$sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
    `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
    `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`, `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
    `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
    `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
    `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`, `LETTING_TYPE`, `LETTING_NOTES`, `LETTING_DATETIME`)
    VALUES ('$LETTING_ID', '$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID','$SHARED_TENANT_IDS','$LETTING_START_DATE',
    '$LETTING_END_DATE', '$LETTING_RENT', '$LETTING_RENT_FREQUENCY', '$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
    '$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE', '$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT',
    '$LETTING_DEPOSIT_DUE_DATE', '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE', '$LETTING_FEES', '$LETTING_FEE_TYPE',
    '$LETTING_FEE_FREQUENCY', '$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY', '$LETTING_ADMIN_FEES',
    '$LETTING_ADMIN_FEE_TYPE', '$LETTING_INVENTORY_FEES', '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE', '$LETTING_LANDLORD_PAYMENT', '$LETTING_VAT_PERCENTAGE', '$LETTING_TYPE', '$LETTING_NOTES', '$LETTING_DATETIME')";
		$db->query($sql);
	}
	echo $p;
?>
<script>
window.location="lettings.php?p=<?= $p+1;?>";
</script>
<?php
}
?>