<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/common/purplekey/Clients.xls';
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
	$L = $T = $S = $Tot_row = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


			// $tablename = 'clients';
			// $Query = "UPDATE `$tablename` SET `RECORD_UPLOADED` = 0";
			// ($db_connect->queryExecute($Query)) ? print("Success<br/><br/>TABLE NAME IS : ".$tablename) : die("ERROR");
			// exit();
			
			$LAN_Query1 = $LAN_locat1 = $LAN_ID = $TEN_Query1 = $TEN_locat1 = $TEN_ID = $CLIENT_TYPE = $SUP_Query1 = $SUP_locat1 = $SUP_ID = $CLIENTID = "";
			$CLIENT_EMAIL_1 = $CLIENT_EMAIL_2 = $CLIENT_EMAIL_3 = "";
			$CLIENT_NAME = "";

            //logic for data mapping.

            $CLIENTID = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $CLIENT_NAME = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
            $COMPANY_NAME = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

            if($CLIENT_NAME==""){
            	$CLIENT_NAME = trim(str_replace("  "," ",trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue())." ".trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue())." ".trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue())));
            }

			if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
				if($CLIENT_NAME!=''){
					$CLIENT_NAME.= ' ('.$COMPANY_NAME.')';
				} else {
					$CLIENT_NAME = $COMPANY_NAME;
				}
				// $CLIENT_NAME.='('.$COMPANY_NAME.')';
			}

			$CLIENT_NAME = addslashes(trim($CLIENT_NAME));

            $CLIENT_TYPE = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            if($CLIENTID =='')
            {
            	if(stristr($CLIENT_TYPE,'landlord'))
            	{
            		$L++;
            		$CLIENT_TYPE = 'LANDLORD';
					$LAN_Query1 = "SELECT count(*) as num_rows FROM `clients`";
		            $LAN_locat1 = json_decode($db_connect->queryFetch($LAN_Query1),true);
		            $LAN_ID = (isset($LAN_locat1['data'][0]['num_rows']))?$LAN_locat1['data'][0]['num_rows']+1 : 0;
		        	$CLIENTID = 'LAN30'.str_pad($LAN_ID, 5, "0", STR_PAD_LEFT);
            	}
            	else if(stristr($CLIENT_TYPE,'tenant'))
            	{
            		$T++;
            		$CLIENT_TYPE = 'TENANT';
            		$TEN_Query1 = "SELECT count(*) as num_rows FROM `clients`";
		            $TEN_locat1 = json_decode($db_connect->queryFetch($TEN_Query1),true);
		            $TEN_ID = (isset($TEN_locat1['data'][0]['num_rows']))?$TEN_locat1['data'][0]['num_rows']+1 : 0;
		        	$CLIENTID = 'TEN20'.str_pad($TEN_ID, 5, "0", STR_PAD_LEFT);
            	}
            	else if(stristr($CLIENT_TYPE,'suppli'))
            	{
            		$S++;
            		$CLIENT_TYPE = 'SUPPLIER';
            		$SUP_Query1 = "SELECT count(*) as num_rows FROM `clients`";
		            $SUP_locat1 = json_decode($db_connect->queryFetch($SUP_Query1),true);
		            $SUP_ID = (isset($SUP_locat1['data'][0]['num_rows']))?$SUP_locat1['data'][0]['num_rows']+1 : 0;
		        	$CLIENTID = 'SUP60'.str_pad($SUP_ID, 5, "0", STR_PAD_LEFT);
            	}
            } else {
            	if(stristr($CLIENT_TYPE,'landlord')){
            		$L++;
            	} else if(stristr($CLIENT_TYPE,'tenant')){
            		$T++;
            	} else if(stristr($CLIENT_TYPE,'suppli')){
            		$S++;
            	}
            }
			
            $CLIENT_PRIMARY_EMAIL = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
            	$CLIENT_PRIMARY_EMAIL = ($CLIENT_PRIMARY_EMAIL) ? "'".$CLIENT_PRIMARY_EMAIL."'" : "NULL";
            $CLIENT_MOBILE_1 = addslashes(str_replace("-"," ",trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue())));
            	$CLIENT_MOBILE_1 = ($CLIENT_MOBILE_1) ? "'".$CLIENT_MOBILE_1."'" : "NULL";
            $CLIENT_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
            	$CLIENT_ADDRESS_LINE_1 = ($CLIENT_ADDRESS_LINE_1) ? "'".$CLIENT_ADDRESS_LINE_1."'" : "NULL";
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()));
            	$CLIENT_ADDRESS_LINE_2 = ($CLIENT_ADDRESS_LINE_2) ? "'".$CLIENT_ADDRESS_LINE_2."'" : "NULL";
            $CLIENT_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue()));
            	$CLIENT_ADDRESS_CITY = ($CLIENT_ADDRESS_CITY) ? "'".$CLIENT_ADDRESS_CITY."'" : "NULL";
            $CLIENT_ADDRESS_TOWN = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
            	$CLIENT_ADDRESS_TOWN = ($CLIENT_ADDRESS_TOWN) ? "'".$CLIENT_ADDRESS_TOWN."'" : "NULL";
            $CLIENT_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue()));
            	$CLIENT_ADDRESS_POSTCODE = ($CLIENT_ADDRESS_POSTCODE) ? "'".$CLIENT_ADDRESS_POSTCODE."'" : "NULL";
            $CLIENT_PRIMARY_PHONE = str_replace("-"," ",trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));
            	$CLIENT_PRIMARY_PHONE = ($CLIENT_PRIMARY_PHONE) ? "'".$CLIENT_PRIMARY_PHONE."'" : "NULL";
            	
        	$Get_Email = explode(",",trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue()));
        	
        	$CLIENT_EMAIL_1 = trim(@$Get_Email[0]);
        		$CLIENT_EMAIL_1 = ($CLIENT_EMAIL_1) ? "'".$CLIENT_EMAIL_1."'" : "NULL";
        	$CLIENT_EMAIL_2 = trim(@$Get_Email[1]);
        		$CLIENT_EMAIL_2 = ($CLIENT_EMAIL_2) ? "'".$CLIENT_EMAIL_2."'" : "NULL";
        	$CLIENT_EMAIL_3 = trim(@$Get_Email[2]);
        		$CLIENT_EMAIL_3 = ($CLIENT_EMAIL_3) ? "'".$CLIENT_EMAIL_3."'" : "NULL";
			$CLIENT_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue());
			
			if(stristr($CLIENT_STATUS,'Inactive')){
				$CLIENT_STATUS = "inactive";
			} else{
				$CLIENT_STATUS = "";
			}

			////////////////////////////// ADD MORE FIELD ///////////////////////
			// echo "<pre>"; print_r($CLIENT_STATUS); echo "</pre>";
			//////////////////////////  END /////////////////////

            $Query1 = "SELECT `CLIENTID` FROM `clients` WHERE CLIENT_NAME = '$CLIENT_NAME' AND CLIENT_TYPE = '$CLIENT_TYPE' AND (CLIENT_PRIMARY_EMAIL = $CLIENT_PRIMARY_EMAIL OR CLIENT_MOBILE_1 = $CLIENT_MOBILE_1 OR CLIENT_PRIMARY_PHONE = $CLIENT_PRIMARY_PHONE)";
			$Clients_exists = json_decode($db_connect->queryFetch($Query1),true);

			if(isset($Clients_exists['data'][0]['CLIENTID'])){

			}else{

				$sql ="INSERT INTO `clients` (`CLIENTID`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`,
					`CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,`CLIENT_EMAIL_1`,`CLIENT_EMAIL_2`,`CLIENT_EMAIL_3`,`CLIENT_STATUS`) VALUES 
					('$CLIENTID', '$CLIENT_NAME', '$CLIENT_TYPE', $CLIENT_PRIMARY_EMAIL, $CLIENT_MOBILE_1, $CLIENT_PRIMARY_PHONE, $CLIENT_ADDRESS_LINE_1, $CLIENT_ADDRESS_LINE_2, $CLIENT_ADDRESS_CITY,
					$CLIENT_ADDRESS_TOWN, $CLIENT_ADDRESS_POSTCODE,$CLIENT_EMAIL_1,$CLIENT_EMAIL_2,$CLIENT_EMAIL_3,'$CLIENT_STATUS')";
				($db_connect->queryExecute($sql)) ? $Tot_row++ : die($sql);
			}
        }
    }
			$z = $row-2;
			echo "TOTAL ROW IS<b> : </b>".$z;
}
?>



<html>
	<style>
		table, th, td {
			border: 1px solid black;
			line-height:20px;
			font-weight:bold;
		}
		th, td {
            padding: 20px;
            background-color:none;
        }
        th{
        	background-color:#ededed;
        }
		body{
			padding:10px;
			line-height:50px;
		}
	</style>
	<body>
		<?php if($Tot_row>0){ echo "<br />SUCCESSFULY INSERTED <b>CLIENTS</b> COUNT IS : ".$Tot_row; ?>
		<div>
			<table>
				<tr>
					<th>LANDLORD</th>
					<th>TENANT</th>
					<th>SUPPLIER</th>
				</tr>
				<tr>
					<td> <?php echo $L ?> </td>
					<td> <?php echo $T ?> </td>
					<td> <?php echo $S ?> </td>
				</tr>
			</table>
		</div>
		<?php } ?>
	</body>
</html>





