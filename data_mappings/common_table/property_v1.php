<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/common/myton/properties.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();
$collect_room = array();
$PROPERTY_NOTES = array();

if($thisProceed){
	
	$update_not_row = $hmo_row = $update_row = $Tot_row = $lan_row =  0;
	
    for($row =1; $row <= $total_rows; $row++){
    	
        if($row>1){

			// $tablename = 'properties';
			// $Query = "UPDATE `$tablename` SET `RECORD_UPLOADED` = 0";
			// ($db_connect->queryExecute($Query)) ? print("Success<br/><br/>TABLE NAME IS : ".$tablename) : die("ERROR");
			// exit();

	        $PROPERTY_STAFF_ID = $PROPERTY_QUALIFIER = $PROPERTY_ADMIN_FEES = '';
	        $PROPERTY_LANDLORD_ID = 0;
	
	        $PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
	        $PROPERTY_TITLE = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
	        $PROPERTY_SHORT_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
	        $PROPERTY_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
	        $PROPERTY_CATEGORY = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
	        $room = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getformattedValue());
	        
	        if($room =='Room 1'){
	        	$PROPERTY_CATEGORY = 'HMO';
	        	$repeat_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
	        	if($repeat_ADDRESS_LINE_1 !=''){
					$PROPERTY_ADDRESS_LINE_1 = '';
				}
	        	$PROPERTY_ADDRESS_LINE_1.= $repeat_ADDRESS_LINE_1;
	        }elseif(stristr($room,'Room')){
	        	$PROPERTY_CATEGORY = '';
	        }elseif(stristr($room,'Office')){
	        	$PROPERTY_CATEGORY = 'COMMERCIAL LETTINGS';
	        	if($room !=''){
	        		$repeat_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
	        		if($repeat_ADDRESS_LINE_1 !=''){
						$address_printer = '';
					}
		        		$address_printer.=$repeat_ADDRESS_LINE_1;
		        		$PROPERTY_ADDRESS_LINE_1=$room.", ".$address_printer;
		        	}else{
		        		$PROPERTY_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
		        	}
	        }else{
	        	$PROPERTY_CATEGORY = 'RESIDENTIAL LETTINGS';
	        	
	        	if($room !=''){
	        		$repeat_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
	        		if($repeat_ADDRESS_LINE_1 !=''){
						$address_printer = '';
					}
		        		$address_printer.=$repeat_ADDRESS_LINE_1;
		        		$PROPERTY_ADDRESS_LINE_1=$room.", ".$address_printer;
		        		
		        	}else{
		        		$PROPERTY_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
		        	}
	        	}
	        	
			// echo "<pre>", ($room),"</pre>";
			// echo "<pre>"; print_r($room."  ==  ".$PROPERTY_CATEGORY); echo "</pre>";
		
	        $PROPERTY_PRICE = trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue());
	        $PROPERTY_PRICE_FREQUENCY = trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue());
	        
	    	if($PROPERTY_PRICE_FREQUENCY == 'Monthly'){
	    		$PROPERTY_PRICE_FREQUENCY = 'pcm';
	    	}
	    	
	        $PROPERTY_AVAILABLE_DATE= Utility::convert_tosqldate(trim($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue()),'d/m/Y');
	        if($PROPERTY_AVAILABLE_DATE){
	        	$PROPERTY_AVAILABLE_DATE = "'".$PROPERTY_AVAILABLE_DATE."'";
	        }
	        else{
	        	$PROPERTY_AVAILABLE_DATE = "NULL";
	        }
	        
	        // echo "<pre>", ($PROPERTY_AVAILABLE_DATE),"</pre>";
	        // $PROPERTY_ADDRESS_LINE_1=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
	        
	        $PROPERTY_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue()));
	        $PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue()));
	        $PROPERTY_ADDRESS_COUNTY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue()));
	        $PROPERTY_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue()));
	        $PROPERTY_FORMATTED_ADDRESS = '';
	
	  //      $Get_ADDRESS = explode(",",trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue()));
	  //      $Get_ADDRESS2 = count($Get_ADDRESS);
	
	  //      if($Get_ADDRESS2 ==3){
	  //      	$PROPERTY_ADDRESS_LINE_1 = $Get_ADDRESS[0];
	  //      	$PROPERTY_ADDRESS_CITY = $Get_ADDRESS[1];
	  //      	$PROPERTY_ADDRESS_POSTCODE = $Get_ADDRESS[2];
	  //      }else if($Get_ADDRESS2 ==4){
	  //      	$PROPERTY_ADDRESS_LINE_1 = $Get_ADDRESS[0];
	  //      	$PROPERTY_ADDRESS_LINE_2 = $Get_ADDRESS[1];
	  //      	$PROPERTY_ADDRESS_CITY = $Get_ADDRESS[2];
	  //      	$PROPERTY_ADDRESS_POSTCODE = $Get_ADDRESS[3];
	  //      }else if($Get_ADDRESS2 ==5){
	  //      	$PROPERTY_ADDRESS_LINE_1 = trim($Get_ADDRESS[0]).", ".trim($Get_ADDRESS[1]);
	  //      	$PROPERTY_ADDRESS_LINE_2 = $Get_ADDRESS[2];
	  //      	$PROPERTY_ADDRESS_CITY = $Get_ADDRESS[3];
	  //      	$PROPERTY_ADDRESS_POSTCODE = $Get_ADDRESS[4];
	  //      } else{
	  //      	$PROPERTY_ADDRESS_LINE_1 = $Get_ADDRESS[0];
	  //      }
	        
	  //      $PROPERTY_ADDRESS_LINE_1 = addslashes(trim($PROPERTY_ADDRESS_LINE_1));
			// $PROPERTY_ADDRESS_LINE_2 = addslashes(trim($PROPERTY_ADDRESS_LINE_2));
			// $PROPERTY_ADDRESS_CITY = addslashes(trim($PROPERTY_ADDRESS_CITY));
			// $PROPERTY_ADDRESS_COUNTY = addslashes(trim($PROPERTY_ADDRESS_COUNTY));
			// $PROPERTY_ADDRESS_POSTCODE = addslashes(trim($PROPERTY_ADDRESS_POSTCODE));
	
			
	        $PROPERTY_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
			$PROPERTY_AVAILABILITY = 'Withdrawn';

	    	
	        $PROPERTY_AVAILABILITY = trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
	        
	    	if($PROPERTY_AVAILABILITY == ''){
	    		$PROPERTY_AVAILABILITY = 'Withdrawn';
	    	}
	    	
	        $PROPERTY_TYPE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue()));
	        
	        if($PROPERTY_TYPE == 'Rooms'){
				$PROPERTY_TYPE = 'Room - House share';
	        }
	        else if($PROPERTY_TYPE=='Studio Flat'|| $PROPERTY_TYPE=='Studio'){
				$PROPERTY_TYPE='Studio';
			}
	        else if(stristr($PROPERTY_TYPE,'Ground Floor') || stristr($PROPERTY_TYPE,'Upper Floor') || stristr($PROPERTY_TYPE,'Flat')){
				$PROPERTY_TYPE = 'flat';
	        }
	        else if(stristr($PROPERTY_TYPE,'Commercial')){
				$PROPERTY_TYPE = 'Commercial';
	        }
			else if($PROPERTY_TYPE=='Flat Maisonette'){
				$PROPERTY_TYPE='Maisonette';
			}
			else if($PROPERTY_TYPE=='Flat Semi Detached'){
				$PROPERTY_TYPE='Semi Detached';
			}
			else if($PROPERTY_TYPE=='Bungalow Detached'){
				$PROPERTY_TYPE='detached bungalow';
			}
			else if(stristr($PROPERTY_TYPE,'House')){
				$PROPERTY_TYPE='house';
			}
			else if($PROPERTY_TYPE =='House Detached'){
				$PROPERTY_TYPE='detached house';
			}
			else if($PROPERTY_TYPE=='House End Terrace'){
				$PROPERTY_TYPE='End Terrace'; 
			}
			else if($PROPERTY_TYPE=='House Maisonette'){
				$PROPERTY_TYPE='Maisonette';
			}
			else if($PROPERTY_TYPE=='House Semi Detached'){
				$PROPERTY_TYPE='semi-detached house';
			}
			else if($PROPERTY_TYPE=='House Terraced'){
				$PROPERTY_TYPE='terraced';
			}
			else if(stristr($PROPERTY_TYPE,'bar')){
				$PROPERTY_TYPE='Pub Bar';
			}
			else if(stristr($PROPERTY_TYPE,'Hostel')){
				$PROPERTY_TYPE='Hotel';
			}
			else if(stristr($PROPERTY_TYPE,'shop') || stristr($PROPERTY_TYPE,'Pizza') || stristr($PROPERTY_TYPE,'Fruits') || stristr($PROPERTY_TYPE,'Restaurant') || stristr($PROPERTY_TYPE,'Parlour') || stristr($PROPERTY_TYPE,'Bakery') || stristr($PROPERTY_TYPE,'Café') || stristr($PROPERTY_TYPE,'Cafe') || stristr($PROPERTY_TYPE,'Store') || stristr($PROPERTY_TYPE,'Salon')){
				$PROPERTY_TYPE='Retail';
			}
			
			else{
				$PROPERTY_TYPE = $PROPERTY_DESCRIPTION;
				if($PROPERTY_TYPE == 'Rooms'){
					$PROPERTY_TYPE = 'Room - House share';
		        }
		        else if($PROPERTY_TYPE=='Studio Flat'|| $PROPERTY_TYPE=='Studio'){
					$PROPERTY_TYPE='Studio';
				}
		        else if(stristr($PROPERTY_TYPE,'Ground Floor') || stristr($PROPERTY_TYPE,'Upper Floor') || stristr($PROPERTY_TYPE,'Flat')){
					$PROPERTY_TYPE = 'flat';
		        }
		        else if(stristr($PROPERTY_TYPE,'Commercial')){
					$PROPERTY_TYPE = 'Commercial';
		        }
				else if($PROPERTY_TYPE=='Flat Maisonette'){
					$PROPERTY_TYPE='Maisonette';
				}
				else if($PROPERTY_TYPE=='Flat Semi Detached'){
					$PROPERTY_TYPE='Semi Detached';
				}
				else if($PROPERTY_TYPE=='Bungalow Detached'){
					$PROPERTY_TYPE='detached bungalow';
				}
				else if(stristr($PROPERTY_TYPE,'House')){
					$PROPERTY_TYPE='house';
				}
				else if($PROPERTY_TYPE =='House Detached'){
					$PROPERTY_TYPE='detached house';
				}
				else if($PROPERTY_TYPE=='House End Terrace'){
					$PROPERTY_TYPE='End Terrace'; 
				}
				else if($PROPERTY_TYPE=='House Maisonette'){
					$PROPERTY_TYPE='Maisonette';
				}
				else if($PROPERTY_TYPE=='House Semi Detached'){
					$PROPERTY_TYPE='semi-detached house';
				}
				else if($PROPERTY_TYPE=='House Terraced'){
					$PROPERTY_TYPE='terraced';
				}
				else if(stristr($PROPERTY_TYPE,'bar')){
					$PROPERTY_TYPE='Pub Bar';
				}
				else if(stristr($PROPERTY_TYPE,'Hostel')){
					$PROPERTY_TYPE='Hotel';
				}
				else if(stristr($PROPERTY_TYPE,'shop') || stristr($PROPERTY_TYPE,'Pizza') || stristr($PROPERTY_TYPE,'Fruits') || stristr($PROPERTY_TYPE,'Restaurant') || stristr($PROPERTY_TYPE,'Parlour') || stristr($PROPERTY_TYPE,'Bakery') || stristr($PROPERTY_TYPE,'Café') || stristr($PROPERTY_TYPE,'Cafe') || stristr($PROPERTY_TYPE,'Store') || stristr($PROPERTY_TYPE,'Salon')){
					$PROPERTY_TYPE='Retail';
				}
			}
			
			
	        
	        $PROPERTY_BEDROOMS = trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
	        $PROPERTY_BATHROOMS = trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
	        $PROPERTY_RECEPTION = trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
	        
	        $FURNISHED = trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
	        if($FURNISHED == 'Partly')
	        {
	        	$FURNISHED = 'Part Furnished';
	        }
	        
	        $KITCHEN = $PROPERTY_TENURE = $PROPERTY_CLASSIFICATION = $PROPERTY_CURRENT_OCCUPANT = $KITCHEN_DINER = $OFF_ROAD_PARKING = $ON_ROAD_PARKING = $GARDEN = $WHEELCHAIR_ACCESS = $ELEVATOR_IN_BUILDING = $POOL = $GYM = $DINING_ROOM='';
	        $INTERNET = $WIRELESS_INTERNET = $TV = $WASHER = $DRYER = $DISHWASHER = $PETS_ALLOWED = $FAMILY_OR_CHILD_FRIENDLY = $DSS_ALLOWED = $SMOKING_ALLOWED = $SECURITY = $HOT_TUB = '';
	        $CLEANER = $EN_SUITE = $SECURE_CAR_PARKING = $OPEN_PLAN_LOUNGE = $VIDEO_DOOR_ENTRY = $CONCIERGE_SERVICES = $PROPERTY_CUSTOM_FEATURES = $PROPERTY_ROOMS = $PROPERTY_ASSETS = '';
	        $PROPERTY_IMAGE_1 = $PROPERTY_IMAGE_2 = $PROPERTY_IMAGE_3 = $PROPERTY_IMAGE_4 = $PROPERTY_IMAGE_5 = $PROPERTY_IMAGE_6 = $PROPERTY_IMAGE_7 = $PROPERTY_IMAGE_8 = $PROPERTY_IMAGE_9 = '';
	        $PROPERTY_IMAGE_10 = $PROPERTY_IMAGE_11 = $PROPERTY_IMAGE_12 = $PROPERTY_IMAGE_13 = $PROPERTY_IMAGE_14 = $PROPERTY_IMAGE_15 = $PROPERTY_IMAGE_FLOOR_1 = $PROPERTY_IMAGE_FLOOR_2 = $PROPERTY_IMAGE_FLOOR_3 = $PROPERTY_IMAGE_FLOOR_4 = $PROPERTY_IMAGE_FLOOR_5 = $PROPERTY_IMAGE_EPC_1 = $PROPERTY_IMAGE_EPC_2 = $PROPERTY_IMAGE_EPC_3 = $PROPERTY_IMAGE_EPC_4 = $PROPERTY_IMAGE_EPC_5 = $PROPERTY_EPC_VALUES = $PROPERTY_CREATED_ON = '';
	
	        $CERTIFICATE_EXPIRE_DATE="NULL";
	        $PROPERTY_LETTING_SERVICE = '';
	
	        $get_let_service = trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
	            if($get_let_service == 'Let Only')
	                $PROPERTY_LETTING_SERVICE=2;
	            else if(stristr($get_let_service,'Managed'))
	                $PROPERTY_LETTING_SERVICE=1;
	            else if($get_let_service == 'Rent Collection Only')
	                $PROPERTY_LETTING_SERVICE=3;
	
	        /////////////////////////////ADD MORE FIELDS/////////////////////////
	
	        $PRO_FEATURES = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
	        if($PRO_FEATURES!=''){
	            $PROPERTY_CUSTOM_FEATURES="'".json_encode($PRO_FEATURES)."'";
	        } else{
	            $PROPERTY_CUSTOM_FEATURES='NULL';
	        }
	        $address_printer2 = trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
	        if($address_printer2 !=''){
	        	$address_printer3 = '';
	        }
	        $address_printer3.= $address_printer2;
	        
	        
	        ///////////////// CLIENT INSERTED ////////////////////////
			$CLIENT_ADDRESS_LINE_1 = $CLIENT_ADDRESS_LINE_2 = $CLIENT_ADDRESS_CITY = $CLIENT_ADDRESS_TOWN = $CLIENT_ADDRESS_POSTCODE = $CLIENT_PRIMARY_EMAIL = $CLIENT_EMAIL_1 = '';
	
			$CLIENT_TYPE = 'landlord';
			$CLIENT_NAME = $objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
			if($CLIENT_NAME!=''){
			$EMAIL = explode(';',$objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue());
			$CLIENT_PRIMARY_EMAIL = trim($EMAIL[0]);
			$CLIENT_EMAIL_1 = trim($EMAIL[1]);
			
			$CLIENT_MOBILE_1 = trim($objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue());
			$CLIENT_PRIMARY_PHONE = trim($objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue());
			$CLIENT_PHONE_1 = trim($objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue());
			$addres = explode(',',$objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue());
			$addres2 = count($addres);
			
			if($addres2 ==2)
			{
				$CLIENT_ADDRESS_LINE_1 = $addres[0];
				$CLIENT_ADDRESS_POSTCODE = $addres[1];
			} else if($addres2 ==3)
			{
				$CLIENT_ADDRESS_LINE_1 = $addres[0];
				$CLIENT_ADDRESS_CITY = $addres[1];
				$CLIENT_ADDRESS_POSTCODE = $addres[2];
			}else if($addres2 ==4)
			{
				$CLIENT_ADDRESS_LINE_1 = $addres[0];
				$CLIENT_ADDRESS_LINE_2 = $addres[1];
				$CLIENT_ADDRESS_CITY = $addres[2];
				$CLIENT_ADDRESS_POSTCODE = $addres[3];
			}else if($addres2 ==5)
			{
				$CLIENT_ADDRESS_LINE_1 = trim($addres[0]).", ".trim($addres[1]);
				$CLIENT_ADDRESS_LINE_2 = $addres[2];
				$CLIENT_ADDRESS_CITY = $addres[3];
				$CLIENT_ADDRESS_POSTCODE = $addres[4];
			}else if($addres2 ==6)
			{
				$CLIENT_ADDRESS_LINE_1 = trim($addres[0]).", ".trim($addres[1]);
				$CLIENT_ADDRESS_LINE_2 = $addres[2];
				$CLIENT_ADDRESS_CITY = $addres[3];
				$CLIENT_ADDRESS_TOWN = $addres[4];
				$CLIENT_ADDRESS_POSTCODE = $addres[5];
			}else if($addres2 ==8)
			{
				$CLIENT_ADDRESS_LINE_1 = trim($addres[0]).", ".trim($addres[1]).", ".trim($addres[2]).", ".trim($addres[3]).", ".trim($addres[4]);
				$CLIENT_ADDRESS_LINE_2 = $addres[5];
				$CLIENT_ADDRESS_CITY = $addres[6];
				$CLIENT_ADDRESS_POSTCODE = $addres[7];
			}
			else {
				$CLIENT_ADDRESS_LINE_1 = $addres[0];
			}
			
			$CLIENT_ADDRESS_LINE_1 = addslashes(trim($CLIENT_ADDRESS_LINE_1));
			$CLIENT_ADDRESS_LINE_2 = addslashes(trim($CLIENT_ADDRESS_LINE_2));
			$CLIENT_ADDRESS_CITY = addslashes(trim($CLIENT_ADDRESS_CITY));
			$CLIENT_ADDRESS_TOWN = addslashes(trim($CLIENT_ADDRESS_TOWN));
			$CLIENT_ADDRESS_POSTCODE = addslashes(trim($CLIENT_ADDRESS_POSTCODE));
	
	
	        $Query1 = "SELECT * FROM `clients` WHERE CLIENT_NAME = '$CLIENT_NAME' AND CLIENT_TYPE = '$CLIENT_TYPE' AND `CLIENT_ADDRESS_LINE_1` = '$CLIENT_ADDRESS_LINE_1' AND `CLIENT_ADDRESS_POSTCODE` = '$CLIENT_ADDRESS_POSTCODE' AND (`CLIENT_PRIMARY_EMAIL` = '$CLIENT_PRIMARY_EMAIL' OR `CLIENT_MOBILE_1` = '$CLIENT_MOBILE_1')";
			$Clients_exists = json_decode($db_connect->queryFetch($Query1),true);
	
				if(isset($Clients_exists['data'][0]['CLIENTID'])){
				    $PROPERTY_LANDLORD_ID = trim($Clients_exists['data'][0]['CLIENTID']);
				}
				else{
	
					$Find_Query2 = "SELECT count(*) as num_rows FROM `clients`";
		            $Find_locat2 = json_decode($db_connect->queryFetch($Find_Query2),true);
		            $VIEWING2 = (isset($Find_locat2['data'][0]['num_rows']))?$Find_locat2['data'][0]['num_rows']+1: 0;
		        	$cnt_id=str_pad($VIEWING2, 5, "0", STR_PAD_LEFT);
		            $PROPERTY_LANDLORD_ID = 'LAN30'.$cnt_id;
	
					$Query1 = "INSERT INTO `clients` (`CLIENTID`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,`CLIENT_EMAIL_1`, `CLIENT_PHONE_1`) VALUES 
					('$PROPERTY_LANDLORD_ID', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_MOBILE_1', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_PHONE_1')";
					// ($db_connect->queryExecute($Query1)) ? $lan_row++ : die($Query1);
				}
			}
			////////////////////// END /////////////////
	
	        ///////////////// PROPERTY INSERTED ////////////////////////
	
	        // $Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ADDRESS_LINE_1 = '$PROPERTY_ADDRESS_LINE_1' AND `PROPERTY_ADDRESS_LINE_2` = '$PROPERTY_ADDRESS_LINE_2' AND `PROPERTY_ADDRESS_POSTCODE` = '$PROPERTY_ADDRESS_POSTCODE' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
	        $Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ADDRESS_LINE_1 = '$PROPERTY_ADDRESS_LINE_1'";
			// $Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ID = '$PROPERTY_ID'";
			$property_exists = json_decode($db_connect->queryFetch($Query1),true);
			if(isset($property_exists['data'][0]['PROPERTY_ID'])){
				$PROPERTY_ID = $property_exists['data'][0]['PROPERTY_ID'];
					// // $Query2 = "SELECT DISTINCT `PROPERTY_TYPE` FROM `properties`";
					// // $property_exists2 = json_decode($db_connect->queryFetch($Query2),true);
					// // $property_exists3 = count($property_exists2['data']);
	
					// // for($no=0; $no<= $property_exists3; $no++){
					// // 	$pro_typ = $property_exists2['data'][$no]['PROPERTY_TYPE'];
					// // 	echo$pro_id = $property_exists2['data'][$no]['PROPERTY_ID'];
					// // 	if($pro_typ ==''){
					// // 	echo "<pre>"; print_r($pro_id); echo "</pre>";
					// // 	}
					// // 	// echo "<pre>"; print_r($pro_typ); echo "</pre>";
					// // }
					// // exit();
			}
			else{
				if($PROPERTY_ID ==''){
	
					$Find_Query = "SELECT count(*) as num_rows FROM `properties`";
		            $Find_locat = json_decode($db_connect->queryFetch($Find_Query),true);
		            $VIEWING = (isset($Find_locat['data'][0]['num_rows']))?$Find_locat['data'][0]['num_rows']+1: 0;
		        	$prp_id=str_pad($VIEWING, 5, "0", STR_PAD_LEFT);
		            $PROPERTY_ID = 'PRP500'.$prp_id;
				}
	
				$sql = "INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_SOURCE`, `PROPERTY_VENDOR_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `LEASE_TERM_YEARS`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_NOTES`, `CERTIFICATE_EXPIRE_DATE`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `PROPERTY_VENDOR_SOLICITOR_ID`, `PROPERTY_BUYER_SOLICITOR_ID`, `PROPERTY_BUYER_ID`, `PROPERTY_LETTING_SERVICE`, `PROPERTY_LETTING_FEE`, `PROPERTY_LETTING_FEE_TYPE`, `PROPERTY_LETTING_FEE_FREQUENCY`, `PROPERTY_MANAGEMENT_FEE`, `PROPERTY_MANAGEMENT_FEE_TYPE`, `PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `INSTRUCTED_DATE`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,`PROPERTY_AVAILABLE_DATE`) VALUES 
				('$PROPERTY_ID', '$PROPERTY_ID', 'MANUAL', '$PROPERTY_LANDLORD_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', NULL, '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', NULL, NULL, NULL, '$PROPERTY_EPC_VALUES', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL,'','',$PROPERTY_AVAILABLE_DATE) ";
				// ($db_connect->queryExecute($sql)) ? $Tot_row++ : die($sql);
        	}

			if(stristr($room,'room') && $address_printer3 !=''){
				$replace_room = trim(str_replace('Room','',$room));
        		$rand_no2 = str_pad($replace_room,3,'0',STR_PAD_LEFT);
				$rand_no = $PROPERTY_ID.$rand_no2;
				$collect_room[$address_printer3][] = array('PROPERTY_ROOM_ID'=>$rand_no,'room_title'=>$room);
	        	// echo "<pre>"; print_r($collect_room); echo "</pre>";
			}
			
			if(trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getformattedValue())){
				if(stristr($room,'Room')){
					$PROPERTY_NOTES[$PROPERTY_ID][] = trim($room." - National Insurance No : ".trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getformattedValue()));
				}else{
					$PROPERTY_NOTES[$PROPERTY_ID][] = trim("National Insurance No - ".trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getformattedValue()));
				}
				if(trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getformattedValue())){
					$PROPERTY_NOTES[$PROPERTY_ID][] = trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getformattedValue());
				}
				
			} else if(trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getformattedValue())){
				$PROPERTY_NOTES[$PROPERTY_ID][] = trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getformattedValue());
			}
			// echo "<pre>"; print_r($PROPERTY_NOTES); echo "<pre/>";
			        
		}
	}
	
	$z = $row-2;
	echo "TOTAL ROW IS ".$z."<br><br>";
	if($Tot_row>0){
        echo "<br />SUCCESSFULY INSERTED <b>PROPERTIES</b> COUNT IS : ".$Tot_row;
    }
	if($lan_row>0){
        echo "<br />SUCCESSFULY INSERTED <b>LANDLORD</b> COUNT IS : ".$lan_row;
    }


	foreach($collect_room as $address_printer3 => $room_values){
		$PROPERTY_HMO_ROOMS = json_encode($room_values);
        $Query3 = "UPDATE `properties` SET `PROPERTY_HMO_ROOMS` = '$PROPERTY_HMO_ROOMS' WHERE `PROPERTY_ADDRESS_LINE_1` = '$address_printer3' AND `PROPERTY_CATEGORY` = 'HMO'";
        // ($db_connect->queryExecute($Query3)) ? $update_row++ : die($Query3);
        // echo "<pre>"; print_r($Query3); echo "<pre/>";
	}
	
	
	foreach($PROPERTY_NOTES as $PROPERTY_ID => $value_NOTES){
		// $PROPERTY_NOTES = ['data'];
		// $PROPERTY_NOTES2 = addslashes(utf8_decode([$value_NOTES)]);
		
		$PROPERTY_NOTES = addslashes(json_encode($value_NOTES));
		
		$Query4 = "UPDATE `properties` SET `PROPERTY_NOTES` = '$PROPERTY_NOTES' WHERE `PROPERTY_ID` = '$PROPERTY_ID'";
        ($db_connect->queryExecute($Query4)) ? $update_not_row++ : die($Query4);
        // echo "<pre>"; print_r($Query4); echo "<pre/>";
	}
	if($update_row>0){
        echo "<br />SUCCESSFULY UPDATED <b>PROPERTY HMO ROOMS</b> COUNT IS : ".$update_row;
    }
    if($update_not_row>0){
        echo "<br />SUCCESSFULY UPDATED <b>PROPERTY NOTES</b> COUNT IS : ".$update_not_row;
    }
}
?>