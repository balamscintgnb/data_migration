<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/common/myton/lettings.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$up_row = $Tot_row = $ten_row = $tenancy_no = $number = 0;
    for($row =119; $row <= 119; $row++){
        if($row>1){

	        $LETTING_RENT_PAYMENT_DAY = $LETTING_RENT_ADVANCE = $LETTING_RENT_ADVANCE_TYPE = $LETTING_NOTICE_PERIOD = $LETTING_NOTICE_PERIOD_TYPE = '';
	        $LETTING_SERVICE = $LETTING_INVENTORY_FEE_TYPE = $LETTING_RENT_GUARANTEE = $LETTING_VAT_PERCENTAGE = $LETTING_NOTES = '';
	        $LETTING_NOTES_array = $TENANT_ID = $LETTING_START_DATE = $LETTING_END_DATE = $LETTING_ADMIN_FEE_TYPE= $SHARED_TENANT_IDS = '';
	        $PROPERTY_ADDRESS_LINE_1 = $PROPERTY_ADDRESS_LINE_2 = $PROPERTY_ADDRESS_CITY = $PROPERTY_ADDRESS_COUNTY = $PROPERTY_ADDRESS_POSTCODE = '';

			// $tablename = 'lettings';
			// $Query = "UPDATE `$tablename` SET `RECORD_UPLOADED` = 0";
			// ($db_connect->queryExecute($Query)) ? print("Success<br/><br/>TABLE NAME IS : ".$tablename) : die("ERROR");
			// exit();

	        $PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
	        if($PROPERTY_ID ==''){
	        	$address = explode(",",trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue()));
	        	
	        	$address2 = count($address);
	        	if($address2 ==1){
	        		$PROPERTY_ADDRESS_LINE_1 = trim($address[0]);
	        	}else if($address2 ==2){
	        		$PROPERTY_ADDRESS_LINE_1 = trim($address[0]).", ".trim($address[1]);
	        	}else if($address2 ==3){
	        		$PROPERTY_ADDRESS_LINE_1 = trim($address[0]);
	        		$PROPERTY_ADDRESS_CITY = trim($address[1]);
	        		$PROPERTY_ADDRESS_POSTCODE = trim($address[2]);
	        	}else if($address2 ==4){
	        		$PROPERTY_ADDRESS_LINE_1 = trim($address[0]);
	        		$PROPERTY_ADDRESS_LINE_2 = trim($address[1]);
	        		$PROPERTY_ADDRESS_CITY = trim($address[2]);
	        		$PROPERTY_ADDRESS_POSTCODE = trim($address[3]);
	        	}else if($address2 ==5){
	        		$PROPERTY_ADDRESS_LINE_1 = trim($address[0]).", ".trim($address[1]);
	        		$PROPERTY_ADDRESS_LINE_2 = trim($address[2]);
	        		$PROPERTY_ADDRESS_CITY = trim($address[3]);
	        		$PROPERTY_ADDRESS_POSTCODE = trim($address[4]);
	        	}
	        	$remove_room = trim($objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue());
	        	if(stristr($remove_room,'Room')){
	        		$PROPERTY_ADDRESS_LINE_1 = str_replace("$remove_room","",$PROPERTY_ADDRESS_LINE_1);
	        	}else{
	        		$PROPERTY_ADDRESS_LINE_1 = str_replace("$remove_room","$remove_room,",$PROPERTY_ADDRESS_LINE_1);
	        	}

	        	$PROPERTY_ADDRESS_LINE_1 = addslashes(trim($PROPERTY_ADDRESS_LINE_1));
				$PROPERTY_ADDRESS_LINE_2 = addslashes(trim($PROPERTY_ADDRESS_LINE_2));
				$PROPERTY_ADDRESS_CITY = addslashes(trim($PROPERTY_ADDRESS_CITY));
				$PROPERTY_ADDRESS_COUNTY = addslashes(trim($PROPERTY_ADDRESS_COUNTY));
				$PROPERTY_ADDRESS_POSTCODE = addslashes(trim($PROPERTY_ADDRESS_POSTCODE));
				
				// $Query4 = "SELECT * FROM `properties` WHERE PROPERTY_ADDRESS_LINE_1 = '$PROPERTY_ADDRESS_LINE_1' AND `PROPERTY_ADDRESS_LINE_2` = '$PROPERTY_ADDRESS_LINE_2' AND `PROPERTY_ADDRESS_POSTCODE` = '$PROPERTY_ADDRESS_POSTCODE'";
				$Query4 = "SELECT * FROM `properties` WHERE PROPERTY_ADDRESS_LINE_1 = '$PROPERTY_ADDRESS_LINE_1'";
				$find_property = json_decode($db_connect->queryFetch($Query4),true);
				if(isset($find_property['data'][0]['PROPERTY_ID'])){
					$PROPERTY_ID = $find_property['data'][0]['PROPERTY_ID'];
					$PROPERTY_CATEGORY = $find_property['data'][0]['PROPERTY_CATEGORY'];
					if(stristr($remove_room,'Room')){
					$replace_room = trim(str_replace('Room','',$remove_room));
					$rand_no2 = str_pad($replace_room,3,'0',STR_PAD_LEFT);
					$PROPERTY_ROOM_ID = $PROPERTY_ID.$rand_no2;
					}else{
						$PROPERTY_ROOM_ID = '';
					}
				}
				else{
					$PROPERTY_ID = 0;
					$PROPERTY_ROOM_ID='';
				}
        	}
		// echo "<pre>"; print_r($rand_no); echo "</pre>";

	        $given_date1 = Utility::convert_tosqldate(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getformattedValue()), 'd/m/Y');

	            if($given_date1!=''){
	            	$LETTING_START_DATE = "'".$given_date1."'";
	            } else{
	                $LETTING_START_DATE = "NULL";
	            }

	        $given_date2=Utility::convert_tosqldate(trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getformattedValue()),'d/m/Y');

	            if($given_date2!=''){
	            $LETTING_END_DATE="'".$given_date2."'";
	            } else{
	                $LETTING_END_DATE="NULL";
	            }

	        $LETTING_RENT2 = explode(' ',addslashes(trim(str_replace("k","000",str_replace("K","000",str_replace(",","",str_replace("£","",$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue())))))));
			$LETTING_RENT = trim(@$LETTING_RENT2[0]);
			$frequency = str_replace(".","",trim(@$LETTING_RENT2[1]));

			if($frequency !=''){
				$RENT_FREQUENCY = $frequency;
			}
			// echo "<pre>"; print_r($frequency); echo "</pre>";
	
			$LETTING_RENT = "";
	        if($LETTING_RENT !=''){
	        	$LETTING_RENT = "'".$LETTING_RENT."'";
	        }else{
	        	$LETTING_RENT = "NULL";
	        }
	        $LETTING_RENT_FREQUENCY='';
	        $RENT_FREQUENCY = trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
	        	if($RENT_FREQUENCY == 'Monthly')
	        	{
	        		$RENT_FREQUENCY = 'pcm';
	        	}

	        $LETTING_RENT_PAYMENT_FREQUENCY = trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
	        $LETTING_BREAK_CLAUSE = trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
	        $LETTING_BREAK_CLAUSE = ($LETTING_BREAK_CLAUSE) ? "'".$LETTING_BREAK_CLAUSE."'" : "NULL";
	        $LETTING_BREAK_CLAUSE_TYPE = trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
	        $LETTING_BREAK_CLAUSE_TYPE = ($LETTING_BREAK_CLAUSE_TYPE) ? "'".$LETTING_BREAK_CLAUSE_TYPE."'" : "NULL";

			$LETTING_DEPOSIT = str_replace("£","",trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
			if($LETTING_DEPOSIT == 'Nil'){
				$LETTING_DEPOSIT = "NULL";
			}else if($LETTING_DEPOSIT ==''){
				$LETTING_DEPOSIT = "NULL";
			}else{
				$LETTING_DEPOSIT = "'".$LETTING_DEPOSIT."'";
			}
	        $LETTING_DEPOSIT_HELD_BY = trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
	        $LETTING_DEPOSIT_PROTECTION_SCHEME = trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue());

	        	if($LETTING_DEPOSIT_HELD_BY == 'TDP')
	        	{
	        		$LETTING_DEPOSIT_HELD_BY = 2;
	        		$LETTING_DEPOSIT_PROTECTION_SCHEME = 3;
	        	}
	        	else if($LETTING_DEPOSIT_HELD_BY == 'L/L')
	        	{
	        		$LETTING_DEPOSIT_HELD_BY = 2;
	        		$LETTING_DEPOSIT_PROTECTION_SCHEME = '';
	        	}
        	$LETTING_DEPOSIT_HELD_BY = ($LETTING_DEPOSIT_HELD_BY) ? "'".$LETTING_DEPOSIT_HELD_BY."'" : "NULL";
        	$LETTING_DEPOSIT_PROTECTION_SCHEME = ($LETTING_DEPOSIT_PROTECTION_SCHEME) ? "'".$LETTING_DEPOSIT_PROTECTION_SCHEME."'" : "NULL";

	        $LETTING_FEES = trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue());
        	if($LETTING_FEES !=''){
        		$LETTING_FEES ="'".$LETTING_FEES."'";
        	}else{
        		$LETTING_FEES = 'NULL';
        	}

            if($LETTING_RENT=='0' || $LETTING_RENT==''){
                $LETTING_RENT_FREQUENCY='';
                $LETTING_FEE_FREQUENCY='';
                $LETTING_MANAGEMENT_FEE_FREQUENCY='';
                $LETTING_FEE_TYPE='';
                $LETTING_MANAGEMENT_FEE_TYPE='';
            } else{
                $LETTING_RENT_FREQUENCY=$RENT_FREQUENCY;
                $LETTING_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
                $LETTING_MANAGEMENT_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
                $LETTING_MANAGEMENT_FEE_TYPE='1';
                $LETTING_FEE_TYPE='1';
            }

	        $LETTING_MANAGEMENT_FEES = trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
	        $LETTING_ADMIN_FEES = trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
	        $LETTING_INVENTORY_FEES = trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
	        
			$LETTING_RENT_FREQUENCY = ($LETTING_RENT_FREQUENCY) ? "'".$LETTING_RENT_FREQUENCY."'" : "NULL";
			$LETTING_FEE_FREQUENCY = ($LETTING_FEE_FREQUENCY) ? "'".$LETTING_FEE_FREQUENCY."'" : "NULL";
			$LETTING_MANAGEMENT_FEE_FREQUENCY = ($LETTING_MANAGEMENT_FEE_FREQUENCY) ? "'".$LETTING_MANAGEMENT_FEE_FREQUENCY."'" : "NULL";
			$LETTING_MANAGEMENT_FEE_TYPE = ($LETTING_MANAGEMENT_FEE_TYPE) ? "'".$LETTING_MANAGEMENT_FEE_TYPE."'" : "NULL";
			$LETTING_FEE_TYPE = ($LETTING_FEE_TYPE) ? "'".$LETTING_FEE_TYPE."'" : "NULL";
			$LETTING_MANAGEMENT_FEES = ($LETTING_MANAGEMENT_FEES) ? "'".$LETTING_MANAGEMENT_FEES."'" : "NULL";
			$LETTING_ADMIN_FEES = ($LETTING_ADMIN_FEES) ? "'".$LETTING_ADMIN_FEES."'" : "NULL";
			$LETTING_INVENTORY_FEES = ($LETTING_INVENTORY_FEES) ? "'".$LETTING_INVENTORY_FEES."'" : "NULL";
			$LETTING_NOTES2 = trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
			
			if($LETTING_NOTES2){
				$LETTING_NOTES_array[]= trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
				$LETTING_NOTES = addslashes(json_encode($LETTING_NOTES_array));
			}

	        /////////////////////////////ADD MORE FIELDS/////////////////////////

	        $get_let_service = trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue());
	            if($get_let_service == 'Let Only')
	                $LETTING_SERVICE=2;
	            if(stristr($get_let_service,'Managed'))
	                $LETTING_SERVICE=1;
	            if($get_let_service == 'Rent Collection Only')
	                $LETTING_SERVICE=3;
			$LETTING_SERVICE = ($LETTING_SERVICE) ? "'".$LETTING_SERVICE."'" : "NULL";

	            if($LETTING_RENT_FREQUENCY == 'pcm')
	                $LETTING_RENT_PAYMENT_FREQUENCY='monthly';
	            if($LETTING_RENT_FREQUENCY == 'pw')
	                $LETTING_RENT_PAYMENT_FREQUENCY='weekly';
	            if($LETTING_RENT_FREQUENCY == 'p4w')
	                $LETTING_RENT_PAYMENT_FREQUENCY='four_weekly';

			$LETTING_RENT_PAYMENT_FREQUENCY = ($LETTING_RENT_PAYMENT_FREQUENCY) ? "'".$LETTING_RENT_PAYMENT_FREQUENCY."'" : "NULL";

        	$Mob1 = $Mob2 = $Mob3 = $Mob4 = '';
        	$Email1 = $Email2 = $Email3 = $Email4 = '';
            ///////////////////////////// CLIENT INSERT //////////////////////////
            $CLIENT_TYPE = 'tenant';
        	$TENANT_NAME = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
	        $TENANT_MOB = explode('/',trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
	        $Mob1 = trim(@$TENANT_MOB[0]);
	        $Mob2 = trim(@$TENANT_MOB[1]);
	        $Mob3 = trim(@$TENANT_MOB[2]);
	        $Mob4 = trim(@$TENANT_MOB[3]);
	        // $Mob1 = str_replace("-"," ",$Mob1);
	        // $Mob2 = str_replace("-"," ",$Mob2);
	        $TENANT_EMAIL = explode('/',trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));
	        $Email1 = trim(@$TENANT_EMAIL[0]);
	        $Email2 = trim(@$TENANT_EMAIL[1]);
	        $Email3 = trim(@$TENANT_EMAIL[2]);
	        $Email4 = trim(@$TENANT_EMAIL[3]);
	        $TENANT_ADD = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));


			if($TENANT_NAME){
    		$Query1 = "SELECT * FROM `clients` WHERE CLIENT_NAME = '$TENANT_NAME' AND CLIENT_TYPE = '$CLIENT_TYPE' AND (CLIENT_PRIMARY_EMAIL = '$Email1' OR CLIENT_MOBILE_1 = '$Mob1' OR CLIENT_PRIMARY_PHONE = '$Mob2')";
			$Clients_exists = json_decode($db_connect->queryFetch($Query1),true);

			if(isset($Clients_exists['data'][0]['CLIENTID'])){
			    $TENANT_ID = $Clients_exists['data'][0]['CLIENTID'];
			}else{

				$Find_Query2 = "SELECT count(*) as num_rows FROM `clients`";
	            $Find_locat2 = json_decode($db_connect->queryFetch($Find_Query2),true);
	            $VIEWING2 = (isset($Find_locat2['data'][0]['num_rows']))?$Find_locat2['data'][0]['num_rows']+1: 0;
	        	$ten_id=str_pad($VIEWING2, 5, "0", STR_PAD_LEFT);
	            $TENANT_ID = 'TEN40'.$ten_id;

				$Query2 ="INSERT INTO `clients` (`CLIENTID`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_MOBILE_1`, `CLIENT_PRIMARY_PHONE`,`CLIENT_EMAIL_1`,`CLIENT_EMAIL_2`,`CLIENT_EMAIL_3`,`CLIENT_PHONE_1`,`CLIENT_PHONE_2`) VALUES 
				('$TENANT_ID', '$TENANT_NAME', '$CLIENT_TYPE', '$Email1', '$Mob1', '$Mob2', '$Email2','$Email3','$Email4','$Mob3','$Mob4')";
				($db_connect->queryExecute($Query2)) ? $ten_row++ : die($Query2);
			}
			}else{
				$TENANT_ID = 0;
			}
		// echo "<pre>"; print_r($PROPERTY_ID); echo "</pre>";

        /////////////////////////// END ////////////////////////
	        
	        
	        $Find_Query = "SELECT count(*) as num_rows FROM `lettings`";
            $Find_locat = json_decode($db_connect->queryFetch($Find_Query),true);
            $tenancy_id = (isset($Find_locat['data'][0]['num_rows']))?$Find_locat['data'][0]['num_rows']+1: 0;
        	$LETTING_CUSTOM_REF_NO = "TENANCY".str_pad($tenancy_id, 5, "0", STR_PAD_LEFT);
        	$LETTING_ID = $LETTING_CUSTOM_REF_NO;


			$sql ="INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `TENANT_ID`, `SHARED_TENANT_IDS`, `LETTING_START_DATE`, `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`,
				`LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_BREAK_CLAUSE`, `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`,
				`LETTING_FEES`,`LETTING_FEE_TYPE`,`LETTING_FEE_FREQUENCY`,`LETTING_MANAGEMENT_FEES`,`LETTING_MANAGEMENT_FEE_TYPE`,`LETTING_MANAGEMENT_FEE_FREQUENCY`,`LETTING_ADMIN_FEES`,`LETTING_INVENTORY_FEES`,`PROPERTY_ROOM_ID`,`LETTING_NOTES`) VALUES 
				('$LETTING_ID', '$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$TENANT_ID', '$SHARED_TENANT_IDS', $LETTING_START_DATE, $LETTING_END_DATE, $LETTING_RENT, $LETTING_RENT_FREQUENCY, $LETTING_RENT_PAYMENT_FREQUENCY,
				$LETTING_BREAK_CLAUSE, $LETTING_BREAK_CLAUSE_TYPE, $LETTING_DEPOSIT, $LETTING_DEPOSIT_HELD_BY, $LETTING_DEPOSIT_PROTECTION_SCHEME, $LETTING_SERVICE, $LETTING_FEES, $LETTING_FEE_TYPE,
				$LETTING_FEE_FREQUENCY, $LETTING_MANAGEMENT_FEES, $LETTING_MANAGEMENT_FEE_TYPE, $LETTING_MANAGEMENT_FEE_FREQUENCY, $LETTING_ADMIN_FEES, $LETTING_INVENTORY_FEES,'$PROPERTY_ROOM_ID','$LETTING_NOTES')";
			($db_connect->queryExecute($sql)) ? $Tot_row++ : die($sql);
    }
}
			$z = $row-2;
    		echo "TOTAL ROW IS ".$z."<br><br>";
    		if($Tot_row>0){
                echo "<br />SUCCESSFULY INSERTED <b>LETTINGS</b> COUNT IS : ".$Tot_row;
            }
    		if($ten_row>0){
                echo "<br />SUCCESSFULY INSERTED <b>TENANT</b> COUNT IS : ".$ten_row;
            }
}
?>