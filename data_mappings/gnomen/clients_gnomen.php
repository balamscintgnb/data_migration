<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/gnomen/talbies/applicants.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID=addslashes($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $CLIENT_TITLE='';
            $CLIENT_NAME=trim(str_replace(".","",trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue())));
            $COMPANY_NAME='';
            $CLIENT_TYPE='applicant';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=addslashes($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
            $CLIENT_PRIMARY_PHONE=addslashes($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
            $CLIENT_ADDRESS_CITY=addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            $CLIENT_ADDRESS_TOWN=addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $CLIENT_ADDRESS_POSTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            $CLIENT_PHONE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES = $objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';

			$COMPANY_NAME = trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
				if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
					if($CLIENT_NAME!=''){
						$CLIENT_NAME.=' ';
					}
					$CLIENT_NAME.='('.$COMPANY_NAME.')';
				}
			$CLIENT_NAME=addslashes(trim($CLIENT_NAME));

			$COMMENTS = addslashes($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());

			if($COMMENTS!=''){
				if($CLIENT_NOTES!=''){
					$CLIENT_NOTES.='\n';
				}
				$CLIENT_NOTES.=$COMMENTS;
			}

			$CLIENT_NOTES=addslashes($CLIENT_NOTES);

			$CATEGORY = $objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();

			$SEARCH_CATEGORY='';

			if($CATEGORY=="0"){
    		$SEARCH_CATEGORY=44;
				$MIN_PRICE=$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue();
				$MAX_PRICE=$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
    	}else{
    		$SEARCH_CATEGORY=43;
				$MIN_PRICE=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();
				$MAX_PRICE=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
			}

			$MIN_BEDS = $objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();

			$MAX_BEDS = $objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue();

			$MIN_BATH = $objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();

			$MAX_BATH = $objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();

			$PROPERTY_TYPE = addslashes($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue());

			$PROPERTY_TYPE = explode('-',$PROPERTY_TYPE);

			$PROPERTY_TYPE= implode(' | ',$PROPERTY_TYPE);

			$PROPERTY_LOCATIONS=addslashes(str_replace("'"," ",$objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue()));

			$PROPERTY_LOCATIONS = explode(',',$PROPERTY_LOCATIONS);

			$PROPERTY_LOCATIONS=implode(' | ',$PROPERTY_LOCATIONS);

			$PROPERTY_FURNISHED = $objPHPExcel->getActiveSheet()->getCell('BX'.$row)->getValue();

			$FURNISHED=0;
			$FURNISHED_VALUE='';
			if($PROPERTY_FURNISHED=='Full'){
						$FURNISHED_VALUE='Furnished';
						$FURNISHED=1;
			}

						$DSS = $objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue();

				    $dss_allowed='';

				    if($DSS==1){
				    	$dss_allowed=1;
				    }

						$applicant_search_criteria = array('property_applicant_search_attribute_1'=>$PROPERTY_TYPE,
						'property_applicant_search_attribute_2_from'=>$MIN_BEDS,
						'property_applicant_search_attribute_2_to'=>$MAX_BEDS,
						'property_applicant_search_attribute_3_from'=>$MIN_BATH,
						'property_applicant_search_attribute_3_to'=>$MAX_BATH,
						'property_applicant_search_attribute_4_from'=>'',
						'property_applicant_search_attribute_4_to'=>'',
						'property_applicant_search_attribute_6'=>'',
				    'property_applicant_search_attribute_8'=>'',
						'property_applicant_search_attribute_15'=>$FURNISHED,
						'property_applicant_search_attribute_22'=>'',
						'property_applicant_search_attribute_24'=>$dss_allowed,
						'property_applicant_search_attribute_30'=>$FURNISHED_VALUE
					);

				     $SEARCH_CRITERIA_APPLICANT=array('price'=>$MAX_PRICE
						 ,'pricefrom'=>$MIN_PRICE,'filter_array'=>$applicant_search_criteria,
						 'frequency'=>'','category'=>$SEARCH_CATEGORY,'location'=>$PROPERTY_LOCATIONS);

						 $SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);


            /*echo '<pre><code>';
        print_r();
        echo '</code></pre>';*/


			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
				`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
				`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
				`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`) VALUES 
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";
        $db_connect->queryExecute($sql) or die ($sql);

        }
    }
    	echo $row . " LANDLORDS INSERTED SUCCESSFULLY";
}
?>