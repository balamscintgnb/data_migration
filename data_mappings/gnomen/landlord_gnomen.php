<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/gnomen/talbies/landlords.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

	for($row =1; $row <= $total_rows; $row++){
		if($row>1){


			//logic for data mapping.

			$CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
			$CLIENT_TITLE='';
			$CLIENT_NAME=trim(addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue())));
			$COMPANY_NAME='';
			$CLIENT_TYPE='landlord';
			$CLIENT_SUB_TYPE='';
			$CLIENT_STATUS=addslashes($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
			$CLIENT_STAFF_ID='';
			$CLIENT_PRIMARY_EMAIL=addslashes($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
			
			$CLIENT_PRIMARY_PHONE=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue()));
			// rk added the following preg_replace on 2019-01-07 17-09 for ignoring the special characters while reading content from the cell of the excel page.
			$CLIENT_PRIMARY_PHONE=preg_replace( '/[^0-9\-]/', '', $CLIENT_PRIMARY_PHONE );

			$CLIENT_ADDRESS_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
			$CLIENT_ADDRESS_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
			$CLIENT_ADDRESS_CITY=addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
			$CLIENT_ADDRESS_TOWN=addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
			$CLIENT_ADDRESS_POSTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
			$CLIENT_ADDRESS1_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue());
			$CLIENT_ADDRESS1_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue());
			$CLIENT_ADDRESS1_CITY=addslashes($objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue());
			$CLIENT_ADDRESS1_TOWN=addslashes($objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue());
			$CLIENT_ADDRESS1_POSTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue());
			$CLIENT_ADDRESS2_LINE_1='';
			$CLIENT_ADDRESS2_LINE_2='';
			$CLIENT_ADDRESS2_CITY='';
			$CLIENT_ADDRESS2_TOWN='';
			$CLIENT_ADDRESS2_POSTCODE='';
			$CLIENT_ACCOUNT_NAME=addslashes($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
			$CLIENT_ACCOUNT_NO=addslashes($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
			$CLIENT_ACCOUNT_SORTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue());
			$CLIENT_EMAIL_1=addslashes($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue());
			$CLIENT_EMAIL_2='';
			$CLIENT_EMAIL_3='';
			$CLIENT_EMAIL_4='';
			$CLIENT_EMAIL_5='';
			$CLIENT_PHONE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
			$CLIENT_PHONE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
			$CLIENT_PHONE_3=addslashes($objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue());
			$CLIENT_PHONE_4=addslashes($objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue());
			$CLIENT_PHONE_5='';
			$CLIENT_MOBILE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue());
			$CLIENT_MOBILE_2='';
			$CLIENT_MOBILE_3='';
			$CLIENT_MOBILE_4='';
			$CLIENT_MOBILE_5='';
			$CLIENT_NOTES=addslashes($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue());
			$CLIENT_FAX_1='';
			$CLIENT_FAX_2='';
			$CLIENT_FAX_3='';
			$CLIENT_FAX_4='';
			$CLIENT_FAX_5='';
			$CLIENT_CREATED_ON = '';

			$COMPANY_NAME = $objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();

				if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
					if($CLIENT_NAME!=''){
						$CLIENT_NAME.=' ';
					}
					$CLIENT_NAME.='('.$COMPANY_NAME.')';
				}

			$CLIENT_NAME2=addslashes($objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue());

			if(trim($CLIENT_NAME2)!='' && trim($CLIENT_NAME2)!=trim($CLIENT_NAME)){
						if($CLIENT_NAME!=''){
							$CLIENT_NAME.=' & ';
						}
						$CLIENT_NAME.= $CLIENT_NAME2;
						// $CLIENT_NAME.='('.$CLIENT_NAME2.')';
					}

			$CLIENT_NAME=addslashes($CLIENT_NAME);
			$COMMENTS = $objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();

				if($COMMENTS!=''){
					if($CLIENT_NOTES!=''){
						$CLIENT_NOTES.='\n';
					}
					$CLIENT_NOTES.=$COMMENTS;
				}
			$CLIENT_NOTES=addslashes($CLIENT_NOTES);

			if($CLIENT_PRIMARY_EMAIL == $CLIENT_EMAIL_1)
			{
				$CLIENT_EMAIL_1    = '';
			}

			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
				`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
				`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
				`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES 
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5','$CLIENT_CREATED_ON')";
			$db_connect->queryExecute($sql) or die ($sql);

		}
	}
			$x = 2;  
			$Tot_row = $row-$x;  
			echo $Tot_row . " LANDLORDS INSERTED SUCCESSFULLY";
}
?>