<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/gnomen/talbies/lettings.xls';

// require_once '../includes/config.php';
// require_once '../../header_init.php';
// require_once '../sql_conversion/Classes/PHPExcel/IOFactory.php';

// $file_name='../../data/gnomen/lettings-05-Sep-18.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
		$thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

		for($row =1; $row <= $total_rows; $row++){
			if($row>1){

				$id='';
				$LETTING_ID='';
				$LETTING_CUSTOM_REF_NO=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
				$PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
				$PROPERTY_ROOM_ID='';
				$TENANT_ID=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
				$SHARED_TENANT_IDS='';
				$LETTING_START_DATE=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
				$LETTING_END_DATE=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
					if($LETTING_START_DATE=='' || $LETTING_START_DATE=='0000-00-00'){
						$LETTING_START_DATE='NULL';
					}
					if($LETTING_END_DATE=='' || $LETTING_END_DATE=='0000-00-00'){
						$LETTING_END_DATE='NULL';
					}
					if($LETTING_START_DATE !='NULL'){
						$LETTING_START_DATE = "'".$LETTING_START_DATE."'";
					}
					if($LETTING_END_DATE !='NULL'){
						$LETTING_END_DATE = "'".$LETTING_END_DATE."'";
					}
				$LETTING_RENT=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
				$RENT_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
				$LETTING_RENT_FREQUENCY='';
				$LETTING_RENT_PAYMENT_FREQUENCY='';
					if($RENT_FREQUENCY=='1 month'){
						$LETTING_RENT_FREQUENCY='pcm';
						$LETTING_RENT_PAYMENT_FREQUENCY='monthly';
					}
					else if($RENT_FREQUENCY=='3 month'){
							$LETTING_RENT_FREQUENCY='pq';
							$LETTING_RENT_PAYMENT_FREQUENCY='quarterly';
					}
					else if($RENT_FREQUENCY=='6 month'){
							$LETTING_RENT_FREQUENCY='p6m';
							$LETTING_RENT_PAYMENT_FREQUENCY='semi_annually';
					}
					else if($RENT_FREQUENCY=='12 month'){
							$LETTING_RENT_FREQUENCY='pa';
							$LETTING_RENT_PAYMENT_FREQUENCY='annually';
					}
					else if($RENT_FREQUENCY=='1 year'){
							$LETTING_RENT_FREQUENCY='pa';
							$LETTING_RENT_PAYMENT_FREQUENCY='annually';
					}
					else if($RENT_FREQUENCY=='1 week'){
							$LETTING_RENT_FREQUENCY='pw';
							$LETTING_RENT_PAYMENT_FREQUENCY='weekly';
					}
					else if($RENT_FREQUENCY=='4 week'){
							$LETTING_RENT_FREQUENCY='p4w';
							$LETTING_RENT_PAYMENT_FREQUENCY='four_weekly';
					}
					else if($RENT_FREQUENCY=='8 week'){
							$LETTING_RENT_FREQUENCY='';
							$LETTING_RENT_PAYMENT_FREQUENCY='';
					}
				$LETTING_RENT_PAYMENT_DAY='';
				$LETTING_RENT_ADVANCE='';
				$LETTING_RENT_ADVANCE_TYPE='';
				$LETTING_NOTICE_PERIOD='';
				$LETTING_NOTICE_PERIOD_TYPE='';
				$BREAK_CLAUSE=$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue();
				$LETTING_BREAK_CLAUSE='';
					if($BREAK_CLAUSE=='6 months' || $BREAK_CLAUSE=='12 months'){
						$LETTING_BREAK_CLAUSE=$BREAK_CLAUSE;
					}else{
						$LETTING_BREAK_CLAUSE='';
					}
				$LETTING_BREAK_CLAUSE_TYPE=$objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue();
				$BREAK_CLAUSE_DATE=$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
				$PROPERTY_AVAILABLE_DATE='';
				$LETTING_BREAK_CLAUSE_DATE='';
					if($BREAK_CLAUSE_DATE!='0000-00-00'){
						$LETTING_BREAK_CLAUSE_DATE = "'".$BREAK_CLAUSE_DATE."'";
					}else{
						$LETTING_BREAK_CLAUSE_DATE = "NULL";
					}
				$LETTING_DEPOSIT=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
				$LETTING_DEPOSIT_DUE_DATE='NULL';
				$DEPOSIT_PROTECTION_SCHEME=$objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue();
				$LETTING_DEPOSIT_PROTECTION_SCHEME='';
				$LETTING_DEPOSIT_HELD_BY='';
					if($DEPOSIT_PROTECTION_SCHEME=='TDS'){
						$LETTING_DEPOSIT_PROTECTION_SCHEME=3;
						$LETTING_DEPOSIT_HELD_BY=1;
					}
					else if($DEPOSIT_PROTECTION_SCHEME=='DPS'){
						$LETTING_DEPOSIT_PROTECTION_SCHEME=1;
						$LETTING_DEPOSIT_HELD_BY=1;
					}
					else if($DEPOSIT_PROTECTION_SCHEME=='TDSL'){
						$LETTING_DEPOSIT_PROTECTION_SCHEME=2;
						$LETTING_DEPOSIT_HELD_BY=1;
					}
				$MANAGED_LEVEL=$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();
				$LETTING_SERVICE='';
				if($MANAGED_LEVEL!=''){
					if(stristr($MANAGED_LEVEL, 'MANAGED')){
							$LETTING_SERVICE=1;
					}else if(stristr($MANAGED_LEVEL, 'LET ONLY')){
							$LETTING_SERVICE=2;
					}
					else if(stristr($MANAGED_LEVEL, 'Rent Coll')) {
						$LETTING_SERVICE = 3;
					}
					else if(stristr($MANAGED_LEVEL, 'Management only')) {
						$LETTING_SERVICE = 4;
					}else{
							$LETTING_SERVICE='';
					}
				}
				$LETTING_FEES='NULL';
				$LETTING_FEE_TYPE='';
				$LETTING_FEE_FREQUENCY='';
				$LETTING_MANAGEMENT_FEES=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
				$LETTING_MANAGEMENT_FEE_TYPE=1;
				$LETTING_MANAGEMENT_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
				$LETTING_ADMIN_FEES='';
				$LETTING_ADMIN_FEE_TYPE='';
				$LETTING_INVENTORY_FEES='';
				$LETTING_INVENTORY_FEE_TYPE='';
				$LETTING_RENT_GUARANTEE='';
				$LETTING_LANDLORD_PAYMENT='NULL';
				$LETTING_VAT_PERCENTAGE='';
				$LETTING_NOTES = addslashes($objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue());


			$sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
				`LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
				`LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,`LETTING_BREAK_CLAUSE_DATE`,`LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
				`LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
				`LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
				`LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`) VALUES 
				('$LETTING_ID','$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID','$SHARED_TENANT_IDS',$LETTING_START_DATE,
				$LETTING_END_DATE, '$LETTING_RENT', '$LETTING_RENT_FREQUENCY','$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
				'$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE',$LETTING_BREAK_CLAUSE_DATE,'$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT',
				$LETTING_DEPOSIT_DUE_DATE, '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE',$LETTING_FEES,'$LETTING_FEE_TYPE',
				'$LETTING_FEE_FREQUENCY','$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY', '$LETTING_ADMIN_FEES','$LETTING_ADMIN_FEE_TYPE','$LETTING_INVENTORY_FEES',
				'$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE', $LETTING_LANDLORD_PAYMENT, '$LETTING_VAT_PERCENTAGE','$LETTING_NOTES')";
			$db_connect->queryExecute($sql) or die ($sql);
			}
		}
			$x = 2;  
			$Tot_row = $row-$x;  
			echo $Tot_row . " LETTINGS INSERTED SUCCESSFULLY";
}
?>