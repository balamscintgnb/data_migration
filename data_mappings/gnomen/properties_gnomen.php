<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// // require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/gnomen/talbies/properties.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

	

	for($row =1; $row <= $total_rows; $row++){
		if($row>1){


		// echo$PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();exit;
		$PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
		$PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
		$PROPERTY_VENDOR_ID=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
		$PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
		$PROPERTY_TITLE='';
		$PROPERTY_SHORT_DESCRIPTION = addslashes(trim(preg_replace('/[^(\x20-\x7F)]*/','', $objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue())));
		$PROPERTY_SHORT_DESCRIPTION = trim(htmlentities($PROPERTY_SHORT_DESCRIPTION));
		
		$PROPERTY_DESCRIPTION = addslashes(trim(preg_replace('/[^(\x20-\x7F)]*/','', $objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue())));
		$CATEGORY=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

		$PROPERTY_CATEGORY='';
			if($CATEGORY==1){
					$PROPERTY_CATEGORY='RESIDENTIAL SALES';
			}else if($CATEGORY==2){
					$PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
			}

		$PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
		$PROPERTY_PRICE_FREQUENCY='';
		$given_date=trim($objPHPExcel->getActiveSheet()->getCell('BX'.$row)->getValue());

		$PROPERTY_AVAILABLE_DATE='';
			if($given_date!='0000-00-00'){
				$PROPERTY_AVAILABLE_DATE = "'".$given_date."'";
			}else{
				$PROPERTY_AVAILABLE_DATE = 'NULL';
			}
			
		$PROPERTY_NO = trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
		$PROPERTY_NAME = trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
		$PROPERTY_ADDRESS_LINE_1= trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());

		$PROPERTY_ADDRESS_LINE_2=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue()));
		$PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
		$PROPERTY_ADDRESS_COUNTY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
		$PROPERTY_ADDRESS_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());


			//  rk commented & added the following on 2019-01-10 16-16 for merging the prop#, prop_name and add_line_1 into $PROPERTY_ADDRESS_LINE_1 starts here

    		// if($PROPERTY_NO!=''){
    		// 	$PROPERTY_ADDRESS_LINE_1.=$PROPERTY_NO;
    		// }

    		// if($PROPERTY_NAME!=''){
    		// 	if($PROPERTY_ADDRESS_LINE_1!=""){
    		// 		$PROPERTY_ADDRESS_LINE_1.=', ';
    		// 	}
    		// 	$PROPERTY_ADDRESS_LINE_1.=$PROPERTY_NAME;
    		// }
			
			$address_line_1='';
		
			if($PROPERTY_NO!=''){
				$address_line_1=$PROPERTY_NO;
			}

			if($PROPERTY_NAME!=''){
				if($address_line_1!=""){
					$address_line_1.=', ';
				}
				$address_line_1.=$PROPERTY_NAME;
			}
			
			if($PROPERTY_ADDRESS_LINE_1 != '')
			{
				if($address_line_1!=""){
					$address_line_1.=', ';
				}
				$address_line_1 .= $PROPERTY_ADDRESS_LINE_1;
			}
			
			// $PROPERTY_ADDRESS_LINE_1 = $address_line_1;

			$PROPERTY_ADDRESS_LINE_1 = (substr($address_line_1,-1) == ',') ? substr($address_line_1, 0, -1) : $address_line_1; // omit comma from the end if it's
			$PROPERTY_ADDRESS_LINE_1 = addslashes($PROPERTY_ADDRESS_LINE_1);
			//  rk commented & added the following on 2019-01-10 16-16 for merging the prop#, prop_name and add_line_1 into $PROPERTY_ADDRESS_LINE_1 ends here


		$PROPERTY_STATUS=$objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue(); // fetching from Archived column in excel

		// rk added the following on 2019-01-08  14-48 setting property status starts here
		// if 'Archived' column is 0 & 'Enabled' column is 1 then we set property status as 'Active' to show data in "In Draft" status
		// if($PROPERTY_STATUS == '0' && trim($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue()) == '1') 
		// {
		//         $PROPERTY_STATUS='Active';
		// }
		// rk added the following on 2019-01-08  14-48 setting property status ends here

		// rk added the following on 2019-01-08  14-48 setting property availability into archive starts here
		$PROPERTY_AVAILABILITY=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
		if(stristr($PROPERTY_AVAILABILITY, 'Coming soon') || stristr($PROPERTY_AVAILABILITY, 'Awaiting review'))
		{
			$PROPERTY_AVAILABILITY='AVAILABLE';
		}
		else if(stristr($PROPERTY_AVAILABILITY, 'Managed'))
		{
			$PROPERTY_AVAILABILITY='Let';
		}
		// rk added the following on 2019-01-08  14-48 setting property availability into archive ends here

		$PROPERTY_ADMIN_FEES=$objPHPExcel->getActiveSheet()->getCell('BL'.$row)->getValue();
		$PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();

		if(stristr($PROPERTY_TYPE, 'Flat')){
			$PROPERTY_TYPE='Flat';
		}else if(stristr($PROPERTY_TYPE, 'Townhouse')){
			$PROPERTY_TYPE='Town house';
		}else if(stristr($PROPERTY_TYPE, 'Terrace')){
			$PROPERTY_TYPE='terraced';
		}else if(stristr($PROPERTY_TYPE, 'Semi-Detached')){
			$PROPERTY_TYPE='Semi Detached';
		}else if(stristr($PROPERTY_TYPE, 'Retail')){
			$PROPERTY_TYPE='retail';
		}else if(stristr($PROPERTY_TYPE, 'Detached')){
			$PROPERTY_TYPE='Detached (House)';
		}

		$PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
		$PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
		$PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
		$PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('BW'.$row)->getValue();
		
		$PROPERTY_QUALIFIER=$objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue();
		if (stristr($PROPERTY_QUALIFIER, 'Rent'))
		{
			$PROPERTY_QUALIFIER='';
		}

		$PROPERTY_CLASSIFICATION='';
		$PROPERTY_CURRENT_OCCUPANT='';
		$KITCHEN_DINER='';
		$PARKING=$objPHPExcel->getActiveSheet()->getCell('BM'.$row)->getValue();
		$OFF_ROAD_PARKING=0;
		
			if($PARKING==1){
				$OFF_ROAD_PARKING=filter_var($PARKING,FILTER_VALIDATE_BOOLEAN);
			}

		$ON_ROAD_PARKING='';
		$GARDEN='';
		$WHEELCHAIR_ACCESS='';
		$ELEVATOR_IN_BUILDING='';
		$POOL='';
		$GYM='';
		$KITCHEN='';
		$DINING_ROOM='';
		$FURNISHED=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
		$INTERNET='';
		$WIRELESS_INTERNET='';
		$TV='';

		$WASHER='';
		$DRYER='';
		$DISHWASHER='';
		$PETS=$objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue();
		$PETS_ALLOWED=0;
			if($PETS==1){
				$PETS_ALLOWED=filter_var($PETS,FILTER_VALIDATE_BOOLEAN);
			}

		$FAMILY_OR_CHILD_FRIENDLY='';
		$DSS_ALLOWED='';
		$SMOKING=$objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue();
		$SMOKING_ALLOWED=0;
			if($SMOKING==1){
				$SMOKING_ALLOWED=filter_var($SMOKING,FILTER_VALIDATE_BOOLEAN);
			}

		$SECURITY='';
		$HOT_TUB='';

		$CLEANER='';
		$EN_SUITE='';
		$SECURE_CAR_PARKING='';
		$OPEN_PLAN_LOUNGE='';
		$VIDEO_DOOR_ENTRY='';
		$CONCIERGE_SERVICES='';
		// rk commented & added the following o 2019-01-12 15-07 for avoiding special char to unicode conversion while json encode
		// $PROPERTY_CUSTOM_FEATURES=json_encode(explode(',',addslashes($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue())));
		$PROPERTY_CUSTOM_FEATURES=json_encode(explode(',',addslashes($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue())),JSON_UNESCAPED_UNICODE);
		
		$PROPERTY_ROOMS='';
		$PROPERTY_ASSETS='';

		$PROPERTY_IMAGE_1='';
		$PROPERTY_IMAGE_2='';
		$PROPERTY_IMAGE_3='';
		$PROPERTY_IMAGE_4='';
		$PROPERTY_IMAGE_5='';
		$PROPERTY_IMAGE_6='';
		$PROPERTY_IMAGE_7='';
		$PROPERTY_IMAGE_8='';
		$PROPERTY_IMAGE_9='';

		$PROPERTY_IMAGE_10='';
		$PROPERTY_IMAGE_11='';
		$PROPERTY_IMAGE_12='';
		$PROPERTY_IMAGE_13='';
		$PROPERTY_IMAGE_14='';
		$PROPERTY_IMAGE_15='';
		$PROPERTY_IMAGE_FLOOR_1='';
		$PROPERTY_IMAGE_FLOOR_2='';
		$PROPERTY_IMAGE_FLOOR_3='';
		$PROPERTY_IMAGE_FLOOR_4='';
		$PROPERTY_IMAGE_FLOOR_5='';
		$PROPERTY_IMAGE_EPC_1='';
		$PROPERTY_IMAGE_EPC_2='';
		$PROPERTY_IMAGE_EPC_3='';
		$PROPERTY_IMAGE_EPC_4='';
		$PROPERTY_IMAGE_EPC_5='';
		
		// rk commented the following on 2019-01-07 14-59 for ignoring offset warning notice
		// $EPC_VALUES=explode(',',$objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue());
		// $PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$EPC_VALUES[0], "energy_potential"=> $EPC_VALUES[1], "co2_current"=>$EPC_VALUES[2], "co2_potential"=> $EPC_VALUES[3]));

		// rk added the following on 2019-01-07 15-00 instead of commenting the above EPIC VALUES starts here
		$PROPERTY_EPC_VALUES = "";
		if(stripos($objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue(), ',') !== FALSE)
		{
			$EPC_VALUES=explode(',',$objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue());
			$PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$EPC_VALUES[0], "energy_potential"=> $EPC_VALUES[1], "co2_current"=>$EPC_VALUES[2], "co2_potential"=> $EPC_VALUES[3]));
		}
		else
		{
			$EPC_VALUES=$objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue();
			$PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$EPC_VALUES));
		}
		// rk added the following on 2019-01-07 15-00 instead of commenting the above EPIC VALUES ends here

		$PROPERTY_CREATED_ON='NULL';

		// rk added the following on 2019-01-08 12-12 after getting the new fields from Mr.Bala TL starts here
		$CERTIFICATE_EXPIRE_DATE = '';
  
		$PROPERTY_VENDOR_SOLICITOR_ID = '';
		$PROPERTY_BUYER_SOLICITOR_ID = '';
		$PROPERTY_BUYER_ID = '';
		$INSTRUCTED_DATE = 'NULL';

		$PROPERTY_LETTING_SERVICE = '';
		

		$comments=trim($objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue());
		$notes=trim($objPHPExcel->getActiveSheet()->getCell('CG'.$row)->getValue());
			if($notes == '=FALSE'){
				$notes = '';
			}
		$PROPERTY_NOTES = '';
		if($comments!='' && $notes!='')
			$PROPERTY_NOTES = $comments . '\n'.$notes;
		else if($comments!='' && $notes=='')
			$PROPERTY_NOTES = $comments;
		else if($comments=='' && $notes!='')
			$PROPERTY_NOTES = $notes;
		$PROPERTY_NOTES = addslashes(trim($PROPERTY_NOTES));
		$PROPERTY_LETTING_FEE = trim($objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue());
		$PROPERTY_LETTING_FEE_TYPE = '';
		$PROPERTY_LETTING_FEE_FREQUENCY = '';
		
		$PROPERTY_MANAGEMENT_FEE = '';
		$PROPERTY_MANAGEMENT_FEE_TYPE = '';
		$PROPERTY_MANAGEMENT_FEE_FREQUENCY = '';
		

		$get_lettings_managed = trim($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue());
        if(stristr($get_lettings_managed, 'Full management')) { 
            $PROPERTY_LETTING_SERVICE = '1';
        }
        else if(stristr($get_lettings_managed, 'Let only')) { 
            $PROPERTY_LETTING_SERVICE = '2';
        }
        else if(stristr($get_lettings_managed, 'Rent Coll')) { 
            $PROPERTY_LETTING_SERVICE = '3';
        }
		
		// rk added the following on 2019-01-08 12-12 after getting the new fields from Mr.Bala TL ends here

 

		/////////////////////////////ADD MORE FIELDS/////////////////////////

		$PROPERTY_FORMATTED_ADDRESS='';
		$PROPERTY_ADDRESS_LINE_1 = addslashes($PROPERTY_ADDRESS_LINE_1);
		$PROPERTY_ADDRESS_LINE_2 = addslashes($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY = addslashes($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY = addslashes($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE = addslashes($PROPERTY_ADDRESS_POSTCODE);

		// $PROPERTY_ENABLED=$objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue();

		$living_space = (float)trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
		$land_size = (float)trim($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue());
		$room_info=array();

			if($living_space>0.0){
				$room_info['Living space'] = $living_space;
			}

			if($land_size>0.0){
				$room_info['Land size'] = $land_size;
			}

			$PROPERTY_ROOMS=json_encode($room_info);



		 $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
			`PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`,
			`PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`,
			`PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`,`PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`,
			`PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`,
			`KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`,
			`DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`,
			`DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
			`CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
			`PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
			`PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
			`PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`,
			`CERTIFICATE_EXPIRE_DATE`,`PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,`PROPERTY_VENDOR_SOLICITOR_ID`,
			`PROPERTY_BUYER_SOLICITOR_ID`,  `PROPERTY_BUYER_ID`,  `INSTRUCTED_DATE`,  `PROPERTY_LETTING_SERVICE`,  `PROPERTY_NOTES`,  `PROPERTY_LETTING_FEE`,
			`PROPERTY_LETTING_FEE_TYPE`,  `PROPERTY_LETTING_FEE_FREQUENCY`,  `PROPERTY_MANAGEMENT_FEE`,  `PROPERTY_MANAGEMENT_FEE_TYPE`,  `PROPERTY_MANAGEMENT_FEE_FREQUENCY`)
			 VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
			'$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE ,
			'$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
			'$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS','$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
			'$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
			'$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
			'$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
			'$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
			'$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
			'$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10',
			'$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15',
			'$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
			'$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
			'$PROPERTY_IMAGE_EPC_5', '$CERTIFICATE_EXPIRE_DATE', '$PROPERTY_EPC_VALUES', $PROPERTY_CREATED_ON,'$PROPERTY_CUSTOM_FEATURES',
			'$PROPERTY_ROOMS','','$PROPERTY_VENDOR_SOLICITOR_ID',  '$PROPERTY_BUYER_SOLICITOR_ID',  '$PROPERTY_BUYER_ID', $INSTRUCTED_DATE,
			'$PROPERTY_LETTING_SERVICE',  '$PROPERTY_NOTES',  '$PROPERTY_LETTING_FEE',  '$PROPERTY_LETTING_FEE_TYPE',  '$PROPERTY_LETTING_FEE_FREQUENCY',
			'$PROPERTY_MANAGEMENT_FEE',  '$PROPERTY_MANAGEMENT_FEE_TYPE',  '$PROPERTY_MANAGEMENT_FEE_FREQUENCY')";
		//insert into table
		$db_connect->queryExecute($sql) or die ($sql);
		}
	}
		$x = 2;  
		$Tot_row = $row-$x;  
		echo $Tot_row .   " PROPERTIES INSERTED SUCCESSFULLY";
}
?>