<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/gnomen/talbies/tenants.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $CLIENT_TITLE = '';
            
            // rk added the following code for property genius client only on 2019-01-11 17-03
            // $CLIENT_TITLE=addslashes($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
            // $CLIENT_NAME=addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
            
            // rk commented the following code to not to use the property genius client only on 2019-01-11 17-03
            // $CLIENT_TITLE='';
            $CLIENT_NAME=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));

            $COMPANY_NAME='';
            $CLIENT_TYPE='tenant';
            $CLIENT_SUB_TYPE='';
			$CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=addslashes($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
            $CLIENT_PRIMARY_PHONE=addslashes($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $CLIENT_ADDRESS_CITY=addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
            $CLIENT_ADDRESS_TOWN=addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            $CLIENT_ADDRESS_POSTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';

            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';

            // rk added the following account information on 2019-01-09 16-24 starts here
            $CLIENT_ACCOUNT_NAME=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            $CLIENT_ACCOUNT_NO=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
            $CLIENT_ACCOUNT_SORTCODE=$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue();
            // rk added the following account information on 2019-01-09 16-24 ends here


            $CLIENT_EMAIL_1=addslashes($objPHPExcel->getActiveSheet()->getCell('CO'.$row)->getValue());
            $CLIENT_EMAIL_2=addslashes($objPHPExcel->getActiveSheet()->getCell('CP'.$row)->getValue());
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            $CLIENT_PHONE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
            $CLIENT_PHONE_3=addslashes($objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue());
            $CLIENT_PHONE_4=addslashes($objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue());
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue());
            $CLIENT_MOBILE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('CE'.$row)->getValue());
            $CLIENT_MOBILE_3=addslashes($objPHPExcel->getActiveSheet()->getCell('CF'.$row)->getValue());
            $CLIENT_MOBILE_4=addslashes($objPHPExcel->getActiveSheet()->getCell('CG'.$row)->getValue());
            $CLIENT_MOBILE_5=addslashes($objPHPExcel->getActiveSheet()->getCell('CH'.$row)->getValue());
            
            $CLIENT_NOTES='';
            $CLIENT_NOTES = addslashes($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
                // $COMMENTS = $objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();

                // if($COMMENTS!=''){
                //     if($CLIENT_NOTES!=''){
                //         $CLIENT_NOTES.='\n';
                //     }
                //     $CLIENT_NOTES.=$COMMENTS;
                // }

                // $CLIENT_NOTES=addslashes($CLIENT_NOTES);

            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';

			$COMPANY_NAME = addslashes($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());

			if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
				if($CLIENT_NAME!=''){
					$CLIENT_NAME.=' ';
				}
				$CLIENT_NAME.='('.$COMPANY_NAME.')';
			}

            if($CLIENT_PRIMARY_EMAIL == $CLIENT_EMAIL_1)
            {
                $CLIENT_EMAIL_1    = '';
            }


		    $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
				`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
				`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
				`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES 
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON')";
           //   echo"<br/><br/>";
           // echo$row;
        $db_connect->queryExecute($sql) or die ($sql);

        }
    }
    		$x = 2;  
			$Tot_row = $row-$x;  
			echo $Tot_row .  " TENANTS INSERTED SUCCESSFULLY";
}
?>