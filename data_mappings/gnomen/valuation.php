<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/expert_agent/ramsey_moore/applicants.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
	$Tot_row = 0;
    for($row =1; $row <=$total_rows; $row++){
        if($row>1){
        	// $PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
        	$PROPERTY_VENDOR_ID = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
        	$PROPERTY_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
	        	if(strstr($PROPERTY_STATUS, 'Carried Out')){
	            $PROPERTY_STATUS='1';
		        }elseif(strstr($PROPERTY_STATUS, 'Cancelled')){
		            $PROPERTY_STATUS='0';
		        }elseif(strstr($PROPERTY_STATUS, 'Lost Dead')){
		            $PROPERTY_STATUS='0';
		        }else{
		            $PROPERTY_STATUS='2';
		        }
		        
	        $PROPERTY_PRICE_FROM = trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
	        $PROPERTY_PRICE_TO = trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
	        
	        $PROPERTY_NO = trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
			$PROPERTY_NAME = trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
			$PROPERTY_ADDRESS_LINE_1= trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
				$address_line_1='';
				if($PROPERTY_NO!=''){
					$address_line_1=$PROPERTY_NO;
				} if($PROPERTY_NAME!=''){
					if($address_line_1!=""){
						$address_line_1.=', ';
					}
					$address_line_1.=$PROPERTY_NAME;
				} if($PROPERTY_ADDRESS_LINE_1 != '')
				{
					if($address_line_1!=""){
						$address_line_1.=', ';
					}
					$address_line_1 .= $PROPERTY_ADDRESS_LINE_1;
				}
				$PROPERTY_ADDRESS_LINE_1 = (substr($address_line_1,-1) == ',') ? substr($address_line_1, 0, -1) : $address_line_1; // omit comma from the end if it's
				$PROPERTY_ADDRESS_LINE_1 = addslashes($PROPERTY_ADDRESS_LINE_1);
	
			$PROPERTY_ADDRESS_LINE_2=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue()));
			$PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue()));
			$PROPERTY_ADDRESS_COUNTY = '';
			$PROPERTY_ADDRESS_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
			$PROPERTY_BEDROOMS = trim($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue());
			$PROPERTY_BATHROOMS = trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
			$PROPERTY_RECEPTION = trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
			$FURNISHED = trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue());
			$PROPERTY_VALUER_NOTES = trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue());
			$PROPERTY_CATEGORY = trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue());
				if($PROPERTY_CATEGORY == 'Let'){
					$PROPERTY_CATEGORY = 'RESIDENTIAL LETTINGS';
				} else if($PROPERTY_CATEGORY == 'Sale'){
					$PROPERTY_CATEGORY = 'RESIDENTIAL SALES';
				} else{
					$PROPERTY_CATEGORY = '';
				}
		
		
        	$Query = "INSERT INTO `valuations` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`, `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`, `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`, `PROPERTY_APPOINTMENT_FOLLOWUPDATE`) VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL)";
        	$db_connect->queryExecute($Query) or die($Query);
        	
        }
    }
    		$z = $row-2;
	    	echo "TOTAL ROWS : ".$z. "<br>APPLICANTS INSERTED SUCCESSFULLY COUNT IS : ".$Tot_row;
}
?>