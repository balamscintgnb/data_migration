<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/gnomen/propertry_genius/lettings.xls';

$thisProceed=true;
try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
	$thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();
$client_name_update_count = 0;
$new_tenants_count = 0;
if($thisProceed)
{
	for($row =1; $row <= $total_rows; $row++)
	{
		if($row>1)
		{
			/* rk added the following code on 2019-01-11 18-50 to update the tenant name, email & phone after sync the clients table only for property genius gnomen software starts here */
			$TENANT_ID='';
			$TENANT_ID=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());

			if($TENANT_ID && $TENANT_ID != '0')
			{
				$TENANT_NAME='';
				$TENANT_PHONE='';
				$TENANT_EMAIL='';

				$TENANT_NAME=addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));
				$TENANT_PHONE=trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
				$TENANT_EMAIL=trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());

				$client_name_sql = "SELECT * FROM clients WHERE CLIENT_TYPE='tenant' AND CLIENTID = '" . $TENANT_ID . "'";
				$data_client = '';
				$data_client = json_decode($db_connect->queryFetch($client_name_sql),true);
				// if(isset($data_client['data'][0]))	// if data found then execute the following code for updation
				if(count($data_client['data']) > 0)	// if data found then execute the following code for updation
				{
					$CLIENT_NAME=trim($data_client['data'][0]['CLIENT_NAME']);
					if($CLIENT_NAME=='' && $TENANT_NAME != '')
					{
						$update_tenant_name_sql = "UPDATE clients SET CLIENT_NAME  = '" . $TENANT_NAME . "' WHERE CLIENT_TYPE='tenant' AND CLIENTID = '" . $TENANT_ID . "'";
						$db_connect->queryExecute($update_tenant_name_sql) or die ($update_tenant_name_sql);
						$client_name_update_count++;
					}

					if($TENANT_EMAIL != '')
					{
						$CLIENT_PRIMARY_EMAIL=trim($data_client['data'][0]['CLIENT_PRIMARY_EMAIL']);
						$CLIENT_EMAIL_1=trim($data_client['data'][0]['CLIENT_EMAIL_1']);
						$CLIENT_EMAIL_2=trim($data_client['data'][0]['CLIENT_EMAIL_2']);
						$CLIENT_EMAIL_3=trim($data_client['data'][0]['CLIENT_EMAIL_3']);
						$CLIENT_EMAIL_4=trim($data_client['data'][0]['CLIENT_EMAIL_4']);
						$CLIENT_EMAIL_5=trim($data_client['data'][0]['CLIENT_EMAIL_5']);

						if($TENANT_EMAIL != $CLIENT_PRIMARY_EMAIL && $TENANT_EMAIL != $CLIENT_EMAIL_1 && $TENANT_EMAIL != $CLIENT_EMAIL_2 && $TENANT_EMAIL != $CLIENT_EMAIL_3 && $TENANT_EMAIL != $CLIENT_EMAIL_4 && $TENANT_EMAIL != $CLIENT_EMAIL_5)
						{
							$email_field = '';
							if($CLIENT_PRIMARY_EMAIL == '')
							{
								$email_field = 'CLIENT_PRIMARY_EMAIL';
							}
							else if($CLIENT_EMAIL_1 == '')
							{
								$email_field = 'CLIENT_EMAIL_1';
							}
							else if($CLIENT_EMAIL_2 == '')
							{
								$email_field = 'CLIENT_EMAIL_2';
							}
							else if($CLIENT_EMAIL_3 == '')
							{
								$email_field = 'CLIENT_EMAIL_3';
							}
							else if($CLIENT_EMAIL_4 == '')
							{
								$email_field = 'CLIENT_EMAIL_4';
							}
							else if($CLIENT_EMAIL_5 == '')
							{
								$email_field = 'CLIENT_EMAIL_5';
							}
								$update_tenant_email_sql = "UPDATE clients SET ".$email_field. " = '" . $TENANT_EMAIL . "' WHERE CLIENT_TYPE='tenant' AND CLIENTID = '" . $TENANT_ID . "'";
								$db_connect->queryExecute($update_tenant_email_sql) or die ($update_tenant_email_sql);
						}	// $TENANT_EMAIL inside if condition ends here
					}	// $TENANT_EMAIL if condition ends here

					if($TENANT_PHONE != '')
					{
						$CLIENT_PRIMARY_PHONE=trim($data_client['data'][0]['CLIENT_PRIMARY_PHONE']);
						$CLIENT_PHONE_1=trim($data_client['data'][0]['CLIENT_PHONE_1']);
						$CLIENT_PHONE_2=trim($data_client['data'][0]['CLIENT_PHONE_2']);
						$CLIENT_PHONE_3=trim($data_client['data'][0]['CLIENT_PHONE_3']);
						$CLIENT_PHONE_4=trim($data_client['data'][0]['CLIENT_PHONE_4']);
						$CLIENT_PHONE_5=trim($data_client['data'][0]['CLIENT_PHONE_5']);
						$CLIENT_MOBILE_1=trim($data_client['data'][0]['CLIENT_MOBILE_1']);
						$CLIENT_MOBILE_2=trim($data_client['data'][0]['CLIENT_MOBILE_2']);
						$CLIENT_MOBILE_3=trim($data_client['data'][0]['CLIENT_MOBILE_3']);
						$CLIENT_MOBILE_4=trim($data_client['data'][0]['CLIENT_MOBILE_4']);
						$CLIENT_MOBILE_5=trim($data_client['data'][0]['CLIENT_MOBILE_5']);

						if($TENANT_PHONE != $CLIENT_PRIMARY_PHONE && $TENANT_PHONE != $CLIENT_PHONE_1 && $TENANT_PHONE != $CLIENT_PHONE_2 && $TENANT_PHONE != $CLIENT_PHONE_3 && $TENANT_PHONE != $CLIENT_PHONE_4 && $TENANT_PHONE != $CLIENT_PHONE_5 && $TENANT_PHONE != $CLIENT_MOBILE_1 && $TENANT_PHONE != $CLIENT_MOBILE_2 && $TENANT_PHONE != $CLIENT_MOBILE_3 && $TENANT_PHONE != $CLIENT_MOBILE_4 && $TENANT_PHONE != $CLIENT_MOBILE_5)
						{
							$phone_field = '';
							if($CLIENT_PRIMARY_PHONE == '')
							{
								$phone_field = 'CLIENT_PRIMARY_PHONE';
							}
							else if($CLIENT_PHONE_1 == '')
							{
								$phone_field = 'CLIENT_PHONE_1';
							}
							else if($CLIENT_PHONE_2 == '')
							{
								$phone_field = 'CLIENT_PHONE_2';
							}
							else if($CLIENT_PHONE_3 == '')
							{
								$phone_field = 'CLIENT_PHONE_3';
							}
							else if($CLIENT_PHONE_4 == '')
							{
								$phone_field = 'CLIENT_PHONE_4';
							}
							else if($CLIENT_PHONE_5 == '')
							{
								$phone_field = 'CLIENT_PHONE_5';
							}
							else if($CLIENT_MOBILE_1 == '')
							{
								$phone_field = 'CLIENT_MOBILE_1';
							}
							else if($CLIENT_MOBILE_2 == '')
							{
								$phone_field = 'CLIENT_MOBILE_2';
							}
							else if($CLIENT_MOBILE_3 == '')
							{
								$phone_field = 'CLIENT_MOBILE_3';
							}
							else if($CLIENT_MOBILE_4 == '')
							{
								$phone_field = 'CLIENT_MOBILE_4';
							}
							else if($CLIENT_MOBILE_5 == '')
							{
								$phone_field = 'CLIENT_MOBILE_5';
							}

								$update_tenant_phone_sql = "UPDATE clients SET ".$phone_field. " = '" . $TENANT_PHONE . "' WHERE CLIENT_TYPE='tenant' AND CLIENTID = '" . $TENANT_ID . "'";
								$db_connect->queryExecute($update_tenant_phone_sql) or die ($update_tenant_phone_sql);
						}	// $TENANT_PHONE inside if condition ends here
					}	// $TENANT_PHONE if condition ends here

				}	// $data_client isset if condition ends here
				else
				{
					// if data not found in clients table then make insertion by getting data from lettings.xls & execute the following code for insertion
					$CLIENTID=$TENANT_ID;

					// rk added the following code for property genius client only on 2019-01-11 17-03
					$CLIENT_TITLE='';
					$CLIENT_NAME=$TENANT_NAME;

					$COMPANY_NAME='';
					$CLIENT_TYPE='tenant';
					$CLIENT_SUB_TYPE='';
					$CLIENT_STATUS='';
					$CLIENT_STAFF_ID='';
					$CLIENT_PRIMARY_EMAIL=$TENANT_EMAIL;
					$CLIENT_PRIMARY_PHONE=$TENANT_PHONE;
					$CLIENT_ADDRESS_LINE_1='';
					$CLIENT_ADDRESS_LINE_2='';
					$CLIENT_ADDRESS_CITY='';
					$CLIENT_ADDRESS_TOWN='';
					$CLIENT_ADDRESS_POSTCODE='';
					$CLIENT_ADDRESS1_LINE_1='';
					$CLIENT_ADDRESS1_LINE_2='';
					$CLIENT_ADDRESS1_CITY='';
					$CLIENT_ADDRESS1_TOWN='';
					$CLIENT_ADDRESS1_POSTCODE='';
					$CLIENT_ADDRESS2_LINE_1='';
					$CLIENT_ADDRESS2_LINE_2='';
					$CLIENT_ADDRESS2_CITY='';
					$CLIENT_ADDRESS2_TOWN='';
					$CLIENT_ADDRESS2_POSTCODE='';

					$CLIENT_ACCOUNT_NAME='';
					$CLIENT_ACCOUNT_NO='';
					$CLIENT_ACCOUNT_SORTCODE='';

					$CLIENT_EMAIL_1='';
					$CLIENT_EMAIL_2='';
					$CLIENT_EMAIL_3='';
					$CLIENT_EMAIL_4='';
					$CLIENT_EMAIL_5='';
					$CLIENT_PHONE_1='';
					$CLIENT_PHONE_2='';
					$CLIENT_PHONE_3='';
					$CLIENT_PHONE_4='';
					$CLIENT_PHONE_5='';
					$CLIENT_MOBILE_1='';
					$CLIENT_MOBILE_2='';
					$CLIENT_MOBILE_3='';
					$CLIENT_MOBILE_4='';
					$CLIENT_MOBILE_5='';
					
					$CLIENT_NOTES='';

					$CLIENT_FAX_1='';
					$CLIENT_FAX_2='';
					$CLIENT_FAX_3='';
					$CLIENT_FAX_4='';
					$CLIENT_FAX_5='';
					$CLIENT_CREATED_ON='';


					$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
						`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
						`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
						`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
						`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
						`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
						`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES 
						('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
						'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
						'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
						'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
						'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
						'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
						'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON')";
					$db_connect->queryExecute($sql) or die ($sql);
					$new_tenants_count++;
				}	// $data_client isset else ends here	
			}	// $TENANT_ID if condition ends here
			/* rk added the following code on 2019-01-11 18-50 to update the tenant name, email & phone after sync the clients table only for property genius gnomen software ends here */
		}	// $row if condition ends here
	}	// $row=1 for loop ends here
	echo "IN " . $row . " ROUND(S) " . $client_name_update_count . " TENANT(S) INFO UPDATED SUCCESSFULLY";
	echo "<br/><br/>IN " . $row . " ROUND(S) " . $new_tenants_count . " TENANT(S) HAVE BEEN INSERTED SUCCESSFULLY";
}	// $thisProceed (bool) if condition ends here
?>