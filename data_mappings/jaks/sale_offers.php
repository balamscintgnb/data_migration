<?php 
// error_reporting(E_ALL);
// ini_set('display_errors',1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/southwells/offers.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}
$db_utility = new Utility;
//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns


if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){ 

            //logic for data mapping.
            $SALE_OFFER_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();

            $SALE_PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            // $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
           // $SALE_PROPERTY_ID = ''.$pro_id;
            
            $PROPERTY_APPLICANT_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

            $PROPERTY_SALE_PRICE = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
            $PROPERTY_SALE_PRICE = str_replace('£','',$PROPERTY_SALE_PRICE);
            $PROPERTY_SALE_OFFER_PRICE = str_replace(',','',$PROPERTY_SALE_PRICE);
             
            $PROPERTY_SALE_SOLICITOR_ID='NULL';
            $STATUS=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
            
            $given_date=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue(), 'l dS M y');   

            $PROPERTY_SALE_OFFER_DATE=$given_date;
            $PROPERTY_SALE_OFFER_TIME= $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

            $PROPERTY_SALE_OFFER_TIME = date("G:i:s", strtotime($PROPERTY_SALE_OFFER_TIME)); 
            $PROPERTY_SALE_OFFER_DATETIME="'".$PROPERTY_SALE_OFFER_DATE.' '.$PROPERTY_SALE_OFFER_TIME. "'";

             
            if($given_date==''){
                $PROPERTY_SALE_OFFER_DATETIME="NULL";
            }

            if(strstr($STATUS, 'Accept')){
                $PROPERTY_SALE_OFFER_STATUS='1';
            }elseif(strstr($STATUS, 'Decline')){
                $PROPERTY_SALE_OFFER_STATUS='2';
            }elseif(strstr($STATUS, 'Pending')){
                $PROPERTY_SALE_OFFER_STATUS='0';
            }else{
                $PROPERTY_SALE_OFFER_STATUS='0';
            } 

            // echo $PROPERTY_SALE_OFFER_STATUS; 

            $sql = "INSERT INTO `sale_offers` (`SALE_OFFER_ID`, `SALE_PROPERTY_ID`, `PROPERTY_APPLICANT_ID`, `PROPERTY_SALE_OFFER_PRICE`,
                    `PROPERTY_SALE_SOLICITOR_ID`, `PROPERTY_SALE_OFFER_STATUS`, `PROPERTY_SALE_OFFER_DATETIME`)
                    VALUES ('$SALE_OFFER_ID', '$SALE_PROPERTY_ID', '$PROPERTY_APPLICANT_ID', '$PROPERTY_SALE_OFFER_PRICE', $PROPERTY_SALE_SOLICITOR_ID,
                     '$PROPERTY_SALE_OFFER_STATUS', $PROPERTY_SALE_OFFER_DATETIME)";
               
         //    $db_connect->queryExecute($sql) or die($sql);


    }
}
}
?>  