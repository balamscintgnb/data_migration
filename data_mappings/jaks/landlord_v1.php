<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 1000);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/southwells/clients.xlsx';

$thisProceed=true;


try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);



//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 
// echo $total_rows;exit;
//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            //logic for data mapping.
            $client_code = preg_replace( '/[^[:print:]]/', '',$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $client_type = $objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue();
            if($client_code == "" || $client_type !='TRUE'){
                continue;
            }
 
            $CLIENTID=$client_code;
           
            $CLIENT_TITLE=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

            
            $name1=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());             
            Utility::format_content($name1);             
            if($name1!=''){
            $CLIENT_NAME=$name1;
            }
           
           
            $CLIENT_NAME = preg_replace( '/[^[:print:]]/', '',$CLIENT_NAME);
            $CLIENT_NAME = str_replace("  "," ",$CLIENT_NAME);
            $CLIENT_NAME = addslashes($CLIENT_NAME);
            $COMPANY_NAME=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $CLIENT_TYPE='LANDLORD'; 
            
            $CLIENT_SUB_TYPE=($objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue()==true)?"Guarantor":""; 
            $CLIENT_STATUS=($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()==true)?"Active":"Inactive"; 
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();

            $CLIENT_ADDRESS_LINE_1=trim($objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2='';
            $CLIENT_ADDRESS_CITY='';
            $CLIENT_ADDRESS_TOWN='';
            $CLIENT_ADDRESS_POSTCODE='';

            $CLIENT_ADDRESS_LINE_1 = str_replace("_x000D_"," ",$CLIENT_ADDRESS_LINE_1);

            $CLIENT_PRIMARY_EMAIL = addslashes($CLIENT_PRIMARY_EMAIL);
            $CLIENT_ADDRESS_LINE_1 = addslashes($CLIENT_ADDRESS_LINE_1);             
            
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES='';
            $CLIENT_FAX_1= $objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON=date("Y-m-d H:i:s", 0);
          if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
			if($CLIENT_NAME==''){
                $CLIENT_NAME=$COMPANY_NAME;
            }else{
                $CLIENT_NAME.=$COMPANY_NAME;
            }
		}

      /*  $get_department = '';
            $SEARCH_CATEGORY ='';
        if($get_department == 'residential sales'){
            $SEARCH_CATEGORY = 43;
        }else if($get_department == 'residential lettings'){
            $SEARCH_CATEGORY = 44;
        }else if($get_department == 'Commercial sales'){
            $SEARCH_CATEGORY = 45;
        }else if($get_department == 'Commercial lettings'){
            $SEARCH_CATEGORY = 46;
        }else{
            $SEARCH_CATEGORY ='';
        }*/

        $PROPERTY_LOCATIONS = '';
      
     
     $SEARCH_CRITERIA = '';

     
        $exist_qry = "SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$CLIENTID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
        $client_exists = json_decode($db_connect->queryFetch($exist_qry),true);
       
        if(@$client_exists['data'][0]['CLIENTID']){
            
        }else{
            $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`, `RECORD_UPLOADED`) 
            VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA','0')";        

           // echo $sql.'<br/>';
            $db_connect->queryExecute($sql) or die($sql);
        } 

        } 
    }
}
?>