<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 1000);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/southwells/vendor_property.xlsx';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
        
        $vendor_id=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
        $Propertyid=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
        $room_qry = "SELECT PROPERTY_ID FROM  properties WHERE `PROPERTY_REF_ID` = '$Propertyid'";
        $room_qry_exists = json_decode($db_connect->queryFetch($room_qry),true);
        $count=sizeof($room_qry_exists);
        if($count>0){
            $Propertyid=trim($room_qry_exists['data'][0]['PROPERTY_ID']);
        }        

       
        $Query = "update cnb_southwells.`properties` set `PROPERTY_VENDOR_ID`= '$vendor_id' where PROPERTY_ID='$Propertyid' ";
        $db_connect->queryExecute($Query) or die($Query);
      //  echo "<pre>"; print_r($Query); echo "<pre/>";
        }   

/* $sql3 ="INSERT INTO `property_room`(`Propertyid`, `room_order`, `room_type`, `room_title`,
 `room_description`, `room_size`, `room_sizes`, `room_meter1`, `room_meter2`, `room_feet1`, `room_inch1`, `room_feet2`, `room_inch2`, `photofile`) VALUES 
('$Propertyid', '$room_order', '$room_type', '$room_title', '$room_description', '$room_size', '$room_sizes', '$room_meter1', '$room_meter2', '$room_feet1', '$room_inch1', '$room_feet2', '$room_inch2', '$photofile')";
$db_connect->queryExecute($sql3) or die($sql3); */



        }
    }

?>