<?php 
//todo standard

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\data_migration_bkup\keystone/DIARY.xls'; 
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);



//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
		$type =	$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
		$applicant_id	= preg_replace( '/[^[:print:]]/', '',$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
		if($type !="Viewing" || $applicant_id==""){
			continue;
		}	

       
		$VIEWING_ID	= $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
		
		$client_id_qry = "SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$applicant_id'";
		$cliend_id_detail = json_decode($db_connect->queryFetch($client_id_qry),true);

		if($cliend_id_detail['data'][0]['CLIENTID']){
			$VIEWING_APPLICANT_ID = $cliend_id_detail['data'][0]['CLIENTID'];
		}else{
			continue;
		}

		$VIEWING_USER_ID = 0;
		$system_user_id = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
		$system_login_qry = "SELECT `COL 14` FROM system_login WHERE `COL 1` LIKE '$system_user_id'";
		$system_login_detail = json_decode($db_connect->queryFetch($system_login_qry),true);
		if($system_login_detail['data'][0]['COL 14']){
			$VIEWING_USER_ID = $system_login_detail['data'][0]['COL 14'];
		
		}

	
	
		$VIEWING_PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
		$get_viewing_date = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getformattedValue();
		
		if($get_viewing_date!=""){
			$viewing_date = Utility::convert_tosqldatetime($get_viewing_date, 'd/m/y H:i');
			$VIEWING_DATETIME = date('Y-m-d H:i', strtotime($viewing_date));
		}else{
			$VIEWING_DATETIME = "0000-00-00";
		}
		
		$VIEWING_NOTES = trim(addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getformattedValue()));

		$completed_status = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
		$viewing_status = $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
	
		if($completed_status==1){
			$VIEWING_STATUS = 2;
		}elseif($viewing_status == 1){
			$VIEWING_STATUS = 1;
		}else{
			$VIEWING_STATUS = 0;
		}	
	

		
		$insert_query ='INSERT INTO viewings ( `VIEWING_ID`,`VIEWING_APPLICANT_ID`,`VIEWING_USER_ID`,`VIEWING_PROPERTY_ID`,`VIEWING_DATETIME`,`VIEWING_NOTES`, `VIEWING_TYPE`,`VIEWING_STATUS` ) VALUES (';
        $insert_query .= "'$VIEWING_ID','$VIEWING_APPLICANT_ID','$VIEWING_USER_ID','$VIEWING_PROPERTY_ID','$VIEWING_DATETIME','$VIEWING_NOTES', 3,'$VIEWING_STATUS'";
        $insert_query .= ');';
       
		
		$db_connect->queryExecute($insert_query) or die($insert_query);


		}
	}
}

      
//


?>