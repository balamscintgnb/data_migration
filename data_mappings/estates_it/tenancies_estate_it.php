<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/keystone/CONTRACT.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//print_r($sheet);exit;

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

          $id='';
          $LETTING_ID='';
          $LETTING_CUSTOM_REF_NO=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
          $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
          $PROPERTY_ROOM_ID='';

					$query_tenant="SELECT CLIENT_ID FROM tenant WHERE `LETTING_ID` LIKE '$LETTING_CUSTOM_REF_NO' order by CLIENT_ID asc";
	        $FIND_PRO_ID = json_decode($db_connect->queryFetch($query_tenant),true);
	        $FIND_PRO_ID = $FIND_PRO_ID['data'] ;
	        $TENANT_ID=$FIND_PRO_ID[0]['CLIENT_ID'];
					$TENANT_ID =preg_replace( '/[^[:print:]]/', '',$TENANT_ID);

	        $SHARED_TENANT_IDS='';
	        if(count($FIND_PRO_ID)>1){
	            $shared_tenant_ids=array();
	            array_shift($FIND_PRO_ID);
	            foreach($FIND_PRO_ID as $shared_tenant_id){
								  $shared_tenant=preg_replace( '/[^[:print:]]/', '',$shared_tenant_id['CLIENT_ID']);
	                array_push($shared_tenant_ids,$shared_tenant);
	            }
	            $SHARED_TENANT_IDS = json_encode($shared_tenant_ids);

	        }

					$query_property="SELECT agency_fee FROM property WHERE `PROPCODE` LIKE '$PROPERTY_ID' order by PROPCODE asc";
					$property_fetch = json_decode($db_connect->queryFetch($query_property),true);
					$property_fetch_data = $property_fetch['data'] ;
					$ADMIN_FEE=$property_fetch_data[0]['agency_fee'];

					//echo '<pre>',print_r($ADMIN_FEE),'</pre>';

  			  //$LETTING_START_DATE=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
          //$LETTING_END_DATE=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();

					$LETTING_START_DATE='';

	        $given_date1=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getformattedValue(),'d/m/y');

					if($given_date1!=''){
              $LETTING_START_DATE=$given_date1;
          }
          if($given_date1==''){
              $LETTING_START_DATE="0000-00-00";
          }

	        $LETTING_END_DATE='';

	        $given_date2=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getformattedValue(),'d/m/y');

          if($given_date2!=''){
							$LETTING_END_DATE=$given_date2;
          }
          if($given_date2==''){
              $LETTING_END_DATE="0000-00-00";
          }


          $LETTING_RENT=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
          $RENT_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();

          $LETTING_RENT_FREQUENCY='';
					$LETTING_RENT_PAYMENT_FREQUENCY='';

          if($RENT_FREQUENCY=='Monthly'){
              $LETTING_RENT_FREQUENCY='pcm';
							$LETTING_RENT_PAYMENT_FREQUENCY='monthly';
          }else if($RENT_FREQUENCY=='Weekly'){
              $LETTING_RENT_FREQUENCY='pw';
							$LETTING_RENT_PAYMENT_FREQUENCY='weekly';
          }else if($RENT_FREQUENCY=='Four Weekly'){
              $LETTING_RENT_FREQUENCY='p4w';
							$LETTING_RENT_PAYMENT_FREQUENCY='four_weekly';
          }else if($RENT_FREQUENCY=='6 month'){
              $LETTING_RENT_FREQUENCY='p6m';
							$LETTING_RENT_PAYMENT_FREQUENCY='semi_annually';
          }else if($RENT_FREQUENCY=='8 week'){
              $LETTING_RENT_FREQUENCY='';
							$LETTING_RENT_PAYMENT_FREQUENCY='';
          }

          $LETTING_RENT_PAYMENT_DAY='';
          $LETTING_RENT_ADVANCE='';
          $LETTING_RENT_ADVANCE_TYPE='';
          $LETTING_NOTICE_PERIOD='';
          $LETTING_NOTICE_PERIOD_TYPE='';

          $BREAK_CLAUSE='';

          $LETTING_BREAK_CLAUSE='';

          if($BREAK_CLAUSE=='6 months' || $BREAK_CLAUSE=='12 months'){
            $LETTING_BREAK_CLAUSE=$BREAK_CLAUSE;
          }else{
            $LETTING_BREAK_CLAUSE='';
          }

          $LETTING_BREAK_CLAUSE_TYPE='';

					$BREAK_CLAUSE_DATE='';

					$PROPERTY_AVAILABLE_DATE='';
	        if($BREAK_CLAUSE_DATE!='0000-00-00'){
	        	$LETTING_BREAK_CLAUSE_DATE=$BREAK_CLAUSE_DATE;
	        }

          $LETTING_DEPOSIT=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
          $LETTING_DEPOSIT_DUE_DATE='';

					$DEPOSIT_PROTECTION_SCHEME='';

					$LETTING_DEPOSIT_PROTECTION_SCHEME='';
					$LETTING_DEPOSIT_HELD_BY=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();

					if($DEPOSIT_PROTECTION_SCHEME == 'My Deposits'){
	            $LETTING_DEPOSIT_HELD_BY='3';
	            $LETTING_DEPOSIT_PROTECTION_SCHEME = '2';
	        }elseif($DEPOSIT_PROTECTION_SCHEME == '3'){
	            $LETTING_DEPOSIT_HELD_BY='3';
	            $LETTING_DEPOSIT_PROTECTION_SCHEME ='4';
	        }elseif($DEPOSIT_PROTECTION_SCHEME =='1'){
	            $LETTING_DEPOSIT_HELD_BY='3';
	            $LETTING_DEPOSIT_PROTECTION_SCHEME ='1';
	        }

          $MANAGED_LEVEL='';

					$LETTING_SERVICE='';

	        if($MANAGED_LEVEL!=''){
	            if(stristr($MANAGED_LEVEL, 'LET ONLY')){
	                $LETTING_SERVICE=2;
	            }else if(stristr($MANAGED_LEVEL, 'MANAGED')){
	                $LETTING_SERVICE=1;
	            }else{
	                $LETTING_SERVICE='';
	            }
	        }


          $LETTING_FEES='';
          $LETTING_FEE_TYPE='';
          $LETTING_FEE_FREQUENCY='';
          $LETTING_MANAGEMENT_FEES='';
          $LETTING_MANAGEMENT_FEE_TYPE='';
          $LETTING_MANAGEMENT_FEE_FREQUENCY='';
          echo $LETTING_ADMIN_FEES=$ADMIN_FEE;
          $LETTING_ADMIN_FEE_TYPE='';
          $LETTING_INVENTORY_FEES='';
          $LETTING_INVENTORY_FEE_TYPE='';
          $LETTING_RENT_GUARANTEE='';
          $LETTING_LANDLORD_PAYMENT='';
          $LETTING_VAT_PERCENTAGE='';
          $LETTING_NOTES='';


       $sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
          `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
          `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,`LETTING_BREAK_CLAUSE_DATE`,`LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
          `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
          `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
          `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`)
           VALUES ('$LETTING_ID','$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID','$SHARED_TENANT_IDS','$LETTING_START_DATE',
					'$LETTING_END_DATE', '$LETTING_RENT', '$LETTING_RENT_FREQUENCY','$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
					'$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE','$LETTING_BREAK_CLAUSE_DATE','$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT'
					, '$LETTING_DEPOSIT_DUE_DATE', '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE','$LETTING_FEES','$LETTING_FEE_TYPE',
					'$LETTING_FEE_FREQUENCY','$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY', '$LETTING_ADMIN_FEES','$LETTING_ADMIN_FEE_TYPE','$LETTING_INVENTORY_FEES',
					 '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE', '$LETTING_LANDLORD_PAYMENT', '$LETTING_VAT_PERCENTAGE','$LETTING_NOTES')";

          $db_connect->queryExecute($sql);

					// if($row>20){
					// 	break;
					// }


        }
    }

}
