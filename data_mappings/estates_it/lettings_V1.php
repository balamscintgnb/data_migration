<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='D:\data_migration_bkup\phillips/Tenancies.csv';

$thisProceed=true;


try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();
if($_GET['clear'] == 1){ 
$truncate_data = "TRUNCATE TABLE `lettings`"; 
$db_connect->queryExecute($truncate_data);
echo 'cleared'; exit;

}
if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        //logic for data mapping.
        $LETTING_ID='';
        $LETTING_CUSTOM_REF_NO=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue();
        $PROPERTY_ROOM_ID='';

       
        $query_1="SELECT CLIENTID FROM clients_tenant_lettings WHERE `CLIENT_STAFF_ID` LIKE '$LETTING_CUSTOM_REF_NO' order by id asc";
        $FIND_PRO_ID = json_decode($db_connect->queryFetch($query_1),true);

        $FIND_PRO_ID = $FIND_PRO_ID['data'] ;
        $TENANT_ID=$FIND_PRO_ID[0]['CLIENTID'];

        $SHARED_TENANT_IDS='';    
        if(count($FIND_PRO_ID)>1){
            $shared_tenant_ids=array();
            array_shift($FIND_PRO_ID);
            foreach($FIND_PRO_ID as $shared_tenant_id){
                array_push($shared_tenant_ids,$shared_tenant_id['CLIENTID']);
            }
            $SHARED_TENANT_IDS = json_encode($shared_tenant_ids);

        }

        $LETTING_START_DATE='';

        $given_date1=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();

            if($given_date1!=''){
                $LETTING_START_DATE="'".date('Y-m-d',strtotime($given_date1))."'";
            }
            if($given_date1==''){
                $LETTING_START_DATE="0000-00-00";
            }

        $LETTING_END_DATE='';

        $given_date2=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
            if($given_date2!=''){
   
            $LETTING_END_DATE="'".date('Y-m-d',strtotime($given_date2))."'";
            }
            if($given_date2==''){
                $LETTING_END_DATE="0000-00-00";
            }

        $LETTING_RENT=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();

        
        $LETTING_RENT_PAYMENT_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
        if(strtolower($LETTING_RENT_PAYMENT_FREQUENCY) == 'monthly')
            $LETTING_RENT_PAYMENT_FREQUENCY='monthly';
        if(strtolower($LETTING_RENT_PAYMENT_FREQUENCY) == 'weekly')
            $LETTING_RENT_PAYMENT_FREQUENCY='weekly';
        if(strtolower($LETTING_RENT_PAYMENT_FREQUENCY) == 'bi-annually')
            $LETTING_RENT_PAYMENT_FREQUENCY='semi_annually';
        if(strtolower($LETTING_RENT_PAYMENT_FREQUENCY) == 'quarterly')
            $LETTING_RENT_PAYMENT_FREQUENCY='quarterly';

       
        
        if($LETTING_RENT_PAYMENT_FREQUENCY == 'monthly'){
            $LETTING_RENT_FREQUENCY='pcm';
        }elseif($LETTING_RENT_PAYMENT_FREQUENCY == 'weekly'){
            $LETTING_RENT_FREQUENCY='pw';
        }elseif($LETTING_RENT_PAYMENT_FREQUENCY == 'semi_annually'){
            $LETTING_RENT_FREQUENCY='p6m';
        }elseif($LETTING_RENT_PAYMENT_FREQUENCY == 'quarterly'){
            $LETTING_RENT_FREQUENCY='p4w';
        }             
        
        
        $LETTING_RENT_PAYMENT_DAY=preg_replace("/[^0-9]/","",$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        $LETTING_RENT_ADVANCE='';

        $LETTING_RENT_ADVANCE_TYPE='';
        $LETTING_NOTICE_PERIOD='';
        $LETTING_NOTICE_PERIOD_TYPE='';
        $LETTING_BREAK_CLAUSE='';
        $LETTING_BREAK_CLAUSE_TYPE='';
        $LETTING_DEPOSIT=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();

        $LETTING_DEPOSIT_DUE_DATE='';
        $LETTING_DEPOSIT_HELD_BY='';

        $protection_scheme = $objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $LETTING_DEPOSIT_PROTECTION_SCHEME = '';
        
        if($protection_scheme == 'My Deposits'){
            $LETTING_DEPOSIT_HELD_BY='3';
            $LETTING_DEPOSIT_PROTECTION_SCHEME = '2';
        }elseif($protection_scheme == 'TDS (Tenancy Deposit Sceme)'){
            $LETTING_DEPOSIT_HELD_BY='3';
            $LETTING_DEPOSIT_PROTECTION_SCHEME ='4';
        }elseif($protection_scheme =='DPS (Deposit Protection Service)'){
            $LETTING_DEPOSIT_HELD_BY='3';
            $LETTING_DEPOSIT_PROTECTION_SCHEME ='1';
        }
      

        $LETTING_SERVICE='' ;
        $get_let_service = $objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();
            if($get_let_service == 'Let Only')
                $LETTING_SERVICE=2;
            if($get_let_service == 'Fully Managed')
                $LETTING_SERVICE=1;
            if($get_let_service == 'Rent Collection')
                $LETTING_SERVICE=3;

     
        


        if($LETTING_SERVICE == 1){
            $LETTING_MANAGEMENT_FEES=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
            $LETTING_MANAGEMENT_FEE_TYPE='1';
            $LETTING_MANAGEMENT_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;

            $LETTING_FEES='0';
            $LETTING_FEE_TYPE='1';
            $LETTING_FEE_FREQUENCY='';
            
        }else{
            $LETTING_MANAGEMENT_FEES='0';
            $LETTING_MANAGEMENT_FEE_TYPE='1';
            $LETTING_MANAGEMENT_FEE_FREQUENCY='';

            $LETTING_FEES=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
            $LETTING_FEE_TYPE='1';
            $LETTING_FEE_FREQUENCY=$LETTING_RENT_FREQUENCY;
        }
        $LETTING_ADMIN_FEES='';
        $LETTING_ADMIN_FEE_TYPE='';
        $LETTING_INVENTORY_FEES='';      
        $LETTING_INVENTORY_FEE_TYPE='';
        $LETTING_RENT_GUARANTEE='';

        $LETTING_LANDLORD_PAYMENT='';
        $LETTING_VAT_PERCENTAGE='';
        $LETTING_NOTES=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
        $TENANT_NAME=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        
        
        
        
        
        
            
        /////////////////////////////SUB QUERY//////////////////////////
      
        ///////////////////////////END SUB QUERY////////////////////////


         $sql ="INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`, `SHARED_TENANT_IDS`,
                    `LETTING_START_DATE`, `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`,
                    `LETTING_RENT_ADVANCE`, `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,
                    `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`,
                    `LETTING_SERVICE`,`LETTING_FEES`, `LETTING_FEE_TYPE`, `LETTING_FEE_FREQUENCY`,`LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`,
                    `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`, `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`,
                    `LETTING_RENT_GUARANTEE`, `LETTING_VAT_PERCENTAGE`, `LETTING_NOTES`) VALUES
                    ('$LETTING_ID', '$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID', '$SHARED_TENANT_IDS', $LETTING_START_DATE,
                     $LETTING_END_DATE, '$LETTING_RENT', '$LETTING_RENT_FREQUENCY', '$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
                    '$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE', '$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT', 
                    '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE','$LETTING_FEES',
                    '$LETTING_FEE_TYPE', '$LETTING_FEE_FREQUENCY', '$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY',
                    '$LETTING_ADMIN_FEES', '$LETTING_ADMIN_FEE_TYPE', '$LETTING_INVENTORY_FEES', '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE',
                    '$LETTING_VAT_PERCENTAGE', '$LETTING_NOTES')";
            echo "<br/><br/>";
         
    
    //insert into table    
     //$db_connect->queryExecute($sql) ;
    }
}
}
?>