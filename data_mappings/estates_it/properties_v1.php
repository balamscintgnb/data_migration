<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 1000);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/cfl/PROPERTY.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


                 
     
            $PROPERTY_ADMIN_FEES='';
    
            $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $PROPERTY_ID = str_replace('_', '', $PROPERTY_ID);
            $PROPERTY_ID ='CITYFOX_'.$PROPERTY_ID;            
            $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            
            $PROPERTY_STAFF_ID='';
            $PROPERTY_TITLE='';

            $PROPERTY_SHORT_DESCRIPTION='';
            $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();

            $property_category_code=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue(); 
            if($property_category_code==1){
                $PROPERTY_CATEGORY = "RESIDENTIAL SALES";
            }elseif($property_category_code==2){
                $PROPERTY_CATEGORY = "RESIDENTIAL LETTINGS";
            }elseif($property_category_code==3){
                $PROPERTY_CATEGORY = "COMMERCIAL LETTINGS";
            }else{
                $PROPERTY_CATEGORY = "COMMERCIAL SALES";
            }
        
            
            $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue(); 
    
            $get_frequency ='';
            $PROPERTY_PRICE_FREQUENCY='';
            if(stristr($get_frequency,'Monthly') || stristr($get_frequency,'Monthly Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PCM';
            }
            else if(stristr($get_frequency,'Weekly') || stristr($get_frequency,'Weekly Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PW';
            }
            else if(stristr($get_frequency,'Annual') || stristr($get_frequency,'Annual Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PA';
            }

           
            $PROPERTY_QUALIFIER='';
            if(stristr($PROPERTY_CATEGORY, 'SALES')){
                $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue(); 
                $get_qualifier=$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
                if($get_qualifier == 'Asking price of'|| $get_qualifier == 'Fixed Price'){ 
                    $PROPERTY_QUALIFIER='Fixed price';
                }
                else if($get_qualifier == 'Offers in the region of'){
                    $PROPERTY_QUALIFIER='Offers in the region of';
                }
                else if($get_qualifier == 'Guide Price' || $get_qualifier == 'Auction guide price of'){
                    $PROPERTY_QUALIFIER='Guide price';
                }
                else if($get_qualifier == 'From'){
                    $PROPERTY_QUALIFIER='From';
                }
                else if($get_qualifier == 'Offers in excess of' || $get_qualifier == 'Offers above'){
                    $PROPERTY_QUALIFIER='Offers in excess of';
                }
                else if($get_qualifier == 'Offers Over'){ 
                    $PROPERTY_QUALIFIER='Offers over';
                }

                //doubt
                //Keen to sell
                //Must be seen
               //Reduced
               //Reduced for Quick Sale

            }
    
    
            ////////////insert date format///////////////
            /*
            $get_available_from = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue(), 'd/m/Y');
            if($get_available_from) { 
                $PROPERTY_AVAILABLE_DATE="'".$get_available_from."'";
            }
            else { 
                $PROPERTY_AVAILABLE_DATE='NULL';
            }
            */
            $PROPERTY_AVAILABLE_DATE='NULL';

                /////////////end//////////////
    
    
            $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();            
            $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()." ".$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()." ".$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
            $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            /* $$PROPERTY_ADDRESS_LINE_1 = Utility::filter($PROPERTY_ADDRESS_LINE_1);
            $PROPERTY_ADDRESS_LINE_2 = Utility::filter($PROPERTY_ADDRESS_LINE_2);
 */
            $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            //$PROPERTY_FORMATTED_ADDRESS=$PROPERTY_ADDRESS_LINE_2.','.$PROPERTY_ADDRESS_CITY.','.$PROPERTY_ADDRESS_COUNTY.','.$PROPERTY_ADDRESS_POSTCODE;
            $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
    
            $PROPERTY_ADDRESS_LINE_1= addslashes($PROPERTY_ADDRESS_LINE_1);
            $PROPERTY_ADDRESS_LINE_2= addslashes($PROPERTY_ADDRESS_LINE_2);
            $PROPERTY_ADDRESS_CITY= addslashes($PROPERTY_ADDRESS_CITY);
            $PROPERTY_ADDRESS_COUNTY= addslashes($PROPERTY_ADDRESS_COUNTY);

            $PROPERTY_STATUS='';
            $active_status = $objPHPExcel->getActiveSheet()->getCell('P'.$row)->getformattedValue();
            if($active_status){
                $PROPERTY_STATUS='ACTIVE';
            }else{
                $PROPERTY_STATUS='';
            }
            
            $get_property_status = $objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue();
            if($get_property_status != ''){
                
                if($get_property_status == 'Let By Other Agency'){
                    $get_property_status = 'Externally';
                }elseif($get_property_status == 'Let STC'){
                    $get_property_status = 'Let Agreed';
                }elseif($get_property_status == 'On Market'|| $get_property_status == 'Available to Let' ){
                    $get_property_status = 'Available';
                }elseif($get_property_status == 'Sold By Other Agent'){
                    $get_property_status = 'Externally Sold';
                }elseif($get_property_status == 'Under Negotiation'){
                    $get_property_status = 'On Hold';
                }elseif($get_property_status == 'Unavailable'||$get_property_status == 'Unavailable.'){
                    $get_property_status = 'Withdrawn';
                }elseif($get_property_status == 'Fall Through'){
                    $get_property_status = 'For sale';
                }


                $PROPERTY_AVAILABILITY=$get_property_status;
                
            }
            else { 
                $PROPERTY_AVAILABILITY='ARCHIVED';
            }
    
            $PROPERTY_ADMIN_FEES='';
          
            $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
            if($PROPERTY_TYPE == "Apartment" || $PROPERTY_TYPE == "Flat" || $PROPERTY_TYPE == "Mansion Block" || $PROPERTY_TYPE == "Mews" || $PROPERTY_TYPE == "Room To Let")
            {
                $PROPERTY_TYPE = "Flat";
            }if($PROPERTY_TYPE == "Conversion"){
                $PROPERTY_TYPE = "house share";
            }if($PROPERTY_TYPE == "Cottage"){
                $PROPERTY_TYPE = "Cottage";
            }if($PROPERTY_TYPE == "Duplex"){
                $PROPERTY_TYPE = "house";
            }
            if($PROPERTY_TYPE == "End Of Terrace" || $PROPERTY_TYPE == "EOT" ){
                $PROPERTY_TYPE = "End Terrace";
            }elseif($PROPERTY_TYPE =="Flatshare"){
                $PROPERTY_TYPE ='flat share';
            }else if($PROPERTY_TYPE=='House'|| $PROPERTY_TYPE=='Duplex' || $PROPERTY_TYPE=='Bedsit' || $PROPERTY_TYPE=='Loft'){
                $PROPERTY_TYPE='house';
            }elseif($PROPERTY_TYPE=='House Share' || $PROPERTY_TYPE=='Room To Let' || $PROPERTY_TYPE=='shared house'){
                $PROPERTY_TYPE='house share';
            }elseif($PROPERTY_TYPE=='Semi Detached'){
                $PROPERTY_TYPE='semi-detached';
            }elseif($PROPERTY_TYPE=='Terraced' || $PROPERTY_TYPE=='MTH'){
                $PROPERTY_TYPE='terraced';
            }elseif($PROPERTY_TYPE=='Maisonette') {
                $PROPERTY_TYPE='maison';
            }elseif($PROPERTY_TYPE=='Office/Shop') {
                $PROPERTY_TYPE='Commercial Property';
            }elseif($PROPERTY_TYPE=='Penthouse') {
                $PROPERTY_TYPE='Penthouse';
            }elseif($PROPERTY_TYPE=='Land' || $PROPERTY_TYPE=='Prupose Built'){
                $PROPERTY_TYPE='Land';
            }elseif($PROPERTY_TYPE=='Shop') {
                $PROPERTY_TYPE='Shop';
            }elseif($PROPERTY_TYPE=='studio') {
                $PROPERTY_TYPE='studio';
            }elseif($PROPERTY_TYPE=='Town House') {
                $PROPERTY_TYPE='Town House';
            }elseif($PROPERTY_TYPE=='Warehouse Conversion' || $PROPERTY_TYPE=='Warehouse') {
                $PROPERTY_TYPE='Warehouse Conversion';
            }   


     
    
            $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue();
            $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue();
            $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue();
            
            $PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();  //DOUBT = AST,AST Let Only,AST Managed,Long Lease.


            if(stristr($PROPERTY_CATEGORY, 'sales')){
                if(stristr($PROPERTY_TENURE, 'AST')){
                    $PROPERTY_TENURE='Freehold';
                }
                if(stristr($PROPERTY_TENURE, 'Freehold')){
                    $PROPERTY_TENURE='Freehold';
                }
                if(stristr($PROPERTY_TENURE, 'Leasehold')){
                    $PROPERTY_TENURE='Leasehold';
                }
                if(stristr($PROPERTY_TENURE, 'Long Lease')){
                    $PROPERTY_TENURE='Leasehold';
                }
                if(stristr($PROPERTY_TENURE, 'Share of Freehold')){
                    $PROPERTY_TENURE='share of freehold';
                }
            }else{
                if(stristr($PROPERTY_TENURE, 'AST')){
                    $PROPERTY_TENURE='Short Let or Long let';
                }
                if(stristr($PROPERTY_TENURE, 'RG AST')){
                    $PROPERTY_TENURE='Short Let or Long let';
                }
                if(stristr($PROPERTY_TENURE, 'RG Agreement')){
                    $PROPERTY_TENURE='Short Let or Long let';
                }
                if(stristr($PROPERTY_TENURE, 'LH+ShareFH')){
                    $PROPERTY_TENURE='Short Let or Long let';
                }
                if(stristr($PROPERTY_TENURE, 'Short Let')){
                    $PROPERTY_TENURE='Short Let';
                }
                if(stristr($PROPERTY_TENURE, 'Six Month')){
                    $PROPERTY_TENURE='Short Let';
                }
                if(stristr($PROPERTY_TENURE, 'Freehold')){
                    $PROPERTY_TENURE='Short Let or Long let';
                }
                if(stristr($PROPERTY_TENURE, 'Leasehold')){
                    $PROPERTY_TENURE='Long Let';
                }
            }
            $Lease_term_years=$objPHPExcel->getActiveSheet()->getCell('DL'.$row)->getValue();
            $PROPERTY_CLASSIFICATION='';
            $PROPERTY_CURRENT_OCCUPANT='';
            $KITCHEN_DINER='';
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING='';
            $GARDEN='';
            $WHEELCHAIR_ACCESS='';
            $ELEVATOR_IN_BUILDING='';
            $POOL='';
            $GYM='';
            $KITCHEN=($objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue() !="")? 1 :'';
            $DINING_ROOM='';
            $FURNISHED='';
            $INTERNET='';
            $WIRELESS_INTERNET='';
            $TV='';
    
            $WASHER='';
            $DRYER='';
            $DISHWASHER='';
            $PETS_ALLOWED='';
            $FAMILY_OR_CHILD_FRIENDLY='';
            $DSS_ALLOWED='';
            $SMOKING_ALLOWED='';
            $SECURITY='';
            $HOT_TUB='';
    
            $CLEANER='';
            $EN_SUITE='';
            $SECURE_CAR_PARKING='';
            $OPEN_PLAN_LOUNGE='';
            $VIDEO_DOOR_ENTRY='';
            $CONCIERGE_SERVICES='';
            $PROPERTY_CUSTOM_FEATURES='';
            $PROPERTY_ROOMS='';
            $PROPERTY_ASSETS='';
    
            $PROPERTY_IMAGE_1='';
            $PROPERTY_IMAGE_2='';
            $PROPERTY_IMAGE_3='';
            $PROPERTY_IMAGE_4='';
            $PROPERTY_IMAGE_5='';
            $PROPERTY_IMAGE_6='';
            $PROPERTY_IMAGE_7='';
            $PROPERTY_IMAGE_8='';
            $PROPERTY_IMAGE_9='';
    
            $PROPERTY_IMAGE_10='';
            $PROPERTY_IMAGE_11='';
            $PROPERTY_IMAGE_12='';
            $PROPERTY_IMAGE_13='';
            $PROPERTY_IMAGE_14='';
            $PROPERTY_IMAGE_15='';
            $PROPERTY_IMAGE_FLOOR_1='';
            $PROPERTY_IMAGE_FLOOR_2='';
            $PROPERTY_IMAGE_FLOOR_3='';
            $PROPERTY_IMAGE_FLOOR_4='';
            $PROPERTY_IMAGE_FLOOR_5='';
            $PROPERTY_IMAGE_EPC_1='';
            $PROPERTY_IMAGE_EPC_2='';
            $PROPERTY_IMAGE_EPC_3='';
            $PROPERTY_IMAGE_EPC_4='';
            $PROPERTY_IMAGE_EPC_5='';
            $PROPERTY_EPC_VALUES='';
            $PROPERTY_CREATED_ON=date('Y-m-d H:i:s');
    
    
            ///////////////////////////// ADD MORE FIELDS /////////////////////////
          
           
            $PROPERTY_SHORT_DESCRIPTION=addslashes(strip_tags(trim($PROPERTY_SHORT_DESCRIPTION)));
            
            $price_text = $objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue(); //DOUBT
            $PROPERTY_ACCETABLE_PRICE = $price_text;    //DOUBT
            
            if($PROPERTY_ACCETABLE_PRICE!=''){
                $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
            }

            $ground_rent = $objPHPExcel->getActiveSheet()->getCell('DO'.$row)->getValue();
            if($ground_rent){
                $PROPERTY_DESCRIPTION.='\n Ground rent: '.$ground_rent;
            }


            $PROPERTY_DESCRIPTION=addslashes(trim(htmlentities($PROPERTY_DESCRIPTION)));
           
    
    
            $vendor_name = $objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue();
            $vendor_solicitor_name = $objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue();
            $PROPERTY_VENDOR_ID=0;
            $PROPERTY_SOLICITOR_ID=0;
            if(stristr($PROPERTY_CATEGORY, 'sales')){
                if($vendor_name !='' ) {
                    $ventor_qry = "SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$vendor_name' AND CLIENT_TYPE LIKE 'VENDOR' ";
                    $vendor_exists = json_decode($db_connect->queryFetch($ventor_qry),true);
                   
                    if(@$vendor_exists['data'][0]['CLIENTID']){
                        $PROPERTY_VENDOR_ID=$vendor_exists['data'][0]['CLIENTID'];
                    }
                   
                }
                
                if($vendor_solicitor_name !='' ) {
                    $solicitor_qry = "SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$vendor_solicitor_name' AND CLIENT_TYPE LIKE 'SOLICITOR' ";
                    $solicitor_exists = json_decode($db_connect->queryFetch($solicitor_qry),true);
                   
                    if(@$solicitor_exists['data'][0]['CLIENTID']){
                        $PROPERTY_SOLICITOR_ID=$solicitor_exists['data'][0]['CLIENTID'];
                    }
                   
                }

            }else{
                $landlord_name = $objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();
                $query_1="SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$landlord_name' AND CLIENT_TYPE LIKE 'LANDLORD' ";
                $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
                if(@$landlord_exists['data'][0]['CLIENTID']){
                    $PROPERTY_VENDOR_ID=$landlord_exists['data'][0]['CLIENTID'];
                   
                }

               
            }

            

          
            $get_instruct_date = $objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getformattedValue();
            
            if($get_instruct_date) { 

                $created_date = Utility::convert_tosqldate($get_instruct_date, 'd/m/y H:i');
                $INSTRUCTED_DATE = date('Y-m-d', strtotime($created_date));
                $INSTRUCTED_DATE="'".$INSTRUCTED_DATE."'";
            }
            else { 
                $INSTRUCTED_DATE='NULL';
            }

            
            $currentEnergyEfficiency = $objPHPExcel->getActiveSheet()->getCell('DU'.$row)->getValue();
            $potentialEnergyEfficiency = $objPHPExcel->getActiveSheet()->getCell('DV'.$row)->getValue();
            $currentEnvironmentImpact = $objPHPExcel->getActiveSheet()->getCell('DW'.$row)->getValue();
            $potentialEnvironmentImpact = $objPHPExcel->getActiveSheet()->getCell('DX'.$row)->getValue();
            if($currentEnergyEfficiency!=""){
            $PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$currentEnergyEfficiency, "energy_potential"=> $potentialEnergyEfficiency,
                  "co2_current"=>$currentEnvironmentImpact, "co2_potential"=> $potentialEnvironmentImpact));             
            }
          
           


            $PROPERTY_LETTING_SERVICE = '';
            $get_lettings_managed = $objPHPExcel->getActiveSheet()->getCell('V'.$row)->getformattedValue();
            if($get_lettings_managed) { 
                $PROPERTY_LETTING_SERVICE = '1';
            }
            

            $get_letting_fees = '';
            $get_letting_fees_percent = '';
            $get_management_fees =  '';
            $PROPERTY_LETTING_FEE_TYPE=$PROPERTY_PRICE_FREQUENCY;
            $PROPERTY_LETTING_FEE_FREQUENCY ='';
            $PROPERTY_MANAGEMENT_FEE_TYPE='';
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY='';
             
             

           
            $PROPERTY_LETTING_FEE = preg_replace("/[^0-9\.]/", '', $get_letting_fees);
            $PROPERTY_MANAGEMENT_FEE='';

            $CERTIFICATE_EXPIRE_DATE='';
            $get_epc_expiry_date = $objPHPExcel->getActiveSheet()->getCell('EA'.$row)->getformattedValue();
            if($get_epc_expiry_date!=""){
                $epc_expiry_date = Utility::convert_tosqldate($get_epc_expiry_date, 'd/m/y');
                $EXPIRY_DATE['4'] = date('Y-m-d', strtotime($epc_expiry_date));
                $CERTIFICATE_EXPIRE_DATE=json_encode($EXPIRY_DATE);
            }

          
            
           
            $extra_features = array();
            $extra_feature_1 = $objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue();
            if($extra_feature_1 !="" ){
                $extra_feature_1_value ="KITCHEN : ".$extra_feature_1;
                array_push($extra_features,$extra_feature_1_value);
            }
           

            $prop_sqft = $objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue();
            $prop_sqft2 = $objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue();
            if(($prop_sqft !="" && $prop_sqft!=0) || ($prop_sqft2 !="" && $prop_sqft2 !=0 )){
                $PROPERTY_DESCRIPTION.='\n Area Square feet: '.(($prop_sqft)>0 ? $prop_sqft : $prop_sqft2);
                $prop_sqft = "Area Square feet : ".(($prop_sqft)>0 ? $prop_sqft : $prop_sqft2);
                array_push($extra_features,$prop_sqft);
            }

            if(!empty($extra_features)){
                $PROPERTY_CUSTOM_FEATURES=addslashes(json_encode($extra_features));
            }


            $PROPERTY_ROOMS='';
            if($PROPERTY_REF_ID !=""){

                $room_size_qry = "select * from property_rooms where property_id = '$PROPERTY_REF_ID'";
                $room_details = json_decode($db_connect->queryFetch($room_size_qry),true);
                $rooms=[];
                foreach($room_details['data'] as $room_row){
                    $rooms[] = array("room_name"=>addslashes($room_row['property_room_title']),
                     "room_size"=>stripslashes(addslashes($room_row['property_room_size'])), 
                    "room_desc"=>addslashes($room_row['property_room_description']),
                   );
                }
              $PROPERTY_ROOMS=addslashes(json_encode($rooms));
             
            }

           
    
           // if($get_property_status == 'Appraisal' || $get_property_status == 'Potential Vendor') {
            if($get_property_status == 'Promotion' || $get_property_status == 'Valuation') {


            $PROPERTY_APPOINTMENT_STARTTIME="NULL";
            $PROPERTY_APPOINTMENT_ENDTIME="NULL";
            $PROPERTY_AGE='';
            $PROPERTY_CONDITION='';
            $PROPERTY_PRICE_FROM='0';
            $PROPERTY_VENDOR_PRICE='';
            $PROPERTY_PROPOSED_PRICE='';
            $PROPERTY_VALUER_NOTES='';
            $PROPERTY_VALUER_NOTES_1='';
            $PROPERTY_VALUER_NOTES_2='';
            $PROPERTY_VALUER_NOTES_3='';
            $PROPERTY_VALUER_NOTES_4='';
            $PROPERTY_VALUER_NOTES_5='';

             $sql="INSERT INTO `valuations`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`,
            `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`,
            `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`,
            `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`,
            `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`,
            `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`) VALUES
            ('$PROPERTY_ID','$PROPERTY_REF_ID','$PROPERTY_VENDOR_ID','$PROPERTY_CATEGORY','$PROPERTY_ADDRESS_LINE_1','$PROPERTY_ADDRESS_LINE_2',
            '$PROPERTY_ADDRESS_CITY','$PROPERTY_ADDRESS_COUNTY','$PROPERTY_ADDRESS_POSTCODE','$PROPERTY_STATUS','$PROPERTY_TYPE',
            '$PROPERTY_BEDROOMS','$PROPERTY_BATHROOMS','$PROPERTY_RECEPTION','$PROPERTY_TENURE','$PROPERTY_AGE','$PROPERTY_CLASSIFICATION',
            '$PROPERTY_CONDITION','$PROPERTY_PRICE_FROM','$PROPERTY_PRICE','$PROPERTY_VENDOR_PRICE','$PROPERTY_PROPOSED_PRICE',
            '$PROPERTY_VALUER_NOTES','$PROPERTY_VALUER_NOTES_1','$PROPERTY_VALUER_NOTES_2','$PROPERTY_VALUER_NOTES_3','$PROPERTY_VALUER_NOTES_4',
            '$PROPERTY_VALUER_NOTES_5',$PROPERTY_APPOINTMENT_STARTTIME,$PROPERTY_APPOINTMENT_ENDTIME)";

             $db_connect->queryExecute($sql) or die($sql);

                /* $sql ="INSERT INTO `valuations` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
                `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
                `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
                `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
                `KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
                `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
                `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
                `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
                `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
                `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
                `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
                `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
                `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
                `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
                `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
                 VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
                '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
                '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
                '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
                '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)";  */
            }
            else { 
                 $sql1 ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_VENDOR_SOLICITOR_ID`,`PROPERTY_TITLE`,
                `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
                `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
                `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
                `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
                `KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
                `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
                `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
                `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
                `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
                `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
                `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
                `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
                `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
                `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
                `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
                 VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID','$PROPERTY_SOLICITOR_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
                '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
                '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
                '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
                '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)"; 
                 $db_connect->queryExecute($sql1) or die($sql1);
            }

           

                //insert into table    
               

        }
    }
}
?>