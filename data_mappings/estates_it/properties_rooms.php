<?php
error_reporting(E_ERROR);

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\data_migration_bkup\keystone\ROOMS.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        $Propertyid=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
        $room_order=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        $room_type=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $room_title=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        $room_description=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $room_size=($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
        $room_sizes=($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
        $room_meter1=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $room_meter2=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
        $room_feet1=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
        $room_inch1=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
        $room_feet2=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $room_inch2=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
        $photofile=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();

$sql3 ="INSERT INTO `property_room`(`Propertyid`, `room_order`, `room_type`, `room_title`,
 `room_description`, `room_size`, `room_sizes`, `room_meter1`, `room_meter2`, `room_feet1`, `room_inch1`, `room_feet2`, `room_inch2`, `photofile`) VALUES 
('$Propertyid', '$room_order', '$room_type', '$room_title', '$room_description', '$room_size', '$room_sizes', '$room_meter1', '$room_meter2', '$room_feet1', '$room_inch1', '$room_feet2', '$room_inch2', '$photofile')";
$db_connect->queryExecute($sql3) or die($sql3);



        }
    }
}
?>