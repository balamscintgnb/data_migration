<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\data_migration_bkup\keystone/DIARY.xls'; 
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);



//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
    
            $type =	$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
            $applicant_id	= preg_replace( '/[^[:print:]]/', '',$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            if($type !="Offer" || $applicant_id==""){
              continue;
            }

            $ID = $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
            $APPLICANT_ID='';
            $client_id_qry = "SELECT CLIENTID FROM clients WHERE `CLIENTID` LIKE '$applicant_id'";
            $cliend_id_detail = json_decode($db_connect->queryFetch($client_id_qry),true);

            if($cliend_id_detail['data'][0]['CLIENTID']){
              $APPLICANT_ID = $cliend_id_detail['data'][0]['CLIENTID'];
            }else{
              continue;
            }

            $SOLICITOR_ID='';
            $get_category_qry = "SELECT 	PROPERTY_CATEGORY FROM properties where PROPERTY_ID = '$PROPERTY_ID'";
            $get_category_result = json_decode($db_connect->queryFetch($get_category_qry),true);
            $CATEGORY_ID=0;
            if(isset($get_category_result['data'][0]['PROPERTY_CATEGORY']) && stristr($get_category_result['data'][0]['PROPERTY_CATEGORY'],'SALE')){
              $CATEGORY_ID=1;
              $SOLICITOR_ID = $objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            }
        


            $AMOUNT=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            $PROPERTY_PRICE_FREQUENCY='';
            if($CATEGORY_ID==0){
              $frequency_qry = "SELECT 	LETTING_RENT_FREQUENCY FROM lettings where PROPERTY_ID = '$PROPERTY_ID' and TENANT_ID='$APPLICANT_ID' or PROPERTY_ID = '$PROPERTY_ID' limit 1 ";
              $frequency_result = json_decode($db_connect->queryFetch($frequency_qry),true);
              if(isset($frequency_result['data'][0]['LETTING_RENT_FREQUENCY'])){
                $PROPERTY_PRICE_FREQUENCY=$frequency_result['data'][0]['LETTING_RENT_FREQUENCY'];
              }else{
                continue;
              }
            }

            $cancelled_status = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            $completed_status = $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
            if($cancelled_status==1){
              $Status = 2;
            }elseif($completed_status == 1){
              $Status = 1;
            }else{
              $Status = 0;
            }
           

            $get_offer_date = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getformattedValue();
            if($get_offer_date!=""){
              $offer_date = Utility::convert_tosqldatetime($get_offer_date, 'd/m/y H:i');
              $OFFER_DATE = date('Y-m-d H:i', strtotime($offer_date));
            }else{
              $OFFER_DATE = "0000-00-00";
            }

	
            if($CATEGORY_ID==1){
               $sql = "INSERT INTO `sale_offers` (`SALE_OFFER_ID`, `SALE_PROPERTY_ID`, `PROPERTY_APPLICANT_ID`,
              `PROPERTY_SALE_OFFER_PRICE`,`PROPERTY_SALE_SOLICITOR_ID`,`PROPERTY_SALE_OFFER_STATUS`,`PROPERTY_SALE_OFFER_DATETIME`)
              VALUES ('$ID', '$PROPERTY_ID', '$APPLICANT_ID', '$AMOUNT','$SOLICITOR_ID','$Status','$OFFER_DATE')";
            }else{
               $sql = "INSERT INTO `letting_offers` (`PROPERTY_LETTING_OFFER_ID`, `PROPERTY_ID`, `PROPERTY_APPLICANT_ID`, `PROPERTY_LETTING_OFFER_RENT`,
              `PROPERTY_LETTING_OFFER_RENT_FREQUENCY`, `PROPERTY_LETTING_OFFER_STATUS`, `PROPERTY_LETTING_OFFER_DATETIME`)
              VALUES ('$ID', '$PROPERTY_ID', '$APPLICANT_ID', '$AMOUNT', '$PROPERTY_PRICE_FREQUENCY', '$Status', '$OFFER_DATE');";
            }



            $db_connect->queryExecute($sql);
              
            
           

		  }
  }
  
}


?>
