<?php
include '../datamapping_config.php';
include 'db_names.php';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);

if(isset($_GET['p']) && $_GET['p']!=''){ 
  $p=$_GET['p'];
  $page = $p*100;
}else { 
  $p=0;
  $page=0;
}
if(@$_GET['clear']==1){ 
$query = $db1->execute("TRUNCATE TABLE `maintenance_jobs`");  
}

// contactpropertyrequirements
$query = $db2->execute("SELECT w.`ID` as workid,w.`ContractorID` as supplier_id,w.`PropertyID`,w.`WorkOrderTypeID`,w.`Subject`,
w.`Body`,w.`DueDate` as job_on,w.`Amount` as cost,w.`ContactID` as paid_by,w.`CompletedDate` as completed_date,
`b`.Amount as total_amount,GROUP_CONCAT(t.`ContactID`) as pay_user_id,GROUP_CONCAT(t.`Debit`) as Debit_amount,
GROUP_CONCAT(t.`Credit`) as Credit_amount from `workorders` as w left join `workordertypes` as wt on w.WorkOrderTypeID=wt.ID 
left join `bills` as b on w.ID=b.WorkOrderID left join `transactions` as t on b.ID=t.BillID group by `w`.ID LIMIT $page,100");

if($query->num_rows>0){
while($row = $query->fetch_array()){

$MAINTENANCE_CATEGORY='';
$REPORT_PROBLEM_ID=1;
$JOB_REF_NO = $row['workid'];
$SUPPLIER_ID = 'SUPP_'.$row['supplier_id'];
$PROPERTY_ID = 'LEBALI_00'.$row['PropertyID'];
$JOB_CATEGORY = $row['WorkOrderTypeID'];

if($JOB_CATEGORY=="1"){
   $MAINTENANCE_CATEGORY='24';
}else if($JOB_CATEGORY=="2"){
   $MAINTENANCE_CATEGORY='11';
}else if($JOB_CATEGORY=="4"){
   $MAINTENANCE_CATEGORY='2';
}else if($JOB_CATEGORY=="5"){
   $MAINTENANCE_CATEGORY='4';
}else if($JOB_CATEGORY=="7"){
   $MAINTENANCE_CATEGORY='6';
}else if($JOB_CATEGORY=="8"){
   $MAINTENANCE_CATEGORY='7';
}else if($JOB_CATEGORY=="9"){
   $MAINTENANCE_CATEGORY='8';
}else if($JOB_CATEGORY=="14"){
   $MAINTENANCE_CATEGORY='16';
}else if($JOB_CATEGORY=="17"){
   $MAINTENANCE_CATEGORY='19';
}else if($JOB_CATEGORY=="20"){
   $MAINTENANCE_CATEGORY='22';
}else if($JOB_CATEGORY=="21"){
   $MAINTENANCE_CATEGORY='23';
}

$JOB_DESCRIPTION= trim($row['Subject'].' '.$row['Body']);

if($JOB_DESCRIPTION==''){
  $JOB_DESCRIPTION.='Maintenance';
}

$JOB_ON = date('Y-m-d', strtotime($row['job_on']));
$JOB_PRIORITY='';
$JOB_ESTIMATED_COST=$row['cost'];
$JOB_WILL_BE_PAID_BY='APP_'.$row['paid_by'];
$JOB_CONTRACTOR_INVOICE_FILE='';
$JOB_PAYABLE_USER_ID=$row['pay_user_id'];
$JOB_NOTES='';
$JOB_REQUEST_DATE=$JOB_CREATED_ON=date('Y-m-d', strtotime($row['job_on']));
$JOB_COMPLETED_ON=$row['completed_date'];

$JOB_INVOICE_AMT_CONTRACTOR=$row['total_amount']??0;

$DEBIT =  $row['Debit_amount'];
$CREDIT = $row['Credit_amount'];

$sql = "INSERT INTO `maintenance_jobs`(`JOB_REF_NO`, `REPORT_PROBLEM_ID`, `SUPPLIER_ID`, `PROPERTY_ID`,
`JOB_CATEGORY`, `JOB_DESCRIPTION`, `JOB_ON`,  `JOB_PRIORITY`, `JOB_ESTIMATED_COST`,`JOB_INVOICE_AMT_CONTRACTOR`, `JOB_WILL_BE_PAID_BY`, `JOB_PAYABLE_USER_ID`,
`JOB_PAYABLE_STATUS`, `JOB_PAYABLE_AMOUNT`, `JOB_PAYABLE_PAID_AMOUNT`, `JOB_REQUEST_DATE`,  `JOB_STATUS`, `JOB_CREATED_ON`,
`JOB_COMPLETED_ON`) VALUES ('$JOB_REF_NO','$REPORT_PROBLEM_ID', '$SUPPLIER_ID','$PROPERTY_ID','$MAINTENANCE_CATEGORY','$JOB_DESCRIPTION','$JOB_ON', '0',
'$JOB_ESTIMATED_COST','$JOB_INVOICE_AMT_CONTRACTOR', '$JOB_WILL_BE_PAID_BY','$JOB_PAYABLE_USER_ID','0','$DEBIT', '$CREDIT','$JOB_REQUEST_DATE',
'0', '$JOB_CREATED_ON','$JOB_COMPLETED_ON')";

$db1->execute($sql) or die($sql);

}

}
?>
