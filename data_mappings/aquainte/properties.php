<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);
include '../datamapping_config.php';
include 'db_names.php';

	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);


if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*100;
}
else{
	$p=0;
	$page = 0;
}


$query = "select *, pt.Description as property_type,p.ID as property_id,p.Description as short_desc from `properties` as p
left join `propertytypes` as pt on p.Type=pt.ID limit $page,100";


$query_data = $db2->execute($query);
$query_data_1 = $db2->execute($query);

$property_array = array();
while($row3 = $query_data->fetch_array()){
	$property_array[] = $row3['property_id'];
}

$rooms	= array();

if(sizeof($property_array)>0){

	$property_ids = implode(',', $property_array);
    $room_rows 	= $db2->execute("SELECT * FROM `propertyrooms` WHERE `PropertyID` IN ($property_ids)");
	//$room_rows	= $rooms_obj->rows;
    $rooms		= array();
    
	while($room_row = $room_rows->fetch_array()){

		$rooms[$room_row['PropertyID']][] = array("room_name"=>$room_row['Description'], "room_size"=>'', "room_desc"=>$room_row['FullDescription'], "room_width"=>$room_row['Width'], "room_length"=>$room_row['Length']);
	}

}

if($query_data_1->num_rows>0){
while($row = $query_data_1->fetch_array()){


		$PROPERTY_ID				= 'LEBALI_00'.$row['property_id'];

		$PROPERTY_REF_ID			= 'LEBALI_00'.$row['property_id'];


		$PROPERTY_STAFF_ID			= 0;

		$PROPERTY_TITLE				= '';
		$PROPERTY_SHORT_DESCRIPTION	= $row['short_desc'];
		$PROPERTY_DESCRIPTION		= $row['FullDescription'];

		$PROPERTY_NOTES		= $row['Notes'];

		$PROPERTY_CONTACT_ID		= $row['ContactID'];

		$PROPERTY_USER_ID		= $row['UserID'];

		$PROPERTY_CLASSIFICATION	= '';

		$PROPERTY_CATEGORY		= $row['Category'];

		$PROPERTY_PRICE				= $row['Price'];
		$PROPERTY_QUALIFIER			= '';
		$PROPERTY_PRICE_FREQUENCY	= '';
		$FURNISHING='';

		$rent_frequency				= '';
		//if($PROPERTY_PRICE>0){
			$rent_frequency 		= $row['PriceFrequency'];
		//}

		if($PROPERTY_CATEGORY==1){
			$PROPERTY_CATEGORY="RESIDENTIAL LETTINGS";
			$PROPERTY_VENDOR_ID			= 'LAN_'.$row['ContactID'];

		}else{
			$PROPERTY_CATEGORY="RESIDENTIAL SALES";
			$PROPERTY_VENDOR_ID			= 'VEN_'.$row['ContactID'];
		}

		/*$rent_frequency 		= $item_array[$row['PANTRT']];
		$rent_frequency 		= $item_array[$row['PLETPCT']];
		$rent_frequency 		= $item_array[$row['PLETFEE']];*/

		if($PROPERTY_TITLE!=''){
			if($PROPERTY_DESCRIPTION!=''){
				$PROPERTY_DESCRIPTION.='\n';
			}
			$PROPERTY_DESCRIPTION.=$PROPERTY_TITLE;
		}


		if($rent_frequency=='0'){
			$PROPERTY_PRICE_FREQUENCY	= 'PCM';
		}else if($rent_frequency=='1'){
			$PROPERTY_PRICE_FREQUENCY	= 'PW';
		}

		$PROPERTY_AVAILABLE_DATE	= date('Y-m-d', strtotime($row['RentalAvailableDate']));

		$PROPERTY_ADDRESS_LINE_1	= '';
		$PROPERTY_ADDRESS_LINE_2	= '';
		$PROPERTY_ADDRESS_CITY		= '';
		$PROPERTY_ADDRESS_COUNTY	= '';
		$PROPERTY_ADDRESS_POSTCODE	= '';


		$housename			= trim($row['PropertyName']);
		$housenumber		= '';
		$street				= trim($row['Street']);
		$locality			= trim($row['Locality']);
		$area				= $row['PropertyAreaID'];
		$town				= trim($row['Town']);
		$county				= trim($row['Region']);
		$postcode			= trim($row['Postcode']);

		if($housename!=''){
			$PROPERTY_ADDRESS_LINE_1.=$housename;
		}

		if($housenumber!=''){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$housenumber;
		}

		if($street!=''){
			$PROPERTY_ADDRESS_LINE_2.=$street;
		}

		if($locality!=''){
			$PROPERTY_ADDRESS_CITY.=$locality;
		}

		if($area!=''){
			if($PROPERTY_ADDRESS_CITY!=""){
				$PROPERTY_ADDRESS_CITY.=', ';
			}
			$PROPERTY_ADDRESS_CITY.=$area;
		}

		if($town!=''){
			$PROPERTY_ADDRESS_COUNTY.=$town;
		}

		if($county!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$county;
		}

		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}

		$PROPERTY_ADDRESS_LINE_1		= $PROPERTY_ADDRESS_LINE_1;
		$PROPERTY_ADDRESS_LINE_2		= $PROPERTY_ADDRESS_LINE_2;
		$PROPERTY_ADDRESS_CITY			= $PROPERTY_ADDRESS_CITY;
		$PROPERTY_ADDRESS_COUNTY		= $PROPERTY_ADDRESS_COUNTY;
		$PROPERTY_ADDRESS_POSTCODE		= $PROPERTY_ADDRESS_POSTCODE;

		$PROPERTY_STATUS			= '';  //ARCHIVED

		$PROPERTY_ADMIN_FEES		= '';

		if($row['CommissionFee']>0){
			$PROPERTY_ADMIN_FEES		= $row['CommissionFee'];
		}

		$PROPERTY_TYPE				= $row['property_type'];
		$PROPERTY_BEDROOMS			= $row['Bedrooms'];
		$PROPERTY_BATHROOMS			= $row['Bathrooms'];
		$PROPERTY_RECEPTION			= $row['ReceptionRooms'];
		$PROPERTY_TENURE			= $row['Tenure'];

		$KITCHEN_DINER				= 'FALSE';
		$OFF_ROAD_PARKING			= ($row['ParkingOffStreet']==1)?'TRUE':'FALSE';
		$ON_ROAD_PARKING			= 'FALSE';
		$GARDEN						= ($row['Gardens']==1)?'TRUE':'FALSE';
		$WHEELCHAIR_ACCESS			= 'FALSE';
		$ELEVATOR_IN_BUILDING		= ($row['Lift']==1)?'TRUE':'FALSE';
		$POOL						= 'FALSE';
		$GYM						= 'FALSE';
		$KITCHEN					= 'FALSE';
		$DINING_ROOM				= 'FALSE';
		$FURNISHED					= 'FALSE';
		$INTERNET					= 'FALSE';
		$WIRELESS_INTERNET			= 'FALSE';
		$TV							= 'FALSE';
		$WASHER						= 'FALSE';
		$DRYER						= 'FALSE';
		$DISHWASHER					= 'FALSE';
		$PETS_ALLOWED				= 'FALSE';
		$FAMILY_OR_CHILD_FRIENDLY	= 'FALSE';
		$DSS_ALLOWED				= 'FALSE';
		$SMOKING_ALLOWED			= 'FALSE';
		$SECURITY					= 'FALSE';
		$HOT_TUB					= 'FALSE';
		$CLEANER					= 'FALSE';
		$EN_SUITE					= 'FALSE';
		$SECURE_CAR_PARKING			= 'FALSE';
		$OPEN_PLAN_LOUNGE			= 'FALSE';
		$VIDEO_DOOR_ENTRY			= 'FALSE';
		$CONCIERGE_SERVICES			= 'FALSE';

		$features_array				= array();

		$features_array = explode('*',$row['BulletPoints']);

		$PROPERTY_AVAILABILITY = $row['Status'];

		$EPC_EXPIRY_DATE=date('Y-m-d', strtotime(trim($row["EPCExpiryDate"])));

		$LATITUDE=trim($row["Latitude"]);

		$LONGITUDE=trim($row["Longitude"]);

		$PROPERTY_FORMATTED_ADDRESS=trim($row["DisplayAddress"]);

		

		if($PROPERTY_NOTES!=''){
			$PROPERTY_DESCRIPTION.='\n'.$PROPERTY_NOTES;
		}


			$FURNISHING_STATUS='';


			if($PROPERTY_AVAILABILITY=="0" || $PROPERTY_AVAILABILITY==4){
				$PROPERTY_AVAILABILITY	= 'AVAILABLE';
			}

			if($PROPERTY_AVAILABILITY==1){
				$PROPERTY_AVAILABILITY	= 'Under Offer';
			}

			if($PROPERTY_AVAILABILITY==2 || $PROPERTY_AVAILABILITY==3){
				$PROPERTY_AVAILABILITY	= 'Withdrawn';
			}

			if($PROPERTY_AVAILABILITY==5){
				$PROPERTY_AVAILABILITY	= '';
			}


			if($PROPERTY_TENURE==0){
					$PROPERTY_TENURE='Freehold';
			}else if($PROPERTY_TENURE==1){
					$PROPERTY_TENURE='Leasehold';
			}else if($PROPERTY_TENURE==2){
					$PROPERTY_TENURE='Freehold';
			}


    /*$FURNISHING=$row['Match3'];

		$FURNISHING_STATUS='';

		if(stristr($FURNISHING,'WHITE GOODS') || stristr($FURNISHING,'YES')){
			 $FURNISHING_STATUS					= 'Furnished';
		}else if(stristr($FURNISHING,'NO') || stristr($FURNISHING,'Unknown')){
				$FURNISHING_STATUS					= 'Unfurnished';
		}else if(stristr($FURNISHING,'PART')){
				$FURNISHING_STATUS					= 'Part Furnished';
		}else{
				$FURNISHING_STATUS='';
		}*/



		if($row['Gardens']==1){
			$GARDEN					= 'TRUE';
		}

		if($PROPERTY_TYPE=='' && $row['Land']==1){
				$PROPERTY_TYPE='Land';
		}

		if($PROPERTY_TYPE=='' && $row['Garage']==1){
				$PROPERTY_TYPE='Garage';
				array_push($features_array,"Garage");
		}

		if($row['ParkingOffStreet']==1){
			$OFF_ROAD_PARKING		= 'TRUE';
		}

		if($row['DoubleGlazing']==1){
			array_push($features_array, "Double Gazing");
		}

		if($row['CentralHeating']==1){
			array_push($features_array,"Central Heating");
		}

		if($row['Gas']==1){
			array_push($features_array,"Gas");
		}

		if($row['Electric']==1){
			array_push($features_array,"Electric");
		}

		if($row['Water']==1){
			array_push($features_array,"Water");
		}

		///

		if(array_search('pool', $features_array)){
			$POOL					= 'TRUE';
		}

		if(array_search('concierge', $features_array)){
			$CONCIERGE_SERVICES		= 'TRUE';
		}

		if(array_search('garden', $features_array)){
			$GARDEN					= 'TRUE';
		}

		if(array_search('lift', $features_array)){
			$ELEVATOR_IN_BUILDING	= 'TRUE';
		}

		if(array_search('security', $features_array)){
			$SECURITY				= 'TRUE';
		}

		if(array_search('gym', $features_array)){
			$GYM				= 'GYM';
		}

		if(array_search('internet', $features_array)){
			$INTERNET = 'TRUE';
		}

		if(array_search('parking', $features_array)){
			$OFF_ROAD_PARKING		= 'TRUE';
		}

		if(array_search('EN-SUITE', $features_array)){
			$EN_SUITE		= 'TRUE';
		}

		$PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$row['EERCurrent'], "energy_potential"=> $row['EERPotential'], "co2_current"=>$row['EIRCurrent'], "co2_potential"=> $row['EIRPotential']));

		//$PROPERTY_ROOMS				= $rooms[$PROPERTY_ID]:array();

		$PROPERTY_ASSETS			= json_encode(array());
		$PROPERTY_CUSTOM_FEATURES	= json_encode($features_array);
		$PROPERTY_ROOMS				= addslashes(json_encode($rooms[$row['property_id']]));

		$PROPERTY_CREATED_ON		= date('Y-m-d H:i:s', strtotime($row['AddedDate']));

		$sql = "INSERT INTO properties (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHING_STATUS', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON')";

		$db1->execute($sql) or die($sql);

	
	}
	echo $p;
	?>
	<script>
	window.location="properties.php?p=<?= $p+1;?>";
	</script>
	<?php
	}
	
	
	else { 
		echo "END ";
	}
	?>
	