<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = "SELECT p.*,a.`ID` as appraisal_id,a.`StartDate`,a.`EndDate`,a.`Notes` as appointment_notes,a.`Confirmed`,a.`FollowedUpDate`,ap.`PropertyID`,GROUP_CONCAT(ac.`ContactID`)
 as contacts_id,p.`ID`,pt.`Description` as property_type,p.`ID` as property_id  FROM `appointments` as a
 left join `appointmentcontacts` as ac on a.ID=ac.AppointmentID
 left join `appointmentproperties` as ap on a.ID=ap.AppointmentID
 left join `properties` as p on ap.`PropertyID`=p.ID
 left join `propertytypes` as pt on p.Type=pt.ID WHERE a.`AppointmentTypeID` IN (4) group by a.`ID`";

$data = json_decode($db_connect_src->queryFetch($query),true);

echo '<pre>';
print_r($data);
echo '</pre>';

exit;

if(count($data)>0){
		foreach($data['data'] as $row){

			$PROPERTY_CLASSIFICATION	= '';

			$PROPERTY_REF_ID			= $row['appraisal_id'];
			$PROPERTY_VENDOR_ID			= $row['contacts_id'];
			$PROPERTY_CATEGORY			= $row['Category'];

			if($PROPERTY_CATEGORY==1){
				$PROPERTY_CATEGORY="RESIDENTIAL LETTINGS";
			}else{
				$PROPERTY_CATEGORY="RESIDENTIAL SALES";
			}

			$PROPERTY_ADDRESS_LINE_1	= '';
			$PROPERTY_ADDRESS_LINE_2	= '';
			$PROPERTY_ADDRESS_CITY		= '';
			$PROPERTY_ADDRESS_COUNTY	= '';
			$PROPERTY_ADDRESS_POSTCODE	= '';


			$housename			= trim($row['PropertyName']);
			$housenumber		= '';
			$street				= trim($row['Street']);
			$locality			= trim($row['Locality']);
			$area				= $area_array[$row['PropertyAreaID']];
			$town				= trim($row['Town']);
			$county				= trim($row['Region']);
			$postcode			= trim($row['Postcode']);

			if($housename!=''){
				$PROPERTY_ADDRESS_LINE_1.=$housename;
			}

			if($housenumber!=''){
				if($PROPERTY_ADDRESS_LINE_1!=""){
					$PROPERTY_ADDRESS_LINE_1.=', ';
				}
				$PROPERTY_ADDRESS_LINE_1.=$housenumber;
			}

			if($street!=''){
				$PROPERTY_ADDRESS_LINE_2.=$street;
			}

			if($locality!=''){
				$PROPERTY_ADDRESS_CITY.=$locality;
			}

			if($area!=''){
				if($PROPERTY_ADDRESS_CITY!=""){
					$PROPERTY_ADDRESS_CITY.=', ';
				}
				$PROPERTY_ADDRESS_CITY.=$area;
			}

			if($town!=''){
				$PROPERTY_ADDRESS_COUNTY.=$town;
			}

			if($county!=''){
				if($PROPERTY_ADDRESS_COUNTY!=""){
					$PROPERTY_ADDRESS_COUNTY.=', ';
				}
				$PROPERTY_ADDRESS_COUNTY.=$county;
			}

			if($postcode!=''){
				$PROPERTY_ADDRESS_POSTCODE.=$postcode;
			}

			$PROPERTY_ADDRESS_LINE_1		= $PROPERTY_ADDRESS_LINE_1;
			$PROPERTY_ADDRESS_LINE_2		= $PROPERTY_ADDRESS_LINE_2;
			$PROPERTY_ADDRESS_CITY			= $PROPERTY_ADDRESS_CITY;
			$PROPERTY_ADDRESS_COUNTY		= $PROPERTY_ADDRESS_COUNTY;
			$PROPERTY_ADDRESS_POSTCODE		= $PROPERTY_ADDRESS_POSTCODE;

			$PROPERTY_TYPE				= $row['property_type'];
			$PROPERTY_BEDROOMS			= $row['Bedrooms'];
			$PROPERTY_BATHROOMS			= $row['Bathrooms'];
			$PROPERTY_RECEPTION			= $row['ReceptionRooms'];

			$PROPERTY_VALUER_NOTES= $row['Description'];
			$PROPERTY_PRICE_FROM        = $row['PriceMatch'];
			$PROPERTY_PRICE_TO          = $row['Price'];
			$PROPERTY_VENDOR_PRICE      = '';
			$PROPERTY_PROPOSED_PRICE    = $row['SoldPrice'];

			$PROPERTY_AGE               = '';
			if($row['Age']>0)
			$PROPERTY_AGE               = $row['Age'];

			$PROPERTY_CURRENT_OCCUPANT	= '';

			$PROPERTY_APPOINTMENT_STARTTIME=$row['StartDate'];
			$PROPERTY_APPOINTMENT_ENDTIME=$row['EndDate'];
			$PROPERTY_VALUER_NOTES_1=htmlentities($row['appointment_notes']);

			$PROPERTY_STATUS=0;

			if($row['Confirmed']==1){
	        $PROPERTY_STATUS=1;
	    }else{
	        $PROPERTY_STATUS=0;
	    }

			$PROPERTY_ACTIVE			= '0';

			$PROPERTY_FOLLOW_UP_DATE='';

			if($row['FollowedUpDate']==''){
					$PROPERTY_FOLLOW_UP_DATE=$row['FollowedUpDate'];
			}


			$sql = "INSERT INTO `valuations` (`PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`,
			 `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`,
			 `PROPERTY_RECEPTION`, `PROPERTY_AGE`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`,
			 `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`, `PROPERTY_VALUER_NOTES_1`, `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`
			 ,`PROPERTY_APPOINTMENT_FOLLOWUPDATE`)
			 VALUES ('$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_CATEGORY', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY',
				  '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_STATUS', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION',
					 '$PROPERTY_AGE', '$PROPERTY_PRICE_FROM', '$PROPERTY_PRICE_TO', '', '$PROPERTY_PROPOSED_PRICE', '$PROPERTY_VALUER_NOTES', '$PROPERTY_VALUER_NOTES_1',
					 '$PROPERTY_APPOINTMENT_STARTTIME', '$PROPERTY_APPOINTMENT_ENDTIME','$PROPERTY_FOLLOW_UP_DATE')";

			 $db_connect->queryExecute($sql);

		}
}


?>
