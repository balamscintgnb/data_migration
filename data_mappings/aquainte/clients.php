<?php 

include '../datamapping_config.php';
include 'db_names.php';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);

if($_GET['clear']==1){
	$db1->execute('TRUNCATE TABLE `clients`');
}

if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*100;
}
else{
	$p=0;
	$page = 0;
}


$query = $db2->execute("SELECT c.*, t.Description as ctitle,ty.Description as ctype FROM `contacts` as c 
inner join `titles` as t on t.ID=c.TitleID 
inner join `types` as ty on ty.ID=c.TypeID
limit $page,100");


if($query->num_rows>0){
while($row = $query->fetch_array()){
	if($row['DateAdded'])
	$CLIENT_CREATED_ON 		= $row['DateAdded'];
	else 
	$CLIENT_CREATED_ON = date('Y-m-d');

	$CLIENT_TITLE			= $row['ctitle'];
	$CLIENT_NAME			= $row['FirstNames'].' '.$row['LastName'];

	$CLIENT_STAFF_ID		= $row['UserID'];

	$CLIENT_SUB_TYPE		= '';

	$CLIENT_TYPE			='';

	$row_ctype=strtoupper($row['ctype']);
	/* 
	277
ZPT PROSPECTS
131
TENANT
82
LANDLORD
40
SUPPLIER
34
CONVERTED LANDLORD
30
ADDRESSBOOK
25
VOID
3
PROSPECT
3
APPLICANT
3
GAS ENGINEER
1
VENDOR
1
MANAGING AGENT
1
CLEANER
1
PAINTER/DECORATOR
1
PROPERTY UPLOAD
1
CONVEYANCE PROVIDER
1
CONSULTANT
1
DEBT RECOVERY AGENT
1
AUCTION HOUSE
1

1
LODGER
*/

	if($row_ctype=='LOCAL AUTHORITY' || $row_ctype=='Utility Supplier' || $row_ctype=='Lock Smith' || $row_ctype=='Contractor' || $row_ctype=='Insurance Company' || $row_ctype=='Bank' ||  $row_ctype=='Company - System' || $row_ctype=='GAS ENGINEER' || $row_ctype=='CLEANER'
	|| $row_ctype=='ELECTRICAL ENGINEER' ){
		$CLIENT_TYPE			= 'SUPPLIER';
		$CLIENTID				= 'SUPP_'.$row['ID'];
		$CLIENT_SUB_TYPE		= $row_ctype;

	}

	else if($row_ctype=='USER' || $row_ctype=='GUARANTOR' || $row_ctype=='PURCHASER' || $row_ctype=='Other'){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENTID				= $row['ID'];

	}
	else if(stristr($row_ctype,'TENANT')){
		$CLIENT_TYPE			= 'TENANT';
		$CLIENTID				= 'TEN_'.$row['ID'];

	}

	else if(stristr($row_ctype,'Vendor')){
		$CLIENTID = 'VEN_'.$row['ID'];
		$CLIENT_TYPE			= 'VENDOR';
	}
	else if($row_ctype=='Prospect Landlord'  || $row_ctype=='Block Management Company' ||  $row_ctype=='Landlord' ||  $row_ctype=='Managing agent' || $row_ctype=='Estate Agent'  ||  $row_ctype=='Publication'){
		$CLIENTID = 'LAN_'.$row['ID'];
		$CLIENT_TYPE			= 'LANDLORD';
	}
	else if($row_ctype=='Applicant' || $row_ctype=='Referee' || $row_ctype=='Shutter' ){
		$CLIENTID = 'APP_'.$row['ID'];
		$CLIENT_TYPE			= 'APPLICANT';	
	}
	else if(stristr($row_ctype,'prospect')){ 
		$CLIENTID = 'BUY_'.$row['ID'];
		$CLIENT_TYPE			= 'BUYER';	
	}
	else {

		$CLIENT_TYPE			= strtoupper($row_ctype);
		$CLIENTID				= $row['ID'];

	}

	$CLIENT_STATUS = 'ACTIVE';
	if($row['StatusID']==2){
		$CLIENT_STATUS = 'INACTIVE';
	}

	$email1					= trim($row['Email']);


	$CLIENT_NOTES			= '';
	$CLIENT_PRIMARY_PHONE	= $row['MobileTelephone'];
	$CLIENT_PRIMARY_EMAIL	= $email1;


	$CLIENT_NOTES			= $email1.' ';
	

	$CLIENT_EMAIL_1			= trim($row['EmailAlt']);
	$CLIENT_EMAIL_2			= '';
	$CLIENT_EMAIL_3			= '';
	$CLIENT_EMAIL_4			= '';
	$CLIENT_EMAIL_5			= '';

	$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= '';
	$mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= '';
	$fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';

	$phone_2 				= $row['HomeTelephone'];
	$phone_3 				= $row['HomeTelephoneAlt'];
	$phone_4 				= $row['BusinessTelephone'];

	$fax_2=  $row['HomeFax'];
	$fax_3=  $row['BusinessFax'];


	$CLIENT_ADDRESS_LINE_1 	= ' ';
	$CLIENT_ADDRESS_LINE_2	= ' ';
	$CLIENT_ADDRESS_CITY	= ' ';
	$CLIENT_ADDRESS_TOWN	= ' ';
	$CLIENT_ADDRESS_POSTCODE= ' ';

	$housename			= trim($row['HomeProperty']);
	$street				= trim($row['HomeStreet']);
	$locality			= trim($row['HomeLocality']);
	$town				= trim($row['HomeTown']);
	$county				= trim($row['HomeRegion']);
	$postcode			= trim($row['HomePostcode']);

	if($housename!=''){
		$CLIENT_ADDRESS_LINE_1.=$housename;
	}

	

	if($street!=''){
		$CLIENT_ADDRESS_LINE_2.=$street;
	}

	if($locality!=''){
		$CLIENT_ADDRESS_CITY.=$locality;
	}



	if($town!=''){
		$CLIENT_ADDRESS_TOWN.=$town;
	}

	if($county!=''){
		if($CLIENT_ADDRESS_TOWN!=""){
			$CLIENT_ADDRESS_TOWN.=', ';
		}
		$CLIENT_ADDRESS_TOWN.=$county;
	}

	if($postcode!=''){
		$CLIENT_ADDRESS_POSTCODE.=$postcode;
	}

	$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_2;
	$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_1;
	$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
	$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
	$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

	$CLIENT_PHONE_1			= $phone_2;
	$CLIENT_PHONE_2			= $phone_3;
	$CLIENT_PHONE_3			= $phone_4;
	$CLIENT_PHONE_4			= $phone_5;
	$CLIENT_PHONE_5			= '';

	$CLIENT_MOBILE_1		= $mobile_2;
	$CLIENT_MOBILE_2		= $mobile_3;
	$CLIENT_MOBILE_3		= $mobile_4;
	$CLIENT_MOBILE_4		= $mobile_5;
	$CLIENT_MOBILE_5		= '';

	$CLIENT_FAX_1			= $fax_2;
	$CLIENT_FAX_2			= $fax_3;
	$CLIENT_FAX_3			= $fax_4;
	$CLIENT_FAX_4			= $fax_5;
	$CLIENT_FAX_5			= '';


	$CLIENT_ACCOUNT_NAME= ''; 
	$CLIENT_ACCOUNT_NO = '';
	$CLIENT_ACCOUNT_SORTCODE='';
	


	// Search Criteria

	
	// $contact_max_price=$row['MaxPrice'];
	// $contact_min_price=$row['MinPrice'];
	// $frequency_val=1;
	// $category=44;
	// $location='';
		

	// $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$property_type,
	// 'property_applicant_search_attribute_2_from'=>$row['Bedrooms'], 
	// 'property_applicant_search_attribute_2_to'=>$row['MaxBedrooms'],
	// 'property_applicant_search_attribute_3_from'=>$row['Bathrooms'], 
	// 'property_applicant_search_attribute_3_to'=>$row['Bathrooms'],
	// 'property_applicant_search_attribute_4_from'=>$row['ReceptionRooms'], 
	// 'property_applicant_search_attribute_4_to'=>$row['ReceptionRooms'],
	// 'property_applicant_search_attribute_6'=>$row['Parking'],
	// 'property_applicant_search_attribute_8'=>$row['Garden'],'property_applicant_search_attribute_22'=>$row['RentalPets']);
	
	// $search_criteria=array('price'=>$contact_max_price,'pricefrom'=>$contact_min_price,'filter_array'=>$applicant_search_criteria,
	// 'frequency'=>$frequency_val,'category'=>$category,'location'=>$location);
	

	$search = json_encode([]);



	$sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ACCOUNT_NAME`,`CLIENT_ACCOUNT_NO`,`CLIENT_ACCOUNT_SORTCODE`,`CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ACCOUNT_NAME','$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON','$search', '0')";
	$db1->execute($sql) or die($sql);
	
}
echo $p;
?>
<script>
window.location="clients.php?p=<?= $p+1;?>";
</script>
<?php
}


else { 
	echo "END ";
}
?>
