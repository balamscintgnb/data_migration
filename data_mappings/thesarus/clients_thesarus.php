<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/excess/ApplicantContacts.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();


//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

/*echo '<pre><code>';
			print_r($sheet);
			echo '</code></pre>';*/

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.
						$CLIENT_ADDRESS_LINE_1='';

            $CLIENTID=addslashes($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue());
            $CLIENT_TITLE=addslashes($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            $CLIENT_NAME=addslashes($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
            $COMPANY_NAME=addslashes($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
            $CLIENT_TYPE='applicant';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            $CLIENT_PRIMARY_PHONE=addslashes($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());

						$CLIENT_ADDRESS_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            $CLIENT_ADDRESS_CITY=addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
            $CLIENT_ADDRESS_TOWN=addslashes($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
            $CLIENT_ADDRESS_POSTCODE=addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
            $CLIENT_PHONE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';

						$NOTES=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();

						$CLIENT_NOTES='';

						if($NOTES!=''){
            	$CLIENT_NOTES='Status : '.$NOTES;
						}


            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';


						if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
							if($CLIENT_NAME!=''){
								$CLIENT_NAME.=' ';
							}
							$CLIENT_NAME.='('.$COMPANY_NAME.')';
						}

						$CLIENT_NAME=addslashes($CLIENT_NAME);

						/*$SOLICITOR			= $objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();

						if($SOLICITOR!=''){
							if($CLIENT_NOTES!=''){
								$CLIENT_NOTES.='\n';
							}
							$CLIENT_NOTES.=$SOLICITOR;
						}

						$SOLICITOR_CONTACT			= $objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();

						if($SOLICITOR_CONTACT!=''){
							if($CLIENT_NOTES!=''){
								$CLIENT_NOTES.='\n';
							}
							$CLIENT_NOTES.=$SOLICITOR_CONTACT;
						}

						$SOLICITOR_PHONE			= $objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();

						if($SOLICITOR_PHONE!=''){
							if($CLIENT_NOTES!=''){
								$CLIENT_NOTES.='\n';
							}
							$CLIENT_NOTES.=$SOLICITOR_PHONE;
						}*/


						$CLIENT_NOTES=addslashes($CLIENT_NOTES);

						$SEARCH_CRITERIA = '';


						/*echo '<pre><code>';
						print_r();
						echo '</code></pre>';*/


						echo $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";

						$db_connect->queryExecute($sql);

			        }
			    }
}
?>
