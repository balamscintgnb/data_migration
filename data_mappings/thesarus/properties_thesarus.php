<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/excess/Instructions.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


        $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
        $PROPERTY_VENDOR_ID='';
        $PROPERTY_STAFF_ID='';
        $PROPERTY_TITLE='';
        $PROPERTY_SHORT_DESCRIPTION='';
        $PROPERTY_DESCRIPTION='';

		$PROPERTY_CATEGORY=43;


        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY='';

				$PROPERTY_AVAILABLE_DATE='';

				$PROPERTY_NO=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        $PROPERTY_NAME='';

				$PROPERTY_ADDRESS_LINE_1=$PROPERTY_NO.' '.$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();


				$PROPERTY_STATUS='';
        $PROPERTY_AVAILABILITY=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE='';

        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION='';
        $PROPERTY_TENURE='';
        $PROPERTY_QUALIFIER=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';

        $OFF_ROAD_PARKING ='';

		$ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';


		$PETS_ALLOWED='';

        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';


		$SMOKING_ALLOWED='';


        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';


        $PROPERTY_EPC_VALUES = '';

        $PROPERTY_CREATED_ON='';

        /////////////////////////////ADD MORE FIELDS/////////////////////////

        $PROPERTY_FORMATTED_ADDRESS='';
        $PROPERTY_ADDRESS_LINE_1=addslashes($PROPERTY_ADDRESS_LINE_1);
        $PROPERTY_ADDRESS_LINE_2=addslashes($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY			= addslashes($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY		= addslashes($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE		= addslashes($PROPERTY_ADDRESS_POSTCODE);

		$PROPERTY_ROOMS='';



        echo $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
        `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`,
        `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`,
        `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`,`PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`,
        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`,
        `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`,
        `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`,
        `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
        `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
        `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
        `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
        `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`,
        `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`)
         VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
        '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE' ,
        '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
        '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS','$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
        '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
        '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
        '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
        '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
        '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
        '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10',
        '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15',
        '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
        '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
        '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','')";

         echo "<br/><br/>";


    //insert into table
    $db_connect->queryExecute($sql);

    }
}
}
?>
