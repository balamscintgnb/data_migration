<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/expert_agent/ramsey_moore/rooms.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

	        $PROPERTYID = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
	        $ROOM_TITLE = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
	        $ROOM_SIZE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
	        $DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));

			$sql ="INSERT INTO `property_rooms` (`property_id`, `property_room_title`, `property_room_size`, `property_room_description`, `room_id`) VALUES ('$PROPERTYID', '$ROOM_TITLE', '$ROOM_SIZE', '$DESCRIPTION', '') ";
			$db_connect->queryExecute($sql) or die($sql);
        }
    }
    		$z = $row-2;
    		echo $z." PROPERTY ROOMS INSERTED SUCCESSFULLY";
}
?>