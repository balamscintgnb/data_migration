<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/expert_agent/ramsey_moore/Keys.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
	$Tot_row = 0;
    for($row =1; $row <=$total_rows; $row++){
        if($row>1){
        	// $update_Query = "UPDATE `property_keys` SET `RECORD_UPLOADED` = 0";
        	// $db_connect->queryExecute($update_Query);
        	// exit();
        	
        	$KeyNotes = '';
            $KeyNumber = '';
            $REF_id = '';
            $KeyId = '';
            //logic for data mapping.

            $KeyId = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()));
            $REF_id = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
            $KeyNumber = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
            $KeyNotes = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()));
            $KeyStatus = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
            $Property_id = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());

            // echo "<pre>"; print_r($REF_id." = ".$Property_id); echo "</pre>";

            $Query = "INSERT INTO `property_keys` (`KeyId`,`PropertyId`,`KeyCount`,`KeyNumber`,`KeyNotes`,`KeyStatus`)VALUES
            	('$KeyId','$Property_id',NULL,'$KeyNumber','$KeyNotes','$KeyStatus') ";
            	$db_connect->queryExecute($Query) or die($Query);
        }
    }
    		$z = $row-2;
			echo $z." KEYS INSERTED SUCCESSFULLY";
}
?>