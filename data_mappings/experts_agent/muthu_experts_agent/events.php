<?php 
error_reporting(E_ERROR);

//require_once '../../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\ideal_expertagent/Events.csv';
$db_name = 'cnb_ideal';
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
		$event_type = $objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
		if($event_type !="Note"){
			continue;
		}
       
		$VIEWING_ID				= $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
		$VIEWING_APPLICANT_ID	= $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
		if($VIEWING_APPLICANT_ID ==0){
			continue;
		}	
		$VIEWING_USER_ID 		= 0;

		$get_property_id = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
		if($get_property_id !=0){
			$get_property_result = json_decode($db_connect->queryFetch("SELECT PROPERTY_ID FROM cnb_ideal.properties WHERE `PROPERTY_REF_ID`='$get_property_id'"), true);
			$VIEWING_PROPERTY_ID = $get_property_result['data'][0]['PROPERTY_ID'];
		}else{
			$VIEWING_PROPERTY_ID =0;
		}
	
		$BRANCH_ID=0;
		$get_branch_name = $objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
		if($get_branch_name=='Ideal Locations London City'){
			$BRANCH_ID=1;
		}elseif($get_branch_name=='Ideal Locations'){
			$BRANCH_ID=2;
		}

		
		$get_viewing_date = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getformattedValue();
		$VIEWING_DATETIME		= $get_viewing_date;
		$VIEWING_NOTES = trim(addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
		
		$VIEWING_STATUS=0;

		$insert_query ='INSERT INTO '.$db_name.'.notes ( `VIEWING_ID`,`BRANCH_ID`,`VIEWING_APPLICANT_ID`,`VIEWING_USER_ID`,`VIEWING_PROPERTY_ID`,`VIEWING_DATETIME`,`VIEWING_NOTES`, `VIEWING_TYPE`,`VIEWING_STATUS` ) VALUES (';
        $insert_query .= "'$VIEWING_ID','$BRANCH_ID','$VIEWING_APPLICANT_ID','$VIEWING_USER_ID','$VIEWING_PROPERTY_ID','$VIEWING_DATETIME','$VIEWING_NOTES', 10,'$VIEWING_STATUS'";
        $insert_query .= ');';
	  
		//	echo "<pre>"; print_R($insert_query); echo "</pre>";
		$db_connect->queryExecute($insert_query) or die($insert_query);



		}
	}
 
}

?>