<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\ideal_expertagent/Offers.csv';



$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.
            $offer_status = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
         
            if($offer_status != 'Accepted'){
                continue;
            }
           
            $applicant_id = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
            $query_1="SELECT * FROM cnb_ideal.clients_all_orignial  WHERE `CLIENTID` LIKE '$applicant_id' AND CLIENT_TYPE LIKE 'APPLICANT' ";
            $buyer_detail = json_decode($db_connect->queryFetch($query_1),true);
            
            if(!empty($buyer_detail['data'][0])){
                $buyer_detail = $buyer_detail['data'][0];
            }
           
           
            $CLIENTID='BUY_'.$buyer_detail['CLIENTID'];
            $CLIENT_NAME = $buyer_detail['CLIENT_NAME'];
           
            $CLIENT_TITLE=$buyer_detail['CLIENT_TITLE'];
            
            $COMPANY_NAME=$buyer_detail['COMPANY_NAME'];
            $CLIENT_TYPE='BUYER';
            $CLIENT_SUB_TYPE=$buyer_detail['CLIENT_SUB_TYPE'];
            $CLIENT_STATUS=$buyer_detail['CLIENT_STATUS'];
            $CLIENT_STAFF_ID=$buyer_detail['CLIENT_STAFF_ID'];
            $CLIENT_PRIMARY_EMAIL=$buyer_detail['CLIENT_PRIMARY_EMAIL'];
            $CLIENT_PRIMARY_PHONE=$buyer_detail['CLIENT_PRIMARY_PHONE'];
            $CLIENT_ADDRESS_LINE_1=$buyer_detail['CLIENT_ADDRESS_LINE_1'];
            $CLIENT_ADDRESS_LINE_2=$buyer_detail['CLIENT_ADDRESS_LINE_2'];
            $CLIENT_ADDRESS_CITY=$buyer_detail['CLIENT_ADDRESS_CITY'];
            $CLIENT_ADDRESS_TOWN=$buyer_detail['CLIENT_ADDRESS_TOWN'];
            $CLIENT_ADDRESS_POSTCODE=$buyer_detail['CLIENT_ADDRESS_POSTCODE'];
            $CLIENT_ADDRESS1_LINE_1=$buyer_detail['CLIENT_ADDRESS1_LINE_1'];
            $CLIENT_ADDRESS1_LINE_2=$buyer_detail['CLIENT_ADDRESS1_LINE_2'];
            $CLIENT_ADDRESS1_CITY=$buyer_detail['CLIENT_ADDRESS1_CITY'];
            $CLIENT_ADDRESS1_TOWN=$buyer_detail['CLIENT_ADDRESS1_TOWN'];
            $CLIENT_ADDRESS1_POSTCODE=$buyer_detail['CLIENT_ADDRESS1_POSTCODE'];
            $CLIENT_ADDRESS2_LINE_1=$buyer_detail['CLIENT_ADDRESS2_LINE_1'];
            $CLIENT_ADDRESS2_LINE_2=$buyer_detail['CLIENT_ADDRESS2_LINE_2'];
            $CLIENT_ADDRESS2_CITY=$buyer_detail['CLIENT_ADDRESS2_CITY'];
            $CLIENT_ADDRESS2_TOWN=$buyer_detail['CLIENT_ADDRESS2_TOWN'];
            $CLIENT_ADDRESS2_POSTCODE=$buyer_detail['CLIENT_ADDRESS2_POSTCODE'];
            $CLIENT_ACCOUNT_NAME=$buyer_detail['CLIENT_ACCOUNT_NAME'];
            $CLIENT_ACCOUNT_NO=$buyer_detail['CLIENT_ACCOUNT_NO'];
            $CLIENT_ACCOUNT_SORTCODE=$buyer_detail['CLIENT_ACCOUNT_SORTCODE'];
            $CLIENT_EMAIL_1=$buyer_detail['CLIENT_EMAIL_1'];
            $CLIENT_EMAIL_2=$buyer_detail['CLIENT_EMAIL_2'];
            $CLIENT_EMAIL_3=$buyer_detail['CLIENT_EMAIL_3'];
            $CLIENT_EMAIL_4=$buyer_detail['CLIENT_EMAIL_4'];
            $CLIENT_EMAIL_5=$buyer_detail['CLIENT_EMAIL_5'];
            $CLIENT_PHONE_1=$buyer_detail['CLIENT_PHONE_1'];
            $CLIENT_PHONE_2=$buyer_detail['CLIENT_PHONE_2'];
            $CLIENT_PHONE_3=$buyer_detail['CLIENT_PHONE_3'];
            $CLIENT_PHONE_4=$buyer_detail['CLIENT_PHONE_4'];
            $CLIENT_PHONE_5=$buyer_detail['CLIENT_PHONE_5'];
            $CLIENT_MOBILE_1=$buyer_detail['CLIENT_MOBILE_1'];
            $CLIENT_MOBILE_2=$buyer_detail['CLIENT_MOBILE_2'];
            $CLIENT_MOBILE_3=$buyer_detail['CLIENT_MOBILE_3'];
            $CLIENT_MOBILE_4=$buyer_detail['CLIENT_MOBILE_4'];
            $CLIENT_MOBILE_5=$buyer_detail['CLIENT_MOBILE_5'];
            $CLIENT_NOTES=addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
            $CLIENT_FAX_1=$buyer_detail['CLIENT_FAX_1'];
            $CLIENT_FAX_2=$buyer_detail['CLIENT_FAX_2'];
            $CLIENT_FAX_3=$buyer_detail['CLIENT_FAX_3'];
            $CLIENT_FAX_4=$buyer_detail['CLIENT_FAX_4'];
            $CLIENT_FAX_5=$buyer_detail['CLIENT_FAX_5'];
            $CLIENT_CREATED_ON=date('Y-m-d');
            

        
           
            $SEARCH_CRITERIA='';
      

        


    $sql ="INSERT INTO cnb_ideal.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`, `RECORD_UPLOADED`) 
    VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA',0)";


    $db_connect->queryExecute($sql) or die($sql);

    
    }
}
}
?>