<?php 
error_reporting(E_ERROR);

//require_once '../../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\ideal_expertagent/Viewings.csv';
$db_name = 'cnb_ideal';
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

       
		$VIEWING_ID				= 0;
		$VIEWING_APPLICANT_ID	= $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();	
		$VIEWING_USER_ID 		= 0;

		$get_property_id = $objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
		if($get_property_id ==0)	{
			continue;
		}

		$get_property_result = json_decode($db_connect->queryFetch("SELECT PROPERTY_ID FROM cnb_ideal.properties WHERE `PROPERTY_REF_ID`='$get_property_id'"), true);
		$VIEWING_PROPERTY_ID = $get_property_result['data'][0]['PROPERTY_ID'];
	

		

		
		if($VIEWING_APPLICANT_ID ==0){
			continue;
		}
		
		$VIEWING_DATETIME		= $objPHPExcel->getActiveSheet()->getCell('K'.$row)->getformattedValue();
		$VIEWING_NOTES = trim(addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
		
		$get_viewing_status = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
		if(isset($get_viewing_status) && $get_viewing_status=='Confirmed'){
			$VIEWING_STATUS = 1;
		}elseif(isset($get_viewing_status) && $get_viewing_status=='Cancelled'){
			$VIEWING_STATUS = 2;
		}else{
			$VIEWING_STATUS=0;	
		}

		$insert_query ='INSERT INTO '.$db_name.'.viewings ( `VIEWING_ID`,`VIEWING_APPLICANT_ID`,`VIEWING_USER_ID`,`VIEWING_PROPERTY_ID`,`VIEWING_DATETIME`,`VIEWING_NOTES`, `VIEWING_TYPE`,`VIEWING_STATUS` ) VALUES (';
        $insert_query .= "'$VIEWING_ID','$VIEWING_APPLICANT_ID','$VIEWING_USER_ID','$VIEWING_PROPERTY_ID','$VIEWING_DATETIME','$VIEWING_NOTES', 3,'$VIEWING_STATUS'";
        $insert_query .= ');';
     // echo "<pre>"; print_R($insert_query); echo "</pre>";
		$db_connect->queryExecute($insert_query) or die($insert_query);



		}
	}
 
}

?>