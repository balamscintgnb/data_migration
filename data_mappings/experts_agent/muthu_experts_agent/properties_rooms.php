<?php
error_reporting(E_ERROR);

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\ideal_expertagent\Rooms.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        $PROPERTYID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $ROOM_TITLE=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
        $ROOM_SIZE=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        $DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $ROOMlength=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
        $ROOMheight=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();

$sql3 ="INSERT INTO cnb_ideal.`property_rooms`(`PropertyId`, `AreaName`, `SizeText`, `AreaDescription`,
 `LengthMetres`, `WidthMetres`) VALUES 
('$PROPERTYID', '$ROOM_TITLE', '$ROOM_SIZE', '$DESCRIPTION', '$ROOMlength', '$ROOMheight')";
$db_connect->queryExecute($sql3) or die($sql3);



        }
    }
}
?>