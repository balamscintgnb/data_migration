<?php
error_reporting(E_ERROR);

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\Mcmahon_ExpertAgent\Properties.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


                 
            $PROPERTY_DESCRIPTION='';
            $PROPERTY_ADMIN_FEES='';
    
            $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $CLIENT_NAME=trim($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue());
            $PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
            $PROPERTY_TITLE=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $PROPERTY_SHORT_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();

            $PROPERTY_CATEGORY=strtoupper($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            
            $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
    
                $get_frequency =Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
                $PROPERTY_PRICE_FREQUENCY='';
                    if(stristr($get_frequency,'PCM') || stristr($get_frequency,'month') ){ 
                        $PROPERTY_PRICE_FREQUENCY='PCM';
                    }
                    else if(stristr($get_frequency,'PW') || stristr($get_frequency,'week') ){ 
                        $PROPERTY_PRICE_FREQUENCY='PW';
                    }
                    else if(stristr($get_frequency,'PA') || stristr($get_frequency,'Annual') ){ 
                        $PROPERTY_PRICE_FREQUENCY='PA';
                    }
            $PROPERTY_QUALIFIER='';
    
    
            ////////////insert date format///////////////
            $get_available_from = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue(), 'd/m/Y');
            if($get_available_from) { 
                $PROPERTY_AVAILABLE_DATE="'".$get_available_from."'";
            }
            else { 
                $PROPERTY_AVAILABLE_DATE='NULL';
            }
    
    
                /////////////end//////////////
    
    
            $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
            $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
            $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
            $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('BT'.$row)->getValue();
    
            $get_property_status = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
    
            if($get_property_status != ''){
                
                        $PROPERTY_STATUS='Inactive';
                        $PROPERTY_AVAILABILITY=$get_property_status;
                
                
            }
            else { 
                $PROPERTY_STATUS='Inactive';
                $PROPERTY_AVAILABILITY='ARCHIVED';
            }
    
            $PROPERTY_ADMIN_FEES='';
            $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
            if($PROPERTY_TYPE='Double Room' || $PROPERTY_TYPE='Single Room')
            $PROPERTY_TYPE='house share';
    
            $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();
            $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
            $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            $PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
            $Lease_term_years='';
            $PROPERTY_CLASSIFICATION='';
            $PROPERTY_CURRENT_OCCUPANT='';
            $KITCHEN_DINER='';
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING='';
            $GARDEN='';
            $WHEELCHAIR_ACCESS='';
            $ELEVATOR_IN_BUILDING='';
            $POOL='';
            $GYM='';
            $KITCHEN='';
            $DINING_ROOM='';
            $FURNISHED='';
            $INTERNET='';
            $WIRELESS_INTERNET='';
            $TV='';
    
            $WASHER='';
            $DRYER='';
            $DISHWASHER='';
            $PETS_ALLOWED='';
            $FAMILY_OR_CHILD_FRIENDLY='';
            $DSS_ALLOWED='';
            $SMOKING_ALLOWED='';
            $SECURITY='';
            $HOT_TUB='';
    
            $CLEANER='';
            $EN_SUITE='';
            $SECURE_CAR_PARKING='';
            $OPEN_PLAN_LOUNGE='';
            $VIDEO_DOOR_ENTRY='';
            $CONCIERGE_SERVICES='';
            $PROPERTY_CUSTOM_FEATURES='';
            $PROPERTY_ROOMS='';
            $PROPERTY_ASSETS='';
    
            $PROPERTY_IMAGE_1='';
            $PROPERTY_IMAGE_2='';
            $PROPERTY_IMAGE_3='';
            $PROPERTY_IMAGE_4='';
            $PROPERTY_IMAGE_5='';
            $PROPERTY_IMAGE_6='';
            $PROPERTY_IMAGE_7='';
            $PROPERTY_IMAGE_8='';
            $PROPERTY_IMAGE_9='';
    
            $PROPERTY_IMAGE_10='';
            $PROPERTY_IMAGE_11='';
            $PROPERTY_IMAGE_12='';
            $PROPERTY_IMAGE_13='';
            $PROPERTY_IMAGE_14='';
            $PROPERTY_IMAGE_15='';
            $PROPERTY_IMAGE_FLOOR_1='';
            $PROPERTY_IMAGE_FLOOR_2='';
            $PROPERTY_IMAGE_FLOOR_3='';
            $PROPERTY_IMAGE_FLOOR_4='';
            $PROPERTY_IMAGE_FLOOR_5='';
            $PROPERTY_IMAGE_EPC_1='';
            $PROPERTY_IMAGE_EPC_2='';
            $PROPERTY_IMAGE_EPC_3='';
            $PROPERTY_IMAGE_EPC_4='';
            $PROPERTY_IMAGE_EPC_5='';
            $PROPERTY_EPC_VALUES='';
            $PROPERTY_CREATED_ON='';
    
    
            /////////////////////////////ADD MORE FIELDS/////////////////////////
          
            $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION);
            $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);
    
            $PROPERTY_ACCETABLE_PRICE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();        
    
            if($PROPERTY_ACCETABLE_PRICE!=''){
                $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
            }
    
            $Maintenance_charge=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            if($Maintenance_charge!=''){
                $PROPERTY_ADMIN_FEES.='Maintenance charge: '.$Maintenance_charge;
            }
    
    
            $get_name_prop = $objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();
            $CLIENT_NAME = trim($get_name_prop);

            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue();
    
            $query_1="SELECT COL2 FROM cnb_mcmahon.landlord_property WHERE `COL1` = '$PROPERTY_ID' ";
    
            $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
            if($landlord_exists['data'][0]['COL2']){
            continue;
            }
            else {
      
               $query_2="SELECT CLIENTID FROM cnb_mcmahon.clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME'  ";
    
                $landlord_exists1 = json_decode($db_connect->queryFetch($query_2),true);
                if($landlord_exists1['data'][0]['CLIENTID']){
                    $PROPERTY_VENDOR_ID=$landlord_exists1['data'][0]['CLIENTID'];
                    } else { 
                        continue;
                    }

            }
    
    
                    
            $get_instruct_date = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue(), 'd/m/Y');
            if($get_instruct_date) { 
                $INSTRUCTED_DATE="'".$get_instruct_date."'";
            }
            else { 
                $INSTRUCTED_DATE='NULL';
            }
            $PROPERTY_LETTING_SERVICE = '';
    
            if(strstr($get_lettings_managed, 'Fully Managed')) { 
                $PROPERTY_LETTING_SERVICE = '1';
            }
            else if(strstr($get_lettings_managed, 'Introduction')) { 
                $PROPERTY_LETTING_SERVICE = '2';
            }
            else if(strstr($get_lettings_managed, 'Rent Coll')) { 
                $PROPERTY_LETTING_SERVICE = '3';
            }
            
    
            $get_letting_fees = $objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue();
            $get_management_fees =  $objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue();
            $PROPERTY_LETTING_FEE_TYPE='';
            if(stristr($get_letting_fees, '%')){
            $PROPERTY_LETTING_FEE_TYPE=1;
            $PROPERTY_LETTING_FEE_FREQUENCY = end(explode(' ', $get_letting_fees));
    
            }else { 
            $PROPERTY_LETTING_FEE_TYPE=2;
            $PROPERTY_LETTING_FEE_FREQUENCY='';
            }
            $PROPERTY_LETTING_FEE = preg_replace("/[^0-9\.]/", '', $get_letting_fees);
    
    
            $PROPERTY_MANAGEMENT_FEE='';
            if(stristr($get_management_fees, '%')){
            $PROPERTY_MANAGEMENT_FEE_TYPE=1;
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY = end(explode(' ', $get_management_fees));    
            } else { 
            $PROPERTY_MANAGEMENT_FEE_TYPE=2;
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY='';
            }
    
            $PROPERTY_MANAGEMENT_FEE = preg_replace("/[^0-9\.]/", '', $get_management_fees);
    
    
         
            $CERTIFICATE_EXPIRE_DATE='';
            $Gas_certificate_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue();
            $PAT_test_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue();
            $Electricity_certificate_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue();
            $EPC_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue();
            $Insurance_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue();
            $Legionella_risk_assessment_date=$objPHPExcel->getActiveSheet()->getCell('BJ'.$row)->getValue();
            $Smoke_CO_alarm_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue();
    
            $CERTIFICATE_DATE = array('1'=>$Gas_certificate_expiry_date,
                '2'=>$PAT_test_expiry_date, 
                '3'=>$Electricity_certificate_expiry_date,
                '4'=>$EPC_expiry_date, 
                '5'=>$Insurance_expiry_date,
                '6'=>$Legionella_risk_assessment_date, 
                '7'=>$Smoke_CO_alarm_expiry_date);
    
                $CERTIFICATE_EXPIRE_DATE = json_encode($CERTIFICATE_DATE);
    
    
    
            $sql ="INSERT INTO cnb_mcmahon.`properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
                `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
                `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
                `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
                `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
                `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
                `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
                `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
                `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
                `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
                `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
                `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
                `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
                `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
                `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
                `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
                 VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
                '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','','','',
                '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
                '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
                '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)"; 
                //insert into table    
     $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>