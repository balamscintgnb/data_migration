<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\ideal_expertagent/Applicants.csv';



$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

            $buyer_fetch_qry = "SELECT ApplicantId FROM cnb_ideal.offer_csv WHERE ApplicantId = '$CLIENTID'  AND `Status` IN ('Accepted','Complete') ";
           
            $buyer_fetch_result =  json_decode($db_connect->queryFetch($buyer_fetch_qry),true);
            if(isset($buyer_fetch_result['data'][0]['ApplicantId']) && $buyer_fetch_result['data'][0]['ApplicantId']>0 ){
                $CLIENTID = $buyer_fetch_result['data'][0]['ApplicantId'];
            }else{
                continue;
            }


            $buyer_solicitor_id = $objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();
            if($buyer_solicitor_id==0 || $buyer_solicitor_id==''){
                continue;
            }




           
            $CLIENT_TITLE='';
            $name1=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            $name2=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());

            if($name2!='')
            $CLIENT_NAME=$name1.' & '.$name2;
            else 
            $CLIENT_NAME=$name1;
            

            
            $COMPANY_NAME='';
            $CLIENT_TYPE='BUYER';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            //buyer solicitor id
            $CLIENT_STAFF_ID=$buyer_solicitor_id;
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_2=trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
            $CLIENT_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();
            $CLIENT_ADDRESS_TOWN=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
            $CLIENT_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES=addslashes($objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue());
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON=date('Y-m-d');
            



            $SEARCH_CRITERIA = '';


            $sql ="INSERT INTO cnb_ideal.buyer_solicitor(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`, `RECORD_UPLOADED`) 
            VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA',0)";


        $db_connect->queryExecute($sql) or die($sql);

    
    }
}
}
?>