<?php 
$data_mapping=true;
//require_once '../experts_agent/includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/expert_agent/idlo/Properties.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    /*if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else {*/ 
    	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
    //}
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){

        if($row>1){

			$get_property_status = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
			
			$valuation_type = array('Appraisal','Potential Vendor');
			if(in_array($get_property_status,$valuation_type)){
				continue;
			}
			
			$get_property_id=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
			$get_property_result = json_decode($db_connect->queryFetch("SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_REF_ID`='$get_property_id'"), true);

			if(isset($get_property_result['data'][0]['PROPERTY_ID'])){
				$PROPERTY_ID = $get_property_result['data'][0]['PROPERTY_ID'];
			}else{
			    //echo "SELECT PROPERTY_ID FROM properties WHERE `PROPERTY_REF_ID`='$get_property_id'<br/>";
				continue;
			}
			
			$VIEWING_CATEGORY = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();

			//viewing arrangements
			$PROPERTY_NOTES = Utility::format_content(strip_tags(trim($objPHPExcel->getActiveSheet()->getCell('BR'.$row)->getValue())));

			//
			$occupier_details = Utility::format_content(strip_tags(trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue())));
			$access_notes = Utility::format_content(strip_tags(trim($objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue())));

			$occupier_name = Utility::format_content(strip_tags(trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue())));
			//$occupier_salutation = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue()));

			$occupier='';
			
			/*if($occupier_salutation!=''){
				$occupier.= $occupier_salutation;
			}*/
			
			if($occupier_name!=''){
				/*if($occupier_salutation!=''){
					$occupier.='.';
				}*/
				$occupier= $occupier_name;
			}
			

			/*if($PROPERTY_NOTES==""){
				continue;
			}*/

			if($VIEWING_CATEGORY=='Residential Sales'){
				$PROPERTY_CATEGORY = "RESIDENTIAL SALES";
				$NOTES_TYPE = 70;
            }elseif($VIEWING_CATEGORY=='Residential Lettings'){
				$PROPERTY_CATEGORY = "RESIDENTIAL LETTINGS";
				$NOTES_TYPE = 60;
            }elseif($VIEWING_CATEGORY=='Commercial Lettings'){
				$PROPERTY_CATEGORY = "COMMERCIAL LETTINGS";
				$NOTES_TYPE = 60;
            }else{
				$PROPERTY_CATEGORY = "COMMERCIAL SALES";
				$NOTES_TYPE = 70;
			}
			
			if($PROPERTY_NOTES!=''){
				$PROPERTY_NOTES = 'Viewing arrangements:- '.$PROPERTY_NOTES;
				$sql = "INSERT INTO notes (PROPERTY_ID,PROPERTY_CATEGORY,NOTES_TYPE,PROPERTY_NOTES) VALUES ('$PROPERTY_ID','$PROPERTY_CATEGORY','$NOTES_TYPE','$PROPERTY_NOTES')";
				$db_connect->queryExecute($sql) or die($sql);
				//echo '<br/>';
			}

			if($occupier_details!=''){
				$occupier_details = 'Occupier Notes:- '.$occupier_details;
				$sql = "INSERT INTO notes (PROPERTY_ID,PROPERTY_CATEGORY,NOTES_TYPE,PROPERTY_NOTES) VALUES ('$PROPERTY_ID','$PROPERTY_CATEGORY','$NOTES_TYPE','$occupier_details')";
				$db_connect->queryExecute($sql) or die($sql);
				//echo '<br/>';
			}

			if($access_notes!=''){
				$access_notes = 'Access Notes:- '.$access_notes;
				$sql = "INSERT INTO notes (PROPERTY_ID,PROPERTY_CATEGORY,NOTES_TYPE,PROPERTY_NOTES) VALUES ('$PROPERTY_ID','$PROPERTY_CATEGORY','$NOTES_TYPE','$access_notes')";
				$db_connect->queryExecute($sql) or die($sql);
				//echo '<br/>';
			}

			if($occupier!=''){
				$occupier = 'Current Occupier:- '.$occupier;
				$sql = "INSERT INTO notes (PROPERTY_ID,PROPERTY_CATEGORY,NOTES_TYPE,PROPERTY_NOTES) VALUES ('$PROPERTY_ID','$PROPERTY_CATEGORY','$NOTES_TYPE','$occupier')";
				$db_connect->queryExecute($sql) or die($sql);
				//echo '<br/>';
			}

			/*if($row>100){
				break;
			}*/

		}
	}
}

echo 'Done';
?>