<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/expert_agent/ramsey_moore/applicants.xls';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
	$Tot_row = 0;
    for($row =1; $row <=$total_rows; $row++){
        if($row>1){
            //logic for data mapping.
            // $update_Query = "UPDATE clients SET `RECORD_UPLOADED` = 0 WHERE `CLIENTID` = '140938358'";
            // $db_connect->queryExecute($update_Query);
            // exit();

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
           
            $CLIENT_TITLE='';
            $name1 = str_replace("  "," ",trim(trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue())));
            $name2 = str_replace("  "," ",trim(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue())));

            if($name2!='')
            $CLIENT_NAME=$name1.' & '.$name2;
            else 
            $CLIENT_NAME=$name1;
            
            $COMPANY_NAME = '';
            $CLIENT_TYPE='APPLICANT';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS = 'inactive';
            
			$CUR_DATE_STT = strtotime('2017-12-31 23:59:59');
	
				$date_enter = $objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getformattedValue();
				$CHK_DATE_STT = strtotime($date_enter);
				
				if($CHK_DATE_STT > $CUR_DATE_STT)
				{
				$CLIENT_STATUS = 'active';
				}
	          
            // $CLIENT_STATUS = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());

            // echo"<pre>";
            // print_r($date_enter."  =  ".$CLIENT_STATUS);
            // echo"</pre>";
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue()));
            	if(stristr($CLIENT_PRIMARY_EMAIL,'n/a')){
            		$CLIENT_PRIMARY_EMAIL = '';
            	}
            $CLIENT_PRIMARY_PHONE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue()));
            $CLIENT_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim(trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue())));
            $CLIENT_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
            $CLIENT_ADDRESS_TOWN = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
            $CLIENT_ADDRESS_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1 = trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES = str_replace("Â£","£",trim($objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue()));
            $CLIENT_NOTES2 = str_replace("Â£","£",trim($objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue()));
            if($CLIENT_NOTES2){
            	if($CLIENT_NOTES){
            		$CLIENT_NOTES.="<br>";
            	}
            	$CLIENT_NOTES.=$CLIENT_NOTES2;
            }
            $CLIENT_NOTES=(trim($CLIENT_NOTES)!='') ? addslashes(trim($CLIENT_NOTES)) : '';
            // $CLIENT_NOTES=(trim($CLIENT_NOTES)!='') ? addslashes(json_encode(trim($CLIENT_NOTES))) : '';
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON = $date_enter;
            $MAX_BEDS = $objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            $MIN_BATH = '';
			$MAX_BATH = '';
        	$FURNISHED = '';
        	$dss_allowed = '';
        	$PROPERTY_TYPE = '';
        	$PROPERTY_TYPE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue()));
        	if($PROPERTY_TYPE == ''){
        		$PROPERTY_TYPE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue()));
        	}
			$PROPERTY_TYPE = explode(',',$PROPERTY_TYPE);
			$PROPERTY_TYPE= implode(' | ',$PROPERTY_TYPE);
        	
	        if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
				if($CLIENT_NAME==''){
					
	                $CLIENT_NAME=$COMPANY_NAME;
	            }
			}
			$CLIENT_NAME = addslashes(str_replace("  "," ",$CLIENT_NAME));
	        $get_department = strtolower($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
	        if($get_department == 'residential sales')
	        $SEARCH_CATEGORY = 43;
	        else if($get_department == 'residential lettings')
	        $SEARCH_CATEGORY = 44;
	        else if($get_department == 'commercial sales')
	        $SEARCH_CATEGORY = 45;
	        else if($get_department == 'commercial lettings')
	        $SEARCH_CATEGORY = 46;
	        
	        $PROPERTY_LOCATIONS = $objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
	        $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$PROPERTY_TYPE,
        	'property_applicant_search_attribute_2_from'=>$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue(),
	        'property_applicant_search_attribute_2_to'=>$MAX_BEDS,
	        'property_applicant_search_attribute_3_from'=>$MIN_BATH,
	        'property_applicant_search_attribute_3_to'=>$MAX_BATH,
	        'property_applicant_search_attribute_4_from'=>'',
	        'property_applicant_search_attribute_4_to'=>'',
	        'property_applicant_search_attribute_6'=>'',
	        'property_applicant_search_attribute_8'=>'',
	        'property_applicant_search_attribute_15'=>$FURNISHED,
	        'property_applicant_search_attribute_22'=>'',
	        'property_applicant_search_attribute_24'=>$dss_allowed,
	        'property_applicant_search_attribute_30'=>''
	    	);

		     $SEARCH_CRITERIA_APPLICANT=array('price'=>$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue()
		         ,'pricefrom'=>$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue(),'filter_array'=>$applicant_search_criteria,
		         'frequency'=>'','category'=>$SEARCH_CATEGORY,'location'=>$PROPERTY_LOCATIONS);
	         $SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);
	         
			$Query2 = "SELECT * FROM `clients` WHERE CLIENTID = '$CLIENTID' AND CLIENT_TYPE = '$CLIENT_TYPE' ";
			$Clients_Exist = json_decode($db_connect->queryFetch($Query2),true);
				if(@$Clients_Exist['data'][0]['CLIENTID']){
					#continue;
				}
				else{
			$Tot_row++;
			
			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
				`CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
				`CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`,
				`CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`,
				`CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`,
				`CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`,
				`CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`,
				`CLIENT_CREATED_ON`,`SEARCH_CRITERIA`,`CLIENT_COMPANY_NAME`) VALUES 
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL',
				'$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
				'$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
				'$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
				'$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
				'$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
				'$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA','')";
			$db_connect->queryExecute($sql) or die($sql);
		}
        }
    }
	    	$z = $row-2;
	    	echo "TOTAL ROWS : ".$z. "<br>APPLICANTS INSERTED SUCCESSFULLY COUNT IS : ".$Tot_row;
}
?>