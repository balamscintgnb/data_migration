<?php
error_reporting(0);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/ideal/ideal_expertagent/Maintenance.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}


//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

    $JOB_NOTES='';
    
    $MAINTENANCE_CAT_TYPE			= trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
	
	$MAINTENANCE_ITEM_DESCRIPTION = '';
	
	$MAINTENANCE_CATEGORY = '';
	
	if($MAINTENANCE_CAT_TYPE=="Plumbing"){
      $MAINTENANCE_CATEGORY='22';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Plumbing';
    }else if($MAINTENANCE_CAT_TYPE=="14"){
      $MAINTENANCE_CATEGORY='23';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Window cleaning';
    }else if(stristr($MAINTENANCE_CAT_TYPE,'Decoration')){
      $MAINTENANCE_CATEGORY='14';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Decoration';
    }else if($MAINTENANCE_CAT_TYPE=="Electrical"){
      $MAINTENANCE_CATEGORY='7';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Electrical';
    }else if($MAINTENANCE_CAT_TYPE=="Cleaning"){
      $MAINTENANCE_CATEGORY='24';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Cleaning';
    }else if($MAINTENANCE_CAT_TYPE=="6"){
      $MAINTENANCE_CATEGORY='8';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Gardening';
    }else if($MAINTENANCE_CAT_TYPE=="Safety Check"){
      $MAINTENANCE_CATEGORY='24';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Other';
    }else if($MAINTENANCE_CAT_TYPE=="Services"){
      $MAINTENANCE_CATEGORY='24';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Other';
    }else if($MAINTENANCE_CAT_TYPE=="Structural"){
      $MAINTENANCE_CATEGORY='24';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Other';
    }else if($MAINTENANCE_CAT_TYPE=="Heating"){
      $MAINTENANCE_CATEGORY='12';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Heating';
    }else{
      $MAINTENANCE_CATEGORY='24';
      $MAINTENANCE_ITEM_DESCRIPTION.='\n Other';
    }


    $MAINTENANCE_CODE			= trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
    $PROPERTY_ID				= trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
	
	$query_PROPERTY="SELECT PROPERTY_ID FROM `properties` WHERE `PROPERTY_REF_ID` LIKE '$PROPERTY_ID' order by PROPERTY_REF_ID asc";
	$FIND_PRO_ID = json_decode($db_connect->queryFetch($query_PROPERTY),true);
	$FIND_PRO_ID = $FIND_PRO_ID['data'] ;
	$PROPERTY_REF_ID=$FIND_PRO_ID[0]['PROPERTY_ID'];
	
	
    $TENANCY_CODE			= trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
    $TENANT_CODE			= '';
	$STATUS					= trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
	
	$JOB_STATUS	= '';
	if($STATUS=='Awaiting Landlord Consent' || $STATUS=='Awaiting Tenant Availability'){
		$JOB_STATUS='3';
	}else if($STATUS=='Future Job' || $STATUS=='In Progress' || $STATUS=='New' || $STATUS=='Waiting for Contractor'){
		$JOB_STATUS='0';
	}else if($STATUS=='Completed'){
		$JOB_STATUS='8';
	}
	
	
	
	
    $SUPPLIER_CODE			= trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
    $ESTIMATE_COST			= '';

	 $DATE_REPORTED=trim(Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getformattedValue(),'m/d/Y h:i'));
	
	//echo '<br>';

    $MAINTENANCE_DATE_REPORTED='';
    if($DATE_REPORTED!=''){
      $MAINTENANCE_DATE_REPORTED			= $DATE_REPORTED;
    }
	
	$DATE_COMPLETED = trim(Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getformattedValue(),'m/d/Y h:i'));

    $MAINTENANCE_DATE_COMPLETED='';
    if($DATE_COMPLETED!=''){
      $MAINTENANCE_DATE_COMPLETED			= $DATE_COMPLETED;
    }

    $MAINTENANCE_NOTES			= trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
	
	$REFERENCE = trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
	
	$REPORTED_BY = trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
	
	if($REFERENCE	==''){
      $MAINTENANCE_NOTES.='\n '.$REFERENCE;
    }
	
	if($REPORTED_BY	==''){
      $MAINTENANCE_NOTES.='\n '.$REPORTED_BY;
    }

	$MAINTENANCE_ITEM_DESCRIPTION			= str_replace("'",'',addslashes(preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue()))));
    
    if($MAINTENANCE_ITEM_DESCRIPTION	==''){
      $MAINTENANCE_ITEM_DESCRIPTION.='MAINTENANCE';
    }
    
    $JOB_NOTES=str_replace("'",'',addslashes(preg_replace( '/[^[:print:]]/', '',$MAINTENANCE_NOTES)));

    $JOB_NOTES=json_encode($JOB_NOTES);
    

  $sql = "INSERT INTO `maintenance_jobs` (`JOB_REF_NO`, `REPORT_PROBLEM_ID`, `SUPPLIER_ID`, `PROPERTY_ID`, `JOB_CATEGORY`, `JOB_DESCRIPTION`, `JOB_ON`, `JOB_PRIORITY`,
  `JOB_ESTIMATED_COST`, `JOB_WILL_BE_PAID_BY`, `JOB_CONTRACTOR_INVOICE_FILE`, `JOB_AGENT_INVOICE_FILE`, `JOB_REQUEST_DATE`, `JOB_CONFIRMED_DATE`, `JOB_CONTRACTOR_QUOTE_FILE`,
  `JOB_AGENT_QUOTE_FILE`, `JOB_INVOICE_AMT_CONTRACTOR`, `JOB_INVOICE_AMT_AGENT`, `JOB_PAYABLE_USER_ID`, `JOB_PAYABLE_STATUS`, `JOB_PAYABLE_AMOUNT`, `JOB_PAYABLE_PAID_AMOUNT`,
    `JOB_PAYABLE_INVOICE_FILE`, `JOB_PAYABLE_TRANSACTION_IDS`, `JOB_STATUS`,`JOB_NOTES`,`JOB_CREATED_ON`, `JOB_COMPLETED_ON`, `JOB_TRANSACTION_ID`, `JOB_UPDATED_ON`)
    VALUES ('$MAINTENANCE_CODE', '', '$SUPPLIER_CODE', '$PROPERTY_REF_ID', '$MAINTENANCE_CATEGORY', '$MAINTENANCE_ITEM_DESCRIPTION', '$MAINTENANCE_DATE_REPORTED', '1', '$ESTIMATE_COST', '$PAID_BY', '', '', '', '', '', '', '$MAINTENANCE_ITEM_AMOUNT', '', '', '', '', '', '', '','$JOB_STATUS','$JOB_NOTES', '$MAINTENANCE_DATE_REPORTED', '$MAINTENANCE_DATE_COMPLETED', '', '')";

  $db_connect->queryExecute($sql) or die($sql);
  }

}

}
?>
