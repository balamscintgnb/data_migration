<?php
extract($_GET);

// Report all PHP errors (see changelog)
error_reporting(E_ALL);
ini_set('display_errors',1);

//copy ('https://app.jetsoftware.co.uk/stn/exitstaging/pictures/LHA/08/LHA080029_01.jpg', 'test.jpg'); exit;


require_once '../plugins/PHPExcel/IOFactory.php';

$dest = '../files';
$software='experts_agent';
$cnb='mcmahon';

$totalProperties=0;
$totalImages=0;
$totalImagesCopied=0;
$totalPropertiesWithImages=0;

$source_path = "../source_data/$software/".$cnb;

if($file==1){
    $file_name=$source_path.'/Brochures.xls';
}

if($file==2){
    $file_name=$source_path.'/Documents.xls';
}

if($file==3){
    $file_name=$source_path.'/FloorPlans.xls';
}

if($file==4 || $file==5){
    $file_name=$source_path.'/Links.xls';
}

echo $file_name.'<br/>';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
    die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$property_ids = array();

$property_id_images = array();

$rooms = array();

$sub_dir='';

if($file==1){
    $sub_dir='brochures/';
}else if($file==2){
    $sub_dir='documents/';
}else if($file==3){
    $sub_dir='floorplans/';
}else if($file==4 || $file==5){
    $sub_dir='links/';
}else{
    $thisProceed=false;
}

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){

        if($row>1){

            $FILE_URL='';

            if($file==2){
                $PROPERTY_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                $FILE_URL=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            }else{
                $PROPERTY_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                $FILE_URL=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            }

            if($file==5){
                $FILE_URL=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            }

            //echo $FILE_URL.'<br/>';
            //$request = curl_get_file_info($FILE_URL, 'Content-Length');

            if($FILE_URL!='' && $PROPERTY_ID!=''){

                $totalImages++;

                $fileMetaData = pathinfo($FILE_URL);

                $FILE_URL = str_replace($fileMetaData['basename'], rawurlencode($fileMetaData['basename']), $FILE_URL);                

                echo $PROPERTY_ID.'<br/>';
                echo $FILE_URL.'<br/>';

                //if($request>0){

                    $path = $dest.'/'.$software.'/'.$cnb.'/'.$PROPERTY_ID.'/'.$sub_dir;
                       //exit; 
                    if(!is_dir($path)){
                        mkdir($path, 0777, true);
                        $property_ids[]=$PROPERTY_ID;
                    }

                    if(!file_exists($path.$fileMetaData['basename']) || filesize($path.$fileMetaData['basename'])<=0){

                        $request1 = getUrlContent($FILE_URL);

                        //var_dump($request1); 
                        
                        //exit;

                        if($request1!==false){
                            
                            if(file_put_contents($path.$fileMetaData['basename'], $request1)){
                                $totalImagesCopied++;
                                $property_id_images[]=$PROPERTY_ID;
                            }else{
                                echo 'Copy Failed<br/>';
                            }
                        }else{
                            var_dump($request1, 'Failed');
                            echo '<br/>';
                        }
                    }else{
                        echo (filesize($path.$fileMetaData['basename'])/1024).'<br/>';
                        $totalImagesCopied++;
                    }
                //}
            }
            //sleep(1);
            //break;
        }
    }
}

/*
$file ='https://app.jetsoftware.co.uk/stn/exitstaging/pictures/LHA/08/LHA080029_01.jpg';

$request = getUrlContent($file);

var_dump($request);		
*/

$property_ids = array_unique($property_ids);

echo 'Total Properties: '.count($property_ids).'<br/>';
echo 'Total Images: '.$totalImages.'<br/>';
echo 'Total Images Copied: '.$totalImagesCopied.'<br/>';
echo 'Total Properties with Images: '.count($property_id_images).'<br/>';

function getUrlContent($url){
	
	//$cookie="_ga=GA1.3.935046737.1524546204; ASP.NET_SessionId=tdnnvqxiulg1vo1y1f1l10bp; _gid=GA1.3.1319944012.1524720051; .ASPXAUTH=F9208F7F86C9632F0EC5905663307AF27CFF8412B55106273E17F6DCA49710CB2BA613561A90A1AA599A4B0EA904BB5201DB061E1D6AA5AA4A7B34BB1A1B40E7C87F4A62C7228198A11464C5E4B2872FBDA991B79FBCBC4034D1AA16B6E6C2559CAB3D48C2F0DB77A7442CE0E669BF56530FE8DB";
	
	//$header = array("Cookie: $cookie", "Host: rtlive.co.uk", "Referer: https://rtlive.co.uk/properties/search");
    
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //curl_setopt($ch, CURLOPT_USERPWD, 'exit' . ":" . '907c46d4cdd190df99f9ba4088be02d3');

    //Digest username="exit", realm="stnrealm", nonce="O/XpIxl7BQA=789de86c3361ca897f3c3cc07bf2b47a4f307ca0", uri="/stn/exitstaging/pictures/LHA/08/LHA080029_01.jpg", algorithm=MD5, response="3ac45da2ec069cd28ae76d14560cfe54", qop=auth, nc=00000001, cnonce="22da39b43641ceb0"
    $headers = array(
        'Authorization: Basic '. base64_encode("exit:907c46d4cdd190df99f9ba4088be02d3") // <---
    );
    //stnrealm
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    //curl_setopt($ch, CURLOPT_POST, 1);

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 0);
	
	//curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	
	curl_setopt($ch, CURLOPT_VERBOSE, true);
	
	//for connection problem
	/*curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );*/
	 
	$html = curl_exec($ch);
	//$redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);
	
	//handle curl error 
    if(curl_errno($ch)) {
       $html=false;
       //json_encode(array("success"=>false, "response"=> 'Curl error: ' . curl_error($ch)))
    }
	
	curl_close($ch);
	return $html;
}
?>