<?php
error_reporting(E_ALL);
ini_set('display_errors',0);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';


$file_name='D:\data_migration_bkup\mcmohan/FinancialTransactions.csv';

$thisProceed=true;
/*
$financial_entry_data['data'][0]['descrption'] = 'Mr  Alexandre Pollazzon - Rent Charge for the period 02 October 2016 to 01 November 2016';
if(strrchr($financial_entry_data['data'][0]['descrption'],'Rent Charge for the period')){
  $rent_due_date_string = str_replace("Rent Charge for the period","",strrchr($financial_entry_data['data'][0]['descrption'],'Rent Charge for the period'));
  $rent_due_dates = explode(" to ",$rent_due_date_string);
  $RENT_STARTDATE = $rent_due_dates[0];
  $RENT_ENDDATE = $rent_due_dates[1];

}
*/

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 







//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =2; $row <= $total_rows; $row++){
       
        if($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue() != '' && $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue() != '0') { 
        
          $TRANSACTION_ID = $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
          
          

          $tenancy_id = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
          $query_1="SELECT 	LETTING_CUSTOM_REF_NO,PROPERTY_ID,LETTING_RENT,LETTING_RENT_FREQUENCY,LETTING_RENT_PAYMENT_DAY,TENANT_ID,SHARED_TENANT_IDS FROM lettings WHERE `LETTING_CUSTOM_REF_NO` LIKE '$tenancy_id'";

          $TENANCY_DETAIL = json_decode($db_connect->queryFetch($query_1),true);

          $LETTING_CUSTOM_REF_NO = $TENANCY_DETAIL['data'][0]['LETTING_CUSTOM_REF_NO'];
          $letting_rent = $TENANCY_DETAIL['data'][0]['LETTING_RENT'];
          $letting_rent_frequency = $TENANCY_DETAIL['data'][0]['LETTING_RENT_FREQUENCY'];

          $PROPERTY_ID =$TENANCY_DETAIL['data'][0]['PROPERTY_ID']; 

          $PROPERTY_ROOM_ID ='';
          $TENANT_ID = $TENANCY_DETAIL['data'][0]['TENANT_ID']; 
          $SHARED_TENANT_IDS = $TENANCY_DETAIL['data'][0]['SHARED_TENANT_IDS'];
          $RENT_STARTDATE = 'null';
          $RENT_ENDDATE = 'null';

          //$DUE_DATE = $TENANCY_DETAIL['data'][0]['LETTING_RENT_PAYMENT_DAY']; 



          $get_transaction_date = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getformattedValue();
    
          $transaction_date =Utility::convert_tosqldate($get_transaction_date, 'd/m/Y h:i:s');
          $PAYMENT_DATE = $transaction_date;
          //$DUE_AMOUNT=$letting_rent;

          $dt = new DateTime($transaction_date);
          $DUE_DATE = $dt->format('Y').'-'.$dt->format('m').'-'.$TENANCY_DETAIL['data'][0]['LETTING_RENT_PAYMENT_DAY']; 
         
          $financial_entry_qry ="SELECT * FROM financialentries WHERE `transaction_id` LIKE '$TRANSACTION_ID' and `credit`!=0";
          $financial_entry_data = json_decode($db_connect->queryFetch($financial_entry_qry),true);
          
          $DUE_AMOUNT=$PAID_AMOUNT=$financial_entry_data['data'][0]['credit']; 
          $TRANSACTION_STATUS ='paid';

          $TRANSACTION_TYPE = '';
          if($financial_entry_data['data'][0]['entry_type']=='RD'){
            if(strrchr($financial_entry_data['data'][0]['descrption'],'Rent Charge for the period')){
              $rent_due_date_string = str_replace("Rent Charge for the period","",strrchr($financial_entry_data['data'][0]['descrption'],'Rent Charge for the period'));
              $rent_due_dates = explode(" to ",$rent_due_date_string);
              $RENT_STARTDATE = date('Y-m-d',strtotime($rent_due_dates[0]));
              $RENT_ENDDATE = date('Y-m-d',strtotime($rent_due_dates[1]));
            }
            //$TRANSACTION_TYPE='1';
            $TRANSACTION_TYPE='RENT';
            $DUE_AMOUNT=$letting_rent;
          }elseif($financial_entry_data['data'][0]['entry_type']=='DDS'){
            //$TRANSACTION_TYPE='3';
            $TRANSACTION_TYPE='DEPOSIT';
          }elseif($financial_entry_data['data'][0]['entry_type']=='AP'){
            //$TRANSACTION_TYPE='5';
            $TRANSACTION_TYPE='ADMIN';
          }elseif($financial_entry_data['data'][0]['entry_type']=='PI'){
            //$TRANSACTION_TYPE='6';
            $TRANSACTION_TYPE='INVENTORY';
          }elseif($financial_entry_data['data'][0]['entry_type']=='PR'){
            $TRANSACTION_TYPE='11';
            continue;
          }else{
            $TRANSACTION_TYPE='21';
            continue;
          }
if( $PAID_AMOUNT != $DUE_AMOUNT)
{
  $DUE_AMOUNT=$PAID_AMOUNT;
}

/*
          <select id="receipt_type" name="receipt_type" class="form-control" onchange="show_tbl_receipts(this.value);">
          <option value="1">Rent</option>
              <option value="3">Deposit</option>
              <option value="5">Admin fee</option>
              <option value="6">Inventory fee</option>
              <option value="11">Tenant referencing fee</option>
              <option value="12">Tenant renewal fee</option>
              <option value="13">Checkout fee</option>
              <option value="14">Late fee</option>
              <option value="20">Premium</option>
              <option value="22">Holding deposit</option>
              <option value="23">Security deposit</option>
              <option value="21">Others</option>
          
          </select>
*/




          $TRANSACTION_NO='';
          $PAYMENT_METHOD='Cash';
          $TRANSACTION_DESCRIPTION =addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
          $TRANSACTION_NOTES=addslashes($financial_entry_data['data'][0]['descrption']); 

           $sql="INSERT INTO `tenant_transactions`(`TRANSACTION_ID`,`LETTING_CUSTOM_REF_NO`,`PROPERTY_ID`,`TENANT_ID`,`RENT_STARTDATE`,`RENT_ENDDATE`,`DUE_DATE`,`PAYMENT_DATE`,`DUE_AMOUNT`,`PAID_AMOUNT`,`TRANSACTION_STATUS`,`TRANSACTION_TYPE`,`TRANSACTION_NO`,`PAYMENT_METHOD`,`TRANSACTION_DESCRIPTION`,`TRANSACTION_NOTES`) VALUES
           ('$TRANSACTION_ID','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$TENANT_ID','$RENT_STARTDATE','$RENT_ENDDATE','$DUE_DATE','$PAYMENT_DATE','$DUE_AMOUNT','$PAID_AMOUNT','$TRANSACTION_STATUS','$TRANSACTION_TYPE','$TRANSACTION_NO','$PAYMENT_METHOD','$TRANSACTION_DESCRIPTION','$TRANSACTION_NOTES');";
       
    

          /*
           $sql="INSERT INTO `tenant_transactions`( `LETTING_CUSTOM_REF_NO`, `RENT_STARTDATE`, `RENT_ENDDATE`, `DUE_DATE`,
         `PAYMENT_DATE`, `DUE_AMOUNT`, `PAID_AMOUNT`, `TRANSACTION_TYPE`,  `TRANSACTION_DESCRIPTION`) VALUES
          ('$LETTING_REF_NO',$RENT_STARTDATE,$RENT_ENDDATE, '$DATE_RECEIVED','$DATE_RECEIVED','$AMOUNT_DUE','$RECEVIED', '$TRANSACTION_TYPE','$DESCRIPTION') ;";
          */
 
        $db_connect->queryExecute($sql) or die($sql); 
          

      }
    }
}

?>
