<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/expert_agent/ramsey_moore/properties.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){
		$appris = $prope_no = 0;
    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
        	
        	// $update_Query = "update `properties` set `RECORD_UPLOADED` = 0 where `PROPERTY_ROOMS` !='' LIMIT 15";
        	$update_Query = "UPDATE `property_rooms` SET RECORD_UPLOADED = 0 WHERE `property_id` = '1465039'";
        	$db_connect->queryExecute($update_Query);echo "SUCCESS";
        	exit();

            $PROPERTY_DESCRIPTION='';
            $PROPERTY_ADMIN_FEES='';
            $PROPERTY_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $PROPERTY_VENDOR_ID = '';
            $PROPERTY_REF_ID = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $PROPERTY_STAFF_ID='';
            $PROPERTY_TITLE='';
            $PROPERTY_SHORT_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BW'.$row)->getValue()));
            $PROPERTY_DESCRIPTION = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BU'.$row)->getValue()));

            $PROPERTY_CATEGORY = strtoupper(trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()));
            
            $PROPERTY_PRICE = trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
    
                $get_frequency =Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
                $PROPERTY_PRICE_FREQUENCY='';
                    if(stristr($get_frequency,'PCM') || stristr($get_frequency,'month') )
                    { 
                        $PROPERTY_PRICE_FREQUENCY='PCM';
                    }
                    else if(stristr($get_frequency,'PW') || stristr($get_frequency,'week') )
                    { 
                        $PROPERTY_PRICE_FREQUENCY='PW';
                    }
                    else if(stristr($get_frequency,'PA') || stristr($get_frequency,'Annual') )
                    {
                        $PROPERTY_PRICE_FREQUENCY='PA';
                    }
                    $PROPERTY_QUALIFIER='';
                    if(stristr($PROPERTY_CATEGORY, 'sale'))
                    {
                        $get_qualifier=Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
						if($get_qualifier == 'Asking Price')
						{ 
							$PROPERTY_QUALIFIER='Fixed price';
						}
						else if($get_qualifier == 'Offers in the Region Of')
						{
							$PROPERTY_QUALIFIER='Offers in the region of';
						}
						else if($get_qualifier == 'Guide Price')
						{
							$PROPERTY_QUALIFIER='Guide price';
						}
						else if($get_qualifier == 'From')
						{
							$PROPERTY_QUALIFIER='From';
						}
						else if($get_qualifier == 'Offers in Excess of')
						{
							$PROPERTY_QUALIFIER='Offers in excess of';
						}
						else if($get_qualifier == 'Offers Over')
						{ 
							$PROPERTY_QUALIFIER='Offers over';
						}
                    }
    
    
            ////////////insert date format///////////////
            $get_available_from = Utility::convert_tosqldate(trim($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue()), 'd/m/Y');
            if($get_available_from) { 
                $PROPERTY_AVAILABLE_DATE="'".$get_available_from."'";
            }
            else { 
                $PROPERTY_AVAILABLE_DATE='NULL';
            }
    
                /////////////end//////////////

            $PROPERTY_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
            $PROPERTY_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
            $PROPERTY_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));
            $PROPERTY_ADDRESS_COUNTY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));
            $PROPERTY_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue()));
            $PROPERTY_FORMATTED_ADDRESS = '';

            $get_property_status = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());

            if($get_property_status != ''){
                
                        $PROPERTY_STATUS='Inactive';
                        $PROPERTY_AVAILABILITY = trim($get_property_status);
                
                
            }
            else { 
                $PROPERTY_STATUS='Inactive';
                $PROPERTY_AVAILABILITY='ARCHIVED';
            }

            $PROPERTY_ADMIN_FEES='';
            $PROPERTY_TYPE = trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
                if(stristr($PROPERTY_TYPE,'shop'))
                { 
                    $PROPERTY_TYPE='Retail';
                }
                else if (stristr($PROPERTY_TYPE,'Semi Detached')){ 
                    $PROPERTY_TYPE='Semi-Detached';
                }
            $PROPERTY_BEDROOMS = trim($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue());
            $PROPERTY_BATHROOMS = trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
            $PROPERTY_RECEPTION = trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
            $PROPERTY_TENURE = trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue());
            $Lease_term_years='';
            $PROPERTY_CLASSIFICATION='';
            $PROPERTY_CURRENT_OCCUPANT='';
            $KITCHEN_DINER='';
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING='';
            $GARDEN='';
            $WHEELCHAIR_ACCESS='';
            $ELEVATOR_IN_BUILDING='';
            $POOL='';
            $GYM='';
            $KITCHEN='';
            $DINING_ROOM='';
            $FURNISHED='';
            $INTERNET='';
            $WIRELESS_INTERNET='';
            $TV='';
    
            $WASHER='';
            $DRYER='';
            $DISHWASHER='';
            $PETS_ALLOWED='';
            $FAMILY_OR_CHILD_FRIENDLY='';
            $DSS_ALLOWED='';
            $SMOKING_ALLOWED='';
            $SECURITY='';
            $HOT_TUB='';
    
            $CLEANER='';
            $EN_SUITE='';
            $SECURE_CAR_PARKING='';
            $OPEN_PLAN_LOUNGE='';
            $VIDEO_DOOR_ENTRY='';
            $CONCIERGE_SERVICES='';
            $custom_features = array(
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('BX'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('BZ'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CA'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CB'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CC'.$row)->getValue())), 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CD'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CE'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CF'.$row)->getValue())) , 
                stripslashes(trim($objPHPExcel->getActiveSheet()->getCell('CG'.$row)->getValue())) , 
            );

            $PROPERTY_CUSTOM_FEATURES=addslashes(json_encode($custom_features));
            $PROPERTY_ROOMS='';


            $get_room_query = json_decode($db_connect->queryFetch("SELECT * FROM property_rooms WHERE `property_id`='$PROPERTY_ID'"), true);
            $rooms=[];
                if(isset($get_room_query['data']) && count($get_room_query['data'])>0)
				{
				  foreach($get_room_query['data'] as $room_row)
				  {
				      $rooms[] = array("property_room_title"=>$room_row['property_room_title'],
				       "property_room_size"=>stripslashes($room_row['property_room_size']),
				      "property_room_description"=>$room_row['property_room_description'],
				     );
				  }
				}
			$PROPERTY_ROOMS=addslashes(json_encode($rooms));

            $PROPERTY_ASSETS='';
            $PROPERTY_IMAGE_1='';
            $PROPERTY_IMAGE_2='';
            $PROPERTY_IMAGE_3='';
            $PROPERTY_IMAGE_4='';
            $PROPERTY_IMAGE_5='';
            $PROPERTY_IMAGE_6='';
            $PROPERTY_IMAGE_7='';
            $PROPERTY_IMAGE_8='';
            $PROPERTY_IMAGE_9='';
    
            $PROPERTY_IMAGE_10='';
            $PROPERTY_IMAGE_11='';
            $PROPERTY_IMAGE_12='';
            $PROPERTY_IMAGE_13='';
            $PROPERTY_IMAGE_14='';
            $PROPERTY_IMAGE_15='';
            $PROPERTY_IMAGE_FLOOR_1='';
            $PROPERTY_IMAGE_FLOOR_2='';
            $PROPERTY_IMAGE_FLOOR_3='';
            $PROPERTY_IMAGE_FLOOR_4='';
            $PROPERTY_IMAGE_FLOOR_5='';
            $PROPERTY_IMAGE_EPC_1='';
            $PROPERTY_IMAGE_EPC_2='';
            $PROPERTY_IMAGE_EPC_3='';
            $PROPERTY_IMAGE_EPC_4='';
            $PROPERTY_IMAGE_EPC_5='';
            $PROPERTY_EPC_VALUES='';
            $PROPERTY_CREATED_ON = 'NULL';


            /////////////////////////////ADD MORE FIELDS/////////////////////////

            $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION);
            $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);

            $PROPERTY_ACCETABLE_PRICE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();        

            if($PROPERTY_ACCETABLE_PRICE!='')
            {
                $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
            }

            $Maintenance_charge=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            if($Maintenance_charge!='')
            {
                $PROPERTY_ADMIN_FEES.='Maintenance charge: '.$Maintenance_charge;
            }


            // $get_name_prop = $objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();
            // $CLIENT_NAME = trim($get_name_prop);

            // $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue();

            // $query_1="SELECT COL2 FROM   WHERE `COL1` LIKE '$PROPERTY_ID' ";
    
            // $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);

            // if($landlord_exists['data'][0]['COL2'])
            // {
            //     $PROPERTY_VENDOR_ID=$landlord_exists['data'][0]['COL2'];
            // }
            // else {

            //   $query_2="SELECT CLIENTID FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME'  ";

            //     $landlord_exists1 = json_decode($db_connect->queryFetch($query_2),true);
            //     if($landlord_exists1['data'][0]['CLIENTID']){
            //         $PROPERTY_VENDOR_ID=$landlord_exists1['data'][0]['CLIENTID'];
            //         } else { 
            //             $PROPERTY_VENDOR_ID=0;
            //         }
            // }


            $get_instruct_date = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('CP'.$row)->getValue(), 'd-m-Y H:i:s');
            if($get_instruct_date)
            { 
                $INSTRUCTED_DATE="'".$get_instruct_date."'";
            }
            else
            { 
                $INSTRUCTED_DATE='NULL';
            }
            $PROPERTY_LETTING_SERVICE = '';

            $get_letting_fees = trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
            $get_letting_fees_percent = trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue());
            $get_management_fees =  trim($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue());
            $PROPERTY_LETTING_FEE_TYPE = '';
            if($get_letting_fees==1)
            {
            $PROPERTY_LETTING_FEE_TYPE=1;
            $PROPERTY_LETTING_FEE_FREQUENCY = 'pcm';
            }
            else
            { 
            $PROPERTY_LETTING_FEE_TYPE=2;
            $PROPERTY_LETTING_FEE_FREQUENCY='';
            }
            $PROPERTY_LETTING_FEE = preg_replace("/[^0-9\.]/", '', $get_letting_fees);
            $PROPERTY_MANAGEMENT_FEE = '';
            if(stristr($get_management_fees, '%'))
            {
            $PROPERTY_MANAGEMENT_FEE_TYPE = 1;
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY = end(explode(' ', $get_management_fees));    
            }
            else
            { 
            $PROPERTY_MANAGEMENT_FEE_TYPE=2;
            $PROPERTY_MANAGEMENT_FEE_FREQUENCY='';
            }
            $PROPERTY_MANAGEMENT_FEE = preg_replace("/[^0-9\.]/", '', $get_management_fees);


            $CERTIFICATE_EXPIRE_DATE='';
            $Gas_certificate_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue());
            $PAT_test_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue());
            $Electricity_certificate_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue());
            $EPC_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue());
            $Insurance_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue());
            $Legionella_risk_assessment_date = trim($objPHPExcel->getActiveSheet()->getCell('BJ'.$row)->getValue());
            $Smoke_CO_alarm_expiry_date = trim($objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue());

            $CERTIFICATE_DATE = array('1'=>$Gas_certificate_expiry_date,
                '2'=>$PAT_test_expiry_date, 
                '3'=>$Electricity_certificate_expiry_date,
                '4'=>$EPC_expiry_date, 
                '5'=>$Insurance_expiry_date,
                '6'=>$Legionella_risk_assessment_date, 
                '7'=>$Smoke_CO_alarm_expiry_date);

                $CERTIFICATE_EXPIRE_DATE = json_encode($CERTIFICATE_DATE);


            if($get_property_status == 'Appraisal' || $get_property_status == 'Potential Vendor')
            {
            	$Query2 = "SELECT * FROM `valuations` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
				$Valuation_exists = json_decode($db_connect->queryFetch($Query2),true);
				
				if(@$Valuation_exists['data'][0]['PROPERTY_ID'])
				{
				    
				}
				else
				{
            	$appris++;
            	$PROPERTY_STATUS = 0;
                $sql ="INSERT INTO `valuations` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
	                `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
	                `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
	                `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
	                `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
	                `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
	                `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
	                `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
	                `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
	                `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
	                `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
	                `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
	                `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
	                `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
	                `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
	                `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
	                 VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
	                '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
	                '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
	                '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
	                '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
	                '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
	                '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
	                '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
	                '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
	                '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
	                '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
	                '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
	                '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
	                '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
	                '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
	                '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
	                '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)";
	                $db_connect->queryExecute($sql) or die($sql);
				}
            }
            else {
            	$Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
				$property_exists = json_decode($db_connect->queryFetch($Query1),true);
				
				if(@$property_exists['data'][0]['PROPERTY_ID'])
				{

				}
				else
				{
            	$prope_no++;
				$sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
					`PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
					`PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
					`PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
					`PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
					`KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
					`DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
					`DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
					`CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
					`PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
					`PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
					`PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
					`PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
					`PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
					`PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
					`PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`,`PROPERTY_NOTES`) 
					VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
					'$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
					'$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
					'$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
					'$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
					'$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
					'$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
					'$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
					'$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
					'$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
					'$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
					'$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
					'$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
					'$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', $PROPERTY_CREATED_ON, '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
					'$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
					'$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
					'$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE,'')";
					$db_connect->queryExecute($sql) or die($sql);
				}
            }
        }
    }
    		echo $prope_no." PROPERTY INSERTED SUCCESSFULLY<br><br>";
    		echo $appris." VALUATION INSERTED SUCCESSFULLY";
}
?>