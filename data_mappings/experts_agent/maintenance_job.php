<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='D:\data_migration_bkup\mcmohan/Maintenance.csv';

$thisProceed=true;


try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 





if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            if($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue() =="No Charge"){
                continue;
            }
          
          
        $JOB_REF_NO = $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
      


        $PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $MAINTENANCE_CATEGORY='24';


        $JOB_DESCRIPTION= $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();

        if($JOB_DESCRIPTION==''){
        $JOB_DESCRIPTION.='Maintenance';
        }

       
        $JOB_PRIORITY='';
        $JOB_ESTIMATED_COST='';
        $JOB_WILL_BE_PAID_BY='';
        $JOB_CONTRACTOR_INVOICE_FILE='';
        //$JOB_PAYABLE_USER_ID=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $SUPPLIER_ID = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $JOB_NOTES= $objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
        if($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()!=''){
            $JOB_ON=$JOB_REQUEST_DATE=$JOB_CREATED_ON=date('Y-m-d', strtotime($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()));
        }else{
            $JOB_ON=$JOB_REQUEST_DATE=$JOB_CREATED_ON='';
        }

        $JOB_INVOICE_AMT_CONTRACTOR='';

        
        $maintenance_contact_id = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $maintenance_tenancy_id = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
       
        /*
        $PATTERN = 'cleaning,clean,adjust,repair,service,fitting,installation,work,decoration,replacement,fixing';
        $query= "select transaction_id,landlord_id,transaction_date,description from financial_transaction where contact_id='$maintenance_contact_id' and (property_id='$PROPERTY_ID' or tenancy_id='$maintenance_tenancy_id') and '$PATTERN' LIKE CONCAT(description,'%') ";
*/

        $PATTERN = '[cleaning|clean|adjust|repair|service|fitting|installation|work|decoration|replacement|fixing]';
        $query= "select transaction_id,landlord_id,transaction_date,description from financial_transaction where contact_id='$maintenance_contact_id' and (property_id='$PROPERTY_ID' or tenancy_id='$maintenance_tenancy_id') and description REGEXP '$PATTERN' ";
        $fin_transact = json_decode($db_connect->queryFetch($query),true);

        $financial_transaction_datas = $fin_transact['data'];

        



        

        if(!empty($financial_transaction_datas)){

            foreach($financial_transaction_datas as $financial_transac_data){


                            $financial_transaction_id = $financial_transac_data['transaction_id'];
                            $landlord_id = $financial_transac_data['landlord_id'];
                            $transaction_date = $financial_transac_data['transaction_date']; 

                            if($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()!=''){
                                $JOB_COMPLETED_ON=date('Y-m-d', strtotime($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
                            }else{
                                $transaction_date = Utility::convert_tosqldate($transaction_date, 'd/m/Y h:i:s');
                                $JOB_COMPLETED_ON= (strlen($transaction_date)>0)? date('Y-m-d', strtotime($transaction_date)) : '';
                            }

                            
                            $query_2 = "select * from financialentries where transaction_id='$financial_transaction_id' and entry_type='PI' and cancelled NOT IN ('CANCELLATION','CANCELLED')  limit 2";
                            $financial_entry_data = json_decode($db_connect->queryFetch($query_2),true);
                        
                            $tenancy_id = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
                            $query_3= "select TENANT_ID from lettings where PROPERTY_ID='$PROPERTY_ID' and 	LETTING_CUSTOM_REF_NO='$tenancy_id' ";
                            $lettings_data = json_decode($db_connect->queryFetch($query_3),true);
                            $tenant_id = $lettings_data['data'][0]['TENANT_ID'];
                        

                        
                            $DEBIT =  $financial_entry_data['data'][0]['debit'].',0';
                            if($financial_entry_data['data'][0]['nominal']=='LANDLORDCONTROL'){
                                $JOB_PAYABLE_USER_ID= $landlord_id.','.$SUPPLIER_ID;
                            }else{
                                $JOB_PAYABLE_USER_ID= $tenant_id.','.$SUPPLIER_ID;
                            }

                            $CREDIT = '0,'.$financial_entry_data['data'][1]['credit'];
                        



                            /*
                            $SUPPLIER_ID = '';
                            $tenancy_id = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
                            $query_3= "select TENANT_ID from lettings where PROPERTY_ID='$PROPERTY_ID' and 	LETTING_CUSTOM_REF_NO='$tenancy_id' ";
                            $lettings_data = json_decode($db_connect->queryFetch($query_3),true);
                            $SUPPLIER_ID = $lettings_data['data'][0]['TENANT_ID'];
                    */



                        

                    $sql = "INSERT INTO `maintenance_jobs`(`JOB_REF_NO`, `SUPPLIER_ID`, `PROPERTY_ID`,
                    `JOB_CATEGORY`, `JOB_DESCRIPTION`, `JOB_ON`,  `JOB_PRIORITY`, `JOB_ESTIMATED_COST`,`JOB_INVOICE_AMT_CONTRACTOR`, `JOB_WILL_BE_PAID_BY`, `JOB_PAYABLE_USER_ID`,
                    `JOB_PAYABLE_STATUS`, `JOB_PAYABLE_AMOUNT`, `JOB_PAYABLE_PAID_AMOUNT`, `JOB_REQUEST_DATE`,  `JOB_STATUS`, `JOB_CREATED_ON`,
                    `JOB_COMPLETED_ON`) VALUES ('$JOB_REF_NO','$SUPPLIER_ID','$PROPERTY_ID','$MAINTENANCE_CATEGORY','$JOB_DESCRIPTION','$JOB_ON', '0',
                    '$JOB_ESTIMATED_COST','$JOB_INVOICE_AMT_CONTRACTOR', '$JOB_WILL_BE_PAID_BY','$JOB_PAYABLE_USER_ID','0','$DEBIT', '$CREDIT','$JOB_REQUEST_DATE',
                    '0', '$JOB_CREATED_ON','$JOB_COMPLETED_ON')";
                    echo "<pre>"; print_R( $sql); echo "</pre>";
                    //$db_connect->queryExecute($sql);

                } 

            }               

        }
    }
}      



?>
