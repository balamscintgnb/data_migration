
<?php
error_reporting(E_ERROR);

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\data_migration_bkup\mcmohan\Properties.csv';

$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


                 
            $PROPERTY_DESCRIPTION='';
            $PROPERTY_ADMIN_FEES='';
    
            $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            
            $PROPERTY_STAFF_ID='';
            $PROPERTY_TITLE='';

            $PROPERTY_SHORT_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('BW'.$row)->getValue();
            $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('BU'.$row)->getValue();

            $PROPERTY_CATEGORY=strtoupper($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
        
            if($PROPERTY_CATEGORY=='COMMERCIAL'){
                $PROPERTY_CATEGORY = 'COMMERCIAL SALES';
            }
            
            $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
    
            $get_frequency =Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
            $PROPERTY_PRICE_FREQUENCY='';
            if(stristr($get_frequency,'Monthly') || stristr($get_frequency,'Monthly Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PCM';
            }
            else if(stristr($get_frequency,'Weekly') || stristr($get_frequency,'Weekly Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PW';
            }
            else if(stristr($get_frequency,'Annual') || stristr($get_frequency,'Annual Rental Of') ){ 
                $PROPERTY_PRICE_FREQUENCY='PA';
            }

           
            $PROPERTY_QUALIFIER='';
            if(stristr($PROPERTY_CATEGORY, 'SALE')){
               
                $get_qualifier=Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
                if($get_qualifier == 'Asking Price'){ 
                    $PROPERTY_QUALIFIER='Fixed price';
                }
                else if($get_qualifier == 'Offers in the Region Of'){
                    $PROPERTY_QUALIFIER='Offers in the region of';
                }
                else if($get_qualifier == 'Guide Price'){
                    $PROPERTY_QUALIFIER='Guide price';
                }
                else if($get_qualifier == 'From'){
                    $PROPERTY_QUALIFIER='From';
                }
                else if($get_qualifier == 'Offers in Excess of'){
                    $PROPERTY_QUALIFIER='Offers in excess of';
                }
                else if($get_qualifier == 'Offers Over'){ 
                    $PROPERTY_QUALIFIER='Offers over';
                }

            }
    
    
            ////////////insert date format///////////////
            /*
            $get_available_from = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue(), 'd/m/Y');
            if($get_available_from) { 
                $PROPERTY_AVAILABLE_DATE="'".$get_available_from."'";
            }
            else { 
                $PROPERTY_AVAILABLE_DATE='NULL';
            }*/
            $PROPERTY_AVAILABLE_DATE='NULL';

                /////////////end//////////////
    
    
            $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

            $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()." ".$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
            $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();

            if($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()!=""){
                $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue().', '.$PROPERTY_ADDRESS_CITY.', '.substr($PROPERTY_ADDRESS_POSTCODE,0,3);
            }else{
                $PROPERTY_FORMATTED_ADDRESS= $PROPERTY_ADDRESS_CITY.', '.$PROPERTY_ADDRESS_COUNTY.', '.substr($PROPERTY_ADDRESS_POSTCODE,0,3);
            }
  


            $get_property_status = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
            $PROPERTY_STATUS='';
            if($get_property_status != ''){
                
                if($get_property_status == 'Let By Other Agency'){
                    $get_property_status = 'Externally';
                }elseif($get_property_status == 'Let STC'){
                    $get_property_status = 'Let Agreed';
                }elseif($get_property_status == 'On Market'|| $get_property_status == 'Available to Let' ){
                    $get_property_status = 'Available';
                }elseif($get_property_status == 'Sold By Other Agent'){
                    $get_property_status = 'Externally Sold';
                }elseif($get_property_status == 'Under Negotiation'){
                    $get_property_status = 'On Hold';
                }
                $PROPERTY_AVAILABILITY=$get_property_status;
            }
            else { 
                $PROPERTY_AVAILABILITY='ARCHIVED';
            }
    
            $PROPERTY_ADMIN_FEES='';
          
            $PROPERTY_TYPE=trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            if($PROPERTY_TYPE=='Flat'|| $PROPERTY_TYPE=='Flat Flat'){
                $PROPERTY_TYPE = 'Flat';
            }else if($PROPERTY_TYPE=='Flat Maisonette'){
                $PROPERTY_TYPE='Maisonette';
            }else if($PROPERTY_TYPE=='Flat Semi Detached'){
                $PROPERTY_TYPE='Semi Detached';
            }else if($PROPERTY_TYPE=='Bungalow Detached'){
                $PROPERTY_TYPE='detached bungalow';
            }else if($PROPERTY_TYPE=='House'){
                $PROPERTY_TYPE='house';
            }else if($PROPERTY_TYPE =='House Detached'){
                $PROPERTY_TYPE='detached house';
            }else if($PROPERTY_TYPE=='House End Terrace'){
                $PROPERTY_TYPE='End Terrace'; 
            }else if($PROPERTY_TYPE=='House Maisonette'){
                $PROPERTY_TYPE='Maisonette';
            }else if($PROPERTY_TYPE=='House Semi Detached'){
                $PROPERTY_TYPE='semi-detached house';
            }else if($PROPERTY_TYPE=='House Terraced'){
                $PROPERTY_TYPE='terraced';
            }else if($PROPERTY_TYPE=='Studio Flat'|| $PROPERTY_TYPE=='Studio'){
                $PROPERTY_TYPE='Studio';
            }
          

          
            
    
            $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();
            $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
            $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
            $PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
            $Lease_term_years='';
            $PROPERTY_CLASSIFICATION='';
            $PROPERTY_CURRENT_OCCUPANT='';
            $KITCHEN_DINER='';
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING='';
            $GARDEN='';
            $WHEELCHAIR_ACCESS='';
            $ELEVATOR_IN_BUILDING='';
            $POOL='';
            $GYM='';
            $KITCHEN='';
            $DINING_ROOM='';
            $FURNISHED='';
            $INTERNET='';
            $WIRELESS_INTERNET='';
            $TV='';
    
            $WASHER='';
            $DRYER='';
            $DISHWASHER='';
            $PETS_ALLOWED='';
            $FAMILY_OR_CHILD_FRIENDLY='';
            $DSS_ALLOWED='';
            $SMOKING_ALLOWED='';
            $SECURITY='';
            $HOT_TUB='';
    
            $CLEANER='';
            $EN_SUITE='';
            $SECURE_CAR_PARKING='';
            $OPEN_PLAN_LOUNGE='';
            $VIDEO_DOOR_ENTRY='';
            $CONCIERGE_SERVICES='';
            $PROPERTY_CUSTOM_FEATURES='';
            $PROPERTY_ROOMS='';
            $PROPERTY_ASSETS='';
    
            $PROPERTY_IMAGE_1='';
            $PROPERTY_IMAGE_2='';
            $PROPERTY_IMAGE_3='';
            $PROPERTY_IMAGE_4='';
            $PROPERTY_IMAGE_5='';
            $PROPERTY_IMAGE_6='';
            $PROPERTY_IMAGE_7='';
            $PROPERTY_IMAGE_8='';
            $PROPERTY_IMAGE_9='';
    
            $PROPERTY_IMAGE_10='';
            $PROPERTY_IMAGE_11='';
            $PROPERTY_IMAGE_12='';
            $PROPERTY_IMAGE_13='';
            $PROPERTY_IMAGE_14='';
            $PROPERTY_IMAGE_15='';
            $PROPERTY_IMAGE_FLOOR_1='';
            $PROPERTY_IMAGE_FLOOR_2='';
            $PROPERTY_IMAGE_FLOOR_3='';
            $PROPERTY_IMAGE_FLOOR_4='';
            $PROPERTY_IMAGE_FLOOR_5='';
            $PROPERTY_IMAGE_EPC_1='';
            $PROPERTY_IMAGE_EPC_2='';
            $PROPERTY_IMAGE_EPC_3='';
            $PROPERTY_IMAGE_EPC_4='';
            $PROPERTY_IMAGE_EPC_5='';
            $PROPERTY_EPC_VALUES='';
            $PROPERTY_CREATED_ON='';
    
    
            ///////////////////////////// ADD MORE FIELDS /////////////////////////
          
           
            $PROPERTY_SHORT_DESCRIPTION=addslashes(strip_tags(trim($PROPERTY_SHORT_DESCRIPTION)));
            
            $price_text = $objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
            $PROPERTY_ACCETABLE_PRICE = $price_text;    
            
            if($PROPERTY_ACCETABLE_PRICE!=''){
                $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
            }
            $PROPERTY_DESCRIPTION=addslashes(trim(htmlentities($PROPERTY_DESCRIPTION)));
           
    
    
            $get_name_prop = $objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();
            $vendor_name = trim($get_name_prop);

        
           
            $VENDOR_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
            $vendor_primary_contact = $objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();

            $PROPERTY_VENDOR_ID=0;
            if(stristr($PROPERTY_CATEGORY, 'SALES')){
                if($vendor_name !='' ) {
                    $ventor_qry = "SELECT CLIENTID FROM clients_17_10 WHERE `CLIENT_NAME` LIKE '%$vendor_name%'";
                    if($VENDOR_PRIMARY_EMAIL!=""){
                        $ventor_qry.= " or CLIENT_PRIMARY_EMAIL LIKE '$VENDOR_PRIMARY_EMAIL'";
                    }
                    if($vendor_primary_contact!=""){
                        $ventor_qry.= " or CLIENT_PRIMARY_PHONE LIKE '$vendor_primary_contact'";
                    }    
                    $ventor_qry.="AND CLIENT_TYPE LIKE 'VENDOR'";
                    $vendor_exists = json_decode($db_connect->queryFetch($ventor_qry),true);
                  
                    if($vendor_exists['data'][0]['CLIENTID']){
                        $PROPERTY_VENDOR_ID=$vendor_exists['data'][0]['CLIENTID'];
                    }
                   
                }
            }else{ 
                 $query_1="SELECT `COL2` FROM landlord_property WHERE `COL1` = '$PROPERTY_REF_ID' ";
                $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
                if($landlord_exists['data'][0]['COL2']){
                    $PROPERTY_VENDOR_ID=$landlord_exists['data'][0]['COL2'];
                }
            }


            $get_instruct_date = $objPHPExcel->getActiveSheet()->getCell('CP'.$row)->getValue();
            if($get_instruct_date) { 
                $get_instruct_date = date('Y-m-d',strtotime($get_instruct_date));
                $INSTRUCTED_DATE="'".$get_instruct_date."'";
            }
            else { 
                $INSTRUCTED_DATE='NULL';
            }



            $PROPERTY_LETTING_SERVICE = '';
            $get_letting_fees = $objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $get_letting_fees_percent = $objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
            $get_management_fees =  '';
            $PROPERTY_LETTING_FEE_TYPE=$PROPERTY_PRICE_FREQUENCY;

           
            $PROPERTY_LETTING_FEE = preg_replace("/[^0-9\.]/", '', $get_letting_fees);
            $PROPERTY_MANAGEMENT_FEE='';


            $CERTIFICATE_EXPIRE_DATE='';
           
            $extra_features = array();
            $extra_fea_pattern='\b(\w*Available\w*)\b';

           
            $extra_feature_1 = $objPHPExcel->getActiveSheet()->getCell('BX'.$row)->getValue();
            if($extra_feature_1 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_1)==false){
                array_push($extra_features,($extra_feature_1));
            }

            $extra_feature_2 = $objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue();
            if($extra_feature_2 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_2)==false){
                array_push($extra_features,($extra_feature_2));
            }
            

            $extra_feature_3 = $objPHPExcel->getActiveSheet()->getCell('BZ'.$row)->getValue();
            if($extra_feature_3 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_3)==false){
                array_push($extra_features,($extra_feature_3));
            }

            $extra_feature_4 = $objPHPExcel->getActiveSheet()->getCell('CA'.$row)->getValue();
            if($extra_feature_4 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_4)==false){
                array_push($extra_features,($extra_feature_4));
            }

            $extra_feature_5 = $objPHPExcel->getActiveSheet()->getCell('CB'.$row)->getValue();
            if($extra_feature_5 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_5)==false){
                array_push($extra_features,($extra_feature_5));
            }

            $extra_feature_6 = $objPHPExcel->getActiveSheet()->getCell('CC'.$row)->getValue();
            if($extra_feature_6 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_6)==false){
                array_push($extra_features,($extra_feature_6));
            }

            $extra_feature_7 = $objPHPExcel->getActiveSheet()->getCell('CD'.$row)->getValue();
            if($extra_feature_7 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_7)==false){
                array_push($extra_features,($extra_feature_7));
            }

            $extra_feature_8 = $objPHPExcel->getActiveSheet()->getCell('CE'.$row)->getValue();
            if($extra_feature_8 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_8)==false){
                array_push($extra_features,($extra_feature_8));
            }

            $extra_feature_9 = $objPHPExcel->getActiveSheet()->getCell('CF'.$row)->getValue();
            if($extra_feature_9 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_9)==false){
                array_push($extra_features,($extra_feature_9));
            }

            $extra_feature_10 = $objPHPExcel->getActiveSheet()->getCell('CG'.$row)->getValue();
            if($extra_feature_10 !="" && preg_match("/$extra_fea_pattern/i",$extra_feature_10)==false){
                array_push($extra_features,($extra_feature_10));
            }

           

            if(!empty($extra_features)){
                $PROPERTY_CUSTOM_FEATURES=addslashes(json_encode($extra_features));

               
            }
           
    
            if($get_property_status == 'Appraisal' || $get_property_status == 'Potential Vendor') {
                continue;
                 $sql ="INSERT INTO `valuations` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
                 `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
                 `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
                 `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
                 `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
                 `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
                 `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
                 `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
                 `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
                 `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
                 `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
                 `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
                 `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
                 `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
                 `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
                 `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
                  VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                 '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
                 '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                 '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                 '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                 '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                 '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                 '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                 '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                 '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                 '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                 '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                 '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                 '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
                 '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
                 '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
                 '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)"; 
                 // $sql =" UPDATE `properties` SET `PROPERTY_TITLE`= '', `PROPERTY_TYPE`='$PROPERTY_TYPE', `PROPERTY_SHORT_DESCRIPTION`='$PROPERTY_SHORT_DESCRIPTION',
                  //`PROPERTY_DESCRIPTION`='$PROPERTY_DESCRIPTION' , PROPERTY_CUSTOM_FEATURES='$PROPERTY_CUSTOM_FEATURES',PROPERTY_FORMATTED_ADDRESS='$PROPERTY_FORMATTED_ADDRESS'  WHERE `PROPERTY_ID` = '$PROPERTY_REF_ID'"; 

            }
            else { 
                 $sql =" UPDATE `properties` SET `PROPERTY_TITLE`= '',  `PROPERTY_TYPE`='$PROPERTY_TYPE',  `PROPERTY_SHORT_DESCRIPTION`='$PROPERTY_SHORT_DESCRIPTION',
                 `PROPERTY_DESCRIPTION`='$PROPERTY_DESCRIPTION', PROPERTY_CUSTOM_FEATURES='$PROPERTY_CUSTOM_FEATURES',PROPERTY_FORMATTED_ADDRESS='$PROPERTY_FORMATTED_ADDRESS' WHERE `PROPERTY_ID` = '$PROPERTY_REF_ID'"; 
                 
                //  ", '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
                // '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
                // '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
                // '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
                // '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
                // '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
                // '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
                // '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
                // '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
                // '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
                // '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
                // '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
                // '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
                // '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','',
                // '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
                // '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
                // '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)"; 
            }

            

                //insert into table    
                $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>