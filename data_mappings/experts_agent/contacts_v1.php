<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

//$file_name='D:\Board\Mcmahon_ExpertAgent/Contacts.csv';
$file_name='D:\Board\ideal_expertagent/Contacts.csv';
$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else {
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

/* General
Utility
Local Authorities
Builders
Locksmith
Heating Services
Solicitors
Mortgage Advisor
Decorator
Agency
Accountant
Plumbers
Cleaning Services

*/

            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();

            $CLIENT_TITLE='';
            $name1=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            $name2=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());

            if($name2!='')
            $CLIENT_NAME=$name1.' & '.$name2;
            else
            $CLIENT_NAME=$name1;



            $COMPANY_NAME=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();



            $get_contact_type=strtolOwer($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
            $CLIENT_TYPE='';
            $CLIENT_SUB_TYPE='';
                if($get_contact_type == 'general' || $get_contact_type == 'utility' ||$get_contact_type == 'locksmith' || $get_contact_type == 'heating services' ||$get_contact_type == 'mortgage advisor' ||$get_contact_type == 'decorator'  ||$get_contact_type == 'accountant'  ||$get_contact_type == 'plumber'   ||$get_contact_type == 'cleaning services'   )
                {
                    $CLIENT_TYPE='SUPPLIER';
                    $CLIENT_SUB_TYPE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
                    continue;
                }
                else if ($get_contact_type == 'builders' || $get_contact_type == 'agency'){
                    $CLIENT_TYPE='LANDLORD';
                    $CLIENT_SUB_TYPE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
                }
                else if ($get_contact_type == 'solicitors' || $get_contact_type == 'Local Authorities' ){
                    $CLIENT_TYPE='SOLICITOR';
                    $CLIENT_SUB_TYPE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
                    continue;
                }else {
                    $CLIENT_TYPE='ADDRESSBOOK';
                    $CLIENT_SUB_TYPE='';
                    continue;
                }




            $CLIENT_STATUS=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue().'. '.$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
            $CLIENT_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
            $CLIENT_ADDRESS_TOWN=$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();
            $CLIENT_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $CLIENT_ACCOUNT_NO=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
            $CLIENT_ACCOUNT_SORTCODE=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
            $CLIENT_FAX_1=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON=date('Y-m-d');




        if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
			if($CLIENT_NAME==''){

                $CLIENT_NAME=$COMPANY_NAME;
            }
		}

        $get_department = strtolower($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        if($get_department == 'residential sales')
        $SEARCH_CATEGORY = 43;
        else if($get_department == 'residential lettings')
        $SEARCH_CATEGORY = 44;
        else if($get_department == 'Commercial sales')
        $SEARCH_CATEGORY = 45;
        else if($get_department == 'Commercial lettings')
        $SEARCH_CATEGORY = 46;

        $PROPERTY_LOCATIONS = $objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue();
        $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue(),
        'property_applicant_search_attribute_2_from'=>$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue(),
        'property_applicant_search_attribute_2_to'=>$MAX_BEDS,
        'property_applicant_search_attribute_3_from'=>$MIN_BATH,
        'property_applicant_search_attribute_3_to'=>$MAX_BATH,
        'property_applicant_search_attribute_4_from'=>'',
        'property_applicant_search_attribute_4_to'=>'',
        'property_applicant_search_attribute_6'=>'',
        'property_applicant_search_attribute_8'=>'',
        'property_applicant_search_attribute_15'=>$FURNISHED,
        'property_applicant_search_attribute_22'=>'',
        'property_applicant_search_attribute_24'=>$dss_allowed,
        'property_applicant_search_attribute_30'=>''
    );

     $SEARCH_CRITERIA_APPLICANT=array('price'=>$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue()
         ,'pricefrom'=>$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue(),'filter_array'=>$applicant_search_criteria,
         'frequency'=>'','category'=>$SEARCH_CATEGORY,'location'=>$PROPERTY_LOCATIONS);

         $SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);


    $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`, `RECORD_UPLOADED`)
    VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA',0)";


        $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>
