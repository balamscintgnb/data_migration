<?php

/**
 * Parser for Rightmove's lovely BLM files.
 */

class ParseBLM {

    private $blm;
    private $dataCount = 0;

    private $header = array();
    private $def = array();
    private $data = array();
    
    private $status_text=true;

    public $STATUS_ID = array(  "Available",
        "SSTC (Sales only)",
        "SSTCM(Scottish Sales only)",
        "Under Offer (Sales only)",
        "Reserved (Lettings only)",
        "Let Agreed (Lettings only)"
    );

    public $PRICE_QUALIFIER     = array(
        0 => "Default",
        1 => "Price on application",
        2 => "Guide price",
        3 => "Fixed price",
        4 => "Offers in excess of",
        5 => "Offers in the region of",
        6 => "Sale by tender",
        7 => "From",
        9 => "Shared Ownership",
        10 => "Offers over",
        11 => "Part Buy Part Rent",
        12 => "Shared Equity"
    );

    public $PUBLISHED_FLAG      = array(0 => "Hidden/invisible", 1 => "Visible");
    public $LET_TYPE_ID         = array(0=>"Not Specified", 1=>"Long Term", 2=>"Short Term", 3=>"Student", 4=>"Commercial");
    public $LET_FURN_ID         = array(0 => "Furnished", 1 => "Part Furnished", 2 => "Unfurnished", 3 => "Not Specified", 4=>"Furnished/Un Furnished");
    public $LET_RENT_FREQUENCY  = array(0 => "Weekly", 1 => "Monthly", 2 => "Quarterly", 3 => "Annual");
    public $TENURE_TYPE_ID      = array(1 => "Freehold", 2 => "Leasehold", 3 => "Feudal", 4 => "Commonhold", 5 => "Share of Freehold");
    public $TRANS_TYPE_ID       = array(1 => "Resale", 2=> "Lettings");
    public $NEW_HOME_FLAG       = array("Y" => "New Home", "N" => "Non New Home");
    
    public $PROP_SUB_ID         = array(
        0=>"Not Specified",
        1=>"Terraced",
        2=>"End of Terrace",
        3=>"Semi-Detached",
        4=>"Detached",
        5=>"Mews",
        6=>"Cluster House",
        7=>"Ground Flat",
        8=>"Flat",
        9=>"Studio",
        10=>"Ground Maisonette",
        11=>"Maisonette",
        12=>"Bungalow",
        13=>"Terraced Bungalow",
        14=>"Semi-Detached Bungalow",
        15=>"Detached Bungalow",
        16=>"Mobile Home",
        17=>"Hotel",
        18=>"Guest House",
        19=>"Commercial Property",
        20=>"Land",
        21=>"Link Detached House",
        22=>"Town House",
        23=>"Cottage",
        24=>"Chalet",
        25=>"Unique",
        26=>"House",
        27=>"Villa",
        28=>"Apartment",
        29=>"Penthouse",
        30=>"Finca",
        43=>"Barn Conversion",
        44=>"Serviced Apartments",
        45=>"Parking",
        46=>"Sheltered Housing",
        47=>"Retirement Property",
        48=>"House Share",
        49=>"Flat Share",
        50=>"Park Home",
        51=>"Garages",
        52=>"Farm House",
        53=>"Equestrian",
        56=>"Duplex",
        59=>"Triplex",
        62=>"Longere",
        65=>"Gite",
        68=>"Barn",
        71=>"Trulli",
        74=>"Mill",
        77=>"Ruins",
        80=>"Restaurant",
        83=>"Cafe",
        86=>"Mill",
        89=>"Trulli",
        92=>"Castle",
        95=>"Village House",
        101=>"Cave House",
        104=>"Cortijo",
        107=>"Farm Land",
        110=>"Plot",
        113=>"Country House",
        116=>"Stone House",
        117=>"Caravan",
        118=>"Lodge",
        119=>"Log Cabin",
        120=>"Manor House",
        121=>"Stately Home",
        125=>"Off-Plan",
        128=>"Semi-detached Villa",
        131=>"Detached Villa",
        134=>"Bar",
        137=>"Shop",
        140=>"Riad",
        141=>"House Boat",
        142=>"Hotel Room",
        178=>"Office",//todo check
        
        143=>"Block of Apartments",
		144=>"Private Halls",
		181=>"Business Park",
		184=>"Serviced Office",
		187=>"Retail Property (High Street)",
		190=>"Retail Property (Out of Town)",
		193=>"Convenience Store 196 Garages",
		199=>"Hairdresser / Barber Shop",
		202=>"Hotel",
		205=>"Petrol Station",
		
		208=>"Post Office",
		211=>"Pub",
		214=>"Workshop & Retail Space",
		217=>"Distribution Warehouse",
		220=>"Factory",
		223=>"Heavy Industrial",
		226=>"Industrial Park",
		229=>"Light Industrial",
		232=>"Storage",
		235=>"Showroom",
		238=>"Warehouse",
		241=>"Land (Commercial)",
		244=>"Commercial Development",
		247=>"Industrial Development",
		250=>"Residential Development",
		253=>"Commercial Property",
		256=>"Data Centre",
		259=>"Farm",
		262=>"Healthcare Facility",
		265=>"Marine Property",
		268=>"Mixed Use",
		271=>"Research & Development Facility",
		274=>"Science Park",
		277=>"Guest House",
		280=>"Hospitality",
		283=>"Leisure Facility",
		298=>"Takeaway",
		301=>"Childcare facility",
		304=>"Smallholding 307 Place of Worship",
		310=>"Trade Counter",
		511=>"Coach House",
		512=>"House of Multiple Occupation",
		535=>"Sports Facility",
		538=>"Spa",
		541=>"Campsite & Holiday Village"
    );
    
    public $PROP_SUB_COMMERCIAL_ID         = array(19, 80, 83, 86, 134, 137, 178, 181, 184, 187, 190, 193, 196, 199, 202, 205, 208, 211, 214, 217, 220, 223, 226, 229, 232, 235, 238, 241, 244, 247, 250, 253, 256, 259, 262, 265, 268, 271, 274, 277, 280, 283, 298, 301, 307, 310, 512, 535, 538, 541);


    public function __construct($file_content, $status_text=true) {
        
        if(!isset($status_text)){
            $status_text=true;
        }
        
        $this->status_text=$status_text;
        
        if(trim($file_content)!=''){
            $this->blm = $file_content;
            $this->splitPieces();
        }else{
            $this->dataCount=0;
        }
    }

    //Return specific field from row
    /*public function getData($data, $row) {
        return $this->data[$row][$data];
    }*/

    // Return header info
    public function getHeader($hdr) {
        return $this->header[$hdr];
    }
    
    public function getHeaderAll() {
        return $this->header;
    }

    public function getDefinition() {
        return $this->def;
    }

    // This will return the actual number of properties, $blm->getHeader('Property Count');
    public function propCount() {
        return $this->dataCount;
    }

    public function properties() {
        return $this->data;
    }

    // Splits the BLM data into constituent parts
    private function splitPieces() {
        $pieces = explode("#", $this->blm);
        
        // Get the header (includes EOF/EOR stuff)
        $header = explode("\n", trim($pieces[2]));
        foreach ($header as $h) {
            $h = preg_replace("/\'/", "", $h);	// Remove quotes on EOF/EOR
            $h_pieces = explode(" : ", $h);
            $this->header[trim($h_pieces[0])] = trim($h_pieces[1]);
        }

        // Get the definitions
        $def_row = explode($this->header['EOR'], trim($pieces[4]));
        $def = explode($this->header['EOF'], trim($def_row[0]));

        $def_field_count=sizeof($def);
        foreach ($def as $key => $value) {
            if($key<$def_field_count-1) {
                $this->def[] = $value;
            }
        }

        //Get the Data
        $data_tag_length=strlen('#DATA#');

        $data_length = strpos($this->blm, '#END#' )-strpos($this->blm,'#DATA#')-$data_tag_length;
        $data =  substr($this->blm,strpos($this->blm,'#DATA#')+$data_tag_length, $data_length);

        $data_array = explode($this->header['EOR'],$data);
        
        //property missing bug fix remove count-1
        $this->dataCount = count($data_array);

        $data_array = array_filter($data_array,array($this,"cleanArray"));
        array_walk($data_array, array($this, 'trimArray'));//trim the lines

        for ($i=0; $i<$this->dataCount-1; $i++) {
            $row = explode($this->header['EOF'], trim($data_array[$i]));
            $row_arr=array();
            for ($j=0; $j<count($row)-1; $j++) {
                
                $row_arr[$this->def[$j]] = $row[$j]; //utf8_encode(

                //put few text values for status code from manual
                if($this->status_text){
                    
                    if(strstr($this->def[$j],"STATUS_ID") && $row[$j]!=''){
                        $row_arr['STATUS_ID_TEXT'] = $this->STATUS_ID[$row[$j]];
                    }
    
                    if(@strstr($this->def[$j],"PRICE_QUALIFIER") && $row[$j]!=''){
                        $row_arr['PRICE_QUALIFIER_TEXT'] = @$this->PRICE_QUALIFIER[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"PUBLISHED_FLAG") && $row[$j]!=''){
                        $row_arr['PUBLISHED_FLAG_TEXT'] = $this->PUBLISHED_FLAG[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"LET_TYPE_ID") && $row[$j]!=''){
                        $row_arr['LET_TYPE_ID_TEXT'] = $this->LET_TYPE_ID[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"LET_FURN_ID") && $row[$j]!=''){
                        $row_arr['LET_FURN_ID_TEXT'] = $this->LET_FURN_ID[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"LET_RENT_FREQUENCY") && $row[$j]!=''){
                        $row_arr['LET_RENT_FREQUENCY_TEXT'] = $this->LET_RENT_FREQUENCY[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"TENURE_TYPE_ID") && $row[$j]!=''){
                        $row_arr['TENURE_TYPE_ID_TEXT'] = $this->TENURE_TYPE_ID[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"TRANS_TYPE_ID") && $row[$j]!=''){
                        $row_arr['TRANS_TYPE_ID_TEXT'] = $this->TRANS_TYPE_ID[$row[$j]];
                    }
    
                    if(strstr($this->def[$j],"NEW_HOME_FLAG") && $row[$j]!=''){
                        $row_arr['NEW_HOME_FLAG_TEXT'] = $this->NEW_HOME_FLAG[$row[$j]];
                    }
                    
                    if(@strstr($this->def[$j],"PROP_SUB_ID") && $row[$j]!=''){
                        $row_arr['PROP_SUB_ID_TEXT'] = @$this->PROP_SUB_ID[$row[$j]];
                    }
                }

            }
            $this->data[] = $row_arr;
        }
    }

    //filter array, ommit empty value
    private function cleanArray($var){
        $replace_chars = array(" ","\n","\r","\t");
        $var = trim(str_replace($replace_chars,"",$var));
        return (isset($var) && $var!='');
    }

    //trim an array
    private function trimArray(&$var){
    	//todo check
        $var = utf8_encode(trim($var));
        //$var = mb_convert_encoding($var, 'UTF-8');
        //$var = $this->format_content($var);
        //utf8_encode, utf8_decode
    }
    
    //extras
    public static function is_dir_empty($dir) {
        
        if (!is_readable($dir)) return FALSE; 
        
        $i=0;
        
        $handle = opendir($dir);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $i++;
            }
        }
        
        closedir($handle); 
        
        if($i>0){
            return FALSE;
        }
         
        return TRUE;
    }
    
    public static function delete_files_by_date($dir, $threshold_date, $file_types, $date_format){

        if (is_dir($dir)){
            
        	if ($dh = opendir($dir)){
        	    
        		while (($file = readdir($dh)) !== false){
        		    
        		    $file_ext = explode(".", $file);
        		    $file_ext = strtolower($file_ext[count($file_ext)-1]);
        		    
        			if($file!='' && $file!='.' && $file!='..' && (count($file_types)<=0 || in_array($file_ext, $file_types))){
        			    
                        $file_date = date ($date_format, filemtime($dir.'/'.$file));
                        
                        if(strtotime($file_date) < strtotime($threshold_date)){
                            @unlink($dir.'/'.$file);
                        }
        			}
        		}
        	}
        }
    }
    
    public static function delete_files_recursive($dir, $file_types){

        if (is_dir($dir)) {
            
            $objects = scandir($dir);
            
            foreach ($objects as $object) {
                
                if ($object!='' && $object != "." && $object != "..") {
                    
                    if (is_dir($dir . "/" . $object)) {
                        self::delete_files_recursive($dir . "/" . $object, $file_types);
                    }else{
                        $file_ext = explode(".", $object);
                        $file_ext = strtolower($file_ext[count($file_ext)-1]);
                        
                        if(count($file_types)<=0 || in_array($file_ext, $file_types)){
                            @unlink($dir . "/" . $object);
                        }
                    }
                }
            }
            
            if(self::is_dir_empty($dir)){
                @rmdir($dir);
            }
        }
    }
    
    public static function delete_files_recursive_by_date($dir, $file_types, $threshold_date, $date_format){

        if (is_dir($dir)) {
            
            $objects = scandir($dir);
            
            foreach ($objects as $object) {
                
                if ($object!='' && $object != "." && $object != "..") {
                    
                    if (is_dir($dir . "/" . $object)) {
                        self::delete_files_recursive_by_date($dir . "/" . $object, $file_types, $threshold_date, $date_format);
                    }else{
                        $file_ext = explode(".", $object);
                        $file_ext = strtolower($file_ext[count($file_ext)-1]);
                        
                        if(count($file_types)<=0 || in_array($file_ext, $file_types)){
                            
                            $file_date = date ($date_format, filemtime($dir.'/'.$object));
                            
                            if(strtotime($file_date) < strtotime($threshold_date)){
                                @unlink($dir.'/'.$object);
                            }
                        }
                    }
                }
            }
            
            if(self::is_dir_empty($dir)){
                rmdir($dir);
            }
        }
    }
    
    public static function strposArray($needle, $array){
        
        foreach($array as $string){
            
            if(strpos($needle, $string) !== false) {
                return true;
            }
        }
        return false;
    }
    
    public static function format_content($in_string){ 

	    //$in_string =  iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $in_string);
	    // //IGNORE
	    $in_string =  iconv(mb_detect_encoding($in_string, mb_detect_order(), true), "UTF-8//TRANSLIT", $in_string);
	    $in_string = addslashes(preg_replace('/^\p{Z}+|\p{Z}+$/u', '', $in_string));
	    return $in_string;
	}
}
?>