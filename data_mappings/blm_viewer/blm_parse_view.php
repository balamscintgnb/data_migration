<?php
/*// Report all PHP errors (see changelog)
error_reporting(E_ALL);

// Report all PHP errors
error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);*/

ini_set('default_charset', 'utf-8');
header('Content-Type: text/html; charset=utf-8');

require_once 'ParseBLM_class.php';
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
  <meta charset="utf-8">
  <title>BLM Viewer</title>
</head>
<body style="padding: 5px; margin: 5px;">

<!--<div style="width:100%; overflow-x:scroll;">-->
<?php

extract($_GET);

$today_date='blm_files';

//$property_type_array = array('barn_conversion', 'block_of_flats', 'bungalow', 'business_park', 'chalet', 'chateau', 'cottage', 'country_house', 'detached', 'detached_bungalow', 'end_terrace', 'equestrian', 'farm', 'farmhouse', 'finca', 'flat', 'hotel', 'houseboat', 'industrial', 'land', 'leisure', 'light_industrial', 'link_detached', 'lodge', 'longere', 'maisonette', 'mews', 'office', 'park_home', 'parking', 'pub_bar', 'restaurant', 'retail', 'riad', 'semi_detached', 'semi_detached_bungalow', 'studio', 'terraced', 'terraced_bungalow', 'town_house', 'villa', 'warehouse', 'retirement', 'house', 'garage', 'barn', 'mobile_home', 'bedsit', 'commercial', 'other');
                    
$table_name = array();

if (is_dir($today_date)){

    $century21=true;

    if ($dh = opendir($today_date)){
		
		$total_count=0;

        while (($file = readdir($dh)) !== false){

            $file_ext = explode(".", $file);
            $file_ext = strtolower($file_ext[count($file_ext)-1]);
            $break=0;

            if($file!='' && $file!='.' && $file!='..' && ( $file_ext=='blm'|| $file_ext=='blt') && file_exists($today_date.'/'.$file)){

                $file_contents = file_get_contents($today_date.'/'.$file);
				
				echo "<br/><b>File - $file.</b><br/>";
                                
                if($century21){
                    $file_contents = str_replace('search~', 'search-', $file_contents);
                }

                $blm = new ParseBLM($file_contents, false);

                $properties = $blm->properties();

                $this_headers = $blm->getDefinition();

                if($properties>0){

                    $this_count=sizeof($properties);
                    
					$total_count+=$this_count;
					
                    echo "<b>TOTAL Count $total_count, This Count $this_count,</b><br/>";
                    
                    echo '<table cellspacing="3" cellpadding="3" class="table table-striped table-bordered" id="table_'.$this_count.'" style="width:100%">';

                    $table_name[]='table_'.$this_count;

                    echo '<thead><tr>';

                    foreach($this_headers as $header){
                        echo '<th>'.$header.'</th>';
                    }
                    echo '</tr></thead>';

                    echo '<tbody>';
					
                    foreach($properties as $property){
						
                        echo '<tr>';
                        foreach($property as $key=>$property_detail){
                            echo '<td>';

                            if($key=='SUMMARY' || $key=='DESCRIPTION'){
                                $property_detail = substr($property_detail, 0 , 100);
                            }

                            echo htmlspecialchars(utf8_encode($property_detail));
                            //var_dump($property['AGENT_REF'], $property['TRANS_TYPE_ID'], $property['ADDRESS_1'], $property['ADDRESS_2'],  $property['ADDRESS_3'],  $property['TOWN'], $property['BRANCH_ID'], $property['STATUS_ID_TEXT'], $property['PRICE'], $property['PRICE_QUALIFIER_TEXT'], $property['PROP_SUB_ID_TEXT'] , $property['DISPLAY_ADDRESS'], $property['PUBLISHED_FLAG_TEXT'], $property['LET_RENT_FREQUENCY'], $property['TRANS_TYPE_ID_TEXT'], $property['CREATE_DATE'], $property['UPDATE_DATE']);
                            echo '</td>';
                            //break;
                        }
                        echo '</tr>';
                        //break;
                    }

                    echo '</tbody>';

                    echo '<tfoot><tr>';

                    foreach($this_headers as $header){
                        echo '<th>'.$header.'</th>';
                    }
                    echo '</tr></tfoot>';

                    echo '</table><br/><br/>';
                }
            }
        }
    }
}

echo "<b>Final TOTAL Count $total_count, This Count $this_count</b>";

?>
<!--</div>-->

<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" language="javascript" >
$(document).ready(function() {
    <?php
    foreach($table_name as $table){
        ?>
        $('#<?=$table;?>').DataTable({
            "searching": true,
            dom: 'Bfrtip',
            "order": [[ 0, 'asc' ]],
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
        <?php
    }
    ?>
} );
</script>

</body>
</html>