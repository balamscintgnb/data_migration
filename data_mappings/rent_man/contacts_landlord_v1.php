<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/marlborough/landlord.xls';

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            $CLIENT_TITLE = '';
            $title = trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            $name1 = trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $name2 = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $title2 = trim($objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue());
            $name3 = trim($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue());
            $name4 = trim($objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue());
            $title3 = trim($objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue());
            $name5 = trim($objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue());
            $name6 = trim($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue());
            $CLIENT_NAME_3 = addslashes(trim(str_replace("  "," ",$title3." ".$name5." ".$name6)));
            $CLIENT_NAME_2 = addslashes(trim(str_replace("  "," ",$title2." ".$name3." ".$name4)));
            $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$title." ".$name1." ".$name2)));

            if($CLIENT_NAME == $CLIENT_NAME_3){
                $CLIENT_NAME_3 = '';
            }
            if($CLIENT_NAME_2 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " & ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_2;
            }
            if($CLIENT_NAME_3 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " & ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_3;
            }
            $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$CLIENT_NAME)));
            // echo "<pre>"; print_r($CLIENT_NAME); echo "</pre>";
            $COMPANY_NAME = '';
            $CLIENT_TYPE='landlord';
            $CLIENT_SUB_TYPE = '';
            $CLIENT_STATUS = '';
            $CLIENT_STAFF_ID = '';
            $CLIENT_PRIMARY_EMAIL = $objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE = trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());

            $CLIENT_ADDRESS_LINE_1 = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2 = trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $CLIENT_ADDRESS_CITY = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
            $CLIENT_ADDRESS_TOWN = '';
            $CLIENT_ADDRESS_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());

            
            $CLIENT_ADDRESS1_LINE_1 = trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue());
            $CLIENT_ADDRESS1_LINE_2 = trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue());
            $CLIENT_ADDRESS1_CITY = trim($objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue());
            $CLIENT_ADDRESS1_TOWN = trim($objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue());
            $CLIENT_ADDRESS1_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue());

            $CLIENT_ADDRESS2_LINE_1 = trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
            $CLIENT_ADDRESS2_LINE_2 = trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
            $CLIENT_ADDRESS2_CITY = trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
            $CLIENT_ADDRESS2_TOWN = '';
            $CLIENT_ADDRESS2_POSTCODE = trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());

            $CLIENT_ACCOUNT_NAME = trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
            $CLIENT_ACCOUNT_NO = trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());
            $CLIENT_ACCOUNT_SORTCODE = trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());


            $E_mail = trim($objPHPExcel->getActiveSheet()->getCell('CW'.$row)->getValue());

            preg_match('/\b[^\s]+@[^\s]+/', $E_mail, $match1);
            
            $CLIENT_EMAIL_1 = '';
            $CLIENT_EMAIL_1 = $match1[0];
            $CLIENT_EMAIL_2 = '';
            $CLIENT_EMAIL_3 = '';
            $CLIENT_EMAIL_4 = '';
            $CLIENT_EMAIL_5 = '';
            //echo "<pre>"; print_r($CLIENT_EMAIL_1); echo "</pre>";
            $mobile_number = trim($objPHPExcel->getActiveSheet()->getCell('CV'.$row)->getValue());
            $mobile_number_1 = explode(";",$mobile_number);

            $F_num = "";
            $s_num = "";
            $F_num = $mobile_number_1[0];
            $s_num = $mobile_number_1[1];

            $CLIENT_PHONE_1 = trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
            $CLIENT_PHONE_2 = $F_num;
            $CLIENT_PHONE_3 = $s_num;
            $CLIENT_PHONE_4 = '';
            $CLIENT_PHONE_5 = '';


            $CLIENT_MOBILE_1 = '';
            $CLIENT_MOBILE_2 = trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue());
            $CLIENT_MOBILE_3 = trim($objPHPExcel->getActiveSheet()->getCell('BM'.$row)->getValue());
            $CLIENT_MOBILE_4 = '';
            $CLIENT_MOBILE_5 = '';
            $CLIENT_NOTES = '';
            $CLIENT_FAX_1 = trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            $CLIENT_FAX_2 = '';
            $CLIENT_FAX_3 = '';
            $CLIENT_FAX_4 = '';
            $CLIENT_FAX_5 = '';
            $CLIENT_CREATED_ON = '';
          
            if($CLIENT_PRIMARY_EMAIL=='.'){
                $CLIENT_PRIMARY_EMAIL="";
            }
            
        
        $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME',
                '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '0')";
               echo "<br/><br/>";
            echo$row;
        $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>