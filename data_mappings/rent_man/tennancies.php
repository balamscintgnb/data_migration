<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/rent_man/bel/deals.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

	          $id='';
	          $LETTING_ID='';
	          $LETTING_CUSTOM_REF_NO=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
	          $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
	          $PROPERTY_ROOM_ID='';

			$name1=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            $name2=trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
            $name3=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
            $name4=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
            $name5=trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
            $name6=trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());

            $tenant_name_array=array();

            if($name1!=''){
                $tenant_name_array[]=$name1;
            }

            if($name2!=''){
                $tenant_name_array[]=$name2;
            }

            if($name3!=''){
                $tenant_name_array[]=$name3;
            }

            if($name4!=''){
                $tenant_name_array[]=$name4;
            }

            if($name5!=''){
                $tenant_name_array[]=$name5;
            }

            if($name6!=''){
                $tenant_name_array[]=$name6;
            }

						//$tenant_array=array_unique($tenant_name_array);


						$tenant_i=0;

						$this_contact_id=0;
						$shared_tenant_contact_array = array();

						foreach($tenant_name_array as $CLIENT_NAME){

							$query_client="SELECT *FROM `clients` WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME'";

			        $data = json_decode($db_connect->queryFetch($query_client),true);

							$client_exist=$data['data'];


							$contact_id=$data['data'][0]['CLIENTID'];


							//echo count($client_exist);

							if(count($client_exist)==0){

	 							echo $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
									`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
									`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
									`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
									`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
									`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
									`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`) VALUES
									('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', 'tenant', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
									'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
									'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
									'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
									'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
									'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
									'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";

					//echo $row;
						 			$contact_id=$db_connect->queryExecute($sql) or die ($sql);

									$update="UPDATE `clients` SET `CLIENTID`='$contact_id' where `id`='$contact_id'";

									$db_connect->queryExecute($update);

								}

								if($tenant_i==0){
										$tenant_i++;
										$this_contact_id=$contact_id;
									 // echo " API: $notice_api_item_id Tenant rentman inserted (Mapped to lettings) - $tenant_name ";
								}else{
									$shared_tenant_contact_array[]=$contact_id;
								}



			   }

         $TENANT_ID=$this_contact_id;

				 $SHARED_TENANT_IDS = json_encode($shared_tenant_contact_array);

					$LETTING_START_DATE='';

	        $given_date1=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getformattedValue(),'d/m/Y');

					if($given_date1!=''){
              $LETTING_START_DATE=$given_date1;
          }
          if($given_date1==''){
              $LETTING_START_DATE="0000-00-00";
          }

	        $LETTING_END_DATE='';

	        $given_date2=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getformattedValue(),'d/m/Y');

          if($given_date2!=''){
							$LETTING_END_DATE=$given_date2;
          }
          if($given_date2==''){
              $LETTING_END_DATE="0000-00-00";
          }


					$rent = $objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();

					$weekly_rent = $objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();

					$deposit = $objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();

					$agent_hold=$objPHPExcel->getActiveSheet()->getCell('CO'.$row)->getValue();

					$holder=$objPHPExcel->getActiveSheet()->getCell('CP'.$row)->getValue();

					$notes=$objPHPExcel->getActiveSheet()->getCell('DI'.$row)->getValue();

				  $deposit_holder=2;
          if(!stristr($agent_hold, 'TRUE')){
              if(stristr($holder, 'Agent')){
                  $deposit_holder=3;
              }
          }

          $rent_frequency = $objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();

            /*pcm,pw,pd,per term,p2w,p4w,p6m,pa,psa",pq",pppw,pppn*/
            /*2 	Fortnightly
			1 	FourWeekly
			75 	Monthly
			1 	TwiceMonthly
			157 	Weekly*/
						$rentFrequencyActual='';
            if(stristr($rent_frequency, 'TwiceMonthly') || stristr($rent_frequency, 'Monthly')){
                $rentFrequencyActual='pcm';
            }else if(stristr($rent_frequency, 'FourWeekly')){
                $rentFrequencyActual='p4w';
            }else if(stristr($rent_frequency, 'Weekly')){
                $rentFrequencyActual='pw';
                $rent=$weekly_rent;
            }else if(stristr($rent_frequency, 'Fortnightly')){
                $rentFrequencyActual='p2w';
            }else if($rent_frequency=='HalfYearly'){
                $rentFrequencyActual='p6m';
            }else if(stristr($rent_frequency, 'Yearly')){
                $rentFrequencyActual='pa';
            }


					$LETTING_RENT=$rent;
          $LETTING_RENT_FREQUENCY=$rentFrequencyActual;
          $LETTING_RENT_PAYMENT_FREQUENCY=$rentFrequencyActual;

					$break_when = $objPHPExcel->getActiveSheet()->getCell('GL'.$row)->getValue();

          $break_clause = $objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();

            $break_term_when=-1;

            if(stristr($break_clause, 'TRUE')){
                $break_term_when=$break_when;
            }

            $advance=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();

            $terms= $objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();

            $termType='months';

            //TRUE FALSE
            $managed_by= $objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();

            //Managed for Gnomen
            $managed_level=2;

            if(stristr($managed_by, 'TRUE')){
                $managed_level=1;
            }

            $deal_notes= $objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue();

            if($deal_notes!=''){
            	 if($notes!=''){
            	 	$notes.="\n";
            	 }
            	$notes.=$deal_notes;
            }

						$first_rent= $objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue();

						if($first_rent!=''){
            	 if($notes!=''){
            	 	$notes.="\n";
            	 }
            	$notes.='First rent: '.$first_rent;
            }


            $vat_rate=$objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();

            $inventory_fee_amount = $objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();

            //$name12=trim($tenancy['Quarterly']);//Y

            //$deposit = (float)trim($tenancy['Vat']);

          $com_rate = $objPHPExcel->getActiveSheet()->getCell('DN'.$row)->getValue();

          $contract_amount = $objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();

          $LETTING_RENT_PAYMENT_DAY='';
          $LETTING_RENT_ADVANCE=$advance;
          $LETTING_RENT_ADVANCE_TYPE='';
          $LETTING_NOTICE_PERIOD=$terms;
          $LETTING_NOTICE_PERIOD_TYPE=$termType;

          $BREAK_CLAUSE=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();

          $LETTING_BREAK_CLAUSE='';

          $LETTING_BREAK_CLAUSE=$break_term_when;

					$LETTING_BREAK_CLAUSE_TYPE='months';

					$PROPERTY_AVAILABLE_DATE='';

          $LETTING_DEPOSIT=$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();

          $LETTING_DEPOSIT_DUE_DATE='';

					$LETTING_DEPOSIT_PROTECTION_SCHEME='';

					$LETTING_DEPOSIT_HELD_BY='';

					$deposit_scheme = $objPHPExcel->getActiveSheet()->getCell('DQ'.$row)->getValue();

			    $deposit_protection_scheme='';

					if(stristr($deposit_scheme,'DPS')){
						$deposit_protection_scheme=1;
					}else if($deposit_scheme=='TDS'){
						$deposit_protection_scheme=3;
					}else if($deposit_scheme=='TDSL'){
						$deposit_protection_scheme=2;
					}

					$LETTING_DEPOSIT_PROTECTION_SCHEME=$deposit_protection_scheme;

					$LETTING_DEPOSIT_HELD_BY=$deposit_holder;

					$LETTING_SERVICE=$managed_level;

					$LETTING_FEES='';
          $LETTING_FEE_TYPE='';
          $LETTING_FEE_FREQUENCY='';
          $LETTING_MANAGEMENT_FEES='';
          $LETTING_MANAGEMENT_FEE_TYPE='';
          $LETTING_MANAGEMENT_FEE_FREQUENCY='';
          $LETTING_ADMIN_FEES=$com_rate;
          $LETTING_ADMIN_FEE_TYPE='';
          $LETTING_INVENTORY_FEES=$inventory_fee_amount;
          $LETTING_INVENTORY_FEE_TYPE='';
          $LETTING_RENT_GUARANTEE='';
          $LETTING_LANDLORD_PAYMENT='';
          $LETTING_VAT_PERCENTAGE=$vat_rate;
          $LETTING_NOTES=$notes;

					$gas='';
					$electricity='';

					$gas=$objPHPExcel->getActiveSheet()->getCell('BR'.$row)->getValue();
					$electricity= $objPHPExcel->getActiveSheet()->getCell('BT'.$row)->getValue();
					$water= '';

					if($gas!='' || $electricity!=''){
						$utility_reading=array("1"=>$gas,"2"=>$electricity,"3"=>$water);
					}

					$UTILITY=json_encode($utility_reading);

					$query_letting="SELECT *FROM `lettings` WHERE `LETTING_CUSTOM_REF_NO`='$LETTING_CUSTOM_REF_NO'";

					$data = json_decode($db_connect->queryFetch($query_letting),true);

					$letting_exist=$data['data'];

					if(count($letting_exist)==0){


			      echo $sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
			          `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
			          `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,`LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
			          `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
			          `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
			          `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`,`LETTING_UTILITY_READING`)
			          VALUES ('$LETTING_ID','$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID','$SHARED_TENANT_IDS','$LETTING_START_DATE',
								'$LETTING_END_DATE', '$LETTING_RENT', '$LETTING_RENT_FREQUENCY','$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
								'$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE','$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT'
								,'$LETTING_DEPOSIT_DUE_DATE', '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE','$LETTING_FEES','$LETTING_FEE_TYPE',
								'$LETTING_FEE_FREQUENCY','$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY', '$LETTING_ADMIN_FEES','$LETTING_ADMIN_FEE_TYPE','$LETTING_INVENTORY_FEES',
								 '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE', '$LETTING_LANDLORD_PAYMENT', '$LETTING_VAT_PERCENTAGE','$LETTING_NOTES','$UTILITY')";

			          $db_connect->queryExecute($sql);

				}

        }
    }

}
