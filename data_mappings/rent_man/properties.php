<?php

//require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/rent_man/bel/rentals.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

	//$prop_ref=array(827,244,773,792,710,237,924,205,920,783,180,643,782);

    for($row =1; $row <= $total_rows; $row++){
    	
        if($row>1){

        $features_array				= array();
        
        $PROPERTY_STATUS='';
        
        $PROPERTY_SOURCE = 'rentman';
        
        $FURNISHED  = '';
        
        //todo AW - commercial, AQ - Building ID, CO - fee, DR - student, EN - EO - sale comm, EP - lease agent, EQ - service fee

        $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
        $PROPERTY_VENDOR_ID=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
        $PROPERTY_STAFF_ID='';
        $PROPERTY_TITLE=addslashes($objPHPExcel->getActiveSheet()->getCell('FD'.$row)->getValue());
        $PROPERTY_SHORT_DESCRIPTION=addslashes($objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue());
        $PROPERTY_DESCRIPTION=addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());

		$property_archive=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getformattedValue()));
		
		if($property_archive =='true'){
			$PROPERTY_STATUS ='';
		}else{
			$PROPERTY_STATUS ='ACTIVE';
		}
		
		//todo
		/*$is_vacant=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('FG'.$row)->getValue()));
		
		if($is_vacant=='true'){
			//true
		}*/
		
		//todo later
		$gas_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue()));
		
		$elec_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BJ'.$row)->getValue()));
		
		$water_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue()));
		
		$tele_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BL'.$row)->getValue()));
		
		$cable_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BM'.$row)->getValue()));
		
		$oil_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BN'.$row)->getValue()));
		
		$sewage_co=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('BO'.$row)->getValue()));
		//
		
		$agent_id=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('CY'.$row)->getValue()));
		
		//todo check
		//$admin_fees=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('DM'.$row)->getValue()));
		
		
		//INSTRUCTED_DATE
		$INSTRUCTED_DATE='';
		if($objPHPExcel->getActiveSheet()->getCell('EX'.$row)->getformattedValue()!=''){
			$this_instruct_date = str_replace('/','-', $objPHPExcel->getActiveSheet()->getCell('EX'.$row)->getformattedValue());
			$INSTRUCTED_DATE = date('Y-m-d', strtotime($this_instruct_date));
		}
		//
		
		
		
		//todo check % or fixed
		$PROPERTY_ADMIN_FEES=strtolower(trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue()));
		
		$ELEVATOR_IN_BUILDING=trim($objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue());

        $PROPERTY_DESCRIPTION = str_replace('_x000D_', '', $PROPERTY_DESCRIPTION);

        $CATEGORY=addslashes($objPHPExcel->getActiveSheet()->getCell('EE'.$row)->getValue());

		$PROPERTY_CATEGORY='';
		if($CATEGORY==1){
			$PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
		}else if($CATEGORY==2){
			$PROPERTY_CATEGORY='RESIDENTIAL SALES';
		}else if($CATEGORY==3){
			$PROPERTY_CATEGORY='RESIDENTIAL SALES';
		}

        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $PRICE_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('DF'.$row)->getValue();

        if($PRICE_FREQUENCY==1){
      	    $PROPERTY_PRICE_FREQUENCY = 'pcm';
      	}

      	if($PRICE_FREQUENCY==2){
      	    $PROPERTY_PRICE_FREQUENCY = 'pw';
      	}

		$PROPERTY_AVAILABLE_DATE='';

		$this_avail_date = trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getformattedValue());
		
		$this_avail_date = str_replace('/','-', $this_avail_date);
		//Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getformattedValue()
        $PROPERTY_AVAILABLE_DATE=date('Y-m-d', strtotime($this_avail_date));
        
        //var_dump($this_avail_date, $PROPERTY_AVAILABLE_DATE);
        
        //break;

        $PROPERTY_ADDRESS_LINE_1	= '';
        $PROPERTY_ADDRESS_LINE_2	= '';
        $PROPERTY_ADDRESS_CITY		= '';
        $PROPERTY_ADDRESS_COUNTY	= '';
        $PROPERTY_ADDRESS_POSTCODE	= '';

        $area=trim($objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue());

        $housename='';
        $housenumber=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
        $street=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
        $locality='';
        $town=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
        $county=trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        $postcode=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());

        if($housename!=''){
    		$PROPERTY_ADDRESS_LINE_1.=$housename;
        }

        if($housenumber!=''){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$housenumber;
        }

        if($street!=''){
          $PROPERTY_ADDRESS_LINE_2.=$street;
        }

		if($locality!=''){
			$PROPERTY_ADDRESS_CITY.=$locality;
		}
		
		if($area!=''){
			if($PROPERTY_ADDRESS_CITY!=""){
				$PROPERTY_ADDRESS_CITY.=', ';
			}
			$PROPERTY_ADDRESS_CITY.=$area;
		}
		
		if($town!=''){
			$PROPERTY_ADDRESS_COUNTY.=$town;
		}
		
		if($county!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$county;
		}
		
		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}

        

        $PROPERTY_AVAILABILITY=$objPHPExcel->getActiveSheet()->getCell('CN'.$row)->getValue();

		if($PROPERTY_AVAILABILITY=="Valuation"){
			$PROPERTY_AVAILABILITY	= 'Appraisal';
		}


        //$PROPERTY_ADMIN_FEES=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('GA'.$row)->getValue()));
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();

        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
        $PROPERTY_TENURE='';
        $PROPERTY_QUALIFIER='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';

        $PARKING=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();

		$OFF_ROAD_PARKING=str_replace('=','',$PARKING);

		$OFF_ROAD_PARKING=str_replace('()','',$OFF_ROAD_PARKING);

		// $OFF_ROAD_PARKING=0;
		// if($PARKING==1){
		// 	$OFF_ROAD_PARKING=filter_var($PARKING,FILTER_VALIDATE_BOOLEAN);
		// }

        $ON_ROAD_PARKING='';

        $GARDEN=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();

		$GARDEN=str_replace('=','',$GARDEN);

		$GARDEN=str_replace('()','',$GARDEN);

		// if($GARDEN==1){
		// 	$GARDEN=filter_var($GARDEN,FILTER_VALIDATE_BOOLEAN);
		// }

        $PORTER=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue());

		$PORTER=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue());

				// if($PORTER==TRUE){
				// 	$features_array[]='PORTER';
				// }

        $GARAGE=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());

		$GARAGE=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());

				// if($GARAGE==TRUE){
        //   $features_array[]='GARAGE';
				// }

        $burglarAlarm=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('EA'.$row)->getValue());

		$burglarAlarm=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('EA'.$row)->getValue());

				// if($burglarAlarm==TRUE){
        //   $features_array[]='Burglar Alarm';
				// }

        $WASH=$objPHPExcel->getActiveSheet()->getCell('DY'.$row)->getValue();

		$WASHER=str_replace('=','',$WASH);

		$WASHER=str_replace('()','',$WASHER);

				//echo '<br>';

        // $WASHER='';
				//
				// if($WASHMACHINE==1){
        //   $WASHER=filter_var($WASHMACHINE,FILTER_VALIDATE_BOOLEAN);
				// }

        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';

        //$FURNISHED=$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();

        //if($FURNISHED==1){
          //$FURNISHED='';
				//}

        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $DRYER='';
        $DISHWASHER='';

        $PETS_ALLOWED=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('FW'.$row)->getValue());

				//$PETS_ALLOWED=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('FW'.$row)->getValue());

        $FAMILY_OR_CHILD_FRIENDLY='';

        $DSS_ALLOWED=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('FA'.$row)->getValue());

				//$DSS_ALLOWED=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('FA'.$row)->getValue());

        $SMOKING_ALLOWED=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('FR'.$row)->getValue());

		//$SMOKING_ALLOWED=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('FR'.$row)->getValue());

		if($SMOKING_ALLOWED==1){
			$SMOKING_ALLOWED='TRUE';
		}

		$INTERNET=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue());

				//$INTERNET=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('BV'.$row)->getValue());

        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';


		$BALCONY=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());

		$BALCONY=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue());

        // if($BALCONY==TRUE){
        //   $features_array[]='BALCONY';
        // }

        $keys = str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());

		$keys = str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());

        // if($keys==TRUE){
        //   $features_array[]='Key';
        // }

        $HEAT=$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();

        if($HEAT!=''){
            $features_array[]=$HEAT;
        }

        $kitchen=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();

        if($kitchen!=''){
            $features_array[]=$kitchen;
            $KITCHEN_DINER=1;
        }

        $SHOWER=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

				//$SHOWER=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

				// if($SHOWER==TRUE){
				// 	$features_array[]='SHOWER';
				// }

        //$ELEVATOR_IN_BUILDING=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

				//$ELEVATOR_IN_BUILDING=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

        $ADVERT=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

				//$ADVERT=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue());

        // if($ADVERT==TRUE){
        //   $features_array[]='ADVERT';
        // }

        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $EPC_VALUES='';
		$PROPERTY_COMMENTS='';
		$PROPERTY_NOTES=$objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue();

        $PROPERTY_EPC_VALUES = '';

        $PROPERTY_CREATED_ON='';



				/*if($PROPERTY_NOTES!=''){
					$PROPERTY_DESCRIPTION.='\n'.$PROPERTY_NOTES;
				}

				if($PROPERTY_COMMENTS!=''){
					$PROPERTY_DESCRIPTION.='\n'.$PROPERTY_COMMENTS;
				}*/


        /////////////////////////////ADD MORE FIELDS/////////////////////////

        $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('FY'.$row)->getValue();
        $PROPERTY_ADDRESS_LINE_1=addslashes($PROPERTY_ADDRESS_LINE_1);
        $PROPERTY_ADDRESS_LINE_2=addslashes($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY			= addslashes($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY		= addslashes($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE		= addslashes($PROPERTY_ADDRESS_POSTCODE);



		$PROPERTY_ENABLED='';

		$PROPERTY_ROOMS='NULL';

		$descBilltvsubscription=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DW'.$row)->getValue());
		$descBilltvsubscription=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DW'.$row)->getValue());
		$descBillelectricity=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DX'.$row)->getValue());
		$descBillelectricity=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DX'.$row)->getValue());
		$descBillgas=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DV'.$row)->getValue());
		$descBillgas=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DV'.$row)->getValue());
		$descBillwater=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DU'.$row)->getValue());
		$descBillwater=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DU'.$row)->getValue());
		$descBilltvlicence=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DT'.$row)->getValue());
		$descBilltvlicence=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DT'.$row)->getValue());
		$descBillinternet=str_replace('=','',$objPHPExcel->getActiveSheet()->getCell('DS'.$row)->getValue());
		$descBillinternet=str_replace('()','',$objPHPExcel->getActiveSheet()->getCell('DS'.$row)->getValue());

        // if($descBilltvsubscription==1){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>TV Subscription Bill Included</b>';
        //     $features_array[]='TV Subscription Bill Included';
        // }
				//
        // if($descBillelectricity==TRUE){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>Electricity Bill Included</b>';
        //     $features_array[]='Electricity Bill Included';
        // }
				//
        // if($descBillgas==TRUE){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>Gas Bill Included</b>';
        //     $features_array[]='Gas Bill Included';
        // }
				//
        // if($descBillwater==TRUE){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>Water Bill Included</b>';
        //     $features_array[]='Water Bill Included';
        // }
				//
        // if($descBilltvlicence==TRUE){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>TV Licence Bill Included</b>';
        //     $features_array[]='TV Licence Bill Included';
        // }
				//
        // if($descBillinternet==TRUE){
        //     //$PROPERTY_DESCRIPTION.='<br/><b>Internet Bill Included</b>';
        //     $features_array[]='Internet Bill Included';
        // }


		$PROPERTY_CUSTOM_FEATURES	= json_encode($features_array);
		
		$CERTIFICATE_EXPIRE_DATE = $objPHPExcel->getActiveSheet()->getCell('FY'.$row)->getValue();

		//if($CATEGORY!=3 && !in_array($PROPERTY_ID,$prop_ref)){

		 $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
        `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`,
        `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`,
        `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`,`PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`,
        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`,
        `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`,
        `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`,
        `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
        `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
        `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
        `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
        `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`,
        `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`, `INSTRUCTED_DATE`, `PROPERTY_SOURCE`,`CERTIFICATE_EXPIRE_DATE`)
         VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
        '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE' ,
        '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
	        '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS','$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
        '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
        '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
        '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
        '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
        '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
        '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10',
        '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15',
        '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
        '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
        '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','', '$INSTRUCTED_DATE', '$PROPERTY_SOURCE','$CERTIFICATE_EXPIRE_DATE')";
	
         echo "<br/><br/>";

		 //}


		//insert into table
		$db_connect->queryExecute($sql);
	
		//break;
		}
	}
}
?>
