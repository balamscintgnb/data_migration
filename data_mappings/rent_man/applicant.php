<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/marlborough/applicant.xls';

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            // $empty = addslashes(trim());
            $search_criteria=array();
            //logic for data mapping.

            $CLIENTID = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue()));
            $CLIENT_TITLE = '';

            ///////////////FILTER SPECIAL CHERACTOR///////////////////////

            $title = trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
            $name1 = trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
            $name2 = trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
            $title2 = trim($objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue());
            $name3 = trim($objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue());
            $name4 = trim($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue());
            $title3 = trim($objPHPExcel->getActiveSheet()->getCell('CA'.$row)->getValue());
            $name5 = trim($objPHPExcel->getActiveSheet()->getCell('BX'.$row)->getValue());
            $name6 = trim($objPHPExcel->getActiveSheet()->getCell('BW'.$row)->getValue());
            $title4 = trim($objPHPExcel->getActiveSheet()->getCell('CF'.$row)->getValue());
            $name7 = trim($objPHPExcel->getActiveSheet()->getCell('CC'.$row)->getValue());
            $name8 = trim($objPHPExcel->getActiveSheet()->getCell('CB'.$row)->getValue());
            $CLIENT_NAME_4 = addslashes(trim(str_replace("  "," ",$title4." ".$name7." ".$name8)));
            $CLIENT_NAME_3 = addslashes(trim(str_replace("  "," ",$title3." ".$name5." ".$name6)));
            $CLIENT_NAME_2 = addslashes(trim(str_replace("  "," ",$title2." ".$name3." ".$name4)));
            $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$title." ".$name1." ".$name2)));
            if($CLIENT_NAME_2 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " & ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_2;
            }
            if($CLIENT_NAME_3 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " & ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_3;
            }
            if($CLIENT_NAME_4 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " & ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_4;
            }

            $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$CLIENT_NAME)));
            // echo "<per>"; print_r($CLIENT_NAME); echo "</pre>";
            $COMPANY_NAME = '';
            $CLIENT_TYPE='applicant';
            $CLIENT_SUB_TYPE = '';
            $CLIENT_STATUS = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue()));
 
            if($CLIENT_STATUS == '=TRUE'){
                $CLIENT_STATUS = 'Active';
            }else{
                $CLIENT_STATUS = 'inactive';
            }

           // echo "<pre>"; print_r($CLIENT_STATUS); echo "</pre>";
            $CLIENT_PRI_PHONE = '';
            $CLIENT_PRIMARY_PHONE = '';
            $CLIENT_STAFF_ID = '';
            $CLIENT_PRIMARY_EMAIL = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue()));
            $CLIENT_PRI_PHONE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
            if(strlen($CLIENT_PRI_PHONE)<11){
                if($CLIENT_PRI_PHONE !=""){
                $CLIENT_PRIMARY_PHONE = "0".$CLIENT_PRI_PHONE;
                }
            }
            else{
                $CLIENT_PRIMARY_PHONE = $CLIENT_PRI_PHONE;
            }

            $CLIENT_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
            $CLIENT_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
            $CLIENT_ADDRESS_TOWN = '';
            $CLIENT_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));

            
            $CLIENT_ADDRESS1_LINE_1 = '';
            $CLIENT_ADDRESS1_LINE_2 = '';
            $CLIENT_ADDRESS1_CITY = '';
            $CLIENT_ADDRESS1_TOWN = '';
            $CLIENT_ADDRESS1_POSTCODE = '';

            $CLIENT_ADDRESS2_LINE_1 = '';
            $CLIENT_ADDRESS2_LINE_2 = '';
            $CLIENT_ADDRESS2_CITY = '';
            $CLIENT_ADDRESS2_TOWN = '';
            $CLIENT_ADDRESS2_POSTCODE = '';

            $CLIENT_ACCOUNT_NAME = '';
            $CLIENT_ACCOUNT_NO = '';
            $CLIENT_ACCOUNT_SORTCODE = '';

            $CLIENT_EMAIL_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('CG'.$row)->getValue()));
            $CLIENT_EMAIL_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('CH'.$row)->getValue()));
            $CLIENT_EMAIL_3 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('CI'.$row)->getValue()));
            $CLIENT_EMAIL_4 = '';
            $CLIENT_EMAIL_5 = '';

            $CLIENT_PHONE_1 = '';
            $CLIENT_ONE_1 = '';
            $CLIENT_ONE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue()));
            if(strlen($CLIENT_ONE_1)<11){
                if($CLIENT_ONE_1 != ""){
                $CLIENT_PHONE_1 = "0".$CLIENT_ONE_1;
                }
            }
            else{
                $CLIENT_PHONE_1 = $CLIENT_ONE_1;
            }
            $CLIENT_PHONE_2 = '';
            $CLIENT_PHONE_3 = '';
            $CLIENT_PHONE_4 = '';
            $CLIENT_PHONE_5 = '';

            $CLIENT_MOBILE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
            $CLIENT_MOBILE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue()));
            $CLIENT_MOBILE_3 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BY'.$row)->getValue()));
            $CLIENT_MOBILE_4 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('CD'.$row)->getValue()));
            $CLIENT_MOBILE_5 = '';
            $CLIENT_NOTES = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue()));
            $CLIENT_FAX_1 = '';
            $CLIENT_FAX_2 = '';
            $CLIENT_FAX_3 = '';
            $CLIENT_FAX_4 = '';
            $CLIENT_FAX_5 = '';
            $CLIENT_CREATED_ON = '';
            
            //ADD SEARCH CRITE AREA ///
            $Maximum_Price = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue()));
            $Bed_room = addslashes(trim(str_replace("or","",$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue())));
            $furnished = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue()));
            $PROPERTY_TYPE = addslashes($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());
            $category = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue()));
            $Dss = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue()));
            $Smoke = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue()));
            $Pet = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue()));
            $Parking = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue()));
            $Garden = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue()));

            if($furnished == '=TRUE'){
                $furnished = '1';
            }else{
                $furnished = '0';
            }

            if($Dss == '=TRUE'){
                $Dss = '1';
            }else{
                $Dss = '0';
            }

            if($Parking == '=TRUE'){
                $Parking = '1';
            }else{
                $Parking = '0';
            }

            if($Garden == '=TRUE'){
                $Garden = '1';
            }else{
                $Garden = '0';
            }

            if($category == '0' || $category == '2'){
                $category = '43';
            }
            else if($category == '1'){
                $category = '44';
            }

        // echo "<pre>"; print_r($Bed_room); echo"</pre>";
            

            $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$PROPERTY_TYPE,
						'property_applicant_search_attribute_2_from'=>'',
						'property_applicant_search_attribute_2_to'=>$Bed_room,
						'property_applicant_search_attribute_3_from'=>'',
						'property_applicant_search_attribute_3_to'=>'',
						'property_applicant_search_attribute_4_from'=>'',
						'property_applicant_search_attribute_4_to'=>'',
						'property_applicant_search_attribute_6'=>$Parking,
				    	'property_applicant_search_attribute_8'=>$Garden,
						'property_applicant_search_attribute_15'=>'',
						'property_applicant_search_attribute_22'=>$Pet,
                        'property_applicant_search_attribute_24'=>$Dss,
                        'property_applicant_search_attribute_25'=>$Smoke,
						'property_applicant_search_attribute_30'=>$furnished
					    );

				    $SEARCH_CRITERIA_APPLICANT=array('price'=>$Maximum_Price
						 ,'pricefrom'=>'','filter_array'=>$applicant_search_criteria,
						 'frequency'=>$Frequency,'category'=>$category,'location'=>'');

				$SEARCH_CRITERIA = json_encode($SEARCH_CRITERIA_APPLICANT);

            // echo "<pre>"; print_r($SEARCH_CRITERIA_APPLICANT); echo "<pre/>";
            // echo "<pre>"; print_r($CLIENT_PRIMARY_PHONE); echo "<pre/>";


            /////////////// END //////////////////

            $Query2="SELECT CLIENTID FROM `clients` WHERE `CLIENTID` LIKE '$CLIENTID' AND `CLIENT_TYPE` LIKE 'applicant'";

            $Insert_Query2 = json_decode($db_connect->queryFetch($Query2),true);
    
            $PROPERTY_ID=$Insert_Query2['data'][0]['CLIENTID'];

            if($PROPERTY_ID ==''){
     
  
            
         $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, 
                    `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`,
                    `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, 
                    `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, 
                    `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, 
                    `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, 
                    `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, 
                    `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,
                    `SEARCH_CRITERIA`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', 
                    '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2',
                    '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', 
                    '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
                    '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO',
                    '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5',
                    '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1',
                    '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', 
                    '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '$SEARCH_CRITERIA')";
           
          echo $row .'<br/>'; 
          $db_connect->queryExecute($sql) or die ($sql);
            }else{
                continue;
            }
        }
    }
}
?>