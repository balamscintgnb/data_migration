<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/marlborough/contractor.xls';

$db_utility = new Utility();

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);


//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));
            $CLIENT_TITLE = '';
            $CLIENT_NAME = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()));
            $CLIENT_NAME_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()));
            if($CLIENT_NAME_2 != ''){
                if($CLIENT_NAME != ''){
                    $CLIENT_NAME.= " - ";
                }
                $CLIENT_NAME.= $CLIENT_NAME_2;
            }
            $CLIENT_NAME = addslashes(trim(str_replace("  "," ",$CLIENT_NAME)));
            // echo "<pre>"; print_r($CLIENT_NAME); echo "</pre>";
            $COMPANY_NAME = '';
            $CLIENT_TYPE='SUPPLIER';
            $CLIENT_SUB_TYPE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));
            $CLIENT_STATUS = '';
            $CLIENT_STAFF_ID = '';
            $CLIENT_PRIMARY_EMAIL = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue()));
            $CLIENT_PRIMARY_PHONE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));

            $CLIENT_ADDRESS_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue()));
            $CLIENT_ADDRESS_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
            $CLIENT_ADDRESS_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
            $CLIENT_ADDRESS_TOWN = '';
            $CLIENT_ADDRESS_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue()));

            
            $CLIENT_ADDRESS1_LINE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue()));
            $CLIENT_ADDRESS1_LINE_2 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue()));
            $CLIENT_ADDRESS1_CITY = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue()));
            $CLIENT_ADDRESS1_TOWN = '';
            $CLIENT_ADDRESS1_POSTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue()));

            $CLIENT_ADDRESS2_LINE_1 = '';
            $CLIENT_ADDRESS2_LINE_2 = '';
            $CLIENT_ADDRESS2_CITY = '';
            $CLIENT_ADDRESS2_TOWN = '';
            $CLIENT_ADDRESS2_POSTCODE = '';

            $CLIENT_ACCOUNT_NAME = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
            $CLIENT_ACCOUNT_NO = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue()));
            $CLIENT_ACCOUNT_SORTCODE = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));

            
            $CLIENT_EMAIL_1 = '';
            $CLIENT_EMAIL_1 = '';
            $CLIENT_EMAIL_2 = '';
            $CLIENT_EMAIL_3 = '';
            $CLIENT_EMAIL_4 = '';
            $CLIENT_EMAIL_5 = '';
            //echo "<pre>"; print_r($CLIENT_EMAIL_1); echo "</pre>";

            $CLIENT_PHONE_1 = '';
            $CLIENT_PHONE_2 = '';
            $CLIENT_PHONE_3 = '';
            $CLIENT_PHONE_4 = '';
            $CLIENT_PHONE_5 = '';


            $CLIENT_MOBILE_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue()));
            $CLIENT_MOBILE_2 = '';
            $CLIENT_MOBILE_3 = '';
            $CLIENT_MOBILE_4 = '';
            $CLIENT_MOBILE_5 = '';
            
            $CLIENT_NOTES = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue()));
            $CLIENT_FAX_1 = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()));
            $CLIENT_FAX_2 = '';
            $CLIENT_FAX_3 = '';
            $CLIENT_FAX_4 = '';
            $CLIENT_FAX_5 = '';
            $CLIENT_CREATED_ON = '';
          
            
        
        $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,
                `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`,
                `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, 
                `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`,
                `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, 
                `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
                `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
                `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME',
                '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
                '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE',
                '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
                '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
                '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3',
                '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
                '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
                '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON')";
               echo $row."<br/><br/>";
        $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>