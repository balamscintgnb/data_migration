<?php
extract($_GET);

require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/amazon/notice_images.xls';

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$failed_image_ids = array();

$page_total=100+$page;

for($row =$page; $row <= $page_total; $row++){

    $notice_image_id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
    $property_id=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
    $file_name_this = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
    $image_type = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

    $this_amazon_image = "https://gnb-user-uploads.s3.amazonaws.com/cnb/images/bonners/notices/$property_id/gallery/$file_name_this";

    $remote_file_size = curl_get_file_info($this_amazon_image, 'Content-Length');

    if($remote_file_size===false || $remote_file_size<=0){
        $failed_image_ids[]=$notice_image_id;
    }else{
        echo '<pre><code>';
        var_dump($this_amazon_image, $remote_file_size);
        echo '</code></pre>';
    }

    usleep(700000);
    //if($row>110)
    //break;
}

//exit;

if(count($failed_image_ids)>0){
    echo '<pre><code>';
    var_dump(" Count ", count($failed_image_ids), $failed_image_ids);
    echo '</code></pre>';

    $failed_image_ids=array_unique($failed_image_ids);
    $failed_image_ids=implode(",", $failed_image_ids);

    $log_file_path = '../source_data/amazon/id.txt';
    $log_prev_content='';

    if(file_exists($log_file_path)){
        $log_prev_content = file_get_contents($log_file_path);
    }

    $log_file = fopen($log_file_path, 'w');

    if($log_file){
        fwrite($log_file, $failed_image_ids.','.$log_prev_content);
        fclose($log_file);
    }
}

$page_total++;

                            
//
/**
 * Returns the header/meta info of remote file.
 *
 * @param $url - The location of the remote file to download. Cannot
 * be null or empty.
 *
 * @param $headerName - The header info of the remote file to. Cannot
 * be null or empty.
 * 
 * @return The header info of the file referenced by $url with $headerName or -1.
 */
 
function curl_get_file_info( $url, $headerName ) {
    
    $result = -1;
    
    $curl = curl_init( $url );
    
    // Issue a HEAD request and follow any redirects.
    curl_setopt( $curl, CURLOPT_NOBODY, true );
    curl_setopt( $curl, CURLOPT_HEADER, true );
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    
    $headerData = curl_exec( $curl );
    
    curl_close( $curl );
    
    //var_dump($headerData);
    $header = false;
    
    if( $headerData ) {
        
        $status = "";
        
        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $headerData, $matches ) ) {
            $status = (int)$matches[1];
        }
        
        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
            $header = get_header_from_curl_response($headerData, $headerName);
        }
    }
    
    return $header;
}

function get_header_from_curl_response($headerContent, $headerName){
    
    $header=false;

    // Split the string on every "double" new line.
    $arrRequests = explode("\r\n\r\n", $headerContent);

    // Loop of response headers. The "count() -1" is to 
    //avoid an empty row for the extra line break before the body of the response.
    $break=false;
    for ($index = 0; $index < count($arrRequests) -1; $index++) {
        foreach (explode("\r\n", $arrRequests[$index]) as $i => $line){
            $key = explode(': ', $line);
            if($key[0]==$headerName){
                $header = $key[1];
                $break=true;
                break;
            }
        }
        if($break){
        	break;
        }
    }
   
    return $header;
}

if($page<=8844){
    echo "<script> 
    setTimeout(function(){ 
    window.location.href='check_amazon_images.php?page=$page_total';
    },1500);
    </script>";
}
?>