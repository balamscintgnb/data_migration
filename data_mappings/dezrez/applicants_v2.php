<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/dezres/reinhard/sale_applicants.csv';


$thisProceed=true;

try {
    //Load the excel(.xls/.xlsx) file
    if(pathinfo($file_name, PATHINFO_EXTENSION)=='csv'){ 
        $objReader = PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($file_name);
    }else { 
    $objPHPExcel = PHPExcel_IOFactory::load($file_name);
    }
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow(); 

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $CLIENT_TITLE='';
            $get_name = trim(trim(trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue())).' '.trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
            $get_name2 = trim(trim(trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()).' '.trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue())).' '.trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue()));
            if($get_name2 != '')
                $CLIENT_NAME=trim($get_name).' & '.trim($get_name2);
            else 
                $CLIENT_NAME=trim($get_name);
                
        	$CLIENT_NAME = addslashes(trim($CLIENT_NAME));
            $COMPANY_NAME='';
            $CLIENT_TYPE='APPLICANT';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='active';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_1 = addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2 = addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
            $CLIENT_ADDRESS_CITY = addslashes($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
            $CLIENT_ADDRESS_TOWN = addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());
            $CLIENT_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1=$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            $CLIENT_PHONE_2=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
            $CLIENT_PHONE_3=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES='Dairy code:'.$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue().'<br /> Period:'.$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue();
            $CLIENT_FAX_1=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),'d/m/Y H:i:s');
            

            $applicant_search_criteria = array(
                'property_applicant_search_attribute_2_from'=>$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue(), 
            );

            $get_price_period = trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());
            $price_period ='';
	            if(strstr($get_price_period,'Month') || strstr($get_price_period,'Quarter'))
	            {
	                $price_period = '2';
	            }
	            else if(strstr($get_price_period,'Week'))
	            {
	                $price_period = '3';
	            }
	            else if(strstr($get_price_period,'Year') || strstr($get_price_period,'An'))
	            {
	                $price_period = '1';
	            }


    
                $search_criteria1=array('price'=>$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue(),
                'pricefrom'=>$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue(),
                'filter_array'=>$applicant_search_criteria,                'category'=>43, 'frequency'=> $price_period

                );
    
            $SEARCH_CRITERIA = json_encode($search_criteria1);
    		
    		$Query = "SELECT CLIENTID FROM clients WHERE CLIENTID LIKE '$CLIENTID' AND CLIENT_TYPE LIKE '$CLIENT_TYPE' ";
	        $client_exists = json_decode($db_connect->queryFetch($Query),true);
	       
	        if(@$client_exists['data'][0]['CLIENTID']){
	            
	        }else{
			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`,
				`CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`,
				`CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`,
				`CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`) VALUES 
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";
			
			$db_connect->queryExecute($sql) or die($sql);
	        }

        }
    }
    		echo "CLIENTS INSERTED SUCCESSFULLY";
}
?>