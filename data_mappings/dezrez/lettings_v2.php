<?php
// error_reporting(E_ALL);
// ini_set('display_errors',1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/dezres/reinhard/lettings/propertyList_otm_all.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();
$truncate = "TRUNCATE TABLE `lettings`";
$db_connect->queryExecute($truncate);

if($thisProceed){

    for($row =2; $row <= $total_rows; $row++){

        $id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();


        //logic for data mapping.

        $LETTING_ID =$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $LETTING_CUSTOM_REF_NO =str_replace('#','',$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
        $COMPLETE_ADDRESS =$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

                $address = explode(',',$COMPLETE_ADDRESS);
                $address_line_1 = $address[0];
                $address_line_2 = $address[1];
                $address_city = $address[2];
                $address_code = $address[3];
                
              $PROPERTY_sql="SELECT `PROPERTY_REF_ID` FROM properties WHERE 
                `PROPERTY_ADDRESS_LINE_1` LIKE '%$address_line_1,$address_line_2%' ";


                $test = json_decode($db_connect->queryFetch($PROPERTY_sql));


        $PROPERTY_ID=$test->data[0]->PROPERTY_REF_ID;
        $PROPERTY_ROOM_ID=NULL;
        $NUMBER= $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $NAME=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $contact_sql="SELECT `CLIENTID` FROM clients WHERE `CLIENT_NAME` LIKE '%$NAME%'";
        $test_contact = json_decode($db_connect->queryFetch($contact_sql));
        $TENANT_ID=$test_contact->data[0]->CLIENTID;
        $SHARED_TENANT_IDS=NULL;
        $date1 = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $date_create1 = DateTime::createFromFormat('d/m/Y',$date1);
        $date2 = $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
        $date_create2 = DateTime::createFromFormat('d/m/Y',$date2);
        $LETTING_START_DATE=$date_create1->format('Y-m-d');
        $LETTING_END_DATE= $date_create2->format('Y-m-d');
        $get_rent = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $rent = explode(' ',str_replace('£','',$get_rent));
        $LETTING_RENT=str_replace(',','',$rent[0]);
        $LETTING_RENT_FREQUENCY=$rent[1];
        $LETTING_RENT_PAYMENT_FREQUENCY=strtolower($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
        $LETTING_RENT_PAYMENT_DAY='';
        $LETTING_RENT_ADVANCE='';
        $LETTING_RENT_ADVANCE_TYPE='';
        $LETTING_NOTICE_PERIOD='';
        $LETTING_NOTICE_PERIOD_TYPE='';
        $LETTING_BREAK_CLAUSE='';
        $LETTING_BREAK_CLAUSE_TYPE='';
        $LETTING_DEPOSIT=str_replace(',','',str_replace('£','',$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue()));
        $LETTING_DEPOSIT_DUE_DATE='0000-00-00';

        $LETTING_DEPOSIT_HELD_BY='';
        $LETTING_DEPOSIT_PROTECTION_SCHEME ='';
        $get_deposit_by = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();

            if($get_deposit_by == 'Landlord')
                $LETTING_DEPOSIT_HELD_BY=2;
            if($get_deposit_by == 'Protection Scheme'){
                $LETTING_DEPOSIT_HELD_BY=1;
                $LETTING_DEPOSIT_PROTECTION_SCHEME =1;
            }
            if($get_deposit_by == 'Agent')
                $LETTING_DEPOSIT_HELD_BY=3;

        $LETTING_SERVICE='';
        $get_let_service = $objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            if($get_let_service == 'Introduction-only')
                $LETTING_SERVICE=2;
            if($get_let_service == 'Fully managed')
                $LETTING_SERVICE=1;
            if($get_let_service == 'Introduction with rent collection')
                $LETTING_SERVICE=3;

        $LETTING_FEES='0.00';
        $get_let_fee = $objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
        $fee = substr($get_let_fee, strpos( $get_let_fee,'£')+2);
                if($fee>0)
                   $LETTING_FEES= trim(str_replace(',','',str_replace(')','',$fee)));

        $LETTING_FEE_TYPE=2;

            if(stristr($get_let_fee,'pcm'))
        $LETTING_FEE_FREQUENCY='pcm';
            if(stristr($get_let_fee,'pa'))
        $LETTING_FEE_FREQUENCY='pa';
            if(stristr($get_let_fee,'pw'))
        $LETTING_FEE_FREQUENCY='pw';
        $management_fee = '';//substr($get_management_fee,strpos('%',3));
        $management_type='';
        $get_management_fee = $objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue();
                if(stristr($get_management_fee, '%')){
                    $management_fee = explode('%',$get_management_fee);//substr($get_management_fee,strpos('%',3));
                    $management_type=1;
                }
                if(stristr($get_management_fee, '£')) {
                     $management_fee= explode(' ', str_replace('£','',$get_management_fee));
                     $management_type=2;
                }

        $LETTING_MANAGEMENT_FEES=$management_fee[0];
        $LETTING_MANAGEMENT_FEE_TYPE=$management_type;
        $LETTING_MANAGEMENT_FEE_FREQUENCY='';
        $LETTING_ADMIN_FEES=NULL;
        $LETTING_ADMIN_FEE_TYPE='';
        $LETTING_INVENTORY_FEES='';
        $LETTING_INVENTORY_FEE_TYPE='';
        $get_rent_guarantee = strtolower($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
            if($get_rent_guarantee == 'yes')
            $LETTING_RENT_GUARANTEE='1';
            else 
            $LETTING_RENT_GUARANTEE='0';

        
               $get_landlord = $objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue();
            if($get_landlord == 'Introduction-only')
                $LETTING_SERVICE=1;
            if($get_landlord == 'Fully managed')
                $LETTING_SERVICE=3;
            if($get_let_service == 'Introduction with rent collection')
                $LETTING_SERVICE=2;

        $LETTING_LANDLORD_PAYMENT='0.00';
        $LETTING_VAT_PERCENTAGE='0.0';
        $LETTING_NOTES=NULL;
        $LETTING_DATETIME="0000-00-00";
        $RECORD_UPLOADED=0;
    

        $sql="INSERT INTO `lettings`(`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`, `SHARED_TENANT_IDS`, `LETTING_START_DATE`, `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`, `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`, `LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`, `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`,
           `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`, `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`, `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,
            `LETTING_NOTES`, `LETTING_DATETIME`, `RECORD_UPLOADED`) VALUES
            ('$LETTING_ID','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$PROPERTY_ROOM_ID','$TENANT_ID','$SHARED_TENANT_IDS','$LETTING_START_DATE','$LETTING_END_DATE','$LETTING_RENT','$LETTING_RENT_FREQUENCY','$LETTING_RENT_PAYMENT_FREQUENCY','$LETTING_RENT_PAYMENT_DAY','$LETTING_RENT_ADVANCE','$LETTING_RENT_ADVANCE_TYPE','$LETTING_NOTICE_PERIOD','$LETTING_NOTICE_PERIOD_TYPE','$LETTING_BREAK_CLAUSE','$LETTING_BREAK_CLAUSE_TYPE','$LETTING_DEPOSIT','$LETTING_DEPOSIT_DUE_DATE','$LETTING_DEPOSIT_HELD_BY',
            '$LETTING_DEPOSIT_PROTECTION_SCHEME','$LETTING_SERVICE','$LETTING_FEES','$LETTING_FEE_TYPE','$LETTING_FEE_FREQUENCY','$LETTING_MANAGEMENT_FEES','$LETTING_MANAGEMENT_FEE_TYPE','$LETTING_MANAGEMENT_FEE_FREQUENCY','$LETTING_ADMIN_FEES','$LETTING_ADMIN_FEE_TYPE','$LETTING_INVENTORY_FEES','$LETTING_INVENTORY_FEE_TYPE','$LETTING_RENT_GUARANTEE','$LETTING_LANDLORD_PAYMENT','$LETTING_VAT_PERCENTAGE',
            '$LETTING_NOTES','$LETTING_DATETIME','$RECORD_UPLOADED') ;";
       $db_connect->queryExecute($sql);
    }
       echo "LETTINGS INSERTED SUCCESSFULLY";
}
?>
