<?php
// error_reporting(E_ALL);
// ini_set('display_errors',1);
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/dezres/reinhard/lettings/withdrawn_instructions.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){       
       
        $PROPERTY_SHORT_DESCRIPTION='';

        $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $PROPERTY_VENDOR_ID='';
        $CLIENT_NAME=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        $PROPERTY_STAFF_ID='';
        $PROPERTY_TITLE='';
        $PROPERTY_SHORT_DESCRIPTION='';
        $PROPERTY_DESCRIPTION='';
        $PROPERTY_CATEGORY=strtoupper(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue())).' LETTINGS';

        $PROPERTY_PRICE='';

            $PRICE = $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $rent_price = explode(' ',$PRICE);
            $PRICE_exiest = count($rent_price);
            if($PRICE_exiest == 2){
              $PROPERTY_PRICE = trim(str_replace("£","",$rent_price[0]));
            }
            else if($PRICE_exiest == 3){
                
                  $PROPERTY_PRICE = trim(str_replace("£","",$rent_price[0]));
            }
            else if($PRICE_exiest == 4){
                
                  $PROPERTY_PRICE = trim(str_replace("£","",$rent_price[0]));
            } else {
                $PROPERTY_PRICE = trim(str_replace("£","",$rent_price[0]));
            }

        $get_frequency =Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
        $PROPERTY_PRICE_FREQUENCY='';
        if(strstr($get_frequency,'PCM') || strstr($get_frequency,'month') ){ 
            $PROPERTY_PRICE_FREQUENCY='PCM';
        }
        else if(strstr($get_frequency,'PW') || strstr($get_frequency,'week') ){ 
            $PROPERTY_PRICE_FREQUENCY='PW';
        }

        $PROPERTY_QUALIFIER='';      
        
        
        $get_address = explode(',',$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

        $count_val = count($get_address);

        if($count_val==4)    {
        $PROPERTY_ADDRESS_LINE_1=$get_address[0];
        $PROPERTY_ADDRESS_LINE_2=$get_address[1];
        $PROPERTY_ADDRESS_CITY=$get_address[2];
        $PROPERTY_ADDRESS_COUNTY='';
        $PROPERTY_ADDRESS_POSTCODE=str_replace('.','',$get_address[3]);
        } else  if($count_val==5)    {
            $PROPERTY_ADDRESS_LINE_1=$get_address[0];
            $PROPERTY_ADDRESS_LINE_2=$get_address[1];
            $PROPERTY_ADDRESS_CITY=$get_address[2];
            $PROPERTY_ADDRESS_COUNTY=$get_address[3];
            $PROPERTY_ADDRESS_POSTCODE=str_replace('.','',$get_address[4]);
        } else  if($count_val==6)    {
                $PROPERTY_ADDRESS_LINE_1=$get_address[0].$get_address[1];
                $PROPERTY_ADDRESS_LINE_2=$get_address[2];
                $PROPERTY_ADDRESS_CITY=$get_address[3];
                $PROPERTY_ADDRESS_COUNTY=$get_address[4];
                $PROPERTY_ADDRESS_POSTCODE=str_replace('.','',$get_address[5]);
        }
        else { 
            $PROPERTY_ADDRESS_LINE_1=$get_address[0];

        }
        $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_STATUS='';
        $PROPERTY_AVAILABILITY='WITHDRAWN';
        $PROPERTY_ADMIN_FEES = addslashes(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));

        $PROPERTY_TYPE='';
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
        $PROPERTY_TENURE='';
        $Lease_term_years='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON=date('Y-m-d');


        $get_available_from = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue(), 'd/m/Y');
        if($get_available_from) { 
            $PROPERTY_AVAILABLE_DATE="'".$get_available_from."'";
        }
        else { 
            $PROPERTY_AVAILABLE_DATE='NULL';
        }
  

        $get_instruct_date = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue(), 'd/m/Y');
        if($get_instruct_date) { 
            $INSTRUCTED_DATE="'".$get_instruct_date."'";
        }
        else { 
            $INSTRUCTED_DATE='NULL';
        }
        
    	$get_lettings_managed = $objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_LETTING_SERVICE = '';

        if(strstr($get_lettings_managed, 'Fully Managed')) { 
            $PROPERTY_LETTING_SERVICE = '1';
        }
        else if(strstr($get_lettings_managed, 'Tenant Find')) { 
            $PROPERTY_LETTING_SERVICE = '2';
        }
        else if(strstr($get_lettings_managed, 'Rent Coll')) { 
            $PROPERTY_LETTING_SERVICE = '3';
        }


        /////////////////////////////ADD MORE FIELDS/////////////////////////

        /////////////// PROPERTY TYPE //////////////

        if( strstr($PROPERTY_TYPE,'Semi-Detached Bungalow')){
            $PROPERTY_TYPE='semi-detached bungalow';      
        } else if( strstr($PROPERTY_TYPE,'Semi-Detached (House)')){
            $PROPERTY_TYPE='semi-detached house';   
        } else if( strstr($PROPERTY_TYPE,'Mid Terrace (House)')){
            $PROPERTY_TYPE='Terraced';   
        } else if( strstr($PROPERTY_TYPE,'End Terrace (House)')){
            $PROPERTY_TYPE='End Terrace';   
        } else if( strstr($PROPERTY_TYPE,'Detached (Bungalow)')){
            $PROPERTY_TYPE='detached bungalow';   
        } else if( strstr($PROPERTY_TYPE,'Terraced (House)')){
            $PROPERTY_TYPE='terraced (house)';   
        } else if( strstr($PROPERTY_TYPE,'Detached (House)')){
            $PROPERTY_TYPE='detached house';   
        } else if( strstr($PROPERTY_TYPE,'Maisonette')){
            $PROPERTY_TYPE='maison';   
        } else if( strstr($PROPERTY_TYPE,'semi-detached')){
            $PROPERTY_TYPE='semi-detached';   
        } else if( strstr($PROPERTY_TYPE,'Studio')){
            $PROPERTY_TYPE='Studio';   
        }

        ///////////////// END ///////////////
        
        ////////////////// PRICE QUALIFIER ///////////////

        $get_qualifier_1=Utility::unformatted_price_frequency($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
        $get_qualifier = strtolower($get_qualifier_1);
              if($get_qualifier == 'Asking Price'){ 
                  $PROPERTY_QUALIFIER='Fixed price';
              }
              else if($get_qualifier == 'offersintheregionof' || $get_qualifier == 'Offersinregionof' || $get_qualifier == 'offersinregionof' || $get_qualifier == 'offersintheregion' || $get_qualifier == 'Offersintheregionof'){
                  $PROPERTY_QUALIFIER='Offers in the region of';
              }
              else if($get_qualifier == 'guideprice'){
                  $PROPERTY_QUALIFIER='Guide price';
              }
              else if($get_qualifier == 'From'){
                  $PROPERTY_QUALIFIER='From';
              }
              else if($get_qualifier == 'offersinexcess' || $get_qualifier == 'Offers in excess of' || $get_qualifier == 'offerinexcess' || $get_qualifier == 'offersinexcessof'){
                  $PROPERTY_QUALIFIER='Offers in excess of';
              }
              else if($get_qualifier == 'offersver:' || $get_qualifier == 'offersover:'|| $get_qualifier == 'offersver'|| $get_qualifier == 'offersover'|| $get_qualifier == 'offersabove:'){ 
                  $PROPERTY_QUALIFIER='Offers over';
              }
              else{
                  $PROPERTY_QUALIFIER="";
              }
               //echo "<pre>"; print_r($get_qualifier_1); echo "<pre/>";
               //echo "<pre>"; print_r($get_qualifier); echo "<pre/>";
               //echo "<pre>"; print_r($PROPERTY_QUALIFIER); echo "<pre/>";
          /////////////////////// END /////////////////

        $CLIENT_TYPE='landlord';
        $CLIENT_NAME = addslashes($CLIENT_NAME);

        $query_1="SELECT CLIENTID FROM `clients` WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE 'landlord'";

        $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
        if($landlord_exists['data'][0]['CLIENTID'] !='')
        $PROPERTY_VENDOR_ID=$landlord_exists['data'][0]['CLIENTID'];
        else {
            $rand_no = '500005'.$row;
            $sql2 ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
		        `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`,
		        `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`,
		        `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`,
		        `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
		        `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`,
		        `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`) VALUES 
		        ('$rand_no', '$CLIENT_TITLE', '$CLIENT_NAME', 'LANDLORD', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
		        '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
		        '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
		        '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
		        '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4',
		        '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1',
		        '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";
	        $db_connect->queryExecute($sql2) or die($sql2);
        	$PROPERTY_VENDOR_ID=$rand_no;
        }
        
		$Query1 = "SELECT * FROM `properties` WHERE PROPERTY_ID LIKE '$PROPERTY_ID' AND PROPERTY_CATEGORY LIKE '$PROPERTY_CATEGORY' ";
		$property_exists = json_decode($db_connect->queryFetch($Query1),true);
		
		if(@$property_exists['data'][0]['PROPERTY_ID']){
		    continue;
		}else{

        $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
			`PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
			`PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
			`PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
			`PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
			`KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
			`DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
			`DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
			`CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
			`PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
			`PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
			`PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
			`PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`,`PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) VALUES 
			('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
			'$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
			'$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
			'$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
			'$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
			'$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
			'$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
			'$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
			'$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
			'$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
			'$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
			'$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
			'$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
			'$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON','','','','$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)";
		
		$db_connect->queryExecute($sql) or die($sql);
		}
        }
    }
    	echo "PROPERTIES INSERTED SUCCESSFULLY";
}
?>