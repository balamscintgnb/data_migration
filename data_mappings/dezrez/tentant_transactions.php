<?php
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$dir_name=glob('data_source/lettings_records/L0006.*');
//$truncate = "TRUNCATE TABLE cnb_lgkl.`tenant_transactions`";
//$db_connect->queryExecute($truncate);

foreach($dir_name as $single_file) { 
    $thisProceed=true;

    try {
        //Load the excel(.xls/.xlsx) file
        $objPHPExcel = PHPExcel_IOFactory::load($single_file);
    } catch (Exception $e) {
        $thisProceed=false;
        die('Error loading file "' . pathinfo($single_file, PATHINFO_BASENAME). '": ' . $e->getMessage());
    }



//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =2; $row <= $total_rows; $row++){
       
        if($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue() != '') { 
        
         $LETTING_REF_NO =  pathinfo($single_file, PATHINFO_FILENAME );

        
        $date1 = $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
        $date_create1 =Utility::convert_tosqldate($date1);

        $DATE_RECEIVED=$date_create1;
      $DESCRIPTION =$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

       

            if(stristr($DESCRIPTION,'Rent')){
                
                $TRANSACTION_TYPE = 'RENT';
                $trim_description = str_replace('Rent:','',$DESCRIPTION);
                $dates = explode('-', $trim_description);
                $date_1 = Utility::convert_tosqldate($dates[0]);
                 $RENT_STARTDATE ="'".$date_1."'";
                $date_2 = Utility::convert_tosqldate($dates[1]);
                 $RENT_ENDDATE = "'".$date_2."'";    
            }
            elseif(stristr($DESCRIPTION,'deposit')){
                $TRANSACTION_TYPE = 'DEPOSIT';
                $RENT_STARTDATE = "NULL";
                $RENT_ENDDATE = "NULL";    
            }
            else 
            { 
                $TRANSACTION_TYPE = 'ADMIN';
                $RENT_STARTDATE = "null";
                $RENT_ENDDATE = "null";    
            }

        $TENANT= $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();


        $AMOUNT_DUE= Utility::unformatted_price($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());

        $RECEVIED  = Utility::unformatted_price($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
    
        if($AMOUNT_DUE==0)
            $AMOUNT_DUE = $RECEVIED;

        $OUT_STANDING = Utility::unformatted_price($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
             
        $BALANCE = Utility::unformatted_price( $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());

          $sql="INSERT INTO cnb_lgkl.`tenant_transactions`( `LETTING_CUSTOM_REF_NO`, `RENT_STARTDATE`, `RENT_ENDDATE`, `DUE_DATE`,
         `PAYMENT_DATE`, `DUE_AMOUNT`, `PAID_AMOUNT`, `TRANSACTION_TYPE`,  `TRANSACTION_DESCRIPTION`) VALUES
          ('$LETTING_REF_NO',$RENT_STARTDATE,$RENT_ENDDATE, '$DATE_RECEIVED','$DATE_RECEIVED','$AMOUNT_DUE','$RECEVIED', '$TRANSACTION_TYPE','$DESCRIPTION') ;";
 
        // $db_connect->queryExecute($sql); 

    }
    }
}
}
?>
