<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);


$query_user = "SELECT * FROM `en_user` WHERE `email` != '';";

$data_user = json_decode($db_connect_src->queryFetch($query_user),true);


$admin_users = array();

if(count($data_user)>0){
	foreach($data_user['data'] as $user){
		$admin_users[$user['userid']] = $user['email'];
	}
}

//$limit=250;

/*if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}*/

$query = "SELECT * FROM `en_client`";

$data = json_decode($db_connect_src->queryFetch($query),true);



if(count($data)>0){

	foreach($data['data'] as $row){

		//echo '<pre>';
		//print_r($row);
		//echo '</pre>';

		//exit;



	$CLIENT_CREATED_ON 		= $row['dateentered'];
	$CLIENTID				= $row['clientid'];
	$CLIENT_TITLE			= $row['title'];
	$CLIENT_NAME			= $row['forename'].' '.$row['surname'];
	$CLIENT_NAME			= $CLIENT_NAME;

	$CLIENT_STAFF_ID		= $admin_users[$row['userid']];

	$CLIENT_TYPE			= '';
	$CLIENT_SUB_TYPE		= '';

	if($row['recordtype']==0){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==80){
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==81){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'BUILDER';
	}
	else if($row['recordtype']==84){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'DEVELOPER';
	}
	else if($row['recordtype']==85){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'DEALER';
	}
	else if($row['recordtype']==93){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'CANVASSING';
	}
	else if($row['recordtype']==303){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'NEW HOME DEVELOPER';
	}
	else if($row['recordtype']==937){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'INVESTOR';
	}
	else if($row['recordtype']==938){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= 'POTENTIAL VENDOR';
	}
	else if($row['recordtype']==939){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'AUCTION';
	}
	else if($row['recordtype']==940){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==943){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'EXTERNAL SALE';
	}
	else if($row['recordtype']==944){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= 'MARKET APPRAISAL';
	}
	else if($row['recordtype']==1073){
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'POTENTIAL LANDLORD';
	}
	else if($row['recordtype']==1228){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1229){
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1241){
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= 'CLIENT';
	}
	else if($row['recordtype']==1251){
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= 'PREVIOUS CLIENT';
	}
	else if($row['recordtype']==1444){
		$CLIENT_TYPE			= 'TENANT';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1529){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'PROBATE';
	}


	$CLIENT_STATUS = 'ACTIVE';
	if($row['sys_active']==1){
		$CLIENT_STATUS = 'INACTIVE';
	}

	$email1					= trim($row['email1']);


	$CLIENT_NOTES			= '';
	$CLIENT_PRIMARY_PHONE	= $row['tel1'];

	if(Utility::validate_email_id($email1)){
		$CLIENT_PRIMARY_EMAIL	= $email1;
	}
	else{
		$CLIENT_NOTES			= $email1.' ';
	}

	$CLIENT_EMAIL_1			= $row['email2'];
	$CLIENT_EMAIL_2			= '';
	$CLIENT_EMAIL_3			= '';
	$CLIENT_EMAIL_4			= '';
	$CLIENT_EMAIL_5			= '';

	$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';

	$tel2_type 				= $row['teltype2'];
	$tel3_type 				= $row['teltype3'];
	$tel4_type 				= $row['teltype4'];

	$tel2_val 				= $row['tel2'];
	$tel3_val 				= $row['tel3'];
	$tel4_val 				= $row['tel4'];

	if($tel2_type==96 || $tel2_type==98 || $tel2_type==100 || $tel2_type==102 || $tel2_type==1359){
		$phone_2 = $tel2_val;
	}

	if($tel2_type==103 || $tel2_type==104 ){
		$mobile_2 = $tel2_val;
	}

	if($tel2_type==105 || $tel2_type==106 ){
		$fax_2 = $tel2_val;
	}


	if($tel3_type==96 || $tel3_type==98 || $tel3_type==100 || $tel3_type==102 || $tel3_type==1359){
		$phone_3 = $tel3_val;
	}

	if($tel3_type==103 || $tel3_type==104 ){
		$mobile_3 = $tel3_val;
	}

	if($tel3_type==105 || $tel3_type==106 ){
		$fax_3 = $tel3_val;
	}

	if($tel4_type==96 || $tel4_type==98 || $tel4_type==100 || $tel4_type==102 || $tel4_type==1359){
		$phone_4 = $tel4_val;
	}

	if($tel4_type==103 || $tel4_type==104 ){
		$mobile_4 = $tel4_val;
	}

	if($tel4_type==105 || $tel4_type==106 ){
		$fax_4 = $tel4_val;
	}

	$CLIENT_ADDRESS_LINE_1 	= '';
	$CLIENT_ADDRESS_LINE_2	= '';
	$CLIENT_ADDRESS_CITY	= '';
	$CLIENT_ADDRESS_TOWN	= '';
	$CLIENT_ADDRESS_POSTCODE= '';

	$housename			= trim($row['Housename']);
	$housenumber		= trim($row['Housenumber']);
	$street				= trim($row['Street']);
	$locality			= trim($row['Locality']);
	$area				= trim($row['Area']);
	$town				= trim($row['Town']);
	$county				= trim($row['County']);
	$postcode			= trim($row['postcode']);

	if($housename!=''){
		$CLIENT_ADDRESS_LINE_1.=$housename;
	}

	if($housenumber!=''){
		if($CLIENT_ADDRESS_LINE_1!=""){
			$CLIENT_ADDRESS_LINE_1.=', ';
		}
		$CLIENT_ADDRESS_LINE_1.=$housenumber;
	}

	if($street!=''){
		$CLIENT_ADDRESS_LINE_2.=$street;
	}

	if($locality!=''){
		$CLIENT_ADDRESS_CITY.=$locality;
	}

	if($area!=''){
		if($CLIENT_ADDRESS_CITY!=""){
			$CLIENT_ADDRESS_CITY.=', ';
		}
		$CLIENT_ADDRESS_CITY.=$area;
	}

	if($town!=''){
		$CLIENT_ADDRESS_TOWN.=$town;
	}

	if($county!=''){
		if($CLIENT_ADDRESS_TOWN!=""){
			$CLIENT_ADDRESS_TOWN.=', ';
		}
		$CLIENT_ADDRESS_TOWN.=$county;
	}

	if($postcode!=''){
		$CLIENT_ADDRESS_POSTCODE.=$postcode;
	}

	$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
	$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
	$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
	$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
	$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

	$CLIENT_PHONE_1			= $phone_2;
	$CLIENT_PHONE_2			= $phone_3;
	$CLIENT_PHONE_3			= $phone_4;
	$CLIENT_PHONE_4			= $phone_5;
	$CLIENT_PHONE_5			= '';

	$CLIENT_MOBILE_1		= $mobile_2;
	$CLIENT_MOBILE_2		= $mobile_3;
	$CLIENT_MOBILE_3		= $mobile_4;
	$CLIENT_MOBILE_4		= $mobile_5;
	$CLIENT_MOBILE_5		= '';

	$CLIENT_FAX_1			= $fax_2;
	$CLIENT_FAX_2			= $fax_3;
	$CLIENT_FAX_3			= $fax_4;
	$CLIENT_FAX_4			= $fax_5;
	$CLIENT_FAX_5			= '';

	$CLIENT_NOTES.= $row['notes'];

	echo $sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON', '0')";

	$db_connect->queryExecute($sql);
}
echo $p;

}
?>
