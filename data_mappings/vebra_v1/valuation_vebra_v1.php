<?php
include 'config.php';
$_database1 = 'cnb_gnbdata';
$_database2 = '1';

$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);

$insert_query = '';
$insert_query .='INSERT INTO '.$_database1.'.Viewings (';

$item_query = $db2->execute("SELECT * FROM `$_database2`.en_item");
$item_array = array();
while($item = $item_query->fetch_object()){
    $item_array[$item->itemid] = $item->description;
}

$column_list_1  = $db1->execute("SHOW COLUMNS FROM viewings WHERE (FIELD NOT LIKE 'ID') and (FIELD NOT LIKE 'RECORD_UPLOADED') ");
while($row1 = $column_list_1->fetch_object()){

    $result1[] = $row1->Field;
}


foreach($result1 as $key1 => $single_1){

    if($key1 == 29)
      $insert_query .= '`'.$single_1.'`';
    else
      $insert_query .= '`'.$single_1.'`,';

}
$insert_query .=') VALUES ';

$column_list_2 = $db2->execute("SELECT env.*,enp.* FROM en_valuation as env LEFT JOIN en_property as enp ON enp.propertyid=env.propertyid");
$rowscount=0;
$total_rows = $column_list_2->num_rows;
while($row = $column_list_2->fetch_object()){

    $insert_query .= '(';
        $PROPERTY_ID = $row->propertyid;
        $PROPERTY_REF_ID			= '';
		$PROPERTY_VENDOR_ID			= $row->sys_vendorid;
		$PROPERTY_CATEGORY			= '';
		$recordtype					= $row->recordtype;
		$category					= $row->category;

		$PROPERTY_CLASSIFICATION	= '';

		if($recordtype==433 && $category==41){
			$PROPERTY_CATEGORY		= 'COMMERCIAL LETTING';
		}
		if($recordtype==301){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
			$PROPERTY_CLASSIFICATION= 'New Build';
		}
		else if($recordtype==23 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
		}
		else if($recordtype==23 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL SALES';
		}
		else if($recordtype==24 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
		}
		else if($recordtype==24 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL LETTINGS';
		}
		else if($recordtype==26 && $category==41){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
		}
		else if($recordtype==26 && $category==43){
			$PROPERTY_CATEGORY		= 'COMMERCIAL AUCTION';
		}
		else if($recordtype==23 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL SALES';
		}
		else if($recordtype==24 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL LETTINGS';
		}
		else if($recordtype==26 && $category==0){
			$PROPERTY_CATEGORY		= 'RESIDENTIAL AUCTION';
		}

		$PROPERTY_PRICE				= $row->price;

		$PROPERTY_ADDRESS_LINE_1	= '';
		$PROPERTY_ADDRESS_LINE_2	= '';
		$PROPERTY_ADDRESS_CITY		= '';
		$PROPERTY_ADDRESS_COUNTY	= '';
		$PROPERTY_ADDRESS_POSTCODE	= '';

		$housename			= trim($row->Housename);
		$housenumber		= trim($row->Housenumber);
		$street				= trim($row->Street);
		$locality			= trim($row->Locality);
		$area				= trim($row->Area);
		$town				= trim($row->Town);
		$county				= trim($row->County);
		$postcode			= trim($row->postcode);

		if($housename!=''){
			$PROPERTY_ADDRESS_LINE_1.=$housename;
		}

		if($housenumber!=''){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$housenumber;
		}

		if($street!=''){
			$PROPERTY_ADDRESS_LINE_2.=$street;
		}

		if($locality!=''){
			$PROPERTY_ADDRESS_CITY.=$locality;
		}

		if($area!=''){
			if($PROPERTY_ADDRESS_CITY!=""){
				$PROPERTY_ADDRESS_CITY.=', ';
			}
			$PROPERTY_ADDRESS_CITY.=$area;
		}

		if($town!=''){
			$PROPERTY_ADDRESS_COUNTY.=$town;
		}

		if($county!=''){
			if($PROPERTY_ADDRESS_COUNTY!=""){
				$PROPERTY_ADDRESS_COUNTY.=', ';
			}
			$PROPERTY_ADDRESS_COUNTY.=$county;
		}

		if($postcode!=''){
			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
		}

		$PROPERTY_ADDRESS_LINE_1		= $db2->escape($PROPERTY_ADDRESS_LINE_1);
		$PROPERTY_ADDRESS_LINE_2		= $db2->escape($PROPERTY_ADDRESS_LINE_2);
		$PROPERTY_ADDRESS_CITY			= $db2->escape($PROPERTY_ADDRESS_CITY);
		$PROPERTY_ADDRESS_COUNTY		= $db2->escape($PROPERTY_ADDRESS_COUNTY);
		$PROPERTY_ADDRESS_POSTCODE		= $db2->escape($PROPERTY_ADDRESS_POSTCODE);


		$PROPERTY_FORMATTED_ADDRESS	= '';
		if($row->sys_status==117){
			$PROPERTY_STATUS			= 'ARCHIVED';
			$PROPERTY_AVAILABILITY		= '';
		}
		else{
			$PROPERTY_STATUS			= 'ACTIVE';
			$PROPERTY_AVAILABILITY		= $item_array[$row->sys_status];
		}
		$PROPERTY_ADMIN_FEES		= '';
		$PROPERTY_TYPE				= $item_array[$row->propertytype];
		$PROPERTY_BEDROOMS			= $row->beds;
		$PROPERTY_BATHROOMS			= $row->bathrooms;
		$PROPERTY_RECEPTION			= $row->receptions;

		$PROPERTY_TENURE            = '';
		if($row->tenure != 0)
		$PROPERTY_TENURE            = $item_array[$row->tenure];

		$PROPERTY_AGE               = '';
		if($row->age != 0)
		$PROPERTY_AGE               = $item_array[$row->age];
		// $row->condition."SD";

        $PROPERTY_CLASSIFICATION    = '';
        $PROPERTY_CONDITION         = $item_array[$row->condition];
        $PROPERTY_PRICE_FROM        = $row->amountlow;
        $PROPERTY_PRICE_TO          = $row->amounthigh;
        $PROPERTY_VENDOR_PRICE      = $row->amountvendor;
        $PROPERTY_PROPOSED_PRICE    = $row->propprice;
		//$PROPERTY_TENURE			= $item_array[$row->tenure];
        $PROPERTY_CURRENT_OCCUPANT	= '';
        $PROPERTY_VALUER_NOTES='';
        $PROPERTY_VALUER_NOTES_1=htmlentities($db2->escape($row->valuernotes));
        $PROPERTY_VALUER_NOTES_2='';
        $PROPERTY_VALUER_NOTES_3='';
        $PROPERTY_VALUER_NOTES_4='';
        $PROPERTY_VALUER_NOTES_5='';
        $PROPERTY_APPOINTMENT_STARTTIME=$row->dateentered;
        $PROPERTY_APPOINTMENT_ENDTIME=$row->dateentered;

        $insert_query .= "'$PROPERTY_ID','$PROPERTY_REF_ID','$PROPERTY_VENDOR_ID','$PROPERTY_CATEGORY','$PROPERTY_ADDRESS_LINE_1','$PROPERTY_ADDRESS_LINE_2',
        '$PROPERTY_ADDRESS_CITY','$PROPERTY_ADDRESS_COUNTY','$PROPERTY_ADDRESS_POSTCODE','$PROPERTY_STATUS','$PROPERTY_TYPE','$PROPERTY_BEDROOMS','$PROPERTY_BATHROOMS','$PROPERTY_RECEPTION','$PROPERTY_TENURE',
        '$PROPERTY_AGE','$PROPERTY_CLASSIFICATION', '$PROPERTY_CONDITION', '$PROPERTY_PRICE_FROM', '$PROPERTY_PRICE_TO','$PROPERTY_VENDOR_PRICE','$PROPERTY_PROPOSED_PRICE',
        '$PROPERTY_VALUER_NOTES','$PROPERTY_VALUER_NOTES_1','$PROPERTY_VALUER_NOTES_2','$PROPERTY_VALUER_NOTES_3','$PROPERTY_VALUER_NOTES_4','$PROPERTY_VALUER_NOTES_5','$PROPERTY_APPOINTMENT_STARTTIME',
        '$PROPERTY_APPOINTMENT_ENDTIME'";
        if($total_rows-1 == $rowscount)
            $insert_query .= ')';
        else
        $insert_query .= '),';

        $rowscount++;

}
echo $insert_query;


//echo $db1->execute($insert_query);


?>
