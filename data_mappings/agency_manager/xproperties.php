<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = "select * from xproperties ORDER BY `id` ASC";

// $query = "select * from properties where ID='2470' ORDER BY 'ID' ASC ";

$data = json_decode($db_connect_src->queryFetch($query),true);
	
if(count($data)>0){
    $SNO=1;
	foreach($data['data'] as $row){
        $SNO++;
    /*  echo '<pre>';
		print_r($row);
		echo '</pre>';

        exit;  */
        $Enter_PROPERTY_ID = $row['ID'];
        $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
        $PROPERTY_ID    = ''.$pro_id;
        $PROPERTY_REF_ID = $PROPERTY_ID;
        $PROPERTY_TYPE  = $row['PropertyType'];

        $PROPERTY_ADDRESS_LINE_1='';
        $add_sub_type_1 = $row['NoName'];
        $add_sub_type_2 = $row['AddressOne'];
        $add_sub_type_3 = $row['AddressTwo'];

        if($add_sub_type_1!=''){
            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
        }
        
        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
        }
        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
        }
        $PROPERTY_ADDRESS_LINE_2= $row['AddressTwo'];
        $PROPERTY_ADDRESS_CITY  = '';
        $PROPERTY_ADDRESS_CITY  = $row['City'];
        $PROPERTY_ADDRESS_COUNTY = $row['County'];
        $PROPERTY_ADDRESS_POSTCODE =  $row['Postcode'];
        $PROPERTY_FORMATTED_ADDRESS='';
        $PROPERTY_BEDROOMS = $row['NumberOfBedrooms'];
        $PROPERTY_RECEPTION= $row['NumberOfReceptions'];
        $PROPERTY_BATHROOMS= $row['NumberOfBathrooms'];
        $PARKING = $row['Parking'];
        $OFF_ROAD_PARKING='';        
        $ON_ROAD_PARKING = ''; 
        if($PARKING!=''){
            if(strstr($PARKING, 'On')){
                $ON_ROAD_PARKING = '1';
            }
            if(strstr($PARKING, 'Off')){
                $OFF_ROAD_PARKING = '1';
            }
            if(strstr($PARKING, 'Car')){
                $CAR_PARKING = '1';
            }

        }else{
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING = ''; 
        }
        $GARDEN = $row['Garden'];
        $PROPERTY_TENURE=$row['Tenure'];
        $SERVICE_CHARGE= $row['ServiceCharge'];
        $PROPERTY_DESCRIPTION =$row['PropertyComments'];
        $PROPERTY_PRICE =$row['AskingPrice'];
        $GroundRent=$row['GroundRent'];
        $RemainingLease= $row['RemainingLease'];

        $CUSTOM_FEATURES=array();
         
        
        

        if($row['AdvertisingBulletPointOne']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointOne'];
        }
         if($row['AdvertisingBulletPointTwo']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointTwo'];
        }
         if($row['AdvertisingBulletPointThree']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointThree'];
        }
        if($row['AdvertisingBulletPointFour']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointFour'];
        }
       if($row['AdvertisingBulletPointFive']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointFive'];
        }
        if($row['AdvertisingBulletPointSix']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointSix'];
        }
        if($row['AdvertisingBulletPointSeven']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointSeven'];
        }
         if($row['AdvertisingBulletPointEight']!=''){
            $CUSTOM_FEATURES[]=$row['AdvertisingBulletPointEight'];
        } 
        Utility::filter($CUSTOM_FEATURES);
        $PROPERTY_CUSTOM_FEATURES="".json_encode($CUSTOM_FEATURES)."";
       // echo '<pre>',$PROPERTY_CUSTOM_FEATURES,'</pre>';
        $PROPERTY_STAFF_ID='';
        $PROPERTY_TITLE='';
        $PROPERTY_SHORT_DESCRIPTION='';
        $PROPERTY_CATEGORY='RESIDENTIAL SALES';
        $PROPERTY_PRICE_FREQUENCY='';
        $PROPERTY_QUALIFIER='';
        $PROPERTY_AVAILABLE_DATE='NULL';
        $PROPERTY_ADDRESS_LINE_1='';
        $PROPERTY_FORMATTED_ADDRESS='';
        $STATUS='';
        $PROPERTY_AVAILABILITY='Withdrawn';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';

        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';         
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON='NULL';

        $STATUS=trim($row['Status']);
        if($STATUS=='On Market'){
            $PROPERTY_STATUS='ACTIVE';
            $CLIENT_STATUS='ACTIVE';
        }else{
            $PROPERTY_STATUS='INACTIVE';
            $CLIENT_STATUS='INACTIVE';
        }

        $PROPERTY_ADMIN_FEES='';
        $Lease_term_years='';
        $KITCHEN ='';
        $CERTIFICATE_EXPIRE_DATE='NULL'; 
        $PROPERTY_BUYER_ID='';
        $PROPERTY_VENDOR_SOLICITOR_ID = $row['VendorSolicitorID'];
        $PROPERTY_BUYER_SOLICITOR_ID = '';

        Utility::filter($row['VendorFirstName']);
        Utility::filter($row['VendorSurname']);

        $CLIENT_FIRST_NAME =trim($row['VendorFirstName']);
        $CLIENT_SURNAME = trim($row['VendorSurname']);
        $CLIENT_TITLE = $row['VendorTitle'];
        $CLIENT_NAME  = $CLIENT_FIRST_NAME.' '.$CLIENT_SURNAME;
        $CLIENT_SUB_TYPE= '';
        
        $CLIENT_STAFF_ID='';
        $CLIENT_PRIMARY_EMAIL= $row['VendorEmail'];
        $CLIENT_PRIMARY_PHONE = $row['VendorHomeTelephone'];

        Utility::filter($row['VendorAddressOne']);
        Utility::filter($row['VendorAddressTwo']);

        $CLIENT_ADDRESS_LINE_1 = $row['VendorAddressOne'];
        $CLIENT_ADDRESS_LINE_2 = $row['VendorAddressTwo'];
        $CLIENT_ADDRESS_CITY = $row['VendorCity'];
        $CLIENT_ADDRESS_TOWN = $row['VendorCounty'];
        $CLIENT_ADDRESS_POSTCODE =  $row['VendorPostcode'];
        
        $CLIENT_ADDRESS1_LINE_1 = $row['VendorSecondAddressOne'];
        $CLIENT_ADDRESS1_LINE_2 = $row['VendorSecondAddressTwo'];
        $CLIENT_ADDRESS1_CITY = $row['VendorSecondCity'];
        $CLIENT_ADDRESS1_TOWN = $row['VendorSecondCounty'];
        $CLIENT_ADDRESS1_POSTCODE =  $row['VendorSecondPostcode'];
        
        $CLIENT_ADDRESS2_LINE_1 = '';
        $CLIENT_ADDRESS2_LINE_2 = '';
        $CLIENT_ADDRESS2_CITY = '';
        $CLIENT_ADDRESS2_TOWN = '';
        $CLIENT_ADDRESS2_POSTCODE= '';
        $CLIENT_ACCOUNT_NAME = '';
        $CLIENT_ACCOUNT_NO='';
        $CLIENT_ACCOUNT_SORTCODE='';
        $CLIENT_EMAIL_1 =$row['VendorSecondEmail'];
        $CLIENT_EMAIL_2 ='';
        $CLIENT_EMAIL_3 ='';
        $CLIENT_EMAIL_4 ='';
        $CLIENT_EMAIL_5='';
        $CLIENT_PHONE_1 =$row['VendorWorkTelephone'];
        $CLIENT_PHONE_2 =$row['VendorOtherTelephone'];
        $CLIENT_PHONE_3 =$row['VendorSecondHomeTelephone'];
        $CLIENT_PHONE_4 =$row['VendorSecondWorkTelephone'];
        $CLIENT_PHONE_5 =$row['VendorSecondOtherTelephone'];
        $CLIENT_MOBILE_1 =$row['VendorMobileTelephone'];
        $CLIENT_MOBILE_2 =$row['VendorSecondMobileTelephone'];
        $CLIENT_MOBILE_3 ='';
        $CLIENT_MOBILE_4 ='';
        $CLIENT_MOBILE_5 ='';
        $CLIENT_NOTES =$row['VendorNotes'];
        $CLIENT_FAX_1 ='';
        $CLIENT_FAX_2='';
        $CLIENT_FAX_3 ='';
        $CLIENT_FAX_4 ='';
        $CLIENT_FAX_5 ='';         
        $CLIENT_CREATED_ON =date('Y-m-d H:i:s');
        $SEARCH_CRITERIA='';
       // echo $CLIENT_NAME;exit;
       

       $query_1="SELECT CLIENTID FROM cnb_lopr0.`clients` WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE 'VENDOR'";
      // echo $query_1;exit;
       $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
       if(@$landlord_exists['data'][0]['CLIENTID'] !=''){
       $PROPERTY_VENDOR_ID=@$landlord_exists['data'][0]['CLIENTID'];
       // echo 'vendor id exist'.$PROPERTY_VENDOR_ID.'<br/><br/>';
       }else {
           $rand_no = 'VEN_'.$SNO;
           $sql2 ="INSERT INTO cnb_lopr0.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$rand_no', '$CLIENT_TITLE', '$CLIENT_NAME', 'VENDOR', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA', '0')";
        //   $db_connect->queryExecute($sql2) or die($sql2);
           echo $sql2.'<br/><br/>';
           $PROPERTY_VENDOR_ID=$rand_no;//['data'][0]['CLIENTID'];
         echo 'Added : '.$PROPERTY_VENDOR_ID.'<br/><br/>';
       }

       $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`,`PROPERTY_BUYER_ID`,`PROPERTY_VENDOR_SOLICITOR_ID`,`PROPERTY_BUYER_SOLICITOR_ID`, `PROPERTY_TITLE`,
       `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
       `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
       `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
       `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
       `KITCHEN-DINER`, `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
       `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
       `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
       `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
       `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
       `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
       `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
       `CERTIFICATE_EXPIRE_DATE`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`) 
       VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_BUYER_ID','$PROPERTY_VENDOR_SOLICITOR_ID','$PROPERTY_BUYER_SOLICITOR_ID','$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
       '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE ,
       '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
       '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
       '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
       '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
       '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
       '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
       '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
       '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
       '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
       '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
       '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
       '$PROPERTY_IMAGE_EPC_5', $CERTIFICATE_EXPIRE_DATE, '$PROPERTY_EPC_VALUES', $PROPERTY_CREATED_ON, $PROPERTY_CUSTOM_FEATURES,'$PROPERTY_ROOMS','$PROPERTY_ASSETS')"; 

      // echo $sql;
        $db_connect->queryExecute($sql) or die($sql);
    }
}