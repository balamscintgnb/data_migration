<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = "select * from xproperties where  `VendorFirstName`!='' or `VendorSurname` !='' ORDER BY `id` ASC";

// $query = "select * from properties where ID='2470' ORDER BY 'ID' ASC ";

$data = json_decode($db_connect_src->queryFetch($query),true);
	
if(count($data)>0){
    $SNO=131;
	foreach($data['data'] as $row){
        $SNO++;
    /*  echo '<pre>';
		print_r($row);
		echo '</pre>';

        exit;  */
        $Enter_PROPERTY_ID = $row['ID'];
        $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
        $PROPERTY_ID    = ''.$pro_id;
        $PROPERTY_REF_ID = $PROPERTY_ID;
        $PROPERTY_TYPE  = $row['PropertyType'];

        $PROPERTY_ADDRESS_LINE_1='';
        $add_sub_type_1 = $row['NoName'];
        $add_sub_type_2 = $row['AddressOne'];
        $add_sub_type_3 = $row['AddressTwo'];

        if($add_sub_type_1!=''){
            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
        }
        
        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
        }
        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
        }
        $PROPERTY_ADDRESS_LINE_2= $row['AddressTwo'];
        $PROPERTY_ADDRESS_CITY  = '';
        $PROPERTY_ADDRESS_CITY  = $row['City'];
        $PROPERTY_ADDRESS_COUNTY = $row['County'];
        $PROPERTY_ADDRESS_POSTCODE =  $row['Postcode'];
        $PROPERTY_FORMATTED_ADDRESS='';
        $PROPERTY_BEDROOMS = $row['NumberOfBedrooms'];
        $PROPERTY_RECEPTION= $row['NumberOfReceptions'];
        $PROPERTY_BATHROOMS= $row['NumberOfBathrooms'];
        $PARKING = $row['Parking'];
        $OFF_ROAD_PARKING='';        
        $ON_ROAD_PARKING = ''; 
        if($PARKING!=''){
            if(strstr($PARKING, 'On')){
                $ON_ROAD_PARKING = '1';
            }
            if(strstr($PARKING, 'Off')){
                $OFF_ROAD_PARKING = '1';
            }
            if(strstr($PARKING, 'Car')){
                $CAR_PARKING = '1';
            }

        }else{
            $OFF_ROAD_PARKING='';
            $ON_ROAD_PARKING = ''; 
        }
        $GARDEN = $row['Garden'];
        $PROPERTY_TENURE=$row['Tenure'];
        $PROPERTY_DESCRIPTION =$row['PropertyComments'];

        Utility::filter($row['VendorFirstName']);
        Utility::filter($row['VendorSurname']);

        $CLIENT_FIRST_NAME =trim($row['VendorFirstName']);
        $CLIENT_SURNAME = trim($row['VendorSurname']);
        $CLIENT_TITLE = $row['VendorTitle'];
        $CLIENT_NAME  = $CLIENT_FIRST_NAME.' '.$CLIENT_SURNAME;
        $CLIENT_SUB_TYPE= '';
        $CLIENT_STATUS='INACTIVE';
        $CLIENT_STAFF_ID='';
        $CLIENT_PRIMARY_EMAIL= $row['VendorEmail'];
        $CLIENT_PRIMARY_PHONE = $row['VendorHomeTelephone'];

        Utility::filter($row['VendorAddressOne']);
        Utility::filter($row['VendorAddressTwo']);

        $CLIENT_ADDRESS_LINE_1 = $row['VendorAddressOne'];
        $CLIENT_ADDRESS_LINE_2 = $row['VendorAddressTwo'];
        $CLIENT_ADDRESS_CITY = $row['VendorCity'];
        $CLIENT_ADDRESS_TOWN = $row['VendorCounty'];
        $CLIENT_ADDRESS_POSTCODE =  $row['VendorPostcode'];
        
        $CLIENT_ADDRESS1_LINE_1 = $row['VendorSecondAddressOne'];
        $CLIENT_ADDRESS1_LINE_2 = $row['VendorSecondAddressTwo'];
        $CLIENT_ADDRESS1_CITY = $row['VendorSecondCity'];
        $CLIENT_ADDRESS1_TOWN = $row['VendorSecondCounty'];
        $CLIENT_ADDRESS1_POSTCODE =  $row['VendorSecondPostcode'];
        
        $CLIENT_ADDRESS2_LINE_1 = '';
        $CLIENT_ADDRESS2_LINE_2 = '';
        $CLIENT_ADDRESS2_CITY = '';
        $CLIENT_ADDRESS2_TOWN = '';
        $CLIENT_ADDRESS2_POSTCODE= '';
        $CLIENT_ACCOUNT_NAME = '';
        $CLIENT_ACCOUNT_NO='';
        $CLIENT_ACCOUNT_SORTCODE='';
        $CLIENT_EMAIL_1 =$row['VendorSecondEmail'];
        $CLIENT_EMAIL_2 ='';
        $CLIENT_EMAIL_3 ='';
        $CLIENT_EMAIL_4 ='';
        $CLIENT_EMAIL_5='';
        $CLIENT_PHONE_1 =$row['VendorWorkTelephone'];
        $CLIENT_PHONE_2 =$row['VendorOtherTelephone'];
        $CLIENT_PHONE_3 =$row['VendorSecondHomeTelephone'];
        $CLIENT_PHONE_4 =$row['VendorSecondWorkTelephone'];
        $CLIENT_PHONE_5 =$row['VendorSecondOtherTelephone'];
        $CLIENT_MOBILE_1 =$row['VendorMobileTelephone'];
        $CLIENT_MOBILE_2 =$row['VendorSecondMobileTelephone'];
        $CLIENT_MOBILE_3 ='';
        $CLIENT_MOBILE_4 ='';
        $CLIENT_MOBILE_5 ='';
        $CLIENT_NOTES =$row['VendorNotes'];
        $CLIENT_FAX_1 ='';
        $CLIENT_FAX_2='';
        $CLIENT_FAX_3 ='';
        $CLIENT_FAX_4 ='';
        $CLIENT_FAX_5 ='';         
        $CLIENT_CREATED_ON =date('Y-m-d H:i:s');
        $SEARCH_CRITERIA='';
       // echo $CLIENT_NAME;exit;
       $query_1="SELECT CLIENTID FROM cnb_lopr0.`clients` WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_TYPE` LIKE 'vendor'";
      // echo $query_1;exit;
       $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
       if(@$landlord_exists['data'][0]['CLIENTID'] !=''){
       $PROPERTY_VENDOR_ID=@$landlord_exists['data'][0]['CLIENTID'];
       }else {
           $rand_no = 'VEN_'.$SNO;
           $sql2 ="INSERT INTO cnb_lopr0.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`) VALUES ('$rand_no', '$CLIENT_TITLE', '$CLIENT_NAME', 'VENDOR', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA', '0')";
           $db_connect->queryExecute($sql2) or die($sql2);
          
           $PROPERTY_VENDOR_ID=$rand_no;//['data'][0]['CLIENTID'];

       }


       
    }
}