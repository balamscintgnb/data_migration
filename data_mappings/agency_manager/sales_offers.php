<?php 
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = "SELECT * FROM `xsalespipeline` ORDER BY `ID` ASC";

$data = json_decode($db_connect_src->queryFetch($query),true);
if(count($data)>0){
    $SNO=1;
    $CNO=1;
    foreach($data['data'] as $row){
        $SNO++;
        $SALE_OFFER_ID = $row['ID'];        
        $PROPERTY_APPLICANT_ID = $row['ApplicantID'];
        $SALE_PROPERTY_ID = $row['PropertyId'];
        $STATUS = $row['SalesStatus'];
        $PROPERTY_SALE_OFFER_PRICE = $row['OfferPrice'];
        $AgreedPrice = $row['AgreedPrice'];
        $PROPERTY_SALE_OFFER_DATETIME = $row['SurveyDate'];
        $ExchangeDate = $row['ExchangeDate'];
        $CompletionDate = $row['CompletionDate'];
        $SalesPipelineComments = $row['SalesPipelineComments'];

        $SurveyorID = $row['SurveyorID'];
        $SurveyorName = $row['SurveyorName'];
        $SurveyorContact = $row['SurveyorContact'];
        $SurveyorEmail = $row['SurveyorEmail'];
        $SurveyorReference = $row['SurveyorReference'];

        $SurveyorAddressOne = $row['SurveyorAddressOne'];
        $SurveyorAddressTwo = $row['SurveyorAddressTwo'];
        $SurveyorCity = $row['SurveyorCity'];
        $SurveyorCounty = $row['SurveyorCounty'];
        $SurveyorPostCode = $row['SurveyorPostCode'];

        $SurveyorTelephone = $row['SurveyorTelephone'];
        $SurveyorFax = $row['SurveyorFax'];
        $RenegotiationPrice = $row['RenegotiationPrice'];
        $RenegotiationDate = $row['RenegotiationDate'];
        $SaleAgreedDate = $row['SaleAgreedDate'];

        $FinalSoldPrice = $row['FinalSoldPrice'];
        $Checklist = $row['Checklist'];
        $SalesPipelineExtraOne = $row['SalesPipelineExtraOne'];
        $SalesPipelineExtraTwo = $row['SalesPipelineExtraTwo'];
        $SalesPipelineExtraThree = $row['SalesPipelineExtraThree'];
        $SalesPipelineExtraFour = $row['SalesPipelineExtraFour'];
        $SalesPipelineExtraFive = $row['SalesPipelineExtraFive'];
        $SalesPipelineExtraSix = $row['SalesPipelineExtraSix'];
        $SalesPipelineExtraSeven = $row['SalesPipelineExtraSeven'];
        $SalesPipelineExtraEight = $row['SalesPipelineExtraEight'];

        $SalesPipelineExtraNine = $row['SalesPipelineExtraNine'];
        $SalesPipelineExtraTen = $row['SalesPipelineExtraTen'];
        $SalesPipelineExtraEleven = $row['SalesPipelineExtraEleven'];
        $SalesPipelineExtraTwelve = $row['SalesPipelineExtraTwelve'];
        $Comments = $row['Comments'];
        
if($SurveyorID!='' && $SurveyorID!=NULL){
    $CNO++; 

    $CLIENTID= $PROPERTY_APPLICANT_ID;
    $CLIENT_TYPE='SUPPLIER';
    $CLIENT_SUB_TYPE='SURVEYOR';
    $CLIENT_TITLE='';
    $CLIENT_NAME= $SurveyorName;
    $CLIENT_STATUS='INACTIVE';
   // $CLIENT_STAFF_ID=$SurveyorID;
   $CLIENT_STAFF_ID='';
    $CLIENT_COMPANY_NAME=$SurveyorContact;
    $CLIENT_PRIMARY_EMAIL = $SurveyorEmail;
    $CLIENT_PRIMARY_PHONE=$SurveyorTelephone;
    
    $CLIENT_ADDRESS_LINE_1=$SurveyorAddressOne;
    $CLIENT_ADDRESS_LINE_2=$SurveyorAddressTwo;
    $CLIENT_ADDRESS_CITY=$SurveyorCity;
    $CLIENT_ADDRESS_TOWN=$SurveyorCounty;
    $CLIENT_ADDRESS_POSTCODE=$SurveyorPostCode;

    $CLIENT_ADDRESS1_LINE_1='';
    $CLIENT_ADDRESS1_LINE_2='';
    $CLIENT_ADDRESS1_CITY='';
    $CLIENT_ADDRESS1_TOWN='';
    $CLIENT_ADDRESS1_POSTCODE='';

    $CLIENT_ADDRESS2_LINE_1='';
    $CLIENT_ADDRESS2_LINE_2='';
    $CLIENT_ADDRESS2_CITY='';
    $CLIENT_ADDRESS2_TOWN='';
    $CLIENT_ADDRESS2_POSTCODE='';

    $CLIENT_ACCOUNT_NAME='';
    $CLIENT_ACCOUNT_NO='';
    $CLIENT_ACCOUNT_SORTCODE='';
    $CLIENT_EMAIL_1='';
    $CLIENT_EMAIL_2='';
    $CLIENT_EMAIL_3='';
    $CLIENT_EMAIL_4='';
    $CLIENT_EMAIL_5='';
    $CLIENT_PHONE_1='';
    $CLIENT_PHONE_2='';
    $CLIENT_PHONE_3='';
    $CLIENT_PHONE_4='';
    $CLIENT_PHONE_5='';
    $CLIENT_MOBILE_1='';
    $CLIENT_MOBILE_2='';
    $CLIENT_MOBILE_3='';
    $CLIENT_MOBILE_4='';
    $CLIENT_MOBILE_5='';
    $CLIENT_NOTES='';

    $CLIENT_FAX_1=$SurveyorFax;
    $CLIENT_FAX_2='';
    $CLIENT_FAX_3='';
    $CLIENT_FAX_4='';
    $CLIENT_FAX_5='';
    $CLIENT_CREATED_ON=date('Y-m-d H:i:s');
    $SEARCH_CRITERIA='';
    $PROPERTY_SALE_SOLICITOR_ID ='';
    /* $query_1="SELECT CLIENTID FROM cnb_lopr0.`clients` WHERE `CLIENTID` ='$CLIENTID' AND `CLIENT_TYPE` LIKE 'SUPPLIER'";         
    $supplier_exists = json_decode($db_connect->queryFetch($query_1),true);
    if(@$supplier_exists['data'][0]['CLIENTID'] !=''){
       $SUPPLIER_ID=@$supplier_exists['data'][0]['CLIENTID'];
    }else {  */


    $sql2 ="INSERT INTO cnb_lopr0.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,`CLIENT_COMPANY_NAME`, `CLIENT_PRIMARY_EMAIL`,
        `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`,
        `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, 
        `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`,
        `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
        `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, 
        `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`)
        VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_COMPANY_NAME','$CLIENT_PRIMARY_EMAIL', 
        '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', 
        '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
        '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
        '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4',
        '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2',
        '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5',
        '$CLIENT_CREATED_ON','$SEARCH_CRITERIA', '0')"; 

      //  $db_connect->queryExecute($sql2) or die($sql2);

          echo $CNO.' '. $sql2.'<br/><br/>';
  //  }
          

}
    $pro_id=str_pad($SALE_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
    $SALE_PROPERTY_ID    = ''.$pro_id;

    if($PROPERTY_SALE_OFFER_DATETIME==''){
        $PROPERTY_SALE_OFFER_DATETIME=date('Y-m-d H:i:s');
    }

    if(strstr($STATUS, 'Completed')){
        $PROPERTY_SALE_OFFER_STATUS='1';
    }elseif(strstr($STATUS, 'Fallen Through')){
        $PROPERTY_SALE_OFFER_STATUS='2';
    }elseif(strstr($STATUS, 'Under Offer')){
        $PROPERTY_SALE_OFFER_STATUS='0';
    }else{
        $PROPERTY_SALE_OFFER_STATUS='0';
    } 
    if($SurveyorID!='' && $SurveyorID!=NULL){
        $PROPERTY_APPLICANT_ID =$PROPERTY_APPLICANT_ID;
    }else{
        $PROPERTY_APPLICANT_ID = 'NULL';
    }
    

    $sql = "INSERT INTO `sale_offers` (`SALE_OFFER_ID`, `SALE_PROPERTY_ID`, `PROPERTY_APPLICANT_ID`, `PROPERTY_SALE_OFFER_PRICE`,
    `PROPERTY_SALE_SOLICITOR_ID`, `PROPERTY_SALE_OFFER_STATUS`, `PROPERTY_SALE_OFFER_DATETIME`)
    VALUES ('$SALE_OFFER_ID', '$SALE_PROPERTY_ID', '$PROPERTY_APPLICANT_ID', '$PROPERTY_SALE_OFFER_PRICE', '$PROPERTY_SALE_SOLICITOR_ID',
    '$PROPERTY_SALE_OFFER_STATUS', '$PROPERTY_SALE_OFFER_DATETIME')";    
        // echo $SNO.' '. $sql.'<br/><br/>';
    //  $db_connect->queryExecute($sql) or die($sql);

  
 

  $sql_update="UPDATE cnb_lopr0.properties SET  PROPERTY_BUYER_ID = '$PROPERTY_APPLICANT_ID' WHERE  PROPERTY_ID='$SALE_PROPERTY_ID'";         
  // $db_connect->queryExecute($sql_update) or die($sql_update);
   // echo $SNO.' '. $sql_update.'<br/><br/>';

    }
   
    

}
?>