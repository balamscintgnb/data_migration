<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = "select * from xapplicants  ORDER BY 'ID' ASC";

$data = json_decode($db_connect_src->queryFetch($query),true);
if(count($data)>0){
    $SNO=1;
    foreach($data['data'] as $row){
        $SNO++;

        $search_criteria=array();    
        //logic for data mapping.

        $CLIENTID= $row['ID'];
        $CLIENT_TITLE1=$row['ApplicantTitle'];
        $CLIENT_TITLE2=$row['SecondTitle'];
        Utility::filter($row['ApplicantFirstName']);
        Utility::filter($row['ApplicantSurname']);
        Utility::filter($row['SecondFirstName']);
        Utility::filter($row['SecondSurname']);
        $FirstName =$row['ApplicantFirstName'];        
        $Surname = $row['ApplicantSurname']; 
        $SecondFirstName =$row['SecondFirstName'];
        $SecondSurname =$row['SecondSurname'];

        $CLIENT_TITLE='';
        if($CLIENT_TITLE1!='' && $CLIENT_TITLE2==''){
            $CLIENT_TITLE=$CLIENT_TITLE1;
        }
        if($CLIENT_TITLE2!=''  && $CLIENT_TITLE1!=''){
            $CLIENT_TITLE=$CLIENT_TITLE1.'&'.$CLIENT_TITLE2;
        }
        $ApplicantFirstName = $FirstName.' '.$Surname;
        $ApplicantsecondName='';
        if($SecondFirstName!=''  || $SecondSurname!=''){
            $ApplicantsecondName = ', '.$SecondFirstName.' '.$SecondSurname;
        }
        $applicant_name=$ApplicantFirstName. $ApplicantsecondName;     
        $CLIENT_NAME = trim($applicant_name);
        $COMPANY_NAME=$row['ApplicantCompanyName']; 
        $CLIENT_TYPE='applicant';
        $CLIENT_SUB_TYPE='';
        $CLIENT_STATUS='INACTIVE';

        $CLIENT_STAFF_ID='';
        $CLIENT_PRIMARY_EMAIL= $row['ApplicantEmail'];
        
        $CLIENT_PRIMARY_PHONE=$row['ApplicantHomeTelephone'];
        
        Utility::filter($row['ApplicantAddressOne']);
        Utility::filter($row['ApplicantAddressTwo']);
        Utility::filter($row['ApplicantCity']);
        Utility::filter($row['ApplicantCounty']);

        $CLIENT_ADDRESS_LINE_1=$row['ApplicantAddressOne'];
        $CLIENT_ADDRESS_LINE_2=$row['ApplicantAddressTwo'];
        $CLIENT_ADDRESS_CITY=$row['ApplicantCity'];
        $CLIENT_ADDRESS_TOWN=$row['ApplicantCounty'];
        $CLIENT_ADDRESS_POSTCODE=$row['ApplicantPostcode'];

       

        $CLIENT_ADDRESS1_LINE_1='';
        $CLIENT_ADDRESS1_LINE_2='';
        $CLIENT_ADDRESS1_CITY='';
        $CLIENT_ADDRESS1_TOWN='';
        $CLIENT_ADDRESS1_POSTCODE='';

        $CLIENT_ADDRESS2_LINE_1='';
        $CLIENT_ADDRESS2_LINE_2='';
        $CLIENT_ADDRESS2_CITY='';
        $CLIENT_ADDRESS2_TOWN='';
        $CLIENT_ADDRESS2_POSTCODE='';

        $CLIENT_ACCOUNT_NAME='';
        $CLIENT_ACCOUNT_NO=$row['BankAccountNo'];
        $CLIENT_ACCOUNT_SORTCODE=$row['BankSortCode'];

        $CLIENT_EMAIL_1='';
        $CLIENT_EMAIL_2='';
        $CLIENT_EMAIL_3='';
        $CLIENT_EMAIL_4='';
        $CLIENT_EMAIL_5='';
        $CLIENT_PHONE_1=$row['ApplicantOtherTelephone'];
        $CLIENT_PHONE_2='';
        $CLIENT_PHONE_3='';
        $CLIENT_PHONE_4='';
        $CLIENT_PHONE_5='';

        $MOBILE_1= $row['ApplicantMobile'];
        
        $CLIENT_NUMBER = preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1);
        $CLIENT_MOBILE_1='';
        $CLIENT_MOBILE_2='';
        $CLIENT_MOBILE_3='';
        $CLIENT_MOBILE_4='';
        $CLIENT_MOBILE_5='';
        $CLIENT_NOTES='';
        $CLIENT_FAX_1='';
        $CLIENT_FAX_2='';
        $CLIENT_FAX_3='';
        $CLIENT_FAX_4='';
        $CLIENT_FAX_5='';
        $CLIENT_CREATED_ON=date('Y-m-d H:i:s');
        
        //ADD SEARCH CRITE AREA ///
        $Maximum_Price=$row['MaximumPrice'];
        $Minimum_Bedrooms=$row['MinimumBedrooms'];
        $Minimum_Bathrooms=$row['MinimumBathrooms'];
        $Minimum_Ensuites='';
        $Minimum_Toilets='';
        $Minimum_Reception_Rooms=$row['MinimumReceptions'];
        $Floor_Area_Actual_Units_Sq_Ft='';
        $Minimum_Acres='';
        $Property_Style=$row['ApplicantPropertyType'];
        $category='43';

        $applicant_search_criteria = array('property_applicant_search_attribute_1'=>$Property_Style,
        'property_applicant_search_attribute_2_from'=>$Minimum_Bedrooms, 
        'property_applicant_search_attribute_2_to'=>'',
        'property_applicant_search_attribute_3_from'=>$Minimum_Bathrooms, 
        'property_applicant_search_attribute_3_to'=>'',
        'property_applicant_search_attribute_4_from'=>$Minimum_Reception_Rooms, 
        'property_applicant_search_attribute_4_to'=>'');
            
        $search_criteria=array('price'=>$Maximum_Price,'pricefrom'=>'',
        'filter_array'=>$applicant_search_criteria,
        'frequency'=>'','category'=>$category,'location'=>'');

        //print_r($search_criteria);

        //exit;
        

        $SEARCH_CRITERIA = json_encode($search_criteria);

        if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
            if($CLIENT_NAME!=''){
                $CLIENT_NAME.=' ';
            }
            $CLIENT_NAME.='('.$COMPANY_NAME.')';
        }

         /////////////MOBILE NUMBER TYPE////////////////
         $tel_type='';
         



         if($tel_type=="Mobile"){
             $CLIENT_MOBILE_1 = $CLIENT_NUMBER;
         }

         else if($tel_type=="Home"){
             $CLIENT_PRIMARY_PHONE = $CLIENT_NUMBER;   
         }  
         else if($tel_type=="Work"){
             $CLIENT_PHONE_1 = $CLIENT_NUMBER;   
         }

         //////////////// END /////////////////


     $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, 
                `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`,
                `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, 
                `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, 
                `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, 
                `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, 
                `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, 
                `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,
                `SEARCH_CRITERIA`,`RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', 
                '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2',
                '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', 
                '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
                '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO',
                '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5',
                '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1',
                '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', 
                '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '$SEARCH_CRITERIA','0')";
        // echo $sql .'<br/>'; exit;
     $db_connect->queryExecute($sql) or die($sql);

    }
}  
 