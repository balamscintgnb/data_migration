<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once '../includes/config.php';

require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);
$query = "select * from contacts where ContactsType = 'Solicitor' ORDER BY 'id' ASC";

$data = json_decode($db_connect_src->queryFetch($query),true);
if(count($data)>0){
    $SNO=1;
	foreach($data['data'] as $row){
        $SNO++;        
        $CLIENTID = $row['ID'];
        $CLIENT_TITLE= '';
        $CLIENT_NAME= $row['Contactname'];
        $CLIENT_COMPANY_NAME= $row['ContactsCompany'];

        if($CLIENT_NAME=='' && $CLIENT_COMPANY_NAME!=''){
            $CLIENT_NAME= $CLIENT_COMPANY_NAME;
        }
        if($CLIENT_COMPANY_NAME=='' && $CLIENT_NAME!=''){
            $CLIENT_COMPANY_NAME= $CLIENT_NAME;
        }
        $CLIENT_SUB_TYPE='';
        $CLIENT_STATUS=$row['contact_status'];
        $CLIENT_STAFF_ID =
        $CLIENT_TYPE= strtoupper($row['ContactsType']);
        $CLIENT_PRIMARY_EMAIL= $row['ContactsEmail']; 
        $CLIENT_PRIMARY_PHONE = $row['ContactsMainTelephone'];
        $CLIENT_ADDRESS_LINE_1=$row['ContactsAddressOne'];
        $CLIENT_ADDRESS_LINE_2=$row['ContactsAddressTwo'];
        $CLIENT_ADDRESS_CITY=$row['ContactsCity'];
        $CLIENT_ADDRESS_TOWN=$row['ContactsCounty'];
        $CLIENT_ADDRESS_POSTCODE=$row['ContactsPostcode'];
        $CLIENT_ADDRESS1_LINE_1= '';
        $CLIENT_ADDRESS1_LINE_2='';
        $CLIENT_ADDRESS1_CITY='';
        $CLIENT_ADDRESS1_TOWN='';
        $CLIENT_ADDRESS1_POSTCODE='';
        $CLIENT_ADDRESS2_LINE_1='';
        $CLIENT_ADDRESS2_LINE_2='';
        $CLIENT_ADDRESS2_CITY='';
        $CLIENT_ADDRESS2_TOWN='';
        $CLIENT_ADDRESS2_POSTCODE='';
        $CLIENT_ACCOUNT_NAME='';
        $CLIENT_ACCOUNT_NO='';
        $CLIENT_ACCOUNT_SORTCODE='';
        $CLIENT_EMAIL_1=$row['ContactsEmail1'];
        $CLIENT_EMAIL_2='';
        $CLIENT_EMAIL_3='';
        $CLIENT_EMAIL_4='';
        $CLIENT_EMAIL_5='';
        $CLIENT_PHONE_1=$row['ContactsSecondTelephone'];
        $CLIENT_PHONE_2='';
        $CLIENT_PHONE_3='';
        $CLIENT_PHONE_4='';
        $CLIENT_PHONE_5='';
        $CLIENT_MOBILE_1=$row['ContactsMobile'];
        $CLIENT_MOBILE_2='';
        $CLIENT_MOBILE_3='';
        $CLIENT_MOBILE_4='';
        $CLIENT_MOBILE_5='';
        $CLIENT_NOTES=$row['ContactsComments'];
        $CLIENT_FAX_1=$row['ContactsFax'];
        $CLIENT_FAX_2='';
        $CLIENT_FAX_3='';
        $CLIENT_FAX_4='';
        $CLIENT_FAX_5='';
        $CLIENT_CREATED_ON=date('Y-m-d H:i:s');
        $SEARCH_CRITERIA='';
        
 
         $sql2 ="INSERT INTO cnb_lopr0.`clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`,`CLIENT_COMPANY_NAME`, `CLIENT_PRIMARY_EMAIL`,
         `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`,
          `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, 
          `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`,
           `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`,
            `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, 
            `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `SEARCH_CRITERIA`, `RECORD_UPLOADED`)
         VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_COMPANY_NAME','$CLIENT_PRIMARY_EMAIL', 
         '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', 
         '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE',
          '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE',
           '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4',
            '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2',
             '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5',
              '$CLIENT_CREATED_ON','$SEARCH_CRITERIA', '0')"; 
              
             $db_connect->queryExecute($sql2) or die($sql2);
         
    }
}

