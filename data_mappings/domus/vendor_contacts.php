<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
$file_name='../source_data/domus/vendorcustomers.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


            //logic for data mapping.

            $CLIENTID='';
            $CLIENT_TITLE=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $CLIENT_NAME=addslashes($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
            $COMPANY_NAME='';

						$CLIENT_TYPE			= 'VENDOR';
						$CLIENT_SUB_TYPE		= '';

						$COMPANY_NAME='';

						if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
							if($CLIENT_NAME!=''){
								$CLIENT_NAME.=' ';
							}
							$CLIENT_NAME.='('.$COMPANY_NAME.')';
						}

						$CLIENT_ADDRESS_LINE_1 	= '';
						$CLIENT_ADDRESS_LINE_2	= '';
						$CLIENT_ADDRESS_CITY	= '';
						$CLIENT_ADDRESS_TOWN	= '';
						$CLIENT_ADDRESS_POSTCODE= '';

						$housename=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
						$housenumber=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
						$street=trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
						$locality=trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
						$town=trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
						$county=trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
						$postcode=trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());

						if($housename!=''){
							$CLIENT_ADDRESS_LINE_1.=$housename;
						}

						if($housenumber!=''){
							if($CLIENT_ADDRESS_LINE_1!=""){
								$CLIENT_ADDRESS_LINE_1.=', ';
							}
							$CLIENT_ADDRESS_LINE_1.=$housenumber;
						}

						if($street!=''){
							$CLIENT_ADDRESS_LINE_2.=$street;
						}

						if($locality!=''){
							$CLIENT_ADDRESS_CITY.=$locality;
						}

						if($town!=''){
							$CLIENT_ADDRESS_TOWN.=$town;
						}

						if($county!=''){
							if($CLIENT_ADDRESS_TOWN!=""){
								$CLIENT_ADDRESS_TOWN.=', ';
							}
							$CLIENT_ADDRESS_TOWN.=$county;
						}

						if($postcode!=''){
							$CLIENT_ADDRESS_POSTCODE.=$postcode;
						}

						$CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=addslashes($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            $CLIENT_PRIMARY_PHONE=addslashes($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
						$CLIENT_ADDRESS_LINE_1			= $CLIENT_ADDRESS_LINE_1;
						$CLIENT_ADDRESS_LINE_2			= $CLIENT_ADDRESS_LINE_2;
						$CLIENT_ADDRESS_CITY			= $CLIENT_ADDRESS_CITY;
						$CLIENT_ADDRESS_TOWN			= $CLIENT_ADDRESS_TOWN;
						$CLIENT_ADDRESS_POSTCODE		= $CLIENT_ADDRESS_POSTCODE;

            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';
            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES='';
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $CLIENT_CREATED_ON='';

						$CLIENT_NAME=addslashes($CLIENT_NAME);

					  $CLIENT_NOTES=addslashes($CLIENT_NOTES);




            /*echo '<pre><code>';
        print_r();
        echo '</code></pre>';*/


			$sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`,
				`CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`,
				`CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`,
				`CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`,
				`CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`,
				`CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`,
				`CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`,`SEARCH_CRITERIA`) VALUES
				('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE',
				'$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1',
				'$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2',
				'$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1',
				'$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5',
				'$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3',
				'$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON','$SEARCH_CRITERIA')";
			  echo"<br/><br/>";
			//echo $row;
        $db_connect->queryExecute($sql) or die ($sql);

        }
    }
}
?>
