  <?php
  error_reporting(0);
  //ini_set('display_errors',1);

  require_once '../../header_init.php';
  require_once '../includes/config.php';
  require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

  //HUM4RX_

  extract($_GET);

  //save file as XLS,



      $file_name1='../source_data/domus/rooms.xls';

      $thisProceed=true;

      try {
          //Load the excel(.xls/.xlsx) file
          $objPHPExcel = PHPExcel_IOFactory::load($file_name1);
      } catch (Exception $e) {
          $thisProceed=false;
          die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);

      //It returns the highest number of rows
      $total_rows = $sheet->getHighestRow();

      //print_r($sheet);exit;

      //It returns the highest number of columns
      //$highest_column = $sheet->getHighestColumn();

      $rooms = array();

      if($thisProceed){

          for($row =1; $row <= $total_rows; $row++){

              if($row>1){
                  $room_id=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()));
                  $property_id=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue()));
                  $floor_name=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
                  $room_name=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
                  $length=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
                  $width=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()));
                  $description=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue()));

                  if($property_id!=''){
                      $rooms[$property_id][] = array('room_id'=>$room_id, 'room_name'=>$room_name, 'floor_name'=>$floor_name, 'length'=>$length, 'width'=>$width, 'description'=>$description);
                  }
              }
          }
      }

      //delete  FROM `address_book` WHERE `address_user_type` IN (3,4,5)

      //
      $file_name2='../source_data/domus/propertytypes.xls';

      $thisProceed=true;

      try {
          //Load the excel(.xls/.xlsx) file
          $objPHPExcel = PHPExcel_IOFactory::load($file_name2);
      } catch (Exception $e) {
          $thisProceed=false;
          die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);

      //It returns the highest number of rows
      $total_rows = $sheet->getHighestRow();

      //It returns the highest number of columns
      //$highest_column = $sheet->getHighestColumn();


      $propertyTypes = array();

      if($thisProceed){

          for($row =1; $row <= $total_rows; $row++){

              if($row>1){
                  $property_id=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                  $description=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
                  $rightmove_group=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());

                  if($property_id!=''){
                      $propertyTypes[$property_id] = array('description'=>$description, 'rightmove_group'=>$rightmove_group);
                  }
              }
          }
      }
      //

      //
      $file_name3='../source_data/domus/propertymatchoptions.xls';

      $thisProceed=true;

      try {
          //Load the excel(.xls/.xlsx) file
          $objPHPExcel = PHPExcel_IOFactory::load($file_name3);
      } catch (Exception $e) {
          $thisProceed=false;
          die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);

      //It returns the highest number of rows
      $total_rows = $sheet->getHighestRow();

      //It returns the highest number of columns
      //$highest_column = $sheet->getHighestColumn();

      $propertyFeatures = array();

      if($thisProceed){

          for($row =1; $row <= $total_rows; $row++){

              if($row>1){
                  $property_id=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                  $matchOption=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());

                  if($property_id!=''){
                      $propertyFeatures[$property_id][] = $matchOption;
                  }
              }
          }
      }
      //

      $property_related_data = 'hce_properties.json';

      $properties = array();

      if(!file_exists($property_related_data)){

          if(count($rooms)>0){
              $properties['rooms']=$rooms;
          }

          if(count($propertyTypes)>0){
              $properties['property_types']=$propertyTypes;
          }

          if(count($propertyFeatures)>0){
              $properties['property_features']=$propertyFeatures;
          }

          file_put_contents($property_related_data, json_encode($properties));

          echo 'Config Data Written<br/>';
      }


	  /////////////sales////////////


	  $file_name3='../source_data/domus/propertysales.xls';

      $thisProceed=true;

      try {
          //Load the excel(.xls/.xlsx) file
          $objPHPExcel = PHPExcel_IOFactory::load($file_name3);
      } catch (Exception $e) {
          $thisProceed=false;
          die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);

      //It returns the highest number of rows
      $total_rows = $sheet->getHighestRow();

      //It returns the highest number of columns
      //$highest_column = $sheet->getHighestColumn();

      $propertySales = array();

      if($thisProceed){

          for($row =1; $row <= $total_rows; $row++){

              if($row>1){
                  $property_id=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

                  if($property_id!=''){
                      $propertySales[] = $property_id;
                  }
              }
          }
      }

	  ///////

	  //echo '<pre>',print_r($propertySales),'</pre>';

      //properties import
      $file_name4='../source_data/domus/properties.xls';

      $thisProceed=true;

      try {
          //Load the excel(.xls/.xlsx) file
          $objPHPExcel = PHPExcel_IOFactory::load($file_name4);
      } catch (Exception $e) {
          $thisProceed=false;
          die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);

      //It returns the highest number of rows
      $total_rows = $sheet->getHighestRow();

      //It returns the highest number of columns
      //$highest_column = $sheet->getHighestColumn();

      $rooms = array();

	  if($thisProceed){

          for($row =1; $row <= $total_rows; $row++){

              if($row>1){

                  $PROPERTY_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());

				  $PROPERTY_CATEGORY		= '';

				  $cate='';

				  if(in_array($PROPERTY_ID,$propertySales)){
					  echo $PROPERTY_ID;
					  echo '<br/>';
					  $PROPERTY_CATEGORY="RESIDENTIAL SALES";
					  $cate='43';
				  }else{
				  	  $PROPERTY_CATEGORY="RESIDENTIAL LETTINGS";
					  $cate='44';
				  }

                  $PROPERTY_REF_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
                  $PROPERTY_VENDOR_ID=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());

        					$PROPERTY_ADDRESS_LINE_1	= '';
        					$PROPERTY_ADDRESS_LINE_2	= '';
        					$PROPERTY_ADDRESS_CITY		= '';
        					$PROPERTY_ADDRESS_COUNTY	= '';
        					$PROPERTY_ADDRESS_POSTCODE	= '';

                  $area=trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());

                  $housename=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());
                  $housenumber=trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
                  $street=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
                  $locality=trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
                  $town=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
                  $county=trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue());
                  $postcode=trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
                  $countrycode=trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());

                  if($housename!=''){
              			$PROPERTY_ADDRESS_LINE_1.=$housename;
              		}

              		if($housenumber!=''){
              			if($PROPERTY_ADDRESS_LINE_1!=""){
              				$PROPERTY_ADDRESS_LINE_1.=', ';
              			}
              			$PROPERTY_ADDRESS_LINE_1.=$housenumber;
              		}

              		if($street!=''){
              			$PROPERTY_ADDRESS_LINE_2.=$street;
              		}

              		if($locality!=''){
              			$PROPERTY_ADDRESS_CITY.=$locality;
              		}

              		if($area!=''){
              			if($PROPERTY_ADDRESS_CITY!=""){
              				$PROPERTY_ADDRESS_CITY.=', ';
              			}
              			$PROPERTY_ADDRESS_CITY.=$area;
              		}

              		if($town!=''){
              			$PROPERTY_ADDRESS_COUNTY.=$town;
              		}

              		if($county!=''){
              			if($PROPERTY_ADDRESS_COUNTY!=""){
              				$PROPERTY_ADDRESS_COUNTY.=', ';
              			}
              			$PROPERTY_ADDRESS_COUNTY.=$county;
              		}

              		if($postcode!=''){
              			$PROPERTY_ADDRESS_POSTCODE.=$postcode;
              		}

              		$PROPERTY_ADDRESS_LINE_1		= $PROPERTY_ADDRESS_LINE_1;
              		$PROPERTY_ADDRESS_LINE_2		= $PROPERTY_ADDRESS_LINE_2;
              		$PROPERTY_ADDRESS_CITY			= $PROPERTY_ADDRESS_CITY;
              		$PROPERTY_ADDRESS_COUNTY		= $PROPERTY_ADDRESS_COUNTY;
              		$PROPERTY_ADDRESS_POSTCODE		= $PROPERTY_ADDRESS_POSTCODE;


                  $latitude=trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
                  $longitude=trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());

                  $PROPERTY_TYPE=trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue());

                  $PROPERTY_TENURE=trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());
                  $PROPERTY_BEDROOMS=trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());

                  $PROPERTY_PRICE=trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());

				  if($PROPERTY_PRICE>50000){
				  	$PROPERTY_CATEGORY="RESIDENTIAL SALES";
					$cate='43';
				  }

                  $PROPERTY_ADMIN_FEES=trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue());
                  $PROPERTY_AVAILABLE_DATE=trim(Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getformattedValue(),'Y-m-d'));



                  $currentEnergyEfficiency=trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
                  $potentialEnergyEfficiency=trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue());
                  $currentEnvironmentImpact=trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());
                  $potentialEnvironmentImpact=trim($objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue());


                  $PROPERTY_EPC_VALUES = json_encode(array("energy_current"=>$currentEnergyEfficiency, "energy_potential"=> $potentialEnergyEfficiency,
                  "co2_current"=>$currentEnvironmentImpact, "co2_potential"=> $potentialEnvironmentImpact));

                  $features_array				= array();

                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue()));
                  $features_array[]=preg_replace( '/[^[:print:]]/', '',trim($objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue()));

                  $PROPERTY_CUSTOM_FEATURES	= json_encode($features_array);

                  $PROPERTY_DESCRIPTION=trim($objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue());

                  $PROPERTY_DESCRIPTION=preg_replace( '/[^[:print:]]/', '',$PROPERTY_DESCRIPTION);

                  $lastGasCheck=trim($objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue());
                  $nextGasCheck=trim($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue());
                  $nextElectricCheck=trim($objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue());

                  $dateCreated=trim(Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getformattedValue(),'Y-m-d H:i:s'));

				          $PROPERTY_CREATED_ON		= $dateCreated;

                  $notes=trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());

                  if($notes!=''){
              			$PROPERTY_DESCRIPTION.='\n'.$notes;
              		}

                  $PROPERTY_TENURE=trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue());

                  //todo check
      				    if($cate=="43"){
                        	$PROPERTY_AVAILABILITY=trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
      				   }else if($cate=="44"){
                        	$PROPERTY_AVAILABILITY=trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue());
      				   }

      				   if($PROPERTY_AVAILABILITY=="New Valuation" || $PROPERTY_AVAILABILITY=="Valuation Completed"){
      						$PROPERTY_AVAILABILITY	= 'Appraisal';
      					}

      					if($PROPERTY_AVAILABILITY=="Sold Subject to Contract"){
      						$PROPERTY_AVAILABILITY	= 'Sold';
      					}


                  $GAS_CERTIFICATE=trim($objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue());

                  $ELECTRIC_CERTIFICATE=trim($objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue());

                  $certificates_array='';

                  if($GAS_CERTIFICATE!=''){
                    $certificates_array = array('1'=>$GAS_CERTIFICATE, '3'=>$ELECTRIC_CERTIFICATE);
                  }

				          $rooms = array();
                  if($property_id!=''){
                      $rooms[$property_id][] = array('room_id'=>$room_id, 'room_name'=>$room_name, 'floor_name'=>$floor_name, 'length'=>$length, 'width'=>$width, 'description'=>$description);
                  }

                  $PROPERTY_ASSETS			= json_encode(array());

                  $PROPERTY_ROOMS				= json_encode($rooms);

				          $PROPERTY_CERTIFICATE				= json_encode($certificates_array);

              }

              echo $sql = "INSERT INTO properties (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`, `CONCIERGE_SERVICES`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ROOMS`, `PROPERTY_ASSETS`, `PROPERTY_EPC_VALUES`,`CERTIFICATE_EXPIRE_DATE`,`PROPERTY_CREATED_ON`) VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE', '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE', '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS', '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM', '$FURNISHING_STATUS', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY', '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_CUSTOM_FEATURES', '$PROPERTY_ROOMS', '$PROPERTY_ASSETS', '$PROPERTY_EPC_VALUES','$PROPERTY_CERTIFICATE','$PROPERTY_CREATED_ON')";

          		$db_connect->queryExecute($sql);

          }
      }

  ?>
