<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/domus/tenancies.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//print_r($sheet);exit;

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

          $id='';
          $LETTING_ID='';
          $LETTING_CUSTOM_REF_NO=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
          $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
          $PROPERTY_ROOM_ID='';

					$query_tenant="SELECT Customer_Id FROM tenants WHERE `Tenancy_Id` LIKE '$LETTING_CUSTOM_REF_NO' order by Customer_Id asc";
	        $FIND_PRO_ID = json_decode($db_connect->queryFetch($query_tenant),true);
	        $FIND_PRO_ID = $FIND_PRO_ID['data'] ;
	        $TENANT_ID=$FIND_PRO_ID[0]['Customer_Id'];
					$TENANT_ID =preg_replace( '/[^[:print:]]/', '',$TENANT_ID);

	        $SHARED_TENANT_IDS='';
	        if(count($FIND_PRO_ID)>1){
	            $shared_tenant_ids=array();
	            array_shift($FIND_PRO_ID);
	            foreach($FIND_PRO_ID as $shared_tenant_id){
								  $shared_tenant=preg_replace( '/[^[:print:]]/', '',$shared_tenant_id['Customer_Id']);
	                array_push($shared_tenant_ids,$shared_tenant);
	            }
	            $SHARED_TENANT_IDS = json_encode($shared_tenant_ids);
					}


					$LETTING_START_DATE='';

	        $given_date1=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getformattedValue(),'Y-m-d');

					if($given_date1!=''){
              $LETTING_START_DATE=$given_date1;
          }
          if($given_date1==''){
              $LETTING_START_DATE="0000-00-00";
          }

	        $LETTING_END_DATE='';

					$term = trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

					$end_Date = date('Y-m-d', strtotime("+$term months", strtotime($given_date1)));

					$end_Date = date('Y-m-d',strtotime("-1 days".$end_Date));

					$LETTING_END_DATE=$end_Date;


          $LETTING_RENT=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
          $RENT_FREQUENCY='';

          $LETTING_RENT_FREQUENCY='pcm';
					$LETTING_RENT_PAYMENT_FREQUENCY='monthly';

          // if($RENT_FREQUENCY=='Monthly'){
          //     $LETTING_RENT_FREQUENCY='pcm';
					// 		$LETTING_RENT_PAYMENT_FREQUENCY='monthly';
          // }else if($RENT_FREQUENCY=='Weekly'){
          //     $LETTING_RENT_FREQUENCY='pw';
					// 		$LETTING_RENT_PAYMENT_FREQUENCY='weekly';
          // }else if($RENT_FREQUENCY=='Four Weekly'){
          //     $LETTING_RENT_FREQUENCY='p4w';
					// 		$LETTING_RENT_PAYMENT_FREQUENCY='four_weekly';
          // }else if($RENT_FREQUENCY=='6 month'){
          //     $LETTING_RENT_FREQUENCY='p6m';
					// 		$LETTING_RENT_PAYMENT_FREQUENCY='semi_annually';
          // }else if($RENT_FREQUENCY=='8 week'){
          //     $LETTING_RENT_FREQUENCY='';
					// 		$LETTING_RENT_PAYMENT_FREQUENCY='';
          // }

          $LETTING_RENT_PAYMENT_DAY='';
          $LETTING_RENT_ADVANCE='';
          $LETTING_RENT_ADVANCE_TYPE='';
          $LETTING_NOTICE_PERIOD='';
          $LETTING_NOTICE_PERIOD_TYPE='';

          $BREAK_CLAUSE='';

          $LETTING_BREAK_CLAUSE='';

          // if($BREAK_CLAUSE=='6 months' || $BREAK_CLAUSE=='12 months'){
          //   $LETTING_BREAK_CLAUSE=$BREAK_CLAUSE;
          // }else{
          //   $LETTING_BREAK_CLAUSE='';
          // }

          $LETTING_BREAK_CLAUSE_TYPE='';

					$BREAK_CLAUSE_DATE='';

					// $PROPERTY_AVAILABLE_DATE='';
	        // if($BREAK_CLAUSE_DATE!='0000-00-00'){
	        // 	$LETTING_BREAK_CLAUSE_DATE=$BREAK_CLAUSE_DATE;
	        // }

          $LETTING_DEPOSIT=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
          $LETTING_DEPOSIT_DUE_DATE='';

					$DEPOSIT_PROTECTION_SCHEME=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
					$LETTING_DEPOSIT_HELD_BY='';

					$LETTING_DEPOSIT_PROTECTION_SCHEME='';
					$DEPOSIT_HELD_BY=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();

					if($DEPOSIT_HELD_BY==1){
							$LETTING_DEPOSIT_HELD_BY=2;
					}

					if(stristr($DEPOSIT_PROTECTION_SCHEME,'The Deposit Protection Service')){
							$LETTING_DEPOSIT_PROTECTION_SCHEME=1;
					}else if(stristr($DEPOSIT_PROTECTION_SCHEME,'Tenancy Deposit Solutions Ltd') || stristr($DEPOSIT_PROTECTION_SCHEME,'The Tenancy Deposit Scheme')){
						  $LETTING_DEPOSIT_PROTECTION_SCHEME=4;
					}


          $MANAGED_LEVEL=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();

					$LETTING_SERVICE='';

	        if($MANAGED_LEVEL!=''){
	            if(stristr($MANAGED_LEVEL, 'Let only')){
	                $LETTING_SERVICE=2;
	            }else if(stristr($MANAGED_LEVEL, 'Managed')){
	                $LETTING_SERVICE=1;
	            }else if(stristr($MANAGED_LEVEL, 'Rent collection')){
	                $LETTING_SERVICE=3;
	            }
	        }

					$LETTING_FEE_TYPE='';

          $LETTING_FEES=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
					if($LETTING_FEES>0){
          	$LETTING_FEE_TYPE='2';
					}

          $LETTING_FEE_FREQUENCY='';
					$LETTING_MANAGEMENT_FEE_TYPE='';

          $LETTING_MANAGEMENT_FEES=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();

					if($LETTING_MANAGEMENT_FEES>0){
          	 $LETTING_MANAGEMENT_FEE_TYPE='2';
					}

          $LETTING_MANAGEMENT_FEE_FREQUENCY='';
          $LETTING_ADMIN_FEES='';
          $LETTING_ADMIN_FEE_TYPE='';
          $LETTING_INVENTORY_FEES='';
          $LETTING_INVENTORY_FEE_TYPE='';
          $LETTING_RENT_GUARANTEE='';
          $LETTING_LANDLORD_PAYMENT='';
          $LETTING_VAT_PERCENTAGE='';

					$NOTES=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();

					if($NOTES!=''){
          	$LETTING_NOTES='First payment date: '.$NOTES;
					}


       echo $sql = "INSERT INTO `lettings` (`LETTING_ID`, `LETTING_CUSTOM_REF_NO`, `PROPERTY_ID`, `PROPERTY_ROOM_ID`, `TENANT_ID`,`SHARED_TENANT_IDS`,`LETTING_START_DATE`,
          `LETTING_END_DATE`, `LETTING_RENT`, `LETTING_RENT_FREQUENCY`, `LETTING_RENT_PAYMENT_FREQUENCY`, `LETTING_RENT_PAYMENT_DAY`, `LETTING_RENT_ADVANCE`,
          `LETTING_RENT_ADVANCE_TYPE`, `LETTING_NOTICE_PERIOD`, `LETTING_NOTICE_PERIOD_TYPE`, `LETTING_BREAK_CLAUSE`,`LETTING_BREAK_CLAUSE_TYPE`, `LETTING_DEPOSIT`,
          `LETTING_DEPOSIT_DUE_DATE`, `LETTING_DEPOSIT_HELD_BY`, `LETTING_DEPOSIT_PROTECTION_SCHEME`, `LETTING_SERVICE`, `LETTING_FEES`, `LETTING_FEE_TYPE`,
          `LETTING_FEE_FREQUENCY`, `LETTING_MANAGEMENT_FEES`, `LETTING_MANAGEMENT_FEE_TYPE`, `LETTING_MANAGEMENT_FEE_FREQUENCY`, `LETTING_ADMIN_FEES`,
          `LETTING_ADMIN_FEE_TYPE`, `LETTING_INVENTORY_FEES`, `LETTING_INVENTORY_FEE_TYPE`, `LETTING_RENT_GUARANTEE`, `LETTING_LANDLORD_PAYMENT`, `LETTING_VAT_PERCENTAGE`,`LETTING_NOTES`)
           VALUES ('$LETTING_ID','$LETTING_CUSTOM_REF_NO', '$PROPERTY_ID', '$PROPERTY_ROOM_ID', '$TENANT_ID','$SHARED_TENANT_IDS','$LETTING_START_DATE',
					'$LETTING_END_DATE', '$LETTING_RENT', '$LETTING_RENT_FREQUENCY','$LETTING_RENT_PAYMENT_FREQUENCY', '$LETTING_RENT_PAYMENT_DAY', '$LETTING_RENT_ADVANCE',
					'$LETTING_RENT_ADVANCE_TYPE', '$LETTING_NOTICE_PERIOD', '$LETTING_NOTICE_PERIOD_TYPE', '$LETTING_BREAK_CLAUSE','$LETTING_BREAK_CLAUSE_TYPE', '$LETTING_DEPOSIT'
					, '$LETTING_DEPOSIT_DUE_DATE', '$LETTING_DEPOSIT_HELD_BY', '$LETTING_DEPOSIT_PROTECTION_SCHEME', '$LETTING_SERVICE','$LETTING_FEES','$LETTING_FEE_TYPE',
					'$LETTING_FEE_FREQUENCY','$LETTING_MANAGEMENT_FEES', '$LETTING_MANAGEMENT_FEE_TYPE', '$LETTING_MANAGEMENT_FEE_FREQUENCY', '$LETTING_ADMIN_FEES','$LETTING_ADMIN_FEE_TYPE','$LETTING_INVENTORY_FEES',
					 '$LETTING_INVENTORY_FEE_TYPE', '$LETTING_RENT_GUARANTEE', '$LETTING_LANDLORD_PAYMENT', '$LETTING_VAT_PERCENTAGE','$LETTING_NOTES')";

          $db_connect->queryExecute($sql);

					// if($row>20){
					// 	break;
					// }


        }
    }

}
