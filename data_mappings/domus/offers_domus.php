<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/domus/offers.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){


    $CATEGORY_ID='';

    $CONTACT_ID=0;

    $PROPERTY_ID=0;

    $Status = '';

    $PROPERTY_PRICE_FREQUENCY	= '';

		$VENDOR_SOLICITOR_ID=0;

		$BUYER_SOLICITOR_ID=0;

    $ID		= $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();

		$PROPERTY_ID				= $objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();

    $query_property="SELECT `PROPERTY_ID`,`PROPERTY_CATEGORY` FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' order by PROPERTY_ID asc";
    $property_fetch = json_decode($db_connect->queryFetch($query_property),true);
    $property_fetch_data = $property_fetch['data'] ;
    $PROPERTY_ID=$property_fetch_data[0]['PROPERTY_ID'];
    $PROPERTY_CATEGORY=$property_fetch_data[0]['PROPERTY_CATEGORY'];

    $APPLICANT_ID = $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();

		$AMOUNT = $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

		$property_offer_status = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();

    if($property_offer_status=="Pending"){
  		$Status=0;
  	}else if($property_offer_status=="Accepted"){
  		$Status=1;
  	}else if($property_offer_status=="Declined" || $property_offer_status=="Withdrawn" || $property_offer_status=="Contracts Exchanged"){
  		$Status=2;
  	}

    echo $PROPERTY_CATEGORY;

    echo '<br>';

    $VENDOR_SOLICITOR_ID = $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();

    $BUYER_SOLICITOR_ID = $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();

    $OFFER_DATE	= Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getformattedValue(),'Y-m-d');

    if($PROPERTY_CATEGORY=='RESIDENTIAL LETTINGS'){
  	    $sql = "INSERT INTO `letting_offers` (`PROPERTY_LETTING_OFFER_ID`, `PROPERTY_ID`, `PROPERTY_APPLICANT_ID`, `PROPERTY_LETTING_OFFER_RENT`,
  			`PROPERTY_LETTING_OFFER_RENT_FREQUENCY`, `PROPERTY_LETTING_OFFER_STATUS`, `PROPERTY_LETTING_OFFER_DATETIME`)
  			VALUES ('$ID', '$PROPERTY_ID', '$APPLICANT_ID', '$AMOUNT', '$PROPERTY_PRICE_FREQUENCY', '$Status', '$OFFER_DATE');";
		}if($PROPERTY_CATEGORY=='RESIDENTIAL SALES'){
				$sql = "INSERT INTO `sale_offers` (`SALE_OFFER_ID`, `SALE_PROPERTY_ID`, `PROPERTY_APPLICANT_ID`,
				`PROPERTY_SALE_OFFER_PRICE`,`PROPERTY_SALE_VENDOR_SOLICITOR_ID`,`PROPERTY_SALE_BUYER_SOLICITOR_ID`,`PROPERTY_SALE_OFFER_STATUS`,`PROPERTY_SALE_OFFER_DATETIME`)
        VALUES ('$ID', '$PROPERTY_ID', '$APPLICANT_ID', '$AMOUNT','$VENDOR_SOLICITOR_ID','$BUYER_SOLICITOR_ID','$Status','$OFFER_DATE')";
		}

		$db_connect->queryExecute($sql);

  }

}

}

?>
