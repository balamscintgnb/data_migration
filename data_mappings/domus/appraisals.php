<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/premier/Lettings/appraisals_02-10-2018_13-42-49.xlsx';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
                    //$id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()*/;
        
        

        //var_dump($id);

        //logic for data mapping.
        
        $Enter_PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $pro_id=str_pad($Enter_PROPERTY_ID, 6, "0", STR_PAD_LEFT);
            $PROPERTY_ID = ''.$pro_id;

        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();
        $PROPERTY_VENDOR_ID=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
        $PROPERTY_CATEGORY=''; 

        $PROPERTY_ADDRESS_LINE_1='';

            $add_sub_type_1=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
            $add_sub_type_2=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $add_sub_type_3=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();

        if($add_sub_type_1!=''){
            $PROPERTY_ADDRESS_LINE_1.=$add_sub_type_1;
        } 

        if($add_sub_type_2!='' && $add_sub_type_1!=$add_sub_type_2){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=', ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_2;
        }
        
        if($add_sub_type_3!='' && $add_sub_type_2!=$add_sub_type_3){
			if($PROPERTY_ADDRESS_LINE_1!=""){
				$PROPERTY_ADDRESS_LINE_1.=' ';
			}
			$PROPERTY_ADDRESS_LINE_1.=$add_sub_type_3;
        } 

        $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY='';
        $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();
        $PROPERTY_STATUS='';
        $PROPERTY_TYPE='';
        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION='';
        $PROPERTY_TENURE='';
        $PROPERTY_AGE='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CONDITION='';
        $PROPERTY_PRICE_FROM=$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();
        $PROPERTY_PRICE_TO=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $PROPERTY_VENDOR_PRICE='';
        $PROPERTY_PROPOSED_PRICE='';
        $PROPERTY_VALUER_NOTES='';

        $Floor_Area=$objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue();
        $Guide_Rent=$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue();
        $Rent_Frequency=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();
        $PROPERTY_VALUER_NOTES="Floor Area is : ".$Floor_Area." ";
        if($Guide_Rent!=''){
            $PROPERTY_VALUER_NOTES.='| Guide Rent is: '.$Guide_Rent;
        }
    
        if($Rent_Frequency!=''&& $Guide_Rent!=0){
            $PROPERTY_VALUER_NOTES.=$Rent_Frequency;
        }


        $PROPERTY_VALUER_NOTES_1='';
        $PROPERTY_VALUER_NOTES_2='';
        $PROPERTY_VALUER_NOTES_3='';
        $PROPERTY_VALUER_NOTES_4='';
        $PROPERTY_VALUER_NOTES_5='';
        $STARTTIME=trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue());
        $PROPERTY_APPOINTMENT = date('H:i:s',strtotime($STARTTIME));
        
        $PROPERTY_APPOINTMENT_ENDTIME='NULL';
        $PROPERTY_APPOINTMENT_FOLLOWUPDATE='NULL';

        $given_date=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue());

        if($given_date!=''){
        $test_date = explode(' ',$given_date);
        $test_date1 = explode('-',$test_date[0]);
        $PROPERTY_APPOINTMENT_STARTTIME="'".$test_date1[0].'-'.$test_date1[1].'-'.$test_date1[2].' '.$PROPERTY_APPOINTMENT."'";
        }
        if($given_date==''){
            $PROPERTY_APPOINTMENT_STARTTIME="NULL";
        }

        

        $query_1="SELECT PROPERTY_TYPE FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' ";

        $landlord_exist = json_decode($db_connect->queryFetch($query_1),true);

        $PROPERTY_TYPE=$landlord_exist['data'][0]['PROPERTY_TYPE'];

        

        $query_2="SELECT PROPERTY_BATHROOMS FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' ";

        $landlord_exis = json_decode($db_connect->queryFetch($query_2),true);

        $PROPERTY_BATHROOMS=$landlord_exis['data'][0]['PROPERTY_BATHROOMS'];




        $query_3="SELECT PROPERTY_RECEPTION FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' ";

        $landlord_exi = json_decode($db_connect->queryFetch($query_3),true);

        $PROPERTY_RECEPTION=$landlord_exi['data'][0]['PROPERTY_RECEPTION'];




        $query_4="SELECT PROPERTY_STATUS FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' ";

        $landlord_exiss = json_decode($db_connect->queryFetch($query_4),true);

        $PROPERTY_STATUS=$landlord_exiss['data'][0]['PROPERTY_STATUS'];



        $query_5="SELECT PROPERTY_CATEGORY FROM properties WHERE `PROPERTY_ID` LIKE '$PROPERTY_ID' ";

        $landlord_exs = json_decode($db_connect->queryFetch($query_5),true);

        $PROPERTY_CATEGORY=$landlord_exs['data'][0]['PROPERTY_CATEGORY'];



        $sql="INSERT INTO `valuations`(`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_CATEGORY`, `PROPERTY_ADDRESS_LINE_1`,
                    `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_STATUS`, `PROPERTY_TYPE`,
                    `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `PROPERTY_AGE`, `PROPERTY_CLASSIFICATION`,
                    `PROPERTY_CONDITION`, `PROPERTY_PRICE_FROM`, `PROPERTY_PRICE_TO`, `PROPERTY_VENDOR_PRICE`, `PROPERTY_PROPOSED_PRICE`, `PROPERTY_VALUER_NOTES`,
                    `PROPERTY_VALUER_NOTES_1`, `PROPERTY_VALUER_NOTES_2`, `PROPERTY_VALUER_NOTES_3`, `PROPERTY_VALUER_NOTES_4`, `PROPERTY_VALUER_NOTES_5`,
                    `PROPERTY_APPOINTMENT_STARTTIME`, `PROPERTY_APPOINTMENT_ENDTIME`, `PROPERTY_APPOINTMENT_FOLLOWUPDATE`) VALUES
                    ('$PROPERTY_ID','$PROPERTY_REF_ID','$PROPERTY_VENDOR_ID','$PROPERTY_CATEGORY','$PROPERTY_ADDRESS_LINE_1','$PROPERTY_ADDRESS_LINE_2',
                    '$PROPERTY_ADDRESS_CITY','$PROPERTY_ADDRESS_COUNTY','$PROPERTY_ADDRESS_POSTCODE','$PROPERTY_STATUS','$PROPERTY_TYPE',
                    '$PROPERTY_BEDROOMS','$PROPERTY_BATHROOMS','$PROPERTY_RECEPTION','$PROPERTY_TENURE','$PROPERTY_AGE','$PROPERTY_CLASSIFICATION',
                    '$PROPERTY_CONDITION','$PROPERTY_PRICE_FROM','$PROPERTY_PRICE_TO','$PROPERTY_VENDOR_PRICE','$PROPERTY_PROPOSED_PRICE',
                    '$PROPERTY_VALUER_NOTES','$PROPERTY_VALUER_NOTES_1','$PROPERTY_VALUER_NOTES_2','$PROPERTY_VALUER_NOTES_3','$PROPERTY_VALUER_NOTES_4',
                    '$PROPERTY_VALUER_NOTES_5',$PROPERTY_APPOINTMENT_STARTTIME,$PROPERTY_APPOINTMENT_ENDTIME,$PROPERTY_APPOINTMENT_FOLLOWUPDATE)";
                echo "<br/><br/>";
                echo$row;
             $db_connect->queryExecute($sql) or die($sql);
        }
    }
}
?>