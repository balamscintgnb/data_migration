<?php
include_once '../classes/gnbcore.class.php';
include_once '../classes/utility.class.php';

$server = $_SERVER["SERVER_NAME"];

if(stristr($server, 'localhost')){
    $db_name = 'cnb_gnbdata'; 
}else{
    $db_name=Utility::get_subdomain($server);
}

include_once 'includes/config.php';

$db_connect = new GNBCore($sql_details, $isDevelopment);

$subdomain_name = 'idlo';

$absolute_path = dirname(__FILE__);
$absolute_path = str_replace('/cnb/temp', '/cnb', $absolute_path);

$utility = new Utility();
$properties = $db_connect->queryFetch("SELECT * FROM `property_images` WHERE `RECORD_UPLOADED` = 0");
$properties = json_decode($properties);
$array = array();

foreach($properties->data as $d){
	if(!isset($array[$d->PROPERTY_SERVER_ID])){
		if(!isset($array[$d->PROPERTY_SERVER_ID][$d->PROPERTY_IMAGE_TYPE])){
			$local_path = $absolute_path.'/data/'.$subdomain_name.'/'.$d->PROPERTY_IMAGE;
			$filename = basename($d->PROPERTY_IMAGE);
			if(!file_exists($local_path)){
				$filename = str_replace('.jpg','.JPG',$filename);
			}
		
			$server_path = 'cnb/images/'.$subdomain_name.'/notices/'.$d->PROPERTY_SERVER_ID.'/gallery/'.$filename;
			$status = $utility->remove_from_cdn_server($server_path);
			$array[$d->PROPERTY_SERVER_ID][$d->PROPERTY_IMAGE_TYPE] = $filename;
		}
	}
}
echo json_encode($array);

