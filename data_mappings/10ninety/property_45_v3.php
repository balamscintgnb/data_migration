<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../data/london_golden/sales.xlsx';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
                    //$id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()*/;
        
        

        //var_dump($id);

        //logic for data mapping.
        $PROPERTY_DESCRIPTION='';
        $Maintenance_charge='';
        //$Ground_rent='';

        $PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_VENDOR_ID=$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue();
        $CLIENT_NAME=$objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue();
        $PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
        $PROPERTY_TITLE=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $PROPERTY_SHORT_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
        $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
        $PROPERTY_CATEGORY=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY='';
        $PROPERTY_QUALIFIER=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
        $PROPERTY_AVAILABLE_DATE='NULL';
        $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
        $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        $PROPERTY_STATUS='';
        $PROPERTY_AVAILABILITY=$objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue();
        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();

        $PROPERTY_BEDROOMS='';
        $PROPERTY_BATHROOMS='';
        $PROPERTY_RECEPTION='';
        $PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue();
        $Lease_term_years=$objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue();
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON='';


        /////////////////////////////ADD MORE FIELDS/////////////////////////
      
        $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION);
        $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);
        $PROPERTY_FORMATTED_ADDRESS=addslashes($PROPERTY_FORMATTED_ADDRESS);
        $PROPERTY_ADDRESS_LINE_1=addslashes($PROPERTY_ADDRESS_LINE_1);

        /*$PROPERTY_ACCETABLE_PRICE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();        

        if($PROPERTY_ACCETABLE_PRICE!=''){
            $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
        }
        $Ground_rent=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();        

        if($Ground_rent!=''){
            $PROPERTY_DESCRIPTION.='\n Ground rent: '.$Ground_rent;
        }*/
        $Maintenance_charge=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
        if($Maintenance_charge!=''){
            $PROPERTY_ADMIN_FEES.='Maintenance charge: '.$Maintenance_charge;
        }

        echo '<pre><code>';
        print_r();
        echo '</code></pre>';

    
        $query_1="SELECT CLIENTID FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' ";

        $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);

        $PROPERTY_VENDOR_ID=$landlord_exists['data'][0]['CLIENTID'];




        echo $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
        `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
        `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
        `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
        `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
        `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
        `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
        `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
        `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
        `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
        `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
        `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`) 
         VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
        '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
        '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
        '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
        '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
        '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
        '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
        '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
        '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
        '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
        '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
        '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
        '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
        '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','','','')";

        echo "<br/><br/>";
    
    //insert into table    
    $db_connect->queryExecute($sql);

    }
}
}
?>