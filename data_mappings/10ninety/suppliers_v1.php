<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='../source_data/fairdeal/contacts/Suppliers.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
            $CLIENT_NAME=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
            $CLIENT_TITLE='';
            $COMPANY_NAME=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $CLIENT_TYPE='SUPPLIER';
            $CLIENT_SUB_TYPE=$objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue();
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
            //$CLIENT_PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();

            $PRIMARY_PHONE=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
            $CLIENT_PRIMARY_PHONE = preg_replace('/[^A-Za-z0-9\-]/', '', $PRIMARY_PHONE);

            $CLIENT_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
            $CLIENT_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
            $CLIENT_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $CLIENT_ADDRESS_TOWN=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
            $CLIENT_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            //$CLIENT_PHONE_1=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();

            $PHONE=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
            $CLIENT_PHONE_1 = preg_replace('/[^A-Za-z0-9\-]/', '', $PHONE);

            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';

            $CLIENT_MOBILE_1='';
            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES=addslashes(str_replace("'",'',$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue()));
            $CLIENT_FAX_1=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $SEARCH_CRITERIA='NULL';
            $CLIENT_CREATED_ON='';
            



        if($COMPANY_NAME!='' && $COMPANY_NAME!=$CLIENT_NAME){
			if($CLIENT_NAME!=''){
				$CLIENT_NAME.=' ';
			}
			$CLIENT_NAME.='('.$COMPANY_NAME.')';
		}

    

         // select email , mobile 
     //$query_1="SELECT id FROM clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND  `CLIENT_PRIMARY_PHONE` LIKE '$CLIENT_PRIMARY_PHONE' AND `CLIENT_PRIMARY_EMAIL` LIKE '$CLIENT_PRIMARY_EMAIL'";

        //$duplicate_exists = json_decode($db_connect->queryFetch($query_1),true);
//PRINT_R($duplicate_exists);
      $num_rows = count($duplicate_exists['data']); 
     
       if($num_rows=="0"){
           echo  $sql ="INSERT
           INTO
                        `clients`(
                   `CLIENTID`,
                   `CLIENT_TITLE`,
                   `CLIENT_NAME`,
                   `CLIENT_TYPE`,
                   `CLIENT_SUB_TYPE`,
                   `CLIENT_STATUS`,
                   `CLIENT_STAFF_ID`,
                   `CLIENT_PRIMARY_EMAIL`,
                   `CLIENT_PRIMARY_PHONE`,
                   `CLIENT_ADDRESS_LINE_1`,
                   `CLIENT_ADDRESS_LINE_2`,
                   `CLIENT_ADDRESS_CITY`,
                   `CLIENT_ADDRESS_TOWN`,
                   `CLIENT_ADDRESS_POSTCODE`,
                   `CLIENT_ADDRESS1_LINE_1`,
                   `CLIENT_ADDRESS1_LINE_2`,
                   `CLIENT_ADDRESS1_CITY`,
                   `CLIENT_ADDRESS1_TOWN`,
                   `CLIENT_ADDRESS1_POSTCODE`,
                   `CLIENT_ADDRESS2_LINE_1`,
                   `CLIENT_ADDRESS2_LINE_2`,
                   `CLIENT_ADDRESS2_CITY`,
                   `CLIENT_ADDRESS2_TOWN`,
                   `CLIENT_ADDRESS2_POSTCODE`,
                   `CLIENT_ACCOUNT_NAME`,
                   `CLIENT_ACCOUNT_NO`,
                   `CLIENT_ACCOUNT_SORTCODE`,
                   `CLIENT_EMAIL_1`,
                   `CLIENT_EMAIL_2`,
                   `CLIENT_EMAIL_3`,
                   `CLIENT_EMAIL_4`,
                   `CLIENT_EMAIL_5`,
                   `CLIENT_PHONE_1`,
                   `CLIENT_PHONE_2`,
                   `CLIENT_PHONE_3`,
                   `CLIENT_PHONE_4`,
                   `CLIENT_PHONE_5`,
                   `CLIENT_MOBILE_1`,
                   `CLIENT_MOBILE_2`,
                   `CLIENT_MOBILE_3`,
                   `CLIENT_MOBILE_4`,
                   `CLIENT_MOBILE_5`,
                   `CLIENT_NOTES`,
                   `CLIENT_FAX_1`,
                   `CLIENT_FAX_2`,
                   `CLIENT_FAX_3`,
                   `CLIENT_FAX_4`,
                   `CLIENT_FAX_5`,
                   `CLIENT_CREATED_ON`,
                   `SEARCH_CRITERIA`
               )
           VALUES(
               '$CLIENTID',
               '$CLIENT_TITLE',
               '$CLIENT_NAME',
               '$CLIENT_TYPE',
               '$CLIENT_SUB_TYPE',
               '$CLIENT_STATUS',
               '$CLIENT_STAFF_ID',
               '$CLIENT_PRIMARY_EMAIL',
               '$CLIENT_PRIMARY_PHONE',
               '$CLIENT_ADDRESS_LINE_1',
               '$CLIENT_ADDRESS_LINE_2',
               '$CLIENT_ADDRESS_CITY',
               '$CLIENT_ADDRESS_TOWN',
               '$CLIENT_ADDRESS_POSTCODE',
               '$CLIENT_ADDRESS1_LINE_1',
               '$CLIENT_ADDRESS1_LINE_2',
               '$CLIENT_ADDRESS1_CITY',
               '$CLIENT_ADDRESS1_TOWN',
               '$CLIENT_ADDRESS1_POSTCODE',
               '$CLIENT_ADDRESS2_LINE_1',
               '$CLIENT_ADDRESS2_LINE_2',
               '$CLIENT_ADDRESS2_CITY',
               '$CLIENT_ADDRESS2_TOWN',
               '$CLIENT_ADDRESS2_POSTCODE',
               '$CLIENT_ACCOUNT_NAME',
               '$CLIENT_ACCOUNT_NO',
               '$CLIENT_ACCOUNT_SORTCODE',
               '$CLIENT_EMAIL_1',
               '$CLIENT_EMAIL_2',
               '$CLIENT_EMAIL_3',
               '$CLIENT_EMAIL_4',
               '$CLIENT_EMAIL_5',
               '$CLIENT_PHONE_1',
               '$CLIENT_PHONE_2',
               '$CLIENT_PHONE_3',
               '$CLIENT_PHONE_4',
               '$CLIENT_PHONE_5',
               '$CLIENT_MOBILE_1',
               '$CLIENT_MOBILE_2',
               '$CLIENT_MOBILE_3',
               '$CLIENT_MOBILE_4',
               '$CLIENT_MOBILE_5',
               '$CLIENT_NOTES',
               '$CLIENT_FAX_1',
               '$CLIENT_FAX_2',
               '$CLIENT_FAX_3',
               '$CLIENT_FAX_4',
               '$CLIENT_FAX_5',
               '$CLIENT_CREATED_ON',
                $SEARCH_CRITERIA
           )";
            echo "<br/><br/>";
        
            $db_connect->queryExecute($sql) or die($sql);
            echo $updated = $row." Inserted <br /> ";

       }
       else { 
        echo $updated = $row." Updated <br />";
      

       }

       

        //echo $sql ="INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_ADDRESS1_LINE_1`, `CLIENT_ADDRESS1_LINE_2`, `CLIENT_ADDRESS1_CITY`, `CLIENT_ADDRESS1_TOWN`, `CLIENT_ADDRESS1_POSTCODE`, `CLIENT_ADDRESS2_LINE_1`, `CLIENT_ADDRESS2_LINE_2`, `CLIENT_ADDRESS2_CITY`, `CLIENT_ADDRESS2_TOWN`, `CLIENT_ADDRESS2_POSTCODE`, `CLIENT_ACCOUNT_NAME`, `CLIENT_ACCOUNT_NO`, `CLIENT_ACCOUNT_SORTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_NOTES`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_ADDRESS1_LINE_1', '$CLIENT_ADDRESS1_LINE_2', '$CLIENT_ADDRESS1_CITY', '$CLIENT_ADDRESS1_TOWN', '$CLIENT_ADDRESS1_POSTCODE', '$CLIENT_ADDRESS2_LINE_1', '$CLIENT_ADDRESS2_LINE_2', '$CLIENT_ADDRESS2_CITY', '$CLIENT_ADDRESS2_TOWN', '$CLIENT_ADDRESS2_POSTCODE', '$CLIENT_ACCOUNT_NAME', '$CLIENT_ACCOUNT_NO', '$CLIENT_ACCOUNT_SORTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_NOTES', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_CREATED_ON', '0')";

        //$db_connect->queryExecute($sql);

        }
    }
}
?>