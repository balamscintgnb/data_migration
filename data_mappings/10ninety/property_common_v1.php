<?php
/*echo '<pre><code>';
var_dump($property);
echo '</pre></code><br/><br/>';*/

$this_api = $property['Property id'];
$api_key=$this_api;

//$landlord_id=(int)$property["Llord"];

$property_type=strtolower(trim($property["Type"]));

$business_class=trim($property["Category"]);
$property_class=trim($property["For"]);
/*
9 	P.Built
9 	Room
*/

$this_property_type='';

if(stristr($property_type, 'Flat')){
    $this_property_type='Flat';
}else if(stristr($property_type, 'Double Room')){
    $this_property_type='Room';
}else if(stristr($property_type, 'Single Room')){
    $this_property_type='Room';
}else if(stristr($property_type, 'Retail')){
    $this_property_type='terraced';
}else if(stristr($property_type, 'Office')){
    $this_property_type='Office';
}else if(stristr($property_type, 'Terraced House')){
    $this_property_type='Terraced';
}else if(stristr($property_type, 'Studio')){
    $this_property_type='Studio';
}else if(stristr($property_type, 'Semi-Detached House')){
    $this_property_type='Semi Detached';
}else if(stristr($property_type, 'Maisonette')){
    $this_property_type='Maisonette';
}else{
    $set_property_type = str_replace( array("-", " "), "_", $property_type);
    $property_type_filtered_array = arrayLikeSearch($set_property_type, $property_type_push_check_array);
    $this_property_type=array_pop($property_type_filtered_array);
}

$this_property_type=ucwords(strtolower(str_replace('_', ' ', $this_property_type))); 
//die;
//availability status
$notice_option_1=-2;
$notice_option_2=0;

//residential, rent 

if($property_class=='1' && $business_class=='Residential') {
	$notice_category=43;
} else if($property_class=='1' && $business_class=='Commercial') {
	$notice_category=45;
} else if($property_class=='2' && $business_class=='Residential') {
	$notice_category=44;
} else if($property_class=='2' && $business_class=='Commercial') {
	$notice_category=46;
}


//$notice_category=44;

$availability_status=trim($property["Status"]);//availability status
    
if(stristr($availability_status, 'Available')){
    $notice_option_1=0;
}


if($property_class=='2') {
if(stristr($availability_status, 'Let')){
    $notice_option_1=4;
}

if(stristr($availability_status, 'Let Agreed')){
    $notice_option_1=3;
}

if(stristr($availability_status, 'Available')){
    $notice_option_1=0;
}

if(stristr($availability_status, 'To let')){
    $notice_option_1=4;
}
}

if($property_class=='1') { 
	if(stristr($availability_status, 'Sold')){
        $notice_option_1=4;
    }
	if(stristr($availability_status, 'SSTC')){
        $notice_option_1=3;
    }
}

$rent_frequency=trim($property["Price qualifier"]);
$let_rent_frequency_text = '';

//echo $notice_price=(float)trim($property["Price"]);
$notice_price = $property["Price"];
$notice_price = preg_replace("/[^0-9\.]/", '', $notice_price);


$notice_option_2=0;
//1-pa, pcm-2, pw -3
if($rent_frequency==='Monthly'){
    $let_rent_frequency_text = 'pcm';
    $notice_option_2=2;
}

if($rent_frequency==='Annual'){
    $let_rent_frequency_text = 'pa';
    $notice_option_2=1;
}

if($rent_frequency==='Daily'){
    $let_rent_frequency_text = 'pw';
    $notice_option_2=3;
    $notice_price = $notice_price * 7;
}


$bedrooms=(int)trim($property["Beds"]);
$bathrooms=(int)trim($property["Baths"]);
$living_rooms=(int)trim($property["Receptions"]);

$furnished_type_update_string='';
/*
$furniture=(int)trim($property["Furnishid"]);


if($furniture===1){
    $furnished_type_update_string='Furnished';
}

if($furniture===0){
    $furnished_type_update_string='Furnished or Unfurnished';
}

if($furniture===2){
    $furnished_type_update_string='Unfurnished';
}

if($furniture===3){
    $furnished_type_update_string='Part Furnished';
}
*/
$description=trim($property["Full details"]);
$additionalinfo=trim($property["Additional Information"]);

$descBillparking=trim($property["Parking"]);
$descBillservices=trim($property["Service type"]);
$descBillaccommodation=trim($property["Accommodation"]);


if($additionalinfo!='' && $additionalinfo!='none'){
	if($description == '') {
		$description.=$additionalinfo;
	}else {
		$description.='<br/><br/>'.$additionalinfo;
	}
   
}
$available=true;


if($descBillparking!='' && $descBillparking!='none'){
	if($description == '') {
		$description.=$descBillparking;
	}else {
    	$description.='<br/><br/>'.$descBillparking;
	}
}
if($descBillservices!='' && $descBillservices!='none'){
	if($description == '') {
		$description.=$descBillservices;
	}else {
    	$description.='<br/><br/>'.$descBillservices;
	}
}
if($descBillaccommodation!='' && $descBillaccommodation!='none'){
	if($description == '') {
		$description.=$descBillaccommodation;
	}else {
		$description.='<br/><br/>'.$descBillaccommodation;
	}
}




//archived 1-archived, 0-active ~~~
$archived=(int)trim($property["Archived"]);

if($archived===1){
    $available=false;
}

$pets=(int)trim($property["Pets"]);
$smoking=(int)trim($property["Smoking"]);


$garden=(int)trim($property["Garden"]);
$dss=(int)trim($property["Dssallowed"]);

$garage=(int)trim($property["Garage"]);
if($garage==1){
    $description.='<br/><b>Garage Available</b>';
}

$burglarAlarm=(int)trim($property["Burglaralarm"]);


if($burglarAlarm==1){
    $description.='<br/><b>Burglar Alarm Available</b>';
}

$washMachine=(int)trim($property["Washmachine"]);
$dishWasher=(int)trim($property["Dishwasher"]);
//$kitchen=(int)trim($property["Kitchen"]);
$lift=(int)trim($property["Lift"]);
$internet=(int)trim($property["Internet"]);

$heating=trim($property["Heating"]);

if($heating!=''){
    $description.='<br/><b>Heating: </b> '.$heating;
}

$latitude='';
$longitude='';
$notice_address_line1='';
$notice_address_line2='';
$notice_city='';
$notice_county='';
$notice_post_code='';
$notice_country='';


$latitude=trim($property["Latitude"]);
$longitude=trim($property["Longitude"]);


$property_availbale_from=trim($property["Available date"]);
$property_availbale_from=str_replace("/", "-", $property_availbale_from);
//die;
$post_code=trim($property["Postcode"]);
$notice_address_line1=trim($property["Address1"]);
$notice_address_line2=trim($property["Address2"]);

$notice_city=trim($property["Town"]);
if($notice_city=='') {
	$notice_city = trim($property["County"]);
	$notice_county = '';
} else {
	$notice_county=trim($property["County"]);
}

$notice_post_code = trim($property["Postcode"]);

//$notice_country=trim($property["Country"]);

//$display_address=trim($property["Display address"]);

$tenure_type='';
$tenure_type_value=trim($property["Tenure"]);
if($tenure_type_value!=''){
    if(stristr($tenure_type_value, 'Freehold')){
        $tenure_type='Freehold';
    }
    if(stristr($tenure_type_value, 'Leasehold')){
        $tenure_type='Leasehold';
    }
    if(stristr($tenure_type_value, 'Share of Freehold')){
        $tenure_type='Leasehold';
    }
}

/*
$prop_address=$house_no;

$notice_address_line1=$house_no;

$temp_display_address=$house_name;

if($house_name!=''){
    if($prop_address!=''){
        $prop_address.=$house_name;
    }
    $prop_address.=$house_name;
    
    if($notice_address_line1!=''){
        $notice_address_line1.=', ';
    }
    $notice_address_line1.=$house_name;
}




if($town!=''){
    if($prop_address!=''){
        $prop_address.=', ';
    }
    $prop_address.=$town;
    
    if($temp_display_address!=''){
        $temp_display_address.=', ';
    }
    $temp_display_address.=$town;
}

if($city!=''){
    if($prop_address!=''){
        $prop_address.=', ';
    }
    $prop_address.=$city;
    
    $notice_city=$city;
    
    if($temp_display_address!=''){
        $temp_display_address.=', ';
    }
    $temp_display_address.=$city;
    
    $notice_county=$town;
    
}else{
    $notice_city=$town;
}

if($area!=''){
    if($prop_address!=''){
        $prop_address.=', ';
    }
    $prop_address.=$area;
    
    if($temp_display_address!=''){
        $temp_display_address.=', ';
    }
    $temp_display_address.=$area;
    
    if($notice_city==''){
        $notice_city=$area;
    }else{
        $notice_address_line2=$area;
    }
}

if($post_code!=''){
    if($prop_address!=''){
        $prop_address.=', ';
    }
    $prop_address.=$post_code;
    
    $notice_post_code=$post_code;
} */

$title='';

if($bedrooms>0){
    $title=$bedrooms.' bedroom';
}

if($title!=''){
    $title.=' ';
}

if($this_property_type!=''){
    $title.=$this_property_type;
}else{
    $title.='Property';
}

if($notice_category==44 || $notice_category==46){
    $title.=' for rent';
}

if($notice_category==43 || $notice_category==45){
    $title.=' for sale';
}

echo "Title: $title<br/>";

$categories = $cnb_category_attributes[$notice_category];
$filter_attribute=array();

foreach ($categories as  $key => $val) {
    if($val['attribute_name']=="Bedrooms"){
        $filter_attribute[$val['attribute_type']][$key] = $bedrooms;
    }else if($val['attribute_name']=="Bathrooms"){
        $filter_attribute[$val['attribute_type']][$key] = $bathrooms;
    }else  if($val['attribute_name']=="Property Type"){
        $filter_attribute[$val['attribute_type']][$key] = $this_property_type;
    }else  if($val['attribute_name']=="Reception Rooms"){
        $filter_attribute[$val['attribute_type']][$key] = $living_rooms;
    }else if($val['attribute_name']=="Furnishing" && $furnished_type_update_string!=''){
		$filter_attribute[$val['attribute_type']][$key] = $furnished_type_update_string;
	}else if($val['attribute_name']=="Off-road Parking" && $parking==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Pets Allowed" && $pets==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Smoking Allowed" && $smoking==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Garden" && $garden==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="DSS Allowed" && $dss==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Washer" && $washMachine==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Dishwasher" && $dishWasher==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}/*else if($val['attribute_name']=="Kitchen-Diner" && $kitchen==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}*/else if($val['attribute_name']=="Elevator in Building" && $lift==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Internet" && $internet==1){
		$filter_attribute[$val['attribute_type']][$key] = 1;
	}else if($val['attribute_name']=="Tenure" && $tenure_type!='' && $tenure_type!='Feudal'){
 		$filter_attribute[$val['attribute_type']][$key] = $tenure_type;
	}
}
?>