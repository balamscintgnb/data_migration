<?php
error_reporting(E_ERROR);
require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\247\residential sales/Properties (3).xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){
       

        //$id=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue()*/;
        
        

        //var_dump($id);

        //logic for data mapping.
        $PROPERTY_DESCRIPTION='';
       
        $PROPERTY_SHORT_DESCRIPTION='';

        $PROPERTY_ID='247ps_'.$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_REF_ID='247ps_'.$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $CLIENT_NAME=$objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue();
        $PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
        $PROPERTY_TITLE=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $PROPERTY_SHORT_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
        $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
        $PROPERTY_CATEGORY='RESIDENTIAL SALES';
        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY='';

       
        $get_price_qualifier = strtolower($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue());
        if($get_price_qualifier=='offersover')
            $PROPERTY_QUALIFIER='Offers over';   
        else if($get_price_qualifier=='fixedprice')
        $PROPERTY_QUALIFIER='fixed price';   
        else if($get_price_qualifier=='guideprice')
        $PROPERTY_QUALIFIER='guide price';   
        else if($get_price_qualifier=='price')
        $PROPERTY_QUALIFIER='From';   
        else if($get_price_qualifier=='oiro')
        $PROPERTY_QUALIFIER='Offers in the region of'; 
        
        $PROPERTY_AVAILABLE_DATE='NULL';
        $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
        $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
       
        
        $get_property_status = $objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue();

        if($get_property_status != ''){
            if(strtolower($get_property_status) == 'inactive'){
                    $PROPERTY_STATUS='Inactive';
                    $PROPERTY_AVAILABILITY='WITHDRAWN';
            } else { 
                    $PROPERTY_STATUS='Inactive';
                    $PROPERTY_AVAILABILITY=$get_property_status;
            
            }
        }
        else { 
            $PROPERTY_STATUS='Inactive';
            $PROPERTY_AVAILABILITY='ARCHIVED';
        }

        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();

        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();
        $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();
        $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $PROPERTY_TENURE=$objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue();
        $PROPERTY_CLASSIFICATION=$objPHPExcel->getActiveSheet()->getCell('BO'.$row)->getValue();
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_CREATED_ON='';


        /////////////////////////////ADD MORE FIELDS/////////////////////////

        $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);
        $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION);


       $PROPERTY_ACCETABLE_PRICE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();        

        if($PROPERTY_ACCETABLE_PRICE!=''){
            $PROPERTY_DESCRIPTION.="\n Minimum accetable price: ".$PROPERTY_ACCETABLE_PRICE;
        }

        $Ground_rent=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();        

        if($Ground_rent!=''){
            $PROPERTY_DESCRIPTION.="\n Ground rent: ".$Ground_rent;
        }


        $Maintenance_charge=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
        if($Maintenance_charge!=''){
            $PROPERTY_ADMIN_FEES="Maintenance charge: ".$Maintenance_charge;
        }
        $PROPERTY_VENDOR_ID=0;

        $CLIENT_PRIMARY_EMAIL=$objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue();

        $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue().'---'. $query_1="SELECT CLIENTID FROM cnb_2pa.clients WHERE `CLIENT_NAME` LIKE '$CLIENT_NAME' AND `CLIENT_PRIMARY_EMAIL`='$CLIENT_PRIMARY_EMAIL' AND CLIENT_TYPE LIKE 'VENDOR'  ";
      
     $test_name=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();
      

        $landlord_exists = json_decode($db_connect->queryFetch($query_1),true);
        if($landlord_exists['data'][0]['CLIENTID']){
        continue;
        }

        else {
          
            ECHO $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue(); 
            ECHO ' ------ ';
             $query_2="SELECT CLIENTID FROM cnb_2pa.clients WHERE `CLIENT_NAME` LIKE '$test_name' AND CLIENT_TYPE LIKE 'VENDOR' ";
ECHO '<BR />';
            $landlord_exists1 = json_decode($db_connect->queryFetch($query_2),true);

            print_r($landlord_exists1);

             if($landlord_exists1['data'][0]['CLIENTID']) { 
 
                 $PROPERTY_VENDOR_ID=$landlord_exists1['data'][0]['CLIENTID'];
 
 
             } else { 

                ECHO $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue(); 
                ECHO ' ------ ';
                 $query_2="SELECT CLIENTID FROM cnb_2pa.clients WHERE `CLIENT_NAME` LIKE '$test_name' AND CLIENT_TYPE LIKE 'VENDOR' ";
    ECHO '<BR />';
                $landlord_exists1 = json_decode($db_connect->queryFetch($query_2),true);
    
                print_r($landlord_exists1);

                
                $PROPERTY_VENDOR_ID=1;
                 continue;
    // ECHO '<PRE>';
    //             $PROPERTY_VENDOR_ID=0;

    //        ECHO     $query_3="SELECT DISTINCT(CLIENTID) FROM cnb_2pa.clients WHERE `CLIENT_NAME` LIKE '$test_name' AND CLIENT_TYPE LIKE 'VENDOR' ";

    //             $landlord_exists2 = json_decode($db_connect->queryFetch($query_3),true);
    //             PRINT_R($landlord_exists2);
    //         ECHO    $PROPERTY_VENDOR_ID=$landlord_exists2['data'][0]['CLIENTID'];
     
    //              if($landlord_exists2['data'][0]['CLIENTID']) { 
     
    //                  $PROPERTY_VENDOR_ID=$landlord_exists2['data'][0]['CLIENTID'];
     
     
    //              }



             }
             
        }




  
      $get_instruct_date = Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue(), 'd/m/Y h:i:s');
      if($get_instruct_date) { 
          $INSTRUCTED_DATE="'".$get_instruct_date."'";
      }
      else { 
          $INSTRUCTED_DATE='NULL';
      }        
       
      $sql ="INSERT INTO cnb_2pa.`properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
      `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
      `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
      `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
      `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
      `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
      `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
      `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN-SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
      `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
      `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
      `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
      `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
      `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`,
      `PROPERTY_LETTING_FEE`,`PROPERTY_LETTING_FEE_TYPE`,`PROPERTY_LETTING_FEE_FREQUENCY`,
      `PROPERTY_MANAGEMENT_FEE`,`PROPERTY_MANAGEMENT_FEE_TYPE`,`PROPERTY_MANAGEMENT_FEE_FREQUENCY`, `CERTIFICATE_EXPIRE_DATE`,
      `PROPERTY_LETTING_SERVICE`,`INSTRUCTED_DATE`) 
       VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
      '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE,
      '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
      '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
      '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
      '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
      '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
      '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
      '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
      '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
      '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
      '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
      '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
      '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON', '0','','','',
      '$PROPERTY_LETTING_FEE','$PROPERTY_LETTING_FEE_TYPE','$PROPERTY_LETTING_FEE_FREQUENCY',
      '$PROPERTY_MANAGEMENT_FEE','$PROPERTY_MANAGEMENT_FEE_TYPE','$PROPERTY_MANAGEMENT_FEE_FREQUENCY', '$CERTIFICATE_EXPIRE_DATE',
      '$PROPERTY_LETTING_SERVICE',$INSTRUCTED_DATE)";


            
    $db_connect->queryExecute($sql) or die($sql);

        }
    }
}
?>
