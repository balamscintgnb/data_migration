<?php

require_once '../includes/config.php';
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';

$file_name='D:\Board\247\Commercial sales/Buyers.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

            //logic for data mapping.

            $CLIENTID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();

            $CLIENT_TITLE='';
            $name1=trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
            $name2=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue().' '.$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());

            if($name2!='')
            $CLIENT_NAME=$name1.' & '.$name2;
            else 
            $CLIENT_NAME=$name1;

            $COMPANY_NAME=$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue();
            $CLIENT_TYPE='BUYER';
            $CLIENT_SUB_TYPE='';
            $CLIENT_STATUS='';
            $CLIENT_STAFF_ID='';
            $CLIENT_PRIMARY_EMAIL=trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
            $CLIENT_PRIMARY_PHONE=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue()));
            $CLIENT_ADDRESS_LINE_1=addslashes($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue());
            $CLIENT_ADDRESS_LINE_2=addslashes($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
            $CLIENT_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue();
            $CLIENT_ADDRESS_TOWN=$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue();
            $CLIENT_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue();
            $CLIENT_ADDRESS1_LINE_1='';
            $CLIENT_ADDRESS1_LINE_2='';
            $CLIENT_ADDRESS1_CITY='';
            $CLIENT_ADDRESS1_TOWN='';
            $CLIENT_ADDRESS1_POSTCODE='';
            $CLIENT_ADDRESS2_LINE_1='';
            $CLIENT_ADDRESS2_LINE_2='';
            $CLIENT_ADDRESS2_CITY='';
            $CLIENT_ADDRESS2_TOWN='';
            $CLIENT_ADDRESS2_POSTCODE='';
            $CLIENT_ACCOUNT_NAME='';
            $CLIENT_ACCOUNT_NO='';
            $CLIENT_ACCOUNT_SORTCODE='';
            $CLIENT_EMAIL_1='';
            $CLIENT_EMAIL_2='';
            $CLIENT_EMAIL_3='';
            $CLIENT_EMAIL_4='';
            $CLIENT_EMAIL_5='';
            $CLIENT_PHONE_1='';
            $CLIENT_PHONE_2='';
            $CLIENT_PHONE_3='';
            $CLIENT_PHONE_4='';
            $CLIENT_PHONE_5='';

            $MOBILE_1=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
            $CLIENT_MOBILE_1 = preg_replace('/[^A-Za-z0-9\-]/', '', $MOBILE_1);

            $CLIENT_MOBILE_2='';
            $CLIENT_MOBILE_3='';
            $CLIENT_MOBILE_4='';
            $CLIENT_MOBILE_5='';
            $CLIENT_NOTES=addslashes(str_replace("'",'',$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
            $CLIENT_FAX_1='';
            $CLIENT_FAX_2='';
            $CLIENT_FAX_3='';
            $CLIENT_FAX_4='';
            $CLIENT_FAX_5='';
            $SEARCH_CRITERIA='NULL';
            $CLIENT_CREATED_ON=Utility::convert_tosqldate($objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue(),'d/m/Y H:i:s');
            


            if( ($CLIENT_NAME=='' || $CLIENT_NAME=='Mr') && $COMPANY_NAME!=''){			
                $CLIENT_NAME=$COMPANY_NAME;
            }


       // select email , mobile 
       $query_1="SELECT * FROM cnb_2pa.clients WHERE `CLIENTID`='$CLIENTID'";

       $duplicate_exists = json_decode($db_connect->queryFetch($query_1),true);


       if(empty($duplicate_exists['data'])){
            $sql="INSERT
           INTO
                       cnb_2pa.`clients`(
                   `CLIENTID`,
                   `CLIENT_TITLE`,
                   `CLIENT_NAME`,
                   `CLIENT_TYPE`,
                   `CLIENT_SUB_TYPE`,
                   `CLIENT_STATUS`,
                   `CLIENT_STAFF_ID`,
                   `CLIENT_PRIMARY_EMAIL`,
                   `CLIENT_PRIMARY_PHONE`,
                   `CLIENT_ADDRESS_LINE_1`,
                   `CLIENT_ADDRESS_LINE_2`,
                   `CLIENT_ADDRESS_CITY`,
                   `CLIENT_ADDRESS_TOWN`,
                   `CLIENT_ADDRESS_POSTCODE`,
                   `CLIENT_ADDRESS1_LINE_1`,
                   `CLIENT_ADDRESS1_LINE_2`,
                   `CLIENT_ADDRESS1_CITY`,
                   `CLIENT_ADDRESS1_TOWN`,
                   `CLIENT_ADDRESS1_POSTCODE`,
                   `CLIENT_ADDRESS2_LINE_1`,
                   `CLIENT_ADDRESS2_LINE_2`,
                   `CLIENT_ADDRESS2_CITY`,
                   `CLIENT_ADDRESS2_TOWN`,
                   `CLIENT_ADDRESS2_POSTCODE`,
                   `CLIENT_ACCOUNT_NAME`,
                   `CLIENT_ACCOUNT_NO`,
                   `CLIENT_ACCOUNT_SORTCODE`,
                   `CLIENT_EMAIL_1`,
                   `CLIENT_EMAIL_2`,
                   `CLIENT_EMAIL_3`,
                   `CLIENT_EMAIL_4`,
                   `CLIENT_EMAIL_5`,
                   `CLIENT_PHONE_1`,
                   `CLIENT_PHONE_2`,
                   `CLIENT_PHONE_3`,
                   `CLIENT_PHONE_4`,
                   `CLIENT_PHONE_5`,
                   `CLIENT_MOBILE_1`,
                   `CLIENT_MOBILE_2`,
                   `CLIENT_MOBILE_3`,
                   `CLIENT_MOBILE_4`,
                   `CLIENT_MOBILE_5`,
                   `CLIENT_NOTES`,
                   `CLIENT_FAX_1`,
                   `CLIENT_FAX_2`,
                   `CLIENT_FAX_3`,
                   `CLIENT_FAX_4`,
                   `CLIENT_FAX_5`,
                   `CLIENT_CREATED_ON`,
                   `SEARCH_CRITERIA`
               )
           VALUES(
               '$CLIENTID',
               '$CLIENT_TITLE',
               '$CLIENT_NAME',
               '$CLIENT_TYPE',
               '$CLIENT_SUB_TYPE',
               '$CLIENT_STATUS',
               '$CLIENT_STAFF_ID',
               '$CLIENT_PRIMARY_EMAIL',
               '$CLIENT_PRIMARY_PHONE',
               '$CLIENT_ADDRESS_LINE_1',
               '$CLIENT_ADDRESS_LINE_2',
               '$CLIENT_ADDRESS_CITY',
               '$CLIENT_ADDRESS_TOWN',
               '$CLIENT_ADDRESS_POSTCODE',
               '$CLIENT_ADDRESS1_LINE_1',
               '$CLIENT_ADDRESS1_LINE_2',
               '$CLIENT_ADDRESS1_CITY',
               '$CLIENT_ADDRESS1_TOWN',
               '$CLIENT_ADDRESS1_POSTCODE',
               '$CLIENT_ADDRESS2_LINE_1',
               '$CLIENT_ADDRESS2_LINE_2',
               '$CLIENT_ADDRESS2_CITY',
               '$CLIENT_ADDRESS2_TOWN',
               '$CLIENT_ADDRESS2_POSTCODE',
               '$CLIENT_ACCOUNT_NAME',
               '$CLIENT_ACCOUNT_NO',
               '$CLIENT_ACCOUNT_SORTCODE',
               '$CLIENT_EMAIL_1',
               '$CLIENT_EMAIL_2',
               '$CLIENT_EMAIL_3',
               '$CLIENT_EMAIL_4',
               '$CLIENT_EMAIL_5',
               '$CLIENT_PHONE_1',
               '$CLIENT_PHONE_2',
               '$CLIENT_PHONE_3',
               '$CLIENT_PHONE_4',
               '$CLIENT_PHONE_5',
               '$CLIENT_MOBILE_1',
               '$CLIENT_MOBILE_2',
               '$CLIENT_MOBILE_3',
               '$CLIENT_MOBILE_4',
               '$CLIENT_MOBILE_5',
               '$CLIENT_NOTES',
               '$CLIENT_FAX_1',
               '$CLIENT_FAX_2',
               '$CLIENT_FAX_3',
               '$CLIENT_FAX_4',
               '$CLIENT_FAX_5',
               '$CLIENT_CREATED_ON',
               NUll
           )";
        
            $db_connect->queryExecute($sql) ;
          

       }

    

        }
    }
}

?>