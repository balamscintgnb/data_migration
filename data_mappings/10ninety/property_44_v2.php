<?php
require_once '../../header_init.php';
require_once '../../plugins/sql_conversion/Classes/PHPExcel/IOFactory.php';
$file_name='../source_data/fairdeal/Properties (2).xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();


$truncate = "DELETE FROM cnb_fairdeal.`properties`";
$db_connect->queryExecute($truncate);

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
        if($row>1){

        $PROPERTY_DESCRIPTION='';
        $Maintenance_charge='';
        $Ground_rent='';
        $PROPERTY_ACCETABLE_PRICE='';

        $Enter_PROPERTY_ID=$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
        $pro_id=str_pad($Enter_PROPERTY_ID, 4, "0", STR_PAD_LEFT);
        $PROPERTY_ID = 'FAIR_'.$pro_id;

        $PROPERTY_REF_ID=$objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
        $PROPERTY_VENDOR=$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue();

            $query_1="SELECT CLIENTID  FROM cnb_fairdeal.`clients` WHERE `CLIENT_NAME` LIKE '$PROPERTY_VENDOR' AND `CLIENT_TYPE` LIKE 'landlord'";

            $landlord = json_decode($db_connect->queryFetch($query_1),true);

            $PROPERTY_VENDOR_ID=$landlord['data'][0]['CLIENTID'];


        $PROPERTY_STAFF_ID=$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue();
        $PROPERTY_TITLE=$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
        $PROPERTY_SHORT_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue();
        $PROPERTY_DESCRIPTION=$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue();
        $PROPERTY_CATEGORY='RESIDENTIAL LETTINGS';
        $PROPERTY_PRICE=$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue();
        $PROPERTY_PRICE_FREQUENCY=$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue();
        $PROPERTY_QUALIFIER='';
        $given_date=trim($objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue());

        if($given_date!=''){
        $test_date = explode(' ',$given_date);
        $test_date1 = explode('/',$test_date[0]);
        $PROPERTY_AVAILABLE_DATE="'".$test_date1[2].'-'.$test_date1[1].'-'.$test_date1[0]."'";
        }
        if($given_date==''){
            $PROPERTY_AVAILABLE_DATE="NULL";
        }
           
        $PROPERTY_ADDRESS_LINE_1=$objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue();
        $PROPERTY_ADDRESS_LINE_2=$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue();
        $PROPERTY_ADDRESS_CITY=$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
        $PROPERTY_ADDRESS_COUNTY=$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
        $PROPERTY_ADDRESS_POSTCODE=$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue();
        $PROPERTY_FORMATTED_ADDRESS=$objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue();
        $PROPERTY_STATUS='';
        $PROPERTY_AVAILABILITY=$objPHPExcel->getActiveSheet()->getCell('AW'.$row)->getValue();
        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TYPE=$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue();

        $PROPERTY_BEDROOMS=$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue();
        $PROPERTY_BATHROOMS=$objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue();
        $PROPERTY_RECEPTION=$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue();
        $PROPERTY_TENURE='';
        $Lease_term_years='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        $GARDEN='';
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        $FURNISHED='';
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
        $PETS_ALLOWED='';
        $FAMILY_OR_CHILD_FRIENDLY='';
        $DSS_ALLOWED='';
        $SMOKING_ALLOWED='';
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';

        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        $CERTIFICATE_EXPIRE_DATE='';
        $PROPERTY_EPC_VALUES=$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue();
        $PROPERTY_CREATED_ON='';


        ////////////////////////EXPIRE DATE ///////////////////
        $Gas_certificate_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue();
        $PAT_test_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue();
        $Electricity_certificate_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue();
        $EPC_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue();
        $Insurance_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue();
        $Legionella_risk_assessment_date=$objPHPExcel->getActiveSheet()->getCell('BJ'.$row)->getValue();
        $Smoke_CO_alarm_expiry_date=$objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue();

        $CERTIFICATE_DATE = array('1'=>$Gas_certificate_expiry_date,
            '2'=>$PAT_test_expiry_date, 
            '3'=>$Electricity_certificate_expiry_date,
            '4'=>$EPC_expiry_date, 
            '5'=>$Insurance_expiry_date,
            '6'=>$Legionella_risk_assessment_date, 
            '7'=>$Smoke_CO_alarm_expiry_date);

            $CERTIFICATE_EXPIRE_DATE = json_encode($CERTIFICATE_DATE);

            ///////////////////// END ////////////////

        /////////////////////////////ADD MORE FIELDS/////////////////////////

        $read_Has_Gas=$objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue();
        $Has_Gas=array();
        $area_squre_feet[]="Has Gas is : ".$Has_Gas;
        $PROPERTY_CUSTOM_FEATURES=json_encode($Has_Gas);


      
        $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION);
        $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);
        $PROPERTY_FORMATTED_ADDRESS=addslashes($PROPERTY_FORMATTED_ADDRESS);
        $PROPERTY_ADDRESS_LINE_1=addslashes($PROPERTY_ADDRESS_LINE_1);
        $PROPERTY_ADDRESS_LINE_2=addslashes($PROPERTY_ADDRESS_LINE_2);

        $PROPERTY_ACCETABLE_PRICE=$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue();        

        if($PROPERTY_ACCETABLE_PRICE!=''){
            $PROPERTY_DESCRIPTION.='\n Minimum accetable price: '.$PROPERTY_ACCETABLE_PRICE;
        }


        $Ground_rent=$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue();        
        if($Ground_rent!=''){
            $PROPERTY_DESCRIPTION.='\n Ground rent: '.$Ground_rent;
        }


        $Maintenance_charge=$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue();
        if($Maintenance_charge!=''){
            $PROPERTY_ADMIN_FEES.='Maintenance charge: '.$Maintenance_charge;
        }

        $Additional_Information=$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue();        
        if($Additional_Information!=''){
            $PROPERTY_DESCRIPTION.='\n AdditionalInformation is : '.$Additional_Information;
        }
        $Additional_Information=addslashes($Additional_Information);
        $PROPERTY_DESCRIPTION=Utility::format_content(addslashes($PROPERTY_DESCRIPTION));
        $PROPERTY_SHORT_DESCRIPTION=Utility::format_content(addslashes($PROPERTY_SHORT_DESCRIPTION));
        $PROPERTY_FORMATTED_ADDRESS=Utility::format_content(addslashes($PROPERTY_FORMATTED_ADDRESS));
        $PROPERTY_ADDRESS_LINE_1=Utility::format_content(addslashes($PROPERTY_ADDRESS_LINE_1));



         $sql ="INSERT INTO cnb_fairdeal.`properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
        `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
        `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
        `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`, `Lease_term_years`, `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
        `KITCHEN-DINER`, `OFF-ROAD_PARKING`, `ON-ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
        `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
        `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
        `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
        `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
        `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
        `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
        `CERTIFICATE_EXPIRE_DATE`, `PROPERTY_EPC_VALUES`, `PROPERTY_CREATED_ON`, `PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`) 
         VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
        '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', $PROPERTY_AVAILABLE_DATE ,
        '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
        '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
        '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE','$Lease_term_years', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$KITCHEN_DINER',
        '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN', '$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM',
        '$FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER', '$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY',
        '$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE',
        '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES', '$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
        '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
        '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
        '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
        '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
        '$PROPERTY_IMAGE_EPC_5', '$CERTIFICATE_EXPIRE_DATE', '$PROPERTY_EPC_VALUES', '$PROPERTY_CREATED_ON','$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','$PROPERTY_ASSETS')";

         echo "<br/><br/>";
         
    
    //insert into table    
    $db_connect->queryExecute($sql) or die($sql);

    }
}
}
?>