<?php
require_once '../includes/config.php';
require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = "select * from rental_receipts";

$data = $db_connect_src->queryFetch($query);

$data = json_decode($data, true);

if(count($data)>0){

	foreach($data['data'] as $row){

		$LETTING_CUSTOM_REF_NO=	 $row['tenancy_reference'];
		$PROPERTY_ID			=0;
		$PROPERTY_ROOM_ID			= 0;
		$TENANT_ID			=0;
		$SHARED_TENANT_IDS=json_encode(array());
		$DUE_DATE=$row['date_due'];
		$RENT_START = NULL;
		$RENT_END = NULL;
		$PAYMENT_DATE=$row['date_due'];

		$DUE_AMOUNT=null;
		$PAID_AMOUNT=$row['receipt_amount'];
		$TRANSACTION_STATUS='PAID';
		$TRANSACTION_NO='';
		$TRANSACTION_ID=0;

		if($row['receipt_no'] != ''){
			$TRANSACTION_NO=$row['receipt_no'];
		}

		$payment_method_src = $row['payment_method'];

		$PAYMENT_METHOD='CASH';
			
		if(stristr($payment_method_src, 'bank')){
			$PAYMENT_METHOD='BANK';
		}/*else if(stristr($payment_method_src, 'credit')){
			$PAYMENT_METHOD='CASH';
		}else if(stristr($payment_method_src, 'charge')){
			$PAYMENT_METHOD='CASH';
		}*/

		$TRANSACTION_TYPE='RENT'; 

		$TRANSACTION_DESCRIPTION='';
		$TRANSACTION_NOTES = '';
	 	$sql_query = "insert into tenant_transactions VALUES (NULL,'$TRANSACTION_ID','$LETTING_CUSTOM_REF_NO','$PROPERTY_ID','$PROPERTY_ROOM_ID',
		'$TENANT_ID','$SHARED_TENANT_IDS','$RENT_START','$RENT_END', '$DUE_DATE','$PAYMENT_DATE',
		'$DUE_AMOUNT','$PAID_AMOUNT','$TRANSACTION_STATUS','$TRANSACTION_TYPE','$TRANSACTION_NO','$PAYMENT_METHOD','$TRANSACTION_DESCRIPTION','$TRANSACTION_NOTES',0);";

		$db_connect->queryExecute($sql_query) or die($sql_query);
	}
}