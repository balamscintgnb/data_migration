<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

$data_mapping = true;

//require_once 'includes/config.php';
require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';

$file_name='../source_data/common/sls/properties.xls';

$thisProceed=true;

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file_name, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

$inserted = $failed = 0;

if($thisProceed){

    for($row =2; $row <= $total_rows; $row++){
    	
        //NOTE: changing columns according to data import requiremetns sheet
        
        //todo escape, encode, sanitize

        //logic for data mapping.
        $PROPERTY_STAFF_ID='';
        $PROPERTY_ADMIN_FEES='';
        $PROPERTY_TENURE='';
        $PROPERTY_CLASSIFICATION='';
        $PROPERTY_CURRENT_OCCUPANT='';
        $KITCHEN_DINER='';
        $OFF_ROAD_PARKING='';
        $ON_ROAD_PARKING='';
        
        $KITCHEN_DINER = '';
        $PROPERTY_CREATED_ON = date('Y-m-d');
        
        $WHEELCHAIR_ACCESS='';
        $ELEVATOR_IN_BUILDING='';
        $POOL='';
        $GYM='';
        $KITCHEN='';
        $DINING_ROOM='';
        
        $INTERNET='';
        $WIRELESS_INTERNET='';
        $TV='';

        $WASHER='';
        $DRYER='';
        $DISHWASHER='';
       
        $FAMILY_OR_CHILD_FRIENDLY='';
        
        $SECURITY='';
        $HOT_TUB='';

        $CLEANER='';
        $EN_SUITE='';
        $SECURE_CAR_PARKING='';
        $OPEN_PLAN_LOUNGE='';
        $VIDEO_DOOR_ENTRY='';
        $CONCIERGE_SERVICES='';
        
        $PROPERTY_SOURCE = 'manual_sls';
        
        $PROPERTY_CUSTOM_FEATURES='';
        $PROPERTY_ROOMS='';
        $PROPERTY_ASSETS='';

        $PROPERTY_IMAGE_1='';
        $PROPERTY_IMAGE_2='';
        $PROPERTY_IMAGE_3='';
        $PROPERTY_IMAGE_4='';
        $PROPERTY_IMAGE_5='';
        $PROPERTY_IMAGE_6='';
        $PROPERTY_IMAGE_7='';
        $PROPERTY_IMAGE_8='';
        $PROPERTY_IMAGE_9='';
        $PROPERTY_IMAGE_10='';
        $PROPERTY_IMAGE_11='';
        $PROPERTY_IMAGE_12='';
        $PROPERTY_IMAGE_13='';
        $PROPERTY_IMAGE_14='';
        $PROPERTY_IMAGE_15='';
        
        $PROPERTY_IMAGE_FLOOR_1='';
        $PROPERTY_IMAGE_FLOOR_2='';
        $PROPERTY_IMAGE_FLOOR_3='';
        $PROPERTY_IMAGE_FLOOR_4='';
        $PROPERTY_IMAGE_FLOOR_5='';
        
        $PROPERTY_IMAGE_EPC_1='';
        $PROPERTY_IMAGE_EPC_2='';
        $PROPERTY_IMAGE_EPC_3='';
        $PROPERTY_IMAGE_EPC_4='';
        $PROPERTY_IMAGE_EPC_5='';
        
        $PROPERTY_EPC_VALUES='';
        $PROPERTY_QUALIFIER='';

        $PROPERTY_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
        $PROPERTY_REF_ID=trim($objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue());
        $PROPERTY_CATEGORY=trim($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
        
        $PROPERTY_TITLE=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue()));
        $PROPERTY_SHORT_DESCRIPTION=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue()));
        $PROPERTY_DESCRIPTION=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('E'.$row)->getValue()));
        
        $PROPERTY_FEATURES=trim($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
        
        $PROPERTY_AVAILABILITY_STATUS=trim($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
        $PROPERTY_STATUS=trim($objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue());
        
        $PROPERTY_TYPE=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue()));
        $PROPERTY_FURNISHED=trim($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
        
        $PROPERTY_BEDROOMS=trim($objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue());
        $PROPERTY_BATHROOMS=trim($objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue());
        $PROPERTY_RECEPTION=trim($objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue());
        
        $PROPERTY_ADDRESS_LINE_1=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue()));
        $PROPERTY_ADDRESS_LINE_2=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue()));
        $PROPERTY_ADDRESS_CITY=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue()));
        $PROPERTY_ADDRESS_COUNTY=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue()));
        $PROPERTY_ADDRESS_POSTCODE=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue()));
        $PROPERTY_FORMATTED_ADDRESS=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue()));
        
        //for SLS
        $PROPERTY_FORMATTED_ADDRESS = '';
        
        $PROPERTY_PRICE=trim($objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue());
        $PROPERTY_PRICE_FREQUENCY=Utility::format_content(trim($objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue()));
        
        $PROPERTY_ADMIN_FEES=trim($objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue());
        
        $PROPERTY_VENDOR_ID=trim($objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue());
        
        $PROPERTY_TENURE = trim($objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue());
        
        //convert_tosqldate
        
        $GARDEN=trim($objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue());
        
        $SMOKING_ALLOWED=trim($objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue());
        
        $DSS_ALLOWED=trim($objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue());
        
		$PETS_ALLOWED=trim($objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue());
        
        $get_available_from = trim($objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue());
        
        if(stristr($get_available_from, 'vacant')){
        	$PROPERTY_AVAILABLE_DATE = date('Y-m-d');
        }else{
        	$PROPERTY_AVAILABLE_DATE = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($get_available_from));
        	
        	if(!$PROPERTY_AVAILABLE_DATE){
        		$PROPERTY_AVAILABLE_DATE = Utility::convert_tosqldate($PROPERTY_AVAILABLE_DATE, 'Y-m-d');
        	}
        }
        
        if(!isset($PROPERTY_STATUS) || $PROPERTY_STATUS==null || $PROPERTY_STATUS==''){
        	$PROPERTY_STATUS='INACTIVE';	
        }
        
        if(stristr($PROPERTY_PRICE_FREQUENCY, 'P.A')){
        	$PROPERTY_PRICE_FREQUENCY='pa';
        }
        
        $PROPERTY_DESCRIPTION=addslashes($PROPERTY_DESCRIPTION.'<br/><br/><b>Price:</b>'.$PROPERTY_PRICE.'<br/><br/><b>Property Type:</b>'.$PROPERTY_TYPE);
        $PROPERTY_SHORT_DESCRIPTION=addslashes($PROPERTY_SHORT_DESCRIPTION);
        
        if($PROPERTY_ID!=''){
				
	         $sql ="INSERT INTO `properties` (`PROPERTY_ID`, `PROPERTY_REF_ID`, `PROPERTY_VENDOR_ID`, `PROPERTY_STAFF_ID`, `PROPERTY_TITLE`,
	        `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_QUALIFIER`, 
	        `PROPERTY_AVAILABLE_DATE`, `PROPERTY_ADDRESS_LINE_1`, `PROPERTY_ADDRESS_LINE_2`, `PROPERTY_ADDRESS_CITY`, `PROPERTY_ADDRESS_COUNTY`, 
	        `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_ADMIN_FEES`, `PROPERTY_TYPE`, 
	        `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `PROPERTY_RECEPTION`, `PROPERTY_TENURE`,  `PROPERTY_CLASSIFICATION`, `PROPERTY_CURRENT_OCCUPANT`, 
	         `OFF_ROAD_PARKING`, `ON_ROAD_PARKING`, `GARDEN`, `WHEELCHAIR_ACCESS`, `ELEVATOR_IN_BUILDING`, `POOL`, `GYM`, `KITCHEN`, 
	        `DINING_ROOM`, `FURNISHED`, `INTERNET`, `WIRELESS_INTERNET`, `TV`, `WASHER`, `DRYER`, `DISHWASHER`, `PETS_ALLOWED`, `FAMILY_OR_CHILD_FRIENDLY`, 
	        `DSS_ALLOWED`, `SMOKING_ALLOWED`, `24_7_SECURITY`, `HOT_TUB`, `CLEANER`, `EN_SUITE`, `SECURE_CAR_PARKING`, `OPEN_PLAN_LOUNGE`, `VIDEO_DOOR_ENTRY`,
	        `CONCIERGE_SERVICES`, `PROPERTY_IMAGE_1`, `PROPERTY_IMAGE_2`, `PROPERTY_IMAGE_3`, `PROPERTY_IMAGE_4`, `PROPERTY_IMAGE_5`, `PROPERTY_IMAGE_6`,
	        `PROPERTY_IMAGE_7`, `PROPERTY_IMAGE_8`, `PROPERTY_IMAGE_9`, `PROPERTY_IMAGE_10`, `PROPERTY_IMAGE_11`, `PROPERTY_IMAGE_12`, `PROPERTY_IMAGE_13`,
	        `PROPERTY_IMAGE_14`, `PROPERTY_IMAGE_15`, `PROPERTY_IMAGE_FLOOR_1`, `PROPERTY_IMAGE_FLOOR_2`, `PROPERTY_IMAGE_FLOOR_3`, `PROPERTY_IMAGE_FLOOR_4`,
	        `PROPERTY_IMAGE_FLOOR_5`, `PROPERTY_IMAGE_EPC_1`, `PROPERTY_IMAGE_EPC_2`, `PROPERTY_IMAGE_EPC_3`, `PROPERTY_IMAGE_EPC_4`, `PROPERTY_IMAGE_EPC_5`, 
	        `PROPERTY_EPC_VALUES`,  `RECORD_UPLOADED`,`PROPERTY_CUSTOM_FEATURES`,`PROPERTY_ROOMS`,`PROPERTY_ASSETS`, `PROPERTY_SOURCE`, `KITCHEN-DINER`, `PROPERTY_CREATED_ON`) 
	         VALUES ('$PROPERTY_ID', '$PROPERTY_REF_ID', '$PROPERTY_VENDOR_ID', '$PROPERTY_STAFF_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION',
	        '$PROPERTY_DESCRIPTION', '$PROPERTY_CATEGORY', '$PROPERTY_PRICE', '$PROPERTY_PRICE_FREQUENCY', '$PROPERTY_QUALIFIER', '$PROPERTY_AVAILABLE_DATE',
	        '$PROPERTY_ADDRESS_LINE_1', '$PROPERTY_ADDRESS_LINE_2', '$PROPERTY_ADDRESS_CITY', '$PROPERTY_ADDRESS_COUNTY', '$PROPERTY_ADDRESS_POSTCODE',
	        '$PROPERTY_FORMATTED_ADDRESS', '$PROPERTY_STATUS', '$PROPERTY_AVAILABILITY_STATUS', '$PROPERTY_ADMIN_FEES', '$PROPERTY_TYPE', '$PROPERTY_BEDROOMS',
	        '$PROPERTY_BATHROOMS', '$PROPERTY_RECEPTION', '$PROPERTY_TENURE', '$PROPERTY_CLASSIFICATION', '$PROPERTY_CURRENT_OCCUPANT', '$OFF_ROAD_PARKING', '$ON_ROAD_PARKING', '$GARDEN' ,'$WHEELCHAIR_ACCESS', '$ELEVATOR_IN_BUILDING', '$POOL', '$GYM', '$KITCHEN', '$DINING_ROOM','$PROPERTY_FURNISHED', '$INTERNET', '$WIRELESS_INTERNET', '$TV', '$WASHER', '$DRYER','$DISHWASHER', '$PETS_ALLOWED', '$FAMILY_OR_CHILD_FRIENDLY','$DSS_ALLOWED', '$SMOKING_ALLOWED', '$SECURITY', '$HOT_TUB', '$CLEANER', '$EN_SUITE', '$SECURE_CAR_PARKING', '$OPEN_PLAN_LOUNGE', '$VIDEO_DOOR_ENTRY', '$CONCIERGE_SERVICES','$PROPERTY_IMAGE_1', '$PROPERTY_IMAGE_2', '$PROPERTY_IMAGE_3', '$PROPERTY_IMAGE_4',
	        '$PROPERTY_IMAGE_5', '$PROPERTY_IMAGE_6', '$PROPERTY_IMAGE_7', '$PROPERTY_IMAGE_8', '$PROPERTY_IMAGE_9', '$PROPERTY_IMAGE_10', 
	        '$PROPERTY_IMAGE_11', '$PROPERTY_IMAGE_12', '$PROPERTY_IMAGE_13', '$PROPERTY_IMAGE_14', '$PROPERTY_IMAGE_15', 
	        '$PROPERTY_IMAGE_FLOOR_1', '$PROPERTY_IMAGE_FLOOR_2', '$PROPERTY_IMAGE_FLOOR_3', '$PROPERTY_IMAGE_FLOOR_4',
	        '$PROPERTY_IMAGE_FLOOR_5', '$PROPERTY_IMAGE_EPC_1', '$PROPERTY_IMAGE_EPC_2', '$PROPERTY_IMAGE_EPC_3', '$PROPERTY_IMAGE_EPC_4',
	        '$PROPERTY_IMAGE_EPC_5', '$PROPERTY_EPC_VALUES',  '0', '$PROPERTY_CUSTOM_FEATURES','$PROPERTY_ROOMS','$PROPERTY_ASSETS', '$PROPERTY_SOURCE', '$KITCHEN_DINER', '$PROPERTY_CREATED_ON')";
	
			$db_connect->queryExecute($sql);
			
			$inserted++;
        }else{
        	$failed++;
        }
	}
	
	echo "Done $inserted, Failed $failed.";
}
?>