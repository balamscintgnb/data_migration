<?php 
require_once '../../header_init.php';

//require_once '../includes/config.php';
//require_once '../../header_init.php';
require_once '../plugins/PHPExcel/IOFactory.php';
//require_once 'includes/classes/DocXConversion.php';

$thisProceed=true;

$data_folder = '../../data/inso/';

$log="";

$doc_file_type = array('doc', 'docx');

$file_name='../../data/inso/instant_soluation.xls';

try {
	//Load the excel(.xls/.xlsx) file
	$objPHPExcel = PHPExcel_IOFactory::load($file_name);
} catch (Exception $e) {
    $thisProceed=false;
	die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
}

//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
$sheet = $objPHPExcel->getSheet(0);

//It returns the highest number of rows
$total_rows = $sheet->getHighestRow();

//It returns the highest number of columns
//$highest_column = $sheet->getHighestColumn();

if($thisProceed){

    for($row =1; $row <= $total_rows; $row++){
    	
        if($row>1){
        	
    		$PROPERTY_ID = $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
    		
    		$PROPERTY_ID = str_replace(' ', '_', $PROPERTY_ID);
    		
    		$PROPERTY_ADDRESS_1 = Utility::format_content($objPHPExcel->getActiveSheet()->getCell('B'.$row)->getValue());
    		
    		$PROPERTY_ADDRESS = explode('(', $PROPERTY_ADDRESS_1);
    		
    		$PROPERTY_POST_CODE = str_replace(')', '', $PROPERTY_ADDRESS[1]);
    		
    		$PROPERTY_TITLE = Utility::format_content($objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue());
    		
    		$PROPERTY_RENT = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
    		
    		$PROPERTY_SHORT_DESCRIPTION = Utility::format_content($objPHPExcel->getActiveSheet()->getCell('F'.$row)->getValue());
    		
    		$PROPERTY_DESCRIPTION = Utility::format_content($objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue());
    		
    		$PROPERTY_TYPE = $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue();
    		
    		$PROPERTY_FURNISHED_TYPE = $objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue();
    		
    		$PROPERTY_CUSTOM_FEATURE = Utility::format_content($objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue());
    		
    		$PROPERTY_CUSTOM_FEATURE = json_encode(explode(',', $PROPERTY_CUSTOM_FEATURE));
    		
    		$PROPERTY_FEE = $objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue();
    		
    		$PROPERTY_BEDROOM = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
    		
    		$PROPERTY_BATHROOM = $objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue();
    		
    		$PROPERTY_DESCRIPTION .= Utility::format_content($objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue());
    		
    		//
    		$sql = "INSERT INTO properties (`PROPERTY_ID`, `PROPERTY_TITLE`, `PROPERTY_SHORT_DESCRIPTION`, `PROPERTY_DESCRIPTION`, `PROPERTY_CATEGORY`, `PROPERTY_PRICE`, `PROPERTY_PRICE_FREQUENCY`, `PROPERTY_ADDRESS_LINE_1`,  `PROPERTY_ADDRESS_POSTCODE`, `PROPERTY_FORMATTED_ADDRESS`, `PROPERTY_STATUS`, `PROPERTY_AVAILABILITY`, `PROPERTY_TYPE`, `PROPERTY_BEDROOMS`, `PROPERTY_BATHROOMS`, `FURNISHED`, `PROPERTY_CUSTOM_FEATURES`, `PROPERTY_ADMIN_FEES`) VALUES ('$PROPERTY_ID', '$PROPERTY_TITLE', '$PROPERTY_SHORT_DESCRIPTION', '$PROPERTY_DESCRIPTION', 'RESIDENTIAL LETTINGS', '$PROPERTY_RENT', 'PCM', '$PROPERTY_ADDRESS_1', '$PROPERTY_POST_CODE', '$PROPERTY_ADDRESS_1', 'INACTIVE', 'Let', '$PROPERTY_TYPE', '$PROPERTY_BEDROOM', '$PROPERTY_BATHROOM', '$PROPERTY_FURNISHED_TYPE', '$PROPERTY_CUSTOM_FEATURE', '$PROPERTY_FEE')";

      		$db_connect->queryExecute($sql);
          	//
          	
          	//break;
        }
    }
}

echo 'Done';

exit;

if($thisProceed){
 
	if (is_dir($data_folder)){
		
		$log.=date('d-m-Y H:i:s').': -'.$data_folder."- Directory Valid\n";

		if ($dh = opendir($data_folder)){
			
			$log.=date('d-m-Y H:i:s').': -'.$data_folder."- Directory Accessed\n";
		    
			while (($file = readdir($dh)) !== false) {
				
				$sub_folder = $data_folder.'/'.$file;
				
				$log.=date('d-m-Y H:i:s').': '." -$sub_folder- "."Directory Valid\n";
			    
				if(is_dir($sub_folder) && file_exists($sub_folder) && $file!='images' && $file!='.' && $file!='..'){
					
					//
					if ($dh1 = opendir($sub_folder)){
			
						$log.=date('d-m-Y H:i:s').': '." -$sub_folder- "."Directory Accessed\n";
					    
						while (($file1 = readdir($dh1)) !== false) {
							
							$property_file = $sub_folder.'/'.$file1;
							
							if(!is_dir($property_file) && file_exists($property_file) && $file1!='.' && $file1!='..'){
								echo $property_file.'<br/>';
								
								$path_info = pathinfo($property_file);
								
								$ext_lower = strtolower($path_info['extension']);
								
								if(in_array($ext_lower,$doc_file_type)){
									
									$docObj = new RD_Text_Extraction();
									//$docObj = new Doc2Txt("test.doc");
									
									$txt = $docObj->convert_to_text($property_file);
									var_dump($txt);
								}
							}
							
							//$log.=date('d-m-Y H:i:s').': '." -$file1- "."Directory Accessed\n";
						}
					}
					//
					
				}
				break;
			}
			closedir($dh);
		}
	}
	
	//echo str_replace("\n", '<br/>', $log);
}
?>