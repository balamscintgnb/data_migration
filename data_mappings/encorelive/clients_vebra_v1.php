<?php 
error_reporting(E_ALL);
ini_set('display_errors',1);
include 'datamapping_config.php';
$_database1 = 'cnb_jennings';
$_database2 = 'jennings_vebra_sales';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);



$user_query = $db2->execute("SELECT * FROM `en_user` WHERE `email` != ''  ;");
$admin_users = array();
while($user = $user_query->fetch_array()){
	$admin_users[$user['userid']] = $user['email'];
}

if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*1000;
}
else{
	$p=0;
	$page = 0;
}

$query = $db2->execute("SELECT * FROM `en_client` WHERE 1 LIMIT $page, 1000");
if($query->num_rows>0){
while($row = $query->fetch_array()){
	$CLIENT_CREATED_ON 		= $row['dateentered'];
	$CLIENTID				= $row['clientid'];
	$CLIENT_TITLE			= $db2->escape($row['title']);
	$CLIENT_NAME			= $row['forename'].' '.$row['surname'];
	$CLIENT_NAME			= $db2->escape($CLIENT_NAME);

	$CLIENT_STAFF_ID		= '';

	$CLIENT_TYPE			= '';
	$CLIENT_SUB_TYPE		= '';

	if($row['recordtype']==0){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==80){
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==81){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'BUILDER';
	}
	else if($row['recordtype']==84){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'DEVELOPER';
	}
	else if($row['recordtype']==85){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'DEALER';
	}
	else if($row['recordtype']==93){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'CANVASSING';
	}
	else if($row['recordtype']==303){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'NEW HOME DEVELOPER';
	}
	else if($row['recordtype']==937){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'INVESTOR';
	}
	else if($row['recordtype']==938){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= 'POTENTIAL VENDOR';
	}
	else if($row['recordtype']==939){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'AUCTION';
	}
	else if($row['recordtype']==940){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==943){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'EXTERNAL SALE';
	}
	else if($row['recordtype']==944){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= 'MARKET APPRAISAL';
	}
	else if($row['recordtype']==1073){ //86 , 88, 87
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'POTENTIAL LANDLORD';
	}
	else if($row['recordtype']==86){ //86 , 88, 87
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'SUB AGENTS';
	}
	else if($row['recordtype']==87){ //86 , 88, 87
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'RETAINED';
	}
	else if($row['recordtype']==88){ //86 , 88, 87
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= 'RETAINED - OTHER AGENTS';
	}

	else if($row['recordtype']==1228){
		$CLIENT_TYPE			= 'VENDOR';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1229){
		$CLIENT_TYPE			= 'LANDLORD';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1241){ //323
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= 'CLIENT';
	}
	else if($row['recordtype']==323){ //
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= 'INTERNET REGISTERED';
	}
	else if($row['recordtype']==1251){
		$CLIENT_TYPE			= 'APPLICANT';
		$CLIENT_SUB_TYPE		= 'PREVIOUS CLIENT';
	}
	else if($row['recordtype']==1444){
		$CLIENT_TYPE			= 'TENANT';
		$CLIENT_SUB_TYPE		= '';
	}
	else if($row['recordtype']==1529){
		$CLIENT_TYPE			= 'ADDRESSBOOK';
		$CLIENT_SUB_TYPE		= 'PROBATE';
	}


	$CLIENT_STATUS = 'ACTIVE';
	if($row['sys_active']==1){
		$CLIENT_STATUS = 'INACTIVE';
	}

	$email1					= trim($row['email1']);


	$CLIENT_NOTES			= '';
	$CLIENT_PRIMARY_PHONE	= $db2->escape($row['tel1']);
	$CLIENT_PRIMARY_EMAIL ='';
	if($db2->validate_email($email1)){
		$CLIENT_PRIMARY_EMAIL	= $db2->escape($email1);
	}
	else{
		$CLIENT_NOTES			= $db2->escape($email1).' ';
	}

	$CLIENT_EMAIL_1			= $db2->escape($row['email2']);
	$CLIENT_EMAIL_2			= '';
	$CLIENT_EMAIL_3			= '';
	$CLIENT_EMAIL_4			= '';
	$CLIENT_EMAIL_5			= '';

	$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';

	$tel2_type 				= $row['teltype2'];
	$tel3_type 				= $row['teltype3'];
	$tel4_type 				= $row['teltype4'];

	$tel2_val 				= $db2->escape($row['tel2']);
	$tel3_val 				= $db2->escape($row['tel3']);
	$tel4_val 				= $db2->escape($row['tel4']);

	if($tel2_type==96 || $tel2_type==98 || $tel2_type==100 || $tel2_type==102 || $tel2_type==1359){
		$phone_2 = $tel2_val;
	}

	if($tel2_type==103 || $tel2_type==104 ){
		$mobile_2 = $tel2_val;
	}

	if($tel2_type==105 || $tel2_type==106 ){
		$fax_2 = $tel2_val;
	}


	if($tel3_type==96 || $tel3_type==98 || $tel3_type==100 || $tel3_type==102 || $tel3_type==1359){
		$phone_3 = $tel3_val;
	}

	if($tel3_type==103 || $tel3_type==104 ){
		$mobile_3 = $tel3_val;
	}

	if($tel3_type==105 || $tel3_type==106 ){
		$fax_3 = $tel3_val;
	}

	if($tel4_type==96 || $tel4_type==98 || $tel4_type==100 || $tel4_type==102 || $tel4_type==1359){
		$phone_4 = $tel4_val;
	}

	if($tel4_type==103 || $tel4_type==104 ){
		$mobile_4 = $tel4_val;
	}

	if($tel4_type==105 || $tel4_type==106 ){
		$fax_4 = $tel4_val;
	}

	$CLIENT_ADDRESS_LINE_1 	= '';
	$CLIENT_ADDRESS_LINE_2	= '';
	$CLIENT_ADDRESS_CITY	= '';
	$CLIENT_ADDRESS_TOWN	= '';
	$CLIENT_ADDRESS_POSTCODE= '';

	$housename			= trim($row['Housename']);
	$housenumber		= trim($row['Housenumber']);
	$street				= trim($row['Street']);
	$locality			= trim($row['Locality']);
	$area				= trim($row['Area']);
	$town				= trim($row['Town']);
	$county				= trim($row['County']);
	$postcode			= trim($row['postcode']);

	if($housename!=''){
		$CLIENT_ADDRESS_LINE_1.=$housename;
	}

	if($housenumber!=''){
		if($CLIENT_ADDRESS_LINE_1!=""){
			$CLIENT_ADDRESS_LINE_1.=', ';
		}
		$CLIENT_ADDRESS_LINE_1.=$housenumber;
	}

	if($street!=''){
		$CLIENT_ADDRESS_LINE_2.=$street;
	}

	if($locality!=''){
		$CLIENT_ADDRESS_CITY.=$locality;
	}

	if($area!=''){
		if($CLIENT_ADDRESS_CITY!=""){
			$CLIENT_ADDRESS_CITY.=', ';
		}
		$CLIENT_ADDRESS_CITY.=$area;
	}

	if($town!=''){
		$CLIENT_ADDRESS_TOWN.=$town;
	}

	if($county!=''){
		if($CLIENT_ADDRESS_TOWN!=""){
			$CLIENT_ADDRESS_TOWN.=', ';
		}
		$CLIENT_ADDRESS_TOWN.=$county;
	}

	if($postcode!=''){
		$CLIENT_ADDRESS_POSTCODE.=$postcode;
	}

	$CLIENT_ADDRESS_LINE_1			= $db2->escape($CLIENT_ADDRESS_LINE_1);
	$CLIENT_ADDRESS_LINE_2			= $db2->escape($CLIENT_ADDRESS_LINE_2);
	$CLIENT_ADDRESS_CITY			= $db2->escape($CLIENT_ADDRESS_CITY);
	$CLIENT_ADDRESS_TOWN			= $db2->escape($CLIENT_ADDRESS_TOWN);
	$CLIENT_ADDRESS_POSTCODE		= $db2->escape($CLIENT_ADDRESS_POSTCODE);

	$CLIENT_PHONE_1			= $phone_2;
	$CLIENT_PHONE_2			= $phone_3;
	$CLIENT_PHONE_3			= $phone_4;
	$CLIENT_PHONE_4			= $phone_5;
	$CLIENT_PHONE_5			= '';

	$CLIENT_MOBILE_1		= $mobile_2;
	$CLIENT_MOBILE_2		= $mobile_3;
	$CLIENT_MOBILE_3		= $mobile_4;
	$CLIENT_MOBILE_4		= $mobile_5;
	$CLIENT_MOBILE_5		= '';

	$CLIENT_FAX_1			= $fax_2;
	$CLIENT_FAX_2			= $fax_3;
	$CLIENT_FAX_3			= $fax_4;
	$CLIENT_FAX_4			= $fax_5;
	$CLIENT_FAX_5			= '';

	$CLIENT_NOTES.= $db2->escape($row['notes']);

	$sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON', '0')";

	$db1->execute($sql) or die($sql);
}
echo $p;
?>
<script>
window.location="clients_vebra_v1.php?p=<?= $p+1;?>";
</script>
<?php
} 
else { 
	echo "END ";
}
?>
