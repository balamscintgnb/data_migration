<?php
require_once '../includes/config.php';
require_once '../../header_init.php';

$db_connect_src = new GNBCore($sql_details_src, $isDevelopment);

$query = "SELECT * FROM `en_conv_history` WHERE `AppID` != '0'";

$data = json_decode($db_connect_src->queryFetch($query),true);

/*print_r($data);
echo '</pre>';

exit;*/

if(count($data)>0){

  foreach($data['data'] as $row){

    $NOTES			= '';
    $VIEWING_STATUS='';
    $APPLICANT_ID=0;
    $PROPERTY_ID=0;
    $USER_ID=0;
    $USER_EMAIL='';

    $ID		= $row['PK_HistoryID'];
    $NOTES.='Event Date:'.date('d.m.Y',strtotime($row['EventDate']));
    $NOTES.='\n '.$row['Comment'];
    $NOTES.='\n '.$row['FollowNotes'];
    $NOTES.='\n '.date('d.m.Y',strtotime($row['FollowDate']));

    if($row['Deposit']>0){
        $NOTES.='\n Deposit:'.$row['Deposit'];
    }

    if($row['Amount']>0){
      $NOTES.='\n Amount:'.$row['Amount'];
    }

    if($row['Confirmed']==1){
        $VIEWING_STATUS=1;
    }else{
        $VIEWING_STATUS=0;
    }

    $PROPERTY_ID				= $row['PropID'];

    $APPLICANT_ID = $row['AppID'];

    $USER_ID = $row['UserID'];

    if($USER_ID==1){
			$USER_EMAIL='admin@lighthouse-estateagents.co.uk';
		}else if($USER_ID==3 || $USER_ID==4 || $USER_ID==5 || $USER_ID==7 || $USER_ID==11){
			$USER_EMAIL='anne@lighthouse-estateagents.co.uk';
		}else if($USER_ID==6){
			$USER_EMAIL='Jessica@lighthouse-estateagents.co.uk';
		}else if($USER_ID==8){
			$USER_EMAIL='leanne@lighthouse-estateagent.co.uk';
		}else if($USER_ID==12){
			$USER_EMAIL='simon@lighthouse-estateagents.co.uk';
		}else{
			$USER_EMAIL='';
		}

    $VIEWING_DATE	= date('Y-m-d H:i:s',strtotime($row['ViewDate']));

    $sql = "INSERT INTO `viewings2` (`VIEWING_ID`, `VIEWING_APPLICANT_ID`, `VIEWING_USER_ID`, `VIEWING_PROPERTY_ID`, `VIEWING_DATETIME`, `VIEWING_NOTES`,`VIEWING_STATUS`)
    VALUES ('$ID', '$APPLICANT_ID', '$USER_EMAIL', '$PROPERTY_ID', '$VIEWING_DATE', '$NOTES','$VIEWING_STATUS');";
		$db_connect->queryExecute($sql);
  }

}

?>
