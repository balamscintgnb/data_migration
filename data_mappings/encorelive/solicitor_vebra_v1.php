<?php

error_reporting(E_ALL);
ini_set('display_errors',1);
include 'datamapping_config.php';
$_database1 = 'cnb_jennings';
$_database2 = 'jennings_vebra_sales';
	 
$db1 = new DB_test($_database1);
$db2 = new DB_test($_database2);



$user_query = $db2->execute("SELECT * FROM `en_user` WHERE `email` != '';");
$admin_users = array();
while($user = $user_query->fetch_array()){
	$admin_users[$user['userid']] = $user['email'];
}

if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}

$query = $db2->execute("SELECT * FROM `en_contact` WHERE 1 LIMIT $page, 250");

if($query->num_rows>0){

while($row = $query->fetch_array()){

	$CLIENT_TYPE			= 'SOLICITOR';
	$CLIENT_SUB_TYPE		= $row['department'];

	$CLIENT_CREATED_ON 		= $row['dateentered'];
	$CLIENTID				= $row['contactid'];
	$CLIENT_TITLE			= $row['title'];
	$CLIENT_NAME			= $row['forename'].' '.$row['surname'];

	$SOLICITOR_NAME			= $row['nme'];

	if($SOLICITOR_NAME!='' && $SOLICITOR_NAME!=$CLIENT_NAME){
 	 if($CLIENT_NAME!=''){
 		 $CLIENT_NAME.=' ';
 	 }
 	 $CLIENT_NAME.='('.$SOLICITOR_NAME.')';
  }

	$CLIENT_NAME			= trim($CLIENT_NAME);
	$CLIENT_STAFF_ID		= $admin_users[$row['userid']];

	$CLIENT_STATUS = 'ACTIVE';

	$CLIENT_PRIMARY_PHONE='';

	$CLIENT_PRIMARY_EMAIL='';

  $email1					= trim($row['email1']);

	$CLIENT_NOTES			= '';
	$CLIENT_PRIMARY_PHONE	= trim($row['tel1']);

	if($db2->validate_email($email1)){
		$CLIENT_PRIMARY_EMAIL	= $email1;
	}
	else{
		$CLIENT_NOTES			= $email1.' ';
	}

	$CLIENT_EMAIL_1			= $row['email2'];
	$CLIENT_EMAIL_2			= '';
	$CLIENT_EMAIL_3			= '';
	$CLIENT_EMAIL_4			= '';
	$CLIENT_EMAIL_5			= '';

	$phone_1= ''; $phone_2= ''; $phone_3= ''; $phone_4= ''; $phone_5= ''; $mobile_1= ''; $mobile_2= ''; $mobile_3= ''; $mobile_4= ''; $mobile_5= ''; $fax_1= ''; $fax_2= ''; $fax_3= ''; $fax_4= ''; $fax_5= '';

	$tel2_type 				= $row['teltype2'];
	$tel3_type 				= $row['teltype3'];
	$tel4_type 				= $row['teltype4'];

	$tel2_val 				= trim($row['tel2']);
	$tel3_val 				= trim($row['tel3']);
	$tel4_val 				= trim($row['tel4']);


	if($tel2_type==96 || $tel2_type==98 || $tel2_type==100 || $tel2_type==102 || $tel2_type==1359 || $tel2_type==261 || $tel2_type==262 || $tel2_type==263 || $tel2_type==264 || $tel2_type==0){
		$phone_2 = $tel2_val;
	}

	if($tel2_type==103 || $tel2_type==104 ){
		$mobile_2 = $tel2_val;
	}

	if($tel2_type==105 || $tel2_type==106 ){
		$fax_2 = $tel2_val;
	}


	if($tel3_type==96 || $tel3_type==98 || $tel3_type==100 || $tel3_type==102 || $tel3_type==1359 || $tel3_type==261 || $tel3_type==262 || $tel3_type==263 || $tel3_type==264 || $tel3_type==0){
		$phone_3 = $tel3_val;
	}

	if($tel3_type==103 || $tel3_type==104 ){
		$mobile_3 = $tel3_val;
	}

	if($tel3_type==105 || $tel3_type==106 ){
		$fax_3 = $tel3_val;
	}

	if($tel4_type==96 || $tel4_type==98 || $tel4_type==100 || $tel4_type==102 || $tel4_type==1359 || $tel4_type==261 || $tel4_type==262 || $tel4_type==263 || $tel4_type==264 || $tel4_type==0){
		$phone_4 = $tel4_val;
	}

	if($tel4_type==103 || $tel4_type==104 ){
		$mobile_4 = $tel4_val;
	}

	if($tel4_type==105 || $tel4_type==106 ){
		$fax_4 = $tel4_val;
	}

	/*if($mobile_2==''){
		$mobile_2 	= trim($row['s_tel1']);
	}

	if($mobile_3==''){
		$mobile_3 	= trim($row['s_tel2']);
	}

	if($mobile_4==''){
		$mobile_4 	= trim($row['s_tel3']);
	}

	if($mobile_5==''){
		$mobile_5 	= trim($row['s_tel4']);
	}*/


	$CLIENT_ADDRESS_LINE_1 	= '';
	$CLIENT_ADDRESS_LINE_2	= '';
	$CLIENT_ADDRESS_CITY	= '';
	$CLIENT_ADDRESS_TOWN	= '';
	$CLIENT_ADDRESS_POSTCODE= '';

	$housename			= trim($row['housename']);
	$housenumber		= trim($row['housenumber']);
	$street				= trim($row['street']);
	$locality			= trim($row['locality']);
	$area				= trim($row['area']);
	$town				= trim($row['town']);
	$county				= trim($row['county']);
	$postcode			= trim($row['postcode']);

	if($housename!=''){
		$CLIENT_ADDRESS_LINE_1.=$housename;
	}

	if($housenumber!=''){
		if($CLIENT_ADDRESS_LINE_1!=""){
			$CLIENT_ADDRESS_LINE_1.=', ';
		}
		$CLIENT_ADDRESS_LINE_1.=$housenumber;
	}

	if($street!=''){
		$CLIENT_ADDRESS_LINE_2.=$street;
	}

	if($locality!=''){
		$CLIENT_ADDRESS_CITY.=$locality;
	}

	if($area!=''){
		if($CLIENT_ADDRESS_CITY!=""){
			$CLIENT_ADDRESS_CITY.=', ';
		}
		$CLIENT_ADDRESS_CITY.=$area;
	}

	if($town!=''){
		$CLIENT_ADDRESS_TOWN.=$town;
	}

	if($county!=''){
		if($CLIENT_ADDRESS_TOWN!=""){
			$CLIENT_ADDRESS_TOWN.=', ';
		}
		$CLIENT_ADDRESS_TOWN.=$county;
	}

	if($postcode!=''){
		$CLIENT_ADDRESS_POSTCODE.=$postcode;
	}

	$CLIENT_ADDRESS_LINE_1			= trim($CLIENT_ADDRESS_LINE_1);
	$CLIENT_ADDRESS_LINE_2			= trim($CLIENT_ADDRESS_LINE_2);
	$CLIENT_ADDRESS_CITY			= trim($CLIENT_ADDRESS_CITY);
	$CLIENT_ADDRESS_TOWN			= trim($CLIENT_ADDRESS_TOWN);
	$CLIENT_ADDRESS_POSTCODE		= trim($CLIENT_ADDRESS_POSTCODE);

	$CLIENT_PHONE_1			= $phone_2;
	$CLIENT_PHONE_2			= $phone_3;
	$CLIENT_PHONE_3			= $phone_4;
	$CLIENT_PHONE_4			= $phone_5;
	$CLIENT_PHONE_5			= '';

	$CLIENT_MOBILE_1		= $mobile_2;
	$CLIENT_MOBILE_2		= $mobile_3;
	$CLIENT_MOBILE_3		= $mobile_4;
	$CLIENT_MOBILE_4		= $mobile_5;
	$CLIENT_MOBILE_5		= '';

	$CLIENT_FAX_1			= $fax_2;
	$CLIENT_FAX_2			= $fax_3;
	$CLIENT_FAX_3			= $fax_4;
	$CLIENT_FAX_4			= $fax_5;
	$CLIENT_FAX_5			= '';

	$CLIENT_NOTES.= trim($row['notes']);

	echo $sql = "INSERT INTO `clients`(`CLIENTID`, `CLIENT_TITLE`, `CLIENT_NAME`, `CLIENT_TYPE`, `CLIENT_SUB_TYPE`, `CLIENT_STATUS`, `CLIENT_STAFF_ID`, `CLIENT_PRIMARY_EMAIL`, `CLIENT_PRIMARY_PHONE`, `CLIENT_ADDRESS_LINE_1`, `CLIENT_ADDRESS_LINE_2`, `CLIENT_ADDRESS_CITY`, `CLIENT_ADDRESS_TOWN`, `CLIENT_ADDRESS_POSTCODE`, `CLIENT_EMAIL_1`, `CLIENT_EMAIL_2`, `CLIENT_EMAIL_3`, `CLIENT_EMAIL_4`, `CLIENT_EMAIL_5`, `CLIENT_PHONE_1`, `CLIENT_PHONE_2`, `CLIENT_PHONE_3`, `CLIENT_PHONE_4`, `CLIENT_PHONE_5`, `CLIENT_MOBILE_1`, `CLIENT_MOBILE_2`, `CLIENT_MOBILE_3`, `CLIENT_MOBILE_4`, `CLIENT_MOBILE_5`, `CLIENT_FAX_1`, `CLIENT_FAX_2`, `CLIENT_FAX_3`, `CLIENT_FAX_4`, `CLIENT_FAX_5`, `CLIENT_NOTES`, `CLIENT_CREATED_ON`, `RECORD_UPLOADED`) VALUES ('$CLIENTID', '$CLIENT_TITLE', '$CLIENT_NAME', '$CLIENT_TYPE', '$CLIENT_SUB_TYPE', '$CLIENT_STATUS', '$CLIENT_STAFF_ID', '$CLIENT_PRIMARY_EMAIL', '$CLIENT_PRIMARY_PHONE', '$CLIENT_ADDRESS_LINE_1', '$CLIENT_ADDRESS_LINE_2', '$CLIENT_ADDRESS_CITY', '$CLIENT_ADDRESS_TOWN', '$CLIENT_ADDRESS_POSTCODE', '$CLIENT_EMAIL_1', '$CLIENT_EMAIL_2', '$CLIENT_EMAIL_3', '$CLIENT_EMAIL_4', '$CLIENT_EMAIL_5', '$CLIENT_PHONE_1', '$CLIENT_PHONE_2', '$CLIENT_PHONE_3', '$CLIENT_PHONE_4', '$CLIENT_PHONE_5', '$CLIENT_MOBILE_1', '$CLIENT_MOBILE_2', '$CLIENT_MOBILE_3', '$CLIENT_MOBILE_4', '$CLIENT_MOBILE_5', '$CLIENT_FAX_1', '$CLIENT_FAX_2', '$CLIENT_FAX_3', '$CLIENT_FAX_4', '$CLIENT_FAX_5', '$CLIENT_NOTES', '$CLIENT_CREATED_ON', '0')";
	$db1->execute($sql);


}

}


?>
