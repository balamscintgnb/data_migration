<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item">
        <a class="nav-link " href="index.php">
            <span data-feather="home"></span>
            Dashboard <span class="sr-only">(current)</span>
        </a>
        </li>  
        <li class="nav-item">
        <a class="nav-link " href="table-lists.php">
            <span data-feather="list"></span>
            Table List 
        </a>
        </li>
        
        <li class="nav-item">
	        <a class="nav-link " href="generate_data.php">
	            <span data-feather="list"></span>
	            Generate Data
	        </a>
        </li>
        
        <li class="nav-item">
        <a class="nav-link " href="settings.php">
            <span data-feather="list"></span>
            Settings 
        </a>
        </li>

        <li class="nav-item">
        <a class="nav-link " href="image_upload.php">
            <span data-feather="list"></span>
            Upload Image 
        </a>
        </li>
        
         <li class="nav-item">
	        <a class="nav-link " href="get_external_properties.php">
	            <span data-feather="list"></span>
	            Extract properties
	        </a>
        </li>
    </ul>
    </div>
</nav>