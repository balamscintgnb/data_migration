<?php
extract($_GET);
extract($_POST);

@session_start();

if(isset($debug) && $debug==1){
    error_reporting(E_ALL);
    ini_set('display_errors',1);
}

require_once 'includes/classes/gnbcore.class.php';
require_once 'includes/classes/utility.class.php';

$server = $_SERVER["SERVER_NAME"];

$isDevelopment=true;
/*if(stristr($server, 'localhost')){
    $isDevelopment=true;
    $db_name = 'cnb_gnbdata';//todo change for diff. client's
}else{*/
    $db_name=Utility::get_subdomain($server);
//}

require_once 'includes/config.php';
//var_dump($sql_details);
//exit;
if(isset($setup_cnb) && $setup_cnb){
	$sql_details['database'] = 'gnb';
	
	if(!isset($_GET['verify']) || $_GET['verify']!='verify%^2354' || !isset($_POST['post_verify']) || $_POST['post_verify']!='post#$6Verify'){
		die('Unauthorized Access');
	}
}

$db_connect = new GNBCore($sql_details, $isDevelopment);

$currentFile = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentFile);
$page = $parts[count($parts) - 1];

if(!isset($_SESSION['user']) && $page!='login.php') {
    header('location: login.php');
    exit;
}
?>