<?php
require_once 'header_init.php';

extract($_POST);


$utility = new Utility();

$push_data_limit = array('notes'=>80, 'property_images'=>5, 'property_files'=>5, 'sale_offers'=>10, 'letting_offers'=>10, 'maintenance_jobs'=>50,'contacts_files'=>5); 
//proeprty, lettings
//$query = "SELECT * from $table where  RECORD_UPLOADED=0 limit 0, $record_limit";

//'TY00117', 'TY00114'
//$query = "SELECT * FROM `tenant_transactions` as t where  t.RECORD_UPLOADED=0  order by t.LETTING_CUSTOM_REF_NO asc , ISNULL(t.RENT_STARTDATE) asc, ISNULL(t.DUE_DATE) asc limit 0, $record_limit";

//t.LETTING_CUSTOM_REF_NO in ( '1000', '1001', '1003', '1004') and

/*
//$query = "SELECT t.* FROM `tenant_transactions` as t left join lettings as l on t.`LETTING_CUSTOM_REF_NO`=l.LETTING_CUSTOM_REF_NO where t.LETTING_CUSTOM_REF_NO!='' and ( l.LETTING_END_DATE='0000-00-00' or l.LETTING_END_DATE='1899-12-30') and t.RECORD_UPLOADED=0 order by t.LETTING_CUSTOM_REF_NO asc , ISNULL(t.RENT_STARTDATE) asc, ISNULL(t.DUE_DATE) asc limit 0, $record_limit";

//, 'frien_000006', 'frien_000007'
//PROPERTY_API_KEY in ('frien_000004', 'frien_000006', 'frien_000007') and
//$images_query = "SELECT * FROM `property_files` where  RECORD_UPLOADED=0 order by PROPERTY_API_KEY asc, ISNULL(PROPERTY_ROOM_API_KEY) asc, ISNULL(PROPERTY_FILE_ORDER) asc, PROPERTY_FILE_PATH asc limit 0, $record_limit";
*/

//for rolling tenancies
//$query = "SELECT l.TYVAC, l.TYRENTDAT as FIRST_RENT_DATE ,t.* FROM `tenant_transactions` as t left join host_ten_inf as l on t.`LETTING_CUSTOM_REF_NO`=l.TYCODE where ( l.TYVAC='0000-00-00' or l.TYVAC='1899-12-30' or l.TYVAC is null) and t.RECORD_UPLOADED=0  order by t.RENT_STARTDATE asc, t.LETTING_CUSTOM_REF_NO asc , ISNULL(t.DUE_DATE) asc limit 0, $record_limit"; // and t.RECORD_UPLOADED=0

//$get_table_result = $db_connect->queryFetch($query);
$record_limit = $push_data_limit["$table"];

if(!isset($record_limit) || $record_limit==''){
	$record_limit=80;
}

$get_table_result = $db_connect->get_tablelists($table, $record_limit);

$table_result = json_decode($get_table_result);

$data = array();
$sent_ids = array();

if ($table_result->data) {
    $i = 0;
    foreach ($table_result->data as $table_lists) {
        $data[$i] = $table_lists;
        $sent_ids[] = $table_lists->id;
        $i++;
    }
}

if (count($data) > 0) {

    $board_information_data = $db_connect->get_boardinformation();
	$board_information = json_decode($board_information_data);
    $information = $board_information->data;

    $db_domain =   explode('_', $db_name);

    //todo move this
    $dev_name = '';
    $subdomain_name = $db_domain[2];

    $host = $_SERVER["SERVER_NAME"];

    if(isset($board) && $board != '' && $information->server==1){
       $subdomain_name = $board;
    }

    if($information->server==0){
        $dev_name = '.dev';
    }

    if($table=='property_images'){

        $data = $utility->uploadCDNServer($data, $subdomain_name, false);
 
        $url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/' . $table . '_api_migration.php';

    }else {
        $url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/' . $table . '_migration.php';
    }
	
	// echo $url;
	// exit;
	
	//echo '<pre><code>';var_dump($data, 1);echo '</code></pre>';
    $request = json_encode(array('Token' => 1234, 'Data' => $data), JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES); // token generate on both end
    //echo '<pre><code>';var_dump($request, 3);echo '</code></pre>';

    $header = array('Content-Type: application/json', 'Request-API-Token: GNB-DATA-1234-ASED');

    $response = $utility->getCurlData($url, $request, $header, false);

    $response_json = json_decode($response, true);

    $response_this = array('success' => false, 'message' => 'JSON Error', 'response' => $response, 'message_code'=>101);

    if ($response_json != null) {
        $response_this = $response_json;

        $failed_ids_array = array();

        if (isset($response_json['failed_ids'])) {
            $failed_ids = $response_json['failed_ids'];

            foreach ($failed_ids as $key => $value) {
                $failed_ids_array[] = $key;
            }

            $array_diff = array_diff($sent_ids, $failed_ids_array);

            if (count($array_diff) > 0) {
                $update_ids1 = implode(",", $array_diff);

                $db_connect->record_update_table($table, (array) $update_ids1, 1);
            }
            if(count($failed_ids_array) > 0) {
                $update_ids2 = implode(",", $failed_ids_array);

                $db_connect->record_update_table($table, (array) $update_ids2, 2);
            }

        }

    }
} else {
    $response_this = array('success' => true, 'message' => 'No Records to Push' , 'inserted'=>0, 'failed'=>0, 'message_code'=>100);
}

@header('Content-Type: application/json');
echo json_encode($response_this);
?>