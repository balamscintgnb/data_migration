<?php
require_once 'header_init.php';

extract($_POST);
extract($_GET);

$db_connect = new GNBCore($sql_details, $isDevelopment);

if(isset($_GET['p']) && $_GET['p']!=''){
	$p = $_GET['p'];
	$page = $p*250;
}
else{
	$p=0;
	$page = 0;
}

$utility = new Utility();

$query = "SELECT * FROM `properties` limit $page, 250";

$properties = json_decode($db_connect->queryFetch($query),true);

$query_total = "SELECT * FROM `properties`";

$properties_total = json_decode($db_connect->queryFetch($query_total),true);

$properties_total = sizeof($properties_total['data']);

if(sizeof($properties['data'])>0){

	$request_array = array();
	$ids = array();
	$assets = array();
	
	foreach($properties['data'] as $property){
		$request_array[] = $property['PROPERTY_ID'];
		$ids[$property['PROPERTY_ID']] = $property['id'];
		$assets[$property['id']] = json_decode(preg_replace('/\\\\/', '', $property['PROPERTY_ASSETS']));;
	//	echo json_decode($property['PROPERTY_ASSETS'],true));
	}

	$board_information_data = $db_connect->get_boardinformation();
	$board_information = json_decode($board_information_data);
	$information = $board_information->data;
	$db_domain =   explode('_', $db_name);
	
	//todo move this
	$dev_name = '';
	$subdomain_name = $db_domain[2];
	
	$host = $_SERVER["SERVER_NAME"];
	
	if(isset($board) && $board != '' && $information->server==1){
	   $subdomain_name = $board;
	}
	
	if($information->server==0){
	    $dev_name = '.dev';
	}

	$url = 'https://' . $subdomain_name . $dev_name . '.globalnoticeboard.com' . '/plugins/data_migration/get_property_server_id.php';

	$request = json_encode(array('Token' => 1234, 'Data' => $request_array)); // token generate on both end
	
	$header = array('Content-Type: application/json', 'Request-API-Token: GNB-DATA-1234-ASED');
	
	$response = $utility->getCurlData($url, $request, $header, false);
	$response = json_decode($response);
	
	$success_ids = $response->success_ids;
	
	foreach($success_ids as $key => $value){
		
		 $id = $ids[$key];
	
		//$status = $db_connect->record_update_server_info($table, $id, $value);
		//$status = json_decode($status);
		//if($status->success){
		$property_assets = $assets[$id];
		$i=0;
		
		foreach($property_assets as $property_asset){
			if($property_asset->type=='photo' && $i==0){
				$columns = array('PROPERTY_SERVER_ID', 'PROPERTY_LOCAL_ID', 'PROPERTY_IMAGE_TYPE', 'PROPERTY_IMAGE');
				$values = array($value, $id, 'display_photo', $property_asset->path);
				$db_connect->insert_table_common($columns, $values,'property_images');
			}

			$columns = array('PROPERTY_SERVER_ID', 'PROPERTY_LOCAL_ID', 'PROPERTY_IMAGE_TYPE', 'PROPERTY_IMAGE');
			$values = array($value, $id, $property_asset->type, $property_asset->path);
			$db_connect->insert_table_common($columns, $values,'property_images');
			$i++;
		}
	}
}

echo json_encode(array("success"=> true,"total"=> $properties_total));
?>