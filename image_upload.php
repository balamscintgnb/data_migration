<?php
require_once 'header.php';
?>

<link rel="stylesheet" href="assets/css/dashboard.css" />
<script type="text/javascript" src="assets/js/plupload.full.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
</head>
  <body>
  <?php include_once 'header_menu.php';?>

<div class="container-fluid">
      <div class="row">
        
        <?php include_once 'side_menu.php';?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

<h4 class=""> Upload Data </h4>

<form method="post" id="zip_form" name="zip_form" enctype="multipart/form-data">

<div class="form-group" id="file_container">
	<label for="exampleFormControlFile1">Example file input .zip</label>
	<?php 
	
	$my_url = parse_url($_SERVER['HTTP_HOST']);
	$my_path = explode('.',$my_url['path']);
	?>
	<input type="file" id="zip_bulk" name="file" accept=".zip" />
	<br />
	<input type="hidden" id="filename" name="filename" />
	Path <input type="text" name="folder_path_to" id="folder_path_to" value="" /> Your files will be saved under data/<?php echo $my_path[0]; ?>/< Your Folder >
	
	<br/><br/>
	
	<span id="upload_status"></span>
	
</div>

  <input type="button" class="btn btn-primary" name="save" id="zip_upload" value="Submit" />

</form>

<div class="log_holder">

</div>
</main>

</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace();
    $('.log_holder').height($(window).height()-300)
    
    $(document).ready(function(){
		upload_documents();
	});
	
	function upload_documents(){
		
	var title="Doc files";
	var extensions="ZIP,zip";
	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		browse_button : 'zip_bulk', // you can pass an id...
		container: document.getElementById('file_container'), // ... or DOM Element itself
		url : 'upload_import_documents.php',
		flash_swf_url : 'js/uploadjs/Moxie.swf',
		silverlight_xap_url : 'js/uploadjs/Moxie.xap',
		
		filters : {
			max_file_size : '0',
			mime_types: [
				{title : title, extensions : extensions}
			]
		},
	
		init: {
			PostInit: function() {
			},
	
			FilesAdded: function(up, files) {
				var i=0;
				$('#zip_upload').attr('disabled', true);
				plupload.each(files, function(file) {
					i++;
						//$('#filelist').append(html);
				});
				uploader.start();
					
				},
	
			UploadProgress: function(up, file) {
			document.getElementById("upload_status").innerHTML = '<span>Uploading :' + file.percent + "%</span>";
			},
			
			FileUploaded: function(up, file, info) {
				var response = jQuery.parseJSON(info.response);
				
				document.getElementById("filename").value = response.zipfilename;
				//$('#original_doc_image').val(response.originalname)
				
			},
			UploadComplete: function(){
				$('#zip_upload').attr('disabled', false);
				document.getElementById("upload_status").innerHTML = '<span>Uploaded, Submit to Extract</span>';
			},
			Error: function(up, err) {
				//document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
			}
		}
	});
	uploader.init();
	}
	
	$('#zip_upload').click(function(){
		var vals = $('#zip_form').serialize();
		if($('#filename').val()!=''){
			$.post('upload_zip_docs.php',vals, function(data){
				var info = jQuery.parseJSON(data);
				if(info.success){
					$('#upload_status').html(info.response);
				}else{
					$('#upload_status').html(info.response);
				}
			});
		}
	});
    
    
    </script>


  </body>
</html>


